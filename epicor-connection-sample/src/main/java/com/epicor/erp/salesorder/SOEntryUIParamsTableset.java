
package com.epicor.erp.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOEntryUIParamsTableset complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOEntryUIParamsTableset">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceTableset">
 *       &lt;sequence>
 *         &lt;element name="SOEntryUIParams" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SOEntryUIParamsTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOEntryUIParamsTableset", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "soEntryUIParams"
})
public class SOEntryUIParamsTableset
    extends IceTableset
{

    @XmlElementRef(name = "SOEntryUIParams", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<SOEntryUIParamsTable> soEntryUIParams;

    /**
     * Gets the value of the soEntryUIParams property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SOEntryUIParamsTable }{@code >}
     *     
     */
    public JAXBElement<SOEntryUIParamsTable> getSOEntryUIParams() {
        return soEntryUIParams;
    }

    /**
     * Sets the value of the soEntryUIParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SOEntryUIParamsTable }{@code >}
     *     
     */
    public void setSOEntryUIParams(JAXBElement<SOEntryUIParamsTable> value) {
        this.soEntryUIParams = value;
    }

}
