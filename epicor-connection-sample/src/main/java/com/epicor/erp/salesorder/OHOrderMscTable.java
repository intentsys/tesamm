
package com.epicor.erp.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OHOrderMscTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OHOrderMscTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OHOrderMscRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OHOrderMscRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OHOrderMscTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "ohOrderMscRow"
})
public class OHOrderMscTable {

    @XmlElement(name = "OHOrderMscRow", nillable = true)
    protected List<OHOrderMscRow> ohOrderMscRow;

    /**
     * Gets the value of the ohOrderMscRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ohOrderMscRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOHOrderMscRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OHOrderMscRow }
     * 
     * 
     */
    public List<OHOrderMscRow> getOHOrderMscRow() {
        if (ohOrderMscRow == null) {
            ohOrderMscRow = new ArrayList<OHOrderMscRow>();
        }
        return this.ohOrderMscRow;
    }

    /**
     * Sets the value of the ohOrderMscRow property.
     * 
     * @param ohOrderMscRow
     *     allowed object is
     *     {@link OHOrderMscRow }
     *     
     */
    public void setOHOrderMscRow(List<OHOrderMscRow> ohOrderMscRow) {
        this.ohOrderMscRow = ohOrderMscRow;
    }

}
