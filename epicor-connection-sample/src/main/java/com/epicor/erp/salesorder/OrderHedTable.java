
package com.epicor.erp.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderHedTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderHedTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderHedRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderHedRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderHedTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "orderHedRow"
})
public class OrderHedTable {

    @XmlElement(name = "OrderHedRow", nillable = true)
    protected List<OrderHedRow> orderHedRow;

    /**
     * Gets the value of the orderHedRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderHedRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderHedRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderHedRow }
     * 
     * 
     */
    public List<OrderHedRow> getOrderHedRow() {
        if (orderHedRow == null) {
            orderHedRow = new ArrayList<OrderHedRow>();
        }
        return this.orderHedRow;
    }

    /**
     * Sets the value of the orderHedRow property.
     * 
     * @param orderHedRow
     *     allowed object is
     *     {@link OrderHedRow }
     *     
     */
    public void setOrderHedRow(List<OrderHedRow> orderHedRow) {
        this.orderHedRow = orderHedRow;
    }

}
