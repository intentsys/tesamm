
package com.epicor.erp.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetJobProdResult" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrdRelJobProdTableset" minOccurs="0"/>
 *         &lt;element name="morePages" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getJobProdResult",
    "morePages"
})
@XmlRootElement(name = "GetJobProdResponse")
public class GetJobProdResponse {

    @XmlElementRef(name = "GetJobProdResult", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<OrdRelJobProdTableset> getJobProdResult;
    protected Boolean morePages;

    /**
     * Gets the value of the getJobProdResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrdRelJobProdTableset }{@code >}
     *     
     */
    public JAXBElement<OrdRelJobProdTableset> getGetJobProdResult() {
        return getJobProdResult;
    }

    /**
     * Sets the value of the getJobProdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrdRelJobProdTableset }{@code >}
     *     
     */
    public void setGetJobProdResult(JAXBElement<OrdRelJobProdTableset> value) {
        this.getJobProdResult = value;
    }

    /**
     * Gets the value of the morePages property.
     * This getter has been renamed from isMorePages() to getMorePages() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMorePages() {
        return morePages;
    }

    /**
     * Sets the value of the morePages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMorePages(Boolean value) {
        this.morePages = value;
    }

}
