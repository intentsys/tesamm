
package com.epicor.erp.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BOUpdErrorTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BOUpdErrorTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BOUpdErrorRow" type="{http://schemas.datacontract.org/2004/07/Ice}BOUpdErrorRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BOUpdErrorTable", namespace = "http://schemas.datacontract.org/2004/07/Ice", propOrder = {
    "boUpdErrorRow"
})
public class BOUpdErrorTable {

    @XmlElement(name = "BOUpdErrorRow", nillable = true)
    protected List<BOUpdErrorRow> boUpdErrorRow;

    /**
     * Gets the value of the boUpdErrorRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the boUpdErrorRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBOUpdErrorRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BOUpdErrorRow }
     * 
     * 
     */
    public List<BOUpdErrorRow> getBOUpdErrorRow() {
        if (boUpdErrorRow == null) {
            boUpdErrorRow = new ArrayList<BOUpdErrorRow>();
        }
        return this.boUpdErrorRow;
    }

    /**
     * Sets the value of the boUpdErrorRow property.
     * 
     * @param boUpdErrorRow
     *     allowed object is
     *     {@link BOUpdErrorRow }
     *     
     */
    public void setBOUpdErrorRow(List<BOUpdErrorRow> boUpdErrorRow) {
        this.boUpdErrorRow = boUpdErrorRow;
    }

}
