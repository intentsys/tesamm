
package com.epicor.erp.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderHedAttchTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderHedAttchTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderHedAttchRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderHedAttchRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderHedAttchTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "orderHedAttchRow"
})
public class OrderHedAttchTable {

    @XmlElement(name = "OrderHedAttchRow", nillable = true)
    protected List<OrderHedAttchRow> orderHedAttchRow;

    /**
     * Gets the value of the orderHedAttchRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderHedAttchRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderHedAttchRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderHedAttchRow }
     * 
     * 
     */
    public List<OrderHedAttchRow> getOrderHedAttchRow() {
        if (orderHedAttchRow == null) {
            orderHedAttchRow = new ArrayList<OrderHedAttchRow>();
        }
        return this.orderHedAttchRow;
    }

    /**
     * Sets the value of the orderHedAttchRow property.
     * 
     * @param orderHedAttchRow
     *     allowed object is
     *     {@link OrderHedAttchRow }
     *     
     */
    public void setOrderHedAttchRow(List<OrderHedAttchRow> orderHedAttchRow) {
        this.orderHedAttchRow = orderHedAttchRow;
    }

}
