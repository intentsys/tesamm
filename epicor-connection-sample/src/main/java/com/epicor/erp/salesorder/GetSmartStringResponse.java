
package com.epicor.erp.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SmartString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreatePart" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PromptForPartNum" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="NotifyOfExistingPart" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="NewPartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreateCustPart" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PromptForCustPartNum" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="NewCustPartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PromptForAutoCreatePart" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "smartString",
    "createPart",
    "promptForPartNum",
    "notifyOfExistingPart",
    "newPartNum",
    "createCustPart",
    "promptForCustPartNum",
    "newCustPartNum",
    "promptForAutoCreatePart"
})
@XmlRootElement(name = "GetSmartStringResponse")
public class GetSmartStringResponse {

    @XmlElementRef(name = "SmartString", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> smartString;
    @XmlElement(name = "CreatePart")
    protected Boolean createPart;
    @XmlElement(name = "PromptForPartNum")
    protected Boolean promptForPartNum;
    @XmlElement(name = "NotifyOfExistingPart")
    protected Boolean notifyOfExistingPart;
    @XmlElementRef(name = "NewPartNum", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> newPartNum;
    @XmlElement(name = "CreateCustPart")
    protected Boolean createCustPart;
    @XmlElement(name = "PromptForCustPartNum")
    protected Boolean promptForCustPartNum;
    @XmlElementRef(name = "NewCustPartNum", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> newCustPartNum;
    @XmlElement(name = "PromptForAutoCreatePart")
    protected Boolean promptForAutoCreatePart;

    /**
     * Gets the value of the smartString property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSmartString() {
        return smartString;
    }

    /**
     * Sets the value of the smartString property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSmartString(JAXBElement<String> value) {
        this.smartString = value;
    }

    /**
     * Gets the value of the createPart property.
     * This getter has been renamed from isCreatePart() to getCreatePart() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCreatePart() {
        return createPart;
    }

    /**
     * Sets the value of the createPart property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCreatePart(Boolean value) {
        this.createPart = value;
    }

    /**
     * Gets the value of the promptForPartNum property.
     * This getter has been renamed from isPromptForPartNum() to getPromptForPartNum() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPromptForPartNum() {
        return promptForPartNum;
    }

    /**
     * Sets the value of the promptForPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPromptForPartNum(Boolean value) {
        this.promptForPartNum = value;
    }

    /**
     * Gets the value of the notifyOfExistingPart property.
     * This getter has been renamed from isNotifyOfExistingPart() to getNotifyOfExistingPart() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNotifyOfExistingPart() {
        return notifyOfExistingPart;
    }

    /**
     * Sets the value of the notifyOfExistingPart property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyOfExistingPart(Boolean value) {
        this.notifyOfExistingPart = value;
    }

    /**
     * Gets the value of the newPartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNewPartNum() {
        return newPartNum;
    }

    /**
     * Sets the value of the newPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNewPartNum(JAXBElement<String> value) {
        this.newPartNum = value;
    }

    /**
     * Gets the value of the createCustPart property.
     * This getter has been renamed from isCreateCustPart() to getCreateCustPart() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCreateCustPart() {
        return createCustPart;
    }

    /**
     * Sets the value of the createCustPart property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCreateCustPart(Boolean value) {
        this.createCustPart = value;
    }

    /**
     * Gets the value of the promptForCustPartNum property.
     * This getter has been renamed from isPromptForCustPartNum() to getPromptForCustPartNum() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPromptForCustPartNum() {
        return promptForCustPartNum;
    }

    /**
     * Sets the value of the promptForCustPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPromptForCustPartNum(Boolean value) {
        this.promptForCustPartNum = value;
    }

    /**
     * Gets the value of the newCustPartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNewCustPartNum() {
        return newCustPartNum;
    }

    /**
     * Sets the value of the newCustPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNewCustPartNum(JAXBElement<String> value) {
        this.newCustPartNum = value;
    }

    /**
     * Gets the value of the promptForAutoCreatePart property.
     * This getter has been renamed from isPromptForAutoCreatePart() to getPromptForAutoCreatePart() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPromptForAutoCreatePart() {
        return promptForAutoCreatePart;
    }

    /**
     * Sets the value of the promptForAutoCreatePart property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPromptForAutoCreatePart(Boolean value) {
        this.promptForAutoCreatePart = value;
    }

}
