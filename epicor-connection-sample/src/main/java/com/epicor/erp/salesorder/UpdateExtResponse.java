
package com.epicor.erp.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UpdateExtResult" type="{http://schemas.datacontract.org/2004/07/Ice}BOUpdErrorTableset" minOccurs="0"/>
 *         &lt;element name="ds" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}UpdExtSalesOrderTableset" minOccurs="0"/>
 *         &lt;element name="errorsOccurred" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateExtResult",
    "ds",
    "errorsOccurred"
})
@XmlRootElement(name = "UpdateExtResponse")
public class UpdateExtResponse {

    @XmlElementRef(name = "UpdateExtResult", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<BOUpdErrorTableset> updateExtResult;
    @XmlElementRef(name = "ds", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<UpdExtSalesOrderTableset> ds;
    protected Boolean errorsOccurred;

    /**
     * Gets the value of the updateExtResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BOUpdErrorTableset }{@code >}
     *     
     */
    public JAXBElement<BOUpdErrorTableset> getUpdateExtResult() {
        return updateExtResult;
    }

    /**
     * Sets the value of the updateExtResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BOUpdErrorTableset }{@code >}
     *     
     */
    public void setUpdateExtResult(JAXBElement<BOUpdErrorTableset> value) {
        this.updateExtResult = value;
    }

    /**
     * Gets the value of the ds property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link UpdExtSalesOrderTableset }{@code >}
     *     
     */
    public JAXBElement<UpdExtSalesOrderTableset> getDs() {
        return ds;
    }

    /**
     * Sets the value of the ds property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link UpdExtSalesOrderTableset }{@code >}
     *     
     */
    public void setDs(JAXBElement<UpdExtSalesOrderTableset> value) {
        this.ds = value;
    }

    /**
     * Gets the value of the errorsOccurred property.
     * This getter has been renamed from isErrorsOccurred() to getErrorsOccurred() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getErrorsOccurred() {
        return errorsOccurred;
    }

    /**
     * Sets the value of the errorsOccurred property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setErrorsOccurred(Boolean value) {
        this.errorsOccurred = value;
    }

}
