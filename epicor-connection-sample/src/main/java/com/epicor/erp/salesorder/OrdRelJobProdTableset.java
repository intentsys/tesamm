
package com.epicor.erp.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrdRelJobProdTableset complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrdRelJobProdTableset">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceTableset">
 *       &lt;sequence>
 *         &lt;element name="JobProd" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}JobProdTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrdRelJobProdTableset", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "jobProd"
})
public class OrdRelJobProdTableset
    extends IceTableset
{

    @XmlElementRef(name = "JobProd", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<JobProdTable> jobProd;

    /**
     * Gets the value of the jobProd property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link JobProdTable }{@code >}
     *     
     */
    public JAXBElement<JobProdTable> getJobProd() {
        return jobProd;
    }

    /**
     * Sets the value of the jobProd property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link JobProdTable }{@code >}
     *     
     */
    public void setJobProd(JAXBElement<JobProdTable> value) {
        this.jobProd = value;
    }

}
