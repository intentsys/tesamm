
package com.epicor.erp.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderRelTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderRelTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderRelRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderRelRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderRelTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "orderRelRow"
})
public class OrderRelTable {

    @XmlElement(name = "OrderRelRow", nillable = true)
    protected List<OrderRelRow> orderRelRow;

    /**
     * Gets the value of the orderRelRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderRelRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderRelRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderRelRow }
     * 
     * 
     */
    public List<OrderRelRow> getOrderRelRow() {
        if (orderRelRow == null) {
            orderRelRow = new ArrayList<OrderRelRow>();
        }
        return this.orderRelRow;
    }

    /**
     * Sets the value of the orderRelRow property.
     * 
     * @param orderRelRow
     *     allowed object is
     *     {@link OrderRelRow }
     *     
     */
    public void setOrderRelRow(List<OrderRelRow> orderRelRow) {
        this.orderRelRow = orderRelRow;
    }

}
