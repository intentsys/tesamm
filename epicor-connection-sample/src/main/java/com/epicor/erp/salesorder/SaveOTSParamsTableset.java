
package com.epicor.erp.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SaveOTSParamsTableset complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SaveOTSParamsTableset">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceTableset">
 *       &lt;sequence>
 *         &lt;element name="SaveOTSParams" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SaveOTSParamsTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SaveOTSParamsTableset", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "saveOTSParams"
})
public class SaveOTSParamsTableset
    extends IceTableset
{

    @XmlElementRef(name = "SaveOTSParams", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<SaveOTSParamsTable> saveOTSParams;

    /**
     * Gets the value of the saveOTSParams property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SaveOTSParamsTable }{@code >}
     *     
     */
    public JAXBElement<SaveOTSParamsTable> getSaveOTSParams() {
        return saveOTSParams;
    }

    /**
     * Sets the value of the saveOTSParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SaveOTSParamsTable }{@code >}
     *     
     */
    public void setSaveOTSParams(JAXBElement<SaveOTSParamsTable> value) {
        this.saveOTSParams = value;
    }

}
