
package com.epicor.erp.salesorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EpicorExceptionData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EpicorExceptionData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="m_ColumnNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="m_LineNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="m_Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="m_MessageDetails" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring"/>
 *         &lt;element name="m_Method" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="m_Program" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="m_Properties" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfKeyValueOfstringstring"/>
 *         &lt;element name="m_TraceStack" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="m_UserProperties" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfKeyValueOfstringstring"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EpicorExceptionData", namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", propOrder = {
    "mColumnNumber",
    "mLineNumber",
    "mMessage",
    "mMessageDetails",
    "mMethod",
    "mProgram",
    "mProperties",
    "mTraceStack",
    "mUserProperties"
})
public class EpicorExceptionData {

    @XmlElement(name = "m_ColumnNumber")
    protected int mColumnNumber;
    @XmlElement(name = "m_LineNumber")
    protected int mLineNumber;
    @XmlElement(name = "m_Message", required = true, nillable = true)
    protected String mMessage;
    @XmlElement(name = "m_MessageDetails", required = true, nillable = true)
    protected ArrayOfstring mMessageDetails;
    @XmlElement(name = "m_Method", required = true, nillable = true)
    protected String mMethod;
    @XmlElement(name = "m_Program", required = true, nillable = true)
    protected String mProgram;
    @XmlElement(name = "m_Properties", required = true, nillable = true)
    protected ArrayOfKeyValueOfstringstring mProperties;
    @XmlElement(name = "m_TraceStack", required = true, nillable = true)
    protected String mTraceStack;
    @XmlElement(name = "m_UserProperties", required = true, nillable = true)
    protected ArrayOfKeyValueOfstringstring mUserProperties;

    /**
     * Gets the value of the mColumnNumber property.
     * 
     */
    public int getMColumnNumber() {
        return mColumnNumber;
    }

    /**
     * Sets the value of the mColumnNumber property.
     * 
     */
    public void setMColumnNumber(int value) {
        this.mColumnNumber = value;
    }

    /**
     * Gets the value of the mLineNumber property.
     * 
     */
    public int getMLineNumber() {
        return mLineNumber;
    }

    /**
     * Sets the value of the mLineNumber property.
     * 
     */
    public void setMLineNumber(int value) {
        this.mLineNumber = value;
    }

    /**
     * Gets the value of the mMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMMessage() {
        return mMessage;
    }

    /**
     * Sets the value of the mMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMMessage(String value) {
        this.mMessage = value;
    }

    /**
     * Gets the value of the mMessageDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring }
     *     
     */
    public ArrayOfstring getMMessageDetails() {
        return mMessageDetails;
    }

    /**
     * Sets the value of the mMessageDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring }
     *     
     */
    public void setMMessageDetails(ArrayOfstring value) {
        this.mMessageDetails = value;
    }

    /**
     * Gets the value of the mMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMMethod() {
        return mMethod;
    }

    /**
     * Sets the value of the mMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMMethod(String value) {
        this.mMethod = value;
    }

    /**
     * Gets the value of the mProgram property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMProgram() {
        return mProgram;
    }

    /**
     * Sets the value of the mProgram property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMProgram(String value) {
        this.mProgram = value;
    }

    /**
     * Gets the value of the mProperties property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfKeyValueOfstringstring }
     *     
     */
    public ArrayOfKeyValueOfstringstring getMProperties() {
        return mProperties;
    }

    /**
     * Sets the value of the mProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfKeyValueOfstringstring }
     *     
     */
    public void setMProperties(ArrayOfKeyValueOfstringstring value) {
        this.mProperties = value;
    }

    /**
     * Gets the value of the mTraceStack property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMTraceStack() {
        return mTraceStack;
    }

    /**
     * Sets the value of the mTraceStack property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMTraceStack(String value) {
        this.mTraceStack = value;
    }

    /**
     * Gets the value of the mUserProperties property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfKeyValueOfstringstring }
     *     
     */
    public ArrayOfKeyValueOfstringstring getMUserProperties() {
        return mUserProperties;
    }

    /**
     * Sets the value of the mUserProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfKeyValueOfstringstring }
     *     
     */
    public void setMUserProperties(ArrayOfKeyValueOfstringstring value) {
        this.mUserProperties = value;
    }

}
