
package com.epicor.erp.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="whereClauseOrderHed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseOrderHedAttch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseOHOrderMsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseOrderDtl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseOrderDtlAttch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseOrderMsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseOrderRel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseOrderRelTax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseOrderHedUPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseOrderRepComm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseHedTaxSum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseOrderHist" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClausePartSubs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseSelectedSerialNumbers" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseSNFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whereClauseTaxConnectStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pageSize" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="absolutePage" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "whereClauseOrderHed",
    "whereClauseOrderHedAttch",
    "whereClauseOHOrderMsc",
    "whereClauseOrderDtl",
    "whereClauseOrderDtlAttch",
    "whereClauseOrderMsc",
    "whereClauseOrderRel",
    "whereClauseOrderRelTax",
    "whereClauseOrderHedUPS",
    "whereClauseOrderRepComm",
    "whereClauseHedTaxSum",
    "whereClauseOrderHist",
    "whereClausePartSubs",
    "whereClauseSelectedSerialNumbers",
    "whereClauseSNFormat",
    "whereClauseTaxConnectStatus",
    "pageSize",
    "absolutePage"
})
@XmlRootElement(name = "GetRows")
public class GetRows {

    @XmlElementRef(name = "whereClauseOrderHed", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseOrderHed;
    @XmlElementRef(name = "whereClauseOrderHedAttch", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseOrderHedAttch;
    @XmlElementRef(name = "whereClauseOHOrderMsc", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseOHOrderMsc;
    @XmlElementRef(name = "whereClauseOrderDtl", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseOrderDtl;
    @XmlElementRef(name = "whereClauseOrderDtlAttch", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseOrderDtlAttch;
    @XmlElementRef(name = "whereClauseOrderMsc", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseOrderMsc;
    @XmlElementRef(name = "whereClauseOrderRel", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseOrderRel;
    @XmlElementRef(name = "whereClauseOrderRelTax", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseOrderRelTax;
    @XmlElementRef(name = "whereClauseOrderHedUPS", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseOrderHedUPS;
    @XmlElementRef(name = "whereClauseOrderRepComm", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseOrderRepComm;
    @XmlElementRef(name = "whereClauseHedTaxSum", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseHedTaxSum;
    @XmlElementRef(name = "whereClauseOrderHist", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseOrderHist;
    @XmlElementRef(name = "whereClausePartSubs", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClausePartSubs;
    @XmlElementRef(name = "whereClauseSelectedSerialNumbers", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseSelectedSerialNumbers;
    @XmlElementRef(name = "whereClauseSNFormat", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseSNFormat;
    @XmlElementRef(name = "whereClauseTaxConnectStatus", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whereClauseTaxConnectStatus;
    protected Integer pageSize;
    protected Integer absolutePage;

    /**
     * Gets the value of the whereClauseOrderHed property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseOrderHed() {
        return whereClauseOrderHed;
    }

    /**
     * Sets the value of the whereClauseOrderHed property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseOrderHed(JAXBElement<String> value) {
        this.whereClauseOrderHed = value;
    }

    /**
     * Gets the value of the whereClauseOrderHedAttch property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseOrderHedAttch() {
        return whereClauseOrderHedAttch;
    }

    /**
     * Sets the value of the whereClauseOrderHedAttch property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseOrderHedAttch(JAXBElement<String> value) {
        this.whereClauseOrderHedAttch = value;
    }

    /**
     * Gets the value of the whereClauseOHOrderMsc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseOHOrderMsc() {
        return whereClauseOHOrderMsc;
    }

    /**
     * Sets the value of the whereClauseOHOrderMsc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseOHOrderMsc(JAXBElement<String> value) {
        this.whereClauseOHOrderMsc = value;
    }

    /**
     * Gets the value of the whereClauseOrderDtl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseOrderDtl() {
        return whereClauseOrderDtl;
    }

    /**
     * Sets the value of the whereClauseOrderDtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseOrderDtl(JAXBElement<String> value) {
        this.whereClauseOrderDtl = value;
    }

    /**
     * Gets the value of the whereClauseOrderDtlAttch property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseOrderDtlAttch() {
        return whereClauseOrderDtlAttch;
    }

    /**
     * Sets the value of the whereClauseOrderDtlAttch property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseOrderDtlAttch(JAXBElement<String> value) {
        this.whereClauseOrderDtlAttch = value;
    }

    /**
     * Gets the value of the whereClauseOrderMsc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseOrderMsc() {
        return whereClauseOrderMsc;
    }

    /**
     * Sets the value of the whereClauseOrderMsc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseOrderMsc(JAXBElement<String> value) {
        this.whereClauseOrderMsc = value;
    }

    /**
     * Gets the value of the whereClauseOrderRel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseOrderRel() {
        return whereClauseOrderRel;
    }

    /**
     * Sets the value of the whereClauseOrderRel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseOrderRel(JAXBElement<String> value) {
        this.whereClauseOrderRel = value;
    }

    /**
     * Gets the value of the whereClauseOrderRelTax property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseOrderRelTax() {
        return whereClauseOrderRelTax;
    }

    /**
     * Sets the value of the whereClauseOrderRelTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseOrderRelTax(JAXBElement<String> value) {
        this.whereClauseOrderRelTax = value;
    }

    /**
     * Gets the value of the whereClauseOrderHedUPS property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseOrderHedUPS() {
        return whereClauseOrderHedUPS;
    }

    /**
     * Sets the value of the whereClauseOrderHedUPS property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseOrderHedUPS(JAXBElement<String> value) {
        this.whereClauseOrderHedUPS = value;
    }

    /**
     * Gets the value of the whereClauseOrderRepComm property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseOrderRepComm() {
        return whereClauseOrderRepComm;
    }

    /**
     * Sets the value of the whereClauseOrderRepComm property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseOrderRepComm(JAXBElement<String> value) {
        this.whereClauseOrderRepComm = value;
    }

    /**
     * Gets the value of the whereClauseHedTaxSum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseHedTaxSum() {
        return whereClauseHedTaxSum;
    }

    /**
     * Sets the value of the whereClauseHedTaxSum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseHedTaxSum(JAXBElement<String> value) {
        this.whereClauseHedTaxSum = value;
    }

    /**
     * Gets the value of the whereClauseOrderHist property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseOrderHist() {
        return whereClauseOrderHist;
    }

    /**
     * Sets the value of the whereClauseOrderHist property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseOrderHist(JAXBElement<String> value) {
        this.whereClauseOrderHist = value;
    }

    /**
     * Gets the value of the whereClausePartSubs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClausePartSubs() {
        return whereClausePartSubs;
    }

    /**
     * Sets the value of the whereClausePartSubs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClausePartSubs(JAXBElement<String> value) {
        this.whereClausePartSubs = value;
    }

    /**
     * Gets the value of the whereClauseSelectedSerialNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseSelectedSerialNumbers() {
        return whereClauseSelectedSerialNumbers;
    }

    /**
     * Sets the value of the whereClauseSelectedSerialNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseSelectedSerialNumbers(JAXBElement<String> value) {
        this.whereClauseSelectedSerialNumbers = value;
    }

    /**
     * Gets the value of the whereClauseSNFormat property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseSNFormat() {
        return whereClauseSNFormat;
    }

    /**
     * Sets the value of the whereClauseSNFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseSNFormat(JAXBElement<String> value) {
        this.whereClauseSNFormat = value;
    }

    /**
     * Gets the value of the whereClauseTaxConnectStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhereClauseTaxConnectStatus() {
        return whereClauseTaxConnectStatus;
    }

    /**
     * Sets the value of the whereClauseTaxConnectStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhereClauseTaxConnectStatus(JAXBElement<String> value) {
        this.whereClauseTaxConnectStatus = value;
    }

    /**
     * Gets the value of the pageSize property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * Sets the value of the pageSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageSize(Integer value) {
        this.pageSize = value;
    }

    /**
     * Gets the value of the absolutePage property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAbsolutePage() {
        return absolutePage;
    }

    /**
     * Sets the value of the absolutePage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAbsolutePage(Integer value) {
        this.absolutePage = value;
    }

}
