
package com.epicor.erp.salesorder;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OHOrderMscRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OHOrderMscRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceRow">
 *       &lt;sequence>
 *         &lt;element name="BaseCurrSymbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BitFlag" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ChangeTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ChangeTrackAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ChangeTrackApproved" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ChangeTrackMemoDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChangeTrackMemoText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChangeTrackStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChangedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrSymbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencySwitch" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocDspMiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocInMiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocMiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DspMiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="EntryProcess" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExtCompany" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FreqCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ICPOLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ICPONum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ICPOSeqNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InMiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Linked" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MiscCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MiscCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OrderNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OrderNumCardMemberName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderNumCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Percentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Quoting" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Rpt1DspMiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1InMiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1MiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2DspMiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2InMiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2MiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3DspMiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3InMiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3MiscAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SeqNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SysRevID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OHOrderMscRow", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "baseCurrSymbol",
    "bitFlag",
    "changeDate",
    "changeTime",
    "changeTrackAmount",
    "changeTrackApproved",
    "changeTrackMemoDesc",
    "changeTrackMemoText",
    "changeTrackStatus",
    "changedBy",
    "company",
    "currSymbol",
    "currencyCode",
    "currencySwitch",
    "description",
    "docDspMiscAmt",
    "docInMiscAmt",
    "docMiscAmt",
    "dspMiscAmt",
    "entryProcess",
    "extCompany",
    "freqCode",
    "icpoLine",
    "icpoNum",
    "icpoSeqNum",
    "inMiscAmt",
    "linked",
    "miscAmt",
    "miscCode",
    "miscCodeDescription",
    "orderLine",
    "orderNum",
    "orderNumCardMemberName",
    "orderNumCurrencyCode",
    "percentage",
    "quoting",
    "rpt1DspMiscAmt",
    "rpt1InMiscAmt",
    "rpt1MiscAmt",
    "rpt2DspMiscAmt",
    "rpt2InMiscAmt",
    "rpt2MiscAmt",
    "rpt3DspMiscAmt",
    "rpt3InMiscAmt",
    "rpt3MiscAmt",
    "seqNum",
    "sysRevID",
    "type"
})
public class OHOrderMscRow
    extends IceRow
{

    @XmlElementRef(name = "BaseCurrSymbol", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> baseCurrSymbol;
    @XmlElement(name = "BitFlag")
    protected Integer bitFlag;
    @XmlElementRef(name = "ChangeDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> changeDate;
    @XmlElement(name = "ChangeTime")
    protected Integer changeTime;
    @XmlElement(name = "ChangeTrackAmount")
    protected BigDecimal changeTrackAmount;
    @XmlElement(name = "ChangeTrackApproved")
    protected Boolean changeTrackApproved;
    @XmlElementRef(name = "ChangeTrackMemoDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changeTrackMemoDesc;
    @XmlElementRef(name = "ChangeTrackMemoText", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changeTrackMemoText;
    @XmlElementRef(name = "ChangeTrackStatus", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changeTrackStatus;
    @XmlElementRef(name = "ChangedBy", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changedBy;
    @XmlElementRef(name = "Company", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> company;
    @XmlElementRef(name = "CurrSymbol", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currSymbol;
    @XmlElementRef(name = "CurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElement(name = "CurrencySwitch")
    protected Boolean currencySwitch;
    @XmlElementRef(name = "Description", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElement(name = "DocDspMiscAmt")
    protected BigDecimal docDspMiscAmt;
    @XmlElement(name = "DocInMiscAmt")
    protected BigDecimal docInMiscAmt;
    @XmlElement(name = "DocMiscAmt")
    protected BigDecimal docMiscAmt;
    @XmlElement(name = "DspMiscAmt")
    protected BigDecimal dspMiscAmt;
    @XmlElementRef(name = "EntryProcess", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> entryProcess;
    @XmlElementRef(name = "ExtCompany", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> extCompany;
    @XmlElementRef(name = "FreqCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> freqCode;
    @XmlElement(name = "ICPOLine")
    protected Integer icpoLine;
    @XmlElement(name = "ICPONum")
    protected Integer icpoNum;
    @XmlElement(name = "ICPOSeqNum")
    protected Integer icpoSeqNum;
    @XmlElement(name = "InMiscAmt")
    protected BigDecimal inMiscAmt;
    @XmlElement(name = "Linked")
    protected Boolean linked;
    @XmlElement(name = "MiscAmt")
    protected BigDecimal miscAmt;
    @XmlElementRef(name = "MiscCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> miscCode;
    @XmlElementRef(name = "MiscCodeDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> miscCodeDescription;
    @XmlElement(name = "OrderLine")
    protected Integer orderLine;
    @XmlElement(name = "OrderNum")
    protected Integer orderNum;
    @XmlElementRef(name = "OrderNumCardMemberName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderNumCardMemberName;
    @XmlElementRef(name = "OrderNumCurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderNumCurrencyCode;
    @XmlElement(name = "Percentage")
    protected BigDecimal percentage;
    @XmlElementRef(name = "Quoting", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> quoting;
    @XmlElement(name = "Rpt1DspMiscAmt")
    protected BigDecimal rpt1DspMiscAmt;
    @XmlElement(name = "Rpt1InMiscAmt")
    protected BigDecimal rpt1InMiscAmt;
    @XmlElement(name = "Rpt1MiscAmt")
    protected BigDecimal rpt1MiscAmt;
    @XmlElement(name = "Rpt2DspMiscAmt")
    protected BigDecimal rpt2DspMiscAmt;
    @XmlElement(name = "Rpt2InMiscAmt")
    protected BigDecimal rpt2InMiscAmt;
    @XmlElement(name = "Rpt2MiscAmt")
    protected BigDecimal rpt2MiscAmt;
    @XmlElement(name = "Rpt3DspMiscAmt")
    protected BigDecimal rpt3DspMiscAmt;
    @XmlElement(name = "Rpt3InMiscAmt")
    protected BigDecimal rpt3InMiscAmt;
    @XmlElement(name = "Rpt3MiscAmt")
    protected BigDecimal rpt3MiscAmt;
    @XmlElement(name = "SeqNum")
    protected Integer seqNum;
    @XmlElement(name = "SysRevID")
    protected Long sysRevID;
    @XmlElementRef(name = "Type", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> type;

    /**
     * Gets the value of the baseCurrSymbol property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBaseCurrSymbol() {
        return baseCurrSymbol;
    }

    /**
     * Sets the value of the baseCurrSymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBaseCurrSymbol(JAXBElement<String> value) {
        this.baseCurrSymbol = value;
    }

    /**
     * Gets the value of the bitFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBitFlag() {
        return bitFlag;
    }

    /**
     * Sets the value of the bitFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBitFlag(Integer value) {
        this.bitFlag = value;
    }

    /**
     * Gets the value of the changeDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getChangeDate() {
        return changeDate;
    }

    /**
     * Sets the value of the changeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setChangeDate(JAXBElement<XMLGregorianCalendar> value) {
        this.changeDate = value;
    }

    /**
     * Gets the value of the changeTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChangeTime() {
        return changeTime;
    }

    /**
     * Sets the value of the changeTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChangeTime(Integer value) {
        this.changeTime = value;
    }

    /**
     * Gets the value of the changeTrackAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChangeTrackAmount() {
        return changeTrackAmount;
    }

    /**
     * Sets the value of the changeTrackAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChangeTrackAmount(BigDecimal value) {
        this.changeTrackAmount = value;
    }

    /**
     * Gets the value of the changeTrackApproved property.
     * This getter has been renamed from isChangeTrackApproved() to getChangeTrackApproved() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getChangeTrackApproved() {
        return changeTrackApproved;
    }

    /**
     * Sets the value of the changeTrackApproved property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChangeTrackApproved(Boolean value) {
        this.changeTrackApproved = value;
    }

    /**
     * Gets the value of the changeTrackMemoDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangeTrackMemoDesc() {
        return changeTrackMemoDesc;
    }

    /**
     * Sets the value of the changeTrackMemoDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangeTrackMemoDesc(JAXBElement<String> value) {
        this.changeTrackMemoDesc = value;
    }

    /**
     * Gets the value of the changeTrackMemoText property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangeTrackMemoText() {
        return changeTrackMemoText;
    }

    /**
     * Sets the value of the changeTrackMemoText property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangeTrackMemoText(JAXBElement<String> value) {
        this.changeTrackMemoText = value;
    }

    /**
     * Gets the value of the changeTrackStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangeTrackStatus() {
        return changeTrackStatus;
    }

    /**
     * Sets the value of the changeTrackStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangeTrackStatus(JAXBElement<String> value) {
        this.changeTrackStatus = value;
    }

    /**
     * Gets the value of the changedBy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangedBy() {
        return changedBy;
    }

    /**
     * Sets the value of the changedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangedBy(JAXBElement<String> value) {
        this.changedBy = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompany(JAXBElement<String> value) {
        this.company = value;
    }

    /**
     * Gets the value of the currSymbol property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrSymbol() {
        return currSymbol;
    }

    /**
     * Sets the value of the currSymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrSymbol(JAXBElement<String> value) {
        this.currSymbol = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the currencySwitch property.
     * This getter has been renamed from isCurrencySwitch() to getCurrencySwitch() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCurrencySwitch() {
        return currencySwitch;
    }

    /**
     * Sets the value of the currencySwitch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCurrencySwitch(Boolean value) {
        this.currencySwitch = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Gets the value of the docDspMiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocDspMiscAmt() {
        return docDspMiscAmt;
    }

    /**
     * Sets the value of the docDspMiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocDspMiscAmt(BigDecimal value) {
        this.docDspMiscAmt = value;
    }

    /**
     * Gets the value of the docInMiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocInMiscAmt() {
        return docInMiscAmt;
    }

    /**
     * Sets the value of the docInMiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocInMiscAmt(BigDecimal value) {
        this.docInMiscAmt = value;
    }

    /**
     * Gets the value of the docMiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocMiscAmt() {
        return docMiscAmt;
    }

    /**
     * Sets the value of the docMiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocMiscAmt(BigDecimal value) {
        this.docMiscAmt = value;
    }

    /**
     * Gets the value of the dspMiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDspMiscAmt() {
        return dspMiscAmt;
    }

    /**
     * Sets the value of the dspMiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDspMiscAmt(BigDecimal value) {
        this.dspMiscAmt = value;
    }

    /**
     * Gets the value of the entryProcess property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEntryProcess() {
        return entryProcess;
    }

    /**
     * Sets the value of the entryProcess property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEntryProcess(JAXBElement<String> value) {
        this.entryProcess = value;
    }

    /**
     * Gets the value of the extCompany property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExtCompany() {
        return extCompany;
    }

    /**
     * Sets the value of the extCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExtCompany(JAXBElement<String> value) {
        this.extCompany = value;
    }

    /**
     * Gets the value of the freqCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFreqCode() {
        return freqCode;
    }

    /**
     * Sets the value of the freqCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFreqCode(JAXBElement<String> value) {
        this.freqCode = value;
    }

    /**
     * Gets the value of the icpoLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getICPOLine() {
        return icpoLine;
    }

    /**
     * Sets the value of the icpoLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setICPOLine(Integer value) {
        this.icpoLine = value;
    }

    /**
     * Gets the value of the icpoNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getICPONum() {
        return icpoNum;
    }

    /**
     * Sets the value of the icpoNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setICPONum(Integer value) {
        this.icpoNum = value;
    }

    /**
     * Gets the value of the icpoSeqNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getICPOSeqNum() {
        return icpoSeqNum;
    }

    /**
     * Sets the value of the icpoSeqNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setICPOSeqNum(Integer value) {
        this.icpoSeqNum = value;
    }

    /**
     * Gets the value of the inMiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInMiscAmt() {
        return inMiscAmt;
    }

    /**
     * Sets the value of the inMiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInMiscAmt(BigDecimal value) {
        this.inMiscAmt = value;
    }

    /**
     * Gets the value of the linked property.
     * This getter has been renamed from isLinked() to getLinked() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLinked() {
        return linked;
    }

    /**
     * Sets the value of the linked property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLinked(Boolean value) {
        this.linked = value;
    }

    /**
     * Gets the value of the miscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMiscAmt() {
        return miscAmt;
    }

    /**
     * Sets the value of the miscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMiscAmt(BigDecimal value) {
        this.miscAmt = value;
    }

    /**
     * Gets the value of the miscCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMiscCode() {
        return miscCode;
    }

    /**
     * Sets the value of the miscCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMiscCode(JAXBElement<String> value) {
        this.miscCode = value;
    }

    /**
     * Gets the value of the miscCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMiscCodeDescription() {
        return miscCodeDescription;
    }

    /**
     * Sets the value of the miscCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMiscCodeDescription(JAXBElement<String> value) {
        this.miscCodeDescription = value;
    }

    /**
     * Gets the value of the orderLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderLine() {
        return orderLine;
    }

    /**
     * Sets the value of the orderLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderLine(Integer value) {
        this.orderLine = value;
    }

    /**
     * Gets the value of the orderNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * Sets the value of the orderNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNum(Integer value) {
        this.orderNum = value;
    }

    /**
     * Gets the value of the orderNumCardMemberName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderNumCardMemberName() {
        return orderNumCardMemberName;
    }

    /**
     * Sets the value of the orderNumCardMemberName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderNumCardMemberName(JAXBElement<String> value) {
        this.orderNumCardMemberName = value;
    }

    /**
     * Gets the value of the orderNumCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderNumCurrencyCode() {
        return orderNumCurrencyCode;
    }

    /**
     * Sets the value of the orderNumCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderNumCurrencyCode(JAXBElement<String> value) {
        this.orderNumCurrencyCode = value;
    }

    /**
     * Gets the value of the percentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentage() {
        return percentage;
    }

    /**
     * Sets the value of the percentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentage(BigDecimal value) {
        this.percentage = value;
    }

    /**
     * Gets the value of the quoting property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getQuoting() {
        return quoting;
    }

    /**
     * Sets the value of the quoting property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setQuoting(JAXBElement<String> value) {
        this.quoting = value;
    }

    /**
     * Gets the value of the rpt1DspMiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1DspMiscAmt() {
        return rpt1DspMiscAmt;
    }

    /**
     * Sets the value of the rpt1DspMiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1DspMiscAmt(BigDecimal value) {
        this.rpt1DspMiscAmt = value;
    }

    /**
     * Gets the value of the rpt1InMiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1InMiscAmt() {
        return rpt1InMiscAmt;
    }

    /**
     * Sets the value of the rpt1InMiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1InMiscAmt(BigDecimal value) {
        this.rpt1InMiscAmt = value;
    }

    /**
     * Gets the value of the rpt1MiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1MiscAmt() {
        return rpt1MiscAmt;
    }

    /**
     * Sets the value of the rpt1MiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1MiscAmt(BigDecimal value) {
        this.rpt1MiscAmt = value;
    }

    /**
     * Gets the value of the rpt2DspMiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2DspMiscAmt() {
        return rpt2DspMiscAmt;
    }

    /**
     * Sets the value of the rpt2DspMiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2DspMiscAmt(BigDecimal value) {
        this.rpt2DspMiscAmt = value;
    }

    /**
     * Gets the value of the rpt2InMiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2InMiscAmt() {
        return rpt2InMiscAmt;
    }

    /**
     * Sets the value of the rpt2InMiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2InMiscAmt(BigDecimal value) {
        this.rpt2InMiscAmt = value;
    }

    /**
     * Gets the value of the rpt2MiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2MiscAmt() {
        return rpt2MiscAmt;
    }

    /**
     * Sets the value of the rpt2MiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2MiscAmt(BigDecimal value) {
        this.rpt2MiscAmt = value;
    }

    /**
     * Gets the value of the rpt3DspMiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3DspMiscAmt() {
        return rpt3DspMiscAmt;
    }

    /**
     * Sets the value of the rpt3DspMiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3DspMiscAmt(BigDecimal value) {
        this.rpt3DspMiscAmt = value;
    }

    /**
     * Gets the value of the rpt3InMiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3InMiscAmt() {
        return rpt3InMiscAmt;
    }

    /**
     * Sets the value of the rpt3InMiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3InMiscAmt(BigDecimal value) {
        this.rpt3InMiscAmt = value;
    }

    /**
     * Gets the value of the rpt3MiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3MiscAmt() {
        return rpt3MiscAmt;
    }

    /**
     * Sets the value of the rpt3MiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3MiscAmt(BigDecimal value) {
        this.rpt3MiscAmt = value;
    }

    /**
     * Gets the value of the seqNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSeqNum() {
        return seqNum;
    }

    /**
     * Sets the value of the seqNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSeqNum(Integer value) {
        this.seqNum = value;
    }

    /**
     * Gets the value of the sysRevID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSysRevID() {
        return sysRevID;
    }

    /**
     * Sets the value of the sysRevID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSysRevID(Long value) {
        this.sysRevID = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setType(JAXBElement<String> value) {
        this.type = value;
    }

}
