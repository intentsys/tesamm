
package com.epicor.erp.salesorder;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.5.1
 * 2016-01-18T18:16:30.545+08:00
 * Generated source version: 2.5.1
 */

@WebFault(name = "EpicorFaultDetail", targetNamespace = "http://schemas.datacontract.org/2004/07/Ice.Common")
public class SalesOrderSvcContractChangeSellingQtyMasterEpicorFaultDetailFaultFaultMessage extends Exception {
    
    private com.epicor.erp.salesorder.EpicorFaultDetail epicorFaultDetail;

    public SalesOrderSvcContractChangeSellingQtyMasterEpicorFaultDetailFaultFaultMessage() {
        super();
    }
    
    public SalesOrderSvcContractChangeSellingQtyMasterEpicorFaultDetailFaultFaultMessage(String message) {
        super(message);
    }
    
    public SalesOrderSvcContractChangeSellingQtyMasterEpicorFaultDetailFaultFaultMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public SalesOrderSvcContractChangeSellingQtyMasterEpicorFaultDetailFaultFaultMessage(String message, com.epicor.erp.salesorder.EpicorFaultDetail epicorFaultDetail) {
        super(message);
        this.epicorFaultDetail = epicorFaultDetail;
    }

    public SalesOrderSvcContractChangeSellingQtyMasterEpicorFaultDetailFaultFaultMessage(String message, com.epicor.erp.salesorder.EpicorFaultDetail epicorFaultDetail, Throwable cause) {
        super(message, cause);
        this.epicorFaultDetail = epicorFaultDetail;
    }

    public com.epicor.erp.salesorder.EpicorFaultDetail getFaultInfo() {
        return this.epicorFaultDetail;
    }
}
