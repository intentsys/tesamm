
package com.epicor.erp.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SNFormatTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SNFormatTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SNFormatRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SNFormatRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SNFormatTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "snFormatRow"
})
public class SNFormatTable {

    @XmlElement(name = "SNFormatRow", nillable = true)
    protected List<SNFormatRow> snFormatRow;

    /**
     * Gets the value of the snFormatRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the snFormatRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSNFormatRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SNFormatRow }
     * 
     * 
     */
    public List<SNFormatRow> getSNFormatRow() {
        if (snFormatRow == null) {
            snFormatRow = new ArrayList<SNFormatRow>();
        }
        return this.snFormatRow;
    }

    /**
     * Sets the value of the snFormatRow property.
     * 
     * @param snFormatRow
     *     allowed object is
     *     {@link SNFormatRow }
     *     
     */
    public void setSNFormatRow(List<SNFormatRow> snFormatRow) {
        this.snFormatRow = snFormatRow;
    }

}
