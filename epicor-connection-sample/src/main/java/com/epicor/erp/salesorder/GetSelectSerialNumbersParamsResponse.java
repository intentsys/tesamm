
package com.epicor.erp.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSelectSerialNumbersParamsResult" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SelectSerialNumbersParamsTableset" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSelectSerialNumbersParamsResult"
})
@XmlRootElement(name = "GetSelectSerialNumbersParamsResponse")
public class GetSelectSerialNumbersParamsResponse {

    @XmlElementRef(name = "GetSelectSerialNumbersParamsResult", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<SelectSerialNumbersParamsTableset> getSelectSerialNumbersParamsResult;

    /**
     * Gets the value of the getSelectSerialNumbersParamsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SelectSerialNumbersParamsTableset }{@code >}
     *     
     */
    public JAXBElement<SelectSerialNumbersParamsTableset> getGetSelectSerialNumbersParamsResult() {
        return getSelectSerialNumbersParamsResult;
    }

    /**
     * Sets the value of the getSelectSerialNumbersParamsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SelectSerialNumbersParamsTableset }{@code >}
     *     
     */
    public void setGetSelectSerialNumbersParamsResult(JAXBElement<SelectSerialNumbersParamsTableset> value) {
        this.getSelectSerialNumbersParamsResult = value;
    }

}
