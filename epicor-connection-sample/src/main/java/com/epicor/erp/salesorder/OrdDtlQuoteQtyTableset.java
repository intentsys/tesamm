
package com.epicor.erp.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrdDtlQuoteQtyTableset complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrdDtlQuoteQtyTableset">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceTableset">
 *       &lt;sequence>
 *         &lt;element name="QuoteQty" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}QuoteQtyTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrdDtlQuoteQtyTableset", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "quoteQty"
})
public class OrdDtlQuoteQtyTableset
    extends IceTableset
{

    @XmlElementRef(name = "QuoteQty", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<QuoteQtyTable> quoteQty;

    /**
     * Gets the value of the quoteQty property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QuoteQtyTable }{@code >}
     *     
     */
    public JAXBElement<QuoteQtyTable> getQuoteQty() {
        return quoteQty;
    }

    /**
     * Sets the value of the quoteQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QuoteQtyTable }{@code >}
     *     
     */
    public void setQuoteQty(JAXBElement<QuoteQtyTable> value) {
        this.quoteQty = value;
    }

}
