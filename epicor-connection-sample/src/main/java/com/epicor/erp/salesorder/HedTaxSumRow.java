
package com.epicor.erp.salesorder;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HedTaxSumRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HedTaxSumRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceRow">
 *       &lt;sequence>
 *         &lt;element name="AllocDepInvcNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencySwitch" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DisplaySymbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocDisplaySymbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocReportableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="GroupID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HedNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Percent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RateCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReportableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1ReportableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2ReportableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3ReportableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HedTaxSumRow", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "allocDepInvcNum",
    "company",
    "currencyCode",
    "currencySwitch",
    "displaySymbol",
    "docDisplaySymbol",
    "docReportableAmt",
    "docTaxAmt",
    "docTaxableAmt",
    "groupID",
    "hedNum",
    "percent",
    "rateCode",
    "reportableAmt",
    "rpt1ReportableAmt",
    "rpt1TaxAmt",
    "rpt1TaxableAmt",
    "rpt2ReportableAmt",
    "rpt2TaxAmt",
    "rpt2TaxableAmt",
    "rpt3ReportableAmt",
    "rpt3TaxAmt",
    "rpt3TaxableAmt",
    "taxAmt",
    "taxCode",
    "taxDescription",
    "taxableAmt"
})
public class HedTaxSumRow
    extends IceRow
{

    @XmlElement(name = "AllocDepInvcNum")
    protected Integer allocDepInvcNum;
    @XmlElementRef(name = "Company", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> company;
    @XmlElementRef(name = "CurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElement(name = "CurrencySwitch")
    protected Boolean currencySwitch;
    @XmlElementRef(name = "DisplaySymbol", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> displaySymbol;
    @XmlElementRef(name = "DocDisplaySymbol", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> docDisplaySymbol;
    @XmlElement(name = "DocReportableAmt")
    protected BigDecimal docReportableAmt;
    @XmlElement(name = "DocTaxAmt")
    protected BigDecimal docTaxAmt;
    @XmlElement(name = "DocTaxableAmt")
    protected BigDecimal docTaxableAmt;
    @XmlElementRef(name = "GroupID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> groupID;
    @XmlElement(name = "HedNum")
    protected Integer hedNum;
    @XmlElement(name = "Percent")
    protected BigDecimal percent;
    @XmlElementRef(name = "RateCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rateCode;
    @XmlElement(name = "ReportableAmt")
    protected BigDecimal reportableAmt;
    @XmlElement(name = "Rpt1ReportableAmt")
    protected BigDecimal rpt1ReportableAmt;
    @XmlElement(name = "Rpt1TaxAmt")
    protected BigDecimal rpt1TaxAmt;
    @XmlElement(name = "Rpt1TaxableAmt")
    protected BigDecimal rpt1TaxableAmt;
    @XmlElement(name = "Rpt2ReportableAmt")
    protected BigDecimal rpt2ReportableAmt;
    @XmlElement(name = "Rpt2TaxAmt")
    protected BigDecimal rpt2TaxAmt;
    @XmlElement(name = "Rpt2TaxableAmt")
    protected BigDecimal rpt2TaxableAmt;
    @XmlElement(name = "Rpt3ReportableAmt")
    protected BigDecimal rpt3ReportableAmt;
    @XmlElement(name = "Rpt3TaxAmt")
    protected BigDecimal rpt3TaxAmt;
    @XmlElement(name = "Rpt3TaxableAmt")
    protected BigDecimal rpt3TaxableAmt;
    @XmlElement(name = "TaxAmt")
    protected BigDecimal taxAmt;
    @XmlElementRef(name = "TaxCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxCode;
    @XmlElementRef(name = "TaxDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxDescription;
    @XmlElement(name = "TaxableAmt")
    protected BigDecimal taxableAmt;

    /**
     * Gets the value of the allocDepInvcNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAllocDepInvcNum() {
        return allocDepInvcNum;
    }

    /**
     * Sets the value of the allocDepInvcNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAllocDepInvcNum(Integer value) {
        this.allocDepInvcNum = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompany(JAXBElement<String> value) {
        this.company = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the currencySwitch property.
     * This getter has been renamed from isCurrencySwitch() to getCurrencySwitch() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCurrencySwitch() {
        return currencySwitch;
    }

    /**
     * Sets the value of the currencySwitch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCurrencySwitch(Boolean value) {
        this.currencySwitch = value;
    }

    /**
     * Gets the value of the displaySymbol property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDisplaySymbol() {
        return displaySymbol;
    }

    /**
     * Sets the value of the displaySymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDisplaySymbol(JAXBElement<String> value) {
        this.displaySymbol = value;
    }

    /**
     * Gets the value of the docDisplaySymbol property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDocDisplaySymbol() {
        return docDisplaySymbol;
    }

    /**
     * Sets the value of the docDisplaySymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDocDisplaySymbol(JAXBElement<String> value) {
        this.docDisplaySymbol = value;
    }

    /**
     * Gets the value of the docReportableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocReportableAmt() {
        return docReportableAmt;
    }

    /**
     * Sets the value of the docReportableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocReportableAmt(BigDecimal value) {
        this.docReportableAmt = value;
    }

    /**
     * Gets the value of the docTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTaxAmt() {
        return docTaxAmt;
    }

    /**
     * Sets the value of the docTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTaxAmt(BigDecimal value) {
        this.docTaxAmt = value;
    }

    /**
     * Gets the value of the docTaxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTaxableAmt() {
        return docTaxableAmt;
    }

    /**
     * Sets the value of the docTaxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTaxableAmt(BigDecimal value) {
        this.docTaxableAmt = value;
    }

    /**
     * Gets the value of the groupID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGroupID() {
        return groupID;
    }

    /**
     * Sets the value of the groupID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGroupID(JAXBElement<String> value) {
        this.groupID = value;
    }

    /**
     * Gets the value of the hedNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHedNum() {
        return hedNum;
    }

    /**
     * Sets the value of the hedNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHedNum(Integer value) {
        this.hedNum = value;
    }

    /**
     * Gets the value of the percent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercent() {
        return percent;
    }

    /**
     * Sets the value of the percent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercent(BigDecimal value) {
        this.percent = value;
    }

    /**
     * Gets the value of the rateCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRateCode() {
        return rateCode;
    }

    /**
     * Sets the value of the rateCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRateCode(JAXBElement<String> value) {
        this.rateCode = value;
    }

    /**
     * Gets the value of the reportableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReportableAmt() {
        return reportableAmt;
    }

    /**
     * Sets the value of the reportableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReportableAmt(BigDecimal value) {
        this.reportableAmt = value;
    }

    /**
     * Gets the value of the rpt1ReportableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1ReportableAmt() {
        return rpt1ReportableAmt;
    }

    /**
     * Sets the value of the rpt1ReportableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1ReportableAmt(BigDecimal value) {
        this.rpt1ReportableAmt = value;
    }

    /**
     * Gets the value of the rpt1TaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TaxAmt() {
        return rpt1TaxAmt;
    }

    /**
     * Sets the value of the rpt1TaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TaxAmt(BigDecimal value) {
        this.rpt1TaxAmt = value;
    }

    /**
     * Gets the value of the rpt1TaxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TaxableAmt() {
        return rpt1TaxableAmt;
    }

    /**
     * Sets the value of the rpt1TaxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TaxableAmt(BigDecimal value) {
        this.rpt1TaxableAmt = value;
    }

    /**
     * Gets the value of the rpt2ReportableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2ReportableAmt() {
        return rpt2ReportableAmt;
    }

    /**
     * Sets the value of the rpt2ReportableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2ReportableAmt(BigDecimal value) {
        this.rpt2ReportableAmt = value;
    }

    /**
     * Gets the value of the rpt2TaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TaxAmt() {
        return rpt2TaxAmt;
    }

    /**
     * Sets the value of the rpt2TaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TaxAmt(BigDecimal value) {
        this.rpt2TaxAmt = value;
    }

    /**
     * Gets the value of the rpt2TaxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TaxableAmt() {
        return rpt2TaxableAmt;
    }

    /**
     * Sets the value of the rpt2TaxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TaxableAmt(BigDecimal value) {
        this.rpt2TaxableAmt = value;
    }

    /**
     * Gets the value of the rpt3ReportableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3ReportableAmt() {
        return rpt3ReportableAmt;
    }

    /**
     * Sets the value of the rpt3ReportableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3ReportableAmt(BigDecimal value) {
        this.rpt3ReportableAmt = value;
    }

    /**
     * Gets the value of the rpt3TaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TaxAmt() {
        return rpt3TaxAmt;
    }

    /**
     * Sets the value of the rpt3TaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TaxAmt(BigDecimal value) {
        this.rpt3TaxAmt = value;
    }

    /**
     * Gets the value of the rpt3TaxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TaxableAmt() {
        return rpt3TaxableAmt;
    }

    /**
     * Sets the value of the rpt3TaxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TaxableAmt(BigDecimal value) {
        this.rpt3TaxableAmt = value;
    }

    /**
     * Gets the value of the taxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTaxAmt() {
        return taxAmt;
    }

    /**
     * Sets the value of the taxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxAmt(BigDecimal value) {
        this.taxAmt = value;
    }

    /**
     * Gets the value of the taxCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxCode() {
        return taxCode;
    }

    /**
     * Sets the value of the taxCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxCode(JAXBElement<String> value) {
        this.taxCode = value;
    }

    /**
     * Gets the value of the taxDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxDescription() {
        return taxDescription;
    }

    /**
     * Sets the value of the taxDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxDescription(JAXBElement<String> value) {
        this.taxDescription = value;
    }

    /**
     * Gets the value of the taxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTaxableAmt() {
        return taxableAmt;
    }

    /**
     * Sets the value of the taxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxableAmt(BigDecimal value) {
        this.taxableAmt = value;
    }

}
