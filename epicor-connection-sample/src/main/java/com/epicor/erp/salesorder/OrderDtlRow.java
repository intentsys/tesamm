
package com.epicor.erp.salesorder;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OrderDtlRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderDtlRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceRow">
 *       &lt;sequence>
 *         &lt;element name="AdvanceBillBal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AvailPriceLists" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AvailUMFromQuote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AvailableQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="BaseCurrSymbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BaseCurrencyID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BasePartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BaseRevisionNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BinNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BitFlag" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BreakListCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ChangeTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ChangedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Commissionable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConfigBaseUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ConfigUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Configured" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContractCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContractCodeContractDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContractNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CounterSale" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CounterSaleBinNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CounterSaleDimCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CounterSaleLotNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CounterSaleWarehouse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreateNewJob" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CreditLimitMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditLimitSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CumeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CumeQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CurrSymbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencySwitch" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CustNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CustNumBTName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustNumCustID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustNumName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefaultOversPricing" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DemandContractLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DemandContractNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DemandDtlRejected" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DemandDtlSeq" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DemandHeadSeq" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DemandQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DimCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DimConvFactor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DiscBreakListCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DiscBreakListCodeEndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DiscBreakListCodeListDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DiscBreakListCodeStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DiscListPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DiscountPercent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DisplaySeq" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DoNotShipAfterDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DoNotShipBeforeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DocAdvanceBillBal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocDspDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocDspUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocExtPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocExtPriceDtl" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocInDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocInExtPriceDtl" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocInListPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocInMiscCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocInOrdBasedPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocInUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocLessDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocListPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocMiscCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocOrdBasedPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTotalPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DspDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DspJobType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DspUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DupOnJobCrt" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ECCDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ECCOrderLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ECCOrderNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ECCPlant" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ECCPreventRepricing" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ECCQuoteLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ECCQuoteNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EnableCreateNewJob" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EnableGetDtls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EnableKitUnitPrice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EnableRelJob" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EnableRenewalNbr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EnableSchedJob" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EnableSellingQty" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EntryProcess" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExtCompany" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExtPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ExtPriceDtl" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="FromQuoteLineFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="GetDtls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="GroupSeq" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="HasComplement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="HasDowngrade" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="HasSubstitute" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="HasUpgrade" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ICPOLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ICPONum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InExtPriceDtl" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InListPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InMiscCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InOrdBasedPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InPrice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="InUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InvoiceComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvtyUOM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobTypeMFG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobWasCreated" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="KitAllowUpdate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="KitBackFlush" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="KitChangeParms" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="KitCompOrigPart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="KitCompOrigSeq" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="KitDisable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="KitFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="KitFlagDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="KitOrderQtyUOM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="KitParentLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="KitPricing" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="KitPrintCompsInv" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="KitPrintCompsPS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="KitQtyPer" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="KitShipComplete" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="KitStandard" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="KitsLoaded" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LabCovered" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LaborDuration" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LaborMod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastConfigDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="LastConfigTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LastConfigUserID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LessDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="LineDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Linked" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ListPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="LockDisc" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LockPrice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LockQty" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LotNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MOMsourceEst" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MOMsourceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MatCovered" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MaterialDuration" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MaterialMod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MiscCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MiscCovered" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MiscDuration" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MktgCampaignID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MktgCampaignIDCampDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MktgEvntEvntDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MktgEvntSeq" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MultipleReleases" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="NeedByDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="OldOpenValue" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OldOurOpenQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OldProdCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OldSellingOpenQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OnHandQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Onsite" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OpenLine" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OrdBasedPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OrderComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OrderNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OrderNumBTCustNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OrderNumCardMemberName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderNumCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OrigWhyNoTax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OverrideDiscPriceList" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OverridePriceList" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Overs" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OversUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="POLine" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="POLineRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartExists" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumIUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumPartDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumPricePerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumSalesUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumSellingFactor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PartNumTrackDimension" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartNumTrackLots" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartNumTrackSerialNum" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartTrackDimension" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartTrackLots" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PickListComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanGUID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanUserID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrevPartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrevSellQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PrevXPartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PriceGroupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PriceListCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PriceListCodeDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PricePerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PricingQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PricingValue" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ProFormaInvComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessCounterSale" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ProcessQuickEntry" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ProdCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProdCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProjectID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProjectIDDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QuoteLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QuoteNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QuoteNumCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QuoteQtyNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RMALine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RMANum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RelJob" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="RelWasRecInvoiced" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="RenewalNbr" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RepRate1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RepRate2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RepRate3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RepRate4" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RepRate5" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RepSplit1" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RepSplit2" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RepSplit3" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RepSplit4" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RepSplit5" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RequestDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="RespMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReverseCharge" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="RevisionNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Rework" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Rpt1AdvanceBillBal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1Discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1DspDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1DspUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1ExtPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1ExtPriceDtl" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1InDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1InExtPriceDtl" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1InListPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1InMiscCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1InOrdBasedPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1InUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1LessDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1ListPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1MiscCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1OrdBasedPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TotalPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2AdvanceBillBal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2Discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2DspDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2DspUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2ExtPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2ExtPriceDtl" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2InDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2InExtPriceDtl" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2InListPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2InMiscCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2InOrdBasedPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2InUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2LessDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2ListPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2MiscCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2OrdBasedPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TotalPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3AdvanceBillBal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3Discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3DspDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3DspUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3ExtPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3ExtPriceDtl" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3InDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3InExtPriceDtl" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3InListPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3InMiscCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3InOrdBasedPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3InUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3LessDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3ListPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3MiscCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3OrdBasedPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TotalPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SalesCatID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesCatIDDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepName1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepName2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepName3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepName4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepName5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SchedJob" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SellingFactor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SellingFactorDirection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SellingQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ShipComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipLineComplete" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SmartString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmartStringProcessed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SysRevID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="TMBilling" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TaxCatID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxCatIDDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ThisOrderInvtyQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalReleases" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TotalShipped" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Unders" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="UndersPct" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="VoidLine" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="WarehouseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WarehouseDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Warranty" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="WarrantyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WarrantyCodeWarrDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WarrantyComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="XPartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="XRevisionNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderDtlRow", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "advanceBillBal",
    "availPriceLists",
    "availUMFromQuote",
    "availableQuantity",
    "baseCurrSymbol",
    "baseCurrencyID",
    "basePartNum",
    "baseRevisionNum",
    "binNum",
    "bitFlag",
    "breakListCode",
    "changeDate",
    "changeTime",
    "changedBy",
    "commissionable",
    "company",
    "configBaseUnitPrice",
    "configUnitPrice",
    "configured",
    "contractCode",
    "contractCodeContractDescription",
    "contractNum",
    "counterSale",
    "counterSaleBinNum",
    "counterSaleDimCode",
    "counterSaleLotNum",
    "counterSaleWarehouse",
    "createNewJob",
    "creditLimitMessage",
    "creditLimitSource",
    "cumeDate",
    "cumeQty",
    "currSymbol",
    "currencyCode",
    "currencyID",
    "currencySwitch",
    "custNum",
    "custNumBTName",
    "custNumCustID",
    "custNumName",
    "dum",
    "defaultOversPricing",
    "demandContractLine",
    "demandContractNum",
    "demandDtlRejected",
    "demandDtlSeq",
    "demandHeadSeq",
    "demandQuantity",
    "dimCode",
    "dimConvFactor",
    "discBreakListCode",
    "discBreakListCodeEndDate",
    "discBreakListCodeListDescription",
    "discBreakListCodeStartDate",
    "discListPrice",
    "discount",
    "discountPercent",
    "displaySeq",
    "doNotShipAfterDate",
    "doNotShipBeforeDate",
    "docAdvanceBillBal",
    "docDiscount",
    "docDspDiscount",
    "docDspUnitPrice",
    "docExtPrice",
    "docExtPriceDtl",
    "docInDiscount",
    "docInExtPriceDtl",
    "docInListPrice",
    "docInMiscCharges",
    "docInOrdBasedPrice",
    "docInUnitPrice",
    "docLessDiscount",
    "docListPrice",
    "docMiscCharges",
    "docOrdBasedPrice",
    "docTaxAmt",
    "docTotalPrice",
    "docUnitPrice",
    "dspDiscount",
    "dspJobType",
    "dspUnitPrice",
    "dupOnJobCrt",
    "eccDiscount",
    "eccOrderLine",
    "eccOrderNum",
    "eccPlant",
    "eccPreventRepricing",
    "eccQuoteLine",
    "eccQuoteNum",
    "enableCreateNewJob",
    "enableGetDtls",
    "enableKitUnitPrice",
    "enableRelJob",
    "enableRenewalNbr",
    "enableSchedJob",
    "enableSellingQty",
    "entryProcess",
    "extCompany",
    "extPrice",
    "extPriceDtl",
    "fromQuoteLineFlag",
    "getDtls",
    "groupSeq",
    "hasComplement",
    "hasDowngrade",
    "hasSubstitute",
    "hasUpgrade",
    "icpoLine",
    "icpoNum",
    "ium",
    "inDiscount",
    "inExtPriceDtl",
    "inListPrice",
    "inMiscCharges",
    "inOrdBasedPrice",
    "inPrice",
    "inUnitPrice",
    "invoiceComment",
    "invtyUOM",
    "jobTypeMFG",
    "jobWasCreated",
    "kitAllowUpdate",
    "kitBackFlush",
    "kitChangeParms",
    "kitCompOrigPart",
    "kitCompOrigSeq",
    "kitDisable",
    "kitFlag",
    "kitFlagDescription",
    "kitOrderQtyUOM",
    "kitParentLine",
    "kitPricing",
    "kitPrintCompsInv",
    "kitPrintCompsPS",
    "kitQtyPer",
    "kitShipComplete",
    "kitStandard",
    "kitsLoaded",
    "labCovered",
    "laborDuration",
    "laborMod",
    "lastConfigDate",
    "lastConfigTime",
    "lastConfigUserID",
    "lessDiscount",
    "lineDesc",
    "lineStatus",
    "lineType",
    "linked",
    "listPrice",
    "lockDisc",
    "lockPrice",
    "lockQty",
    "lotNum",
    "moMsourceEst",
    "moMsourceType",
    "matCovered",
    "materialDuration",
    "materialMod",
    "miscCharges",
    "miscCovered",
    "miscDuration",
    "mktgCampaignID",
    "mktgCampaignIDCampDescription",
    "mktgEvntEvntDescription",
    "mktgEvntSeq",
    "multipleReleases",
    "needByDate",
    "oldOpenValue",
    "oldOurOpenQty",
    "oldProdCode",
    "oldSellingOpenQty",
    "onHandQuantity",
    "onsite",
    "openLine",
    "ordBasedPrice",
    "orderComment",
    "orderLine",
    "orderNum",
    "orderNumBTCustNum",
    "orderNumCardMemberName",
    "orderNumCurrencyCode",
    "orderQty",
    "origWhyNoTax",
    "overrideDiscPriceList",
    "overridePriceList",
    "overs",
    "oversUnitPrice",
    "poLine",
    "poLineRef",
    "partExists",
    "partNum",
    "partNumIUM",
    "partNumPartDescription",
    "partNumPricePerCode",
    "partNumSalesUM",
    "partNumSellingFactor",
    "partNumTrackDimension",
    "partNumTrackLots",
    "partNumTrackSerialNum",
    "partTrackDimension",
    "partTrackLots",
    "pickListComment",
    "planGUID",
    "planUserID",
    "prevPartNum",
    "prevSellQty",
    "prevXPartNum",
    "priceGroupCode",
    "priceListCode",
    "priceListCodeDesc",
    "pricePerCode",
    "pricingQty",
    "pricingValue",
    "proFormaInvComment",
    "processCounterSale",
    "processQuickEntry",
    "prodCode",
    "prodCodeDescription",
    "projectID",
    "projectIDDescription",
    "quoteLine",
    "quoteNum",
    "quoteNumCurrencyCode",
    "quoteQtyNum",
    "rmaLine",
    "rmaNum",
    "reference",
    "relJob",
    "relWasRecInvoiced",
    "renewalNbr",
    "repRate1",
    "repRate2",
    "repRate3",
    "repRate4",
    "repRate5",
    "repSplit1",
    "repSplit2",
    "repSplit3",
    "repSplit4",
    "repSplit5",
    "requestDate",
    "respMessage",
    "reverseCharge",
    "revisionNum",
    "rework",
    "rpt1AdvanceBillBal",
    "rpt1Discount",
    "rpt1DspDiscount",
    "rpt1DspUnitPrice",
    "rpt1ExtPrice",
    "rpt1ExtPriceDtl",
    "rpt1InDiscount",
    "rpt1InExtPriceDtl",
    "rpt1InListPrice",
    "rpt1InMiscCharges",
    "rpt1InOrdBasedPrice",
    "rpt1InUnitPrice",
    "rpt1LessDiscount",
    "rpt1ListPrice",
    "rpt1MiscCharges",
    "rpt1OrdBasedPrice",
    "rpt1TaxAmt",
    "rpt1TotalPrice",
    "rpt1UnitPrice",
    "rpt2AdvanceBillBal",
    "rpt2Discount",
    "rpt2DspDiscount",
    "rpt2DspUnitPrice",
    "rpt2ExtPrice",
    "rpt2ExtPriceDtl",
    "rpt2InDiscount",
    "rpt2InExtPriceDtl",
    "rpt2InListPrice",
    "rpt2InMiscCharges",
    "rpt2InOrdBasedPrice",
    "rpt2InUnitPrice",
    "rpt2LessDiscount",
    "rpt2ListPrice",
    "rpt2MiscCharges",
    "rpt2OrdBasedPrice",
    "rpt2TaxAmt",
    "rpt2TotalPrice",
    "rpt2UnitPrice",
    "rpt3AdvanceBillBal",
    "rpt3Discount",
    "rpt3DspDiscount",
    "rpt3DspUnitPrice",
    "rpt3ExtPrice",
    "rpt3ExtPriceDtl",
    "rpt3InDiscount",
    "rpt3InExtPriceDtl",
    "rpt3InListPrice",
    "rpt3InMiscCharges",
    "rpt3InOrdBasedPrice",
    "rpt3InUnitPrice",
    "rpt3LessDiscount",
    "rpt3ListPrice",
    "rpt3MiscCharges",
    "rpt3OrdBasedPrice",
    "rpt3TaxAmt",
    "rpt3TotalPrice",
    "rpt3UnitPrice",
    "salesCatID",
    "salesCatIDDescription",
    "salesRepName1",
    "salesRepName2",
    "salesRepName3",
    "salesRepName4",
    "salesRepName5",
    "salesUM",
    "schedJob",
    "sellingFactor",
    "sellingFactorDirection",
    "sellingQuantity",
    "shipComment",
    "shipLineComplete",
    "smartString",
    "smartStringProcessed",
    "sysRevID",
    "tmBilling",
    "taxAmt",
    "taxCatID",
    "taxCatIDDescription",
    "thisOrderInvtyQty",
    "totalPrice",
    "totalReleases",
    "totalShipped",
    "unders",
    "undersPct",
    "unitPrice",
    "voidLine",
    "warehouseCode",
    "warehouseDesc",
    "warranty",
    "warrantyCode",
    "warrantyCodeWarrDescription",
    "warrantyComment",
    "xPartNum",
    "xRevisionNum"
})
public class OrderDtlRow
    extends IceRow
{

    @XmlElement(name = "AdvanceBillBal")
    protected BigDecimal advanceBillBal;
    @XmlElementRef(name = "AvailPriceLists", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> availPriceLists;
    @XmlElementRef(name = "AvailUMFromQuote", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> availUMFromQuote;
    @XmlElement(name = "AvailableQuantity")
    protected BigDecimal availableQuantity;
    @XmlElementRef(name = "BaseCurrSymbol", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> baseCurrSymbol;
    @XmlElementRef(name = "BaseCurrencyID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> baseCurrencyID;
    @XmlElementRef(name = "BasePartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> basePartNum;
    @XmlElementRef(name = "BaseRevisionNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> baseRevisionNum;
    @XmlElementRef(name = "BinNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> binNum;
    @XmlElement(name = "BitFlag")
    protected Integer bitFlag;
    @XmlElementRef(name = "BreakListCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> breakListCode;
    @XmlElementRef(name = "ChangeDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> changeDate;
    @XmlElement(name = "ChangeTime")
    protected Integer changeTime;
    @XmlElementRef(name = "ChangedBy", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changedBy;
    @XmlElement(name = "Commissionable")
    protected Boolean commissionable;
    @XmlElementRef(name = "Company", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> company;
    @XmlElement(name = "ConfigBaseUnitPrice")
    protected BigDecimal configBaseUnitPrice;
    @XmlElement(name = "ConfigUnitPrice")
    protected BigDecimal configUnitPrice;
    @XmlElementRef(name = "Configured", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> configured;
    @XmlElementRef(name = "ContractCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> contractCode;
    @XmlElementRef(name = "ContractCodeContractDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> contractCodeContractDescription;
    @XmlElement(name = "ContractNum")
    protected Integer contractNum;
    @XmlElement(name = "CounterSale")
    protected Boolean counterSale;
    @XmlElementRef(name = "CounterSaleBinNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> counterSaleBinNum;
    @XmlElementRef(name = "CounterSaleDimCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> counterSaleDimCode;
    @XmlElementRef(name = "CounterSaleLotNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> counterSaleLotNum;
    @XmlElementRef(name = "CounterSaleWarehouse", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> counterSaleWarehouse;
    @XmlElement(name = "CreateNewJob")
    protected Boolean createNewJob;
    @XmlElementRef(name = "CreditLimitMessage", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditLimitMessage;
    @XmlElementRef(name = "CreditLimitSource", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditLimitSource;
    @XmlElementRef(name = "CumeDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> cumeDate;
    @XmlElement(name = "CumeQty")
    protected BigDecimal cumeQty;
    @XmlElementRef(name = "CurrSymbol", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currSymbol;
    @XmlElementRef(name = "CurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElementRef(name = "CurrencyID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyID;
    @XmlElement(name = "CurrencySwitch")
    protected Boolean currencySwitch;
    @XmlElement(name = "CustNum")
    protected Integer custNum;
    @XmlElementRef(name = "CustNumBTName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> custNumBTName;
    @XmlElementRef(name = "CustNumCustID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> custNumCustID;
    @XmlElementRef(name = "CustNumName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> custNumName;
    @XmlElementRef(name = "DUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dum;
    @XmlElementRef(name = "DefaultOversPricing", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultOversPricing;
    @XmlElement(name = "DemandContractLine")
    protected Integer demandContractLine;
    @XmlElement(name = "DemandContractNum")
    protected Integer demandContractNum;
    @XmlElement(name = "DemandDtlRejected")
    protected Boolean demandDtlRejected;
    @XmlElement(name = "DemandDtlSeq")
    protected Integer demandDtlSeq;
    @XmlElement(name = "DemandHeadSeq")
    protected Integer demandHeadSeq;
    @XmlElement(name = "DemandQuantity")
    protected BigDecimal demandQuantity;
    @XmlElementRef(name = "DimCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dimCode;
    @XmlElement(name = "DimConvFactor")
    protected BigDecimal dimConvFactor;
    @XmlElementRef(name = "DiscBreakListCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> discBreakListCode;
    @XmlElementRef(name = "DiscBreakListCodeEndDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> discBreakListCodeEndDate;
    @XmlElementRef(name = "DiscBreakListCodeListDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> discBreakListCodeListDescription;
    @XmlElementRef(name = "DiscBreakListCodeStartDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> discBreakListCodeStartDate;
    @XmlElement(name = "DiscListPrice")
    protected BigDecimal discListPrice;
    @XmlElement(name = "Discount")
    protected BigDecimal discount;
    @XmlElement(name = "DiscountPercent")
    protected BigDecimal discountPercent;
    @XmlElement(name = "DisplaySeq")
    protected BigDecimal displaySeq;
    @XmlElementRef(name = "DoNotShipAfterDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> doNotShipAfterDate;
    @XmlElementRef(name = "DoNotShipBeforeDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> doNotShipBeforeDate;
    @XmlElement(name = "DocAdvanceBillBal")
    protected BigDecimal docAdvanceBillBal;
    @XmlElement(name = "DocDiscount")
    protected BigDecimal docDiscount;
    @XmlElement(name = "DocDspDiscount")
    protected BigDecimal docDspDiscount;
    @XmlElement(name = "DocDspUnitPrice")
    protected BigDecimal docDspUnitPrice;
    @XmlElement(name = "DocExtPrice")
    protected BigDecimal docExtPrice;
    @XmlElement(name = "DocExtPriceDtl")
    protected BigDecimal docExtPriceDtl;
    @XmlElement(name = "DocInDiscount")
    protected BigDecimal docInDiscount;
    @XmlElement(name = "DocInExtPriceDtl")
    protected BigDecimal docInExtPriceDtl;
    @XmlElement(name = "DocInListPrice")
    protected BigDecimal docInListPrice;
    @XmlElement(name = "DocInMiscCharges")
    protected BigDecimal docInMiscCharges;
    @XmlElement(name = "DocInOrdBasedPrice")
    protected BigDecimal docInOrdBasedPrice;
    @XmlElement(name = "DocInUnitPrice")
    protected BigDecimal docInUnitPrice;
    @XmlElement(name = "DocLessDiscount")
    protected BigDecimal docLessDiscount;
    @XmlElement(name = "DocListPrice")
    protected BigDecimal docListPrice;
    @XmlElement(name = "DocMiscCharges")
    protected BigDecimal docMiscCharges;
    @XmlElement(name = "DocOrdBasedPrice")
    protected BigDecimal docOrdBasedPrice;
    @XmlElement(name = "DocTaxAmt")
    protected BigDecimal docTaxAmt;
    @XmlElement(name = "DocTotalPrice")
    protected BigDecimal docTotalPrice;
    @XmlElement(name = "DocUnitPrice")
    protected BigDecimal docUnitPrice;
    @XmlElement(name = "DspDiscount")
    protected BigDecimal dspDiscount;
    @XmlElementRef(name = "DspJobType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dspJobType;
    @XmlElement(name = "DspUnitPrice")
    protected BigDecimal dspUnitPrice;
    @XmlElement(name = "DupOnJobCrt")
    protected Boolean dupOnJobCrt;
    @XmlElement(name = "ECCDiscount")
    protected BigDecimal eccDiscount;
    @XmlElement(name = "ECCOrderLine")
    protected Integer eccOrderLine;
    @XmlElementRef(name = "ECCOrderNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eccOrderNum;
    @XmlElementRef(name = "ECCPlant", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eccPlant;
    @XmlElement(name = "ECCPreventRepricing")
    protected Boolean eccPreventRepricing;
    @XmlElement(name = "ECCQuoteLine")
    protected Integer eccQuoteLine;
    @XmlElementRef(name = "ECCQuoteNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eccQuoteNum;
    @XmlElement(name = "EnableCreateNewJob")
    protected Boolean enableCreateNewJob;
    @XmlElement(name = "EnableGetDtls")
    protected Boolean enableGetDtls;
    @XmlElement(name = "EnableKitUnitPrice")
    protected Boolean enableKitUnitPrice;
    @XmlElement(name = "EnableRelJob")
    protected Boolean enableRelJob;
    @XmlElement(name = "EnableRenewalNbr")
    protected Boolean enableRenewalNbr;
    @XmlElement(name = "EnableSchedJob")
    protected Boolean enableSchedJob;
    @XmlElement(name = "EnableSellingQty")
    protected Boolean enableSellingQty;
    @XmlElementRef(name = "EntryProcess", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> entryProcess;
    @XmlElementRef(name = "ExtCompany", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> extCompany;
    @XmlElement(name = "ExtPrice")
    protected BigDecimal extPrice;
    @XmlElement(name = "ExtPriceDtl")
    protected BigDecimal extPriceDtl;
    @XmlElement(name = "FromQuoteLineFlag")
    protected Boolean fromQuoteLineFlag;
    @XmlElement(name = "GetDtls")
    protected Boolean getDtls;
    @XmlElement(name = "GroupSeq")
    protected Integer groupSeq;
    @XmlElement(name = "HasComplement")
    protected Boolean hasComplement;
    @XmlElement(name = "HasDowngrade")
    protected Boolean hasDowngrade;
    @XmlElement(name = "HasSubstitute")
    protected Boolean hasSubstitute;
    @XmlElement(name = "HasUpgrade")
    protected Boolean hasUpgrade;
    @XmlElement(name = "ICPOLine")
    protected Integer icpoLine;
    @XmlElement(name = "ICPONum")
    protected Integer icpoNum;
    @XmlElementRef(name = "IUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ium;
    @XmlElement(name = "InDiscount")
    protected BigDecimal inDiscount;
    @XmlElement(name = "InExtPriceDtl")
    protected BigDecimal inExtPriceDtl;
    @XmlElement(name = "InListPrice")
    protected BigDecimal inListPrice;
    @XmlElement(name = "InMiscCharges")
    protected BigDecimal inMiscCharges;
    @XmlElement(name = "InOrdBasedPrice")
    protected BigDecimal inOrdBasedPrice;
    @XmlElement(name = "InPrice")
    protected Boolean inPrice;
    @XmlElement(name = "InUnitPrice")
    protected BigDecimal inUnitPrice;
    @XmlElementRef(name = "InvoiceComment", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> invoiceComment;
    @XmlElementRef(name = "InvtyUOM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> invtyUOM;
    @XmlElementRef(name = "JobTypeMFG", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> jobTypeMFG;
    @XmlElement(name = "JobWasCreated")
    protected Boolean jobWasCreated;
    @XmlElement(name = "KitAllowUpdate")
    protected Boolean kitAllowUpdate;
    @XmlElement(name = "KitBackFlush")
    protected Boolean kitBackFlush;
    @XmlElement(name = "KitChangeParms")
    protected Boolean kitChangeParms;
    @XmlElementRef(name = "KitCompOrigPart", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> kitCompOrigPart;
    @XmlElement(name = "KitCompOrigSeq")
    protected Integer kitCompOrigSeq;
    @XmlElement(name = "KitDisable")
    protected Boolean kitDisable;
    @XmlElementRef(name = "KitFlag", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> kitFlag;
    @XmlElementRef(name = "KitFlagDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> kitFlagDescription;
    @XmlElementRef(name = "KitOrderQtyUOM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> kitOrderQtyUOM;
    @XmlElement(name = "KitParentLine")
    protected Integer kitParentLine;
    @XmlElementRef(name = "KitPricing", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> kitPricing;
    @XmlElement(name = "KitPrintCompsInv")
    protected Boolean kitPrintCompsInv;
    @XmlElement(name = "KitPrintCompsPS")
    protected Boolean kitPrintCompsPS;
    @XmlElement(name = "KitQtyPer")
    protected BigDecimal kitQtyPer;
    @XmlElement(name = "KitShipComplete")
    protected Boolean kitShipComplete;
    @XmlElement(name = "KitStandard")
    protected Boolean kitStandard;
    @XmlElement(name = "KitsLoaded")
    protected Boolean kitsLoaded;
    @XmlElement(name = "LabCovered")
    protected Boolean labCovered;
    @XmlElement(name = "LaborDuration")
    protected Integer laborDuration;
    @XmlElementRef(name = "LaborMod", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> laborMod;
    @XmlElementRef(name = "LastConfigDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> lastConfigDate;
    @XmlElement(name = "LastConfigTime")
    protected Integer lastConfigTime;
    @XmlElementRef(name = "LastConfigUserID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lastConfigUserID;
    @XmlElement(name = "LessDiscount")
    protected BigDecimal lessDiscount;
    @XmlElementRef(name = "LineDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lineDesc;
    @XmlElementRef(name = "LineStatus", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lineStatus;
    @XmlElementRef(name = "LineType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lineType;
    @XmlElement(name = "Linked")
    protected Boolean linked;
    @XmlElement(name = "ListPrice")
    protected BigDecimal listPrice;
    @XmlElement(name = "LockDisc")
    protected Boolean lockDisc;
    @XmlElement(name = "LockPrice")
    protected Boolean lockPrice;
    @XmlElement(name = "LockQty")
    protected Boolean lockQty;
    @XmlElementRef(name = "LotNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lotNum;
    @XmlElementRef(name = "MOMsourceEst", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> moMsourceEst;
    @XmlElementRef(name = "MOMsourceType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> moMsourceType;
    @XmlElement(name = "MatCovered")
    protected Boolean matCovered;
    @XmlElement(name = "MaterialDuration")
    protected Integer materialDuration;
    @XmlElementRef(name = "MaterialMod", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> materialMod;
    @XmlElement(name = "MiscCharges")
    protected BigDecimal miscCharges;
    @XmlElement(name = "MiscCovered")
    protected Boolean miscCovered;
    @XmlElement(name = "MiscDuration")
    protected Integer miscDuration;
    @XmlElementRef(name = "MktgCampaignID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mktgCampaignID;
    @XmlElementRef(name = "MktgCampaignIDCampDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mktgCampaignIDCampDescription;
    @XmlElementRef(name = "MktgEvntEvntDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mktgEvntEvntDescription;
    @XmlElement(name = "MktgEvntSeq")
    protected Integer mktgEvntSeq;
    @XmlElement(name = "MultipleReleases")
    protected Boolean multipleReleases;
    @XmlElementRef(name = "NeedByDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> needByDate;
    @XmlElement(name = "OldOpenValue")
    protected BigDecimal oldOpenValue;
    @XmlElement(name = "OldOurOpenQty")
    protected BigDecimal oldOurOpenQty;
    @XmlElementRef(name = "OldProdCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> oldProdCode;
    @XmlElement(name = "OldSellingOpenQty")
    protected BigDecimal oldSellingOpenQty;
    @XmlElement(name = "OnHandQuantity")
    protected BigDecimal onHandQuantity;
    @XmlElement(name = "Onsite")
    protected Boolean onsite;
    @XmlElement(name = "OpenLine")
    protected Boolean openLine;
    @XmlElement(name = "OrdBasedPrice")
    protected BigDecimal ordBasedPrice;
    @XmlElementRef(name = "OrderComment", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderComment;
    @XmlElement(name = "OrderLine")
    protected Integer orderLine;
    @XmlElement(name = "OrderNum")
    protected Integer orderNum;
    @XmlElement(name = "OrderNumBTCustNum")
    protected Integer orderNumBTCustNum;
    @XmlElementRef(name = "OrderNumCardMemberName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderNumCardMemberName;
    @XmlElementRef(name = "OrderNumCurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderNumCurrencyCode;
    @XmlElement(name = "OrderQty")
    protected BigDecimal orderQty;
    @XmlElementRef(name = "OrigWhyNoTax", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> origWhyNoTax;
    @XmlElement(name = "OverrideDiscPriceList")
    protected Boolean overrideDiscPriceList;
    @XmlElement(name = "OverridePriceList")
    protected Boolean overridePriceList;
    @XmlElement(name = "Overs")
    protected BigDecimal overs;
    @XmlElement(name = "OversUnitPrice")
    protected BigDecimal oversUnitPrice;
    @XmlElementRef(name = "POLine", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> poLine;
    @XmlElementRef(name = "POLineRef", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> poLineRef;
    @XmlElement(name = "PartExists")
    protected Boolean partExists;
    @XmlElementRef(name = "PartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNum;
    @XmlElementRef(name = "PartNumIUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNumIUM;
    @XmlElementRef(name = "PartNumPartDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNumPartDescription;
    @XmlElementRef(name = "PartNumPricePerCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNumPricePerCode;
    @XmlElementRef(name = "PartNumSalesUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNumSalesUM;
    @XmlElement(name = "PartNumSellingFactor")
    protected BigDecimal partNumSellingFactor;
    @XmlElement(name = "PartNumTrackDimension")
    protected Boolean partNumTrackDimension;
    @XmlElement(name = "PartNumTrackLots")
    protected Boolean partNumTrackLots;
    @XmlElement(name = "PartNumTrackSerialNum")
    protected Boolean partNumTrackSerialNum;
    @XmlElement(name = "PartTrackDimension")
    protected Boolean partTrackDimension;
    @XmlElement(name = "PartTrackLots")
    protected Boolean partTrackLots;
    @XmlElementRef(name = "PickListComment", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pickListComment;
    @XmlElementRef(name = "PlanGUID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planGUID;
    @XmlElementRef(name = "PlanUserID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planUserID;
    @XmlElementRef(name = "PrevPartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prevPartNum;
    @XmlElement(name = "PrevSellQty")
    protected BigDecimal prevSellQty;
    @XmlElementRef(name = "PrevXPartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prevXPartNum;
    @XmlElementRef(name = "PriceGroupCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> priceGroupCode;
    @XmlElementRef(name = "PriceListCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> priceListCode;
    @XmlElementRef(name = "PriceListCodeDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> priceListCodeDesc;
    @XmlElementRef(name = "PricePerCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pricePerCode;
    @XmlElement(name = "PricingQty")
    protected BigDecimal pricingQty;
    @XmlElement(name = "PricingValue")
    protected BigDecimal pricingValue;
    @XmlElementRef(name = "ProFormaInvComment", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> proFormaInvComment;
    @XmlElement(name = "ProcessCounterSale")
    protected Boolean processCounterSale;
    @XmlElement(name = "ProcessQuickEntry")
    protected Boolean processQuickEntry;
    @XmlElementRef(name = "ProdCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prodCode;
    @XmlElementRef(name = "ProdCodeDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prodCodeDescription;
    @XmlElementRef(name = "ProjectID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> projectID;
    @XmlElementRef(name = "ProjectIDDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> projectIDDescription;
    @XmlElement(name = "QuoteLine")
    protected Integer quoteLine;
    @XmlElement(name = "QuoteNum")
    protected Integer quoteNum;
    @XmlElementRef(name = "QuoteNumCurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> quoteNumCurrencyCode;
    @XmlElement(name = "QuoteQtyNum")
    protected Integer quoteQtyNum;
    @XmlElement(name = "RMALine")
    protected Integer rmaLine;
    @XmlElement(name = "RMANum")
    protected Integer rmaNum;
    @XmlElementRef(name = "Reference", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reference;
    @XmlElement(name = "RelJob")
    protected Boolean relJob;
    @XmlElement(name = "RelWasRecInvoiced")
    protected Boolean relWasRecInvoiced;
    @XmlElement(name = "RenewalNbr")
    protected Integer renewalNbr;
    @XmlElement(name = "RepRate1")
    protected BigDecimal repRate1;
    @XmlElement(name = "RepRate2")
    protected BigDecimal repRate2;
    @XmlElement(name = "RepRate3")
    protected BigDecimal repRate3;
    @XmlElement(name = "RepRate4")
    protected BigDecimal repRate4;
    @XmlElement(name = "RepRate5")
    protected BigDecimal repRate5;
    @XmlElement(name = "RepSplit1")
    protected Integer repSplit1;
    @XmlElement(name = "RepSplit2")
    protected Integer repSplit2;
    @XmlElement(name = "RepSplit3")
    protected Integer repSplit3;
    @XmlElement(name = "RepSplit4")
    protected Integer repSplit4;
    @XmlElement(name = "RepSplit5")
    protected Integer repSplit5;
    @XmlElementRef(name = "RequestDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> requestDate;
    @XmlElementRef(name = "RespMessage", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> respMessage;
    @XmlElement(name = "ReverseCharge")
    protected Boolean reverseCharge;
    @XmlElementRef(name = "RevisionNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> revisionNum;
    @XmlElement(name = "Rework")
    protected Boolean rework;
    @XmlElement(name = "Rpt1AdvanceBillBal")
    protected BigDecimal rpt1AdvanceBillBal;
    @XmlElement(name = "Rpt1Discount")
    protected BigDecimal rpt1Discount;
    @XmlElement(name = "Rpt1DspDiscount")
    protected BigDecimal rpt1DspDiscount;
    @XmlElement(name = "Rpt1DspUnitPrice")
    protected BigDecimal rpt1DspUnitPrice;
    @XmlElement(name = "Rpt1ExtPrice")
    protected BigDecimal rpt1ExtPrice;
    @XmlElement(name = "Rpt1ExtPriceDtl")
    protected BigDecimal rpt1ExtPriceDtl;
    @XmlElement(name = "Rpt1InDiscount")
    protected BigDecimal rpt1InDiscount;
    @XmlElement(name = "Rpt1InExtPriceDtl")
    protected BigDecimal rpt1InExtPriceDtl;
    @XmlElement(name = "Rpt1InListPrice")
    protected BigDecimal rpt1InListPrice;
    @XmlElement(name = "Rpt1InMiscCharges")
    protected BigDecimal rpt1InMiscCharges;
    @XmlElement(name = "Rpt1InOrdBasedPrice")
    protected BigDecimal rpt1InOrdBasedPrice;
    @XmlElement(name = "Rpt1InUnitPrice")
    protected BigDecimal rpt1InUnitPrice;
    @XmlElement(name = "Rpt1LessDiscount")
    protected BigDecimal rpt1LessDiscount;
    @XmlElement(name = "Rpt1ListPrice")
    protected BigDecimal rpt1ListPrice;
    @XmlElement(name = "Rpt1MiscCharges")
    protected BigDecimal rpt1MiscCharges;
    @XmlElement(name = "Rpt1OrdBasedPrice")
    protected BigDecimal rpt1OrdBasedPrice;
    @XmlElement(name = "Rpt1TaxAmt")
    protected BigDecimal rpt1TaxAmt;
    @XmlElement(name = "Rpt1TotalPrice")
    protected BigDecimal rpt1TotalPrice;
    @XmlElement(name = "Rpt1UnitPrice")
    protected BigDecimal rpt1UnitPrice;
    @XmlElement(name = "Rpt2AdvanceBillBal")
    protected BigDecimal rpt2AdvanceBillBal;
    @XmlElement(name = "Rpt2Discount")
    protected BigDecimal rpt2Discount;
    @XmlElement(name = "Rpt2DspDiscount")
    protected BigDecimal rpt2DspDiscount;
    @XmlElement(name = "Rpt2DspUnitPrice")
    protected BigDecimal rpt2DspUnitPrice;
    @XmlElement(name = "Rpt2ExtPrice")
    protected BigDecimal rpt2ExtPrice;
    @XmlElement(name = "Rpt2ExtPriceDtl")
    protected BigDecimal rpt2ExtPriceDtl;
    @XmlElement(name = "Rpt2InDiscount")
    protected BigDecimal rpt2InDiscount;
    @XmlElement(name = "Rpt2InExtPriceDtl")
    protected BigDecimal rpt2InExtPriceDtl;
    @XmlElement(name = "Rpt2InListPrice")
    protected BigDecimal rpt2InListPrice;
    @XmlElement(name = "Rpt2InMiscCharges")
    protected BigDecimal rpt2InMiscCharges;
    @XmlElement(name = "Rpt2InOrdBasedPrice")
    protected BigDecimal rpt2InOrdBasedPrice;
    @XmlElement(name = "Rpt2InUnitPrice")
    protected BigDecimal rpt2InUnitPrice;
    @XmlElement(name = "Rpt2LessDiscount")
    protected BigDecimal rpt2LessDiscount;
    @XmlElement(name = "Rpt2ListPrice")
    protected BigDecimal rpt2ListPrice;
    @XmlElement(name = "Rpt2MiscCharges")
    protected BigDecimal rpt2MiscCharges;
    @XmlElement(name = "Rpt2OrdBasedPrice")
    protected BigDecimal rpt2OrdBasedPrice;
    @XmlElement(name = "Rpt2TaxAmt")
    protected BigDecimal rpt2TaxAmt;
    @XmlElement(name = "Rpt2TotalPrice")
    protected BigDecimal rpt2TotalPrice;
    @XmlElement(name = "Rpt2UnitPrice")
    protected BigDecimal rpt2UnitPrice;
    @XmlElement(name = "Rpt3AdvanceBillBal")
    protected BigDecimal rpt3AdvanceBillBal;
    @XmlElement(name = "Rpt3Discount")
    protected BigDecimal rpt3Discount;
    @XmlElement(name = "Rpt3DspDiscount")
    protected BigDecimal rpt3DspDiscount;
    @XmlElement(name = "Rpt3DspUnitPrice")
    protected BigDecimal rpt3DspUnitPrice;
    @XmlElement(name = "Rpt3ExtPrice")
    protected BigDecimal rpt3ExtPrice;
    @XmlElement(name = "Rpt3ExtPriceDtl")
    protected BigDecimal rpt3ExtPriceDtl;
    @XmlElement(name = "Rpt3InDiscount")
    protected BigDecimal rpt3InDiscount;
    @XmlElement(name = "Rpt3InExtPriceDtl")
    protected BigDecimal rpt3InExtPriceDtl;
    @XmlElement(name = "Rpt3InListPrice")
    protected BigDecimal rpt3InListPrice;
    @XmlElement(name = "Rpt3InMiscCharges")
    protected BigDecimal rpt3InMiscCharges;
    @XmlElement(name = "Rpt3InOrdBasedPrice")
    protected BigDecimal rpt3InOrdBasedPrice;
    @XmlElement(name = "Rpt3InUnitPrice")
    protected BigDecimal rpt3InUnitPrice;
    @XmlElement(name = "Rpt3LessDiscount")
    protected BigDecimal rpt3LessDiscount;
    @XmlElement(name = "Rpt3ListPrice")
    protected BigDecimal rpt3ListPrice;
    @XmlElement(name = "Rpt3MiscCharges")
    protected BigDecimal rpt3MiscCharges;
    @XmlElement(name = "Rpt3OrdBasedPrice")
    protected BigDecimal rpt3OrdBasedPrice;
    @XmlElement(name = "Rpt3TaxAmt")
    protected BigDecimal rpt3TaxAmt;
    @XmlElement(name = "Rpt3TotalPrice")
    protected BigDecimal rpt3TotalPrice;
    @XmlElement(name = "Rpt3UnitPrice")
    protected BigDecimal rpt3UnitPrice;
    @XmlElementRef(name = "SalesCatID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesCatID;
    @XmlElementRef(name = "SalesCatIDDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesCatIDDescription;
    @XmlElementRef(name = "SalesRepName1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepName1;
    @XmlElementRef(name = "SalesRepName2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepName2;
    @XmlElementRef(name = "SalesRepName3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepName3;
    @XmlElementRef(name = "SalesRepName4", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepName4;
    @XmlElementRef(name = "SalesRepName5", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepName5;
    @XmlElementRef(name = "SalesUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesUM;
    @XmlElement(name = "SchedJob")
    protected Boolean schedJob;
    @XmlElement(name = "SellingFactor")
    protected BigDecimal sellingFactor;
    @XmlElementRef(name = "SellingFactorDirection", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sellingFactorDirection;
    @XmlElement(name = "SellingQuantity")
    protected BigDecimal sellingQuantity;
    @XmlElementRef(name = "ShipComment", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipComment;
    @XmlElement(name = "ShipLineComplete")
    protected Boolean shipLineComplete;
    @XmlElementRef(name = "SmartString", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> smartString;
    @XmlElement(name = "SmartStringProcessed")
    protected Boolean smartStringProcessed;
    @XmlElement(name = "SysRevID")
    protected Long sysRevID;
    @XmlElement(name = "TMBilling")
    protected Boolean tmBilling;
    @XmlElement(name = "TaxAmt")
    protected BigDecimal taxAmt;
    @XmlElementRef(name = "TaxCatID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxCatID;
    @XmlElementRef(name = "TaxCatIDDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxCatIDDescription;
    @XmlElement(name = "ThisOrderInvtyQty")
    protected BigDecimal thisOrderInvtyQty;
    @XmlElement(name = "TotalPrice")
    protected BigDecimal totalPrice;
    @XmlElement(name = "TotalReleases")
    protected Integer totalReleases;
    @XmlElement(name = "TotalShipped")
    protected BigDecimal totalShipped;
    @XmlElement(name = "Unders")
    protected BigDecimal unders;
    @XmlElement(name = "UndersPct")
    protected BigDecimal undersPct;
    @XmlElement(name = "UnitPrice")
    protected BigDecimal unitPrice;
    @XmlElement(name = "VoidLine")
    protected Boolean voidLine;
    @XmlElementRef(name = "WarehouseCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> warehouseCode;
    @XmlElementRef(name = "WarehouseDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> warehouseDesc;
    @XmlElement(name = "Warranty")
    protected Boolean warranty;
    @XmlElementRef(name = "WarrantyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> warrantyCode;
    @XmlElementRef(name = "WarrantyCodeWarrDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> warrantyCodeWarrDescription;
    @XmlElementRef(name = "WarrantyComment", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> warrantyComment;
    @XmlElementRef(name = "XPartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> xPartNum;
    @XmlElementRef(name = "XRevisionNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> xRevisionNum;

    /**
     * Gets the value of the advanceBillBal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAdvanceBillBal() {
        return advanceBillBal;
    }

    /**
     * Sets the value of the advanceBillBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAdvanceBillBal(BigDecimal value) {
        this.advanceBillBal = value;
    }

    /**
     * Gets the value of the availPriceLists property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAvailPriceLists() {
        return availPriceLists;
    }

    /**
     * Sets the value of the availPriceLists property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAvailPriceLists(JAXBElement<String> value) {
        this.availPriceLists = value;
    }

    /**
     * Gets the value of the availUMFromQuote property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAvailUMFromQuote() {
        return availUMFromQuote;
    }

    /**
     * Sets the value of the availUMFromQuote property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAvailUMFromQuote(JAXBElement<String> value) {
        this.availUMFromQuote = value;
    }

    /**
     * Gets the value of the availableQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAvailableQuantity() {
        return availableQuantity;
    }

    /**
     * Sets the value of the availableQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAvailableQuantity(BigDecimal value) {
        this.availableQuantity = value;
    }

    /**
     * Gets the value of the baseCurrSymbol property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBaseCurrSymbol() {
        return baseCurrSymbol;
    }

    /**
     * Sets the value of the baseCurrSymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBaseCurrSymbol(JAXBElement<String> value) {
        this.baseCurrSymbol = value;
    }

    /**
     * Gets the value of the baseCurrencyID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBaseCurrencyID() {
        return baseCurrencyID;
    }

    /**
     * Sets the value of the baseCurrencyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBaseCurrencyID(JAXBElement<String> value) {
        this.baseCurrencyID = value;
    }

    /**
     * Gets the value of the basePartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBasePartNum() {
        return basePartNum;
    }

    /**
     * Sets the value of the basePartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBasePartNum(JAXBElement<String> value) {
        this.basePartNum = value;
    }

    /**
     * Gets the value of the baseRevisionNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBaseRevisionNum() {
        return baseRevisionNum;
    }

    /**
     * Sets the value of the baseRevisionNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBaseRevisionNum(JAXBElement<String> value) {
        this.baseRevisionNum = value;
    }

    /**
     * Gets the value of the binNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBinNum() {
        return binNum;
    }

    /**
     * Sets the value of the binNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBinNum(JAXBElement<String> value) {
        this.binNum = value;
    }

    /**
     * Gets the value of the bitFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBitFlag() {
        return bitFlag;
    }

    /**
     * Sets the value of the bitFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBitFlag(Integer value) {
        this.bitFlag = value;
    }

    /**
     * Gets the value of the breakListCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBreakListCode() {
        return breakListCode;
    }

    /**
     * Sets the value of the breakListCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBreakListCode(JAXBElement<String> value) {
        this.breakListCode = value;
    }

    /**
     * Gets the value of the changeDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getChangeDate() {
        return changeDate;
    }

    /**
     * Sets the value of the changeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setChangeDate(JAXBElement<XMLGregorianCalendar> value) {
        this.changeDate = value;
    }

    /**
     * Gets the value of the changeTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChangeTime() {
        return changeTime;
    }

    /**
     * Sets the value of the changeTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChangeTime(Integer value) {
        this.changeTime = value;
    }

    /**
     * Gets the value of the changedBy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangedBy() {
        return changedBy;
    }

    /**
     * Sets the value of the changedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangedBy(JAXBElement<String> value) {
        this.changedBy = value;
    }

    /**
     * Gets the value of the commissionable property.
     * This getter has been renamed from isCommissionable() to getCommissionable() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCommissionable() {
        return commissionable;
    }

    /**
     * Sets the value of the commissionable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCommissionable(Boolean value) {
        this.commissionable = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompany(JAXBElement<String> value) {
        this.company = value;
    }

    /**
     * Gets the value of the configBaseUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getConfigBaseUnitPrice() {
        return configBaseUnitPrice;
    }

    /**
     * Sets the value of the configBaseUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setConfigBaseUnitPrice(BigDecimal value) {
        this.configBaseUnitPrice = value;
    }

    /**
     * Gets the value of the configUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getConfigUnitPrice() {
        return configUnitPrice;
    }

    /**
     * Sets the value of the configUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setConfigUnitPrice(BigDecimal value) {
        this.configUnitPrice = value;
    }

    /**
     * Gets the value of the configured property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getConfigured() {
        return configured;
    }

    /**
     * Sets the value of the configured property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setConfigured(JAXBElement<String> value) {
        this.configured = value;
    }

    /**
     * Gets the value of the contractCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContractCode() {
        return contractCode;
    }

    /**
     * Sets the value of the contractCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContractCode(JAXBElement<String> value) {
        this.contractCode = value;
    }

    /**
     * Gets the value of the contractCodeContractDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContractCodeContractDescription() {
        return contractCodeContractDescription;
    }

    /**
     * Sets the value of the contractCodeContractDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContractCodeContractDescription(JAXBElement<String> value) {
        this.contractCodeContractDescription = value;
    }

    /**
     * Gets the value of the contractNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getContractNum() {
        return contractNum;
    }

    /**
     * Sets the value of the contractNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setContractNum(Integer value) {
        this.contractNum = value;
    }

    /**
     * Gets the value of the counterSale property.
     * This getter has been renamed from isCounterSale() to getCounterSale() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCounterSale() {
        return counterSale;
    }

    /**
     * Sets the value of the counterSale property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCounterSale(Boolean value) {
        this.counterSale = value;
    }

    /**
     * Gets the value of the counterSaleBinNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCounterSaleBinNum() {
        return counterSaleBinNum;
    }

    /**
     * Sets the value of the counterSaleBinNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCounterSaleBinNum(JAXBElement<String> value) {
        this.counterSaleBinNum = value;
    }

    /**
     * Gets the value of the counterSaleDimCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCounterSaleDimCode() {
        return counterSaleDimCode;
    }

    /**
     * Sets the value of the counterSaleDimCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCounterSaleDimCode(JAXBElement<String> value) {
        this.counterSaleDimCode = value;
    }

    /**
     * Gets the value of the counterSaleLotNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCounterSaleLotNum() {
        return counterSaleLotNum;
    }

    /**
     * Sets the value of the counterSaleLotNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCounterSaleLotNum(JAXBElement<String> value) {
        this.counterSaleLotNum = value;
    }

    /**
     * Gets the value of the counterSaleWarehouse property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCounterSaleWarehouse() {
        return counterSaleWarehouse;
    }

    /**
     * Sets the value of the counterSaleWarehouse property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCounterSaleWarehouse(JAXBElement<String> value) {
        this.counterSaleWarehouse = value;
    }

    /**
     * Gets the value of the createNewJob property.
     * This getter has been renamed from isCreateNewJob() to getCreateNewJob() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCreateNewJob() {
        return createNewJob;
    }

    /**
     * Sets the value of the createNewJob property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCreateNewJob(Boolean value) {
        this.createNewJob = value;
    }

    /**
     * Gets the value of the creditLimitMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCreditLimitMessage() {
        return creditLimitMessage;
    }

    /**
     * Sets the value of the creditLimitMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCreditLimitMessage(JAXBElement<String> value) {
        this.creditLimitMessage = value;
    }

    /**
     * Gets the value of the creditLimitSource property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCreditLimitSource() {
        return creditLimitSource;
    }

    /**
     * Sets the value of the creditLimitSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCreditLimitSource(JAXBElement<String> value) {
        this.creditLimitSource = value;
    }

    /**
     * Gets the value of the cumeDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getCumeDate() {
        return cumeDate;
    }

    /**
     * Sets the value of the cumeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setCumeDate(JAXBElement<XMLGregorianCalendar> value) {
        this.cumeDate = value;
    }

    /**
     * Gets the value of the cumeQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCumeQty() {
        return cumeQty;
    }

    /**
     * Sets the value of the cumeQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCumeQty(BigDecimal value) {
        this.cumeQty = value;
    }

    /**
     * Gets the value of the currSymbol property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrSymbol() {
        return currSymbol;
    }

    /**
     * Sets the value of the currSymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrSymbol(JAXBElement<String> value) {
        this.currSymbol = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the currencyID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyID() {
        return currencyID;
    }

    /**
     * Sets the value of the currencyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyID(JAXBElement<String> value) {
        this.currencyID = value;
    }

    /**
     * Gets the value of the currencySwitch property.
     * This getter has been renamed from isCurrencySwitch() to getCurrencySwitch() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCurrencySwitch() {
        return currencySwitch;
    }

    /**
     * Sets the value of the currencySwitch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCurrencySwitch(Boolean value) {
        this.currencySwitch = value;
    }

    /**
     * Gets the value of the custNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustNum() {
        return custNum;
    }

    /**
     * Sets the value of the custNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustNum(Integer value) {
        this.custNum = value;
    }

    /**
     * Gets the value of the custNumBTName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustNumBTName() {
        return custNumBTName;
    }

    /**
     * Sets the value of the custNumBTName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustNumBTName(JAXBElement<String> value) {
        this.custNumBTName = value;
    }

    /**
     * Gets the value of the custNumCustID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustNumCustID() {
        return custNumCustID;
    }

    /**
     * Sets the value of the custNumCustID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustNumCustID(JAXBElement<String> value) {
        this.custNumCustID = value;
    }

    /**
     * Gets the value of the custNumName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustNumName() {
        return custNumName;
    }

    /**
     * Sets the value of the custNumName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustNumName(JAXBElement<String> value) {
        this.custNumName = value;
    }

    /**
     * Gets the value of the dum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDUM() {
        return dum;
    }

    /**
     * Sets the value of the dum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDUM(JAXBElement<String> value) {
        this.dum = value;
    }

    /**
     * Gets the value of the defaultOversPricing property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultOversPricing() {
        return defaultOversPricing;
    }

    /**
     * Sets the value of the defaultOversPricing property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultOversPricing(JAXBElement<String> value) {
        this.defaultOversPricing = value;
    }

    /**
     * Gets the value of the demandContractLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDemandContractLine() {
        return demandContractLine;
    }

    /**
     * Sets the value of the demandContractLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDemandContractLine(Integer value) {
        this.demandContractLine = value;
    }

    /**
     * Gets the value of the demandContractNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDemandContractNum() {
        return demandContractNum;
    }

    /**
     * Sets the value of the demandContractNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDemandContractNum(Integer value) {
        this.demandContractNum = value;
    }

    /**
     * Gets the value of the demandDtlRejected property.
     * This getter has been renamed from isDemandDtlRejected() to getDemandDtlRejected() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDemandDtlRejected() {
        return demandDtlRejected;
    }

    /**
     * Sets the value of the demandDtlRejected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDemandDtlRejected(Boolean value) {
        this.demandDtlRejected = value;
    }

    /**
     * Gets the value of the demandDtlSeq property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDemandDtlSeq() {
        return demandDtlSeq;
    }

    /**
     * Sets the value of the demandDtlSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDemandDtlSeq(Integer value) {
        this.demandDtlSeq = value;
    }

    /**
     * Gets the value of the demandHeadSeq property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDemandHeadSeq() {
        return demandHeadSeq;
    }

    /**
     * Sets the value of the demandHeadSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDemandHeadSeq(Integer value) {
        this.demandHeadSeq = value;
    }

    /**
     * Gets the value of the demandQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDemandQuantity() {
        return demandQuantity;
    }

    /**
     * Sets the value of the demandQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDemandQuantity(BigDecimal value) {
        this.demandQuantity = value;
    }

    /**
     * Gets the value of the dimCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDimCode() {
        return dimCode;
    }

    /**
     * Sets the value of the dimCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDimCode(JAXBElement<String> value) {
        this.dimCode = value;
    }

    /**
     * Gets the value of the dimConvFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDimConvFactor() {
        return dimConvFactor;
    }

    /**
     * Sets the value of the dimConvFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDimConvFactor(BigDecimal value) {
        this.dimConvFactor = value;
    }

    /**
     * Gets the value of the discBreakListCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDiscBreakListCode() {
        return discBreakListCode;
    }

    /**
     * Sets the value of the discBreakListCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDiscBreakListCode(JAXBElement<String> value) {
        this.discBreakListCode = value;
    }

    /**
     * Gets the value of the discBreakListCodeEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDiscBreakListCodeEndDate() {
        return discBreakListCodeEndDate;
    }

    /**
     * Sets the value of the discBreakListCodeEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDiscBreakListCodeEndDate(JAXBElement<XMLGregorianCalendar> value) {
        this.discBreakListCodeEndDate = value;
    }

    /**
     * Gets the value of the discBreakListCodeListDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDiscBreakListCodeListDescription() {
        return discBreakListCodeListDescription;
    }

    /**
     * Sets the value of the discBreakListCodeListDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDiscBreakListCodeListDescription(JAXBElement<String> value) {
        this.discBreakListCodeListDescription = value;
    }

    /**
     * Gets the value of the discBreakListCodeStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDiscBreakListCodeStartDate() {
        return discBreakListCodeStartDate;
    }

    /**
     * Sets the value of the discBreakListCodeStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDiscBreakListCodeStartDate(JAXBElement<XMLGregorianCalendar> value) {
        this.discBreakListCodeStartDate = value;
    }

    /**
     * Gets the value of the discListPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscListPrice() {
        return discListPrice;
    }

    /**
     * Sets the value of the discListPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscListPrice(BigDecimal value) {
        this.discListPrice = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscount(BigDecimal value) {
        this.discount = value;
    }

    /**
     * Gets the value of the discountPercent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountPercent() {
        return discountPercent;
    }

    /**
     * Sets the value of the discountPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountPercent(BigDecimal value) {
        this.discountPercent = value;
    }

    /**
     * Gets the value of the displaySeq property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDisplaySeq() {
        return displaySeq;
    }

    /**
     * Sets the value of the displaySeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDisplaySeq(BigDecimal value) {
        this.displaySeq = value;
    }

    /**
     * Gets the value of the doNotShipAfterDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDoNotShipAfterDate() {
        return doNotShipAfterDate;
    }

    /**
     * Sets the value of the doNotShipAfterDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDoNotShipAfterDate(JAXBElement<XMLGregorianCalendar> value) {
        this.doNotShipAfterDate = value;
    }

    /**
     * Gets the value of the doNotShipBeforeDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDoNotShipBeforeDate() {
        return doNotShipBeforeDate;
    }

    /**
     * Sets the value of the doNotShipBeforeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDoNotShipBeforeDate(JAXBElement<XMLGregorianCalendar> value) {
        this.doNotShipBeforeDate = value;
    }

    /**
     * Gets the value of the docAdvanceBillBal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocAdvanceBillBal() {
        return docAdvanceBillBal;
    }

    /**
     * Sets the value of the docAdvanceBillBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocAdvanceBillBal(BigDecimal value) {
        this.docAdvanceBillBal = value;
    }

    /**
     * Gets the value of the docDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocDiscount() {
        return docDiscount;
    }

    /**
     * Sets the value of the docDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocDiscount(BigDecimal value) {
        this.docDiscount = value;
    }

    /**
     * Gets the value of the docDspDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocDspDiscount() {
        return docDspDiscount;
    }

    /**
     * Sets the value of the docDspDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocDspDiscount(BigDecimal value) {
        this.docDspDiscount = value;
    }

    /**
     * Gets the value of the docDspUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocDspUnitPrice() {
        return docDspUnitPrice;
    }

    /**
     * Sets the value of the docDspUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocDspUnitPrice(BigDecimal value) {
        this.docDspUnitPrice = value;
    }

    /**
     * Gets the value of the docExtPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocExtPrice() {
        return docExtPrice;
    }

    /**
     * Sets the value of the docExtPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocExtPrice(BigDecimal value) {
        this.docExtPrice = value;
    }

    /**
     * Gets the value of the docExtPriceDtl property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocExtPriceDtl() {
        return docExtPriceDtl;
    }

    /**
     * Sets the value of the docExtPriceDtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocExtPriceDtl(BigDecimal value) {
        this.docExtPriceDtl = value;
    }

    /**
     * Gets the value of the docInDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocInDiscount() {
        return docInDiscount;
    }

    /**
     * Sets the value of the docInDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocInDiscount(BigDecimal value) {
        this.docInDiscount = value;
    }

    /**
     * Gets the value of the docInExtPriceDtl property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocInExtPriceDtl() {
        return docInExtPriceDtl;
    }

    /**
     * Sets the value of the docInExtPriceDtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocInExtPriceDtl(BigDecimal value) {
        this.docInExtPriceDtl = value;
    }

    /**
     * Gets the value of the docInListPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocInListPrice() {
        return docInListPrice;
    }

    /**
     * Sets the value of the docInListPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocInListPrice(BigDecimal value) {
        this.docInListPrice = value;
    }

    /**
     * Gets the value of the docInMiscCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocInMiscCharges() {
        return docInMiscCharges;
    }

    /**
     * Sets the value of the docInMiscCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocInMiscCharges(BigDecimal value) {
        this.docInMiscCharges = value;
    }

    /**
     * Gets the value of the docInOrdBasedPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocInOrdBasedPrice() {
        return docInOrdBasedPrice;
    }

    /**
     * Sets the value of the docInOrdBasedPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocInOrdBasedPrice(BigDecimal value) {
        this.docInOrdBasedPrice = value;
    }

    /**
     * Gets the value of the docInUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocInUnitPrice() {
        return docInUnitPrice;
    }

    /**
     * Sets the value of the docInUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocInUnitPrice(BigDecimal value) {
        this.docInUnitPrice = value;
    }

    /**
     * Gets the value of the docLessDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocLessDiscount() {
        return docLessDiscount;
    }

    /**
     * Sets the value of the docLessDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocLessDiscount(BigDecimal value) {
        this.docLessDiscount = value;
    }

    /**
     * Gets the value of the docListPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocListPrice() {
        return docListPrice;
    }

    /**
     * Sets the value of the docListPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocListPrice(BigDecimal value) {
        this.docListPrice = value;
    }

    /**
     * Gets the value of the docMiscCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocMiscCharges() {
        return docMiscCharges;
    }

    /**
     * Sets the value of the docMiscCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocMiscCharges(BigDecimal value) {
        this.docMiscCharges = value;
    }

    /**
     * Gets the value of the docOrdBasedPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocOrdBasedPrice() {
        return docOrdBasedPrice;
    }

    /**
     * Sets the value of the docOrdBasedPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocOrdBasedPrice(BigDecimal value) {
        this.docOrdBasedPrice = value;
    }

    /**
     * Gets the value of the docTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTaxAmt() {
        return docTaxAmt;
    }

    /**
     * Sets the value of the docTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTaxAmt(BigDecimal value) {
        this.docTaxAmt = value;
    }

    /**
     * Gets the value of the docTotalPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTotalPrice() {
        return docTotalPrice;
    }

    /**
     * Sets the value of the docTotalPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTotalPrice(BigDecimal value) {
        this.docTotalPrice = value;
    }

    /**
     * Gets the value of the docUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocUnitPrice() {
        return docUnitPrice;
    }

    /**
     * Sets the value of the docUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocUnitPrice(BigDecimal value) {
        this.docUnitPrice = value;
    }

    /**
     * Gets the value of the dspDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDspDiscount() {
        return dspDiscount;
    }

    /**
     * Sets the value of the dspDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDspDiscount(BigDecimal value) {
        this.dspDiscount = value;
    }

    /**
     * Gets the value of the dspJobType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDspJobType() {
        return dspJobType;
    }

    /**
     * Sets the value of the dspJobType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDspJobType(JAXBElement<String> value) {
        this.dspJobType = value;
    }

    /**
     * Gets the value of the dspUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDspUnitPrice() {
        return dspUnitPrice;
    }

    /**
     * Sets the value of the dspUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDspUnitPrice(BigDecimal value) {
        this.dspUnitPrice = value;
    }

    /**
     * Gets the value of the dupOnJobCrt property.
     * This getter has been renamed from isDupOnJobCrt() to getDupOnJobCrt() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDupOnJobCrt() {
        return dupOnJobCrt;
    }

    /**
     * Sets the value of the dupOnJobCrt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDupOnJobCrt(Boolean value) {
        this.dupOnJobCrt = value;
    }

    /**
     * Gets the value of the eccDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getECCDiscount() {
        return eccDiscount;
    }

    /**
     * Sets the value of the eccDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setECCDiscount(BigDecimal value) {
        this.eccDiscount = value;
    }

    /**
     * Gets the value of the eccOrderLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getECCOrderLine() {
        return eccOrderLine;
    }

    /**
     * Sets the value of the eccOrderLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setECCOrderLine(Integer value) {
        this.eccOrderLine = value;
    }

    /**
     * Gets the value of the eccOrderNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getECCOrderNum() {
        return eccOrderNum;
    }

    /**
     * Sets the value of the eccOrderNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setECCOrderNum(JAXBElement<String> value) {
        this.eccOrderNum = value;
    }

    /**
     * Gets the value of the eccPlant property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getECCPlant() {
        return eccPlant;
    }

    /**
     * Sets the value of the eccPlant property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setECCPlant(JAXBElement<String> value) {
        this.eccPlant = value;
    }

    /**
     * Gets the value of the eccPreventRepricing property.
     * This getter has been renamed from isECCPreventRepricing() to getECCPreventRepricing() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getECCPreventRepricing() {
        return eccPreventRepricing;
    }

    /**
     * Sets the value of the eccPreventRepricing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setECCPreventRepricing(Boolean value) {
        this.eccPreventRepricing = value;
    }

    /**
     * Gets the value of the eccQuoteLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getECCQuoteLine() {
        return eccQuoteLine;
    }

    /**
     * Sets the value of the eccQuoteLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setECCQuoteLine(Integer value) {
        this.eccQuoteLine = value;
    }

    /**
     * Gets the value of the eccQuoteNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getECCQuoteNum() {
        return eccQuoteNum;
    }

    /**
     * Sets the value of the eccQuoteNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setECCQuoteNum(JAXBElement<String> value) {
        this.eccQuoteNum = value;
    }

    /**
     * Gets the value of the enableCreateNewJob property.
     * This getter has been renamed from isEnableCreateNewJob() to getEnableCreateNewJob() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnableCreateNewJob() {
        return enableCreateNewJob;
    }

    /**
     * Sets the value of the enableCreateNewJob property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCreateNewJob(Boolean value) {
        this.enableCreateNewJob = value;
    }

    /**
     * Gets the value of the enableGetDtls property.
     * This getter has been renamed from isEnableGetDtls() to getEnableGetDtls() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnableGetDtls() {
        return enableGetDtls;
    }

    /**
     * Sets the value of the enableGetDtls property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableGetDtls(Boolean value) {
        this.enableGetDtls = value;
    }

    /**
     * Gets the value of the enableKitUnitPrice property.
     * This getter has been renamed from isEnableKitUnitPrice() to getEnableKitUnitPrice() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnableKitUnitPrice() {
        return enableKitUnitPrice;
    }

    /**
     * Sets the value of the enableKitUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableKitUnitPrice(Boolean value) {
        this.enableKitUnitPrice = value;
    }

    /**
     * Gets the value of the enableRelJob property.
     * This getter has been renamed from isEnableRelJob() to getEnableRelJob() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnableRelJob() {
        return enableRelJob;
    }

    /**
     * Sets the value of the enableRelJob property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableRelJob(Boolean value) {
        this.enableRelJob = value;
    }

    /**
     * Gets the value of the enableRenewalNbr property.
     * This getter has been renamed from isEnableRenewalNbr() to getEnableRenewalNbr() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnableRenewalNbr() {
        return enableRenewalNbr;
    }

    /**
     * Sets the value of the enableRenewalNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableRenewalNbr(Boolean value) {
        this.enableRenewalNbr = value;
    }

    /**
     * Gets the value of the enableSchedJob property.
     * This getter has been renamed from isEnableSchedJob() to getEnableSchedJob() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnableSchedJob() {
        return enableSchedJob;
    }

    /**
     * Sets the value of the enableSchedJob property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSchedJob(Boolean value) {
        this.enableSchedJob = value;
    }

    /**
     * Gets the value of the enableSellingQty property.
     * This getter has been renamed from isEnableSellingQty() to getEnableSellingQty() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnableSellingQty() {
        return enableSellingQty;
    }

    /**
     * Sets the value of the enableSellingQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSellingQty(Boolean value) {
        this.enableSellingQty = value;
    }

    /**
     * Gets the value of the entryProcess property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEntryProcess() {
        return entryProcess;
    }

    /**
     * Sets the value of the entryProcess property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEntryProcess(JAXBElement<String> value) {
        this.entryProcess = value;
    }

    /**
     * Gets the value of the extCompany property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExtCompany() {
        return extCompany;
    }

    /**
     * Sets the value of the extCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExtCompany(JAXBElement<String> value) {
        this.extCompany = value;
    }

    /**
     * Gets the value of the extPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExtPrice() {
        return extPrice;
    }

    /**
     * Sets the value of the extPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExtPrice(BigDecimal value) {
        this.extPrice = value;
    }

    /**
     * Gets the value of the extPriceDtl property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExtPriceDtl() {
        return extPriceDtl;
    }

    /**
     * Sets the value of the extPriceDtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExtPriceDtl(BigDecimal value) {
        this.extPriceDtl = value;
    }

    /**
     * Gets the value of the fromQuoteLineFlag property.
     * This getter has been renamed from isFromQuoteLineFlag() to getFromQuoteLineFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getFromQuoteLineFlag() {
        return fromQuoteLineFlag;
    }

    /**
     * Sets the value of the fromQuoteLineFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFromQuoteLineFlag(Boolean value) {
        this.fromQuoteLineFlag = value;
    }

    /**
     * Gets the value of the getDtls property.
     * This getter has been renamed from isGetDtls() to getGetDtls() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getGetDtls() {
        return getDtls;
    }

    /**
     * Sets the value of the getDtls property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGetDtls(Boolean value) {
        this.getDtls = value;
    }

    /**
     * Gets the value of the groupSeq property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGroupSeq() {
        return groupSeq;
    }

    /**
     * Sets the value of the groupSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGroupSeq(Integer value) {
        this.groupSeq = value;
    }

    /**
     * Gets the value of the hasComplement property.
     * This getter has been renamed from isHasComplement() to getHasComplement() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHasComplement() {
        return hasComplement;
    }

    /**
     * Sets the value of the hasComplement property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasComplement(Boolean value) {
        this.hasComplement = value;
    }

    /**
     * Gets the value of the hasDowngrade property.
     * This getter has been renamed from isHasDowngrade() to getHasDowngrade() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHasDowngrade() {
        return hasDowngrade;
    }

    /**
     * Sets the value of the hasDowngrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasDowngrade(Boolean value) {
        this.hasDowngrade = value;
    }

    /**
     * Gets the value of the hasSubstitute property.
     * This getter has been renamed from isHasSubstitute() to getHasSubstitute() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHasSubstitute() {
        return hasSubstitute;
    }

    /**
     * Sets the value of the hasSubstitute property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasSubstitute(Boolean value) {
        this.hasSubstitute = value;
    }

    /**
     * Gets the value of the hasUpgrade property.
     * This getter has been renamed from isHasUpgrade() to getHasUpgrade() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHasUpgrade() {
        return hasUpgrade;
    }

    /**
     * Sets the value of the hasUpgrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasUpgrade(Boolean value) {
        this.hasUpgrade = value;
    }

    /**
     * Gets the value of the icpoLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getICPOLine() {
        return icpoLine;
    }

    /**
     * Sets the value of the icpoLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setICPOLine(Integer value) {
        this.icpoLine = value;
    }

    /**
     * Gets the value of the icpoNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getICPONum() {
        return icpoNum;
    }

    /**
     * Sets the value of the icpoNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setICPONum(Integer value) {
        this.icpoNum = value;
    }

    /**
     * Gets the value of the ium property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIUM() {
        return ium;
    }

    /**
     * Sets the value of the ium property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIUM(JAXBElement<String> value) {
        this.ium = value;
    }

    /**
     * Gets the value of the inDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInDiscount() {
        return inDiscount;
    }

    /**
     * Sets the value of the inDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInDiscount(BigDecimal value) {
        this.inDiscount = value;
    }

    /**
     * Gets the value of the inExtPriceDtl property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInExtPriceDtl() {
        return inExtPriceDtl;
    }

    /**
     * Sets the value of the inExtPriceDtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInExtPriceDtl(BigDecimal value) {
        this.inExtPriceDtl = value;
    }

    /**
     * Gets the value of the inListPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInListPrice() {
        return inListPrice;
    }

    /**
     * Sets the value of the inListPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInListPrice(BigDecimal value) {
        this.inListPrice = value;
    }

    /**
     * Gets the value of the inMiscCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInMiscCharges() {
        return inMiscCharges;
    }

    /**
     * Sets the value of the inMiscCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInMiscCharges(BigDecimal value) {
        this.inMiscCharges = value;
    }

    /**
     * Gets the value of the inOrdBasedPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInOrdBasedPrice() {
        return inOrdBasedPrice;
    }

    /**
     * Sets the value of the inOrdBasedPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInOrdBasedPrice(BigDecimal value) {
        this.inOrdBasedPrice = value;
    }

    /**
     * Gets the value of the inPrice property.
     * This getter has been renamed from isInPrice() to getInPrice() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getInPrice() {
        return inPrice;
    }

    /**
     * Sets the value of the inPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInPrice(Boolean value) {
        this.inPrice = value;
    }

    /**
     * Gets the value of the inUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInUnitPrice() {
        return inUnitPrice;
    }

    /**
     * Sets the value of the inUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInUnitPrice(BigDecimal value) {
        this.inUnitPrice = value;
    }

    /**
     * Gets the value of the invoiceComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInvoiceComment() {
        return invoiceComment;
    }

    /**
     * Sets the value of the invoiceComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInvoiceComment(JAXBElement<String> value) {
        this.invoiceComment = value;
    }

    /**
     * Gets the value of the invtyUOM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInvtyUOM() {
        return invtyUOM;
    }

    /**
     * Sets the value of the invtyUOM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInvtyUOM(JAXBElement<String> value) {
        this.invtyUOM = value;
    }

    /**
     * Gets the value of the jobTypeMFG property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getJobTypeMFG() {
        return jobTypeMFG;
    }

    /**
     * Sets the value of the jobTypeMFG property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setJobTypeMFG(JAXBElement<String> value) {
        this.jobTypeMFG = value;
    }

    /**
     * Gets the value of the jobWasCreated property.
     * This getter has been renamed from isJobWasCreated() to getJobWasCreated() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getJobWasCreated() {
        return jobWasCreated;
    }

    /**
     * Sets the value of the jobWasCreated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setJobWasCreated(Boolean value) {
        this.jobWasCreated = value;
    }

    /**
     * Gets the value of the kitAllowUpdate property.
     * This getter has been renamed from isKitAllowUpdate() to getKitAllowUpdate() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getKitAllowUpdate() {
        return kitAllowUpdate;
    }

    /**
     * Sets the value of the kitAllowUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKitAllowUpdate(Boolean value) {
        this.kitAllowUpdate = value;
    }

    /**
     * Gets the value of the kitBackFlush property.
     * This getter has been renamed from isKitBackFlush() to getKitBackFlush() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getKitBackFlush() {
        return kitBackFlush;
    }

    /**
     * Sets the value of the kitBackFlush property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKitBackFlush(Boolean value) {
        this.kitBackFlush = value;
    }

    /**
     * Gets the value of the kitChangeParms property.
     * This getter has been renamed from isKitChangeParms() to getKitChangeParms() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getKitChangeParms() {
        return kitChangeParms;
    }

    /**
     * Sets the value of the kitChangeParms property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKitChangeParms(Boolean value) {
        this.kitChangeParms = value;
    }

    /**
     * Gets the value of the kitCompOrigPart property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getKitCompOrigPart() {
        return kitCompOrigPart;
    }

    /**
     * Sets the value of the kitCompOrigPart property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setKitCompOrigPart(JAXBElement<String> value) {
        this.kitCompOrigPart = value;
    }

    /**
     * Gets the value of the kitCompOrigSeq property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getKitCompOrigSeq() {
        return kitCompOrigSeq;
    }

    /**
     * Sets the value of the kitCompOrigSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setKitCompOrigSeq(Integer value) {
        this.kitCompOrigSeq = value;
    }

    /**
     * Gets the value of the kitDisable property.
     * This getter has been renamed from isKitDisable() to getKitDisable() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getKitDisable() {
        return kitDisable;
    }

    /**
     * Sets the value of the kitDisable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKitDisable(Boolean value) {
        this.kitDisable = value;
    }

    /**
     * Gets the value of the kitFlag property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getKitFlag() {
        return kitFlag;
    }

    /**
     * Sets the value of the kitFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setKitFlag(JAXBElement<String> value) {
        this.kitFlag = value;
    }

    /**
     * Gets the value of the kitFlagDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getKitFlagDescription() {
        return kitFlagDescription;
    }

    /**
     * Sets the value of the kitFlagDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setKitFlagDescription(JAXBElement<String> value) {
        this.kitFlagDescription = value;
    }

    /**
     * Gets the value of the kitOrderQtyUOM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getKitOrderQtyUOM() {
        return kitOrderQtyUOM;
    }

    /**
     * Sets the value of the kitOrderQtyUOM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setKitOrderQtyUOM(JAXBElement<String> value) {
        this.kitOrderQtyUOM = value;
    }

    /**
     * Gets the value of the kitParentLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getKitParentLine() {
        return kitParentLine;
    }

    /**
     * Sets the value of the kitParentLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setKitParentLine(Integer value) {
        this.kitParentLine = value;
    }

    /**
     * Gets the value of the kitPricing property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getKitPricing() {
        return kitPricing;
    }

    /**
     * Sets the value of the kitPricing property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setKitPricing(JAXBElement<String> value) {
        this.kitPricing = value;
    }

    /**
     * Gets the value of the kitPrintCompsInv property.
     * This getter has been renamed from isKitPrintCompsInv() to getKitPrintCompsInv() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getKitPrintCompsInv() {
        return kitPrintCompsInv;
    }

    /**
     * Sets the value of the kitPrintCompsInv property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKitPrintCompsInv(Boolean value) {
        this.kitPrintCompsInv = value;
    }

    /**
     * Gets the value of the kitPrintCompsPS property.
     * This getter has been renamed from isKitPrintCompsPS() to getKitPrintCompsPS() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getKitPrintCompsPS() {
        return kitPrintCompsPS;
    }

    /**
     * Sets the value of the kitPrintCompsPS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKitPrintCompsPS(Boolean value) {
        this.kitPrintCompsPS = value;
    }

    /**
     * Gets the value of the kitQtyPer property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getKitQtyPer() {
        return kitQtyPer;
    }

    /**
     * Sets the value of the kitQtyPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setKitQtyPer(BigDecimal value) {
        this.kitQtyPer = value;
    }

    /**
     * Gets the value of the kitShipComplete property.
     * This getter has been renamed from isKitShipComplete() to getKitShipComplete() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getKitShipComplete() {
        return kitShipComplete;
    }

    /**
     * Sets the value of the kitShipComplete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKitShipComplete(Boolean value) {
        this.kitShipComplete = value;
    }

    /**
     * Gets the value of the kitStandard property.
     * This getter has been renamed from isKitStandard() to getKitStandard() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getKitStandard() {
        return kitStandard;
    }

    /**
     * Sets the value of the kitStandard property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKitStandard(Boolean value) {
        this.kitStandard = value;
    }

    /**
     * Gets the value of the kitsLoaded property.
     * This getter has been renamed from isKitsLoaded() to getKitsLoaded() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getKitsLoaded() {
        return kitsLoaded;
    }

    /**
     * Sets the value of the kitsLoaded property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKitsLoaded(Boolean value) {
        this.kitsLoaded = value;
    }

    /**
     * Gets the value of the labCovered property.
     * This getter has been renamed from isLabCovered() to getLabCovered() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLabCovered() {
        return labCovered;
    }

    /**
     * Sets the value of the labCovered property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLabCovered(Boolean value) {
        this.labCovered = value;
    }

    /**
     * Gets the value of the laborDuration property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLaborDuration() {
        return laborDuration;
    }

    /**
     * Sets the value of the laborDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLaborDuration(Integer value) {
        this.laborDuration = value;
    }

    /**
     * Gets the value of the laborMod property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLaborMod() {
        return laborMod;
    }

    /**
     * Sets the value of the laborMod property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLaborMod(JAXBElement<String> value) {
        this.laborMod = value;
    }

    /**
     * Gets the value of the lastConfigDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getLastConfigDate() {
        return lastConfigDate;
    }

    /**
     * Sets the value of the lastConfigDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setLastConfigDate(JAXBElement<XMLGregorianCalendar> value) {
        this.lastConfigDate = value;
    }

    /**
     * Gets the value of the lastConfigTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLastConfigTime() {
        return lastConfigTime;
    }

    /**
     * Sets the value of the lastConfigTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLastConfigTime(Integer value) {
        this.lastConfigTime = value;
    }

    /**
     * Gets the value of the lastConfigUserID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLastConfigUserID() {
        return lastConfigUserID;
    }

    /**
     * Sets the value of the lastConfigUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLastConfigUserID(JAXBElement<String> value) {
        this.lastConfigUserID = value;
    }

    /**
     * Gets the value of the lessDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLessDiscount() {
        return lessDiscount;
    }

    /**
     * Sets the value of the lessDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLessDiscount(BigDecimal value) {
        this.lessDiscount = value;
    }

    /**
     * Gets the value of the lineDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLineDesc() {
        return lineDesc;
    }

    /**
     * Sets the value of the lineDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLineDesc(JAXBElement<String> value) {
        this.lineDesc = value;
    }

    /**
     * Gets the value of the lineStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLineStatus() {
        return lineStatus;
    }

    /**
     * Sets the value of the lineStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLineStatus(JAXBElement<String> value) {
        this.lineStatus = value;
    }

    /**
     * Gets the value of the lineType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLineType() {
        return lineType;
    }

    /**
     * Sets the value of the lineType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLineType(JAXBElement<String> value) {
        this.lineType = value;
    }

    /**
     * Gets the value of the linked property.
     * This getter has been renamed from isLinked() to getLinked() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLinked() {
        return linked;
    }

    /**
     * Sets the value of the linked property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLinked(Boolean value) {
        this.linked = value;
    }

    /**
     * Gets the value of the listPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getListPrice() {
        return listPrice;
    }

    /**
     * Sets the value of the listPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setListPrice(BigDecimal value) {
        this.listPrice = value;
    }

    /**
     * Gets the value of the lockDisc property.
     * This getter has been renamed from isLockDisc() to getLockDisc() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLockDisc() {
        return lockDisc;
    }

    /**
     * Sets the value of the lockDisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLockDisc(Boolean value) {
        this.lockDisc = value;
    }

    /**
     * Gets the value of the lockPrice property.
     * This getter has been renamed from isLockPrice() to getLockPrice() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLockPrice() {
        return lockPrice;
    }

    /**
     * Sets the value of the lockPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLockPrice(Boolean value) {
        this.lockPrice = value;
    }

    /**
     * Gets the value of the lockQty property.
     * This getter has been renamed from isLockQty() to getLockQty() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLockQty() {
        return lockQty;
    }

    /**
     * Sets the value of the lockQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLockQty(Boolean value) {
        this.lockQty = value;
    }

    /**
     * Gets the value of the lotNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLotNum() {
        return lotNum;
    }

    /**
     * Sets the value of the lotNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLotNum(JAXBElement<String> value) {
        this.lotNum = value;
    }

    /**
     * Gets the value of the moMsourceEst property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMOMsourceEst() {
        return moMsourceEst;
    }

    /**
     * Sets the value of the moMsourceEst property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMOMsourceEst(JAXBElement<String> value) {
        this.moMsourceEst = value;
    }

    /**
     * Gets the value of the moMsourceType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMOMsourceType() {
        return moMsourceType;
    }

    /**
     * Sets the value of the moMsourceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMOMsourceType(JAXBElement<String> value) {
        this.moMsourceType = value;
    }

    /**
     * Gets the value of the matCovered property.
     * This getter has been renamed from isMatCovered() to getMatCovered() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMatCovered() {
        return matCovered;
    }

    /**
     * Sets the value of the matCovered property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatCovered(Boolean value) {
        this.matCovered = value;
    }

    /**
     * Gets the value of the materialDuration property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaterialDuration() {
        return materialDuration;
    }

    /**
     * Sets the value of the materialDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaterialDuration(Integer value) {
        this.materialDuration = value;
    }

    /**
     * Gets the value of the materialMod property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMaterialMod() {
        return materialMod;
    }

    /**
     * Sets the value of the materialMod property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMaterialMod(JAXBElement<String> value) {
        this.materialMod = value;
    }

    /**
     * Gets the value of the miscCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMiscCharges() {
        return miscCharges;
    }

    /**
     * Sets the value of the miscCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMiscCharges(BigDecimal value) {
        this.miscCharges = value;
    }

    /**
     * Gets the value of the miscCovered property.
     * This getter has been renamed from isMiscCovered() to getMiscCovered() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMiscCovered() {
        return miscCovered;
    }

    /**
     * Sets the value of the miscCovered property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMiscCovered(Boolean value) {
        this.miscCovered = value;
    }

    /**
     * Gets the value of the miscDuration property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMiscDuration() {
        return miscDuration;
    }

    /**
     * Sets the value of the miscDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMiscDuration(Integer value) {
        this.miscDuration = value;
    }

    /**
     * Gets the value of the mktgCampaignID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMktgCampaignID() {
        return mktgCampaignID;
    }

    /**
     * Sets the value of the mktgCampaignID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMktgCampaignID(JAXBElement<String> value) {
        this.mktgCampaignID = value;
    }

    /**
     * Gets the value of the mktgCampaignIDCampDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMktgCampaignIDCampDescription() {
        return mktgCampaignIDCampDescription;
    }

    /**
     * Sets the value of the mktgCampaignIDCampDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMktgCampaignIDCampDescription(JAXBElement<String> value) {
        this.mktgCampaignIDCampDescription = value;
    }

    /**
     * Gets the value of the mktgEvntEvntDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMktgEvntEvntDescription() {
        return mktgEvntEvntDescription;
    }

    /**
     * Sets the value of the mktgEvntEvntDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMktgEvntEvntDescription(JAXBElement<String> value) {
        this.mktgEvntEvntDescription = value;
    }

    /**
     * Gets the value of the mktgEvntSeq property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMktgEvntSeq() {
        return mktgEvntSeq;
    }

    /**
     * Sets the value of the mktgEvntSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMktgEvntSeq(Integer value) {
        this.mktgEvntSeq = value;
    }

    /**
     * Gets the value of the multipleReleases property.
     * This getter has been renamed from isMultipleReleases() to getMultipleReleases() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMultipleReleases() {
        return multipleReleases;
    }

    /**
     * Sets the value of the multipleReleases property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultipleReleases(Boolean value) {
        this.multipleReleases = value;
    }

    /**
     * Gets the value of the needByDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getNeedByDate() {
        return needByDate;
    }

    /**
     * Sets the value of the needByDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setNeedByDate(JAXBElement<XMLGregorianCalendar> value) {
        this.needByDate = value;
    }

    /**
     * Gets the value of the oldOpenValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOldOpenValue() {
        return oldOpenValue;
    }

    /**
     * Sets the value of the oldOpenValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOldOpenValue(BigDecimal value) {
        this.oldOpenValue = value;
    }

    /**
     * Gets the value of the oldOurOpenQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOldOurOpenQty() {
        return oldOurOpenQty;
    }

    /**
     * Sets the value of the oldOurOpenQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOldOurOpenQty(BigDecimal value) {
        this.oldOurOpenQty = value;
    }

    /**
     * Gets the value of the oldProdCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOldProdCode() {
        return oldProdCode;
    }

    /**
     * Sets the value of the oldProdCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOldProdCode(JAXBElement<String> value) {
        this.oldProdCode = value;
    }

    /**
     * Gets the value of the oldSellingOpenQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOldSellingOpenQty() {
        return oldSellingOpenQty;
    }

    /**
     * Sets the value of the oldSellingOpenQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOldSellingOpenQty(BigDecimal value) {
        this.oldSellingOpenQty = value;
    }

    /**
     * Gets the value of the onHandQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOnHandQuantity() {
        return onHandQuantity;
    }

    /**
     * Sets the value of the onHandQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOnHandQuantity(BigDecimal value) {
        this.onHandQuantity = value;
    }

    /**
     * Gets the value of the onsite property.
     * This getter has been renamed from isOnsite() to getOnsite() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOnsite() {
        return onsite;
    }

    /**
     * Sets the value of the onsite property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOnsite(Boolean value) {
        this.onsite = value;
    }

    /**
     * Gets the value of the openLine property.
     * This getter has been renamed from isOpenLine() to getOpenLine() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOpenLine() {
        return openLine;
    }

    /**
     * Sets the value of the openLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpenLine(Boolean value) {
        this.openLine = value;
    }

    /**
     * Gets the value of the ordBasedPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrdBasedPrice() {
        return ordBasedPrice;
    }

    /**
     * Sets the value of the ordBasedPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrdBasedPrice(BigDecimal value) {
        this.ordBasedPrice = value;
    }

    /**
     * Gets the value of the orderComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderComment() {
        return orderComment;
    }

    /**
     * Sets the value of the orderComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderComment(JAXBElement<String> value) {
        this.orderComment = value;
    }

    /**
     * Gets the value of the orderLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderLine() {
        return orderLine;
    }

    /**
     * Sets the value of the orderLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderLine(Integer value) {
        this.orderLine = value;
    }

    /**
     * Gets the value of the orderNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * Sets the value of the orderNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNum(Integer value) {
        this.orderNum = value;
    }

    /**
     * Gets the value of the orderNumBTCustNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNumBTCustNum() {
        return orderNumBTCustNum;
    }

    /**
     * Sets the value of the orderNumBTCustNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNumBTCustNum(Integer value) {
        this.orderNumBTCustNum = value;
    }

    /**
     * Gets the value of the orderNumCardMemberName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderNumCardMemberName() {
        return orderNumCardMemberName;
    }

    /**
     * Sets the value of the orderNumCardMemberName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderNumCardMemberName(JAXBElement<String> value) {
        this.orderNumCardMemberName = value;
    }

    /**
     * Gets the value of the orderNumCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderNumCurrencyCode() {
        return orderNumCurrencyCode;
    }

    /**
     * Sets the value of the orderNumCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderNumCurrencyCode(JAXBElement<String> value) {
        this.orderNumCurrencyCode = value;
    }

    /**
     * Gets the value of the orderQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrderQty() {
        return orderQty;
    }

    /**
     * Sets the value of the orderQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrderQty(BigDecimal value) {
        this.orderQty = value;
    }

    /**
     * Gets the value of the origWhyNoTax property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrigWhyNoTax() {
        return origWhyNoTax;
    }

    /**
     * Sets the value of the origWhyNoTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrigWhyNoTax(JAXBElement<String> value) {
        this.origWhyNoTax = value;
    }

    /**
     * Gets the value of the overrideDiscPriceList property.
     * This getter has been renamed from isOverrideDiscPriceList() to getOverrideDiscPriceList() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOverrideDiscPriceList() {
        return overrideDiscPriceList;
    }

    /**
     * Sets the value of the overrideDiscPriceList property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverrideDiscPriceList(Boolean value) {
        this.overrideDiscPriceList = value;
    }

    /**
     * Gets the value of the overridePriceList property.
     * This getter has been renamed from isOverridePriceList() to getOverridePriceList() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOverridePriceList() {
        return overridePriceList;
    }

    /**
     * Sets the value of the overridePriceList property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverridePriceList(Boolean value) {
        this.overridePriceList = value;
    }

    /**
     * Gets the value of the overs property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOvers() {
        return overs;
    }

    /**
     * Sets the value of the overs property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOvers(BigDecimal value) {
        this.overs = value;
    }

    /**
     * Gets the value of the oversUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOversUnitPrice() {
        return oversUnitPrice;
    }

    /**
     * Sets the value of the oversUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOversUnitPrice(BigDecimal value) {
        this.oversUnitPrice = value;
    }

    /**
     * Gets the value of the poLine property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPOLine() {
        return poLine;
    }

    /**
     * Sets the value of the poLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPOLine(JAXBElement<String> value) {
        this.poLine = value;
    }

    /**
     * Gets the value of the poLineRef property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPOLineRef() {
        return poLineRef;
    }

    /**
     * Sets the value of the poLineRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPOLineRef(JAXBElement<String> value) {
        this.poLineRef = value;
    }

    /**
     * Gets the value of the partExists property.
     * This getter has been renamed from isPartExists() to getPartExists() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartExists() {
        return partExists;
    }

    /**
     * Sets the value of the partExists property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartExists(Boolean value) {
        this.partExists = value;
    }

    /**
     * Gets the value of the partNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNum() {
        return partNum;
    }

    /**
     * Sets the value of the partNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNum(JAXBElement<String> value) {
        this.partNum = value;
    }

    /**
     * Gets the value of the partNumIUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNumIUM() {
        return partNumIUM;
    }

    /**
     * Sets the value of the partNumIUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNumIUM(JAXBElement<String> value) {
        this.partNumIUM = value;
    }

    /**
     * Gets the value of the partNumPartDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNumPartDescription() {
        return partNumPartDescription;
    }

    /**
     * Sets the value of the partNumPartDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNumPartDescription(JAXBElement<String> value) {
        this.partNumPartDescription = value;
    }

    /**
     * Gets the value of the partNumPricePerCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNumPricePerCode() {
        return partNumPricePerCode;
    }

    /**
     * Sets the value of the partNumPricePerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNumPricePerCode(JAXBElement<String> value) {
        this.partNumPricePerCode = value;
    }

    /**
     * Gets the value of the partNumSalesUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNumSalesUM() {
        return partNumSalesUM;
    }

    /**
     * Sets the value of the partNumSalesUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNumSalesUM(JAXBElement<String> value) {
        this.partNumSalesUM = value;
    }

    /**
     * Gets the value of the partNumSellingFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPartNumSellingFactor() {
        return partNumSellingFactor;
    }

    /**
     * Sets the value of the partNumSellingFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPartNumSellingFactor(BigDecimal value) {
        this.partNumSellingFactor = value;
    }

    /**
     * Gets the value of the partNumTrackDimension property.
     * This getter has been renamed from isPartNumTrackDimension() to getPartNumTrackDimension() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartNumTrackDimension() {
        return partNumTrackDimension;
    }

    /**
     * Sets the value of the partNumTrackDimension property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartNumTrackDimension(Boolean value) {
        this.partNumTrackDimension = value;
    }

    /**
     * Gets the value of the partNumTrackLots property.
     * This getter has been renamed from isPartNumTrackLots() to getPartNumTrackLots() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartNumTrackLots() {
        return partNumTrackLots;
    }

    /**
     * Sets the value of the partNumTrackLots property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartNumTrackLots(Boolean value) {
        this.partNumTrackLots = value;
    }

    /**
     * Gets the value of the partNumTrackSerialNum property.
     * This getter has been renamed from isPartNumTrackSerialNum() to getPartNumTrackSerialNum() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartNumTrackSerialNum() {
        return partNumTrackSerialNum;
    }

    /**
     * Sets the value of the partNumTrackSerialNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartNumTrackSerialNum(Boolean value) {
        this.partNumTrackSerialNum = value;
    }

    /**
     * Gets the value of the partTrackDimension property.
     * This getter has been renamed from isPartTrackDimension() to getPartTrackDimension() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartTrackDimension() {
        return partTrackDimension;
    }

    /**
     * Sets the value of the partTrackDimension property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartTrackDimension(Boolean value) {
        this.partTrackDimension = value;
    }

    /**
     * Gets the value of the partTrackLots property.
     * This getter has been renamed from isPartTrackLots() to getPartTrackLots() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartTrackLots() {
        return partTrackLots;
    }

    /**
     * Sets the value of the partTrackLots property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartTrackLots(Boolean value) {
        this.partTrackLots = value;
    }

    /**
     * Gets the value of the pickListComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPickListComment() {
        return pickListComment;
    }

    /**
     * Sets the value of the pickListComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPickListComment(JAXBElement<String> value) {
        this.pickListComment = value;
    }

    /**
     * Gets the value of the planGUID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanGUID() {
        return planGUID;
    }

    /**
     * Sets the value of the planGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanGUID(JAXBElement<String> value) {
        this.planGUID = value;
    }

    /**
     * Gets the value of the planUserID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanUserID() {
        return planUserID;
    }

    /**
     * Sets the value of the planUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanUserID(JAXBElement<String> value) {
        this.planUserID = value;
    }

    /**
     * Gets the value of the prevPartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrevPartNum() {
        return prevPartNum;
    }

    /**
     * Sets the value of the prevPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrevPartNum(JAXBElement<String> value) {
        this.prevPartNum = value;
    }

    /**
     * Gets the value of the prevSellQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrevSellQty() {
        return prevSellQty;
    }

    /**
     * Sets the value of the prevSellQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrevSellQty(BigDecimal value) {
        this.prevSellQty = value;
    }

    /**
     * Gets the value of the prevXPartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrevXPartNum() {
        return prevXPartNum;
    }

    /**
     * Sets the value of the prevXPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrevXPartNum(JAXBElement<String> value) {
        this.prevXPartNum = value;
    }

    /**
     * Gets the value of the priceGroupCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPriceGroupCode() {
        return priceGroupCode;
    }

    /**
     * Sets the value of the priceGroupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPriceGroupCode(JAXBElement<String> value) {
        this.priceGroupCode = value;
    }

    /**
     * Gets the value of the priceListCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPriceListCode() {
        return priceListCode;
    }

    /**
     * Sets the value of the priceListCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPriceListCode(JAXBElement<String> value) {
        this.priceListCode = value;
    }

    /**
     * Gets the value of the priceListCodeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPriceListCodeDesc() {
        return priceListCodeDesc;
    }

    /**
     * Sets the value of the priceListCodeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPriceListCodeDesc(JAXBElement<String> value) {
        this.priceListCodeDesc = value;
    }

    /**
     * Gets the value of the pricePerCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPricePerCode() {
        return pricePerCode;
    }

    /**
     * Sets the value of the pricePerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPricePerCode(JAXBElement<String> value) {
        this.pricePerCode = value;
    }

    /**
     * Gets the value of the pricingQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPricingQty() {
        return pricingQty;
    }

    /**
     * Sets the value of the pricingQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPricingQty(BigDecimal value) {
        this.pricingQty = value;
    }

    /**
     * Gets the value of the pricingValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPricingValue() {
        return pricingValue;
    }

    /**
     * Sets the value of the pricingValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPricingValue(BigDecimal value) {
        this.pricingValue = value;
    }

    /**
     * Gets the value of the proFormaInvComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProFormaInvComment() {
        return proFormaInvComment;
    }

    /**
     * Sets the value of the proFormaInvComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProFormaInvComment(JAXBElement<String> value) {
        this.proFormaInvComment = value;
    }

    /**
     * Gets the value of the processCounterSale property.
     * This getter has been renamed from isProcessCounterSale() to getProcessCounterSale() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getProcessCounterSale() {
        return processCounterSale;
    }

    /**
     * Sets the value of the processCounterSale property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProcessCounterSale(Boolean value) {
        this.processCounterSale = value;
    }

    /**
     * Gets the value of the processQuickEntry property.
     * This getter has been renamed from isProcessQuickEntry() to getProcessQuickEntry() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getProcessQuickEntry() {
        return processQuickEntry;
    }

    /**
     * Sets the value of the processQuickEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProcessQuickEntry(Boolean value) {
        this.processQuickEntry = value;
    }

    /**
     * Gets the value of the prodCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProdCode() {
        return prodCode;
    }

    /**
     * Sets the value of the prodCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProdCode(JAXBElement<String> value) {
        this.prodCode = value;
    }

    /**
     * Gets the value of the prodCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProdCodeDescription() {
        return prodCodeDescription;
    }

    /**
     * Sets the value of the prodCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProdCodeDescription(JAXBElement<String> value) {
        this.prodCodeDescription = value;
    }

    /**
     * Gets the value of the projectID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProjectID() {
        return projectID;
    }

    /**
     * Sets the value of the projectID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProjectID(JAXBElement<String> value) {
        this.projectID = value;
    }

    /**
     * Gets the value of the projectIDDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProjectIDDescription() {
        return projectIDDescription;
    }

    /**
     * Sets the value of the projectIDDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProjectIDDescription(JAXBElement<String> value) {
        this.projectIDDescription = value;
    }

    /**
     * Gets the value of the quoteLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQuoteLine() {
        return quoteLine;
    }

    /**
     * Sets the value of the quoteLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQuoteLine(Integer value) {
        this.quoteLine = value;
    }

    /**
     * Gets the value of the quoteNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQuoteNum() {
        return quoteNum;
    }

    /**
     * Sets the value of the quoteNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQuoteNum(Integer value) {
        this.quoteNum = value;
    }

    /**
     * Gets the value of the quoteNumCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getQuoteNumCurrencyCode() {
        return quoteNumCurrencyCode;
    }

    /**
     * Sets the value of the quoteNumCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setQuoteNumCurrencyCode(JAXBElement<String> value) {
        this.quoteNumCurrencyCode = value;
    }

    /**
     * Gets the value of the quoteQtyNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQuoteQtyNum() {
        return quoteQtyNum;
    }

    /**
     * Sets the value of the quoteQtyNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQuoteQtyNum(Integer value) {
        this.quoteQtyNum = value;
    }

    /**
     * Gets the value of the rmaLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRMALine() {
        return rmaLine;
    }

    /**
     * Sets the value of the rmaLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRMALine(Integer value) {
        this.rmaLine = value;
    }

    /**
     * Gets the value of the rmaNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRMANum() {
        return rmaNum;
    }

    /**
     * Sets the value of the rmaNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRMANum(Integer value) {
        this.rmaNum = value;
    }

    /**
     * Gets the value of the reference property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReference() {
        return reference;
    }

    /**
     * Sets the value of the reference property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReference(JAXBElement<String> value) {
        this.reference = value;
    }

    /**
     * Gets the value of the relJob property.
     * This getter has been renamed from isRelJob() to getRelJob() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRelJob() {
        return relJob;
    }

    /**
     * Sets the value of the relJob property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRelJob(Boolean value) {
        this.relJob = value;
    }

    /**
     * Gets the value of the relWasRecInvoiced property.
     * This getter has been renamed from isRelWasRecInvoiced() to getRelWasRecInvoiced() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRelWasRecInvoiced() {
        return relWasRecInvoiced;
    }

    /**
     * Sets the value of the relWasRecInvoiced property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRelWasRecInvoiced(Boolean value) {
        this.relWasRecInvoiced = value;
    }

    /**
     * Gets the value of the renewalNbr property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRenewalNbr() {
        return renewalNbr;
    }

    /**
     * Sets the value of the renewalNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRenewalNbr(Integer value) {
        this.renewalNbr = value;
    }

    /**
     * Gets the value of the repRate1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRepRate1() {
        return repRate1;
    }

    /**
     * Sets the value of the repRate1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRepRate1(BigDecimal value) {
        this.repRate1 = value;
    }

    /**
     * Gets the value of the repRate2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRepRate2() {
        return repRate2;
    }

    /**
     * Sets the value of the repRate2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRepRate2(BigDecimal value) {
        this.repRate2 = value;
    }

    /**
     * Gets the value of the repRate3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRepRate3() {
        return repRate3;
    }

    /**
     * Sets the value of the repRate3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRepRate3(BigDecimal value) {
        this.repRate3 = value;
    }

    /**
     * Gets the value of the repRate4 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRepRate4() {
        return repRate4;
    }

    /**
     * Sets the value of the repRate4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRepRate4(BigDecimal value) {
        this.repRate4 = value;
    }

    /**
     * Gets the value of the repRate5 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRepRate5() {
        return repRate5;
    }

    /**
     * Sets the value of the repRate5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRepRate5(BigDecimal value) {
        this.repRate5 = value;
    }

    /**
     * Gets the value of the repSplit1 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRepSplit1() {
        return repSplit1;
    }

    /**
     * Sets the value of the repSplit1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRepSplit1(Integer value) {
        this.repSplit1 = value;
    }

    /**
     * Gets the value of the repSplit2 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRepSplit2() {
        return repSplit2;
    }

    /**
     * Sets the value of the repSplit2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRepSplit2(Integer value) {
        this.repSplit2 = value;
    }

    /**
     * Gets the value of the repSplit3 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRepSplit3() {
        return repSplit3;
    }

    /**
     * Sets the value of the repSplit3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRepSplit3(Integer value) {
        this.repSplit3 = value;
    }

    /**
     * Gets the value of the repSplit4 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRepSplit4() {
        return repSplit4;
    }

    /**
     * Sets the value of the repSplit4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRepSplit4(Integer value) {
        this.repSplit4 = value;
    }

    /**
     * Gets the value of the repSplit5 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRepSplit5() {
        return repSplit5;
    }

    /**
     * Sets the value of the repSplit5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRepSplit5(Integer value) {
        this.repSplit5 = value;
    }

    /**
     * Gets the value of the requestDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getRequestDate() {
        return requestDate;
    }

    /**
     * Sets the value of the requestDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setRequestDate(JAXBElement<XMLGregorianCalendar> value) {
        this.requestDate = value;
    }

    /**
     * Gets the value of the respMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRespMessage() {
        return respMessage;
    }

    /**
     * Sets the value of the respMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRespMessage(JAXBElement<String> value) {
        this.respMessage = value;
    }

    /**
     * Gets the value of the reverseCharge property.
     * This getter has been renamed from isReverseCharge() to getReverseCharge() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getReverseCharge() {
        return reverseCharge;
    }

    /**
     * Sets the value of the reverseCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReverseCharge(Boolean value) {
        this.reverseCharge = value;
    }

    /**
     * Gets the value of the revisionNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRevisionNum() {
        return revisionNum;
    }

    /**
     * Sets the value of the revisionNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRevisionNum(JAXBElement<String> value) {
        this.revisionNum = value;
    }

    /**
     * Gets the value of the rework property.
     * This getter has been renamed from isRework() to getRework() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRework() {
        return rework;
    }

    /**
     * Sets the value of the rework property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRework(Boolean value) {
        this.rework = value;
    }

    /**
     * Gets the value of the rpt1AdvanceBillBal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1AdvanceBillBal() {
        return rpt1AdvanceBillBal;
    }

    /**
     * Sets the value of the rpt1AdvanceBillBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1AdvanceBillBal(BigDecimal value) {
        this.rpt1AdvanceBillBal = value;
    }

    /**
     * Gets the value of the rpt1Discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1Discount() {
        return rpt1Discount;
    }

    /**
     * Sets the value of the rpt1Discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1Discount(BigDecimal value) {
        this.rpt1Discount = value;
    }

    /**
     * Gets the value of the rpt1DspDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1DspDiscount() {
        return rpt1DspDiscount;
    }

    /**
     * Sets the value of the rpt1DspDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1DspDiscount(BigDecimal value) {
        this.rpt1DspDiscount = value;
    }

    /**
     * Gets the value of the rpt1DspUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1DspUnitPrice() {
        return rpt1DspUnitPrice;
    }

    /**
     * Sets the value of the rpt1DspUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1DspUnitPrice(BigDecimal value) {
        this.rpt1DspUnitPrice = value;
    }

    /**
     * Gets the value of the rpt1ExtPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1ExtPrice() {
        return rpt1ExtPrice;
    }

    /**
     * Sets the value of the rpt1ExtPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1ExtPrice(BigDecimal value) {
        this.rpt1ExtPrice = value;
    }

    /**
     * Gets the value of the rpt1ExtPriceDtl property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1ExtPriceDtl() {
        return rpt1ExtPriceDtl;
    }

    /**
     * Sets the value of the rpt1ExtPriceDtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1ExtPriceDtl(BigDecimal value) {
        this.rpt1ExtPriceDtl = value;
    }

    /**
     * Gets the value of the rpt1InDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1InDiscount() {
        return rpt1InDiscount;
    }

    /**
     * Sets the value of the rpt1InDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1InDiscount(BigDecimal value) {
        this.rpt1InDiscount = value;
    }

    /**
     * Gets the value of the rpt1InExtPriceDtl property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1InExtPriceDtl() {
        return rpt1InExtPriceDtl;
    }

    /**
     * Sets the value of the rpt1InExtPriceDtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1InExtPriceDtl(BigDecimal value) {
        this.rpt1InExtPriceDtl = value;
    }

    /**
     * Gets the value of the rpt1InListPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1InListPrice() {
        return rpt1InListPrice;
    }

    /**
     * Sets the value of the rpt1InListPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1InListPrice(BigDecimal value) {
        this.rpt1InListPrice = value;
    }

    /**
     * Gets the value of the rpt1InMiscCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1InMiscCharges() {
        return rpt1InMiscCharges;
    }

    /**
     * Sets the value of the rpt1InMiscCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1InMiscCharges(BigDecimal value) {
        this.rpt1InMiscCharges = value;
    }

    /**
     * Gets the value of the rpt1InOrdBasedPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1InOrdBasedPrice() {
        return rpt1InOrdBasedPrice;
    }

    /**
     * Sets the value of the rpt1InOrdBasedPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1InOrdBasedPrice(BigDecimal value) {
        this.rpt1InOrdBasedPrice = value;
    }

    /**
     * Gets the value of the rpt1InUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1InUnitPrice() {
        return rpt1InUnitPrice;
    }

    /**
     * Sets the value of the rpt1InUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1InUnitPrice(BigDecimal value) {
        this.rpt1InUnitPrice = value;
    }

    /**
     * Gets the value of the rpt1LessDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1LessDiscount() {
        return rpt1LessDiscount;
    }

    /**
     * Sets the value of the rpt1LessDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1LessDiscount(BigDecimal value) {
        this.rpt1LessDiscount = value;
    }

    /**
     * Gets the value of the rpt1ListPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1ListPrice() {
        return rpt1ListPrice;
    }

    /**
     * Sets the value of the rpt1ListPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1ListPrice(BigDecimal value) {
        this.rpt1ListPrice = value;
    }

    /**
     * Gets the value of the rpt1MiscCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1MiscCharges() {
        return rpt1MiscCharges;
    }

    /**
     * Sets the value of the rpt1MiscCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1MiscCharges(BigDecimal value) {
        this.rpt1MiscCharges = value;
    }

    /**
     * Gets the value of the rpt1OrdBasedPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1OrdBasedPrice() {
        return rpt1OrdBasedPrice;
    }

    /**
     * Sets the value of the rpt1OrdBasedPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1OrdBasedPrice(BigDecimal value) {
        this.rpt1OrdBasedPrice = value;
    }

    /**
     * Gets the value of the rpt1TaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TaxAmt() {
        return rpt1TaxAmt;
    }

    /**
     * Sets the value of the rpt1TaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TaxAmt(BigDecimal value) {
        this.rpt1TaxAmt = value;
    }

    /**
     * Gets the value of the rpt1TotalPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TotalPrice() {
        return rpt1TotalPrice;
    }

    /**
     * Sets the value of the rpt1TotalPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TotalPrice(BigDecimal value) {
        this.rpt1TotalPrice = value;
    }

    /**
     * Gets the value of the rpt1UnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1UnitPrice() {
        return rpt1UnitPrice;
    }

    /**
     * Sets the value of the rpt1UnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1UnitPrice(BigDecimal value) {
        this.rpt1UnitPrice = value;
    }

    /**
     * Gets the value of the rpt2AdvanceBillBal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2AdvanceBillBal() {
        return rpt2AdvanceBillBal;
    }

    /**
     * Sets the value of the rpt2AdvanceBillBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2AdvanceBillBal(BigDecimal value) {
        this.rpt2AdvanceBillBal = value;
    }

    /**
     * Gets the value of the rpt2Discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2Discount() {
        return rpt2Discount;
    }

    /**
     * Sets the value of the rpt2Discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2Discount(BigDecimal value) {
        this.rpt2Discount = value;
    }

    /**
     * Gets the value of the rpt2DspDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2DspDiscount() {
        return rpt2DspDiscount;
    }

    /**
     * Sets the value of the rpt2DspDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2DspDiscount(BigDecimal value) {
        this.rpt2DspDiscount = value;
    }

    /**
     * Gets the value of the rpt2DspUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2DspUnitPrice() {
        return rpt2DspUnitPrice;
    }

    /**
     * Sets the value of the rpt2DspUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2DspUnitPrice(BigDecimal value) {
        this.rpt2DspUnitPrice = value;
    }

    /**
     * Gets the value of the rpt2ExtPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2ExtPrice() {
        return rpt2ExtPrice;
    }

    /**
     * Sets the value of the rpt2ExtPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2ExtPrice(BigDecimal value) {
        this.rpt2ExtPrice = value;
    }

    /**
     * Gets the value of the rpt2ExtPriceDtl property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2ExtPriceDtl() {
        return rpt2ExtPriceDtl;
    }

    /**
     * Sets the value of the rpt2ExtPriceDtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2ExtPriceDtl(BigDecimal value) {
        this.rpt2ExtPriceDtl = value;
    }

    /**
     * Gets the value of the rpt2InDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2InDiscount() {
        return rpt2InDiscount;
    }

    /**
     * Sets the value of the rpt2InDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2InDiscount(BigDecimal value) {
        this.rpt2InDiscount = value;
    }

    /**
     * Gets the value of the rpt2InExtPriceDtl property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2InExtPriceDtl() {
        return rpt2InExtPriceDtl;
    }

    /**
     * Sets the value of the rpt2InExtPriceDtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2InExtPriceDtl(BigDecimal value) {
        this.rpt2InExtPriceDtl = value;
    }

    /**
     * Gets the value of the rpt2InListPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2InListPrice() {
        return rpt2InListPrice;
    }

    /**
     * Sets the value of the rpt2InListPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2InListPrice(BigDecimal value) {
        this.rpt2InListPrice = value;
    }

    /**
     * Gets the value of the rpt2InMiscCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2InMiscCharges() {
        return rpt2InMiscCharges;
    }

    /**
     * Sets the value of the rpt2InMiscCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2InMiscCharges(BigDecimal value) {
        this.rpt2InMiscCharges = value;
    }

    /**
     * Gets the value of the rpt2InOrdBasedPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2InOrdBasedPrice() {
        return rpt2InOrdBasedPrice;
    }

    /**
     * Sets the value of the rpt2InOrdBasedPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2InOrdBasedPrice(BigDecimal value) {
        this.rpt2InOrdBasedPrice = value;
    }

    /**
     * Gets the value of the rpt2InUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2InUnitPrice() {
        return rpt2InUnitPrice;
    }

    /**
     * Sets the value of the rpt2InUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2InUnitPrice(BigDecimal value) {
        this.rpt2InUnitPrice = value;
    }

    /**
     * Gets the value of the rpt2LessDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2LessDiscount() {
        return rpt2LessDiscount;
    }

    /**
     * Sets the value of the rpt2LessDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2LessDiscount(BigDecimal value) {
        this.rpt2LessDiscount = value;
    }

    /**
     * Gets the value of the rpt2ListPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2ListPrice() {
        return rpt2ListPrice;
    }

    /**
     * Sets the value of the rpt2ListPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2ListPrice(BigDecimal value) {
        this.rpt2ListPrice = value;
    }

    /**
     * Gets the value of the rpt2MiscCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2MiscCharges() {
        return rpt2MiscCharges;
    }

    /**
     * Sets the value of the rpt2MiscCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2MiscCharges(BigDecimal value) {
        this.rpt2MiscCharges = value;
    }

    /**
     * Gets the value of the rpt2OrdBasedPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2OrdBasedPrice() {
        return rpt2OrdBasedPrice;
    }

    /**
     * Sets the value of the rpt2OrdBasedPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2OrdBasedPrice(BigDecimal value) {
        this.rpt2OrdBasedPrice = value;
    }

    /**
     * Gets the value of the rpt2TaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TaxAmt() {
        return rpt2TaxAmt;
    }

    /**
     * Sets the value of the rpt2TaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TaxAmt(BigDecimal value) {
        this.rpt2TaxAmt = value;
    }

    /**
     * Gets the value of the rpt2TotalPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TotalPrice() {
        return rpt2TotalPrice;
    }

    /**
     * Sets the value of the rpt2TotalPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TotalPrice(BigDecimal value) {
        this.rpt2TotalPrice = value;
    }

    /**
     * Gets the value of the rpt2UnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2UnitPrice() {
        return rpt2UnitPrice;
    }

    /**
     * Sets the value of the rpt2UnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2UnitPrice(BigDecimal value) {
        this.rpt2UnitPrice = value;
    }

    /**
     * Gets the value of the rpt3AdvanceBillBal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3AdvanceBillBal() {
        return rpt3AdvanceBillBal;
    }

    /**
     * Sets the value of the rpt3AdvanceBillBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3AdvanceBillBal(BigDecimal value) {
        this.rpt3AdvanceBillBal = value;
    }

    /**
     * Gets the value of the rpt3Discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3Discount() {
        return rpt3Discount;
    }

    /**
     * Sets the value of the rpt3Discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3Discount(BigDecimal value) {
        this.rpt3Discount = value;
    }

    /**
     * Gets the value of the rpt3DspDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3DspDiscount() {
        return rpt3DspDiscount;
    }

    /**
     * Sets the value of the rpt3DspDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3DspDiscount(BigDecimal value) {
        this.rpt3DspDiscount = value;
    }

    /**
     * Gets the value of the rpt3DspUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3DspUnitPrice() {
        return rpt3DspUnitPrice;
    }

    /**
     * Sets the value of the rpt3DspUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3DspUnitPrice(BigDecimal value) {
        this.rpt3DspUnitPrice = value;
    }

    /**
     * Gets the value of the rpt3ExtPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3ExtPrice() {
        return rpt3ExtPrice;
    }

    /**
     * Sets the value of the rpt3ExtPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3ExtPrice(BigDecimal value) {
        this.rpt3ExtPrice = value;
    }

    /**
     * Gets the value of the rpt3ExtPriceDtl property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3ExtPriceDtl() {
        return rpt3ExtPriceDtl;
    }

    /**
     * Sets the value of the rpt3ExtPriceDtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3ExtPriceDtl(BigDecimal value) {
        this.rpt3ExtPriceDtl = value;
    }

    /**
     * Gets the value of the rpt3InDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3InDiscount() {
        return rpt3InDiscount;
    }

    /**
     * Sets the value of the rpt3InDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3InDiscount(BigDecimal value) {
        this.rpt3InDiscount = value;
    }

    /**
     * Gets the value of the rpt3InExtPriceDtl property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3InExtPriceDtl() {
        return rpt3InExtPriceDtl;
    }

    /**
     * Sets the value of the rpt3InExtPriceDtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3InExtPriceDtl(BigDecimal value) {
        this.rpt3InExtPriceDtl = value;
    }

    /**
     * Gets the value of the rpt3InListPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3InListPrice() {
        return rpt3InListPrice;
    }

    /**
     * Sets the value of the rpt3InListPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3InListPrice(BigDecimal value) {
        this.rpt3InListPrice = value;
    }

    /**
     * Gets the value of the rpt3InMiscCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3InMiscCharges() {
        return rpt3InMiscCharges;
    }

    /**
     * Sets the value of the rpt3InMiscCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3InMiscCharges(BigDecimal value) {
        this.rpt3InMiscCharges = value;
    }

    /**
     * Gets the value of the rpt3InOrdBasedPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3InOrdBasedPrice() {
        return rpt3InOrdBasedPrice;
    }

    /**
     * Sets the value of the rpt3InOrdBasedPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3InOrdBasedPrice(BigDecimal value) {
        this.rpt3InOrdBasedPrice = value;
    }

    /**
     * Gets the value of the rpt3InUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3InUnitPrice() {
        return rpt3InUnitPrice;
    }

    /**
     * Sets the value of the rpt3InUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3InUnitPrice(BigDecimal value) {
        this.rpt3InUnitPrice = value;
    }

    /**
     * Gets the value of the rpt3LessDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3LessDiscount() {
        return rpt3LessDiscount;
    }

    /**
     * Sets the value of the rpt3LessDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3LessDiscount(BigDecimal value) {
        this.rpt3LessDiscount = value;
    }

    /**
     * Gets the value of the rpt3ListPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3ListPrice() {
        return rpt3ListPrice;
    }

    /**
     * Sets the value of the rpt3ListPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3ListPrice(BigDecimal value) {
        this.rpt3ListPrice = value;
    }

    /**
     * Gets the value of the rpt3MiscCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3MiscCharges() {
        return rpt3MiscCharges;
    }

    /**
     * Sets the value of the rpt3MiscCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3MiscCharges(BigDecimal value) {
        this.rpt3MiscCharges = value;
    }

    /**
     * Gets the value of the rpt3OrdBasedPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3OrdBasedPrice() {
        return rpt3OrdBasedPrice;
    }

    /**
     * Sets the value of the rpt3OrdBasedPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3OrdBasedPrice(BigDecimal value) {
        this.rpt3OrdBasedPrice = value;
    }

    /**
     * Gets the value of the rpt3TaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TaxAmt() {
        return rpt3TaxAmt;
    }

    /**
     * Sets the value of the rpt3TaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TaxAmt(BigDecimal value) {
        this.rpt3TaxAmt = value;
    }

    /**
     * Gets the value of the rpt3TotalPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TotalPrice() {
        return rpt3TotalPrice;
    }

    /**
     * Sets the value of the rpt3TotalPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TotalPrice(BigDecimal value) {
        this.rpt3TotalPrice = value;
    }

    /**
     * Gets the value of the rpt3UnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3UnitPrice() {
        return rpt3UnitPrice;
    }

    /**
     * Sets the value of the rpt3UnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3UnitPrice(BigDecimal value) {
        this.rpt3UnitPrice = value;
    }

    /**
     * Gets the value of the salesCatID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesCatID() {
        return salesCatID;
    }

    /**
     * Sets the value of the salesCatID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesCatID(JAXBElement<String> value) {
        this.salesCatID = value;
    }

    /**
     * Gets the value of the salesCatIDDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesCatIDDescription() {
        return salesCatIDDescription;
    }

    /**
     * Sets the value of the salesCatIDDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesCatIDDescription(JAXBElement<String> value) {
        this.salesCatIDDescription = value;
    }

    /**
     * Gets the value of the salesRepName1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepName1() {
        return salesRepName1;
    }

    /**
     * Sets the value of the salesRepName1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepName1(JAXBElement<String> value) {
        this.salesRepName1 = value;
    }

    /**
     * Gets the value of the salesRepName2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepName2() {
        return salesRepName2;
    }

    /**
     * Sets the value of the salesRepName2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepName2(JAXBElement<String> value) {
        this.salesRepName2 = value;
    }

    /**
     * Gets the value of the salesRepName3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepName3() {
        return salesRepName3;
    }

    /**
     * Sets the value of the salesRepName3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepName3(JAXBElement<String> value) {
        this.salesRepName3 = value;
    }

    /**
     * Gets the value of the salesRepName4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepName4() {
        return salesRepName4;
    }

    /**
     * Sets the value of the salesRepName4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepName4(JAXBElement<String> value) {
        this.salesRepName4 = value;
    }

    /**
     * Gets the value of the salesRepName5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepName5() {
        return salesRepName5;
    }

    /**
     * Sets the value of the salesRepName5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepName5(JAXBElement<String> value) {
        this.salesRepName5 = value;
    }

    /**
     * Gets the value of the salesUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesUM() {
        return salesUM;
    }

    /**
     * Sets the value of the salesUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesUM(JAXBElement<String> value) {
        this.salesUM = value;
    }

    /**
     * Gets the value of the schedJob property.
     * This getter has been renamed from isSchedJob() to getSchedJob() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSchedJob() {
        return schedJob;
    }

    /**
     * Sets the value of the schedJob property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSchedJob(Boolean value) {
        this.schedJob = value;
    }

    /**
     * Gets the value of the sellingFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSellingFactor() {
        return sellingFactor;
    }

    /**
     * Sets the value of the sellingFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSellingFactor(BigDecimal value) {
        this.sellingFactor = value;
    }

    /**
     * Gets the value of the sellingFactorDirection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSellingFactorDirection() {
        return sellingFactorDirection;
    }

    /**
     * Sets the value of the sellingFactorDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSellingFactorDirection(JAXBElement<String> value) {
        this.sellingFactorDirection = value;
    }

    /**
     * Gets the value of the sellingQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSellingQuantity() {
        return sellingQuantity;
    }

    /**
     * Sets the value of the sellingQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSellingQuantity(BigDecimal value) {
        this.sellingQuantity = value;
    }

    /**
     * Gets the value of the shipComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipComment() {
        return shipComment;
    }

    /**
     * Sets the value of the shipComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipComment(JAXBElement<String> value) {
        this.shipComment = value;
    }

    /**
     * Gets the value of the shipLineComplete property.
     * This getter has been renamed from isShipLineComplete() to getShipLineComplete() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getShipLineComplete() {
        return shipLineComplete;
    }

    /**
     * Sets the value of the shipLineComplete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShipLineComplete(Boolean value) {
        this.shipLineComplete = value;
    }

    /**
     * Gets the value of the smartString property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSmartString() {
        return smartString;
    }

    /**
     * Sets the value of the smartString property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSmartString(JAXBElement<String> value) {
        this.smartString = value;
    }

    /**
     * Gets the value of the smartStringProcessed property.
     * This getter has been renamed from isSmartStringProcessed() to getSmartStringProcessed() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSmartStringProcessed() {
        return smartStringProcessed;
    }

    /**
     * Sets the value of the smartStringProcessed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSmartStringProcessed(Boolean value) {
        this.smartStringProcessed = value;
    }

    /**
     * Gets the value of the sysRevID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSysRevID() {
        return sysRevID;
    }

    /**
     * Sets the value of the sysRevID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSysRevID(Long value) {
        this.sysRevID = value;
    }

    /**
     * Gets the value of the tmBilling property.
     * This getter has been renamed from isTMBilling() to getTMBilling() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getTMBilling() {
        return tmBilling;
    }

    /**
     * Sets the value of the tmBilling property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTMBilling(Boolean value) {
        this.tmBilling = value;
    }

    /**
     * Gets the value of the taxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTaxAmt() {
        return taxAmt;
    }

    /**
     * Sets the value of the taxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxAmt(BigDecimal value) {
        this.taxAmt = value;
    }

    /**
     * Gets the value of the taxCatID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxCatID() {
        return taxCatID;
    }

    /**
     * Sets the value of the taxCatID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxCatID(JAXBElement<String> value) {
        this.taxCatID = value;
    }

    /**
     * Gets the value of the taxCatIDDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxCatIDDescription() {
        return taxCatIDDescription;
    }

    /**
     * Sets the value of the taxCatIDDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxCatIDDescription(JAXBElement<String> value) {
        this.taxCatIDDescription = value;
    }

    /**
     * Gets the value of the thisOrderInvtyQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getThisOrderInvtyQty() {
        return thisOrderInvtyQty;
    }

    /**
     * Sets the value of the thisOrderInvtyQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setThisOrderInvtyQty(BigDecimal value) {
        this.thisOrderInvtyQty = value;
    }

    /**
     * Gets the value of the totalPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    /**
     * Sets the value of the totalPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalPrice(BigDecimal value) {
        this.totalPrice = value;
    }

    /**
     * Gets the value of the totalReleases property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalReleases() {
        return totalReleases;
    }

    /**
     * Sets the value of the totalReleases property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalReleases(Integer value) {
        this.totalReleases = value;
    }

    /**
     * Gets the value of the totalShipped property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalShipped() {
        return totalShipped;
    }

    /**
     * Sets the value of the totalShipped property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalShipped(BigDecimal value) {
        this.totalShipped = value;
    }

    /**
     * Gets the value of the unders property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUnders() {
        return unders;
    }

    /**
     * Sets the value of the unders property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUnders(BigDecimal value) {
        this.unders = value;
    }

    /**
     * Gets the value of the undersPct property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUndersPct() {
        return undersPct;
    }

    /**
     * Sets the value of the undersPct property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUndersPct(BigDecimal value) {
        this.undersPct = value;
    }

    /**
     * Gets the value of the unitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * Sets the value of the unitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUnitPrice(BigDecimal value) {
        this.unitPrice = value;
    }

    /**
     * Gets the value of the voidLine property.
     * This getter has been renamed from isVoidLine() to getVoidLine() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getVoidLine() {
        return voidLine;
    }

    /**
     * Sets the value of the voidLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVoidLine(Boolean value) {
        this.voidLine = value;
    }

    /**
     * Gets the value of the warehouseCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWarehouseCode() {
        return warehouseCode;
    }

    /**
     * Sets the value of the warehouseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWarehouseCode(JAXBElement<String> value) {
        this.warehouseCode = value;
    }

    /**
     * Gets the value of the warehouseDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWarehouseDesc() {
        return warehouseDesc;
    }

    /**
     * Sets the value of the warehouseDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWarehouseDesc(JAXBElement<String> value) {
        this.warehouseDesc = value;
    }

    /**
     * Gets the value of the warranty property.
     * This getter has been renamed from isWarranty() to getWarranty() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getWarranty() {
        return warranty;
    }

    /**
     * Sets the value of the warranty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWarranty(Boolean value) {
        this.warranty = value;
    }

    /**
     * Gets the value of the warrantyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWarrantyCode() {
        return warrantyCode;
    }

    /**
     * Sets the value of the warrantyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWarrantyCode(JAXBElement<String> value) {
        this.warrantyCode = value;
    }

    /**
     * Gets the value of the warrantyCodeWarrDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWarrantyCodeWarrDescription() {
        return warrantyCodeWarrDescription;
    }

    /**
     * Sets the value of the warrantyCodeWarrDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWarrantyCodeWarrDescription(JAXBElement<String> value) {
        this.warrantyCodeWarrDescription = value;
    }

    /**
     * Gets the value of the warrantyComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWarrantyComment() {
        return warrantyComment;
    }

    /**
     * Sets the value of the warrantyComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWarrantyComment(JAXBElement<String> value) {
        this.warrantyComment = value;
    }

    /**
     * Gets the value of the xPartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getXPartNum() {
        return xPartNum;
    }

    /**
     * Sets the value of the xPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setXPartNum(JAXBElement<String> value) {
        this.xPartNum = value;
    }

    /**
     * Gets the value of the xRevisionNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getXRevisionNum() {
        return xRevisionNum;
    }

    /**
     * Sets the value of the xRevisionNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setXRevisionNum(JAXBElement<String> value) {
        this.xRevisionNum = value;
    }

}
