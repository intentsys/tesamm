
package com.epicor.erp.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SelectSerialNumbersParamsTableset complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SelectSerialNumbersParamsTableset">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceTableset">
 *       &lt;sequence>
 *         &lt;element name="SelectSerialNumbersParams" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SelectSerialNumbersParamsTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SelectSerialNumbersParamsTableset", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "selectSerialNumbersParams"
})
public class SelectSerialNumbersParamsTableset
    extends IceTableset
{

    @XmlElementRef(name = "SelectSerialNumbersParams", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<SelectSerialNumbersParamsTable> selectSerialNumbersParams;

    /**
     * Gets the value of the selectSerialNumbersParams property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SelectSerialNumbersParamsTable }{@code >}
     *     
     */
    public JAXBElement<SelectSerialNumbersParamsTable> getSelectSerialNumbersParams() {
        return selectSerialNumbersParams;
    }

    /**
     * Sets the value of the selectSerialNumbersParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SelectSerialNumbersParamsTable }{@code >}
     *     
     */
    public void setSelectSerialNumbersParams(JAXBElement<SelectSerialNumbersParamsTable> value) {
        this.selectSerialNumbersParams = value;
    }

}
