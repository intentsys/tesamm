
package com.epicor.erp.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SelectedSerialNumbersRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SelectedSerialNumbersRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceRow">
 *       &lt;sequence>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Deselected" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="KBLbrAction" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="KBLbrActionDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="KitWhseList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PassedInspection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PreDeselected" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PreventDeselect" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="RawSerialNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReasonCodeDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReasonCodeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNBaseNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNMask" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Scrapped" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ScrappedReasonCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SourceRowID" type="{http://schemas.microsoft.com/2003/10/Serialization/}guid" minOccurs="0"/>
 *         &lt;element name="TransType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Voided" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="XRefPartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="XRefPartType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="poLinkValues" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SelectedSerialNumbersRow", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "company",
    "deselected",
    "kbLbrAction",
    "kbLbrActionDesc",
    "kitWhseList",
    "partNum",
    "passedInspection",
    "preDeselected",
    "preventDeselect",
    "rawSerialNum",
    "reasonCodeDesc",
    "reasonCodeType",
    "reference",
    "snBaseNumber",
    "snMask",
    "snPrefix",
    "scrapped",
    "scrappedReasonCode",
    "serialNumber",
    "sourceRowID",
    "transType",
    "voided",
    "xRefPartNum",
    "xRefPartType",
    "poLinkValues"
})
public class SelectedSerialNumbersRow
    extends IceRow
{

    @XmlElementRef(name = "Company", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> company;
    @XmlElement(name = "Deselected")
    protected Boolean deselected;
    @XmlElement(name = "KBLbrAction")
    protected Integer kbLbrAction;
    @XmlElementRef(name = "KBLbrActionDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> kbLbrActionDesc;
    @XmlElementRef(name = "KitWhseList", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> kitWhseList;
    @XmlElementRef(name = "PartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNum;
    @XmlElement(name = "PassedInspection")
    protected Boolean passedInspection;
    @XmlElement(name = "PreDeselected")
    protected Boolean preDeselected;
    @XmlElement(name = "PreventDeselect")
    protected Boolean preventDeselect;
    @XmlElementRef(name = "RawSerialNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rawSerialNum;
    @XmlElementRef(name = "ReasonCodeDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reasonCodeDesc;
    @XmlElementRef(name = "ReasonCodeType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reasonCodeType;
    @XmlElementRef(name = "Reference", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reference;
    @XmlElementRef(name = "SNBaseNumber", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> snBaseNumber;
    @XmlElementRef(name = "SNMask", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> snMask;
    @XmlElementRef(name = "SNPrefix", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> snPrefix;
    @XmlElement(name = "Scrapped")
    protected Boolean scrapped;
    @XmlElementRef(name = "ScrappedReasonCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> scrappedReasonCode;
    @XmlElementRef(name = "SerialNumber", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serialNumber;
    @XmlElement(name = "SourceRowID")
    protected String sourceRowID;
    @XmlElementRef(name = "TransType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transType;
    @XmlElement(name = "Voided")
    protected Boolean voided;
    @XmlElementRef(name = "XRefPartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> xRefPartNum;
    @XmlElementRef(name = "XRefPartType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> xRefPartType;
    @XmlElementRef(name = "poLinkValues", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> poLinkValues;

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompany(JAXBElement<String> value) {
        this.company = value;
    }

    /**
     * Gets the value of the deselected property.
     * This getter has been renamed from isDeselected() to getDeselected() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDeselected() {
        return deselected;
    }

    /**
     * Sets the value of the deselected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeselected(Boolean value) {
        this.deselected = value;
    }

    /**
     * Gets the value of the kbLbrAction property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getKBLbrAction() {
        return kbLbrAction;
    }

    /**
     * Sets the value of the kbLbrAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setKBLbrAction(Integer value) {
        this.kbLbrAction = value;
    }

    /**
     * Gets the value of the kbLbrActionDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getKBLbrActionDesc() {
        return kbLbrActionDesc;
    }

    /**
     * Sets the value of the kbLbrActionDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setKBLbrActionDesc(JAXBElement<String> value) {
        this.kbLbrActionDesc = value;
    }

    /**
     * Gets the value of the kitWhseList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getKitWhseList() {
        return kitWhseList;
    }

    /**
     * Sets the value of the kitWhseList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setKitWhseList(JAXBElement<String> value) {
        this.kitWhseList = value;
    }

    /**
     * Gets the value of the partNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNum() {
        return partNum;
    }

    /**
     * Sets the value of the partNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNum(JAXBElement<String> value) {
        this.partNum = value;
    }

    /**
     * Gets the value of the passedInspection property.
     * This getter has been renamed from isPassedInspection() to getPassedInspection() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPassedInspection() {
        return passedInspection;
    }

    /**
     * Sets the value of the passedInspection property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPassedInspection(Boolean value) {
        this.passedInspection = value;
    }

    /**
     * Gets the value of the preDeselected property.
     * This getter has been renamed from isPreDeselected() to getPreDeselected() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPreDeselected() {
        return preDeselected;
    }

    /**
     * Sets the value of the preDeselected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreDeselected(Boolean value) {
        this.preDeselected = value;
    }

    /**
     * Gets the value of the preventDeselect property.
     * This getter has been renamed from isPreventDeselect() to getPreventDeselect() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPreventDeselect() {
        return preventDeselect;
    }

    /**
     * Sets the value of the preventDeselect property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreventDeselect(Boolean value) {
        this.preventDeselect = value;
    }

    /**
     * Gets the value of the rawSerialNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRawSerialNum() {
        return rawSerialNum;
    }

    /**
     * Sets the value of the rawSerialNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRawSerialNum(JAXBElement<String> value) {
        this.rawSerialNum = value;
    }

    /**
     * Gets the value of the reasonCodeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReasonCodeDesc() {
        return reasonCodeDesc;
    }

    /**
     * Sets the value of the reasonCodeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReasonCodeDesc(JAXBElement<String> value) {
        this.reasonCodeDesc = value;
    }

    /**
     * Gets the value of the reasonCodeType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReasonCodeType() {
        return reasonCodeType;
    }

    /**
     * Sets the value of the reasonCodeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReasonCodeType(JAXBElement<String> value) {
        this.reasonCodeType = value;
    }

    /**
     * Gets the value of the reference property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReference() {
        return reference;
    }

    /**
     * Sets the value of the reference property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReference(JAXBElement<String> value) {
        this.reference = value;
    }

    /**
     * Gets the value of the snBaseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSNBaseNumber() {
        return snBaseNumber;
    }

    /**
     * Sets the value of the snBaseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSNBaseNumber(JAXBElement<String> value) {
        this.snBaseNumber = value;
    }

    /**
     * Gets the value of the snMask property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSNMask() {
        return snMask;
    }

    /**
     * Sets the value of the snMask property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSNMask(JAXBElement<String> value) {
        this.snMask = value;
    }

    /**
     * Gets the value of the snPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSNPrefix() {
        return snPrefix;
    }

    /**
     * Sets the value of the snPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSNPrefix(JAXBElement<String> value) {
        this.snPrefix = value;
    }

    /**
     * Gets the value of the scrapped property.
     * This getter has been renamed from isScrapped() to getScrapped() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getScrapped() {
        return scrapped;
    }

    /**
     * Sets the value of the scrapped property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setScrapped(Boolean value) {
        this.scrapped = value;
    }

    /**
     * Gets the value of the scrappedReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getScrappedReasonCode() {
        return scrappedReasonCode;
    }

    /**
     * Sets the value of the scrappedReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setScrappedReasonCode(JAXBElement<String> value) {
        this.scrappedReasonCode = value;
    }

    /**
     * Gets the value of the serialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets the value of the serialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSerialNumber(JAXBElement<String> value) {
        this.serialNumber = value;
    }

    /**
     * Gets the value of the sourceRowID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceRowID() {
        return sourceRowID;
    }

    /**
     * Sets the value of the sourceRowID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceRowID(String value) {
        this.sourceRowID = value;
    }

    /**
     * Gets the value of the transType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransType() {
        return transType;
    }

    /**
     * Sets the value of the transType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransType(JAXBElement<String> value) {
        this.transType = value;
    }

    /**
     * Gets the value of the voided property.
     * This getter has been renamed from isVoided() to getVoided() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getVoided() {
        return voided;
    }

    /**
     * Sets the value of the voided property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVoided(Boolean value) {
        this.voided = value;
    }

    /**
     * Gets the value of the xRefPartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getXRefPartNum() {
        return xRefPartNum;
    }

    /**
     * Sets the value of the xRefPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setXRefPartNum(JAXBElement<String> value) {
        this.xRefPartNum = value;
    }

    /**
     * Gets the value of the xRefPartType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getXRefPartType() {
        return xRefPartType;
    }

    /**
     * Sets the value of the xRefPartType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setXRefPartType(JAXBElement<String> value) {
        this.xRefPartType = value;
    }

    /**
     * Gets the value of the poLinkValues property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPoLinkValues() {
        return poLinkValues;
    }

    /**
     * Sets the value of the poLinkValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPoLinkValues(JAXBElement<String> value) {
        this.poLinkValues = value;
    }

}
