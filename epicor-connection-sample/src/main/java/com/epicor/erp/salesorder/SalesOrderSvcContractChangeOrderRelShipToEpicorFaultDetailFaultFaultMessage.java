
package com.epicor.erp.salesorder;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.5.1
 * 2016-01-18T18:16:30.493+08:00
 * Generated source version: 2.5.1
 */

@WebFault(name = "EpicorFaultDetail", targetNamespace = "http://schemas.datacontract.org/2004/07/Ice.Common")
public class SalesOrderSvcContractChangeOrderRelShipToEpicorFaultDetailFaultFaultMessage extends Exception {
    
    private com.epicor.erp.salesorder.EpicorFaultDetail epicorFaultDetail;

    public SalesOrderSvcContractChangeOrderRelShipToEpicorFaultDetailFaultFaultMessage() {
        super();
    }
    
    public SalesOrderSvcContractChangeOrderRelShipToEpicorFaultDetailFaultFaultMessage(String message) {
        super(message);
    }
    
    public SalesOrderSvcContractChangeOrderRelShipToEpicorFaultDetailFaultFaultMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public SalesOrderSvcContractChangeOrderRelShipToEpicorFaultDetailFaultFaultMessage(String message, com.epicor.erp.salesorder.EpicorFaultDetail epicorFaultDetail) {
        super(message);
        this.epicorFaultDetail = epicorFaultDetail;
    }

    public SalesOrderSvcContractChangeOrderRelShipToEpicorFaultDetailFaultFaultMessage(String message, com.epicor.erp.salesorder.EpicorFaultDetail epicorFaultDetail, Throwable cause) {
        super(message, cause);
        this.epicorFaultDetail = epicorFaultDetail;
    }

    public com.epicor.erp.salesorder.EpicorFaultDetail getFaultInfo() {
        return this.epicorFaultDetail;
    }
}
