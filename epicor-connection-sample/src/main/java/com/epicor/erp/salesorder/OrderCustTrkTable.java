
package com.epicor.erp.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderCustTrkTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderCustTrkTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderCustTrkRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderCustTrkRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderCustTrkTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "orderCustTrkRow"
})
public class OrderCustTrkTable {

    @XmlElement(name = "OrderCustTrkRow", nillable = true)
    protected List<OrderCustTrkRow> orderCustTrkRow;

    /**
     * Gets the value of the orderCustTrkRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderCustTrkRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderCustTrkRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderCustTrkRow }
     * 
     * 
     */
    public List<OrderCustTrkRow> getOrderCustTrkRow() {
        if (orderCustTrkRow == null) {
            orderCustTrkRow = new ArrayList<OrderCustTrkRow>();
        }
        return this.orderCustTrkRow;
    }

    /**
     * Sets the value of the orderCustTrkRow property.
     * 
     * @param orderCustTrkRow
     *     allowed object is
     *     {@link OrderCustTrkRow }
     *     
     */
    public void setOrderCustTrkRow(List<OrderCustTrkRow> orderCustTrkRow) {
        this.orderCustTrkRow = orderCustTrkRow;
    }

}
