
package com.epicor.erp.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderMscTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderMscTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderMscRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderMscRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderMscTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "orderMscRow"
})
public class OrderMscTable {

    @XmlElement(name = "OrderMscRow", nillable = true)
    protected List<OrderMscRow> orderMscRow;

    /**
     * Gets the value of the orderMscRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderMscRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderMscRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderMscRow }
     * 
     * 
     */
    public List<OrderMscRow> getOrderMscRow() {
        if (orderMscRow == null) {
            orderMscRow = new ArrayList<OrderMscRow>();
        }
        return this.orderMscRow;
    }

    /**
     * Sets the value of the orderMscRow property.
     * 
     * @param orderMscRow
     *     allowed object is
     *     {@link OrderMscRow }
     *     
     */
    public void setOrderMscRow(List<OrderMscRow> orderMscRow) {
        this.orderMscRow = orderMscRow;
    }

}
