
package com.tesamm.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IceRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IceRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Epicor.Data}TempRowBase">
 *       &lt;sequence>
 *         &lt;element name="RowMod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SpecifiedProperties" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="SysRowID" type="{http://schemas.microsoft.com/2003/10/Serialization/}guid" minOccurs="0"/>
 *         &lt;element name="UserDefinedColumns" type="{http://epicor.com/UserDefinedColumns}UserDefinedColumns" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IceRow", namespace = "http://schemas.datacontract.org/2004/07/Ice", propOrder = {
    "rowMod",
    "specifiedProperties",
    "sysRowID",
    "userDefinedColumns"
})
@XmlSeeAlso({
    BOUpdErrorRow.class,
    OrderDtlAttchRow.class,
    OrderHedAttchRow.class,
    QuoteQtyRow.class,
    GlbSugPOChgRow.class,
    PartSubsRow.class,
    HedTaxSumRow.class,
    OrderMscRow.class,
    SelectedSerialNumbersRow.class,
    OrderCustTrkRow.class,
    SNFormatRow.class,
    OrderRelRow.class,
    SelectSerialNumbersParamsRow.class,
    OrderDtlRow.class,
    OrderHedRow.class,
    OrderHistRow.class,
    SaveOTSParamsRow.class,
    OHOrderMscRow.class,
    OrderRepCommRow.class,
    ETCMessageRow.class,
    OrderHedUPSRow.class,
    OrderHedListRow.class,
    ETCAddressRow.class,
    JobProdRow.class,
    OrderRelTaxRow.class,
    TaxConnectStatusRow.class,
    SOEntryUIParamsRow.class
})
public class IceRow
    extends TempRowBase
{

    @XmlElementRef(name = "RowMod", namespace = "http://schemas.datacontract.org/2004/07/Ice", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rowMod;
    @XmlElementRef(name = "SpecifiedProperties", namespace = "http://schemas.datacontract.org/2004/07/Ice", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> specifiedProperties;
    @XmlElement(name = "SysRowID")
    protected String sysRowID;
    @XmlElementRef(name = "UserDefinedColumns", namespace = "http://schemas.datacontract.org/2004/07/Ice", type = JAXBElement.class, required = false)
    protected JAXBElement<UserDefinedColumns> userDefinedColumns;

    /**
     * Gets the value of the rowMod property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRowMod() {
        return rowMod;
    }

    /**
     * Sets the value of the rowMod property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRowMod(JAXBElement<String> value) {
        this.rowMod = value;
    }

    /**
     * Gets the value of the specifiedProperties property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getSpecifiedProperties() {
        return specifiedProperties;
    }

    /**
     * Sets the value of the specifiedProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setSpecifiedProperties(JAXBElement<byte[]> value) {
        this.specifiedProperties = value;
    }

    /**
     * Gets the value of the sysRowID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysRowID() {
        return sysRowID;
    }

    /**
     * Sets the value of the sysRowID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysRowID(String value) {
        this.sysRowID = value;
    }

    /**
     * Gets the value of the userDefinedColumns property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link UserDefinedColumns }{@code >}
     *     
     */
    public JAXBElement<UserDefinedColumns> getUserDefinedColumns() {
        return userDefinedColumns;
    }

    /**
     * Sets the value of the userDefinedColumns property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link UserDefinedColumns }{@code >}
     *     
     */
    public void setUserDefinedColumns(JAXBElement<UserDefinedColumns> value) {
        this.userDefinedColumns = value;
    }

}
