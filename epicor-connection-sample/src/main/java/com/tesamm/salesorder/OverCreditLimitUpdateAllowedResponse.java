
package com.tesamm.salesorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lUpdateAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lUpdateAllowed"
})
@XmlRootElement(name = "OverCreditLimitUpdateAllowedResponse")
public class OverCreditLimitUpdateAllowedResponse {

    protected Boolean lUpdateAllowed;

    /**
     * Gets the value of the lUpdateAllowed property.
     * This getter has been renamed from isLUpdateAllowed() to getLUpdateAllowed() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLUpdateAllowed() {
        return lUpdateAllowed;
    }

    /**
     * Sets the value of the lUpdateAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLUpdateAllowed(Boolean value) {
        this.lUpdateAllowed = value;
    }

}
