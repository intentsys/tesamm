
package com.tesamm.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SaveOTSParamsTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SaveOTSParamsTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SaveOTSParamsRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SaveOTSParamsRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SaveOTSParamsTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "saveOTSParamsRow"
})
public class SaveOTSParamsTable {

    @XmlElement(name = "SaveOTSParamsRow", nillable = true)
    protected List<SaveOTSParamsRow> saveOTSParamsRow;

    /**
     * Gets the value of the saveOTSParamsRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the saveOTSParamsRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSaveOTSParamsRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SaveOTSParamsRow }
     * 
     * 
     */
    public List<SaveOTSParamsRow> getSaveOTSParamsRow() {
        if (saveOTSParamsRow == null) {
            saveOTSParamsRow = new ArrayList<SaveOTSParamsRow>();
        }
        return this.saveOTSParamsRow;
    }

    /**
     * Sets the value of the saveOTSParamsRow property.
     * 
     * @param saveOTSParamsRow
     *     allowed object is
     *     {@link SaveOTSParamsRow }
     *     
     */
    public void setSaveOTSParamsRow(List<SaveOTSParamsRow> saveOTSParamsRow) {
        this.saveOTSParamsRow = saveOTSParamsRow;
    }

}
