
package com.tesamm.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ds" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SalesOrderTableset" minOccurs="0"/>
 *         &lt;element name="lSubstitutePartsExist" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="uomCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ds",
    "lSubstitutePartsExist",
    "uomCode"
})
@XmlRootElement(name = "ChangePartNum")
public class ChangePartNum {

    @XmlElementRef(name = "ds", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<SalesOrderTableset> ds;
    protected Boolean lSubstitutePartsExist;
    @XmlElementRef(name = "uomCode", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> uomCode;

    /**
     * Gets the value of the ds property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}
     *     
     */
    public JAXBElement<SalesOrderTableset> getDs() {
        return ds;
    }

    /**
     * Sets the value of the ds property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}
     *     
     */
    public void setDs(JAXBElement<SalesOrderTableset> value) {
        this.ds = value;
    }

    /**
     * Gets the value of the lSubstitutePartsExist property.
     * This getter has been renamed from isLSubstitutePartsExist() to getLSubstitutePartsExist() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLSubstitutePartsExist() {
        return lSubstitutePartsExist;
    }

    /**
     * Sets the value of the lSubstitutePartsExist property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLSubstitutePartsExist(Boolean value) {
        this.lSubstitutePartsExist = value;
    }

    /**
     * Gets the value of the uomCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUomCode() {
        return uomCode;
    }

    /**
     * Sets the value of the uomCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUomCode(JAXBElement<String> value) {
        this.uomCode = value;
    }

}
