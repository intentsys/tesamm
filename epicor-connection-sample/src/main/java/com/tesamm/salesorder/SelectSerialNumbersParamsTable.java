
package com.tesamm.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SelectSerialNumbersParamsTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SelectSerialNumbersParamsTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SelectSerialNumbersParamsRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SelectSerialNumbersParamsRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SelectSerialNumbersParamsTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "selectSerialNumbersParamsRow"
})
public class SelectSerialNumbersParamsTable {

    @XmlElement(name = "SelectSerialNumbersParamsRow", nillable = true)
    protected List<SelectSerialNumbersParamsRow> selectSerialNumbersParamsRow;

    /**
     * Gets the value of the selectSerialNumbersParamsRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectSerialNumbersParamsRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectSerialNumbersParamsRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SelectSerialNumbersParamsRow }
     * 
     * 
     */
    public List<SelectSerialNumbersParamsRow> getSelectSerialNumbersParamsRow() {
        if (selectSerialNumbersParamsRow == null) {
            selectSerialNumbersParamsRow = new ArrayList<SelectSerialNumbersParamsRow>();
        }
        return this.selectSerialNumbersParamsRow;
    }

    /**
     * Sets the value of the selectSerialNumbersParamsRow property.
     * 
     * @param selectSerialNumbersParamsRow
     *     allowed object is
     *     {@link SelectSerialNumbersParamsRow }
     *     
     */
    public void setSelectSerialNumbersParamsRow(List<SelectSerialNumbersParamsRow> selectSerialNumbersParamsRow) {
        this.selectSerialNumbersParamsRow = selectSerialNumbersParamsRow;
    }

}
