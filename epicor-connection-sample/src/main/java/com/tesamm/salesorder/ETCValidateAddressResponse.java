
package com.tesamm.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ETCValidateAddressResult" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}ETCAddrValidationTableset" minOccurs="0"/>
 *         &lt;element name="StatusFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ErrorFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ErrorMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExceptionFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "etcValidateAddressResult",
    "statusFlag",
    "errorFlag",
    "errorMsg",
    "exceptionFlag"
})
@XmlRootElement(name = "ETCValidateAddressResponse")
public class ETCValidateAddressResponse {

    @XmlElementRef(name = "ETCValidateAddressResult", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<ETCAddrValidationTableset> etcValidateAddressResult;
    @XmlElement(name = "StatusFlag")
    protected Boolean statusFlag;
    @XmlElement(name = "ErrorFlag")
    protected Boolean errorFlag;
    @XmlElementRef(name = "ErrorMsg", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> errorMsg;
    @XmlElement(name = "ExceptionFlag")
    protected Boolean exceptionFlag;

    /**
     * Gets the value of the etcValidateAddressResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ETCAddrValidationTableset }{@code >}
     *     
     */
    public JAXBElement<ETCAddrValidationTableset> getETCValidateAddressResult() {
        return etcValidateAddressResult;
    }

    /**
     * Sets the value of the etcValidateAddressResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ETCAddrValidationTableset }{@code >}
     *     
     */
    public void setETCValidateAddressResult(JAXBElement<ETCAddrValidationTableset> value) {
        this.etcValidateAddressResult = value;
    }

    /**
     * Gets the value of the statusFlag property.
     * This getter has been renamed from isStatusFlag() to getStatusFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getStatusFlag() {
        return statusFlag;
    }

    /**
     * Sets the value of the statusFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStatusFlag(Boolean value) {
        this.statusFlag = value;
    }

    /**
     * Gets the value of the errorFlag property.
     * This getter has been renamed from isErrorFlag() to getErrorFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getErrorFlag() {
        return errorFlag;
    }

    /**
     * Sets the value of the errorFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setErrorFlag(Boolean value) {
        this.errorFlag = value;
    }

    /**
     * Gets the value of the errorMsg property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getErrorMsg() {
        return errorMsg;
    }

    /**
     * Sets the value of the errorMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setErrorMsg(JAXBElement<String> value) {
        this.errorMsg = value;
    }

    /**
     * Gets the value of the exceptionFlag property.
     * This getter has been renamed from isExceptionFlag() to getExceptionFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getExceptionFlag() {
        return exceptionFlag;
    }

    /**
     * Sets the value of the exceptionFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExceptionFlag(Boolean value) {
        this.exceptionFlag = value;
    }

}
