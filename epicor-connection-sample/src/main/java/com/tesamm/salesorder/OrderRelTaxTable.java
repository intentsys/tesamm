
package com.tesamm.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderRelTaxTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderRelTaxTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderRelTaxRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderRelTaxRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderRelTaxTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "orderRelTaxRow"
})
public class OrderRelTaxTable {

    @XmlElement(name = "OrderRelTaxRow", nillable = true)
    protected List<OrderRelTaxRow> orderRelTaxRow;

    /**
     * Gets the value of the orderRelTaxRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderRelTaxRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderRelTaxRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderRelTaxRow }
     * 
     * 
     */
    public List<OrderRelTaxRow> getOrderRelTaxRow() {
        if (orderRelTaxRow == null) {
            orderRelTaxRow = new ArrayList<OrderRelTaxRow>();
        }
        return this.orderRelTaxRow;
    }

    /**
     * Sets the value of the orderRelTaxRow property.
     * 
     * @param orderRelTaxRow
     *     allowed object is
     *     {@link OrderRelTaxRow }
     *     
     */
    public void setOrderRelTaxRow(List<OrderRelTaxRow> orderRelTaxRow) {
        this.orderRelTaxRow = orderRelTaxRow;
    }

}
