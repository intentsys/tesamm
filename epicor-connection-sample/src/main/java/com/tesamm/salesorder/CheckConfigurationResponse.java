
package com.tesamm.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="canGetDetails" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="needsConfiguration" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="configureRevision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reasonMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "canGetDetails",
    "needsConfiguration",
    "configureRevision",
    "reasonMessage"
})
@XmlRootElement(name = "CheckConfigurationResponse")
public class CheckConfigurationResponse {

    protected Boolean canGetDetails;
    protected Boolean needsConfiguration;
    @XmlElementRef(name = "configureRevision", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> configureRevision;
    @XmlElementRef(name = "reasonMessage", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reasonMessage;

    /**
     * Gets the value of the canGetDetails property.
     * This getter has been renamed from isCanGetDetails() to getCanGetDetails() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCanGetDetails() {
        return canGetDetails;
    }

    /**
     * Sets the value of the canGetDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCanGetDetails(Boolean value) {
        this.canGetDetails = value;
    }

    /**
     * Gets the value of the needsConfiguration property.
     * This getter has been renamed from isNeedsConfiguration() to getNeedsConfiguration() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNeedsConfiguration() {
        return needsConfiguration;
    }

    /**
     * Sets the value of the needsConfiguration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNeedsConfiguration(Boolean value) {
        this.needsConfiguration = value;
    }

    /**
     * Gets the value of the configureRevision property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getConfigureRevision() {
        return configureRevision;
    }

    /**
     * Sets the value of the configureRevision property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setConfigureRevision(JAXBElement<String> value) {
        this.configureRevision = value;
    }

    /**
     * Gets the value of the reasonMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReasonMessage() {
        return reasonMessage;
    }

    /**
     * Sets the value of the reasonMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReasonMessage(JAXBElement<String> value) {
        this.reasonMessage = value;
    }

}
