
package com.tesamm.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderDtlTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderDtlTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderDtlRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderDtlRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderDtlTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "orderDtlRow"
})
public class OrderDtlTable {

    @XmlElement(name = "OrderDtlRow", nillable = true)
    protected List<OrderDtlRow> orderDtlRow;

    /**
     * Gets the value of the orderDtlRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderDtlRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderDtlRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderDtlRow }
     * 
     * 
     */
    public List<OrderDtlRow> getOrderDtlRow() {
        if (orderDtlRow == null) {
            orderDtlRow = new ArrayList<OrderDtlRow>();
        }
        return this.orderDtlRow;
    }

    /**
     * Sets the value of the orderDtlRow property.
     * 
     * @param orderDtlRow
     *     allowed object is
     *     {@link OrderDtlRow }
     *     
     */
    public void setOrderDtlRow(List<OrderDtlRow> orderDtlRow) {
        this.orderDtlRow = orderDtlRow;
    }

}
