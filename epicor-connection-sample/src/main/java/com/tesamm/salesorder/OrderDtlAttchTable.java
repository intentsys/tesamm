
package com.tesamm.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderDtlAttchTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderDtlAttchTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderDtlAttchRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderDtlAttchRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderDtlAttchTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "orderDtlAttchRow"
})
public class OrderDtlAttchTable {

    @XmlElement(name = "OrderDtlAttchRow", nillable = true)
    protected List<OrderDtlAttchRow> orderDtlAttchRow;

    /**
     * Gets the value of the orderDtlAttchRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderDtlAttchRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderDtlAttchRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderDtlAttchRow }
     * 
     * 
     */
    public List<OrderDtlAttchRow> getOrderDtlAttchRow() {
        if (orderDtlAttchRow == null) {
            orderDtlAttchRow = new ArrayList<OrderDtlAttchRow>();
        }
        return this.orderDtlAttchRow;
    }

    /**
     * Sets the value of the orderDtlAttchRow property.
     * 
     * @param orderDtlAttchRow
     *     allowed object is
     *     {@link OrderDtlAttchRow }
     *     
     */
    public void setOrderDtlAttchRow(List<OrderDtlAttchRow> orderDtlAttchRow) {
        this.orderDtlAttchRow = orderDtlAttchRow;
    }

}
