
package com.tesamm.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GlbSugPOChgDeleteResult" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}GlbSugPOChgTableset" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "glbSugPOChgDeleteResult"
})
@XmlRootElement(name = "GlbSugPOChgDeleteResponse")
public class GlbSugPOChgDeleteResponse {

    @XmlElementRef(name = "GlbSugPOChgDeleteResult", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<GlbSugPOChgTableset> glbSugPOChgDeleteResult;

    /**
     * Gets the value of the glbSugPOChgDeleteResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}
     *     
     */
    public JAXBElement<GlbSugPOChgTableset> getGlbSugPOChgDeleteResult() {
        return glbSugPOChgDeleteResult;
    }

    /**
     * Sets the value of the glbSugPOChgDeleteResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}
     *     
     */
    public void setGlbSugPOChgDeleteResult(JAXBElement<GlbSugPOChgTableset> value) {
        this.glbSugPOChgDeleteResult = value;
    }

}
