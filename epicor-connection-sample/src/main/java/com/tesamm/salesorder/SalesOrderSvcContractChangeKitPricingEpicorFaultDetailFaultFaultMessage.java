
package com.tesamm.salesorder;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.5.1
 * 2016-01-20T22:00:56.174+08:00
 * Generated source version: 2.5.1
 */

@WebFault(name = "EpicorFaultDetail", targetNamespace = "http://schemas.datacontract.org/2004/07/Ice.Common")
public class SalesOrderSvcContractChangeKitPricingEpicorFaultDetailFaultFaultMessage extends Exception {
    
    private com.tesamm.salesorder.EpicorFaultDetail epicorFaultDetail;

    public SalesOrderSvcContractChangeKitPricingEpicorFaultDetailFaultFaultMessage() {
        super();
    }
    
    public SalesOrderSvcContractChangeKitPricingEpicorFaultDetailFaultFaultMessage(String message) {
        super(message);
    }
    
    public SalesOrderSvcContractChangeKitPricingEpicorFaultDetailFaultFaultMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public SalesOrderSvcContractChangeKitPricingEpicorFaultDetailFaultFaultMessage(String message, com.tesamm.salesorder.EpicorFaultDetail epicorFaultDetail) {
        super(message);
        this.epicorFaultDetail = epicorFaultDetail;
    }

    public SalesOrderSvcContractChangeKitPricingEpicorFaultDetailFaultFaultMessage(String message, com.tesamm.salesorder.EpicorFaultDetail epicorFaultDetail, Throwable cause) {
        super(message, cause);
        this.epicorFaultDetail = epicorFaultDetail;
    }

    public com.tesamm.salesorder.EpicorFaultDetail getFaultInfo() {
        return this.epicorFaultDetail;
    }
}
