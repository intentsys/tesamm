
package com.tesamm.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderRepCommTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderRepCommTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderRepCommRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderRepCommRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderRepCommTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "orderRepCommRow"
})
public class OrderRepCommTable {

    @XmlElement(name = "OrderRepCommRow", nillable = true)
    protected List<OrderRepCommRow> orderRepCommRow;

    /**
     * Gets the value of the orderRepCommRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderRepCommRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderRepCommRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderRepCommRow }
     * 
     * 
     */
    public List<OrderRepCommRow> getOrderRepCommRow() {
        if (orderRepCommRow == null) {
            orderRepCommRow = new ArrayList<OrderRepCommRow>();
        }
        return this.orderRepCommRow;
    }

    /**
     * Sets the value of the orderRepCommRow property.
     * 
     * @param orderRepCommRow
     *     allowed object is
     *     {@link OrderRepCommRow }
     *     
     */
    public void setOrderRepCommRow(List<OrderRepCommRow> orderRepCommRow) {
        this.orderRepCommRow = orderRepCommRow;
    }

}
