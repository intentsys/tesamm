
package com.tesamm.salesorder;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OrderRelTaxRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderRelTaxRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceRow">
 *       &lt;sequence>
 *         &lt;element name="BitFlag" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ChangeTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ChangedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CollectionType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CollectionTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencySwitch" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DedTaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DefTaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DefTaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DisplaySymbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocDedTaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocDefTaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocDefTaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocDisplaySymbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocFixedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocReportableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ECAcquisitionSeq" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="EntryProcess" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExemptPercent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ExemptType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FixedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ManAdd" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Manual" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="NoChangeManual" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OrderLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OrderNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OrderRelNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Percent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RateCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateCodeDescDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReportableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ResolutionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ResolutionNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReverseCharge" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Rpt1DedTaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1DefTaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1DefTaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1Discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1FixedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1ReportableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2DedTaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2DefTaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2DefTaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2Discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2FixedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2ReportableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3DedTaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3DefTaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3DefTaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3Discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3FixedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3ReportableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SalesTaxDescDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SysRevID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="TaxAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxRateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TaxableAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TextCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Timing" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderRelTaxRow", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "bitFlag",
    "changeDate",
    "changeTime",
    "changedBy",
    "collectionType",
    "collectionTypeDescription",
    "company",
    "currencyCode",
    "currencySwitch",
    "dedTaxAmt",
    "defTaxAmt",
    "defTaxableAmt",
    "discount",
    "displaySymbol",
    "docDedTaxAmt",
    "docDefTaxAmt",
    "docDefTaxableAmt",
    "docDiscount",
    "docDisplaySymbol",
    "docFixedAmount",
    "docReportableAmt",
    "docTaxAmt",
    "docTaxableAmt",
    "ecAcquisitionSeq",
    "entryProcess",
    "exemptPercent",
    "exemptType",
    "fixedAmount",
    "manAdd",
    "manual",
    "noChangeManual",
    "orderLine",
    "orderNum",
    "orderRelNum",
    "percent",
    "rateCode",
    "rateCodeDescDescription",
    "reportableAmt",
    "resolutionDate",
    "resolutionNum",
    "reverseCharge",
    "rpt1DedTaxAmt",
    "rpt1DefTaxAmt",
    "rpt1DefTaxableAmt",
    "rpt1Discount",
    "rpt1FixedAmount",
    "rpt1ReportableAmt",
    "rpt1TaxAmt",
    "rpt1TaxableAmt",
    "rpt2DedTaxAmt",
    "rpt2DefTaxAmt",
    "rpt2DefTaxableAmt",
    "rpt2Discount",
    "rpt2FixedAmount",
    "rpt2ReportableAmt",
    "rpt2TaxAmt",
    "rpt2TaxableAmt",
    "rpt3DedTaxAmt",
    "rpt3DefTaxAmt",
    "rpt3DefTaxableAmt",
    "rpt3Discount",
    "rpt3FixedAmount",
    "rpt3ReportableAmt",
    "rpt3TaxAmt",
    "rpt3TaxableAmt",
    "salesTaxDescDescription",
    "sysRevID",
    "taxAmt",
    "taxCode",
    "taxRateDate",
    "taxableAmt",
    "textCode",
    "timing"
})
public class OrderRelTaxRow
    extends IceRow
{

    @XmlElement(name = "BitFlag")
    protected Integer bitFlag;
    @XmlElementRef(name = "ChangeDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> changeDate;
    @XmlElement(name = "ChangeTime")
    protected Integer changeTime;
    @XmlElementRef(name = "ChangedBy", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changedBy;
    @XmlElement(name = "CollectionType")
    protected Integer collectionType;
    @XmlElementRef(name = "CollectionTypeDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> collectionTypeDescription;
    @XmlElementRef(name = "Company", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> company;
    @XmlElementRef(name = "CurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElement(name = "CurrencySwitch")
    protected Boolean currencySwitch;
    @XmlElement(name = "DedTaxAmt")
    protected BigDecimal dedTaxAmt;
    @XmlElement(name = "DefTaxAmt")
    protected BigDecimal defTaxAmt;
    @XmlElement(name = "DefTaxableAmt")
    protected BigDecimal defTaxableAmt;
    @XmlElement(name = "Discount")
    protected BigDecimal discount;
    @XmlElementRef(name = "DisplaySymbol", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> displaySymbol;
    @XmlElement(name = "DocDedTaxAmt")
    protected BigDecimal docDedTaxAmt;
    @XmlElement(name = "DocDefTaxAmt")
    protected BigDecimal docDefTaxAmt;
    @XmlElement(name = "DocDefTaxableAmt")
    protected BigDecimal docDefTaxableAmt;
    @XmlElement(name = "DocDiscount")
    protected BigDecimal docDiscount;
    @XmlElementRef(name = "DocDisplaySymbol", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> docDisplaySymbol;
    @XmlElement(name = "DocFixedAmount")
    protected BigDecimal docFixedAmount;
    @XmlElement(name = "DocReportableAmt")
    protected BigDecimal docReportableAmt;
    @XmlElement(name = "DocTaxAmt")
    protected BigDecimal docTaxAmt;
    @XmlElement(name = "DocTaxableAmt")
    protected BigDecimal docTaxableAmt;
    @XmlElement(name = "ECAcquisitionSeq")
    protected Integer ecAcquisitionSeq;
    @XmlElementRef(name = "EntryProcess", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> entryProcess;
    @XmlElement(name = "ExemptPercent")
    protected BigDecimal exemptPercent;
    @XmlElement(name = "ExemptType")
    protected Integer exemptType;
    @XmlElement(name = "FixedAmount")
    protected BigDecimal fixedAmount;
    @XmlElement(name = "ManAdd")
    protected Boolean manAdd;
    @XmlElement(name = "Manual")
    protected Boolean manual;
    @XmlElement(name = "NoChangeManual")
    protected Boolean noChangeManual;
    @XmlElement(name = "OrderLine")
    protected Integer orderLine;
    @XmlElement(name = "OrderNum")
    protected Integer orderNum;
    @XmlElement(name = "OrderRelNum")
    protected Integer orderRelNum;
    @XmlElement(name = "Percent")
    protected BigDecimal percent;
    @XmlElementRef(name = "RateCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rateCode;
    @XmlElementRef(name = "RateCodeDescDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rateCodeDescDescription;
    @XmlElement(name = "ReportableAmt")
    protected BigDecimal reportableAmt;
    @XmlElementRef(name = "ResolutionDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> resolutionDate;
    @XmlElementRef(name = "ResolutionNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> resolutionNum;
    @XmlElement(name = "ReverseCharge")
    protected Boolean reverseCharge;
    @XmlElement(name = "Rpt1DedTaxAmt")
    protected BigDecimal rpt1DedTaxAmt;
    @XmlElement(name = "Rpt1DefTaxAmt")
    protected BigDecimal rpt1DefTaxAmt;
    @XmlElement(name = "Rpt1DefTaxableAmt")
    protected BigDecimal rpt1DefTaxableAmt;
    @XmlElement(name = "Rpt1Discount")
    protected BigDecimal rpt1Discount;
    @XmlElement(name = "Rpt1FixedAmount")
    protected BigDecimal rpt1FixedAmount;
    @XmlElement(name = "Rpt1ReportableAmt")
    protected BigDecimal rpt1ReportableAmt;
    @XmlElement(name = "Rpt1TaxAmt")
    protected BigDecimal rpt1TaxAmt;
    @XmlElement(name = "Rpt1TaxableAmt")
    protected BigDecimal rpt1TaxableAmt;
    @XmlElement(name = "Rpt2DedTaxAmt")
    protected BigDecimal rpt2DedTaxAmt;
    @XmlElement(name = "Rpt2DefTaxAmt")
    protected BigDecimal rpt2DefTaxAmt;
    @XmlElement(name = "Rpt2DefTaxableAmt")
    protected BigDecimal rpt2DefTaxableAmt;
    @XmlElement(name = "Rpt2Discount")
    protected BigDecimal rpt2Discount;
    @XmlElement(name = "Rpt2FixedAmount")
    protected BigDecimal rpt2FixedAmount;
    @XmlElement(name = "Rpt2ReportableAmt")
    protected BigDecimal rpt2ReportableAmt;
    @XmlElement(name = "Rpt2TaxAmt")
    protected BigDecimal rpt2TaxAmt;
    @XmlElement(name = "Rpt2TaxableAmt")
    protected BigDecimal rpt2TaxableAmt;
    @XmlElement(name = "Rpt3DedTaxAmt")
    protected BigDecimal rpt3DedTaxAmt;
    @XmlElement(name = "Rpt3DefTaxAmt")
    protected BigDecimal rpt3DefTaxAmt;
    @XmlElement(name = "Rpt3DefTaxableAmt")
    protected BigDecimal rpt3DefTaxableAmt;
    @XmlElement(name = "Rpt3Discount")
    protected BigDecimal rpt3Discount;
    @XmlElement(name = "Rpt3FixedAmount")
    protected BigDecimal rpt3FixedAmount;
    @XmlElement(name = "Rpt3ReportableAmt")
    protected BigDecimal rpt3ReportableAmt;
    @XmlElement(name = "Rpt3TaxAmt")
    protected BigDecimal rpt3TaxAmt;
    @XmlElement(name = "Rpt3TaxableAmt")
    protected BigDecimal rpt3TaxableAmt;
    @XmlElementRef(name = "SalesTaxDescDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesTaxDescDescription;
    @XmlElement(name = "SysRevID")
    protected Long sysRevID;
    @XmlElement(name = "TaxAmt")
    protected BigDecimal taxAmt;
    @XmlElementRef(name = "TaxCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxCode;
    @XmlElementRef(name = "TaxRateDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> taxRateDate;
    @XmlElement(name = "TaxableAmt")
    protected BigDecimal taxableAmt;
    @XmlElementRef(name = "TextCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> textCode;
    @XmlElement(name = "Timing")
    protected Integer timing;

    /**
     * Gets the value of the bitFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBitFlag() {
        return bitFlag;
    }

    /**
     * Sets the value of the bitFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBitFlag(Integer value) {
        this.bitFlag = value;
    }

    /**
     * Gets the value of the changeDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getChangeDate() {
        return changeDate;
    }

    /**
     * Sets the value of the changeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setChangeDate(JAXBElement<XMLGregorianCalendar> value) {
        this.changeDate = value;
    }

    /**
     * Gets the value of the changeTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChangeTime() {
        return changeTime;
    }

    /**
     * Sets the value of the changeTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChangeTime(Integer value) {
        this.changeTime = value;
    }

    /**
     * Gets the value of the changedBy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangedBy() {
        return changedBy;
    }

    /**
     * Sets the value of the changedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangedBy(JAXBElement<String> value) {
        this.changedBy = value;
    }

    /**
     * Gets the value of the collectionType property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCollectionType() {
        return collectionType;
    }

    /**
     * Sets the value of the collectionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCollectionType(Integer value) {
        this.collectionType = value;
    }

    /**
     * Gets the value of the collectionTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCollectionTypeDescription() {
        return collectionTypeDescription;
    }

    /**
     * Sets the value of the collectionTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCollectionTypeDescription(JAXBElement<String> value) {
        this.collectionTypeDescription = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompany(JAXBElement<String> value) {
        this.company = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the currencySwitch property.
     * This getter has been renamed from isCurrencySwitch() to getCurrencySwitch() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCurrencySwitch() {
        return currencySwitch;
    }

    /**
     * Sets the value of the currencySwitch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCurrencySwitch(Boolean value) {
        this.currencySwitch = value;
    }

    /**
     * Gets the value of the dedTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDedTaxAmt() {
        return dedTaxAmt;
    }

    /**
     * Sets the value of the dedTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDedTaxAmt(BigDecimal value) {
        this.dedTaxAmt = value;
    }

    /**
     * Gets the value of the defTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDefTaxAmt() {
        return defTaxAmt;
    }

    /**
     * Sets the value of the defTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDefTaxAmt(BigDecimal value) {
        this.defTaxAmt = value;
    }

    /**
     * Gets the value of the defTaxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDefTaxableAmt() {
        return defTaxableAmt;
    }

    /**
     * Sets the value of the defTaxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDefTaxableAmt(BigDecimal value) {
        this.defTaxableAmt = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscount(BigDecimal value) {
        this.discount = value;
    }

    /**
     * Gets the value of the displaySymbol property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDisplaySymbol() {
        return displaySymbol;
    }

    /**
     * Sets the value of the displaySymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDisplaySymbol(JAXBElement<String> value) {
        this.displaySymbol = value;
    }

    /**
     * Gets the value of the docDedTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocDedTaxAmt() {
        return docDedTaxAmt;
    }

    /**
     * Sets the value of the docDedTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocDedTaxAmt(BigDecimal value) {
        this.docDedTaxAmt = value;
    }

    /**
     * Gets the value of the docDefTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocDefTaxAmt() {
        return docDefTaxAmt;
    }

    /**
     * Sets the value of the docDefTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocDefTaxAmt(BigDecimal value) {
        this.docDefTaxAmt = value;
    }

    /**
     * Gets the value of the docDefTaxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocDefTaxableAmt() {
        return docDefTaxableAmt;
    }

    /**
     * Sets the value of the docDefTaxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocDefTaxableAmt(BigDecimal value) {
        this.docDefTaxableAmt = value;
    }

    /**
     * Gets the value of the docDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocDiscount() {
        return docDiscount;
    }

    /**
     * Sets the value of the docDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocDiscount(BigDecimal value) {
        this.docDiscount = value;
    }

    /**
     * Gets the value of the docDisplaySymbol property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDocDisplaySymbol() {
        return docDisplaySymbol;
    }

    /**
     * Sets the value of the docDisplaySymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDocDisplaySymbol(JAXBElement<String> value) {
        this.docDisplaySymbol = value;
    }

    /**
     * Gets the value of the docFixedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocFixedAmount() {
        return docFixedAmount;
    }

    /**
     * Sets the value of the docFixedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocFixedAmount(BigDecimal value) {
        this.docFixedAmount = value;
    }

    /**
     * Gets the value of the docReportableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocReportableAmt() {
        return docReportableAmt;
    }

    /**
     * Sets the value of the docReportableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocReportableAmt(BigDecimal value) {
        this.docReportableAmt = value;
    }

    /**
     * Gets the value of the docTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTaxAmt() {
        return docTaxAmt;
    }

    /**
     * Sets the value of the docTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTaxAmt(BigDecimal value) {
        this.docTaxAmt = value;
    }

    /**
     * Gets the value of the docTaxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTaxableAmt() {
        return docTaxableAmt;
    }

    /**
     * Sets the value of the docTaxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTaxableAmt(BigDecimal value) {
        this.docTaxableAmt = value;
    }

    /**
     * Gets the value of the ecAcquisitionSeq property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getECAcquisitionSeq() {
        return ecAcquisitionSeq;
    }

    /**
     * Sets the value of the ecAcquisitionSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setECAcquisitionSeq(Integer value) {
        this.ecAcquisitionSeq = value;
    }

    /**
     * Gets the value of the entryProcess property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEntryProcess() {
        return entryProcess;
    }

    /**
     * Sets the value of the entryProcess property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEntryProcess(JAXBElement<String> value) {
        this.entryProcess = value;
    }

    /**
     * Gets the value of the exemptPercent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExemptPercent() {
        return exemptPercent;
    }

    /**
     * Sets the value of the exemptPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExemptPercent(BigDecimal value) {
        this.exemptPercent = value;
    }

    /**
     * Gets the value of the exemptType property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExemptType() {
        return exemptType;
    }

    /**
     * Sets the value of the exemptType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExemptType(Integer value) {
        this.exemptType = value;
    }

    /**
     * Gets the value of the fixedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFixedAmount() {
        return fixedAmount;
    }

    /**
     * Sets the value of the fixedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFixedAmount(BigDecimal value) {
        this.fixedAmount = value;
    }

    /**
     * Gets the value of the manAdd property.
     * This getter has been renamed from isManAdd() to getManAdd() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getManAdd() {
        return manAdd;
    }

    /**
     * Sets the value of the manAdd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setManAdd(Boolean value) {
        this.manAdd = value;
    }

    /**
     * Gets the value of the manual property.
     * This getter has been renamed from isManual() to getManual() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getManual() {
        return manual;
    }

    /**
     * Sets the value of the manual property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setManual(Boolean value) {
        this.manual = value;
    }

    /**
     * Gets the value of the noChangeManual property.
     * This getter has been renamed from isNoChangeManual() to getNoChangeManual() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNoChangeManual() {
        return noChangeManual;
    }

    /**
     * Sets the value of the noChangeManual property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoChangeManual(Boolean value) {
        this.noChangeManual = value;
    }

    /**
     * Gets the value of the orderLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderLine() {
        return orderLine;
    }

    /**
     * Sets the value of the orderLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderLine(Integer value) {
        this.orderLine = value;
    }

    /**
     * Gets the value of the orderNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * Sets the value of the orderNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNum(Integer value) {
        this.orderNum = value;
    }

    /**
     * Gets the value of the orderRelNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderRelNum() {
        return orderRelNum;
    }

    /**
     * Sets the value of the orderRelNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderRelNum(Integer value) {
        this.orderRelNum = value;
    }

    /**
     * Gets the value of the percent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercent() {
        return percent;
    }

    /**
     * Sets the value of the percent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercent(BigDecimal value) {
        this.percent = value;
    }

    /**
     * Gets the value of the rateCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRateCode() {
        return rateCode;
    }

    /**
     * Sets the value of the rateCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRateCode(JAXBElement<String> value) {
        this.rateCode = value;
    }

    /**
     * Gets the value of the rateCodeDescDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRateCodeDescDescription() {
        return rateCodeDescDescription;
    }

    /**
     * Sets the value of the rateCodeDescDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRateCodeDescDescription(JAXBElement<String> value) {
        this.rateCodeDescDescription = value;
    }

    /**
     * Gets the value of the reportableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReportableAmt() {
        return reportableAmt;
    }

    /**
     * Sets the value of the reportableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReportableAmt(BigDecimal value) {
        this.reportableAmt = value;
    }

    /**
     * Gets the value of the resolutionDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getResolutionDate() {
        return resolutionDate;
    }

    /**
     * Sets the value of the resolutionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setResolutionDate(JAXBElement<XMLGregorianCalendar> value) {
        this.resolutionDate = value;
    }

    /**
     * Gets the value of the resolutionNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getResolutionNum() {
        return resolutionNum;
    }

    /**
     * Sets the value of the resolutionNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setResolutionNum(JAXBElement<String> value) {
        this.resolutionNum = value;
    }

    /**
     * Gets the value of the reverseCharge property.
     * This getter has been renamed from isReverseCharge() to getReverseCharge() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getReverseCharge() {
        return reverseCharge;
    }

    /**
     * Sets the value of the reverseCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReverseCharge(Boolean value) {
        this.reverseCharge = value;
    }

    /**
     * Gets the value of the rpt1DedTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1DedTaxAmt() {
        return rpt1DedTaxAmt;
    }

    /**
     * Sets the value of the rpt1DedTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1DedTaxAmt(BigDecimal value) {
        this.rpt1DedTaxAmt = value;
    }

    /**
     * Gets the value of the rpt1DefTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1DefTaxAmt() {
        return rpt1DefTaxAmt;
    }

    /**
     * Sets the value of the rpt1DefTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1DefTaxAmt(BigDecimal value) {
        this.rpt1DefTaxAmt = value;
    }

    /**
     * Gets the value of the rpt1DefTaxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1DefTaxableAmt() {
        return rpt1DefTaxableAmt;
    }

    /**
     * Sets the value of the rpt1DefTaxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1DefTaxableAmt(BigDecimal value) {
        this.rpt1DefTaxableAmt = value;
    }

    /**
     * Gets the value of the rpt1Discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1Discount() {
        return rpt1Discount;
    }

    /**
     * Sets the value of the rpt1Discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1Discount(BigDecimal value) {
        this.rpt1Discount = value;
    }

    /**
     * Gets the value of the rpt1FixedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1FixedAmount() {
        return rpt1FixedAmount;
    }

    /**
     * Sets the value of the rpt1FixedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1FixedAmount(BigDecimal value) {
        this.rpt1FixedAmount = value;
    }

    /**
     * Gets the value of the rpt1ReportableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1ReportableAmt() {
        return rpt1ReportableAmt;
    }

    /**
     * Sets the value of the rpt1ReportableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1ReportableAmt(BigDecimal value) {
        this.rpt1ReportableAmt = value;
    }

    /**
     * Gets the value of the rpt1TaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TaxAmt() {
        return rpt1TaxAmt;
    }

    /**
     * Sets the value of the rpt1TaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TaxAmt(BigDecimal value) {
        this.rpt1TaxAmt = value;
    }

    /**
     * Gets the value of the rpt1TaxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TaxableAmt() {
        return rpt1TaxableAmt;
    }

    /**
     * Sets the value of the rpt1TaxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TaxableAmt(BigDecimal value) {
        this.rpt1TaxableAmt = value;
    }

    /**
     * Gets the value of the rpt2DedTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2DedTaxAmt() {
        return rpt2DedTaxAmt;
    }

    /**
     * Sets the value of the rpt2DedTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2DedTaxAmt(BigDecimal value) {
        this.rpt2DedTaxAmt = value;
    }

    /**
     * Gets the value of the rpt2DefTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2DefTaxAmt() {
        return rpt2DefTaxAmt;
    }

    /**
     * Sets the value of the rpt2DefTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2DefTaxAmt(BigDecimal value) {
        this.rpt2DefTaxAmt = value;
    }

    /**
     * Gets the value of the rpt2DefTaxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2DefTaxableAmt() {
        return rpt2DefTaxableAmt;
    }

    /**
     * Sets the value of the rpt2DefTaxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2DefTaxableAmt(BigDecimal value) {
        this.rpt2DefTaxableAmt = value;
    }

    /**
     * Gets the value of the rpt2Discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2Discount() {
        return rpt2Discount;
    }

    /**
     * Sets the value of the rpt2Discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2Discount(BigDecimal value) {
        this.rpt2Discount = value;
    }

    /**
     * Gets the value of the rpt2FixedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2FixedAmount() {
        return rpt2FixedAmount;
    }

    /**
     * Sets the value of the rpt2FixedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2FixedAmount(BigDecimal value) {
        this.rpt2FixedAmount = value;
    }

    /**
     * Gets the value of the rpt2ReportableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2ReportableAmt() {
        return rpt2ReportableAmt;
    }

    /**
     * Sets the value of the rpt2ReportableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2ReportableAmt(BigDecimal value) {
        this.rpt2ReportableAmt = value;
    }

    /**
     * Gets the value of the rpt2TaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TaxAmt() {
        return rpt2TaxAmt;
    }

    /**
     * Sets the value of the rpt2TaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TaxAmt(BigDecimal value) {
        this.rpt2TaxAmt = value;
    }

    /**
     * Gets the value of the rpt2TaxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TaxableAmt() {
        return rpt2TaxableAmt;
    }

    /**
     * Sets the value of the rpt2TaxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TaxableAmt(BigDecimal value) {
        this.rpt2TaxableAmt = value;
    }

    /**
     * Gets the value of the rpt3DedTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3DedTaxAmt() {
        return rpt3DedTaxAmt;
    }

    /**
     * Sets the value of the rpt3DedTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3DedTaxAmt(BigDecimal value) {
        this.rpt3DedTaxAmt = value;
    }

    /**
     * Gets the value of the rpt3DefTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3DefTaxAmt() {
        return rpt3DefTaxAmt;
    }

    /**
     * Sets the value of the rpt3DefTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3DefTaxAmt(BigDecimal value) {
        this.rpt3DefTaxAmt = value;
    }

    /**
     * Gets the value of the rpt3DefTaxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3DefTaxableAmt() {
        return rpt3DefTaxableAmt;
    }

    /**
     * Sets the value of the rpt3DefTaxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3DefTaxableAmt(BigDecimal value) {
        this.rpt3DefTaxableAmt = value;
    }

    /**
     * Gets the value of the rpt3Discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3Discount() {
        return rpt3Discount;
    }

    /**
     * Sets the value of the rpt3Discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3Discount(BigDecimal value) {
        this.rpt3Discount = value;
    }

    /**
     * Gets the value of the rpt3FixedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3FixedAmount() {
        return rpt3FixedAmount;
    }

    /**
     * Sets the value of the rpt3FixedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3FixedAmount(BigDecimal value) {
        this.rpt3FixedAmount = value;
    }

    /**
     * Gets the value of the rpt3ReportableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3ReportableAmt() {
        return rpt3ReportableAmt;
    }

    /**
     * Sets the value of the rpt3ReportableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3ReportableAmt(BigDecimal value) {
        this.rpt3ReportableAmt = value;
    }

    /**
     * Gets the value of the rpt3TaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TaxAmt() {
        return rpt3TaxAmt;
    }

    /**
     * Sets the value of the rpt3TaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TaxAmt(BigDecimal value) {
        this.rpt3TaxAmt = value;
    }

    /**
     * Gets the value of the rpt3TaxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TaxableAmt() {
        return rpt3TaxableAmt;
    }

    /**
     * Sets the value of the rpt3TaxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TaxableAmt(BigDecimal value) {
        this.rpt3TaxableAmt = value;
    }

    /**
     * Gets the value of the salesTaxDescDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesTaxDescDescription() {
        return salesTaxDescDescription;
    }

    /**
     * Sets the value of the salesTaxDescDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesTaxDescDescription(JAXBElement<String> value) {
        this.salesTaxDescDescription = value;
    }

    /**
     * Gets the value of the sysRevID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSysRevID() {
        return sysRevID;
    }

    /**
     * Sets the value of the sysRevID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSysRevID(Long value) {
        this.sysRevID = value;
    }

    /**
     * Gets the value of the taxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTaxAmt() {
        return taxAmt;
    }

    /**
     * Sets the value of the taxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxAmt(BigDecimal value) {
        this.taxAmt = value;
    }

    /**
     * Gets the value of the taxCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxCode() {
        return taxCode;
    }

    /**
     * Sets the value of the taxCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxCode(JAXBElement<String> value) {
        this.taxCode = value;
    }

    /**
     * Gets the value of the taxRateDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTaxRateDate() {
        return taxRateDate;
    }

    /**
     * Sets the value of the taxRateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTaxRateDate(JAXBElement<XMLGregorianCalendar> value) {
        this.taxRateDate = value;
    }

    /**
     * Gets the value of the taxableAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTaxableAmt() {
        return taxableAmt;
    }

    /**
     * Sets the value of the taxableAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxableAmt(BigDecimal value) {
        this.taxableAmt = value;
    }

    /**
     * Gets the value of the textCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTextCode() {
        return textCode;
    }

    /**
     * Sets the value of the textCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTextCode(JAXBElement<String> value) {
        this.textCode = value;
    }

    /**
     * Gets the value of the timing property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTiming() {
        return timing;
    }

    /**
     * Sets the value of the timing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTiming(Integer value) {
        this.timing = value;
    }

}
