
package com.tesamm.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaxConnectStatusTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaxConnectStatusTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaxConnectStatusRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}TaxConnectStatusRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxConnectStatusTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "taxConnectStatusRow"
})
public class TaxConnectStatusTable {

    @XmlElement(name = "TaxConnectStatusRow", nillable = true)
    protected List<TaxConnectStatusRow> taxConnectStatusRow;

    /**
     * Gets the value of the taxConnectStatusRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxConnectStatusRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxConnectStatusRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaxConnectStatusRow }
     * 
     * 
     */
    public List<TaxConnectStatusRow> getTaxConnectStatusRow() {
        if (taxConnectStatusRow == null) {
            taxConnectStatusRow = new ArrayList<TaxConnectStatusRow>();
        }
        return this.taxConnectStatusRow;
    }

    /**
     * Sets the value of the taxConnectStatusRow property.
     * 
     * @param taxConnectStatusRow
     *     allowed object is
     *     {@link TaxConnectStatusRow }
     *     
     */
    public void setTaxConnectStatusRow(List<TaxConnectStatusRow> taxConnectStatusRow) {
        this.taxConnectStatusRow = taxConnectStatusRow;
    }

}
