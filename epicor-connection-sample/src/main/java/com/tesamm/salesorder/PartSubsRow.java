
package com.tesamm.salesorder;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartSubsRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartSubsRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceRow">
 *       &lt;sequence>
 *         &lt;element name="BitFlag" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Comment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefaultSub" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumIUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumPartDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumPricePerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumSalesUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumSellingFactor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PartNumTrackDimension" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartNumTrackLots" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartNumTrackSerialNum" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="QtyPer" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RecType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Selected" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SubPart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubPartIUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubPartPartDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubPartPricePerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubPartSalesUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubPartSellingFactor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SubPartTrackDimension" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SubPartTrackLots" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SubPartTrackSerialNum" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SubType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SugOrderQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SuggestedQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SysRevID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartSubsRow", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "bitFlag",
    "comment",
    "company",
    "defaultSub",
    "partNum",
    "partNumIUM",
    "partNumPartDescription",
    "partNumPricePerCode",
    "partNumSalesUM",
    "partNumSellingFactor",
    "partNumTrackDimension",
    "partNumTrackLots",
    "partNumTrackSerialNum",
    "price",
    "qtyPer",
    "recType",
    "salesUM",
    "selected",
    "subPart",
    "subPartIUM",
    "subPartPartDescription",
    "subPartPricePerCode",
    "subPartSalesUM",
    "subPartSellingFactor",
    "subPartTrackDimension",
    "subPartTrackLots",
    "subPartTrackSerialNum",
    "subType",
    "sugOrderQty",
    "suggestedQty",
    "sysRevID"
})
public class PartSubsRow
    extends IceRow
{

    @XmlElement(name = "BitFlag")
    protected Integer bitFlag;
    @XmlElementRef(name = "Comment", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> comment;
    @XmlElementRef(name = "Company", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> company;
    @XmlElement(name = "DefaultSub")
    protected Boolean defaultSub;
    @XmlElementRef(name = "PartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNum;
    @XmlElementRef(name = "PartNumIUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNumIUM;
    @XmlElementRef(name = "PartNumPartDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNumPartDescription;
    @XmlElementRef(name = "PartNumPricePerCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNumPricePerCode;
    @XmlElementRef(name = "PartNumSalesUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNumSalesUM;
    @XmlElement(name = "PartNumSellingFactor")
    protected BigDecimal partNumSellingFactor;
    @XmlElement(name = "PartNumTrackDimension")
    protected Boolean partNumTrackDimension;
    @XmlElement(name = "PartNumTrackLots")
    protected Boolean partNumTrackLots;
    @XmlElement(name = "PartNumTrackSerialNum")
    protected Boolean partNumTrackSerialNum;
    @XmlElement(name = "Price")
    protected BigDecimal price;
    @XmlElement(name = "QtyPer")
    protected BigDecimal qtyPer;
    @XmlElementRef(name = "RecType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recType;
    @XmlElementRef(name = "SalesUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesUM;
    @XmlElement(name = "Selected")
    protected Boolean selected;
    @XmlElementRef(name = "SubPart", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subPart;
    @XmlElementRef(name = "SubPartIUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subPartIUM;
    @XmlElementRef(name = "SubPartPartDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subPartPartDescription;
    @XmlElementRef(name = "SubPartPricePerCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subPartPricePerCode;
    @XmlElementRef(name = "SubPartSalesUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subPartSalesUM;
    @XmlElement(name = "SubPartSellingFactor")
    protected BigDecimal subPartSellingFactor;
    @XmlElement(name = "SubPartTrackDimension")
    protected Boolean subPartTrackDimension;
    @XmlElement(name = "SubPartTrackLots")
    protected Boolean subPartTrackLots;
    @XmlElement(name = "SubPartTrackSerialNum")
    protected Boolean subPartTrackSerialNum;
    @XmlElementRef(name = "SubType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subType;
    @XmlElement(name = "SugOrderQty")
    protected BigDecimal sugOrderQty;
    @XmlElement(name = "SuggestedQty")
    protected BigDecimal suggestedQty;
    @XmlElement(name = "SysRevID")
    protected Long sysRevID;

    /**
     * Gets the value of the bitFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBitFlag() {
        return bitFlag;
    }

    /**
     * Sets the value of the bitFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBitFlag(Integer value) {
        this.bitFlag = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setComment(JAXBElement<String> value) {
        this.comment = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompany(JAXBElement<String> value) {
        this.company = value;
    }

    /**
     * Gets the value of the defaultSub property.
     * This getter has been renamed from isDefaultSub() to getDefaultSub() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDefaultSub() {
        return defaultSub;
    }

    /**
     * Sets the value of the defaultSub property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultSub(Boolean value) {
        this.defaultSub = value;
    }

    /**
     * Gets the value of the partNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNum() {
        return partNum;
    }

    /**
     * Sets the value of the partNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNum(JAXBElement<String> value) {
        this.partNum = value;
    }

    /**
     * Gets the value of the partNumIUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNumIUM() {
        return partNumIUM;
    }

    /**
     * Sets the value of the partNumIUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNumIUM(JAXBElement<String> value) {
        this.partNumIUM = value;
    }

    /**
     * Gets the value of the partNumPartDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNumPartDescription() {
        return partNumPartDescription;
    }

    /**
     * Sets the value of the partNumPartDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNumPartDescription(JAXBElement<String> value) {
        this.partNumPartDescription = value;
    }

    /**
     * Gets the value of the partNumPricePerCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNumPricePerCode() {
        return partNumPricePerCode;
    }

    /**
     * Sets the value of the partNumPricePerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNumPricePerCode(JAXBElement<String> value) {
        this.partNumPricePerCode = value;
    }

    /**
     * Gets the value of the partNumSalesUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNumSalesUM() {
        return partNumSalesUM;
    }

    /**
     * Sets the value of the partNumSalesUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNumSalesUM(JAXBElement<String> value) {
        this.partNumSalesUM = value;
    }

    /**
     * Gets the value of the partNumSellingFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPartNumSellingFactor() {
        return partNumSellingFactor;
    }

    /**
     * Sets the value of the partNumSellingFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPartNumSellingFactor(BigDecimal value) {
        this.partNumSellingFactor = value;
    }

    /**
     * Gets the value of the partNumTrackDimension property.
     * This getter has been renamed from isPartNumTrackDimension() to getPartNumTrackDimension() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartNumTrackDimension() {
        return partNumTrackDimension;
    }

    /**
     * Sets the value of the partNumTrackDimension property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartNumTrackDimension(Boolean value) {
        this.partNumTrackDimension = value;
    }

    /**
     * Gets the value of the partNumTrackLots property.
     * This getter has been renamed from isPartNumTrackLots() to getPartNumTrackLots() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartNumTrackLots() {
        return partNumTrackLots;
    }

    /**
     * Sets the value of the partNumTrackLots property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartNumTrackLots(Boolean value) {
        this.partNumTrackLots = value;
    }

    /**
     * Gets the value of the partNumTrackSerialNum property.
     * This getter has been renamed from isPartNumTrackSerialNum() to getPartNumTrackSerialNum() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartNumTrackSerialNum() {
        return partNumTrackSerialNum;
    }

    /**
     * Sets the value of the partNumTrackSerialNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartNumTrackSerialNum(Boolean value) {
        this.partNumTrackSerialNum = value;
    }

    /**
     * Gets the value of the price property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrice(BigDecimal value) {
        this.price = value;
    }

    /**
     * Gets the value of the qtyPer property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQtyPer() {
        return qtyPer;
    }

    /**
     * Sets the value of the qtyPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQtyPer(BigDecimal value) {
        this.qtyPer = value;
    }

    /**
     * Gets the value of the recType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecType() {
        return recType;
    }

    /**
     * Sets the value of the recType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecType(JAXBElement<String> value) {
        this.recType = value;
    }

    /**
     * Gets the value of the salesUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesUM() {
        return salesUM;
    }

    /**
     * Sets the value of the salesUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesUM(JAXBElement<String> value) {
        this.salesUM = value;
    }

    /**
     * Gets the value of the selected property.
     * This getter has been renamed from isSelected() to getSelected() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSelected() {
        return selected;
    }

    /**
     * Sets the value of the selected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSelected(Boolean value) {
        this.selected = value;
    }

    /**
     * Gets the value of the subPart property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubPart() {
        return subPart;
    }

    /**
     * Sets the value of the subPart property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubPart(JAXBElement<String> value) {
        this.subPart = value;
    }

    /**
     * Gets the value of the subPartIUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubPartIUM() {
        return subPartIUM;
    }

    /**
     * Sets the value of the subPartIUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubPartIUM(JAXBElement<String> value) {
        this.subPartIUM = value;
    }

    /**
     * Gets the value of the subPartPartDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubPartPartDescription() {
        return subPartPartDescription;
    }

    /**
     * Sets the value of the subPartPartDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubPartPartDescription(JAXBElement<String> value) {
        this.subPartPartDescription = value;
    }

    /**
     * Gets the value of the subPartPricePerCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubPartPricePerCode() {
        return subPartPricePerCode;
    }

    /**
     * Sets the value of the subPartPricePerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubPartPricePerCode(JAXBElement<String> value) {
        this.subPartPricePerCode = value;
    }

    /**
     * Gets the value of the subPartSalesUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubPartSalesUM() {
        return subPartSalesUM;
    }

    /**
     * Sets the value of the subPartSalesUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubPartSalesUM(JAXBElement<String> value) {
        this.subPartSalesUM = value;
    }

    /**
     * Gets the value of the subPartSellingFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSubPartSellingFactor() {
        return subPartSellingFactor;
    }

    /**
     * Sets the value of the subPartSellingFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSubPartSellingFactor(BigDecimal value) {
        this.subPartSellingFactor = value;
    }

    /**
     * Gets the value of the subPartTrackDimension property.
     * This getter has been renamed from isSubPartTrackDimension() to getSubPartTrackDimension() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSubPartTrackDimension() {
        return subPartTrackDimension;
    }

    /**
     * Sets the value of the subPartTrackDimension property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSubPartTrackDimension(Boolean value) {
        this.subPartTrackDimension = value;
    }

    /**
     * Gets the value of the subPartTrackLots property.
     * This getter has been renamed from isSubPartTrackLots() to getSubPartTrackLots() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSubPartTrackLots() {
        return subPartTrackLots;
    }

    /**
     * Sets the value of the subPartTrackLots property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSubPartTrackLots(Boolean value) {
        this.subPartTrackLots = value;
    }

    /**
     * Gets the value of the subPartTrackSerialNum property.
     * This getter has been renamed from isSubPartTrackSerialNum() to getSubPartTrackSerialNum() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSubPartTrackSerialNum() {
        return subPartTrackSerialNum;
    }

    /**
     * Sets the value of the subPartTrackSerialNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSubPartTrackSerialNum(Boolean value) {
        this.subPartTrackSerialNum = value;
    }

    /**
     * Gets the value of the subType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubType() {
        return subType;
    }

    /**
     * Sets the value of the subType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubType(JAXBElement<String> value) {
        this.subType = value;
    }

    /**
     * Gets the value of the sugOrderQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSugOrderQty() {
        return sugOrderQty;
    }

    /**
     * Sets the value of the sugOrderQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSugOrderQty(BigDecimal value) {
        this.sugOrderQty = value;
    }

    /**
     * Gets the value of the suggestedQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSuggestedQty() {
        return suggestedQty;
    }

    /**
     * Sets the value of the suggestedQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSuggestedQty(BigDecimal value) {
        this.suggestedQty = value;
    }

    /**
     * Gets the value of the sysRevID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSysRevID() {
        return sysRevID;
    }

    /**
     * Sets the value of the sysRevID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSysRevID(Long value) {
        this.sysRevID = value;
    }

}
