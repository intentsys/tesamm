
package com.tesamm.salesorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartSubsTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartSubsTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartSubsRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}PartSubsRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartSubsTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "partSubsRow"
})
public class PartSubsTable {

    @XmlElement(name = "PartSubsRow", nillable = true)
    protected List<PartSubsRow> partSubsRow;

    /**
     * Gets the value of the partSubsRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partSubsRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartSubsRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartSubsRow }
     * 
     * 
     */
    public List<PartSubsRow> getPartSubsRow() {
        if (partSubsRow == null) {
            partSubsRow = new ArrayList<PartSubsRow>();
        }
        return this.partSubsRow;
    }

    /**
     * Sets the value of the partSubsRow property.
     * 
     * @param partSubsRow
     *     allowed object is
     *     {@link PartSubsRow }
     *     
     */
    public void setPartSubsRow(List<PartSubsRow> partSubsRow) {
        this.partSubsRow = partSubsRow;
    }

}
