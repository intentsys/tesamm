
package com.tesamm.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="partNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lSubstitutePartExist" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="lIsPhantom" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="uomCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cDeleteComponentsMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="questionString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cWarningMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="multipleMatch" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="promptToExplodeBOM" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="cConfigPartMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cSubPartMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="explodeBOMerrMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cMsgType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="multiSubsAvail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="runOutQtyAvail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ds" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SalesOrderTableset" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "partNum",
    "lSubstitutePartExist",
    "lIsPhantom",
    "uomCode",
    "cDeleteComponentsMessage",
    "questionString",
    "cWarningMessage",
    "multipleMatch",
    "promptToExplodeBOM",
    "cConfigPartMessage",
    "cSubPartMessage",
    "explodeBOMerrMessage",
    "cMsgType",
    "multiSubsAvail",
    "runOutQtyAvail",
    "ds"
})
@XmlRootElement(name = "ChangePartNumMasterResponse")
public class ChangePartNumMasterResponse {

    @XmlElementRef(name = "partNum", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNum;
    protected Boolean lSubstitutePartExist;
    protected Boolean lIsPhantom;
    @XmlElementRef(name = "uomCode", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> uomCode;
    @XmlElementRef(name = "cDeleteComponentsMessage", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cDeleteComponentsMessage;
    @XmlElementRef(name = "questionString", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> questionString;
    @XmlElementRef(name = "cWarningMessage", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cWarningMessage;
    protected Boolean multipleMatch;
    protected Boolean promptToExplodeBOM;
    @XmlElementRef(name = "cConfigPartMessage", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cConfigPartMessage;
    @XmlElementRef(name = "cSubPartMessage", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cSubPartMessage;
    @XmlElementRef(name = "explodeBOMerrMessage", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> explodeBOMerrMessage;
    @XmlElementRef(name = "cMsgType", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cMsgType;
    protected Boolean multiSubsAvail;
    protected Boolean runOutQtyAvail;
    @XmlElementRef(name = "ds", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<SalesOrderTableset> ds;

    /**
     * Gets the value of the partNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNum() {
        return partNum;
    }

    /**
     * Sets the value of the partNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNum(JAXBElement<String> value) {
        this.partNum = value;
    }

    /**
     * Gets the value of the lSubstitutePartExist property.
     * This getter has been renamed from isLSubstitutePartExist() to getLSubstitutePartExist() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLSubstitutePartExist() {
        return lSubstitutePartExist;
    }

    /**
     * Sets the value of the lSubstitutePartExist property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLSubstitutePartExist(Boolean value) {
        this.lSubstitutePartExist = value;
    }

    /**
     * Gets the value of the lIsPhantom property.
     * This getter has been renamed from isLIsPhantom() to getLIsPhantom() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLIsPhantom() {
        return lIsPhantom;
    }

    /**
     * Sets the value of the lIsPhantom property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLIsPhantom(Boolean value) {
        this.lIsPhantom = value;
    }

    /**
     * Gets the value of the uomCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUomCode() {
        return uomCode;
    }

    /**
     * Sets the value of the uomCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUomCode(JAXBElement<String> value) {
        this.uomCode = value;
    }

    /**
     * Gets the value of the cDeleteComponentsMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCDeleteComponentsMessage() {
        return cDeleteComponentsMessage;
    }

    /**
     * Sets the value of the cDeleteComponentsMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCDeleteComponentsMessage(JAXBElement<String> value) {
        this.cDeleteComponentsMessage = value;
    }

    /**
     * Gets the value of the questionString property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getQuestionString() {
        return questionString;
    }

    /**
     * Sets the value of the questionString property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setQuestionString(JAXBElement<String> value) {
        this.questionString = value;
    }

    /**
     * Gets the value of the cWarningMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCWarningMessage() {
        return cWarningMessage;
    }

    /**
     * Sets the value of the cWarningMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCWarningMessage(JAXBElement<String> value) {
        this.cWarningMessage = value;
    }

    /**
     * Gets the value of the multipleMatch property.
     * This getter has been renamed from isMultipleMatch() to getMultipleMatch() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMultipleMatch() {
        return multipleMatch;
    }

    /**
     * Sets the value of the multipleMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultipleMatch(Boolean value) {
        this.multipleMatch = value;
    }

    /**
     * Gets the value of the promptToExplodeBOM property.
     * This getter has been renamed from isPromptToExplodeBOM() to getPromptToExplodeBOM() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPromptToExplodeBOM() {
        return promptToExplodeBOM;
    }

    /**
     * Sets the value of the promptToExplodeBOM property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPromptToExplodeBOM(Boolean value) {
        this.promptToExplodeBOM = value;
    }

    /**
     * Gets the value of the cConfigPartMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCConfigPartMessage() {
        return cConfigPartMessage;
    }

    /**
     * Sets the value of the cConfigPartMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCConfigPartMessage(JAXBElement<String> value) {
        this.cConfigPartMessage = value;
    }

    /**
     * Gets the value of the cSubPartMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCSubPartMessage() {
        return cSubPartMessage;
    }

    /**
     * Sets the value of the cSubPartMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCSubPartMessage(JAXBElement<String> value) {
        this.cSubPartMessage = value;
    }

    /**
     * Gets the value of the explodeBOMerrMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExplodeBOMerrMessage() {
        return explodeBOMerrMessage;
    }

    /**
     * Sets the value of the explodeBOMerrMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExplodeBOMerrMessage(JAXBElement<String> value) {
        this.explodeBOMerrMessage = value;
    }

    /**
     * Gets the value of the cMsgType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCMsgType() {
        return cMsgType;
    }

    /**
     * Sets the value of the cMsgType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCMsgType(JAXBElement<String> value) {
        this.cMsgType = value;
    }

    /**
     * Gets the value of the multiSubsAvail property.
     * This getter has been renamed from isMultiSubsAvail() to getMultiSubsAvail() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMultiSubsAvail() {
        return multiSubsAvail;
    }

    /**
     * Sets the value of the multiSubsAvail property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultiSubsAvail(Boolean value) {
        this.multiSubsAvail = value;
    }

    /**
     * Gets the value of the runOutQtyAvail property.
     * This getter has been renamed from isRunOutQtyAvail() to getRunOutQtyAvail() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRunOutQtyAvail() {
        return runOutQtyAvail;
    }

    /**
     * Sets the value of the runOutQtyAvail property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRunOutQtyAvail(Boolean value) {
        this.runOutQtyAvail = value;
    }

    /**
     * Gets the value of the ds property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}
     *     
     */
    public JAXBElement<SalesOrderTableset> getDs() {
        return ds;
    }

    /**
     * Sets the value of the ds property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}
     *     
     */
    public void setDs(JAXBElement<SalesOrderTableset> value) {
        this.ds = value;
    }

}
