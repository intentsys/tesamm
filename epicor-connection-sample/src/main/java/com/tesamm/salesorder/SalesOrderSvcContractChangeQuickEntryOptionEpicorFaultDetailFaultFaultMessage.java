
package com.tesamm.salesorder;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.5.1
 * 2016-01-20T22:00:56.162+08:00
 * Generated source version: 2.5.1
 */

@WebFault(name = "EpicorFaultDetail", targetNamespace = "http://schemas.datacontract.org/2004/07/Ice.Common")
public class SalesOrderSvcContractChangeQuickEntryOptionEpicorFaultDetailFaultFaultMessage extends Exception {
    
    private com.tesamm.salesorder.EpicorFaultDetail epicorFaultDetail;

    public SalesOrderSvcContractChangeQuickEntryOptionEpicorFaultDetailFaultFaultMessage() {
        super();
    }
    
    public SalesOrderSvcContractChangeQuickEntryOptionEpicorFaultDetailFaultFaultMessage(String message) {
        super(message);
    }
    
    public SalesOrderSvcContractChangeQuickEntryOptionEpicorFaultDetailFaultFaultMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public SalesOrderSvcContractChangeQuickEntryOptionEpicorFaultDetailFaultFaultMessage(String message, com.tesamm.salesorder.EpicorFaultDetail epicorFaultDetail) {
        super(message);
        this.epicorFaultDetail = epicorFaultDetail;
    }

    public SalesOrderSvcContractChangeQuickEntryOptionEpicorFaultDetailFaultFaultMessage(String message, com.tesamm.salesorder.EpicorFaultDetail epicorFaultDetail, Throwable cause) {
        super(message, cause);
        this.epicorFaultDetail = epicorFaultDetail;
    }

    public com.tesamm.salesorder.EpicorFaultDetail getFaultInfo() {
        return this.epicorFaultDetail;
    }
}
