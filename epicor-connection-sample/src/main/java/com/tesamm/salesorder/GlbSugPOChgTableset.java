
package com.tesamm.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GlbSugPOChgTableset complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GlbSugPOChgTableset">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceTableset">
 *       &lt;sequence>
 *         &lt;element name="GlbSugPOChg" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}GlbSugPOChgTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GlbSugPOChgTableset", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "glbSugPOChg"
})
public class GlbSugPOChgTableset
    extends IceTableset
{

    @XmlElementRef(name = "GlbSugPOChg", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<GlbSugPOChgTable> glbSugPOChg;

    /**
     * Gets the value of the glbSugPOChg property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GlbSugPOChgTable }{@code >}
     *     
     */
    public JAXBElement<GlbSugPOChgTable> getGlbSugPOChg() {
        return glbSugPOChg;
    }

    /**
     * Sets the value of the glbSugPOChg property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GlbSugPOChgTable }{@code >}
     *     
     */
    public void setGlbSugPOChg(JAXBElement<GlbSugPOChgTable> value) {
        this.glbSugPOChg = value;
    }

}
