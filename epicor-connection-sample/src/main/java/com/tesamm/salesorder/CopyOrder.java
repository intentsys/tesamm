
package com.tesamm.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orderNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ipPONum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ipOpenRel" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ipReNbr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ipPreserve" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ipCalc" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "orderNum",
    "ipPONum",
    "ipOpenRel",
    "ipReNbr",
    "ipPreserve",
    "ipCalc"
})
@XmlRootElement(name = "CopyOrder")
public class CopyOrder {

    protected Integer orderNum;
    @XmlElementRef(name = "ipPONum", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ipPONum;
    protected Boolean ipOpenRel;
    protected Boolean ipReNbr;
    protected Boolean ipPreserve;
    protected Boolean ipCalc;

    /**
     * Gets the value of the orderNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * Sets the value of the orderNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNum(Integer value) {
        this.orderNum = value;
    }

    /**
     * Gets the value of the ipPONum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIpPONum() {
        return ipPONum;
    }

    /**
     * Sets the value of the ipPONum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIpPONum(JAXBElement<String> value) {
        this.ipPONum = value;
    }

    /**
     * Gets the value of the ipOpenRel property.
     * This getter has been renamed from isIpOpenRel() to getIpOpenRel() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIpOpenRel() {
        return ipOpenRel;
    }

    /**
     * Sets the value of the ipOpenRel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIpOpenRel(Boolean value) {
        this.ipOpenRel = value;
    }

    /**
     * Gets the value of the ipReNbr property.
     * This getter has been renamed from isIpReNbr() to getIpReNbr() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIpReNbr() {
        return ipReNbr;
    }

    /**
     * Sets the value of the ipReNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIpReNbr(Boolean value) {
        this.ipReNbr = value;
    }

    /**
     * Gets the value of the ipPreserve property.
     * This getter has been renamed from isIpPreserve() to getIpPreserve() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIpPreserve() {
        return ipPreserve;
    }

    /**
     * Sets the value of the ipPreserve property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIpPreserve(Boolean value) {
        this.ipPreserve = value;
    }

    /**
     * Gets the value of the ipCalc property.
     * This getter has been renamed from isIpCalc() to getIpCalc() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIpCalc() {
        return ipCalc;
    }

    /**
     * Sets the value of the ipCalc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIpCalc(Boolean value) {
        this.ipCalc = value;
    }

}
