
package com.tesamm.salesorder;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ds" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SalesOrderTableset" minOccurs="0"/>
 *         &lt;element name="ipSellingQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="chkSellQty" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="negInvTest" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="chgSellQty" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="chgDiscPer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="suppressUserPrompts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="lKeepUnitPrice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="pcPartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pcWhseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pcBinNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pcLotNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pcDimCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pdDimConvFactor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ds",
    "ipSellingQuantity",
    "chkSellQty",
    "negInvTest",
    "chgSellQty",
    "chgDiscPer",
    "suppressUserPrompts",
    "lKeepUnitPrice",
    "pcPartNum",
    "pcWhseCode",
    "pcBinNum",
    "pcLotNum",
    "pcDimCode",
    "pdDimConvFactor"
})
@XmlRootElement(name = "ChangeSellingQtyMaster")
public class ChangeSellingQtyMaster {

    @XmlElementRef(name = "ds", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<SalesOrderTableset> ds;
    protected BigDecimal ipSellingQuantity;
    protected Boolean chkSellQty;
    protected Boolean negInvTest;
    protected Boolean chgSellQty;
    protected Boolean chgDiscPer;
    protected Boolean suppressUserPrompts;
    protected Boolean lKeepUnitPrice;
    @XmlElementRef(name = "pcPartNum", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pcPartNum;
    @XmlElementRef(name = "pcWhseCode", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pcWhseCode;
    @XmlElementRef(name = "pcBinNum", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pcBinNum;
    @XmlElementRef(name = "pcLotNum", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pcLotNum;
    @XmlElementRef(name = "pcDimCode", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pcDimCode;
    protected BigDecimal pdDimConvFactor;

    /**
     * Gets the value of the ds property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}
     *     
     */
    public JAXBElement<SalesOrderTableset> getDs() {
        return ds;
    }

    /**
     * Sets the value of the ds property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}
     *     
     */
    public void setDs(JAXBElement<SalesOrderTableset> value) {
        this.ds = value;
    }

    /**
     * Gets the value of the ipSellingQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIpSellingQuantity() {
        return ipSellingQuantity;
    }

    /**
     * Sets the value of the ipSellingQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIpSellingQuantity(BigDecimal value) {
        this.ipSellingQuantity = value;
    }

    /**
     * Gets the value of the chkSellQty property.
     * This getter has been renamed from isChkSellQty() to getChkSellQty() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getChkSellQty() {
        return chkSellQty;
    }

    /**
     * Sets the value of the chkSellQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChkSellQty(Boolean value) {
        this.chkSellQty = value;
    }

    /**
     * Gets the value of the negInvTest property.
     * This getter has been renamed from isNegInvTest() to getNegInvTest() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNegInvTest() {
        return negInvTest;
    }

    /**
     * Sets the value of the negInvTest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNegInvTest(Boolean value) {
        this.negInvTest = value;
    }

    /**
     * Gets the value of the chgSellQty property.
     * This getter has been renamed from isChgSellQty() to getChgSellQty() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getChgSellQty() {
        return chgSellQty;
    }

    /**
     * Sets the value of the chgSellQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChgSellQty(Boolean value) {
        this.chgSellQty = value;
    }

    /**
     * Gets the value of the chgDiscPer property.
     * This getter has been renamed from isChgDiscPer() to getChgDiscPer() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getChgDiscPer() {
        return chgDiscPer;
    }

    /**
     * Sets the value of the chgDiscPer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChgDiscPer(Boolean value) {
        this.chgDiscPer = value;
    }

    /**
     * Gets the value of the suppressUserPrompts property.
     * This getter has been renamed from isSuppressUserPrompts() to getSuppressUserPrompts() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSuppressUserPrompts() {
        return suppressUserPrompts;
    }

    /**
     * Sets the value of the suppressUserPrompts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSuppressUserPrompts(Boolean value) {
        this.suppressUserPrompts = value;
    }

    /**
     * Gets the value of the lKeepUnitPrice property.
     * This getter has been renamed from isLKeepUnitPrice() to getLKeepUnitPrice() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLKeepUnitPrice() {
        return lKeepUnitPrice;
    }

    /**
     * Sets the value of the lKeepUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLKeepUnitPrice(Boolean value) {
        this.lKeepUnitPrice = value;
    }

    /**
     * Gets the value of the pcPartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPcPartNum() {
        return pcPartNum;
    }

    /**
     * Sets the value of the pcPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPcPartNum(JAXBElement<String> value) {
        this.pcPartNum = value;
    }

    /**
     * Gets the value of the pcWhseCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPcWhseCode() {
        return pcWhseCode;
    }

    /**
     * Sets the value of the pcWhseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPcWhseCode(JAXBElement<String> value) {
        this.pcWhseCode = value;
    }

    /**
     * Gets the value of the pcBinNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPcBinNum() {
        return pcBinNum;
    }

    /**
     * Sets the value of the pcBinNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPcBinNum(JAXBElement<String> value) {
        this.pcBinNum = value;
    }

    /**
     * Gets the value of the pcLotNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPcLotNum() {
        return pcLotNum;
    }

    /**
     * Sets the value of the pcLotNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPcLotNum(JAXBElement<String> value) {
        this.pcLotNum = value;
    }

    /**
     * Gets the value of the pcDimCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPcDimCode() {
        return pcDimCode;
    }

    /**
     * Sets the value of the pcDimCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPcDimCode(JAXBElement<String> value) {
        this.pcDimCode = value;
    }

    /**
     * Gets the value of the pdDimConvFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPdDimConvFactor() {
        return pdDimConvFactor;
    }

    /**
     * Sets the value of the pdDimConvFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPdDimConvFactor(BigDecimal value) {
        this.pdDimConvFactor = value;
    }

}
