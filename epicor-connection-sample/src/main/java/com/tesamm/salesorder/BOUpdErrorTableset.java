
package com.tesamm.salesorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BOUpdErrorTableset complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BOUpdErrorTableset">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceTableset">
 *       &lt;sequence>
 *         &lt;element name="BOUpdError" type="{http://schemas.datacontract.org/2004/07/Ice}BOUpdErrorTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BOUpdErrorTableset", namespace = "http://schemas.datacontract.org/2004/07/Ice", propOrder = {
    "boUpdError"
})
public class BOUpdErrorTableset
    extends IceTableset
{

    @XmlElementRef(name = "BOUpdError", namespace = "http://schemas.datacontract.org/2004/07/Ice", type = JAXBElement.class, required = false)
    protected JAXBElement<BOUpdErrorTable> boUpdError;

    /**
     * Gets the value of the boUpdError property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BOUpdErrorTable }{@code >}
     *     
     */
    public JAXBElement<BOUpdErrorTable> getBOUpdError() {
        return boUpdError;
    }

    /**
     * Sets the value of the boUpdError property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BOUpdErrorTable }{@code >}
     *     
     */
    public void setBOUpdError(JAXBElement<BOUpdErrorTable> value) {
        this.boUpdError = value;
    }

}
