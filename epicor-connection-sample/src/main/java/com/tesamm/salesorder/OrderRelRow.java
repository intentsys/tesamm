
package com.tesamm.salesorder;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OrderRelRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderRelRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceRow">
 *       &lt;sequence>
 *         &lt;element name="AllowTaxCodeUpd" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ApplyChrg" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AvailableQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="BitFlag" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BuyOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="BuyToOrder" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="COD" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CODAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CODCheck" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CODFreight" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ChangeTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ChangedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChrgAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ComplianceMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditLimitMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditLimitSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CumeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CumeQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustAllowOTS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CustomerAllowShipTo3" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CustomerCustID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DatePickTicketPrinted" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DeclaredAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DeclaredIns" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DeliveryType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DemandReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DemandSchedRejected" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DocOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DocSelfAssessTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTotalTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocWithholdTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DockingStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DropShip" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DropShipName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DropShipOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DspInvMeth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DspRevMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ECCPlant" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EnableMake" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EntityUseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntryProcess" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExistPOSugg" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ExtCompany" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FirmRelease" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="GetDfltTaxIds" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="GroundType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Hazmat" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="HdrOTS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ICPOLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ICPONum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ICPORelNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LinkToPONum" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Linked" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Location" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MFCustID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MFCustNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Make" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MakeOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MarkForAddrList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MarkForNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NeedByDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="NotCompliant" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="NotifyEMail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NotifyFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OTMFAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTMFAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTMFAddress3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTMFCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTMFContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTMFCountryDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTMFCountryNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OTMFFaxNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTMFName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTMFPhoneNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTMFState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTMFZIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSAddress3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSCntryDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSCountryNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OTSCustSaved" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OTSFaxNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSPhoneNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSResaleID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSSaveAs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSSaveCustID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSSaved" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OTSShipToNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSTaxRegionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSZIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OnHandQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OpenOrder" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OpenRelease" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OrderLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OrderLineLineDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OrderNumCardMemberName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderNumCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderRelNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OurJobQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OurJobShippedQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OurReqQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OurStockQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OurStockShippedQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OverrideCarrier" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OverrideService" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="POLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PONum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PORelNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PartExists" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumIUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumPartDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumPricePerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumSalesUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNumSellingFactor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PartNumTrackDimension" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartNumTrackLots" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartNumTrackSerialNum" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PhaseID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhaseWasRecInvoiced" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PickError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Plant" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlantName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrevNeedByDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PrevPartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrevReqDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PrevSellQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PrevShipToNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrevXPartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProjectID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurPointAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurPointAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurPointAddress3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurPointCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurPointCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurPointName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurPointPrimPCon" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PurPointState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PurPointZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RAN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RefNotes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RelStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReleaseStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReqDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ResDelivery" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="RevisionNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Rpt1SelfAssessTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TotalTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1WithholdTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2SelfAssessTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TotalTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2WithholdTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3SelfAssessTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TotalTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3WithholdTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SNEnable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SalesOrderLinked" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SalesUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SatDelivery" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SatPickup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ScheduleNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SelectForPicking" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SelfAssessTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SellingFactor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SellingFactorDirection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SellingJobQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SellingJobShippedQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SellingReqQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SellingStockQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SellingStockShippedQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ServAOD" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ServAlert" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ServAuthNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServDeliveryDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ServHomeDel" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ServInstruct" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServPOD" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ServPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServRef1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServRef2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServRef3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServRef4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServRef5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServRelease" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ServSatDelivery" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ServSatPickup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ServSignature" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ShipOvers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ShipRouting" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToAddressList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToContactEMailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToContactName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToCustNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShipToNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToSelected" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ShipViaCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipViaCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipViaCodeWebDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipbyTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShpConNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="StagingBinNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StagingWarehouseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubShipTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SysRevID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="TPShipToBTName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TPShipToCustID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TPShipToName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxConnectCalc" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TaxExempt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxRegionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxRegionCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ThisRelInvtyQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalJobStockShipped" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TransportID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UpdateMarkForRecords" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="UseOTMF" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="UseOTS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="VendorNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="VendorNumAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VendorNumAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VendorNumAddress3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VendorNumCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VendorNumCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VendorNumCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VendorNumDefaultFOB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VendorNumName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VendorNumState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VendorNumTermsCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VendorNumVendorID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VendorNumZIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VerbalConf" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="VoidOrder" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="VoidRelease" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="WIItemPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="WIItemShipCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="WIOrder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WIOrderLine" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WarehouseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WarehouseCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WasRecInvoiced" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="WebSKU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WithholdTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderRelRow", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "allowTaxCodeUpd",
    "applyChrg",
    "availableQuantity",
    "bitFlag",
    "buyOverride",
    "buyToOrder",
    "cod",
    "codAmount",
    "codCheck",
    "codFreight",
    "changeDate",
    "changeTime",
    "changedBy",
    "chrgAmount",
    "company",
    "complianceMsg",
    "creditLimitMessage",
    "creditLimitSource",
    "cumeDate",
    "cumeQty",
    "currencyCode",
    "custAllowOTS",
    "customerAllowShipTo3",
    "customerCustID",
    "customerName",
    "datePickTicketPrinted",
    "declaredAmt",
    "declaredIns",
    "deliveryType",
    "demandReference",
    "demandSchedRejected",
    "docOnly",
    "docSelfAssessTax",
    "docTotalTax",
    "docWithholdTax",
    "dockingStation",
    "dropShip",
    "dropShipName",
    "dropShipOverride",
    "dspInvMeth",
    "dspRevMethod",
    "eccPlant",
    "enableMake",
    "entityUseCode",
    "entryProcess",
    "existPOSugg",
    "extCompany",
    "firmRelease",
    "getDfltTaxIds",
    "groundType",
    "hazmat",
    "hdrOTS",
    "icpoLine",
    "icpoNum",
    "icpoRelNum",
    "ium",
    "lineType",
    "linkToPONum",
    "linked",
    "location",
    "mfCustID",
    "mfCustNum",
    "make",
    "makeOverride",
    "markForAddrList",
    "markForNum",
    "needByDate",
    "notCompliant",
    "notifyEMail",
    "notifyFlag",
    "otmfAddress1",
    "otmfAddress2",
    "otmfAddress3",
    "otmfCity",
    "otmfContact",
    "otmfCountryDescription",
    "otmfCountryNum",
    "otmfFaxNum",
    "otmfName",
    "otmfPhoneNum",
    "otmfState",
    "otmfzip",
    "otsAddress1",
    "otsAddress2",
    "otsAddress3",
    "otsCity",
    "otsCntryDescription",
    "otsContact",
    "otsCountryNum",
    "otsCustSaved",
    "otsFaxNum",
    "otsName",
    "otsPhoneNum",
    "otsResaleID",
    "otsSaveAs",
    "otsSaveCustID",
    "otsSaved",
    "otsShipToNum",
    "otsState",
    "otsTaxRegionCode",
    "otszip",
    "onHandQuantity",
    "openOrder",
    "openRelease",
    "orderLine",
    "orderLineLineDesc",
    "orderNum",
    "orderNumCardMemberName",
    "orderNumCurrencyCode",
    "orderRelNum",
    "ourJobQty",
    "ourJobShippedQty",
    "ourReqQty",
    "ourStockQty",
    "ourStockShippedQty",
    "overrideCarrier",
    "overrideService",
    "poLine",
    "poNum",
    "poRelNum",
    "partExists",
    "partNum",
    "partNumIUM",
    "partNumPartDescription",
    "partNumPricePerCode",
    "partNumSalesUM",
    "partNumSellingFactor",
    "partNumTrackDimension",
    "partNumTrackLots",
    "partNumTrackSerialNum",
    "phaseID",
    "phaseWasRecInvoiced",
    "pickError",
    "plant",
    "plantName",
    "prevNeedByDate",
    "prevPartNum",
    "prevReqDate",
    "prevSellQty",
    "prevShipToNum",
    "prevXPartNum",
    "projectID",
    "purPoint",
    "purPointAddress1",
    "purPointAddress2",
    "purPointAddress3",
    "purPointCity",
    "purPointCountry",
    "purPointName",
    "purPointPrimPCon",
    "purPointState",
    "purPointZip",
    "ran",
    "refNotes",
    "reference",
    "relStatus",
    "releaseStatus",
    "reqDate",
    "resDelivery",
    "revisionNum",
    "rpt1SelfAssessTax",
    "rpt1TotalTax",
    "rpt1WithholdTax",
    "rpt2SelfAssessTax",
    "rpt2TotalTax",
    "rpt2WithholdTax",
    "rpt3SelfAssessTax",
    "rpt3TotalTax",
    "rpt3WithholdTax",
    "snEnable",
    "salesOrderLinked",
    "salesUM",
    "satDelivery",
    "satPickup",
    "scheduleNumber",
    "selectForPicking",
    "selfAssessTax",
    "sellingFactor",
    "sellingFactorDirection",
    "sellingJobQty",
    "sellingJobShippedQty",
    "sellingReqQty",
    "sellingStockQty",
    "sellingStockShippedQty",
    "servAOD",
    "servAlert",
    "servAuthNum",
    "servDeliveryDate",
    "servHomeDel",
    "servInstruct",
    "servPOD",
    "servPhone",
    "servRef1",
    "servRef2",
    "servRef3",
    "servRef4",
    "servRef5",
    "servRelease",
    "servSatDelivery",
    "servSatPickup",
    "servSignature",
    "shipOvers",
    "shipRouting",
    "shipToAddressList",
    "shipToContactEMailAddress",
    "shipToContactName",
    "shipToCustNum",
    "shipToNum",
    "shipToSelected",
    "shipViaCode",
    "shipViaCodeDescription",
    "shipViaCodeWebDesc",
    "shipbyTime",
    "shpConNum",
    "stagingBinNum",
    "stagingWarehouseCode",
    "subShipTo",
    "sysRevID",
    "tpShipToBTName",
    "tpShipToCustID",
    "tpShipToName",
    "taxConnectCalc",
    "taxExempt",
    "taxRegionCode",
    "taxRegionCodeDescription",
    "thisRelInvtyQty",
    "totalJobStockShipped",
    "totalTax",
    "transportID",
    "updateMarkForRecords",
    "useOTMF",
    "useOTS",
    "vendorNum",
    "vendorNumAddress1",
    "vendorNumAddress2",
    "vendorNumAddress3",
    "vendorNumCity",
    "vendorNumCountry",
    "vendorNumCurrencyCode",
    "vendorNumDefaultFOB",
    "vendorNumName",
    "vendorNumState",
    "vendorNumTermsCode",
    "vendorNumVendorID",
    "vendorNumZIP",
    "verbalConf",
    "voidOrder",
    "voidRelease",
    "wiItemPrice",
    "wiItemShipCost",
    "wiOrder",
    "wiOrderLine",
    "warehouseCode",
    "warehouseCodeDescription",
    "wasRecInvoiced",
    "webSKU",
    "withholdTax"
})
public class OrderRelRow
    extends IceRow
{

    @XmlElement(name = "AllowTaxCodeUpd")
    protected Boolean allowTaxCodeUpd;
    @XmlElement(name = "ApplyChrg")
    protected Boolean applyChrg;
    @XmlElement(name = "AvailableQuantity")
    protected BigDecimal availableQuantity;
    @XmlElement(name = "BitFlag")
    protected Integer bitFlag;
    @XmlElement(name = "BuyOverride")
    protected Boolean buyOverride;
    @XmlElement(name = "BuyToOrder")
    protected Boolean buyToOrder;
    @XmlElement(name = "COD")
    protected Boolean cod;
    @XmlElement(name = "CODAmount")
    protected BigDecimal codAmount;
    @XmlElement(name = "CODCheck")
    protected Boolean codCheck;
    @XmlElement(name = "CODFreight")
    protected Boolean codFreight;
    @XmlElementRef(name = "ChangeDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> changeDate;
    @XmlElement(name = "ChangeTime")
    protected Integer changeTime;
    @XmlElementRef(name = "ChangedBy", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changedBy;
    @XmlElement(name = "ChrgAmount")
    protected BigDecimal chrgAmount;
    @XmlElementRef(name = "Company", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> company;
    @XmlElementRef(name = "ComplianceMsg", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> complianceMsg;
    @XmlElementRef(name = "CreditLimitMessage", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditLimitMessage;
    @XmlElementRef(name = "CreditLimitSource", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditLimitSource;
    @XmlElementRef(name = "CumeDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> cumeDate;
    @XmlElement(name = "CumeQty")
    protected BigDecimal cumeQty;
    @XmlElementRef(name = "CurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElement(name = "CustAllowOTS")
    protected Boolean custAllowOTS;
    @XmlElement(name = "CustomerAllowShipTo3")
    protected Boolean customerAllowShipTo3;
    @XmlElementRef(name = "CustomerCustID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerCustID;
    @XmlElementRef(name = "CustomerName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerName;
    @XmlElementRef(name = "DatePickTicketPrinted", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> datePickTicketPrinted;
    @XmlElement(name = "DeclaredAmt")
    protected BigDecimal declaredAmt;
    @XmlElement(name = "DeclaredIns")
    protected Boolean declaredIns;
    @XmlElementRef(name = "DeliveryType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deliveryType;
    @XmlElementRef(name = "DemandReference", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> demandReference;
    @XmlElement(name = "DemandSchedRejected")
    protected Boolean demandSchedRejected;
    @XmlElement(name = "DocOnly")
    protected Boolean docOnly;
    @XmlElement(name = "DocSelfAssessTax")
    protected BigDecimal docSelfAssessTax;
    @XmlElement(name = "DocTotalTax")
    protected BigDecimal docTotalTax;
    @XmlElement(name = "DocWithholdTax")
    protected BigDecimal docWithholdTax;
    @XmlElementRef(name = "DockingStation", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dockingStation;
    @XmlElement(name = "DropShip")
    protected Boolean dropShip;
    @XmlElementRef(name = "DropShipName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dropShipName;
    @XmlElement(name = "DropShipOverride")
    protected Boolean dropShipOverride;
    @XmlElementRef(name = "DspInvMeth", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dspInvMeth;
    @XmlElementRef(name = "DspRevMethod", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dspRevMethod;
    @XmlElementRef(name = "ECCPlant", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eccPlant;
    @XmlElement(name = "EnableMake")
    protected Boolean enableMake;
    @XmlElementRef(name = "EntityUseCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> entityUseCode;
    @XmlElementRef(name = "EntryProcess", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> entryProcess;
    @XmlElement(name = "ExistPOSugg")
    protected Boolean existPOSugg;
    @XmlElementRef(name = "ExtCompany", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> extCompany;
    @XmlElement(name = "FirmRelease")
    protected Boolean firmRelease;
    @XmlElement(name = "GetDfltTaxIds")
    protected Boolean getDfltTaxIds;
    @XmlElementRef(name = "GroundType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> groundType;
    @XmlElement(name = "Hazmat")
    protected Boolean hazmat;
    @XmlElement(name = "HdrOTS")
    protected Boolean hdrOTS;
    @XmlElement(name = "ICPOLine")
    protected Integer icpoLine;
    @XmlElement(name = "ICPONum")
    protected Integer icpoNum;
    @XmlElement(name = "ICPORelNum")
    protected Integer icpoRelNum;
    @XmlElementRef(name = "IUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ium;
    @XmlElementRef(name = "LineType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lineType;
    @XmlElement(name = "LinkToPONum")
    protected Boolean linkToPONum;
    @XmlElement(name = "Linked")
    protected Boolean linked;
    @XmlElementRef(name = "Location", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> location;
    @XmlElementRef(name = "MFCustID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mfCustID;
    @XmlElement(name = "MFCustNum")
    protected Integer mfCustNum;
    @XmlElement(name = "Make")
    protected Boolean make;
    @XmlElement(name = "MakeOverride")
    protected Boolean makeOverride;
    @XmlElementRef(name = "MarkForAddrList", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> markForAddrList;
    @XmlElementRef(name = "MarkForNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> markForNum;
    @XmlElementRef(name = "NeedByDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> needByDate;
    @XmlElement(name = "NotCompliant")
    protected Boolean notCompliant;
    @XmlElementRef(name = "NotifyEMail", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> notifyEMail;
    @XmlElement(name = "NotifyFlag")
    protected Boolean notifyFlag;
    @XmlElementRef(name = "OTMFAddress1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otmfAddress1;
    @XmlElementRef(name = "OTMFAddress2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otmfAddress2;
    @XmlElementRef(name = "OTMFAddress3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otmfAddress3;
    @XmlElementRef(name = "OTMFCity", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otmfCity;
    @XmlElementRef(name = "OTMFContact", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otmfContact;
    @XmlElementRef(name = "OTMFCountryDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otmfCountryDescription;
    @XmlElement(name = "OTMFCountryNum")
    protected Integer otmfCountryNum;
    @XmlElementRef(name = "OTMFFaxNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otmfFaxNum;
    @XmlElementRef(name = "OTMFName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otmfName;
    @XmlElementRef(name = "OTMFPhoneNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otmfPhoneNum;
    @XmlElementRef(name = "OTMFState", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otmfState;
    @XmlElementRef(name = "OTMFZIP", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otmfzip;
    @XmlElementRef(name = "OTSAddress1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsAddress1;
    @XmlElementRef(name = "OTSAddress2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsAddress2;
    @XmlElementRef(name = "OTSAddress3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsAddress3;
    @XmlElementRef(name = "OTSCity", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsCity;
    @XmlElementRef(name = "OTSCntryDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsCntryDescription;
    @XmlElementRef(name = "OTSContact", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsContact;
    @XmlElement(name = "OTSCountryNum")
    protected Integer otsCountryNum;
    @XmlElement(name = "OTSCustSaved")
    protected Boolean otsCustSaved;
    @XmlElementRef(name = "OTSFaxNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsFaxNum;
    @XmlElementRef(name = "OTSName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsName;
    @XmlElementRef(name = "OTSPhoneNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsPhoneNum;
    @XmlElementRef(name = "OTSResaleID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsResaleID;
    @XmlElementRef(name = "OTSSaveAs", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsSaveAs;
    @XmlElementRef(name = "OTSSaveCustID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsSaveCustID;
    @XmlElement(name = "OTSSaved")
    protected Boolean otsSaved;
    @XmlElementRef(name = "OTSShipToNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsShipToNum;
    @XmlElementRef(name = "OTSState", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsState;
    @XmlElementRef(name = "OTSTaxRegionCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsTaxRegionCode;
    @XmlElementRef(name = "OTSZIP", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otszip;
    @XmlElement(name = "OnHandQuantity")
    protected BigDecimal onHandQuantity;
    @XmlElement(name = "OpenOrder")
    protected Boolean openOrder;
    @XmlElement(name = "OpenRelease")
    protected Boolean openRelease;
    @XmlElement(name = "OrderLine")
    protected Integer orderLine;
    @XmlElementRef(name = "OrderLineLineDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderLineLineDesc;
    @XmlElement(name = "OrderNum")
    protected Integer orderNum;
    @XmlElementRef(name = "OrderNumCardMemberName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderNumCardMemberName;
    @XmlElementRef(name = "OrderNumCurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderNumCurrencyCode;
    @XmlElement(name = "OrderRelNum")
    protected Integer orderRelNum;
    @XmlElement(name = "OurJobQty")
    protected BigDecimal ourJobQty;
    @XmlElement(name = "OurJobShippedQty")
    protected BigDecimal ourJobShippedQty;
    @XmlElement(name = "OurReqQty")
    protected BigDecimal ourReqQty;
    @XmlElement(name = "OurStockQty")
    protected BigDecimal ourStockQty;
    @XmlElement(name = "OurStockShippedQty")
    protected BigDecimal ourStockShippedQty;
    @XmlElement(name = "OverrideCarrier")
    protected Boolean overrideCarrier;
    @XmlElement(name = "OverrideService")
    protected Boolean overrideService;
    @XmlElement(name = "POLine")
    protected Integer poLine;
    @XmlElement(name = "PONum")
    protected Integer poNum;
    @XmlElement(name = "PORelNum")
    protected Integer poRelNum;
    @XmlElement(name = "PartExists")
    protected Boolean partExists;
    @XmlElementRef(name = "PartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNum;
    @XmlElementRef(name = "PartNumIUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNumIUM;
    @XmlElementRef(name = "PartNumPartDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNumPartDescription;
    @XmlElementRef(name = "PartNumPricePerCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNumPricePerCode;
    @XmlElementRef(name = "PartNumSalesUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNumSalesUM;
    @XmlElement(name = "PartNumSellingFactor")
    protected BigDecimal partNumSellingFactor;
    @XmlElement(name = "PartNumTrackDimension")
    protected Boolean partNumTrackDimension;
    @XmlElement(name = "PartNumTrackLots")
    protected Boolean partNumTrackLots;
    @XmlElement(name = "PartNumTrackSerialNum")
    protected Boolean partNumTrackSerialNum;
    @XmlElementRef(name = "PhaseID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> phaseID;
    @XmlElement(name = "PhaseWasRecInvoiced")
    protected Boolean phaseWasRecInvoiced;
    @XmlElementRef(name = "PickError", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pickError;
    @XmlElementRef(name = "Plant", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> plant;
    @XmlElementRef(name = "PlantName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> plantName;
    @XmlElementRef(name = "PrevNeedByDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> prevNeedByDate;
    @XmlElementRef(name = "PrevPartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prevPartNum;
    @XmlElementRef(name = "PrevReqDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> prevReqDate;
    @XmlElement(name = "PrevSellQty")
    protected BigDecimal prevSellQty;
    @XmlElementRef(name = "PrevShipToNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prevShipToNum;
    @XmlElementRef(name = "PrevXPartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prevXPartNum;
    @XmlElementRef(name = "ProjectID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> projectID;
    @XmlElementRef(name = "PurPoint", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> purPoint;
    @XmlElementRef(name = "PurPointAddress1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> purPointAddress1;
    @XmlElementRef(name = "PurPointAddress2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> purPointAddress2;
    @XmlElementRef(name = "PurPointAddress3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> purPointAddress3;
    @XmlElementRef(name = "PurPointCity", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> purPointCity;
    @XmlElementRef(name = "PurPointCountry", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> purPointCountry;
    @XmlElementRef(name = "PurPointName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> purPointName;
    @XmlElement(name = "PurPointPrimPCon")
    protected Integer purPointPrimPCon;
    @XmlElementRef(name = "PurPointState", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> purPointState;
    @XmlElementRef(name = "PurPointZip", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> purPointZip;
    @XmlElementRef(name = "RAN", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ran;
    @XmlElementRef(name = "RefNotes", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> refNotes;
    @XmlElementRef(name = "Reference", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reference;
    @XmlElementRef(name = "RelStatus", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> relStatus;
    @XmlElementRef(name = "ReleaseStatus", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> releaseStatus;
    @XmlElementRef(name = "ReqDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> reqDate;
    @XmlElement(name = "ResDelivery")
    protected Boolean resDelivery;
    @XmlElementRef(name = "RevisionNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> revisionNum;
    @XmlElement(name = "Rpt1SelfAssessTax")
    protected BigDecimal rpt1SelfAssessTax;
    @XmlElement(name = "Rpt1TotalTax")
    protected BigDecimal rpt1TotalTax;
    @XmlElement(name = "Rpt1WithholdTax")
    protected BigDecimal rpt1WithholdTax;
    @XmlElement(name = "Rpt2SelfAssessTax")
    protected BigDecimal rpt2SelfAssessTax;
    @XmlElement(name = "Rpt2TotalTax")
    protected BigDecimal rpt2TotalTax;
    @XmlElement(name = "Rpt2WithholdTax")
    protected BigDecimal rpt2WithholdTax;
    @XmlElement(name = "Rpt3SelfAssessTax")
    protected BigDecimal rpt3SelfAssessTax;
    @XmlElement(name = "Rpt3TotalTax")
    protected BigDecimal rpt3TotalTax;
    @XmlElement(name = "Rpt3WithholdTax")
    protected BigDecimal rpt3WithholdTax;
    @XmlElement(name = "SNEnable")
    protected Boolean snEnable;
    @XmlElement(name = "SalesOrderLinked")
    protected Boolean salesOrderLinked;
    @XmlElementRef(name = "SalesUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesUM;
    @XmlElement(name = "SatDelivery")
    protected Boolean satDelivery;
    @XmlElement(name = "SatPickup")
    protected Boolean satPickup;
    @XmlElementRef(name = "ScheduleNumber", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> scheduleNumber;
    @XmlElement(name = "SelectForPicking")
    protected Boolean selectForPicking;
    @XmlElement(name = "SelfAssessTax")
    protected BigDecimal selfAssessTax;
    @XmlElement(name = "SellingFactor")
    protected BigDecimal sellingFactor;
    @XmlElementRef(name = "SellingFactorDirection", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sellingFactorDirection;
    @XmlElement(name = "SellingJobQty")
    protected BigDecimal sellingJobQty;
    @XmlElement(name = "SellingJobShippedQty")
    protected BigDecimal sellingJobShippedQty;
    @XmlElement(name = "SellingReqQty")
    protected BigDecimal sellingReqQty;
    @XmlElement(name = "SellingStockQty")
    protected BigDecimal sellingStockQty;
    @XmlElement(name = "SellingStockShippedQty")
    protected BigDecimal sellingStockShippedQty;
    @XmlElement(name = "ServAOD")
    protected Boolean servAOD;
    @XmlElement(name = "ServAlert")
    protected Boolean servAlert;
    @XmlElementRef(name = "ServAuthNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servAuthNum;
    @XmlElementRef(name = "ServDeliveryDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> servDeliveryDate;
    @XmlElement(name = "ServHomeDel")
    protected Boolean servHomeDel;
    @XmlElementRef(name = "ServInstruct", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servInstruct;
    @XmlElement(name = "ServPOD")
    protected Boolean servPOD;
    @XmlElementRef(name = "ServPhone", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servPhone;
    @XmlElementRef(name = "ServRef1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servRef1;
    @XmlElementRef(name = "ServRef2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servRef2;
    @XmlElementRef(name = "ServRef3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servRef3;
    @XmlElementRef(name = "ServRef4", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servRef4;
    @XmlElementRef(name = "ServRef5", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servRef5;
    @XmlElement(name = "ServRelease")
    protected Boolean servRelease;
    @XmlElement(name = "ServSatDelivery")
    protected Boolean servSatDelivery;
    @XmlElement(name = "ServSatPickup")
    protected Boolean servSatPickup;
    @XmlElement(name = "ServSignature")
    protected Boolean servSignature;
    @XmlElement(name = "ShipOvers")
    protected Boolean shipOvers;
    @XmlElementRef(name = "ShipRouting", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipRouting;
    @XmlElementRef(name = "ShipToAddressList", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipToAddressList;
    @XmlElementRef(name = "ShipToContactEMailAddress", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipToContactEMailAddress;
    @XmlElementRef(name = "ShipToContactName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipToContactName;
    @XmlElement(name = "ShipToCustNum")
    protected Integer shipToCustNum;
    @XmlElementRef(name = "ShipToNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipToNum;
    @XmlElement(name = "ShipToSelected")
    protected Boolean shipToSelected;
    @XmlElementRef(name = "ShipViaCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipViaCode;
    @XmlElementRef(name = "ShipViaCodeDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipViaCodeDescription;
    @XmlElementRef(name = "ShipViaCodeWebDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipViaCodeWebDesc;
    @XmlElement(name = "ShipbyTime")
    protected Integer shipbyTime;
    @XmlElement(name = "ShpConNum")
    protected Integer shpConNum;
    @XmlElementRef(name = "StagingBinNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> stagingBinNum;
    @XmlElementRef(name = "StagingWarehouseCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> stagingWarehouseCode;
    @XmlElementRef(name = "SubShipTo", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subShipTo;
    @XmlElement(name = "SysRevID")
    protected Long sysRevID;
    @XmlElementRef(name = "TPShipToBTName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tpShipToBTName;
    @XmlElementRef(name = "TPShipToCustID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tpShipToCustID;
    @XmlElementRef(name = "TPShipToName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tpShipToName;
    @XmlElement(name = "TaxConnectCalc")
    protected Boolean taxConnectCalc;
    @XmlElementRef(name = "TaxExempt", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxExempt;
    @XmlElementRef(name = "TaxRegionCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxRegionCode;
    @XmlElementRef(name = "TaxRegionCodeDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxRegionCodeDescription;
    @XmlElement(name = "ThisRelInvtyQty")
    protected BigDecimal thisRelInvtyQty;
    @XmlElement(name = "TotalJobStockShipped")
    protected BigDecimal totalJobStockShipped;
    @XmlElement(name = "TotalTax")
    protected BigDecimal totalTax;
    @XmlElementRef(name = "TransportID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transportID;
    @XmlElement(name = "UpdateMarkForRecords")
    protected Boolean updateMarkForRecords;
    @XmlElement(name = "UseOTMF")
    protected Boolean useOTMF;
    @XmlElement(name = "UseOTS")
    protected Boolean useOTS;
    @XmlElement(name = "VendorNum")
    protected Integer vendorNum;
    @XmlElementRef(name = "VendorNumAddress1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vendorNumAddress1;
    @XmlElementRef(name = "VendorNumAddress2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vendorNumAddress2;
    @XmlElementRef(name = "VendorNumAddress3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vendorNumAddress3;
    @XmlElementRef(name = "VendorNumCity", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vendorNumCity;
    @XmlElementRef(name = "VendorNumCountry", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vendorNumCountry;
    @XmlElementRef(name = "VendorNumCurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vendorNumCurrencyCode;
    @XmlElementRef(name = "VendorNumDefaultFOB", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vendorNumDefaultFOB;
    @XmlElementRef(name = "VendorNumName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vendorNumName;
    @XmlElementRef(name = "VendorNumState", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vendorNumState;
    @XmlElementRef(name = "VendorNumTermsCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vendorNumTermsCode;
    @XmlElementRef(name = "VendorNumVendorID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vendorNumVendorID;
    @XmlElementRef(name = "VendorNumZIP", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vendorNumZIP;
    @XmlElement(name = "VerbalConf")
    protected Boolean verbalConf;
    @XmlElement(name = "VoidOrder")
    protected Boolean voidOrder;
    @XmlElement(name = "VoidRelease")
    protected Boolean voidRelease;
    @XmlElement(name = "WIItemPrice")
    protected BigDecimal wiItemPrice;
    @XmlElement(name = "WIItemShipCost")
    protected BigDecimal wiItemShipCost;
    @XmlElementRef(name = "WIOrder", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> wiOrder;
    @XmlElementRef(name = "WIOrderLine", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> wiOrderLine;
    @XmlElementRef(name = "WarehouseCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> warehouseCode;
    @XmlElementRef(name = "WarehouseCodeDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> warehouseCodeDescription;
    @XmlElement(name = "WasRecInvoiced")
    protected Boolean wasRecInvoiced;
    @XmlElementRef(name = "WebSKU", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> webSKU;
    @XmlElement(name = "WithholdTax")
    protected BigDecimal withholdTax;

    /**
     * Gets the value of the allowTaxCodeUpd property.
     * This getter has been renamed from isAllowTaxCodeUpd() to getAllowTaxCodeUpd() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAllowTaxCodeUpd() {
        return allowTaxCodeUpd;
    }

    /**
     * Sets the value of the allowTaxCodeUpd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowTaxCodeUpd(Boolean value) {
        this.allowTaxCodeUpd = value;
    }

    /**
     * Gets the value of the applyChrg property.
     * This getter has been renamed from isApplyChrg() to getApplyChrg() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getApplyChrg() {
        return applyChrg;
    }

    /**
     * Sets the value of the applyChrg property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApplyChrg(Boolean value) {
        this.applyChrg = value;
    }

    /**
     * Gets the value of the availableQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAvailableQuantity() {
        return availableQuantity;
    }

    /**
     * Sets the value of the availableQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAvailableQuantity(BigDecimal value) {
        this.availableQuantity = value;
    }

    /**
     * Gets the value of the bitFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBitFlag() {
        return bitFlag;
    }

    /**
     * Sets the value of the bitFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBitFlag(Integer value) {
        this.bitFlag = value;
    }

    /**
     * Gets the value of the buyOverride property.
     * This getter has been renamed from isBuyOverride() to getBuyOverride() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getBuyOverride() {
        return buyOverride;
    }

    /**
     * Sets the value of the buyOverride property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBuyOverride(Boolean value) {
        this.buyOverride = value;
    }

    /**
     * Gets the value of the buyToOrder property.
     * This getter has been renamed from isBuyToOrder() to getBuyToOrder() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getBuyToOrder() {
        return buyToOrder;
    }

    /**
     * Sets the value of the buyToOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBuyToOrder(Boolean value) {
        this.buyToOrder = value;
    }

    /**
     * Gets the value of the cod property.
     * This getter has been renamed from isCOD() to getCOD() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCOD() {
        return cod;
    }

    /**
     * Sets the value of the cod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCOD(Boolean value) {
        this.cod = value;
    }

    /**
     * Gets the value of the codAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCODAmount() {
        return codAmount;
    }

    /**
     * Sets the value of the codAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCODAmount(BigDecimal value) {
        this.codAmount = value;
    }

    /**
     * Gets the value of the codCheck property.
     * This getter has been renamed from isCODCheck() to getCODCheck() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCODCheck() {
        return codCheck;
    }

    /**
     * Sets the value of the codCheck property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCODCheck(Boolean value) {
        this.codCheck = value;
    }

    /**
     * Gets the value of the codFreight property.
     * This getter has been renamed from isCODFreight() to getCODFreight() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCODFreight() {
        return codFreight;
    }

    /**
     * Sets the value of the codFreight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCODFreight(Boolean value) {
        this.codFreight = value;
    }

    /**
     * Gets the value of the changeDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getChangeDate() {
        return changeDate;
    }

    /**
     * Sets the value of the changeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setChangeDate(JAXBElement<XMLGregorianCalendar> value) {
        this.changeDate = value;
    }

    /**
     * Gets the value of the changeTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChangeTime() {
        return changeTime;
    }

    /**
     * Sets the value of the changeTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChangeTime(Integer value) {
        this.changeTime = value;
    }

    /**
     * Gets the value of the changedBy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangedBy() {
        return changedBy;
    }

    /**
     * Sets the value of the changedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangedBy(JAXBElement<String> value) {
        this.changedBy = value;
    }

    /**
     * Gets the value of the chrgAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChrgAmount() {
        return chrgAmount;
    }

    /**
     * Sets the value of the chrgAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChrgAmount(BigDecimal value) {
        this.chrgAmount = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompany(JAXBElement<String> value) {
        this.company = value;
    }

    /**
     * Gets the value of the complianceMsg property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getComplianceMsg() {
        return complianceMsg;
    }

    /**
     * Sets the value of the complianceMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setComplianceMsg(JAXBElement<String> value) {
        this.complianceMsg = value;
    }

    /**
     * Gets the value of the creditLimitMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCreditLimitMessage() {
        return creditLimitMessage;
    }

    /**
     * Sets the value of the creditLimitMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCreditLimitMessage(JAXBElement<String> value) {
        this.creditLimitMessage = value;
    }

    /**
     * Gets the value of the creditLimitSource property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCreditLimitSource() {
        return creditLimitSource;
    }

    /**
     * Sets the value of the creditLimitSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCreditLimitSource(JAXBElement<String> value) {
        this.creditLimitSource = value;
    }

    /**
     * Gets the value of the cumeDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getCumeDate() {
        return cumeDate;
    }

    /**
     * Sets the value of the cumeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setCumeDate(JAXBElement<XMLGregorianCalendar> value) {
        this.cumeDate = value;
    }

    /**
     * Gets the value of the cumeQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCumeQty() {
        return cumeQty;
    }

    /**
     * Sets the value of the cumeQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCumeQty(BigDecimal value) {
        this.cumeQty = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the custAllowOTS property.
     * This getter has been renamed from isCustAllowOTS() to getCustAllowOTS() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCustAllowOTS() {
        return custAllowOTS;
    }

    /**
     * Sets the value of the custAllowOTS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCustAllowOTS(Boolean value) {
        this.custAllowOTS = value;
    }

    /**
     * Gets the value of the customerAllowShipTo3 property.
     * This getter has been renamed from isCustomerAllowShipTo3() to getCustomerAllowShipTo3() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCustomerAllowShipTo3() {
        return customerAllowShipTo3;
    }

    /**
     * Sets the value of the customerAllowShipTo3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCustomerAllowShipTo3(Boolean value) {
        this.customerAllowShipTo3 = value;
    }

    /**
     * Gets the value of the customerCustID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerCustID() {
        return customerCustID;
    }

    /**
     * Sets the value of the customerCustID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerCustID(JAXBElement<String> value) {
        this.customerCustID = value;
    }

    /**
     * Gets the value of the customerName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerName() {
        return customerName;
    }

    /**
     * Sets the value of the customerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerName(JAXBElement<String> value) {
        this.customerName = value;
    }

    /**
     * Gets the value of the datePickTicketPrinted property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDatePickTicketPrinted() {
        return datePickTicketPrinted;
    }

    /**
     * Sets the value of the datePickTicketPrinted property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDatePickTicketPrinted(JAXBElement<XMLGregorianCalendar> value) {
        this.datePickTicketPrinted = value;
    }

    /**
     * Gets the value of the declaredAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDeclaredAmt() {
        return declaredAmt;
    }

    /**
     * Sets the value of the declaredAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDeclaredAmt(BigDecimal value) {
        this.declaredAmt = value;
    }

    /**
     * Gets the value of the declaredIns property.
     * This getter has been renamed from isDeclaredIns() to getDeclaredIns() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDeclaredIns() {
        return declaredIns;
    }

    /**
     * Sets the value of the declaredIns property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeclaredIns(Boolean value) {
        this.declaredIns = value;
    }

    /**
     * Gets the value of the deliveryType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeliveryType() {
        return deliveryType;
    }

    /**
     * Sets the value of the deliveryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeliveryType(JAXBElement<String> value) {
        this.deliveryType = value;
    }

    /**
     * Gets the value of the demandReference property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDemandReference() {
        return demandReference;
    }

    /**
     * Sets the value of the demandReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDemandReference(JAXBElement<String> value) {
        this.demandReference = value;
    }

    /**
     * Gets the value of the demandSchedRejected property.
     * This getter has been renamed from isDemandSchedRejected() to getDemandSchedRejected() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDemandSchedRejected() {
        return demandSchedRejected;
    }

    /**
     * Sets the value of the demandSchedRejected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDemandSchedRejected(Boolean value) {
        this.demandSchedRejected = value;
    }

    /**
     * Gets the value of the docOnly property.
     * This getter has been renamed from isDocOnly() to getDocOnly() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDocOnly() {
        return docOnly;
    }

    /**
     * Sets the value of the docOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDocOnly(Boolean value) {
        this.docOnly = value;
    }

    /**
     * Gets the value of the docSelfAssessTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocSelfAssessTax() {
        return docSelfAssessTax;
    }

    /**
     * Sets the value of the docSelfAssessTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocSelfAssessTax(BigDecimal value) {
        this.docSelfAssessTax = value;
    }

    /**
     * Gets the value of the docTotalTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTotalTax() {
        return docTotalTax;
    }

    /**
     * Sets the value of the docTotalTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTotalTax(BigDecimal value) {
        this.docTotalTax = value;
    }

    /**
     * Gets the value of the docWithholdTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocWithholdTax() {
        return docWithholdTax;
    }

    /**
     * Sets the value of the docWithholdTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocWithholdTax(BigDecimal value) {
        this.docWithholdTax = value;
    }

    /**
     * Gets the value of the dockingStation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDockingStation() {
        return dockingStation;
    }

    /**
     * Sets the value of the dockingStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDockingStation(JAXBElement<String> value) {
        this.dockingStation = value;
    }

    /**
     * Gets the value of the dropShip property.
     * This getter has been renamed from isDropShip() to getDropShip() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDropShip() {
        return dropShip;
    }

    /**
     * Sets the value of the dropShip property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDropShip(Boolean value) {
        this.dropShip = value;
    }

    /**
     * Gets the value of the dropShipName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDropShipName() {
        return dropShipName;
    }

    /**
     * Sets the value of the dropShipName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDropShipName(JAXBElement<String> value) {
        this.dropShipName = value;
    }

    /**
     * Gets the value of the dropShipOverride property.
     * This getter has been renamed from isDropShipOverride() to getDropShipOverride() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDropShipOverride() {
        return dropShipOverride;
    }

    /**
     * Sets the value of the dropShipOverride property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDropShipOverride(Boolean value) {
        this.dropShipOverride = value;
    }

    /**
     * Gets the value of the dspInvMeth property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDspInvMeth() {
        return dspInvMeth;
    }

    /**
     * Sets the value of the dspInvMeth property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDspInvMeth(JAXBElement<String> value) {
        this.dspInvMeth = value;
    }

    /**
     * Gets the value of the dspRevMethod property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDspRevMethod() {
        return dspRevMethod;
    }

    /**
     * Sets the value of the dspRevMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDspRevMethod(JAXBElement<String> value) {
        this.dspRevMethod = value;
    }

    /**
     * Gets the value of the eccPlant property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getECCPlant() {
        return eccPlant;
    }

    /**
     * Sets the value of the eccPlant property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setECCPlant(JAXBElement<String> value) {
        this.eccPlant = value;
    }

    /**
     * Gets the value of the enableMake property.
     * This getter has been renamed from isEnableMake() to getEnableMake() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnableMake() {
        return enableMake;
    }

    /**
     * Sets the value of the enableMake property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMake(Boolean value) {
        this.enableMake = value;
    }

    /**
     * Gets the value of the entityUseCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEntityUseCode() {
        return entityUseCode;
    }

    /**
     * Sets the value of the entityUseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEntityUseCode(JAXBElement<String> value) {
        this.entityUseCode = value;
    }

    /**
     * Gets the value of the entryProcess property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEntryProcess() {
        return entryProcess;
    }

    /**
     * Sets the value of the entryProcess property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEntryProcess(JAXBElement<String> value) {
        this.entryProcess = value;
    }

    /**
     * Gets the value of the existPOSugg property.
     * This getter has been renamed from isExistPOSugg() to getExistPOSugg() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getExistPOSugg() {
        return existPOSugg;
    }

    /**
     * Sets the value of the existPOSugg property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExistPOSugg(Boolean value) {
        this.existPOSugg = value;
    }

    /**
     * Gets the value of the extCompany property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExtCompany() {
        return extCompany;
    }

    /**
     * Sets the value of the extCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExtCompany(JAXBElement<String> value) {
        this.extCompany = value;
    }

    /**
     * Gets the value of the firmRelease property.
     * This getter has been renamed from isFirmRelease() to getFirmRelease() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getFirmRelease() {
        return firmRelease;
    }

    /**
     * Sets the value of the firmRelease property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFirmRelease(Boolean value) {
        this.firmRelease = value;
    }

    /**
     * Gets the value of the getDfltTaxIds property.
     * This getter has been renamed from isGetDfltTaxIds() to getGetDfltTaxIds() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getGetDfltTaxIds() {
        return getDfltTaxIds;
    }

    /**
     * Sets the value of the getDfltTaxIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGetDfltTaxIds(Boolean value) {
        this.getDfltTaxIds = value;
    }

    /**
     * Gets the value of the groundType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGroundType() {
        return groundType;
    }

    /**
     * Sets the value of the groundType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGroundType(JAXBElement<String> value) {
        this.groundType = value;
    }

    /**
     * Gets the value of the hazmat property.
     * This getter has been renamed from isHazmat() to getHazmat() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHazmat() {
        return hazmat;
    }

    /**
     * Sets the value of the hazmat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHazmat(Boolean value) {
        this.hazmat = value;
    }

    /**
     * Gets the value of the hdrOTS property.
     * This getter has been renamed from isHdrOTS() to getHdrOTS() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHdrOTS() {
        return hdrOTS;
    }

    /**
     * Sets the value of the hdrOTS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHdrOTS(Boolean value) {
        this.hdrOTS = value;
    }

    /**
     * Gets the value of the icpoLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getICPOLine() {
        return icpoLine;
    }

    /**
     * Sets the value of the icpoLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setICPOLine(Integer value) {
        this.icpoLine = value;
    }

    /**
     * Gets the value of the icpoNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getICPONum() {
        return icpoNum;
    }

    /**
     * Sets the value of the icpoNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setICPONum(Integer value) {
        this.icpoNum = value;
    }

    /**
     * Gets the value of the icpoRelNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getICPORelNum() {
        return icpoRelNum;
    }

    /**
     * Sets the value of the icpoRelNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setICPORelNum(Integer value) {
        this.icpoRelNum = value;
    }

    /**
     * Gets the value of the ium property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIUM() {
        return ium;
    }

    /**
     * Sets the value of the ium property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIUM(JAXBElement<String> value) {
        this.ium = value;
    }

    /**
     * Gets the value of the lineType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLineType() {
        return lineType;
    }

    /**
     * Sets the value of the lineType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLineType(JAXBElement<String> value) {
        this.lineType = value;
    }

    /**
     * Gets the value of the linkToPONum property.
     * This getter has been renamed from isLinkToPONum() to getLinkToPONum() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLinkToPONum() {
        return linkToPONum;
    }

    /**
     * Sets the value of the linkToPONum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLinkToPONum(Boolean value) {
        this.linkToPONum = value;
    }

    /**
     * Gets the value of the linked property.
     * This getter has been renamed from isLinked() to getLinked() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLinked() {
        return linked;
    }

    /**
     * Sets the value of the linked property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLinked(Boolean value) {
        this.linked = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocation(JAXBElement<String> value) {
        this.location = value;
    }

    /**
     * Gets the value of the mfCustID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMFCustID() {
        return mfCustID;
    }

    /**
     * Sets the value of the mfCustID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMFCustID(JAXBElement<String> value) {
        this.mfCustID = value;
    }

    /**
     * Gets the value of the mfCustNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMFCustNum() {
        return mfCustNum;
    }

    /**
     * Sets the value of the mfCustNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMFCustNum(Integer value) {
        this.mfCustNum = value;
    }

    /**
     * Gets the value of the make property.
     * This getter has been renamed from isMake() to getMake() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMake() {
        return make;
    }

    /**
     * Sets the value of the make property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMake(Boolean value) {
        this.make = value;
    }

    /**
     * Gets the value of the makeOverride property.
     * This getter has been renamed from isMakeOverride() to getMakeOverride() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMakeOverride() {
        return makeOverride;
    }

    /**
     * Sets the value of the makeOverride property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMakeOverride(Boolean value) {
        this.makeOverride = value;
    }

    /**
     * Gets the value of the markForAddrList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMarkForAddrList() {
        return markForAddrList;
    }

    /**
     * Sets the value of the markForAddrList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMarkForAddrList(JAXBElement<String> value) {
        this.markForAddrList = value;
    }

    /**
     * Gets the value of the markForNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMarkForNum() {
        return markForNum;
    }

    /**
     * Sets the value of the markForNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMarkForNum(JAXBElement<String> value) {
        this.markForNum = value;
    }

    /**
     * Gets the value of the needByDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getNeedByDate() {
        return needByDate;
    }

    /**
     * Sets the value of the needByDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setNeedByDate(JAXBElement<XMLGregorianCalendar> value) {
        this.needByDate = value;
    }

    /**
     * Gets the value of the notCompliant property.
     * This getter has been renamed from isNotCompliant() to getNotCompliant() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNotCompliant() {
        return notCompliant;
    }

    /**
     * Sets the value of the notCompliant property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotCompliant(Boolean value) {
        this.notCompliant = value;
    }

    /**
     * Gets the value of the notifyEMail property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNotifyEMail() {
        return notifyEMail;
    }

    /**
     * Sets the value of the notifyEMail property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNotifyEMail(JAXBElement<String> value) {
        this.notifyEMail = value;
    }

    /**
     * Gets the value of the notifyFlag property.
     * This getter has been renamed from isNotifyFlag() to getNotifyFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNotifyFlag() {
        return notifyFlag;
    }

    /**
     * Sets the value of the notifyFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyFlag(Boolean value) {
        this.notifyFlag = value;
    }

    /**
     * Gets the value of the otmfAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTMFAddress1() {
        return otmfAddress1;
    }

    /**
     * Sets the value of the otmfAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTMFAddress1(JAXBElement<String> value) {
        this.otmfAddress1 = value;
    }

    /**
     * Gets the value of the otmfAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTMFAddress2() {
        return otmfAddress2;
    }

    /**
     * Sets the value of the otmfAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTMFAddress2(JAXBElement<String> value) {
        this.otmfAddress2 = value;
    }

    /**
     * Gets the value of the otmfAddress3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTMFAddress3() {
        return otmfAddress3;
    }

    /**
     * Sets the value of the otmfAddress3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTMFAddress3(JAXBElement<String> value) {
        this.otmfAddress3 = value;
    }

    /**
     * Gets the value of the otmfCity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTMFCity() {
        return otmfCity;
    }

    /**
     * Sets the value of the otmfCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTMFCity(JAXBElement<String> value) {
        this.otmfCity = value;
    }

    /**
     * Gets the value of the otmfContact property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTMFContact() {
        return otmfContact;
    }

    /**
     * Sets the value of the otmfContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTMFContact(JAXBElement<String> value) {
        this.otmfContact = value;
    }

    /**
     * Gets the value of the otmfCountryDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTMFCountryDescription() {
        return otmfCountryDescription;
    }

    /**
     * Sets the value of the otmfCountryDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTMFCountryDescription(JAXBElement<String> value) {
        this.otmfCountryDescription = value;
    }

    /**
     * Gets the value of the otmfCountryNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOTMFCountryNum() {
        return otmfCountryNum;
    }

    /**
     * Sets the value of the otmfCountryNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOTMFCountryNum(Integer value) {
        this.otmfCountryNum = value;
    }

    /**
     * Gets the value of the otmfFaxNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTMFFaxNum() {
        return otmfFaxNum;
    }

    /**
     * Sets the value of the otmfFaxNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTMFFaxNum(JAXBElement<String> value) {
        this.otmfFaxNum = value;
    }

    /**
     * Gets the value of the otmfName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTMFName() {
        return otmfName;
    }

    /**
     * Sets the value of the otmfName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTMFName(JAXBElement<String> value) {
        this.otmfName = value;
    }

    /**
     * Gets the value of the otmfPhoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTMFPhoneNum() {
        return otmfPhoneNum;
    }

    /**
     * Sets the value of the otmfPhoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTMFPhoneNum(JAXBElement<String> value) {
        this.otmfPhoneNum = value;
    }

    /**
     * Gets the value of the otmfState property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTMFState() {
        return otmfState;
    }

    /**
     * Sets the value of the otmfState property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTMFState(JAXBElement<String> value) {
        this.otmfState = value;
    }

    /**
     * Gets the value of the otmfzip property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTMFZIP() {
        return otmfzip;
    }

    /**
     * Sets the value of the otmfzip property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTMFZIP(JAXBElement<String> value) {
        this.otmfzip = value;
    }

    /**
     * Gets the value of the otsAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSAddress1() {
        return otsAddress1;
    }

    /**
     * Sets the value of the otsAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSAddress1(JAXBElement<String> value) {
        this.otsAddress1 = value;
    }

    /**
     * Gets the value of the otsAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSAddress2() {
        return otsAddress2;
    }

    /**
     * Sets the value of the otsAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSAddress2(JAXBElement<String> value) {
        this.otsAddress2 = value;
    }

    /**
     * Gets the value of the otsAddress3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSAddress3() {
        return otsAddress3;
    }

    /**
     * Sets the value of the otsAddress3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSAddress3(JAXBElement<String> value) {
        this.otsAddress3 = value;
    }

    /**
     * Gets the value of the otsCity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSCity() {
        return otsCity;
    }

    /**
     * Sets the value of the otsCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSCity(JAXBElement<String> value) {
        this.otsCity = value;
    }

    /**
     * Gets the value of the otsCntryDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSCntryDescription() {
        return otsCntryDescription;
    }

    /**
     * Sets the value of the otsCntryDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSCntryDescription(JAXBElement<String> value) {
        this.otsCntryDescription = value;
    }

    /**
     * Gets the value of the otsContact property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSContact() {
        return otsContact;
    }

    /**
     * Sets the value of the otsContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSContact(JAXBElement<String> value) {
        this.otsContact = value;
    }

    /**
     * Gets the value of the otsCountryNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOTSCountryNum() {
        return otsCountryNum;
    }

    /**
     * Sets the value of the otsCountryNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOTSCountryNum(Integer value) {
        this.otsCountryNum = value;
    }

    /**
     * Gets the value of the otsCustSaved property.
     * This getter has been renamed from isOTSCustSaved() to getOTSCustSaved() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOTSCustSaved() {
        return otsCustSaved;
    }

    /**
     * Sets the value of the otsCustSaved property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOTSCustSaved(Boolean value) {
        this.otsCustSaved = value;
    }

    /**
     * Gets the value of the otsFaxNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSFaxNum() {
        return otsFaxNum;
    }

    /**
     * Sets the value of the otsFaxNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSFaxNum(JAXBElement<String> value) {
        this.otsFaxNum = value;
    }

    /**
     * Gets the value of the otsName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSName() {
        return otsName;
    }

    /**
     * Sets the value of the otsName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSName(JAXBElement<String> value) {
        this.otsName = value;
    }

    /**
     * Gets the value of the otsPhoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSPhoneNum() {
        return otsPhoneNum;
    }

    /**
     * Sets the value of the otsPhoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSPhoneNum(JAXBElement<String> value) {
        this.otsPhoneNum = value;
    }

    /**
     * Gets the value of the otsResaleID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSResaleID() {
        return otsResaleID;
    }

    /**
     * Sets the value of the otsResaleID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSResaleID(JAXBElement<String> value) {
        this.otsResaleID = value;
    }

    /**
     * Gets the value of the otsSaveAs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSSaveAs() {
        return otsSaveAs;
    }

    /**
     * Sets the value of the otsSaveAs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSSaveAs(JAXBElement<String> value) {
        this.otsSaveAs = value;
    }

    /**
     * Gets the value of the otsSaveCustID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSSaveCustID() {
        return otsSaveCustID;
    }

    /**
     * Sets the value of the otsSaveCustID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSSaveCustID(JAXBElement<String> value) {
        this.otsSaveCustID = value;
    }

    /**
     * Gets the value of the otsSaved property.
     * This getter has been renamed from isOTSSaved() to getOTSSaved() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOTSSaved() {
        return otsSaved;
    }

    /**
     * Sets the value of the otsSaved property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOTSSaved(Boolean value) {
        this.otsSaved = value;
    }

    /**
     * Gets the value of the otsShipToNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSShipToNum() {
        return otsShipToNum;
    }

    /**
     * Sets the value of the otsShipToNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSShipToNum(JAXBElement<String> value) {
        this.otsShipToNum = value;
    }

    /**
     * Gets the value of the otsState property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSState() {
        return otsState;
    }

    /**
     * Sets the value of the otsState property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSState(JAXBElement<String> value) {
        this.otsState = value;
    }

    /**
     * Gets the value of the otsTaxRegionCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSTaxRegionCode() {
        return otsTaxRegionCode;
    }

    /**
     * Sets the value of the otsTaxRegionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSTaxRegionCode(JAXBElement<String> value) {
        this.otsTaxRegionCode = value;
    }

    /**
     * Gets the value of the otszip property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSZIP() {
        return otszip;
    }

    /**
     * Sets the value of the otszip property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSZIP(JAXBElement<String> value) {
        this.otszip = value;
    }

    /**
     * Gets the value of the onHandQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOnHandQuantity() {
        return onHandQuantity;
    }

    /**
     * Sets the value of the onHandQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOnHandQuantity(BigDecimal value) {
        this.onHandQuantity = value;
    }

    /**
     * Gets the value of the openOrder property.
     * This getter has been renamed from isOpenOrder() to getOpenOrder() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOpenOrder() {
        return openOrder;
    }

    /**
     * Sets the value of the openOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpenOrder(Boolean value) {
        this.openOrder = value;
    }

    /**
     * Gets the value of the openRelease property.
     * This getter has been renamed from isOpenRelease() to getOpenRelease() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOpenRelease() {
        return openRelease;
    }

    /**
     * Sets the value of the openRelease property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpenRelease(Boolean value) {
        this.openRelease = value;
    }

    /**
     * Gets the value of the orderLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderLine() {
        return orderLine;
    }

    /**
     * Sets the value of the orderLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderLine(Integer value) {
        this.orderLine = value;
    }

    /**
     * Gets the value of the orderLineLineDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderLineLineDesc() {
        return orderLineLineDesc;
    }

    /**
     * Sets the value of the orderLineLineDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderLineLineDesc(JAXBElement<String> value) {
        this.orderLineLineDesc = value;
    }

    /**
     * Gets the value of the orderNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * Sets the value of the orderNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNum(Integer value) {
        this.orderNum = value;
    }

    /**
     * Gets the value of the orderNumCardMemberName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderNumCardMemberName() {
        return orderNumCardMemberName;
    }

    /**
     * Sets the value of the orderNumCardMemberName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderNumCardMemberName(JAXBElement<String> value) {
        this.orderNumCardMemberName = value;
    }

    /**
     * Gets the value of the orderNumCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderNumCurrencyCode() {
        return orderNumCurrencyCode;
    }

    /**
     * Sets the value of the orderNumCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderNumCurrencyCode(JAXBElement<String> value) {
        this.orderNumCurrencyCode = value;
    }

    /**
     * Gets the value of the orderRelNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderRelNum() {
        return orderRelNum;
    }

    /**
     * Sets the value of the orderRelNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderRelNum(Integer value) {
        this.orderRelNum = value;
    }

    /**
     * Gets the value of the ourJobQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOurJobQty() {
        return ourJobQty;
    }

    /**
     * Sets the value of the ourJobQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOurJobQty(BigDecimal value) {
        this.ourJobQty = value;
    }

    /**
     * Gets the value of the ourJobShippedQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOurJobShippedQty() {
        return ourJobShippedQty;
    }

    /**
     * Sets the value of the ourJobShippedQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOurJobShippedQty(BigDecimal value) {
        this.ourJobShippedQty = value;
    }

    /**
     * Gets the value of the ourReqQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOurReqQty() {
        return ourReqQty;
    }

    /**
     * Sets the value of the ourReqQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOurReqQty(BigDecimal value) {
        this.ourReqQty = value;
    }

    /**
     * Gets the value of the ourStockQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOurStockQty() {
        return ourStockQty;
    }

    /**
     * Sets the value of the ourStockQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOurStockQty(BigDecimal value) {
        this.ourStockQty = value;
    }

    /**
     * Gets the value of the ourStockShippedQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOurStockShippedQty() {
        return ourStockShippedQty;
    }

    /**
     * Sets the value of the ourStockShippedQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOurStockShippedQty(BigDecimal value) {
        this.ourStockShippedQty = value;
    }

    /**
     * Gets the value of the overrideCarrier property.
     * This getter has been renamed from isOverrideCarrier() to getOverrideCarrier() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOverrideCarrier() {
        return overrideCarrier;
    }

    /**
     * Sets the value of the overrideCarrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverrideCarrier(Boolean value) {
        this.overrideCarrier = value;
    }

    /**
     * Gets the value of the overrideService property.
     * This getter has been renamed from isOverrideService() to getOverrideService() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOverrideService() {
        return overrideService;
    }

    /**
     * Sets the value of the overrideService property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverrideService(Boolean value) {
        this.overrideService = value;
    }

    /**
     * Gets the value of the poLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPOLine() {
        return poLine;
    }

    /**
     * Sets the value of the poLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPOLine(Integer value) {
        this.poLine = value;
    }

    /**
     * Gets the value of the poNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPONum() {
        return poNum;
    }

    /**
     * Sets the value of the poNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPONum(Integer value) {
        this.poNum = value;
    }

    /**
     * Gets the value of the poRelNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPORelNum() {
        return poRelNum;
    }

    /**
     * Sets the value of the poRelNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPORelNum(Integer value) {
        this.poRelNum = value;
    }

    /**
     * Gets the value of the partExists property.
     * This getter has been renamed from isPartExists() to getPartExists() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartExists() {
        return partExists;
    }

    /**
     * Sets the value of the partExists property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartExists(Boolean value) {
        this.partExists = value;
    }

    /**
     * Gets the value of the partNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNum() {
        return partNum;
    }

    /**
     * Sets the value of the partNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNum(JAXBElement<String> value) {
        this.partNum = value;
    }

    /**
     * Gets the value of the partNumIUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNumIUM() {
        return partNumIUM;
    }

    /**
     * Sets the value of the partNumIUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNumIUM(JAXBElement<String> value) {
        this.partNumIUM = value;
    }

    /**
     * Gets the value of the partNumPartDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNumPartDescription() {
        return partNumPartDescription;
    }

    /**
     * Sets the value of the partNumPartDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNumPartDescription(JAXBElement<String> value) {
        this.partNumPartDescription = value;
    }

    /**
     * Gets the value of the partNumPricePerCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNumPricePerCode() {
        return partNumPricePerCode;
    }

    /**
     * Sets the value of the partNumPricePerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNumPricePerCode(JAXBElement<String> value) {
        this.partNumPricePerCode = value;
    }

    /**
     * Gets the value of the partNumSalesUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNumSalesUM() {
        return partNumSalesUM;
    }

    /**
     * Sets the value of the partNumSalesUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNumSalesUM(JAXBElement<String> value) {
        this.partNumSalesUM = value;
    }

    /**
     * Gets the value of the partNumSellingFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPartNumSellingFactor() {
        return partNumSellingFactor;
    }

    /**
     * Sets the value of the partNumSellingFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPartNumSellingFactor(BigDecimal value) {
        this.partNumSellingFactor = value;
    }

    /**
     * Gets the value of the partNumTrackDimension property.
     * This getter has been renamed from isPartNumTrackDimension() to getPartNumTrackDimension() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartNumTrackDimension() {
        return partNumTrackDimension;
    }

    /**
     * Sets the value of the partNumTrackDimension property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartNumTrackDimension(Boolean value) {
        this.partNumTrackDimension = value;
    }

    /**
     * Gets the value of the partNumTrackLots property.
     * This getter has been renamed from isPartNumTrackLots() to getPartNumTrackLots() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartNumTrackLots() {
        return partNumTrackLots;
    }

    /**
     * Sets the value of the partNumTrackLots property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartNumTrackLots(Boolean value) {
        this.partNumTrackLots = value;
    }

    /**
     * Gets the value of the partNumTrackSerialNum property.
     * This getter has been renamed from isPartNumTrackSerialNum() to getPartNumTrackSerialNum() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartNumTrackSerialNum() {
        return partNumTrackSerialNum;
    }

    /**
     * Sets the value of the partNumTrackSerialNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartNumTrackSerialNum(Boolean value) {
        this.partNumTrackSerialNum = value;
    }

    /**
     * Gets the value of the phaseID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPhaseID() {
        return phaseID;
    }

    /**
     * Sets the value of the phaseID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPhaseID(JAXBElement<String> value) {
        this.phaseID = value;
    }

    /**
     * Gets the value of the phaseWasRecInvoiced property.
     * This getter has been renamed from isPhaseWasRecInvoiced() to getPhaseWasRecInvoiced() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPhaseWasRecInvoiced() {
        return phaseWasRecInvoiced;
    }

    /**
     * Sets the value of the phaseWasRecInvoiced property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPhaseWasRecInvoiced(Boolean value) {
        this.phaseWasRecInvoiced = value;
    }

    /**
     * Gets the value of the pickError property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPickError() {
        return pickError;
    }

    /**
     * Sets the value of the pickError property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPickError(JAXBElement<String> value) {
        this.pickError = value;
    }

    /**
     * Gets the value of the plant property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlant() {
        return plant;
    }

    /**
     * Sets the value of the plant property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlant(JAXBElement<String> value) {
        this.plant = value;
    }

    /**
     * Gets the value of the plantName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlantName() {
        return plantName;
    }

    /**
     * Sets the value of the plantName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlantName(JAXBElement<String> value) {
        this.plantName = value;
    }

    /**
     * Gets the value of the prevNeedByDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getPrevNeedByDate() {
        return prevNeedByDate;
    }

    /**
     * Sets the value of the prevNeedByDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setPrevNeedByDate(JAXBElement<XMLGregorianCalendar> value) {
        this.prevNeedByDate = value;
    }

    /**
     * Gets the value of the prevPartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrevPartNum() {
        return prevPartNum;
    }

    /**
     * Sets the value of the prevPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrevPartNum(JAXBElement<String> value) {
        this.prevPartNum = value;
    }

    /**
     * Gets the value of the prevReqDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getPrevReqDate() {
        return prevReqDate;
    }

    /**
     * Sets the value of the prevReqDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setPrevReqDate(JAXBElement<XMLGregorianCalendar> value) {
        this.prevReqDate = value;
    }

    /**
     * Gets the value of the prevSellQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrevSellQty() {
        return prevSellQty;
    }

    /**
     * Sets the value of the prevSellQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrevSellQty(BigDecimal value) {
        this.prevSellQty = value;
    }

    /**
     * Gets the value of the prevShipToNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrevShipToNum() {
        return prevShipToNum;
    }

    /**
     * Sets the value of the prevShipToNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrevShipToNum(JAXBElement<String> value) {
        this.prevShipToNum = value;
    }

    /**
     * Gets the value of the prevXPartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrevXPartNum() {
        return prevXPartNum;
    }

    /**
     * Sets the value of the prevXPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrevXPartNum(JAXBElement<String> value) {
        this.prevXPartNum = value;
    }

    /**
     * Gets the value of the projectID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProjectID() {
        return projectID;
    }

    /**
     * Sets the value of the projectID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProjectID(JAXBElement<String> value) {
        this.projectID = value;
    }

    /**
     * Gets the value of the purPoint property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPurPoint() {
        return purPoint;
    }

    /**
     * Sets the value of the purPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPurPoint(JAXBElement<String> value) {
        this.purPoint = value;
    }

    /**
     * Gets the value of the purPointAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPurPointAddress1() {
        return purPointAddress1;
    }

    /**
     * Sets the value of the purPointAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPurPointAddress1(JAXBElement<String> value) {
        this.purPointAddress1 = value;
    }

    /**
     * Gets the value of the purPointAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPurPointAddress2() {
        return purPointAddress2;
    }

    /**
     * Sets the value of the purPointAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPurPointAddress2(JAXBElement<String> value) {
        this.purPointAddress2 = value;
    }

    /**
     * Gets the value of the purPointAddress3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPurPointAddress3() {
        return purPointAddress3;
    }

    /**
     * Sets the value of the purPointAddress3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPurPointAddress3(JAXBElement<String> value) {
        this.purPointAddress3 = value;
    }

    /**
     * Gets the value of the purPointCity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPurPointCity() {
        return purPointCity;
    }

    /**
     * Sets the value of the purPointCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPurPointCity(JAXBElement<String> value) {
        this.purPointCity = value;
    }

    /**
     * Gets the value of the purPointCountry property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPurPointCountry() {
        return purPointCountry;
    }

    /**
     * Sets the value of the purPointCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPurPointCountry(JAXBElement<String> value) {
        this.purPointCountry = value;
    }

    /**
     * Gets the value of the purPointName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPurPointName() {
        return purPointName;
    }

    /**
     * Sets the value of the purPointName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPurPointName(JAXBElement<String> value) {
        this.purPointName = value;
    }

    /**
     * Gets the value of the purPointPrimPCon property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPurPointPrimPCon() {
        return purPointPrimPCon;
    }

    /**
     * Sets the value of the purPointPrimPCon property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPurPointPrimPCon(Integer value) {
        this.purPointPrimPCon = value;
    }

    /**
     * Gets the value of the purPointState property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPurPointState() {
        return purPointState;
    }

    /**
     * Sets the value of the purPointState property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPurPointState(JAXBElement<String> value) {
        this.purPointState = value;
    }

    /**
     * Gets the value of the purPointZip property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPurPointZip() {
        return purPointZip;
    }

    /**
     * Sets the value of the purPointZip property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPurPointZip(JAXBElement<String> value) {
        this.purPointZip = value;
    }

    /**
     * Gets the value of the ran property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRAN() {
        return ran;
    }

    /**
     * Sets the value of the ran property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRAN(JAXBElement<String> value) {
        this.ran = value;
    }

    /**
     * Gets the value of the refNotes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRefNotes() {
        return refNotes;
    }

    /**
     * Sets the value of the refNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRefNotes(JAXBElement<String> value) {
        this.refNotes = value;
    }

    /**
     * Gets the value of the reference property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReference() {
        return reference;
    }

    /**
     * Sets the value of the reference property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReference(JAXBElement<String> value) {
        this.reference = value;
    }

    /**
     * Gets the value of the relStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRelStatus() {
        return relStatus;
    }

    /**
     * Sets the value of the relStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRelStatus(JAXBElement<String> value) {
        this.relStatus = value;
    }

    /**
     * Gets the value of the releaseStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReleaseStatus() {
        return releaseStatus;
    }

    /**
     * Sets the value of the releaseStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReleaseStatus(JAXBElement<String> value) {
        this.releaseStatus = value;
    }

    /**
     * Gets the value of the reqDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getReqDate() {
        return reqDate;
    }

    /**
     * Sets the value of the reqDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setReqDate(JAXBElement<XMLGregorianCalendar> value) {
        this.reqDate = value;
    }

    /**
     * Gets the value of the resDelivery property.
     * This getter has been renamed from isResDelivery() to getResDelivery() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getResDelivery() {
        return resDelivery;
    }

    /**
     * Sets the value of the resDelivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setResDelivery(Boolean value) {
        this.resDelivery = value;
    }

    /**
     * Gets the value of the revisionNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRevisionNum() {
        return revisionNum;
    }

    /**
     * Sets the value of the revisionNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRevisionNum(JAXBElement<String> value) {
        this.revisionNum = value;
    }

    /**
     * Gets the value of the rpt1SelfAssessTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1SelfAssessTax() {
        return rpt1SelfAssessTax;
    }

    /**
     * Sets the value of the rpt1SelfAssessTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1SelfAssessTax(BigDecimal value) {
        this.rpt1SelfAssessTax = value;
    }

    /**
     * Gets the value of the rpt1TotalTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TotalTax() {
        return rpt1TotalTax;
    }

    /**
     * Sets the value of the rpt1TotalTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TotalTax(BigDecimal value) {
        this.rpt1TotalTax = value;
    }

    /**
     * Gets the value of the rpt1WithholdTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1WithholdTax() {
        return rpt1WithholdTax;
    }

    /**
     * Sets the value of the rpt1WithholdTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1WithholdTax(BigDecimal value) {
        this.rpt1WithholdTax = value;
    }

    /**
     * Gets the value of the rpt2SelfAssessTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2SelfAssessTax() {
        return rpt2SelfAssessTax;
    }

    /**
     * Sets the value of the rpt2SelfAssessTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2SelfAssessTax(BigDecimal value) {
        this.rpt2SelfAssessTax = value;
    }

    /**
     * Gets the value of the rpt2TotalTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TotalTax() {
        return rpt2TotalTax;
    }

    /**
     * Sets the value of the rpt2TotalTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TotalTax(BigDecimal value) {
        this.rpt2TotalTax = value;
    }

    /**
     * Gets the value of the rpt2WithholdTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2WithholdTax() {
        return rpt2WithholdTax;
    }

    /**
     * Sets the value of the rpt2WithholdTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2WithholdTax(BigDecimal value) {
        this.rpt2WithholdTax = value;
    }

    /**
     * Gets the value of the rpt3SelfAssessTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3SelfAssessTax() {
        return rpt3SelfAssessTax;
    }

    /**
     * Sets the value of the rpt3SelfAssessTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3SelfAssessTax(BigDecimal value) {
        this.rpt3SelfAssessTax = value;
    }

    /**
     * Gets the value of the rpt3TotalTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TotalTax() {
        return rpt3TotalTax;
    }

    /**
     * Sets the value of the rpt3TotalTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TotalTax(BigDecimal value) {
        this.rpt3TotalTax = value;
    }

    /**
     * Gets the value of the rpt3WithholdTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3WithholdTax() {
        return rpt3WithholdTax;
    }

    /**
     * Sets the value of the rpt3WithholdTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3WithholdTax(BigDecimal value) {
        this.rpt3WithholdTax = value;
    }

    /**
     * Gets the value of the snEnable property.
     * This getter has been renamed from isSNEnable() to getSNEnable() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSNEnable() {
        return snEnable;
    }

    /**
     * Sets the value of the snEnable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSNEnable(Boolean value) {
        this.snEnable = value;
    }

    /**
     * Gets the value of the salesOrderLinked property.
     * This getter has been renamed from isSalesOrderLinked() to getSalesOrderLinked() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSalesOrderLinked() {
        return salesOrderLinked;
    }

    /**
     * Sets the value of the salesOrderLinked property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSalesOrderLinked(Boolean value) {
        this.salesOrderLinked = value;
    }

    /**
     * Gets the value of the salesUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesUM() {
        return salesUM;
    }

    /**
     * Sets the value of the salesUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesUM(JAXBElement<String> value) {
        this.salesUM = value;
    }

    /**
     * Gets the value of the satDelivery property.
     * This getter has been renamed from isSatDelivery() to getSatDelivery() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSatDelivery() {
        return satDelivery;
    }

    /**
     * Sets the value of the satDelivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSatDelivery(Boolean value) {
        this.satDelivery = value;
    }

    /**
     * Gets the value of the satPickup property.
     * This getter has been renamed from isSatPickup() to getSatPickup() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSatPickup() {
        return satPickup;
    }

    /**
     * Sets the value of the satPickup property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSatPickup(Boolean value) {
        this.satPickup = value;
    }

    /**
     * Gets the value of the scheduleNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getScheduleNumber() {
        return scheduleNumber;
    }

    /**
     * Sets the value of the scheduleNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setScheduleNumber(JAXBElement<String> value) {
        this.scheduleNumber = value;
    }

    /**
     * Gets the value of the selectForPicking property.
     * This getter has been renamed from isSelectForPicking() to getSelectForPicking() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSelectForPicking() {
        return selectForPicking;
    }

    /**
     * Sets the value of the selectForPicking property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSelectForPicking(Boolean value) {
        this.selectForPicking = value;
    }

    /**
     * Gets the value of the selfAssessTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSelfAssessTax() {
        return selfAssessTax;
    }

    /**
     * Sets the value of the selfAssessTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSelfAssessTax(BigDecimal value) {
        this.selfAssessTax = value;
    }

    /**
     * Gets the value of the sellingFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSellingFactor() {
        return sellingFactor;
    }

    /**
     * Sets the value of the sellingFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSellingFactor(BigDecimal value) {
        this.sellingFactor = value;
    }

    /**
     * Gets the value of the sellingFactorDirection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSellingFactorDirection() {
        return sellingFactorDirection;
    }

    /**
     * Sets the value of the sellingFactorDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSellingFactorDirection(JAXBElement<String> value) {
        this.sellingFactorDirection = value;
    }

    /**
     * Gets the value of the sellingJobQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSellingJobQty() {
        return sellingJobQty;
    }

    /**
     * Sets the value of the sellingJobQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSellingJobQty(BigDecimal value) {
        this.sellingJobQty = value;
    }

    /**
     * Gets the value of the sellingJobShippedQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSellingJobShippedQty() {
        return sellingJobShippedQty;
    }

    /**
     * Sets the value of the sellingJobShippedQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSellingJobShippedQty(BigDecimal value) {
        this.sellingJobShippedQty = value;
    }

    /**
     * Gets the value of the sellingReqQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSellingReqQty() {
        return sellingReqQty;
    }

    /**
     * Sets the value of the sellingReqQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSellingReqQty(BigDecimal value) {
        this.sellingReqQty = value;
    }

    /**
     * Gets the value of the sellingStockQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSellingStockQty() {
        return sellingStockQty;
    }

    /**
     * Sets the value of the sellingStockQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSellingStockQty(BigDecimal value) {
        this.sellingStockQty = value;
    }

    /**
     * Gets the value of the sellingStockShippedQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSellingStockShippedQty() {
        return sellingStockShippedQty;
    }

    /**
     * Sets the value of the sellingStockShippedQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSellingStockShippedQty(BigDecimal value) {
        this.sellingStockShippedQty = value;
    }

    /**
     * Gets the value of the servAOD property.
     * This getter has been renamed from isServAOD() to getServAOD() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getServAOD() {
        return servAOD;
    }

    /**
     * Sets the value of the servAOD property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServAOD(Boolean value) {
        this.servAOD = value;
    }

    /**
     * Gets the value of the servAlert property.
     * This getter has been renamed from isServAlert() to getServAlert() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getServAlert() {
        return servAlert;
    }

    /**
     * Sets the value of the servAlert property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServAlert(Boolean value) {
        this.servAlert = value;
    }

    /**
     * Gets the value of the servAuthNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServAuthNum() {
        return servAuthNum;
    }

    /**
     * Sets the value of the servAuthNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServAuthNum(JAXBElement<String> value) {
        this.servAuthNum = value;
    }

    /**
     * Gets the value of the servDeliveryDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getServDeliveryDate() {
        return servDeliveryDate;
    }

    /**
     * Sets the value of the servDeliveryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setServDeliveryDate(JAXBElement<XMLGregorianCalendar> value) {
        this.servDeliveryDate = value;
    }

    /**
     * Gets the value of the servHomeDel property.
     * This getter has been renamed from isServHomeDel() to getServHomeDel() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getServHomeDel() {
        return servHomeDel;
    }

    /**
     * Sets the value of the servHomeDel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServHomeDel(Boolean value) {
        this.servHomeDel = value;
    }

    /**
     * Gets the value of the servInstruct property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServInstruct() {
        return servInstruct;
    }

    /**
     * Sets the value of the servInstruct property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServInstruct(JAXBElement<String> value) {
        this.servInstruct = value;
    }

    /**
     * Gets the value of the servPOD property.
     * This getter has been renamed from isServPOD() to getServPOD() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getServPOD() {
        return servPOD;
    }

    /**
     * Sets the value of the servPOD property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServPOD(Boolean value) {
        this.servPOD = value;
    }

    /**
     * Gets the value of the servPhone property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServPhone() {
        return servPhone;
    }

    /**
     * Sets the value of the servPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServPhone(JAXBElement<String> value) {
        this.servPhone = value;
    }

    /**
     * Gets the value of the servRef1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServRef1() {
        return servRef1;
    }

    /**
     * Sets the value of the servRef1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServRef1(JAXBElement<String> value) {
        this.servRef1 = value;
    }

    /**
     * Gets the value of the servRef2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServRef2() {
        return servRef2;
    }

    /**
     * Sets the value of the servRef2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServRef2(JAXBElement<String> value) {
        this.servRef2 = value;
    }

    /**
     * Gets the value of the servRef3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServRef3() {
        return servRef3;
    }

    /**
     * Sets the value of the servRef3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServRef3(JAXBElement<String> value) {
        this.servRef3 = value;
    }

    /**
     * Gets the value of the servRef4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServRef4() {
        return servRef4;
    }

    /**
     * Sets the value of the servRef4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServRef4(JAXBElement<String> value) {
        this.servRef4 = value;
    }

    /**
     * Gets the value of the servRef5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServRef5() {
        return servRef5;
    }

    /**
     * Sets the value of the servRef5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServRef5(JAXBElement<String> value) {
        this.servRef5 = value;
    }

    /**
     * Gets the value of the servRelease property.
     * This getter has been renamed from isServRelease() to getServRelease() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getServRelease() {
        return servRelease;
    }

    /**
     * Sets the value of the servRelease property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServRelease(Boolean value) {
        this.servRelease = value;
    }

    /**
     * Gets the value of the servSatDelivery property.
     * This getter has been renamed from isServSatDelivery() to getServSatDelivery() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getServSatDelivery() {
        return servSatDelivery;
    }

    /**
     * Sets the value of the servSatDelivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServSatDelivery(Boolean value) {
        this.servSatDelivery = value;
    }

    /**
     * Gets the value of the servSatPickup property.
     * This getter has been renamed from isServSatPickup() to getServSatPickup() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getServSatPickup() {
        return servSatPickup;
    }

    /**
     * Sets the value of the servSatPickup property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServSatPickup(Boolean value) {
        this.servSatPickup = value;
    }

    /**
     * Gets the value of the servSignature property.
     * This getter has been renamed from isServSignature() to getServSignature() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getServSignature() {
        return servSignature;
    }

    /**
     * Sets the value of the servSignature property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServSignature(Boolean value) {
        this.servSignature = value;
    }

    /**
     * Gets the value of the shipOvers property.
     * This getter has been renamed from isShipOvers() to getShipOvers() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getShipOvers() {
        return shipOvers;
    }

    /**
     * Sets the value of the shipOvers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShipOvers(Boolean value) {
        this.shipOvers = value;
    }

    /**
     * Gets the value of the shipRouting property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipRouting() {
        return shipRouting;
    }

    /**
     * Sets the value of the shipRouting property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipRouting(JAXBElement<String> value) {
        this.shipRouting = value;
    }

    /**
     * Gets the value of the shipToAddressList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipToAddressList() {
        return shipToAddressList;
    }

    /**
     * Sets the value of the shipToAddressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipToAddressList(JAXBElement<String> value) {
        this.shipToAddressList = value;
    }

    /**
     * Gets the value of the shipToContactEMailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipToContactEMailAddress() {
        return shipToContactEMailAddress;
    }

    /**
     * Sets the value of the shipToContactEMailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipToContactEMailAddress(JAXBElement<String> value) {
        this.shipToContactEMailAddress = value;
    }

    /**
     * Gets the value of the shipToContactName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipToContactName() {
        return shipToContactName;
    }

    /**
     * Sets the value of the shipToContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipToContactName(JAXBElement<String> value) {
        this.shipToContactName = value;
    }

    /**
     * Gets the value of the shipToCustNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShipToCustNum() {
        return shipToCustNum;
    }

    /**
     * Sets the value of the shipToCustNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShipToCustNum(Integer value) {
        this.shipToCustNum = value;
    }

    /**
     * Gets the value of the shipToNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipToNum() {
        return shipToNum;
    }

    /**
     * Sets the value of the shipToNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipToNum(JAXBElement<String> value) {
        this.shipToNum = value;
    }

    /**
     * Gets the value of the shipToSelected property.
     * This getter has been renamed from isShipToSelected() to getShipToSelected() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getShipToSelected() {
        return shipToSelected;
    }

    /**
     * Sets the value of the shipToSelected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShipToSelected(Boolean value) {
        this.shipToSelected = value;
    }

    /**
     * Gets the value of the shipViaCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipViaCode() {
        return shipViaCode;
    }

    /**
     * Sets the value of the shipViaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipViaCode(JAXBElement<String> value) {
        this.shipViaCode = value;
    }

    /**
     * Gets the value of the shipViaCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipViaCodeDescription() {
        return shipViaCodeDescription;
    }

    /**
     * Sets the value of the shipViaCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipViaCodeDescription(JAXBElement<String> value) {
        this.shipViaCodeDescription = value;
    }

    /**
     * Gets the value of the shipViaCodeWebDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipViaCodeWebDesc() {
        return shipViaCodeWebDesc;
    }

    /**
     * Sets the value of the shipViaCodeWebDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipViaCodeWebDesc(JAXBElement<String> value) {
        this.shipViaCodeWebDesc = value;
    }

    /**
     * Gets the value of the shipbyTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShipbyTime() {
        return shipbyTime;
    }

    /**
     * Sets the value of the shipbyTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShipbyTime(Integer value) {
        this.shipbyTime = value;
    }

    /**
     * Gets the value of the shpConNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShpConNum() {
        return shpConNum;
    }

    /**
     * Sets the value of the shpConNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShpConNum(Integer value) {
        this.shpConNum = value;
    }

    /**
     * Gets the value of the stagingBinNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStagingBinNum() {
        return stagingBinNum;
    }

    /**
     * Sets the value of the stagingBinNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStagingBinNum(JAXBElement<String> value) {
        this.stagingBinNum = value;
    }

    /**
     * Gets the value of the stagingWarehouseCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStagingWarehouseCode() {
        return stagingWarehouseCode;
    }

    /**
     * Sets the value of the stagingWarehouseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStagingWarehouseCode(JAXBElement<String> value) {
        this.stagingWarehouseCode = value;
    }

    /**
     * Gets the value of the subShipTo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubShipTo() {
        return subShipTo;
    }

    /**
     * Sets the value of the subShipTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubShipTo(JAXBElement<String> value) {
        this.subShipTo = value;
    }

    /**
     * Gets the value of the sysRevID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSysRevID() {
        return sysRevID;
    }

    /**
     * Sets the value of the sysRevID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSysRevID(Long value) {
        this.sysRevID = value;
    }

    /**
     * Gets the value of the tpShipToBTName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTPShipToBTName() {
        return tpShipToBTName;
    }

    /**
     * Sets the value of the tpShipToBTName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTPShipToBTName(JAXBElement<String> value) {
        this.tpShipToBTName = value;
    }

    /**
     * Gets the value of the tpShipToCustID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTPShipToCustID() {
        return tpShipToCustID;
    }

    /**
     * Sets the value of the tpShipToCustID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTPShipToCustID(JAXBElement<String> value) {
        this.tpShipToCustID = value;
    }

    /**
     * Gets the value of the tpShipToName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTPShipToName() {
        return tpShipToName;
    }

    /**
     * Sets the value of the tpShipToName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTPShipToName(JAXBElement<String> value) {
        this.tpShipToName = value;
    }

    /**
     * Gets the value of the taxConnectCalc property.
     * This getter has been renamed from isTaxConnectCalc() to getTaxConnectCalc() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getTaxConnectCalc() {
        return taxConnectCalc;
    }

    /**
     * Sets the value of the taxConnectCalc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxConnectCalc(Boolean value) {
        this.taxConnectCalc = value;
    }

    /**
     * Gets the value of the taxExempt property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxExempt() {
        return taxExempt;
    }

    /**
     * Sets the value of the taxExempt property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxExempt(JAXBElement<String> value) {
        this.taxExempt = value;
    }

    /**
     * Gets the value of the taxRegionCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxRegionCode() {
        return taxRegionCode;
    }

    /**
     * Sets the value of the taxRegionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxRegionCode(JAXBElement<String> value) {
        this.taxRegionCode = value;
    }

    /**
     * Gets the value of the taxRegionCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxRegionCodeDescription() {
        return taxRegionCodeDescription;
    }

    /**
     * Sets the value of the taxRegionCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxRegionCodeDescription(JAXBElement<String> value) {
        this.taxRegionCodeDescription = value;
    }

    /**
     * Gets the value of the thisRelInvtyQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getThisRelInvtyQty() {
        return thisRelInvtyQty;
    }

    /**
     * Sets the value of the thisRelInvtyQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setThisRelInvtyQty(BigDecimal value) {
        this.thisRelInvtyQty = value;
    }

    /**
     * Gets the value of the totalJobStockShipped property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalJobStockShipped() {
        return totalJobStockShipped;
    }

    /**
     * Sets the value of the totalJobStockShipped property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalJobStockShipped(BigDecimal value) {
        this.totalJobStockShipped = value;
    }

    /**
     * Gets the value of the totalTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalTax() {
        return totalTax;
    }

    /**
     * Sets the value of the totalTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalTax(BigDecimal value) {
        this.totalTax = value;
    }

    /**
     * Gets the value of the transportID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransportID() {
        return transportID;
    }

    /**
     * Sets the value of the transportID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransportID(JAXBElement<String> value) {
        this.transportID = value;
    }

    /**
     * Gets the value of the updateMarkForRecords property.
     * This getter has been renamed from isUpdateMarkForRecords() to getUpdateMarkForRecords() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getUpdateMarkForRecords() {
        return updateMarkForRecords;
    }

    /**
     * Sets the value of the updateMarkForRecords property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUpdateMarkForRecords(Boolean value) {
        this.updateMarkForRecords = value;
    }

    /**
     * Gets the value of the useOTMF property.
     * This getter has been renamed from isUseOTMF() to getUseOTMF() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getUseOTMF() {
        return useOTMF;
    }

    /**
     * Sets the value of the useOTMF property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseOTMF(Boolean value) {
        this.useOTMF = value;
    }

    /**
     * Gets the value of the useOTS property.
     * This getter has been renamed from isUseOTS() to getUseOTS() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getUseOTS() {
        return useOTS;
    }

    /**
     * Sets the value of the useOTS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseOTS(Boolean value) {
        this.useOTS = value;
    }

    /**
     * Gets the value of the vendorNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVendorNum() {
        return vendorNum;
    }

    /**
     * Sets the value of the vendorNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVendorNum(Integer value) {
        this.vendorNum = value;
    }

    /**
     * Gets the value of the vendorNumAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorNumAddress1() {
        return vendorNumAddress1;
    }

    /**
     * Sets the value of the vendorNumAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorNumAddress1(JAXBElement<String> value) {
        this.vendorNumAddress1 = value;
    }

    /**
     * Gets the value of the vendorNumAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorNumAddress2() {
        return vendorNumAddress2;
    }

    /**
     * Sets the value of the vendorNumAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorNumAddress2(JAXBElement<String> value) {
        this.vendorNumAddress2 = value;
    }

    /**
     * Gets the value of the vendorNumAddress3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorNumAddress3() {
        return vendorNumAddress3;
    }

    /**
     * Sets the value of the vendorNumAddress3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorNumAddress3(JAXBElement<String> value) {
        this.vendorNumAddress3 = value;
    }

    /**
     * Gets the value of the vendorNumCity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorNumCity() {
        return vendorNumCity;
    }

    /**
     * Sets the value of the vendorNumCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorNumCity(JAXBElement<String> value) {
        this.vendorNumCity = value;
    }

    /**
     * Gets the value of the vendorNumCountry property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorNumCountry() {
        return vendorNumCountry;
    }

    /**
     * Sets the value of the vendorNumCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorNumCountry(JAXBElement<String> value) {
        this.vendorNumCountry = value;
    }

    /**
     * Gets the value of the vendorNumCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorNumCurrencyCode() {
        return vendorNumCurrencyCode;
    }

    /**
     * Sets the value of the vendorNumCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorNumCurrencyCode(JAXBElement<String> value) {
        this.vendorNumCurrencyCode = value;
    }

    /**
     * Gets the value of the vendorNumDefaultFOB property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorNumDefaultFOB() {
        return vendorNumDefaultFOB;
    }

    /**
     * Sets the value of the vendorNumDefaultFOB property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorNumDefaultFOB(JAXBElement<String> value) {
        this.vendorNumDefaultFOB = value;
    }

    /**
     * Gets the value of the vendorNumName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorNumName() {
        return vendorNumName;
    }

    /**
     * Sets the value of the vendorNumName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorNumName(JAXBElement<String> value) {
        this.vendorNumName = value;
    }

    /**
     * Gets the value of the vendorNumState property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorNumState() {
        return vendorNumState;
    }

    /**
     * Sets the value of the vendorNumState property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorNumState(JAXBElement<String> value) {
        this.vendorNumState = value;
    }

    /**
     * Gets the value of the vendorNumTermsCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorNumTermsCode() {
        return vendorNumTermsCode;
    }

    /**
     * Sets the value of the vendorNumTermsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorNumTermsCode(JAXBElement<String> value) {
        this.vendorNumTermsCode = value;
    }

    /**
     * Gets the value of the vendorNumVendorID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorNumVendorID() {
        return vendorNumVendorID;
    }

    /**
     * Sets the value of the vendorNumVendorID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorNumVendorID(JAXBElement<String> value) {
        this.vendorNumVendorID = value;
    }

    /**
     * Gets the value of the vendorNumZIP property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorNumZIP() {
        return vendorNumZIP;
    }

    /**
     * Sets the value of the vendorNumZIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorNumZIP(JAXBElement<String> value) {
        this.vendorNumZIP = value;
    }

    /**
     * Gets the value of the verbalConf property.
     * This getter has been renamed from isVerbalConf() to getVerbalConf() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getVerbalConf() {
        return verbalConf;
    }

    /**
     * Sets the value of the verbalConf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVerbalConf(Boolean value) {
        this.verbalConf = value;
    }

    /**
     * Gets the value of the voidOrder property.
     * This getter has been renamed from isVoidOrder() to getVoidOrder() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getVoidOrder() {
        return voidOrder;
    }

    /**
     * Sets the value of the voidOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVoidOrder(Boolean value) {
        this.voidOrder = value;
    }

    /**
     * Gets the value of the voidRelease property.
     * This getter has been renamed from isVoidRelease() to getVoidRelease() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getVoidRelease() {
        return voidRelease;
    }

    /**
     * Sets the value of the voidRelease property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVoidRelease(Boolean value) {
        this.voidRelease = value;
    }

    /**
     * Gets the value of the wiItemPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWIItemPrice() {
        return wiItemPrice;
    }

    /**
     * Sets the value of the wiItemPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWIItemPrice(BigDecimal value) {
        this.wiItemPrice = value;
    }

    /**
     * Gets the value of the wiItemShipCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWIItemShipCost() {
        return wiItemShipCost;
    }

    /**
     * Sets the value of the wiItemShipCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWIItemShipCost(BigDecimal value) {
        this.wiItemShipCost = value;
    }

    /**
     * Gets the value of the wiOrder property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWIOrder() {
        return wiOrder;
    }

    /**
     * Sets the value of the wiOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWIOrder(JAXBElement<String> value) {
        this.wiOrder = value;
    }

    /**
     * Gets the value of the wiOrderLine property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWIOrderLine() {
        return wiOrderLine;
    }

    /**
     * Sets the value of the wiOrderLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWIOrderLine(JAXBElement<String> value) {
        this.wiOrderLine = value;
    }

    /**
     * Gets the value of the warehouseCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWarehouseCode() {
        return warehouseCode;
    }

    /**
     * Sets the value of the warehouseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWarehouseCode(JAXBElement<String> value) {
        this.warehouseCode = value;
    }

    /**
     * Gets the value of the warehouseCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWarehouseCodeDescription() {
        return warehouseCodeDescription;
    }

    /**
     * Sets the value of the warehouseCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWarehouseCodeDescription(JAXBElement<String> value) {
        this.warehouseCodeDescription = value;
    }

    /**
     * Gets the value of the wasRecInvoiced property.
     * This getter has been renamed from isWasRecInvoiced() to getWasRecInvoiced() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getWasRecInvoiced() {
        return wasRecInvoiced;
    }

    /**
     * Sets the value of the wasRecInvoiced property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWasRecInvoiced(Boolean value) {
        this.wasRecInvoiced = value;
    }

    /**
     * Gets the value of the webSKU property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWebSKU() {
        return webSKU;
    }

    /**
     * Sets the value of the webSKU property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWebSKU(JAXBElement<String> value) {
        this.webSKU = value;
    }

    /**
     * Gets the value of the withholdTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWithholdTax() {
        return withholdTax;
    }

    /**
     * Sets the value of the withholdTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWithholdTax(BigDecimal value) {
        this.withholdTax = value;
    }

}
