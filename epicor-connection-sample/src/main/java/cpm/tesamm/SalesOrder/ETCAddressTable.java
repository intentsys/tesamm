
package cpm.tesamm.SalesOrder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ETCAddressTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ETCAddressTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ETCAddressRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}ETCAddressRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ETCAddressTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "etcAddressRow"
})
public class ETCAddressTable {

    @XmlElement(name = "ETCAddressRow", nillable = true)
    protected List<ETCAddressRow> etcAddressRow;

    /**
     * Gets the value of the etcAddressRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the etcAddressRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getETCAddressRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ETCAddressRow }
     * 
     * 
     */
    public List<ETCAddressRow> getETCAddressRow() {
        if (etcAddressRow == null) {
            etcAddressRow = new ArrayList<ETCAddressRow>();
        }
        return this.etcAddressRow;
    }

    /**
     * Sets the value of the etcAddressRow property.
     * 
     * @param etcAddressRow
     *     allowed object is
     *     {@link ETCAddressRow }
     *     
     */
    public void setETCAddressRow(List<ETCAddressRow> etcAddressRow) {
        this.etcAddressRow = etcAddressRow;
    }

}
