
package cpm.tesamm.SalesOrder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SalesOrderTableset complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderTableset">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceTableset">
 *       &lt;sequence>
 *         &lt;element name="HedTaxSum" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}HedTaxSumTable" minOccurs="0"/>
 *         &lt;element name="OHOrderMsc" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OHOrderMscTable" minOccurs="0"/>
 *         &lt;element name="OrderDtl" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderDtlTable" minOccurs="0"/>
 *         &lt;element name="OrderDtlAttch" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderDtlAttchTable" minOccurs="0"/>
 *         &lt;element name="OrderHed" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderHedTable" minOccurs="0"/>
 *         &lt;element name="OrderHedAttch" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderHedAttchTable" minOccurs="0"/>
 *         &lt;element name="OrderHedUPS" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderHedUPSTable" minOccurs="0"/>
 *         &lt;element name="OrderHist" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderHistTable" minOccurs="0"/>
 *         &lt;element name="OrderMsc" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderMscTable" minOccurs="0"/>
 *         &lt;element name="OrderRel" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderRelTable" minOccurs="0"/>
 *         &lt;element name="OrderRelTax" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderRelTaxTable" minOccurs="0"/>
 *         &lt;element name="OrderRepComm" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderRepCommTable" minOccurs="0"/>
 *         &lt;element name="PartSubs" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}PartSubsTable" minOccurs="0"/>
 *         &lt;element name="SNFormat" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SNFormatTable" minOccurs="0"/>
 *         &lt;element name="SelectedSerialNumbers" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SelectedSerialNumbersTable" minOccurs="0"/>
 *         &lt;element name="TaxConnectStatus" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}TaxConnectStatusTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderTableset", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "hedTaxSum",
    "ohOrderMsc",
    "orderDtl",
    "orderDtlAttch",
    "orderHed",
    "orderHedAttch",
    "orderHedUPS",
    "orderHist",
    "orderMsc",
    "orderRel",
    "orderRelTax",
    "orderRepComm",
    "partSubs",
    "snFormat",
    "selectedSerialNumbers",
    "taxConnectStatus"
})
public class SalesOrderTableset
    extends IceTableset
{

    @XmlElementRef(name = "HedTaxSum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<HedTaxSumTable> hedTaxSum;
    @XmlElementRef(name = "OHOrderMsc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<OHOrderMscTable> ohOrderMsc;
    @XmlElementRef(name = "OrderDtl", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<OrderDtlTable> orderDtl;
    @XmlElementRef(name = "OrderDtlAttch", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<OrderDtlAttchTable> orderDtlAttch;
    @XmlElementRef(name = "OrderHed", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<OrderHedTable> orderHed;
    @XmlElementRef(name = "OrderHedAttch", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<OrderHedAttchTable> orderHedAttch;
    @XmlElementRef(name = "OrderHedUPS", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<OrderHedUPSTable> orderHedUPS;
    @XmlElementRef(name = "OrderHist", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<OrderHistTable> orderHist;
    @XmlElementRef(name = "OrderMsc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<OrderMscTable> orderMsc;
    @XmlElementRef(name = "OrderRel", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<OrderRelTable> orderRel;
    @XmlElementRef(name = "OrderRelTax", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<OrderRelTaxTable> orderRelTax;
    @XmlElementRef(name = "OrderRepComm", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<OrderRepCommTable> orderRepComm;
    @XmlElementRef(name = "PartSubs", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<PartSubsTable> partSubs;
    @XmlElementRef(name = "SNFormat", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<SNFormatTable> snFormat;
    @XmlElementRef(name = "SelectedSerialNumbers", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<SelectedSerialNumbersTable> selectedSerialNumbers;
    @XmlElementRef(name = "TaxConnectStatus", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<TaxConnectStatusTable> taxConnectStatus;

    /**
     * Gets the value of the hedTaxSum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link HedTaxSumTable }{@code >}
     *     
     */
    public JAXBElement<HedTaxSumTable> getHedTaxSum() {
        return hedTaxSum;
    }

    /**
     * Sets the value of the hedTaxSum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link HedTaxSumTable }{@code >}
     *     
     */
    public void setHedTaxSum(JAXBElement<HedTaxSumTable> value) {
        this.hedTaxSum = value;
    }

    /**
     * Gets the value of the ohOrderMsc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OHOrderMscTable }{@code >}
     *     
     */
    public JAXBElement<OHOrderMscTable> getOHOrderMsc() {
        return ohOrderMsc;
    }

    /**
     * Sets the value of the ohOrderMsc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OHOrderMscTable }{@code >}
     *     
     */
    public void setOHOrderMsc(JAXBElement<OHOrderMscTable> value) {
        this.ohOrderMsc = value;
    }

    /**
     * Gets the value of the orderDtl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrderDtlTable }{@code >}
     *     
     */
    public JAXBElement<OrderDtlTable> getOrderDtl() {
        return orderDtl;
    }

    /**
     * Sets the value of the orderDtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrderDtlTable }{@code >}
     *     
     */
    public void setOrderDtl(JAXBElement<OrderDtlTable> value) {
        this.orderDtl = value;
    }

    /**
     * Gets the value of the orderDtlAttch property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrderDtlAttchTable }{@code >}
     *     
     */
    public JAXBElement<OrderDtlAttchTable> getOrderDtlAttch() {
        return orderDtlAttch;
    }

    /**
     * Sets the value of the orderDtlAttch property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrderDtlAttchTable }{@code >}
     *     
     */
    public void setOrderDtlAttch(JAXBElement<OrderDtlAttchTable> value) {
        this.orderDtlAttch = value;
    }

    /**
     * Gets the value of the orderHed property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrderHedTable }{@code >}
     *     
     */
    public JAXBElement<OrderHedTable> getOrderHed() {
        return orderHed;
    }

    /**
     * Sets the value of the orderHed property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrderHedTable }{@code >}
     *     
     */
    public void setOrderHed(JAXBElement<OrderHedTable> value) {
        this.orderHed = value;
    }

    /**
     * Gets the value of the orderHedAttch property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrderHedAttchTable }{@code >}
     *     
     */
    public JAXBElement<OrderHedAttchTable> getOrderHedAttch() {
        return orderHedAttch;
    }

    /**
     * Sets the value of the orderHedAttch property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrderHedAttchTable }{@code >}
     *     
     */
    public void setOrderHedAttch(JAXBElement<OrderHedAttchTable> value) {
        this.orderHedAttch = value;
    }

    /**
     * Gets the value of the orderHedUPS property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrderHedUPSTable }{@code >}
     *     
     */
    public JAXBElement<OrderHedUPSTable> getOrderHedUPS() {
        return orderHedUPS;
    }

    /**
     * Sets the value of the orderHedUPS property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrderHedUPSTable }{@code >}
     *     
     */
    public void setOrderHedUPS(JAXBElement<OrderHedUPSTable> value) {
        this.orderHedUPS = value;
    }

    /**
     * Gets the value of the orderHist property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrderHistTable }{@code >}
     *     
     */
    public JAXBElement<OrderHistTable> getOrderHist() {
        return orderHist;
    }

    /**
     * Sets the value of the orderHist property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrderHistTable }{@code >}
     *     
     */
    public void setOrderHist(JAXBElement<OrderHistTable> value) {
        this.orderHist = value;
    }

    /**
     * Gets the value of the orderMsc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrderMscTable }{@code >}
     *     
     */
    public JAXBElement<OrderMscTable> getOrderMsc() {
        return orderMsc;
    }

    /**
     * Sets the value of the orderMsc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrderMscTable }{@code >}
     *     
     */
    public void setOrderMsc(JAXBElement<OrderMscTable> value) {
        this.orderMsc = value;
    }

    /**
     * Gets the value of the orderRel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrderRelTable }{@code >}
     *     
     */
    public JAXBElement<OrderRelTable> getOrderRel() {
        return orderRel;
    }

    /**
     * Sets the value of the orderRel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrderRelTable }{@code >}
     *     
     */
    public void setOrderRel(JAXBElement<OrderRelTable> value) {
        this.orderRel = value;
    }

    /**
     * Gets the value of the orderRelTax property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrderRelTaxTable }{@code >}
     *     
     */
    public JAXBElement<OrderRelTaxTable> getOrderRelTax() {
        return orderRelTax;
    }

    /**
     * Sets the value of the orderRelTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrderRelTaxTable }{@code >}
     *     
     */
    public void setOrderRelTax(JAXBElement<OrderRelTaxTable> value) {
        this.orderRelTax = value;
    }

    /**
     * Gets the value of the orderRepComm property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrderRepCommTable }{@code >}
     *     
     */
    public JAXBElement<OrderRepCommTable> getOrderRepComm() {
        return orderRepComm;
    }

    /**
     * Sets the value of the orderRepComm property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrderRepCommTable }{@code >}
     *     
     */
    public void setOrderRepComm(JAXBElement<OrderRepCommTable> value) {
        this.orderRepComm = value;
    }

    /**
     * Gets the value of the partSubs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PartSubsTable }{@code >}
     *     
     */
    public JAXBElement<PartSubsTable> getPartSubs() {
        return partSubs;
    }

    /**
     * Sets the value of the partSubs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PartSubsTable }{@code >}
     *     
     */
    public void setPartSubs(JAXBElement<PartSubsTable> value) {
        this.partSubs = value;
    }

    /**
     * Gets the value of the snFormat property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SNFormatTable }{@code >}
     *     
     */
    public JAXBElement<SNFormatTable> getSNFormat() {
        return snFormat;
    }

    /**
     * Sets the value of the snFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SNFormatTable }{@code >}
     *     
     */
    public void setSNFormat(JAXBElement<SNFormatTable> value) {
        this.snFormat = value;
    }

    /**
     * Gets the value of the selectedSerialNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SelectedSerialNumbersTable }{@code >}
     *     
     */
    public JAXBElement<SelectedSerialNumbersTable> getSelectedSerialNumbers() {
        return selectedSerialNumbers;
    }

    /**
     * Sets the value of the selectedSerialNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SelectedSerialNumbersTable }{@code >}
     *     
     */
    public void setSelectedSerialNumbers(JAXBElement<SelectedSerialNumbersTable> value) {
        this.selectedSerialNumbers = value;
    }

    /**
     * Gets the value of the taxConnectStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TaxConnectStatusTable }{@code >}
     *     
     */
    public JAXBElement<TaxConnectStatusTable> getTaxConnectStatus() {
        return taxConnectStatus;
    }

    /**
     * Sets the value of the taxConnectStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TaxConnectStatusTable }{@code >}
     *     
     */
    public void setTaxConnectStatus(JAXBElement<TaxConnectStatusTable> value) {
        this.taxConnectStatus = value;
    }

}
