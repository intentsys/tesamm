
package cpm.tesamm.SalesOrder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ETCMessageTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ETCMessageTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ETCMessageRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}ETCMessageRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ETCMessageTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "etcMessageRow"
})
public class ETCMessageTable {

    @XmlElement(name = "ETCMessageRow", nillable = true)
    protected List<ETCMessageRow> etcMessageRow;

    /**
     * Gets the value of the etcMessageRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the etcMessageRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getETCMessageRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ETCMessageRow }
     * 
     * 
     */
    public List<ETCMessageRow> getETCMessageRow() {
        if (etcMessageRow == null) {
            etcMessageRow = new ArrayList<ETCMessageRow>();
        }
        return this.etcMessageRow;
    }

    /**
     * Sets the value of the etcMessageRow property.
     * 
     * @param etcMessageRow
     *     allowed object is
     *     {@link ETCMessageRow }
     *     
     */
    public void setETCMessageRow(List<ETCMessageRow> etcMessageRow) {
        this.etcMessageRow = etcMessageRow;
    }

}
