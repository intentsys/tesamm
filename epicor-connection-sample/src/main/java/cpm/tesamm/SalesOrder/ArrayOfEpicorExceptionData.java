
package cpm.tesamm.SalesOrder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfEpicorExceptionData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfEpicorExceptionData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EpicorExceptionData" type="{http://schemas.datacontract.org/2004/07/Ice.Common}EpicorExceptionData" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfEpicorExceptionData", namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", propOrder = {
    "epicorExceptionData"
})
public class ArrayOfEpicorExceptionData {

    @XmlElement(name = "EpicorExceptionData", nillable = true)
    protected List<EpicorExceptionData> epicorExceptionData;

    /**
     * Gets the value of the epicorExceptionData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the epicorExceptionData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEpicorExceptionData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EpicorExceptionData }
     * 
     * 
     */
    public List<EpicorExceptionData> getEpicorExceptionData() {
        if (epicorExceptionData == null) {
            epicorExceptionData = new ArrayList<EpicorExceptionData>();
        }
        return this.epicorExceptionData;
    }

    /**
     * Sets the value of the epicorExceptionData property.
     * 
     * @param epicorExceptionData
     *     allowed object is
     *     {@link EpicorExceptionData }
     *     
     */
    public void setEpicorExceptionData(List<EpicorExceptionData> epicorExceptionData) {
        this.epicorExceptionData = epicorExceptionData;
    }

}
