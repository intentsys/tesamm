
package cpm.tesamm.SalesOrder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SaveOTSParamsRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SaveOTSParamsRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceRow">
 *       &lt;sequence>
 *         &lt;element name="OTSAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSAddress3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSCountryNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OTSCustSaved" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OTSFaxNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OTSPhoneNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSResaleID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSSaveAs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSSaveCustID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSSaved" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OTSShipToNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSTaxRegionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSZIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SaveOTSParamsRow", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "otsAddress1",
    "otsAddress2",
    "otsAddress3",
    "otsCity",
    "otsContact",
    "otsCountryNum",
    "otsCustSaved",
    "otsFaxNum",
    "otsName",
    "otsOverride",
    "otsPhoneNum",
    "otsResaleID",
    "otsSaveAs",
    "otsSaveCustID",
    "otsSaved",
    "otsShipToNum",
    "otsState",
    "otsTaxRegionCode",
    "otszip"
})
public class SaveOTSParamsRow
    extends IceRow
{

    @XmlElementRef(name = "OTSAddress1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsAddress1;
    @XmlElementRef(name = "OTSAddress2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsAddress2;
    @XmlElementRef(name = "OTSAddress3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsAddress3;
    @XmlElementRef(name = "OTSCity", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsCity;
    @XmlElementRef(name = "OTSContact", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsContact;
    @XmlElement(name = "OTSCountryNum")
    protected Integer otsCountryNum;
    @XmlElement(name = "OTSCustSaved")
    protected Boolean otsCustSaved;
    @XmlElementRef(name = "OTSFaxNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsFaxNum;
    @XmlElementRef(name = "OTSName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsName;
    @XmlElement(name = "OTSOverride")
    protected Boolean otsOverride;
    @XmlElementRef(name = "OTSPhoneNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsPhoneNum;
    @XmlElementRef(name = "OTSResaleID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsResaleID;
    @XmlElementRef(name = "OTSSaveAs", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsSaveAs;
    @XmlElementRef(name = "OTSSaveCustID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsSaveCustID;
    @XmlElement(name = "OTSSaved")
    protected Boolean otsSaved;
    @XmlElementRef(name = "OTSShipToNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsShipToNum;
    @XmlElementRef(name = "OTSState", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsState;
    @XmlElementRef(name = "OTSTaxRegionCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsTaxRegionCode;
    @XmlElementRef(name = "OTSZIP", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otszip;

    /**
     * Gets the value of the otsAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSAddress1() {
        return otsAddress1;
    }

    /**
     * Sets the value of the otsAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSAddress1(JAXBElement<String> value) {
        this.otsAddress1 = value;
    }

    /**
     * Gets the value of the otsAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSAddress2() {
        return otsAddress2;
    }

    /**
     * Sets the value of the otsAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSAddress2(JAXBElement<String> value) {
        this.otsAddress2 = value;
    }

    /**
     * Gets the value of the otsAddress3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSAddress3() {
        return otsAddress3;
    }

    /**
     * Sets the value of the otsAddress3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSAddress3(JAXBElement<String> value) {
        this.otsAddress3 = value;
    }

    /**
     * Gets the value of the otsCity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSCity() {
        return otsCity;
    }

    /**
     * Sets the value of the otsCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSCity(JAXBElement<String> value) {
        this.otsCity = value;
    }

    /**
     * Gets the value of the otsContact property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSContact() {
        return otsContact;
    }

    /**
     * Sets the value of the otsContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSContact(JAXBElement<String> value) {
        this.otsContact = value;
    }

    /**
     * Gets the value of the otsCountryNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOTSCountryNum() {
        return otsCountryNum;
    }

    /**
     * Sets the value of the otsCountryNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOTSCountryNum(Integer value) {
        this.otsCountryNum = value;
    }

    /**
     * Gets the value of the otsCustSaved property.
     * This getter has been renamed from isOTSCustSaved() to getOTSCustSaved() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOTSCustSaved() {
        return otsCustSaved;
    }

    /**
     * Sets the value of the otsCustSaved property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOTSCustSaved(Boolean value) {
        this.otsCustSaved = value;
    }

    /**
     * Gets the value of the otsFaxNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSFaxNum() {
        return otsFaxNum;
    }

    /**
     * Sets the value of the otsFaxNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSFaxNum(JAXBElement<String> value) {
        this.otsFaxNum = value;
    }

    /**
     * Gets the value of the otsName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSName() {
        return otsName;
    }

    /**
     * Sets the value of the otsName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSName(JAXBElement<String> value) {
        this.otsName = value;
    }

    /**
     * Gets the value of the otsOverride property.
     * This getter has been renamed from isOTSOverride() to getOTSOverride() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOTSOverride() {
        return otsOverride;
    }

    /**
     * Sets the value of the otsOverride property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOTSOverride(Boolean value) {
        this.otsOverride = value;
    }

    /**
     * Gets the value of the otsPhoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSPhoneNum() {
        return otsPhoneNum;
    }

    /**
     * Sets the value of the otsPhoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSPhoneNum(JAXBElement<String> value) {
        this.otsPhoneNum = value;
    }

    /**
     * Gets the value of the otsResaleID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSResaleID() {
        return otsResaleID;
    }

    /**
     * Sets the value of the otsResaleID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSResaleID(JAXBElement<String> value) {
        this.otsResaleID = value;
    }

    /**
     * Gets the value of the otsSaveAs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSSaveAs() {
        return otsSaveAs;
    }

    /**
     * Sets the value of the otsSaveAs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSSaveAs(JAXBElement<String> value) {
        this.otsSaveAs = value;
    }

    /**
     * Gets the value of the otsSaveCustID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSSaveCustID() {
        return otsSaveCustID;
    }

    /**
     * Sets the value of the otsSaveCustID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSSaveCustID(JAXBElement<String> value) {
        this.otsSaveCustID = value;
    }

    /**
     * Gets the value of the otsSaved property.
     * This getter has been renamed from isOTSSaved() to getOTSSaved() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOTSSaved() {
        return otsSaved;
    }

    /**
     * Sets the value of the otsSaved property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOTSSaved(Boolean value) {
        this.otsSaved = value;
    }

    /**
     * Gets the value of the otsShipToNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSShipToNum() {
        return otsShipToNum;
    }

    /**
     * Sets the value of the otsShipToNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSShipToNum(JAXBElement<String> value) {
        this.otsShipToNum = value;
    }

    /**
     * Gets the value of the otsState property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSState() {
        return otsState;
    }

    /**
     * Sets the value of the otsState property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSState(JAXBElement<String> value) {
        this.otsState = value;
    }

    /**
     * Gets the value of the otsTaxRegionCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSTaxRegionCode() {
        return otsTaxRegionCode;
    }

    /**
     * Sets the value of the otsTaxRegionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSTaxRegionCode(JAXBElement<String> value) {
        this.otsTaxRegionCode = value;
    }

    /**
     * Gets the value of the otszip property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSZIP() {
        return otszip;
    }

    /**
     * Sets the value of the otszip property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSZIP(JAXBElement<String> value) {
        this.otszip = value;
    }

}
