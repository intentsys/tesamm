
package cpm.tesamm.SalesOrder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ds" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SalesOrderTableset" minOccurs="0"/>
 *         &lt;element name="pcMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pcNeqQtyAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="opWarningMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cSellingQuantityChangedMsgText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ds",
    "pcMessage",
    "pcNeqQtyAction",
    "opWarningMsg",
    "cSellingQuantityChangedMsgText"
})
@XmlRootElement(name = "ChangeSellingQtyMasterResponse")
public class ChangeSellingQtyMasterResponse {

    @XmlElementRef(name = "ds", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<SalesOrderTableset> ds;
    @XmlElementRef(name = "pcMessage", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pcMessage;
    @XmlElementRef(name = "pcNeqQtyAction", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pcNeqQtyAction;
    @XmlElementRef(name = "opWarningMsg", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> opWarningMsg;
    @XmlElementRef(name = "cSellingQuantityChangedMsgText", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cSellingQuantityChangedMsgText;

    /**
     * Gets the value of the ds property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}
     *     
     */
    public JAXBElement<SalesOrderTableset> getDs() {
        return ds;
    }

    /**
     * Sets the value of the ds property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}
     *     
     */
    public void setDs(JAXBElement<SalesOrderTableset> value) {
        this.ds = value;
    }

    /**
     * Gets the value of the pcMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPcMessage() {
        return pcMessage;
    }

    /**
     * Sets the value of the pcMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPcMessage(JAXBElement<String> value) {
        this.pcMessage = value;
    }

    /**
     * Gets the value of the pcNeqQtyAction property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPcNeqQtyAction() {
        return pcNeqQtyAction;
    }

    /**
     * Sets the value of the pcNeqQtyAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPcNeqQtyAction(JAXBElement<String> value) {
        this.pcNeqQtyAction = value;
    }

    /**
     * Gets the value of the opWarningMsg property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOpWarningMsg() {
        return opWarningMsg;
    }

    /**
     * Sets the value of the opWarningMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOpWarningMsg(JAXBElement<String> value) {
        this.opWarningMsg = value;
    }

    /**
     * Gets the value of the cSellingQuantityChangedMsgText property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCSellingQuantityChangedMsgText() {
        return cSellingQuantityChangedMsgText;
    }

    /**
     * Sets the value of the cSellingQuantityChangedMsgText property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCSellingQuantityChangedMsgText(JAXBElement<String> value) {
        this.cSellingQuantityChangedMsgText = value;
    }

}
