
package cpm.tesamm.SalesOrder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for JobProdTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JobProdTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="JobProdRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}JobProdRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JobProdTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "jobProdRow"
})
public class JobProdTable {

    @XmlElement(name = "JobProdRow", nillable = true)
    protected List<JobProdRow> jobProdRow;

    /**
     * Gets the value of the jobProdRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the jobProdRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getJobProdRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JobProdRow }
     * 
     * 
     */
    public List<JobProdRow> getJobProdRow() {
        if (jobProdRow == null) {
            jobProdRow = new ArrayList<JobProdRow>();
        }
        return this.jobProdRow;
    }

    /**
     * Sets the value of the jobProdRow property.
     * 
     * @param jobProdRow
     *     allowed object is
     *     {@link JobProdRow }
     *     
     */
    public void setJobProdRow(List<JobProdRow> jobProdRow) {
        this.jobProdRow = jobProdRow;
    }

}
