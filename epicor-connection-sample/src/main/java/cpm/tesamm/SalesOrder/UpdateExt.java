
package cpm.tesamm.SalesOrder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ds" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}UpdExtSalesOrderTableset" minOccurs="0"/>
 *         &lt;element name="continueProcessingOnError" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="rollbackParentOnChildError" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ds",
    "continueProcessingOnError",
    "rollbackParentOnChildError"
})
@XmlRootElement(name = "UpdateExt")
public class UpdateExt {

    @XmlElementRef(name = "ds", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<UpdExtSalesOrderTableset> ds;
    protected Boolean continueProcessingOnError;
    protected Boolean rollbackParentOnChildError;

    /**
     * Gets the value of the ds property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link UpdExtSalesOrderTableset }{@code >}
     *     
     */
    public JAXBElement<UpdExtSalesOrderTableset> getDs() {
        return ds;
    }

    /**
     * Sets the value of the ds property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link UpdExtSalesOrderTableset }{@code >}
     *     
     */
    public void setDs(JAXBElement<UpdExtSalesOrderTableset> value) {
        this.ds = value;
    }

    /**
     * Gets the value of the continueProcessingOnError property.
     * This getter has been renamed from isContinueProcessingOnError() to getContinueProcessingOnError() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getContinueProcessingOnError() {
        return continueProcessingOnError;
    }

    /**
     * Sets the value of the continueProcessingOnError property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContinueProcessingOnError(Boolean value) {
        this.continueProcessingOnError = value;
    }

    /**
     * Gets the value of the rollbackParentOnChildError property.
     * This getter has been renamed from isRollbackParentOnChildError() to getRollbackParentOnChildError() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRollbackParentOnChildError() {
        return rollbackParentOnChildError;
    }

    /**
     * Sets the value of the rollbackParentOnChildError property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRollbackParentOnChildError(Boolean value) {
        this.rollbackParentOnChildError = value;
    }

}
