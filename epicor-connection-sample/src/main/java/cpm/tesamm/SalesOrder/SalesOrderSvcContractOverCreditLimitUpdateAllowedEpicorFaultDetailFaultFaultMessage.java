
package cpm.tesamm.SalesOrder;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.5.1
 * 2016-01-18T17:51:45.212+08:00
 * Generated source version: 2.5.1
 */

@WebFault(name = "EpicorFaultDetail", targetNamespace = "http://schemas.datacontract.org/2004/07/Ice.Common")
public class SalesOrderSvcContractOverCreditLimitUpdateAllowedEpicorFaultDetailFaultFaultMessage extends Exception {
    
    private cpm.tesamm.SalesOrder.EpicorFaultDetail epicorFaultDetail;

    public SalesOrderSvcContractOverCreditLimitUpdateAllowedEpicorFaultDetailFaultFaultMessage() {
        super();
    }
    
    public SalesOrderSvcContractOverCreditLimitUpdateAllowedEpicorFaultDetailFaultFaultMessage(String message) {
        super(message);
    }
    
    public SalesOrderSvcContractOverCreditLimitUpdateAllowedEpicorFaultDetailFaultFaultMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public SalesOrderSvcContractOverCreditLimitUpdateAllowedEpicorFaultDetailFaultFaultMessage(String message, cpm.tesamm.SalesOrder.EpicorFaultDetail epicorFaultDetail) {
        super(message);
        this.epicorFaultDetail = epicorFaultDetail;
    }

    public SalesOrderSvcContractOverCreditLimitUpdateAllowedEpicorFaultDetailFaultFaultMessage(String message, cpm.tesamm.SalesOrder.EpicorFaultDetail epicorFaultDetail, Throwable cause) {
        super(message, cause);
        this.epicorFaultDetail = epicorFaultDetail;
    }

    public cpm.tesamm.SalesOrder.EpicorFaultDetail getFaultInfo() {
        return this.epicorFaultDetail;
    }
}
