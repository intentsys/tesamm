
package cpm.tesamm.SalesOrder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="opIssueWarning" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "opIssueWarning"
})
@XmlRootElement(name = "CheckMakeDirectReleasesResponse")
public class CheckMakeDirectReleasesResponse {

    protected Boolean opIssueWarning;

    /**
     * Gets the value of the opIssueWarning property.
     * This getter has been renamed from isOpIssueWarning() to getOpIssueWarning() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOpIssueWarning() {
        return opIssueWarning;
    }

    /**
     * Sets the value of the opIssueWarning property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpIssueWarning(Boolean value) {
        this.opIssueWarning = value;
    }

}
