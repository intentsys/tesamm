
package cpm.tesamm.SalesOrder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ipCalcQtyPref" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ipCalcQtyPref"
})
@XmlRootElement(name = "SetCalcQtysPref")
public class SetCalcQtysPref {

    protected Boolean ipCalcQtyPref;

    /**
     * Gets the value of the ipCalcQtyPref property.
     * This getter has been renamed from isIpCalcQtyPref() to getIpCalcQtyPref() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIpCalcQtyPref() {
        return ipCalcQtyPref;
    }

    /**
     * Sets the value of the ipCalcQtyPref property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIpCalcQtyPref(Boolean value) {
        this.ipCalcQtyPref = value;
    }

}
