
package cpm.tesamm.SalesOrder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderHedListTableset complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderHedListTableset">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceTableset">
 *       &lt;sequence>
 *         &lt;element name="OrderHedList" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderHedListTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderHedListTableset", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "orderHedList"
})
public class OrderHedListTableset
    extends IceTableset
{

    @XmlElementRef(name = "OrderHedList", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<OrderHedListTable> orderHedList;

    /**
     * Gets the value of the orderHedList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrderHedListTable }{@code >}
     *     
     */
    public JAXBElement<OrderHedListTable> getOrderHedList() {
        return orderHedList;
    }

    /**
     * Sets the value of the orderHedList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrderHedListTable }{@code >}
     *     
     */
    public void setOrderHedList(JAXBElement<OrderHedListTable> value) {
        this.orderHedList = value;
    }

}
