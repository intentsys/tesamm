
package cpm.tesamm.SalesOrder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderHedListTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderHedListTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderHedListRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderHedListRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderHedListTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "orderHedListRow"
})
public class OrderHedListTable {

    @XmlElement(name = "OrderHedListRow", nillable = true)
    protected List<OrderHedListRow> orderHedListRow;

    /**
     * Gets the value of the orderHedListRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderHedListRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderHedListRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderHedListRow }
     * 
     * 
     */
    public List<OrderHedListRow> getOrderHedListRow() {
        if (orderHedListRow == null) {
            orderHedListRow = new ArrayList<OrderHedListRow>();
        }
        return this.orderHedListRow;
    }

    /**
     * Sets the value of the orderHedListRow property.
     * 
     * @param orderHedListRow
     *     allowed object is
     *     {@link OrderHedListRow }
     *     
     */
    public void setOrderHedListRow(List<OrderHedListRow> orderHedListRow) {
        this.orderHedListRow = orderHedListRow;
    }

}
