
package cpm.tesamm.SalesOrder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="iQuoteNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="iPoNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iCreditStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iCreditHold" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "iQuoteNum",
    "iPoNum",
    "iCreditStatus",
    "iCreditHold"
})
@XmlRootElement(name = "CreateOrderFromQuote")
public class CreateOrderFromQuote {

    protected Integer iQuoteNum;
    @XmlElementRef(name = "iPoNum", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> iPoNum;
    @XmlElementRef(name = "iCreditStatus", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> iCreditStatus;
    protected Boolean iCreditHold;

    /**
     * Gets the value of the iQuoteNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIQuoteNum() {
        return iQuoteNum;
    }

    /**
     * Sets the value of the iQuoteNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIQuoteNum(Integer value) {
        this.iQuoteNum = value;
    }

    /**
     * Gets the value of the iPoNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIPoNum() {
        return iPoNum;
    }

    /**
     * Sets the value of the iPoNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIPoNum(JAXBElement<String> value) {
        this.iPoNum = value;
    }

    /**
     * Gets the value of the iCreditStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getICreditStatus() {
        return iCreditStatus;
    }

    /**
     * Sets the value of the iCreditStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setICreditStatus(JAXBElement<String> value) {
        this.iCreditStatus = value;
    }

    /**
     * Gets the value of the iCreditHold property.
     * This getter has been renamed from isICreditHold() to getICreditHold() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getICreditHold() {
        return iCreditHold;
    }

    /**
     * Sets the value of the iCreditHold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setICreditHold(Boolean value) {
        this.iCreditHold = value;
    }

}
