
package cpm.tesamm.SalesOrder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderHistTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderHistTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderHistRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderHistRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderHistTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "orderHistRow"
})
public class OrderHistTable {

    @XmlElement(name = "OrderHistRow", nillable = true)
    protected List<OrderHistRow> orderHistRow;

    /**
     * Gets the value of the orderHistRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderHistRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderHistRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderHistRow }
     * 
     * 
     */
    public List<OrderHistRow> getOrderHistRow() {
        if (orderHistRow == null) {
            orderHistRow = new ArrayList<OrderHistRow>();
        }
        return this.orderHistRow;
    }

    /**
     * Sets the value of the orderHistRow property.
     * 
     * @param orderHistRow
     *     allowed object is
     *     {@link OrderHistRow }
     *     
     */
    public void setOrderHistRow(List<OrderHistRow> orderHistRow) {
        this.orderHistRow = orderHistRow;
    }

}
