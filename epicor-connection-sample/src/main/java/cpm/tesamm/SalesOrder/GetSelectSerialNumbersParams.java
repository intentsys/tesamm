
package cpm.tesamm.SalesOrder;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ipPartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ipWhseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ipBinNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ipQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ipTranType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ipRowID" type="{http://schemas.microsoft.com/2003/10/Serialization/}guid" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ipPartNum",
    "ipWhseCode",
    "ipBinNum",
    "ipQuantity",
    "ipTranType",
    "ipRowID"
})
@XmlRootElement(name = "GetSelectSerialNumbersParams")
public class GetSelectSerialNumbersParams {

    @XmlElementRef(name = "ipPartNum", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ipPartNum;
    @XmlElementRef(name = "ipWhseCode", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ipWhseCode;
    @XmlElementRef(name = "ipBinNum", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ipBinNum;
    protected BigDecimal ipQuantity;
    @XmlElementRef(name = "ipTranType", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ipTranType;
    protected String ipRowID;

    /**
     * Gets the value of the ipPartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIpPartNum() {
        return ipPartNum;
    }

    /**
     * Sets the value of the ipPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIpPartNum(JAXBElement<String> value) {
        this.ipPartNum = value;
    }

    /**
     * Gets the value of the ipWhseCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIpWhseCode() {
        return ipWhseCode;
    }

    /**
     * Sets the value of the ipWhseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIpWhseCode(JAXBElement<String> value) {
        this.ipWhseCode = value;
    }

    /**
     * Gets the value of the ipBinNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIpBinNum() {
        return ipBinNum;
    }

    /**
     * Sets the value of the ipBinNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIpBinNum(JAXBElement<String> value) {
        this.ipBinNum = value;
    }

    /**
     * Gets the value of the ipQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIpQuantity() {
        return ipQuantity;
    }

    /**
     * Sets the value of the ipQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIpQuantity(BigDecimal value) {
        this.ipQuantity = value;
    }

    /**
     * Gets the value of the ipTranType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIpTranType() {
        return ipTranType;
    }

    /**
     * Sets the value of the ipTranType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIpTranType(JAXBElement<String> value) {
        this.ipTranType = value;
    }

    /**
     * Gets the value of the ipRowID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIpRowID() {
        return ipRowID;
    }

    /**
     * Sets the value of the ipRowID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIpRowID(String value) {
        this.ipRowID = value;
    }

}
