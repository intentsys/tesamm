
package cpm.tesamm.SalesOrder;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for JobProdRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JobProdRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceRow">
 *       &lt;sequence>
 *         &lt;element name="AsmPartDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AsmPartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BitFlag" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CallLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CallLineLineDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CallNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DemandContractNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DemandDtlSeq" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DemandHeadSeq" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DemandLinkSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DemandLinkStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DemandScheduleSeq" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JHPartDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JHPartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobNumPartDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MakeToJobQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MakeToStockQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MakeToType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxAllowedQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MtlPartDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MtlPartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrdWIPQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OrderLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OrderLineLineDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OrderNumCardMemberName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderNumCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderRelNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OurStockQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PartIUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartPartDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartPricePerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartSalesUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartSellingFactor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PartTrackDimension" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartTrackLots" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartTrackSerialNum" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PlanID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanUserID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Plant" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProdQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PullFromStockWarehouseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PullFromStockWarehouseDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReceivedQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ShipBy" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ShippedQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SplitQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="StkWIPQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SysRevID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="TFLineNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TFOrdLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TFOrdNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TargetAssemblySeq" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TargetJobNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TargetMtlSeq" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TotalSplitQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TrackSerialNumbers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Valid" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="WIPQty" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="WarehouseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WarehouseCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JobProdRow", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "asmPartDesc",
    "asmPartNum",
    "bitFlag",
    "callLine",
    "callLineLineDesc",
    "callNum",
    "company",
    "custID",
    "custName",
    "demandContractNum",
    "demandDtlSeq",
    "demandHeadSeq",
    "demandLinkSource",
    "demandLinkStatus",
    "demandScheduleSeq",
    "ium",
    "jhPartDesc",
    "jhPartNum",
    "jobNum",
    "jobNumPartDescription",
    "makeToJobQty",
    "makeToStockQty",
    "makeToType",
    "maxAllowedQty",
    "mtlPartDesc",
    "mtlPartNum",
    "ordWIPQty",
    "orderLine",
    "orderLineLineDesc",
    "orderNum",
    "orderNumCardMemberName",
    "orderNumCurrencyCode",
    "orderRelNum",
    "ourStockQty",
    "partIUM",
    "partNum",
    "partPartDescription",
    "partPricePerCode",
    "partSalesUM",
    "partSellingFactor",
    "partTrackDimension",
    "partTrackLots",
    "partTrackSerialNum",
    "planID",
    "planUserID",
    "plant",
    "prodQty",
    "pullFromStockWarehouseCode",
    "pullFromStockWarehouseDesc",
    "receivedQty",
    "shipBy",
    "shippedQty",
    "splitQty",
    "stkWIPQty",
    "sysRevID",
    "tfLineNum",
    "tfOrdLine",
    "tfOrdNum",
    "targetAssemblySeq",
    "targetJobNum",
    "targetMtlSeq",
    "totalSplitQuantity",
    "trackSerialNumbers",
    "valid",
    "wipQty",
    "warehouseCode",
    "warehouseCodeDescription"
})
public class JobProdRow
    extends IceRow
{

    @XmlElementRef(name = "AsmPartDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> asmPartDesc;
    @XmlElementRef(name = "AsmPartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> asmPartNum;
    @XmlElement(name = "BitFlag")
    protected Integer bitFlag;
    @XmlElement(name = "CallLine")
    protected Integer callLine;
    @XmlElementRef(name = "CallLineLineDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callLineLineDesc;
    @XmlElement(name = "CallNum")
    protected Integer callNum;
    @XmlElementRef(name = "Company", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> company;
    @XmlElementRef(name = "CustID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> custID;
    @XmlElementRef(name = "CustName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> custName;
    @XmlElement(name = "DemandContractNum")
    protected Integer demandContractNum;
    @XmlElement(name = "DemandDtlSeq")
    protected Integer demandDtlSeq;
    @XmlElement(name = "DemandHeadSeq")
    protected Integer demandHeadSeq;
    @XmlElementRef(name = "DemandLinkSource", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> demandLinkSource;
    @XmlElementRef(name = "DemandLinkStatus", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> demandLinkStatus;
    @XmlElement(name = "DemandScheduleSeq")
    protected Integer demandScheduleSeq;
    @XmlElementRef(name = "IUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ium;
    @XmlElementRef(name = "JHPartDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> jhPartDesc;
    @XmlElementRef(name = "JHPartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> jhPartNum;
    @XmlElementRef(name = "JobNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> jobNum;
    @XmlElementRef(name = "JobNumPartDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> jobNumPartDescription;
    @XmlElement(name = "MakeToJobQty")
    protected BigDecimal makeToJobQty;
    @XmlElement(name = "MakeToStockQty")
    protected BigDecimal makeToStockQty;
    @XmlElementRef(name = "MakeToType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> makeToType;
    @XmlElement(name = "MaxAllowedQty")
    protected BigDecimal maxAllowedQty;
    @XmlElementRef(name = "MtlPartDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mtlPartDesc;
    @XmlElementRef(name = "MtlPartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mtlPartNum;
    @XmlElement(name = "OrdWIPQty")
    protected BigDecimal ordWIPQty;
    @XmlElement(name = "OrderLine")
    protected Integer orderLine;
    @XmlElementRef(name = "OrderLineLineDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderLineLineDesc;
    @XmlElement(name = "OrderNum")
    protected Integer orderNum;
    @XmlElementRef(name = "OrderNumCardMemberName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderNumCardMemberName;
    @XmlElementRef(name = "OrderNumCurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderNumCurrencyCode;
    @XmlElement(name = "OrderRelNum")
    protected Integer orderRelNum;
    @XmlElement(name = "OurStockQty")
    protected BigDecimal ourStockQty;
    @XmlElementRef(name = "PartIUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partIUM;
    @XmlElementRef(name = "PartNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partNum;
    @XmlElementRef(name = "PartPartDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partPartDescription;
    @XmlElementRef(name = "PartPricePerCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partPricePerCode;
    @XmlElementRef(name = "PartSalesUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partSalesUM;
    @XmlElement(name = "PartSellingFactor")
    protected BigDecimal partSellingFactor;
    @XmlElement(name = "PartTrackDimension")
    protected Boolean partTrackDimension;
    @XmlElement(name = "PartTrackLots")
    protected Boolean partTrackLots;
    @XmlElement(name = "PartTrackSerialNum")
    protected Boolean partTrackSerialNum;
    @XmlElementRef(name = "PlanID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planID;
    @XmlElementRef(name = "PlanUserID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planUserID;
    @XmlElementRef(name = "Plant", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> plant;
    @XmlElement(name = "ProdQty")
    protected BigDecimal prodQty;
    @XmlElementRef(name = "PullFromStockWarehouseCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pullFromStockWarehouseCode;
    @XmlElementRef(name = "PullFromStockWarehouseDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pullFromStockWarehouseDesc;
    @XmlElement(name = "ReceivedQty")
    protected BigDecimal receivedQty;
    @XmlElementRef(name = "ShipBy", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> shipBy;
    @XmlElement(name = "ShippedQty")
    protected BigDecimal shippedQty;
    @XmlElement(name = "SplitQty")
    protected BigDecimal splitQty;
    @XmlElement(name = "StkWIPQty")
    protected BigDecimal stkWIPQty;
    @XmlElement(name = "SysRevID")
    protected Long sysRevID;
    @XmlElementRef(name = "TFLineNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tfLineNum;
    @XmlElement(name = "TFOrdLine")
    protected Integer tfOrdLine;
    @XmlElementRef(name = "TFOrdNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tfOrdNum;
    @XmlElement(name = "TargetAssemblySeq")
    protected Integer targetAssemblySeq;
    @XmlElementRef(name = "TargetJobNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> targetJobNum;
    @XmlElement(name = "TargetMtlSeq")
    protected Integer targetMtlSeq;
    @XmlElement(name = "TotalSplitQuantity")
    protected BigDecimal totalSplitQuantity;
    @XmlElement(name = "TrackSerialNumbers")
    protected Boolean trackSerialNumbers;
    @XmlElement(name = "Valid")
    protected Boolean valid;
    @XmlElement(name = "WIPQty")
    protected BigDecimal wipQty;
    @XmlElementRef(name = "WarehouseCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> warehouseCode;
    @XmlElementRef(name = "WarehouseCodeDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> warehouseCodeDescription;

    /**
     * Gets the value of the asmPartDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAsmPartDesc() {
        return asmPartDesc;
    }

    /**
     * Sets the value of the asmPartDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAsmPartDesc(JAXBElement<String> value) {
        this.asmPartDesc = value;
    }

    /**
     * Gets the value of the asmPartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAsmPartNum() {
        return asmPartNum;
    }

    /**
     * Sets the value of the asmPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAsmPartNum(JAXBElement<String> value) {
        this.asmPartNum = value;
    }

    /**
     * Gets the value of the bitFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBitFlag() {
        return bitFlag;
    }

    /**
     * Sets the value of the bitFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBitFlag(Integer value) {
        this.bitFlag = value;
    }

    /**
     * Gets the value of the callLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCallLine() {
        return callLine;
    }

    /**
     * Sets the value of the callLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCallLine(Integer value) {
        this.callLine = value;
    }

    /**
     * Gets the value of the callLineLineDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallLineLineDesc() {
        return callLineLineDesc;
    }

    /**
     * Sets the value of the callLineLineDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallLineLineDesc(JAXBElement<String> value) {
        this.callLineLineDesc = value;
    }

    /**
     * Gets the value of the callNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCallNum() {
        return callNum;
    }

    /**
     * Sets the value of the callNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCallNum(Integer value) {
        this.callNum = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompany(JAXBElement<String> value) {
        this.company = value;
    }

    /**
     * Gets the value of the custID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustID() {
        return custID;
    }

    /**
     * Sets the value of the custID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustID(JAXBElement<String> value) {
        this.custID = value;
    }

    /**
     * Gets the value of the custName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustName() {
        return custName;
    }

    /**
     * Sets the value of the custName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustName(JAXBElement<String> value) {
        this.custName = value;
    }

    /**
     * Gets the value of the demandContractNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDemandContractNum() {
        return demandContractNum;
    }

    /**
     * Sets the value of the demandContractNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDemandContractNum(Integer value) {
        this.demandContractNum = value;
    }

    /**
     * Gets the value of the demandDtlSeq property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDemandDtlSeq() {
        return demandDtlSeq;
    }

    /**
     * Sets the value of the demandDtlSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDemandDtlSeq(Integer value) {
        this.demandDtlSeq = value;
    }

    /**
     * Gets the value of the demandHeadSeq property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDemandHeadSeq() {
        return demandHeadSeq;
    }

    /**
     * Sets the value of the demandHeadSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDemandHeadSeq(Integer value) {
        this.demandHeadSeq = value;
    }

    /**
     * Gets the value of the demandLinkSource property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDemandLinkSource() {
        return demandLinkSource;
    }

    /**
     * Sets the value of the demandLinkSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDemandLinkSource(JAXBElement<String> value) {
        this.demandLinkSource = value;
    }

    /**
     * Gets the value of the demandLinkStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDemandLinkStatus() {
        return demandLinkStatus;
    }

    /**
     * Sets the value of the demandLinkStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDemandLinkStatus(JAXBElement<String> value) {
        this.demandLinkStatus = value;
    }

    /**
     * Gets the value of the demandScheduleSeq property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDemandScheduleSeq() {
        return demandScheduleSeq;
    }

    /**
     * Sets the value of the demandScheduleSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDemandScheduleSeq(Integer value) {
        this.demandScheduleSeq = value;
    }

    /**
     * Gets the value of the ium property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIUM() {
        return ium;
    }

    /**
     * Sets the value of the ium property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIUM(JAXBElement<String> value) {
        this.ium = value;
    }

    /**
     * Gets the value of the jhPartDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getJHPartDesc() {
        return jhPartDesc;
    }

    /**
     * Sets the value of the jhPartDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setJHPartDesc(JAXBElement<String> value) {
        this.jhPartDesc = value;
    }

    /**
     * Gets the value of the jhPartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getJHPartNum() {
        return jhPartNum;
    }

    /**
     * Sets the value of the jhPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setJHPartNum(JAXBElement<String> value) {
        this.jhPartNum = value;
    }

    /**
     * Gets the value of the jobNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getJobNum() {
        return jobNum;
    }

    /**
     * Sets the value of the jobNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setJobNum(JAXBElement<String> value) {
        this.jobNum = value;
    }

    /**
     * Gets the value of the jobNumPartDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getJobNumPartDescription() {
        return jobNumPartDescription;
    }

    /**
     * Sets the value of the jobNumPartDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setJobNumPartDescription(JAXBElement<String> value) {
        this.jobNumPartDescription = value;
    }

    /**
     * Gets the value of the makeToJobQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMakeToJobQty() {
        return makeToJobQty;
    }

    /**
     * Sets the value of the makeToJobQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMakeToJobQty(BigDecimal value) {
        this.makeToJobQty = value;
    }

    /**
     * Gets the value of the makeToStockQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMakeToStockQty() {
        return makeToStockQty;
    }

    /**
     * Sets the value of the makeToStockQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMakeToStockQty(BigDecimal value) {
        this.makeToStockQty = value;
    }

    /**
     * Gets the value of the makeToType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMakeToType() {
        return makeToType;
    }

    /**
     * Sets the value of the makeToType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMakeToType(JAXBElement<String> value) {
        this.makeToType = value;
    }

    /**
     * Gets the value of the maxAllowedQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxAllowedQty() {
        return maxAllowedQty;
    }

    /**
     * Sets the value of the maxAllowedQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxAllowedQty(BigDecimal value) {
        this.maxAllowedQty = value;
    }

    /**
     * Gets the value of the mtlPartDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMtlPartDesc() {
        return mtlPartDesc;
    }

    /**
     * Sets the value of the mtlPartDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMtlPartDesc(JAXBElement<String> value) {
        this.mtlPartDesc = value;
    }

    /**
     * Gets the value of the mtlPartNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMtlPartNum() {
        return mtlPartNum;
    }

    /**
     * Sets the value of the mtlPartNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMtlPartNum(JAXBElement<String> value) {
        this.mtlPartNum = value;
    }

    /**
     * Gets the value of the ordWIPQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrdWIPQty() {
        return ordWIPQty;
    }

    /**
     * Sets the value of the ordWIPQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrdWIPQty(BigDecimal value) {
        this.ordWIPQty = value;
    }

    /**
     * Gets the value of the orderLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderLine() {
        return orderLine;
    }

    /**
     * Sets the value of the orderLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderLine(Integer value) {
        this.orderLine = value;
    }

    /**
     * Gets the value of the orderLineLineDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderLineLineDesc() {
        return orderLineLineDesc;
    }

    /**
     * Sets the value of the orderLineLineDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderLineLineDesc(JAXBElement<String> value) {
        this.orderLineLineDesc = value;
    }

    /**
     * Gets the value of the orderNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * Sets the value of the orderNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNum(Integer value) {
        this.orderNum = value;
    }

    /**
     * Gets the value of the orderNumCardMemberName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderNumCardMemberName() {
        return orderNumCardMemberName;
    }

    /**
     * Sets the value of the orderNumCardMemberName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderNumCardMemberName(JAXBElement<String> value) {
        this.orderNumCardMemberName = value;
    }

    /**
     * Gets the value of the orderNumCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderNumCurrencyCode() {
        return orderNumCurrencyCode;
    }

    /**
     * Sets the value of the orderNumCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderNumCurrencyCode(JAXBElement<String> value) {
        this.orderNumCurrencyCode = value;
    }

    /**
     * Gets the value of the orderRelNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderRelNum() {
        return orderRelNum;
    }

    /**
     * Sets the value of the orderRelNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderRelNum(Integer value) {
        this.orderRelNum = value;
    }

    /**
     * Gets the value of the ourStockQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOurStockQty() {
        return ourStockQty;
    }

    /**
     * Sets the value of the ourStockQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOurStockQty(BigDecimal value) {
        this.ourStockQty = value;
    }

    /**
     * Gets the value of the partIUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartIUM() {
        return partIUM;
    }

    /**
     * Sets the value of the partIUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartIUM(JAXBElement<String> value) {
        this.partIUM = value;
    }

    /**
     * Gets the value of the partNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartNum() {
        return partNum;
    }

    /**
     * Sets the value of the partNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartNum(JAXBElement<String> value) {
        this.partNum = value;
    }

    /**
     * Gets the value of the partPartDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartPartDescription() {
        return partPartDescription;
    }

    /**
     * Sets the value of the partPartDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartPartDescription(JAXBElement<String> value) {
        this.partPartDescription = value;
    }

    /**
     * Gets the value of the partPricePerCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartPricePerCode() {
        return partPricePerCode;
    }

    /**
     * Sets the value of the partPricePerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartPricePerCode(JAXBElement<String> value) {
        this.partPricePerCode = value;
    }

    /**
     * Gets the value of the partSalesUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartSalesUM() {
        return partSalesUM;
    }

    /**
     * Sets the value of the partSalesUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartSalesUM(JAXBElement<String> value) {
        this.partSalesUM = value;
    }

    /**
     * Gets the value of the partSellingFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPartSellingFactor() {
        return partSellingFactor;
    }

    /**
     * Sets the value of the partSellingFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPartSellingFactor(BigDecimal value) {
        this.partSellingFactor = value;
    }

    /**
     * Gets the value of the partTrackDimension property.
     * This getter has been renamed from isPartTrackDimension() to getPartTrackDimension() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartTrackDimension() {
        return partTrackDimension;
    }

    /**
     * Sets the value of the partTrackDimension property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartTrackDimension(Boolean value) {
        this.partTrackDimension = value;
    }

    /**
     * Gets the value of the partTrackLots property.
     * This getter has been renamed from isPartTrackLots() to getPartTrackLots() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartTrackLots() {
        return partTrackLots;
    }

    /**
     * Sets the value of the partTrackLots property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartTrackLots(Boolean value) {
        this.partTrackLots = value;
    }

    /**
     * Gets the value of the partTrackSerialNum property.
     * This getter has been renamed from isPartTrackSerialNum() to getPartTrackSerialNum() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPartTrackSerialNum() {
        return partTrackSerialNum;
    }

    /**
     * Sets the value of the partTrackSerialNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartTrackSerialNum(Boolean value) {
        this.partTrackSerialNum = value;
    }

    /**
     * Gets the value of the planID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanID() {
        return planID;
    }

    /**
     * Sets the value of the planID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanID(JAXBElement<String> value) {
        this.planID = value;
    }

    /**
     * Gets the value of the planUserID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanUserID() {
        return planUserID;
    }

    /**
     * Sets the value of the planUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanUserID(JAXBElement<String> value) {
        this.planUserID = value;
    }

    /**
     * Gets the value of the plant property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlant() {
        return plant;
    }

    /**
     * Sets the value of the plant property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlant(JAXBElement<String> value) {
        this.plant = value;
    }

    /**
     * Gets the value of the prodQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProdQty() {
        return prodQty;
    }

    /**
     * Sets the value of the prodQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProdQty(BigDecimal value) {
        this.prodQty = value;
    }

    /**
     * Gets the value of the pullFromStockWarehouseCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPullFromStockWarehouseCode() {
        return pullFromStockWarehouseCode;
    }

    /**
     * Sets the value of the pullFromStockWarehouseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPullFromStockWarehouseCode(JAXBElement<String> value) {
        this.pullFromStockWarehouseCode = value;
    }

    /**
     * Gets the value of the pullFromStockWarehouseDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPullFromStockWarehouseDesc() {
        return pullFromStockWarehouseDesc;
    }

    /**
     * Sets the value of the pullFromStockWarehouseDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPullFromStockWarehouseDesc(JAXBElement<String> value) {
        this.pullFromStockWarehouseDesc = value;
    }

    /**
     * Gets the value of the receivedQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReceivedQty() {
        return receivedQty;
    }

    /**
     * Sets the value of the receivedQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReceivedQty(BigDecimal value) {
        this.receivedQty = value;
    }

    /**
     * Gets the value of the shipBy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getShipBy() {
        return shipBy;
    }

    /**
     * Sets the value of the shipBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setShipBy(JAXBElement<XMLGregorianCalendar> value) {
        this.shipBy = value;
    }

    /**
     * Gets the value of the shippedQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShippedQty() {
        return shippedQty;
    }

    /**
     * Sets the value of the shippedQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShippedQty(BigDecimal value) {
        this.shippedQty = value;
    }

    /**
     * Gets the value of the splitQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSplitQty() {
        return splitQty;
    }

    /**
     * Sets the value of the splitQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSplitQty(BigDecimal value) {
        this.splitQty = value;
    }

    /**
     * Gets the value of the stkWIPQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStkWIPQty() {
        return stkWIPQty;
    }

    /**
     * Sets the value of the stkWIPQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStkWIPQty(BigDecimal value) {
        this.stkWIPQty = value;
    }

    /**
     * Gets the value of the sysRevID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSysRevID() {
        return sysRevID;
    }

    /**
     * Sets the value of the sysRevID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSysRevID(Long value) {
        this.sysRevID = value;
    }

    /**
     * Gets the value of the tfLineNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTFLineNum() {
        return tfLineNum;
    }

    /**
     * Sets the value of the tfLineNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTFLineNum(JAXBElement<String> value) {
        this.tfLineNum = value;
    }

    /**
     * Gets the value of the tfOrdLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTFOrdLine() {
        return tfOrdLine;
    }

    /**
     * Sets the value of the tfOrdLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTFOrdLine(Integer value) {
        this.tfOrdLine = value;
    }

    /**
     * Gets the value of the tfOrdNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTFOrdNum() {
        return tfOrdNum;
    }

    /**
     * Sets the value of the tfOrdNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTFOrdNum(JAXBElement<String> value) {
        this.tfOrdNum = value;
    }

    /**
     * Gets the value of the targetAssemblySeq property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTargetAssemblySeq() {
        return targetAssemblySeq;
    }

    /**
     * Sets the value of the targetAssemblySeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTargetAssemblySeq(Integer value) {
        this.targetAssemblySeq = value;
    }

    /**
     * Gets the value of the targetJobNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTargetJobNum() {
        return targetJobNum;
    }

    /**
     * Sets the value of the targetJobNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTargetJobNum(JAXBElement<String> value) {
        this.targetJobNum = value;
    }

    /**
     * Gets the value of the targetMtlSeq property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTargetMtlSeq() {
        return targetMtlSeq;
    }

    /**
     * Sets the value of the targetMtlSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTargetMtlSeq(Integer value) {
        this.targetMtlSeq = value;
    }

    /**
     * Gets the value of the totalSplitQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalSplitQuantity() {
        return totalSplitQuantity;
    }

    /**
     * Sets the value of the totalSplitQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalSplitQuantity(BigDecimal value) {
        this.totalSplitQuantity = value;
    }

    /**
     * Gets the value of the trackSerialNumbers property.
     * This getter has been renamed from isTrackSerialNumbers() to getTrackSerialNumbers() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getTrackSerialNumbers() {
        return trackSerialNumbers;
    }

    /**
     * Sets the value of the trackSerialNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTrackSerialNumbers(Boolean value) {
        this.trackSerialNumbers = value;
    }

    /**
     * Gets the value of the valid property.
     * This getter has been renamed from isValid() to getValid() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getValid() {
        return valid;
    }

    /**
     * Sets the value of the valid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setValid(Boolean value) {
        this.valid = value;
    }

    /**
     * Gets the value of the wipQty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWIPQty() {
        return wipQty;
    }

    /**
     * Sets the value of the wipQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWIPQty(BigDecimal value) {
        this.wipQty = value;
    }

    /**
     * Gets the value of the warehouseCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWarehouseCode() {
        return warehouseCode;
    }

    /**
     * Sets the value of the warehouseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWarehouseCode(JAXBElement<String> value) {
        this.warehouseCode = value;
    }

    /**
     * Gets the value of the warehouseCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWarehouseCodeDescription() {
        return warehouseCodeDescription;
    }

    /**
     * Sets the value of the warehouseCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWarehouseCodeDescription(JAXBElement<String> value) {
        this.warehouseCodeDescription = value;
    }

}
