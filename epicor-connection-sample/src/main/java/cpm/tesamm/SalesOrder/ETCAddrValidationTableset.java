
package cpm.tesamm.SalesOrder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ETCAddrValidationTableset complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ETCAddrValidationTableset">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceTableset">
 *       &lt;sequence>
 *         &lt;element name="ETCAddress" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}ETCAddressTable" minOccurs="0"/>
 *         &lt;element name="ETCMessage" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}ETCMessageTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ETCAddrValidationTableset", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "etcAddress",
    "etcMessage"
})
public class ETCAddrValidationTableset
    extends IceTableset
{

    @XmlElementRef(name = "ETCAddress", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<ETCAddressTable> etcAddress;
    @XmlElementRef(name = "ETCMessage", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<ETCMessageTable> etcMessage;

    /**
     * Gets the value of the etcAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ETCAddressTable }{@code >}
     *     
     */
    public JAXBElement<ETCAddressTable> getETCAddress() {
        return etcAddress;
    }

    /**
     * Sets the value of the etcAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ETCAddressTable }{@code >}
     *     
     */
    public void setETCAddress(JAXBElement<ETCAddressTable> value) {
        this.etcAddress = value;
    }

    /**
     * Gets the value of the etcMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ETCMessageTable }{@code >}
     *     
     */
    public JAXBElement<ETCMessageTable> getETCMessage() {
        return etcMessage;
    }

    /**
     * Sets the value of the etcMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ETCMessageTable }{@code >}
     *     
     */
    public void setETCMessage(JAXBElement<ETCMessageTable> value) {
        this.etcMessage = value;
    }

}
