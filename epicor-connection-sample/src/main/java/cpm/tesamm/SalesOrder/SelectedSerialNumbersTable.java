
package cpm.tesamm.SalesOrder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SelectedSerialNumbersTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SelectedSerialNumbersTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SelectedSerialNumbersRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SelectedSerialNumbersRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SelectedSerialNumbersTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "selectedSerialNumbersRow"
})
public class SelectedSerialNumbersTable {

    @XmlElement(name = "SelectedSerialNumbersRow", nillable = true)
    protected List<SelectedSerialNumbersRow> selectedSerialNumbersRow;

    /**
     * Gets the value of the selectedSerialNumbersRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectedSerialNumbersRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectedSerialNumbersRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SelectedSerialNumbersRow }
     * 
     * 
     */
    public List<SelectedSerialNumbersRow> getSelectedSerialNumbersRow() {
        if (selectedSerialNumbersRow == null) {
            selectedSerialNumbersRow = new ArrayList<SelectedSerialNumbersRow>();
        }
        return this.selectedSerialNumbersRow;
    }

    /**
     * Sets the value of the selectedSerialNumbersRow property.
     * 
     * @param selectedSerialNumbersRow
     *     allowed object is
     *     {@link SelectedSerialNumbersRow }
     *     
     */
    public void setSelectedSerialNumbersRow(List<SelectedSerialNumbersRow> selectedSerialNumbersRow) {
        this.selectedSerialNumbersRow = selectedSerialNumbersRow;
    }

}
