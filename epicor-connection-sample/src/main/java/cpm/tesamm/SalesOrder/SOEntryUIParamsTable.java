
package cpm.tesamm.SalesOrder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOEntryUIParamsTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOEntryUIParamsTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SOEntryUIParamsRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SOEntryUIParamsRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOEntryUIParamsTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "soEntryUIParamsRow"
})
public class SOEntryUIParamsTable {

    @XmlElement(name = "SOEntryUIParamsRow", nillable = true)
    protected List<SOEntryUIParamsRow> soEntryUIParamsRow;

    /**
     * Gets the value of the soEntryUIParamsRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the soEntryUIParamsRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSOEntryUIParamsRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SOEntryUIParamsRow }
     * 
     * 
     */
    public List<SOEntryUIParamsRow> getSOEntryUIParamsRow() {
        if (soEntryUIParamsRow == null) {
            soEntryUIParamsRow = new ArrayList<SOEntryUIParamsRow>();
        }
        return this.soEntryUIParamsRow;
    }

    /**
     * Sets the value of the soEntryUIParamsRow property.
     * 
     * @param soEntryUIParamsRow
     *     allowed object is
     *     {@link SOEntryUIParamsRow }
     *     
     */
    public void setSOEntryUIParamsRow(List<SOEntryUIParamsRow> soEntryUIParamsRow) {
        this.soEntryUIParamsRow = soEntryUIParamsRow;
    }

}
