
package cpm.tesamm.SalesOrder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EpicorFaultDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EpicorFaultDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataList" type="{http://schemas.datacontract.org/2004/07/Ice.Common}ArrayOfEpicorExceptionData" minOccurs="0"/>
 *         &lt;element name="ExceptionKindValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="m_exKindQualifiedName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="m_exceptionQualifiedName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EpicorFaultDetail", namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", propOrder = {
    "dataList",
    "exceptionKindValue",
    "message",
    "mExKindQualifiedName",
    "mExceptionQualifiedName"
})
public class EpicorFaultDetail {

    @XmlElementRef(name = "DataList", namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfEpicorExceptionData> dataList;
    @XmlElementRef(name = "ExceptionKindValue", namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", type = JAXBElement.class, required = false)
    protected JAXBElement<String> exceptionKindValue;
    @XmlElementRef(name = "Message", namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", type = JAXBElement.class, required = false)
    protected JAXBElement<String> message;
    @XmlElementRef(name = "m_exKindQualifiedName", namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mExKindQualifiedName;
    @XmlElementRef(name = "m_exceptionQualifiedName", namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mExceptionQualifiedName;

    /**
     * Gets the value of the dataList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfEpicorExceptionData }{@code >}
     *     
     */
    public JAXBElement<ArrayOfEpicorExceptionData> getDataList() {
        return dataList;
    }

    /**
     * Sets the value of the dataList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfEpicorExceptionData }{@code >}
     *     
     */
    public void setDataList(JAXBElement<ArrayOfEpicorExceptionData> value) {
        this.dataList = value;
    }

    /**
     * Gets the value of the exceptionKindValue property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExceptionKindValue() {
        return exceptionKindValue;
    }

    /**
     * Sets the value of the exceptionKindValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExceptionKindValue(JAXBElement<String> value) {
        this.exceptionKindValue = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMessage(JAXBElement<String> value) {
        this.message = value;
    }

    /**
     * Gets the value of the mExKindQualifiedName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMExKindQualifiedName() {
        return mExKindQualifiedName;
    }

    /**
     * Sets the value of the mExKindQualifiedName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMExKindQualifiedName(JAXBElement<String> value) {
        this.mExKindQualifiedName = value;
    }

    /**
     * Gets the value of the mExceptionQualifiedName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMExceptionQualifiedName() {
        return mExceptionQualifiedName;
    }

    /**
     * Sets the value of the mExceptionQualifiedName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMExceptionQualifiedName(JAXBElement<String> value) {
        this.mExceptionQualifiedName = value;
    }

}
