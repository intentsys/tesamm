
package cpm.tesamm.SalesOrder;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dRepRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="iRepSplit" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cRepName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dRepRate",
    "iRepSplit",
    "cRepName"
})
@XmlRootElement(name = "ChangeSalesRepResponse")
public class ChangeSalesRepResponse {

    protected BigDecimal dRepRate;
    protected Integer iRepSplit;
    @XmlElementRef(name = "cRepName", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cRepName;

    /**
     * Gets the value of the dRepRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDRepRate() {
        return dRepRate;
    }

    /**
     * Sets the value of the dRepRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDRepRate(BigDecimal value) {
        this.dRepRate = value;
    }

    /**
     * Gets the value of the iRepSplit property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIRepSplit() {
        return iRepSplit;
    }

    /**
     * Sets the value of the iRepSplit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIRepSplit(Integer value) {
        this.iRepSplit = value;
    }

    /**
     * Gets the value of the cRepName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCRepName() {
        return cRepName;
    }

    /**
     * Sets the value of the cRepName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCRepName(JAXBElement<String> value) {
        this.cRepName = value;
    }

}
