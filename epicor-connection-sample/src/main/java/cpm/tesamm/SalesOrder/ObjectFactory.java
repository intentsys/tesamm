
package cpm.tesamm.SalesOrder;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cpm.tesamm.SalesOrder package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SaveOTSParamsTableset_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SaveOTSParamsTableset");
    private final static QName _QuoteQtyRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "QuoteQtyRow");
    private final static QName _OrderCustTrkTableset_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderCustTrkTableset");
    private final static QName _SalesOrderTableset_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesOrderTableset");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _BOUpdErrorRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice", "BOUpdErrorRow");
    private final static QName _SelectedSerialNumbersTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SelectedSerialNumbersTable");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _OrderMscTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderMscTable");
    private final static QName _OrderDtlAttchTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderDtlAttchTable");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _BOUpdErrorTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice", "BOUpdErrorTable");
    private final static QName _OrderMscRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderMscRow");
    private final static QName _SOEntryUIParamsTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SOEntryUIParamsTable");
    private final static QName _PartSubsRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PartSubsRow");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _SelectSerialNumbersParamsTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SelectSerialNumbersParamsTable");
    private final static QName _OrderHedTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedTable");
    private final static QName _SNFormatRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SNFormatRow");
    private final static QName _OrderHedAttchTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedAttchTable");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _OrderCustTrkRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderCustTrkRow");
    private final static QName _ArrayOfKeyValueOfstringstring_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfKeyValueOfstringstring");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _IceTableset_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice", "IceTableset");
    private final static QName _OrderRepCommTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderRepCommTable");
    private final static QName _GlbSugPOChgTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "GlbSugPOChgTable");
    private final static QName _SelectSerialNumbersParamsRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SelectSerialNumbersParamsRow");
    private final static QName _OrderHedUPSTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedUPSTable");
    private final static QName _OrderHistTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHistTable");
    private final static QName _OrderHedRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedRow");
    private final static QName _QuoteQtyTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "QuoteQtyTable");
    private final static QName _SaveOTSParamsRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SaveOTSParamsRow");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _OHOrderMscRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OHOrderMscRow");
    private final static QName _OrderDtlRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderDtlRow");
    private final static QName _ETCAddressTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ETCAddressTable");
    private final static QName _IceRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice", "IceRow");
    private final static QName _HedTaxSumTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "HedTaxSumTable");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _ETCAddressRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ETCAddressRow");
    private final static QName _SNFormatTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SNFormatTable");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _TaxConnectStatusTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TaxConnectStatusTable");
    private final static QName _OrderRepCommRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderRepCommRow");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _UpdExtSalesOrderTableset_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "UpdExtSalesOrderTableset");
    private final static QName _TaxConnectStatusRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TaxConnectStatusRow");
    private final static QName _SOEntryUIParamsRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SOEntryUIParamsRow");
    private final static QName _OrderHedAttchRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedAttchRow");
    private final static QName _GlbSugPOChgRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "GlbSugPOChgRow");
    private final static QName _OrderDtlAttchRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderDtlAttchRow");
    private final static QName _OrderHedListTableset_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedListTableset");
    private final static QName _ArrayOfstring_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _SelectSerialNumbersParamsTableset_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SelectSerialNumbersParamsTableset");
    private final static QName _ArrayOfguid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfguid");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _GlbSugPOChgTableset_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "GlbSugPOChgTableset");
    private final static QName _JobProdTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "JobProdTable");
    private final static QName _OHOrderMscTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OHOrderMscTable");
    private final static QName _HedTaxSumRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "HedTaxSumRow");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _OrderDtlTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderDtlTable");
    private final static QName _SelectedSerialNumbersRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SelectedSerialNumbersRow");
    private final static QName _PartSubsTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PartSubsTable");
    private final static QName _OrdDtlQuoteQtyTableset_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrdDtlQuoteQtyTableset");
    private final static QName _ETCMessageTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ETCMessageTable");
    private final static QName _UserDefinedColumns_QNAME = new QName("http://epicor.com/UserDefinedColumns", "UserDefinedColumns");
    private final static QName _OrderCustTrkTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderCustTrkTable");
    private final static QName _OrdRelJobProdTableset_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrdRelJobProdTableset");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _OrderRelRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderRelRow");
    private final static QName _ArrayOfEpicorExceptionData_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice.Common", "ArrayOfEpicorExceptionData");
    private final static QName _EpicorExceptionData_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice.Common", "EpicorExceptionData");
    private final static QName _OrderHistRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHistRow");
    private final static QName _TempRowBase_QNAME = new QName("http://schemas.datacontract.org/2004/07/Epicor.Data", "TempRowBase");
    private final static QName _ETCAddrValidationTableset_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ETCAddrValidationTableset");
    private final static QName _SOEntryUIParamsTableset_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SOEntryUIParamsTableset");
    private final static QName _BOUpdErrorTableset_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice", "BOUpdErrorTableset");
    private final static QName _OrderRelTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderRelTable");
    private final static QName _ETCMessageRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ETCMessageRow");
    private final static QName _OrderHedUPSRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedUPSRow");
    private final static QName _OrderHedListRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedListRow");
    private final static QName _JobProdRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "JobProdRow");
    private final static QName _OrderRelTaxTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderRelTaxTable");
    private final static QName _OrderRelTaxRow_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderRelTaxRow");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _OrderHedListTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedListTable");
    private final static QName _SaveOTSParamsTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SaveOTSParamsTable");
    private final static QName _EpicorFaultDetail_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice.Common", "EpicorFaultDetail");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _ChkLtrOfCrdtCARLOCID_QNAME = new QName("Erp:BO:SalesOrder", "cARLOCID");
    private final static QName _SetUPSQVEnableResponseDs_QNAME = new QName("Erp:BO:SalesOrder", "ds");
    private final static QName _OrderDtlRowMaterialMod_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MaterialMod");
    private final static QName _OrderDtlRowKitFlagDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "KitFlagDescription");
    private final static QName _OrderDtlRowPartNumPricePerCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PartNumPricePerCode");
    private final static QName _OrderDtlRowJobTypeMFG_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "JobTypeMFG");
    private final static QName _OrderDtlRowLineStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "LineStatus");
    private final static QName _OrderDtlRowRevisionNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RevisionNum");
    private final static QName _OrderDtlRowAvailPriceLists_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "AvailPriceLists");
    private final static QName _OrderDtlRowCompany_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Company");
    private final static QName _OrderDtlRowBinNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BinNum");
    private final static QName _OrderDtlRowPriceListCodeDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PriceListCodeDesc");
    private final static QName _OrderDtlRowMktgCampaignID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MktgCampaignID");
    private final static QName _OrderDtlRowPartNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PartNum");
    private final static QName _OrderDtlRowMktgCampaignIDCampDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MktgCampaignIDCampDescription");
    private final static QName _OrderDtlRowOrigWhyNoTax_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrigWhyNoTax");
    private final static QName _OrderDtlRowBaseCurrSymbol_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BaseCurrSymbol");
    private final static QName _OrderDtlRowPOLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "POLine");
    private final static QName _OrderDtlRowProjectIDDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ProjectIDDescription");
    private final static QName _OrderDtlRowProFormaInvComment_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ProFormaInvComment");
    private final static QName _OrderDtlRowLotNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "LotNum");
    private final static QName _OrderDtlRowCumeDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CumeDate");
    private final static QName _OrderDtlRowOrderNumCardMemberName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderNumCardMemberName");
    private final static QName _OrderDtlRowIUM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "IUM");
    private final static QName _OrderDtlRowPOLineRef_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "POLineRef");
    private final static QName _OrderDtlRowKitCompOrigPart_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "KitCompOrigPart");
    private final static QName _OrderDtlRowEntryProcess_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "EntryProcess");
    private final static QName _OrderDtlRowLastConfigUserID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "LastConfigUserID");
    private final static QName _OrderDtlRowTaxCatIDDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TaxCatIDDescription");
    private final static QName _OrderDtlRowDoNotShipAfterDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DoNotShipAfterDate");
    private final static QName _OrderDtlRowPriceListCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PriceListCode");
    private final static QName _OrderDtlRowKitOrderQtyUOM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "KitOrderQtyUOM");
    private final static QName _OrderDtlRowCounterSaleLotNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CounterSaleLotNum");
    private final static QName _OrderDtlRowProjectID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ProjectID");
    private final static QName _OrderDtlRowSalesUM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesUM");
    private final static QName _OrderDtlRowDiscBreakListCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DiscBreakListCode");
    private final static QName _OrderDtlRowDoNotShipBeforeDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DoNotShipBeforeDate");
    private final static QName _OrderDtlRowQuoteNumCurrencyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "QuoteNumCurrencyCode");
    private final static QName _OrderDtlRowDiscBreakListCodeEndDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DiscBreakListCodeEndDate");
    private final static QName _OrderDtlRowMktgEvntEvntDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MktgEvntEvntDescription");
    private final static QName _OrderDtlRowDUM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DUM");
    private final static QName _OrderDtlRowLineType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "LineType");
    private final static QName _OrderDtlRowECCQuoteNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ECCQuoteNum");
    private final static QName _OrderDtlRowWarehouseDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "WarehouseDesc");
    private final static QName _OrderDtlRowCounterSaleDimCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CounterSaleDimCode");
    private final static QName _OrderDtlRowProdCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ProdCode");
    private final static QName _OrderDtlRowECCOrderNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ECCOrderNum");
    private final static QName _OrderDtlRowCurrencyID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CurrencyID");
    private final static QName _OrderDtlRowCounterSaleBinNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CounterSaleBinNum");
    private final static QName _OrderDtlRowCustNumCustID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CustNumCustID");
    private final static QName _OrderDtlRowMOMsourceType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MOMsourceType");
    private final static QName _OrderDtlRowECCPlant_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ECCPlant");
    private final static QName _OrderDtlRowExtCompany_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ExtCompany");
    private final static QName _OrderDtlRowSalesRepName3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesRepName3");
    private final static QName _OrderDtlRowCreditLimitMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CreditLimitMessage");
    private final static QName _OrderDtlRowPickListComment_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PickListComment");
    private final static QName _OrderDtlRowSalesRepName2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesRepName2");
    private final static QName _OrderDtlRowSalesRepName1_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesRepName1");
    private final static QName _OrderDtlRowDspJobType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DspJobType");
    private final static QName _OrderDtlRowSalesRepName5_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesRepName5");
    private final static QName _OrderDtlRowSalesRepName4_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesRepName4");
    private final static QName _OrderDtlRowProdCodeDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ProdCodeDescription");
    private final static QName _OrderDtlRowShipComment_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipComment");
    private final static QName _OrderDtlRowWarehouseCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "WarehouseCode");
    private final static QName _OrderDtlRowBaseCurrencyID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BaseCurrencyID");
    private final static QName _OrderDtlRowDimCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DimCode");
    private final static QName _OrderDtlRowWarrantyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "WarrantyCode");
    private final static QName _OrderDtlRowInvtyUOM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "InvtyUOM");
    private final static QName _OrderDtlRowCustNumName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CustNumName");
    private final static QName _OrderDtlRowCurrencyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CurrencyCode");
    private final static QName _OrderDtlRowContractCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ContractCode");
    private final static QName _OrderDtlRowPriceGroupCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PriceGroupCode");
    private final static QName _OrderDtlRowXRevisionNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "XRevisionNum");
    private final static QName _OrderDtlRowChangeDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ChangeDate");
    private final static QName _OrderDtlRowDefaultOversPricing_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DefaultOversPricing");
    private final static QName _OrderDtlRowPrevPartNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PrevPartNum");
    private final static QName _OrderDtlRowConfigured_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Configured");
    private final static QName _OrderDtlRowOrderComment_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderComment");
    private final static QName _OrderDtlRowSellingFactorDirection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SellingFactorDirection");
    private final static QName _OrderDtlRowLastConfigDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "LastConfigDate");
    private final static QName _OrderDtlRowChangedBy_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ChangedBy");
    private final static QName _OrderDtlRowBreakListCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BreakListCode");
    private final static QName _OrderDtlRowPricePerCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PricePerCode");
    private final static QName _OrderDtlRowMOMsourceEst_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MOMsourceEst");
    private final static QName _OrderDtlRowKitPricing_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "KitPricing");
    private final static QName _OrderDtlRowWarrantyCodeWarrDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "WarrantyCodeWarrDescription");
    private final static QName _OrderDtlRowBasePartNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BasePartNum");
    private final static QName _OrderDtlRowOldProdCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OldProdCode");
    private final static QName _OrderDtlRowCounterSaleWarehouse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CounterSaleWarehouse");
    private final static QName _OrderDtlRowPartNumSalesUM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PartNumSalesUM");
    private final static QName _OrderDtlRowSmartString_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SmartString");
    private final static QName _OrderDtlRowWarrantyComment_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "WarrantyComment");
    private final static QName _OrderDtlRowSalesCatID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesCatID");
    private final static QName _OrderDtlRowDiscBreakListCodeStartDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DiscBreakListCodeStartDate");
    private final static QName _OrderDtlRowDiscBreakListCodeListDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DiscBreakListCodeListDescription");
    private final static QName _OrderDtlRowCurrSymbol_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CurrSymbol");
    private final static QName _OrderDtlRowOrderNumCurrencyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderNumCurrencyCode");
    private final static QName _OrderDtlRowXPartNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "XPartNum");
    private final static QName _OrderDtlRowPartNumPartDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PartNumPartDescription");
    private final static QName _OrderDtlRowPrevXPartNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PrevXPartNum");
    private final static QName _OrderDtlRowRequestDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RequestDate");
    private final static QName _OrderDtlRowRespMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RespMessage");
    private final static QName _OrderDtlRowLineDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "LineDesc");
    private final static QName _OrderDtlRowBaseRevisionNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BaseRevisionNum");
    private final static QName _OrderDtlRowPlanUserID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PlanUserID");
    private final static QName _OrderDtlRowAvailUMFromQuote_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "AvailUMFromQuote");
    private final static QName _OrderDtlRowTaxCatID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TaxCatID");
    private final static QName _OrderDtlRowKitFlag_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "KitFlag");
    private final static QName _OrderDtlRowPlanGUID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PlanGUID");
    private final static QName _OrderDtlRowReference_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Reference");
    private final static QName _OrderDtlRowCreditLimitSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CreditLimitSource");
    private final static QName _OrderDtlRowContractCodeContractDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ContractCodeContractDescription");
    private final static QName _OrderDtlRowPartNumIUM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PartNumIUM");
    private final static QName _OrderDtlRowCustNumBTName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CustNumBTName");
    private final static QName _OrderDtlRowInvoiceComment_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "InvoiceComment");
    private final static QName _OrderDtlRowLaborMod_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "LaborMod");
    private final static QName _OrderDtlRowNeedByDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "NeedByDate");
    private final static QName _OrderDtlRowSalesCatIDDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesCatIDDescription");
    private final static QName _CheckCustOnCreditHoldCCustID_QNAME = new QName("Erp:BO:SalesOrder", "cCustID");
    private final static QName _OnChangeofTaxRgnITaxRegionCode_QNAME = new QName("Erp:BO:SalesOrder", "iTaxRegionCode");
    private final static QName _PhantomComponentsResponseErrMessage_QNAME = new QName("Erp:BO:SalesOrder", "errMessage");
    private final static QName _JobProdDeleteCJobNum_QNAME = new QName("Erp:BO:SalesOrder", "cJobNum");
    private final static QName _ChangeSellingQtyMasterResponsePcNeqQtyAction_QNAME = new QName("Erp:BO:SalesOrder", "pcNeqQtyAction");
    private final static QName _ChangeSellingQtyMasterResponseCSellingQuantityChangedMsgText_QNAME = new QName("Erp:BO:SalesOrder", "cSellingQuantityChangedMsgText");
    private final static QName _ChangeSellingQtyMasterResponsePcMessage_QNAME = new QName("Erp:BO:SalesOrder", "pcMessage");
    private final static QName _ChangeSellingQtyMasterResponseOpWarningMsg_QNAME = new QName("Erp:BO:SalesOrder", "opWarningMsg");
    private final static QName _ValidateInvQtyResponseOpNegInvMessage_QNAME = new QName("Erp:BO:SalesOrder", "opNegInvMessage");
    private final static QName _ValidateInvQtyResponseOpNegQtyAction_QNAME = new QName("Erp:BO:SalesOrder", "opNegQtyAction");
    private final static QName _GlbSugPOChgTablesetGlbSugPOChg_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "GlbSugPOChg");
    private final static QName _GetRowsResponseGetRowsResult_QNAME = new QName("Erp:BO:SalesOrder", "GetRowsResult");
    private final static QName _GetSmartStringResponseNewCustPartNum_QNAME = new QName("Erp:BO:SalesOrder", "NewCustPartNum");
    private final static QName _GetSmartStringResponseNewPartNum_QNAME = new QName("Erp:BO:SalesOrder", "NewPartNum");
    private final static QName _GetSmartStringResponseSmartString_QNAME = new QName("Erp:BO:SalesOrder", "SmartString");
    private final static QName _SOEntryUIParamsRowLocalization_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Localization");
    private final static QName _OrderRepCommRowSalesRepCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesRepCode");
    private final static QName _OrderRepCommRowSeq_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Seq");
    private final static QName _OrderRepCommRowName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Name");
    private final static QName _GetSmartStringRevisionNum_QNAME = new QName("Erp:BO:SalesOrder", "RevisionNum");
    private final static QName _GetSmartStringPartNum_QNAME = new QName("Erp:BO:SalesOrder", "PartNum");
    private final static QName _JobProdRowPullFromStockWarehouseDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PullFromStockWarehouseDesc");
    private final static QName _JobProdRowDemandLinkStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DemandLinkStatus");
    private final static QName _JobProdRowPartSalesUM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PartSalesUM");
    private final static QName _JobProdRowAsmPartNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "AsmPartNum");
    private final static QName _JobProdRowPartPricePerCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PartPricePerCode");
    private final static QName _JobProdRowMtlPartNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MtlPartNum");
    private final static QName _JobProdRowAsmPartDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "AsmPartDesc");
    private final static QName _JobProdRowPlanID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PlanID");
    private final static QName _JobProdRowPartIUM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PartIUM");
    private final static QName _JobProdRowMtlPartDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MtlPartDesc");
    private final static QName _JobProdRowJHPartDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "JHPartDesc");
    private final static QName _JobProdRowShipBy_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipBy");
    private final static QName _JobProdRowDemandLinkSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DemandLinkSource");
    private final static QName _JobProdRowMakeToType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MakeToType");
    private final static QName _JobProdRowPlant_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Plant");
    private final static QName _JobProdRowTFLineNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TFLineNum");
    private final static QName _JobProdRowTFOrdNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TFOrdNum");
    private final static QName _JobProdRowPullFromStockWarehouseCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PullFromStockWarehouseCode");
    private final static QName _JobProdRowPartPartDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PartPartDescription");
    private final static QName _JobProdRowJHPartNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "JHPartNum");
    private final static QName _JobProdRowJobNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "JobNum");
    private final static QName _JobProdRowCallLineLineDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CallLineLineDesc");
    private final static QName _JobProdRowJobNumPartDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "JobNumPartDescription");
    private final static QName _JobProdRowCustName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CustName");
    private final static QName _JobProdRowOrderLineLineDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderLineLineDesc");
    private final static QName _JobProdRowTargetJobNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TargetJobNum");
    private final static QName _JobProdRowCustID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CustID");
    private final static QName _JobProdRowWarehouseCodeDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "WarehouseCodeDescription");
    private final static QName _OrderDtlAttchRowDrawDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DrawDesc");
    private final static QName _OrderDtlAttchRowDocTypeID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DocTypeID");
    private final static QName _OrderDtlAttchRowFileName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FileName");
    private final static QName _OrderDtlAttchRowPDMDocID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PDMDocID");
    private final static QName _OrderRelTaxRowTaxRateDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TaxRateDate");
    private final static QName _OrderRelTaxRowResolutionNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ResolutionNum");
    private final static QName _OrderRelTaxRowTextCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TextCode");
    private final static QName _OrderRelTaxRowResolutionDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ResolutionDate");
    private final static QName _OrderRelTaxRowRateCodeDescDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RateCodeDescDescription");
    private final static QName _OrderRelTaxRowSalesTaxDescDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesTaxDescDescription");
    private final static QName _OrderRelTaxRowDocDisplaySymbol_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DocDisplaySymbol");
    private final static QName _OrderRelTaxRowDisplaySymbol_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DisplaySymbol");
    private final static QName _OrderRelTaxRowRateCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RateCode");
    private final static QName _OrderRelTaxRowTaxCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TaxCode");
    private final static QName _OrderRelTaxRowCollectionTypeDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CollectionTypeDescription");
    private final static QName _OrderCustTrkTablesetOrderCustTrk_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderCustTrk");
    private final static QName _GlbSugPOChgRowDisplaySuggestionStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DisplaySuggestionStatus");
    private final static QName _GlbSugPOChgRowSourceName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SourceName");
    private final static QName _GlbSugPOChgRowABCCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ABCCode");
    private final static QName _GlbSugPOChgRowRequireDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RequireDate");
    private final static QName _GlbSugPOChgRowNewShipByDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "NewShipByDate");
    private final static QName _GlbSugPOChgRowComment_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Comment");
    private final static QName _GlbSugPOChgRowSuggestionStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SuggestionStatus");
    private final static QName _GlbSugPOChgRowSalesIUM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesIUM");
    private final static QName _GlbSugPOChgRowSuggestionCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SuggestionCode");
    private final static QName _GlbSugPOChgRowGlbCompany_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "GlbCompany");
    private final static QName _GlbSugPOChgRowCancelReason_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CancelReason");
    private final static QName _GlbSugPOChgRowBuyerID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BuyerID");
    private final static QName _GlbSugPOChgRowTransDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TransDate");
    private final static QName _GetListResponseGetListResult_QNAME = new QName("Erp:BO:SalesOrder", "GetListResult");
    private final static QName _GetSelectSerialNumbersParamsIpTranType_QNAME = new QName("Erp:BO:SalesOrder", "ipTranType");
    private final static QName _GetSelectSerialNumbersParamsIpWhseCode_QNAME = new QName("Erp:BO:SalesOrder", "ipWhseCode");
    private final static QName _GetSelectSerialNumbersParamsIpPartNum_QNAME = new QName("Erp:BO:SalesOrder", "ipPartNum");
    private final static QName _GetSelectSerialNumbersParamsIpBinNum_QNAME = new QName("Erp:BO:SalesOrder", "ipBinNum");
    private final static QName _CheckComplianceOrderFailResponseCompliantMsg_QNAME = new QName("Erp:BO:SalesOrder", "compliantMsg");
    private final static QName _CheckICPOReadyToSendResponseCReadyToSendMsgText_QNAME = new QName("Erp:BO:SalesOrder", "cReadyToSendMsgText");
    private final static QName _ChangePartNumMasterPartNum_QNAME = new QName("Erp:BO:SalesOrder", "partNum");
    private final static QName _ChangePartNumMasterUomCode_QNAME = new QName("Erp:BO:SalesOrder", "uomCode");
    private final static QName _ChangePartNumMasterRowType_QNAME = new QName("Erp:BO:SalesOrder", "rowType");
    private final static QName _OnChangeofSoldToCreditCheckICustID_QNAME = new QName("Erp:BO:SalesOrder", "iCustID");
    private final static QName _GetListCustomWhereClause_QNAME = new QName("Erp:BO:SalesOrder", "whereClause");
    private final static QName _GetListCustomCustomClause_QNAME = new QName("Erp:BO:SalesOrder", "customClause");
    private final static QName _GetBreakListCodeDescResponseGetBreakListCodeDescResult_QNAME = new QName("Erp:BO:SalesOrder", "getBreakListCodeDescResult");
    private final static QName _OnChangeOfTaxPercentTaxCode_QNAME = new QName("Erp:BO:SalesOrder", "TaxCode");
    private final static QName _CheckProjectIDIpProjectID_QNAME = new QName("Erp:BO:SalesOrder", "ipProjectID");
    private final static QName _SelectSerialNumbersParamsRowPoLinkValues_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "poLinkValues");
    private final static QName _SelectSerialNumbersParamsRowXrefPartNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "xrefPartNum");
    private final static QName _SelectSerialNumbersParamsRowPartNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "partNum");
    private final static QName _SelectSerialNumbersParamsRowXrefPartType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "xrefPartType");
    private final static QName _SelectSerialNumbersParamsRowWhereClause_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "whereClause");
    private final static QName _SelectSerialNumbersParamsRowTransType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "transType");
    private final static QName _SelectSerialNumbersParamsRowPlant_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "plant");
    private final static QName _QuoteQtyRowMarkUpID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MarkUpID");
    private final static QName _QuoteQtyRowPercentType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PercentType");
    private final static QName _QuoteQtyRowPriceSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PriceSource");
    private final static QName _QuoteQtyRowMiscCostDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MiscCostDesc");
    private final static QName _QuoteQtyRowMiscChrg_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MiscChrg");
    private final static QName _QuoteQtyRowQuoteLineLineDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "QuoteLineLineDesc");
    private final static QName _OrderCustTrkRowPONum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PONum");
    private final static QName _OrderCustTrkRowShipToNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipToNum");
    private final static QName _OrderCustTrkRowOrderDtlNeedByDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderDtlNeedByDate");
    private final static QName _OrderCustTrkRowCustomerName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CustomerName");
    private final static QName _OrderCustTrkRowSoldToCustName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SoldToCustName");
    private final static QName _OrderCustTrkRowShipToName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipToName");
    private final static QName _OrderCustTrkRowOrderHedRequestDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedRequestDate");
    private final static QName _OrderCustTrkRowBTContactPhoneNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BTContactPhoneNum");
    private final static QName _OrderCustTrkRowOrderDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderDate");
    private final static QName _OrderCustTrkRowBTAddressList_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BTAddressList");
    private final static QName _OrderCustTrkRowBTCustomerName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BTCustomerName");
    private final static QName _OrderCustTrkRowOrderHedNeedByDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedNeedByDate");
    private final static QName _OrderCustTrkRowBTContactFaxNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BTContactFaxNum");
    private final static QName _OrderCustTrkRowOrderDtlRequestDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderDtlRequestDate");
    private final static QName _OrderCustTrkRowBTContactName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BTContactName");
    private final static QName _OrderCustTrkRowBTCustID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BTCustID");
    private final static QName _OrderCustTrkRowSoldToCustID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SoldToCustID");
    private final static QName _CheckSONumResponseOpMessage_QNAME = new QName("Erp:BO:SalesOrder", "opMessage");
    private final static QName _GetUIParamsResponseGetUIParamsResult_QNAME = new QName("Erp:BO:SalesOrder", "GetUIParamsResult");
    private final static QName _CheckOrderHedChangesResponseCOrderChangedMsgText_QNAME = new QName("Erp:BO:SalesOrder", "cOrderChangedMsgText");
    private final static QName _CheckConfigurationSourceRev_QNAME = new QName("Erp:BO:SalesOrder", "sourceRev");
    private final static QName _CheckConfigurationSourcePart_QNAME = new QName("Erp:BO:SalesOrder", "sourcePart");
    private final static QName _CreateOrderFromQuoteIPoNum_QNAME = new QName("Erp:BO:SalesOrder", "iPoNum");
    private final static QName _CreateOrderFromQuoteICreditStatus_QNAME = new QName("Erp:BO:SalesOrder", "iCreditStatus");
    private final static QName _CheckCustomerCreditLimitResponseCCreditLimitMessage_QNAME = new QName("Erp:BO:SalesOrder", "cCreditLimitMessage");
    private final static QName _ChangeBTCustIDMasterNewBillToCustID_QNAME = new QName("Erp:BO:SalesOrder", "NewBillToCustID");
    private final static QName _ETCMessageRowHelplink_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Helplink");
    private final static QName _ETCMessageRowSummary_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Summary");
    private final static QName _ETCMessageRowSeverity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Severity");
    private final static QName _ETCMessageRowAddrSourceID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "AddrSourceID");
    private final static QName _ETCMessageRowAddrSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "AddrSource");
    private final static QName _ETCMessageRowSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Source");
    private final static QName _ETCMessageRowTransactionID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TransactionID");
    private final static QName _ETCMessageRowDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Details");
    private final static QName _ETCMessageRowRequestID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RequestID");
    private final static QName _ETCMessageRowRefersTo_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RefersTo");
    private final static QName _OrdRelJobProdTablesetJobProd_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "JobProd");
    private final static QName _ChangeNeedByDateCTableName_QNAME = new QName("Erp:BO:SalesOrder", "cTableName");
    private final static QName _OrderHedListRowBTCustNumName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BTCustNumName");
    private final static QName _OrderHedListRowDemandContract_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DemandContract");
    private final static QName _OrderHedListRowBTCustNumCustID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BTCustNumCustID");
    private final static QName _OrderHedListRowCustomerBTName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CustomerBTName");
    private final static QName _OrderHedListRowCustomerCustID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CustomerCustID");
    private final static QName _BOUpdErrorTablesetBOUpdError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice", "BOUpdError");
    private final static QName _SaveOTSParamsRowOTSSaveAs_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSSaveAs");
    private final static QName _SaveOTSParamsRowOTSCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSCity");
    private final static QName _SaveOTSParamsRowOTSPhoneNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSPhoneNum");
    private final static QName _SaveOTSParamsRowOTSTaxRegionCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSTaxRegionCode");
    private final static QName _SaveOTSParamsRowOTSName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSName");
    private final static QName _SaveOTSParamsRowOTSSaveCustID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSSaveCustID");
    private final static QName _SaveOTSParamsRowOTSZIP_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSZIP");
    private final static QName _SaveOTSParamsRowOTSAddress1_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSAddress1");
    private final static QName _SaveOTSParamsRowOTSResaleID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSResaleID");
    private final static QName _SaveOTSParamsRowOTSContact_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSContact");
    private final static QName _SaveOTSParamsRowOTSAddress2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSAddress2");
    private final static QName _SaveOTSParamsRowOTSAddress3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSAddress3");
    private final static QName _SaveOTSParamsRowOTSFaxNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSFaxNum");
    private final static QName _SaveOTSParamsRowOTSState_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSState");
    private final static QName _SaveOTSParamsRowOTSShipToNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSShipToNum");
    private final static QName _OnChangeRateCodeProposedRateCode_QNAME = new QName("Erp:BO:SalesOrder", "proposedRateCode");
    private final static QName _CopyOrderResponseOutMessage_QNAME = new QName("Erp:BO:SalesOrder", "OutMessage");
    private final static QName _ReopenReleaseResponseReopenReleaseResult_QNAME = new QName("Erp:BO:SalesOrder", "ReopenReleaseResult");
    private final static QName _GetBreakListCodeDescPcLineWarehouse_QNAME = new QName("Erp:BO:SalesOrder", "pcLineWarehouse");
    private final static QName _GetBreakListCodeDescPcPartNum_QNAME = new QName("Erp:BO:SalesOrder", "pcPartNum");
    private final static QName _GetBreakListCodeDescPcListCode_QNAME = new QName("Erp:BO:SalesOrder", "pcListCode");
    private final static QName _GetBreakListCodeDescPcCurrencyCode_QNAME = new QName("Erp:BO:SalesOrder", "pcCurrencyCode");
    private final static QName _GetBreakListCodeDescPdtOrderDate_QNAME = new QName("Erp:BO:SalesOrder", "pdtOrderDate");
    private final static QName _TaxConnectStatusRowErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ErrorMessage");
    private final static QName _CloseOrderResponseCloseOrderResult_QNAME = new QName("Erp:BO:SalesOrder", "CloseOrderResult");
    private final static QName _OrderRelRowOTMFState_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTMFState");
    private final static QName _OrderRelRowOTMFCountryDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTMFCountryDescription");
    private final static QName _OrderRelRowRefNotes_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RefNotes");
    private final static QName _OrderRelRowShipViaCodeDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipViaCodeDescription");
    private final static QName _OrderRelRowOTMFName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTMFName");
    private final static QName _OrderRelRowServInstruct_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ServInstruct");
    private final static QName _OrderRelRowGroundType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "GroundType");
    private final static QName _OrderRelRowTPShipToName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TPShipToName");
    private final static QName _OrderRelRowShipViaCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipViaCode");
    private final static QName _OrderRelRowShipToAddressList_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipToAddressList");
    private final static QName _OrderRelRowVendorNumTermsCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "VendorNumTermsCode");
    private final static QName _OrderRelRowShipRouting_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipRouting");
    private final static QName _OrderRelRowPlantName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PlantName");
    private final static QName _OrderRelRowPurPointAddress2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PurPointAddress2");
    private final static QName _OrderRelRowPurPointAddress1_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PurPointAddress1");
    private final static QName _OrderRelRowOTMFCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTMFCity");
    private final static QName _OrderRelRowPurPointAddress3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PurPointAddress3");
    private final static QName _OrderRelRowTransportID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TransportID");
    private final static QName _OrderRelRowPurPointCountry_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PurPointCountry");
    private final static QName _OrderRelRowOTMFPhoneNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTMFPhoneNum");
    private final static QName _OrderRelRowPurPointName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PurPointName");
    private final static QName _OrderRelRowEntityUseCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "EntityUseCode");
    private final static QName _OrderRelRowServAuthNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ServAuthNum");
    private final static QName _OrderRelRowOTMFFaxNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTMFFaxNum");
    private final static QName _OrderRelRowOTMFContact_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTMFContact");
    private final static QName _OrderRelRowDockingStation_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DockingStation");
    private final static QName _OrderRelRowOTMFZIP_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTMFZIP");
    private final static QName _OrderRelRowWIOrder_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "WIOrder");
    private final static QName _OrderRelRowDropShipName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DropShipName");
    private final static QName _OrderRelRowPurPointCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PurPointCity");
    private final static QName _OrderRelRowStagingBinNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "StagingBinNum");
    private final static QName _OrderRelRowNotifyEMail_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "NotifyEMail");
    private final static QName _OrderRelRowDspRevMethod_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DspRevMethod");
    private final static QName _OrderRelRowRelStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RelStatus");
    private final static QName _OrderRelRowSubShipTo_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SubShipTo");
    private final static QName _OrderRelRowVendorNumAddress1_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "VendorNumAddress1");
    private final static QName _OrderRelRowVendorNumAddress3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "VendorNumAddress3");
    private final static QName _OrderRelRowVendorNumAddress2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "VendorNumAddress2");
    private final static QName _OrderRelRowComplianceMsg_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ComplianceMsg");
    private final static QName _OrderRelRowTPShipToCustID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TPShipToCustID");
    private final static QName _OrderRelRowOTMFAddress1_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTMFAddress1");
    private final static QName _OrderRelRowOTMFAddress2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTMFAddress2");
    private final static QName _OrderRelRowOTMFAddress3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTMFAddress3");
    private final static QName _OrderRelRowPrevReqDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PrevReqDate");
    private final static QName _OrderRelRowRAN_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RAN");
    private final static QName _OrderRelRowLocation_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Location");
    private final static QName _OrderRelRowVendorNumState_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "VendorNumState");
    private final static QName _OrderRelRowOTSCntryDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSCntryDescription");
    private final static QName _OrderRelRowShipViaCodeWebDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipViaCodeWebDesc");
    private final static QName _OrderRelRowMFCustID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MFCustID");
    private final static QName _OrderRelRowMarkForNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MarkForNum");
    private final static QName _OrderRelRowDemandReference_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DemandReference");
    private final static QName _OrderRelRowVendorNumCurrencyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "VendorNumCurrencyCode");
    private final static QName _OrderRelRowPickError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PickError");
    private final static QName _OrderRelRowPurPoint_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PurPoint");
    private final static QName _OrderRelRowReleaseStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ReleaseStatus");
    private final static QName _OrderRelRowDeliveryType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DeliveryType");
    private final static QName _OrderRelRowStagingWarehouseCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "StagingWarehouseCode");
    private final static QName _OrderRelRowDatePickTicketPrinted_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DatePickTicketPrinted");
    private final static QName _OrderRelRowPrevNeedByDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PrevNeedByDate");
    private final static QName _OrderRelRowVendorNumVendorID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "VendorNumVendorID");
    private final static QName _OrderRelRowReqDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ReqDate");
    private final static QName _OrderRelRowVendorNumDefaultFOB_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "VendorNumDefaultFOB");
    private final static QName _OrderRelRowPrevShipToNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PrevShipToNum");
    private final static QName _OrderRelRowPurPointState_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PurPointState");
    private final static QName _OrderRelRowTaxRegionCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TaxRegionCode");
    private final static QName _OrderRelRowMarkForAddrList_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MarkForAddrList");
    private final static QName _OrderRelRowPhaseID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PhaseID");
    private final static QName _OrderRelRowServPhone_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ServPhone");
    private final static QName _OrderRelRowPurPointZip_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PurPointZip");
    private final static QName _OrderRelRowVendorNumName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "VendorNumName");
    private final static QName _OrderRelRowVendorNumZIP_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "VendorNumZIP");
    private final static QName _OrderRelRowServRef5_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ServRef5");
    private final static QName _OrderRelRowServRef2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ServRef2");
    private final static QName _OrderRelRowServRef1_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ServRef1");
    private final static QName _OrderRelRowServRef4_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ServRef4");
    private final static QName _OrderRelRowServRef3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ServRef3");
    private final static QName _OrderRelRowTPShipToBTName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TPShipToBTName");
    private final static QName _OrderRelRowScheduleNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ScheduleNumber");
    private final static QName _OrderRelRowShipToContactName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipToContactName");
    private final static QName _OrderRelRowVendorNumCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "VendorNumCity");
    private final static QName _OrderRelRowTaxRegionCodeDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TaxRegionCodeDescription");
    private final static QName _OrderRelRowVendorNumCountry_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "VendorNumCountry");
    private final static QName _OrderRelRowWebSKU_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "WebSKU");
    private final static QName _OrderRelRowDspInvMeth_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DspInvMeth");
    private final static QName _OrderRelRowTaxExempt_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TaxExempt");
    private final static QName _OrderRelRowWIOrderLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "WIOrderLine");
    private final static QName _OrderRelRowServDeliveryDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ServDeliveryDate");
    private final static QName _OrderRelRowShipToContactEMailAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipToContactEMailAddress");
    private final static QName _UpdExtSalesOrderTablesetPartSubs_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PartSubs");
    private final static QName _UpdExtSalesOrderTablesetHedTaxSum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "HedTaxSum");
    private final static QName _UpdExtSalesOrderTablesetOrderRelTax_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderRelTax");
    private final static QName _UpdExtSalesOrderTablesetOrderHedAttch_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedAttch");
    private final static QName _UpdExtSalesOrderTablesetOrderMsc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderMsc");
    private final static QName _UpdExtSalesOrderTablesetTaxConnectStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TaxConnectStatus");
    private final static QName _UpdExtSalesOrderTablesetOrderDtl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderDtl");
    private final static QName _UpdExtSalesOrderTablesetOrderHedUPS_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedUPS");
    private final static QName _UpdExtSalesOrderTablesetOrderHist_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHist");
    private final static QName _UpdExtSalesOrderTablesetOrderRel_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderRel");
    private final static QName _UpdExtSalesOrderTablesetOrderRepComm_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderRepComm");
    private final static QName _UpdExtSalesOrderTablesetOHOrderMsc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OHOrderMsc");
    private final static QName _UpdExtSalesOrderTablesetSNFormat_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SNFormat");
    private final static QName _UpdExtSalesOrderTablesetOrderDtlAttch_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderDtlAttch");
    private final static QName _UpdExtSalesOrderTablesetOrderHed_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHed");
    private final static QName _UpdExtSalesOrderTablesetSelectedSerialNumbers_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SelectedSerialNumbers");
    private final static QName _CCLoadTranDataInTranDate_QNAME = new QName("Erp:BO:SalesOrder", "inTranDate");
    private final static QName _GetBySysRowIDsIds_QNAME = new QName("Erp:BO:SalesOrder", "ids");
    private final static QName _SNFormatRowSNMaskSuffix_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SNMaskSuffix");
    private final static QName _SNFormatRowSNPrefix_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SNPrefix");
    private final static QName _SNFormatRowSerialMaskDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SerialMaskDescription");
    private final static QName _SNFormatRowSNMaskPrefix_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SNMaskPrefix");
    private final static QName _SNFormatRowSerialMaskMask_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SerialMaskMask");
    private final static QName _SNFormatRowSerialMaskExample_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SerialMaskExample");
    private final static QName _SNFormatRowSNBaseDataType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SNBaseDataType");
    private final static QName _SNFormatRowSNLastUsedSeq_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SNLastUsedSeq");
    private final static QName _SNFormatRowSNMask_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SNMask");
    private final static QName _CheckSellingFactorDirectionIpSellingFactorDirection_QNAME = new QName("Erp:BO:SalesOrder", "ip_SellingFactorDirection");
    private final static QName _UpdateExtResponseUpdateExtResult_QNAME = new QName("Erp:BO:SalesOrder", "UpdateExtResult");
    private final static QName _CheckQuoteForCreditLimitResponseCCreditStatus_QNAME = new QName("Erp:BO:SalesOrder", "cCreditStatus");
    private final static QName _CloseOrderLineResponseCloseOrderLineResult_QNAME = new QName("Erp:BO:SalesOrder", "CloseOrderLineResult");
    private final static QName _ChangeSellingQtyMasterPcDimCode_QNAME = new QName("Erp:BO:SalesOrder", "pcDimCode");
    private final static QName _ChangeSellingQtyMasterPcWhseCode_QNAME = new QName("Erp:BO:SalesOrder", "pcWhseCode");
    private final static QName _ChangeSellingQtyMasterPcLotNum_QNAME = new QName("Erp:BO:SalesOrder", "pcLotNum");
    private final static QName _ChangeSellingQtyMasterPcBinNum_QNAME = new QName("Erp:BO:SalesOrder", "pcBinNum");
    private final static QName _ChangeContractNumMasterResponseOutMsg_QNAME = new QName("Erp:BO:SalesOrder", "outMsg");
    private final static QName _GetQuoteQtyResponseGetQuoteQtyResult_QNAME = new QName("Erp:BO:SalesOrder", "GetQuoteQtyResult");
    private final static QName _ChangeCardNumberInCardNumber_QNAME = new QName("Erp:BO:SalesOrder", "inCardNumber");
    private final static QName _UpdateOrderDtlDiscountPercentResponseUpdateOrderDtlDiscountPercentResult_QNAME = new QName("Erp:BO:SalesOrder", "UpdateOrderDtlDiscountPercentResult");
    private final static QName _SOEntryUIParamsTablesetSOEntryUIParams_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SOEntryUIParams");
    private final static QName _ChangeOrderRelShipToCustIDIShipToCustID_QNAME = new QName("Erp:BO:SalesOrder", "iShipToCustID");
    private final static QName _HasMultipleSubsIPartNum_QNAME = new QName("Erp:BO:SalesOrder", "iPartNum");
    private final static QName _ChangeRevNumMasterProposedRev_QNAME = new QName("Erp:BO:SalesOrder", "proposedRev");
    private final static QName _ChangeMiscAmountTableName_QNAME = new QName("Erp:BO:SalesOrder", "tableName");
    private final static QName _GlbSugPOChgDeleteCRowIdent_QNAME = new QName("Erp:BO:SalesOrder", "cRowIdent");
    private final static QName _OrderHedUPSRowEmailAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "EmailAddress");
    private final static QName _SelectSerialNumbersParamsTablesetSelectSerialNumbersParams_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SelectSerialNumbersParams");
    private final static QName _GetByIDLinkedOrderResponseGetByIDLinkedOrderResult_QNAME = new QName("Erp:BO:SalesOrder", "GetByIDLinkedOrderResult");
    private final static QName _CreateOrderFromQuoteResponseCreateOrderFromQuoteResult_QNAME = new QName("Erp:BO:SalesOrder", "CreateOrderFromQuoteResult");
    private final static QName _VerifySendICPOSuggResponseCVerifySendMsgText_QNAME = new QName("Erp:BO:SalesOrder", "cVerifySendMsgText");
    private final static QName _ETCAddressRowValidLine4_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ValidLine4");
    private final static QName _ETCAddressRowPostalCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PostalCode");
    private final static QName _ETCAddressRowCounty_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "County");
    private final static QName _ETCAddressRowValidLine1_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ValidLine1");
    private final static QName _ETCAddressRowValidLine2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ValidLine2");
    private final static QName _ETCAddressRowValidRegion_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ValidRegion");
    private final static QName _ETCAddressRowValidLine3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ValidLine3");
    private final static QName _ETCAddressRowAddressTypeDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "AddressTypeDesc");
    private final static QName _ETCAddressRowOTSCountry_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OTSCountry");
    private final static QName _ETCAddressRowValidCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ValidCity");
    private final static QName _ETCAddressRowPostNet_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PostNet");
    private final static QName _ETCAddressRowCarrierRoute_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CarrierRoute");
    private final static QName _ETCAddressRowAddressCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "AddressCode");
    private final static QName _ETCAddressRowCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "City");
    private final static QName _ETCAddressRowCarrierRouteDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CarrierRouteDesc");
    private final static QName _ETCAddressRowFipsCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FipsCode");
    private final static QName _ETCAddressRowCountry_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Country");
    private final static QName _ETCAddressRowRegion_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Region");
    private final static QName _ETCAddressRowAddressType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "AddressType");
    private final static QName _ETCAddressRowLine1_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Line1");
    private final static QName _ETCAddressRowLine2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Line2");
    private final static QName _ETCAddressRowResultCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ResultCode");
    private final static QName _ETCAddressRowValidPostalCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ValidPostalCode");
    private final static QName _ETCAddressRowLine3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Line3");
    private final static QName _ETCAddressRowValidCountry_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ValidCountry");
    private final static QName _GetBasePartForConfigResponseBasePartNum_QNAME = new QName("Erp:BO:SalesOrder", "basePartNum");
    private final static QName _GetBasePartForConfigResponseBaseRevisionNum_QNAME = new QName("Erp:BO:SalesOrder", "baseRevisionNum");
    private final static QName _SelectedSerialNumbersRowXRefPartNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "XRefPartNum");
    private final static QName _SelectedSerialNumbersRowTransType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TransType");
    private final static QName _SelectedSerialNumbersRowSNBaseNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SNBaseNumber");
    private final static QName _SelectedSerialNumbersRowKBLbrActionDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "KBLbrActionDesc");
    private final static QName _SelectedSerialNumbersRowReasonCodeType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ReasonCodeType");
    private final static QName _SelectedSerialNumbersRowReasonCodeDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ReasonCodeDesc");
    private final static QName _SelectedSerialNumbersRowRawSerialNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RawSerialNum");
    private final static QName _SelectedSerialNumbersRowSerialNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SerialNumber");
    private final static QName _SelectedSerialNumbersRowXRefPartType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "XRefPartType");
    private final static QName _SelectedSerialNumbersRowKitWhseList_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "KitWhseList");
    private final static QName _SelectedSerialNumbersRowScrappedReasonCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ScrappedReasonCode");
    private final static QName _CheckPartRevisionChangeResponseCPartNum_QNAME = new QName("Erp:BO:SalesOrder", "cPartNum");
    private final static QName _CheckPartRevisionChangeResponseCMsgType_QNAME = new QName("Erp:BO:SalesOrder", "cMsgType");
    private final static QName _CheckPartRevisionChangeResponseCConfigPartMessage_QNAME = new QName("Erp:BO:SalesOrder", "cConfigPartMessage");
    private final static QName _CheckPartRevisionChangeResponseCSubPartMessage_QNAME = new QName("Erp:BO:SalesOrder", "cSubPartMessage");
    private final static QName _OnChangeOfMktgEvntMktgCampaignID_QNAME = new QName("Erp:BO:SalesOrder", "MktgCampaignID");
    private final static QName _ChangeManualTaxCalcIpRateCode_QNAME = new QName("Erp:BO:SalesOrder", "ipRateCode");
    private final static QName _ChangeManualTaxCalcIpTaxCode_QNAME = new QName("Erp:BO:SalesOrder", "ipTaxCode");
    private final static QName _GetRowsWhereClauseSNFormat_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseSNFormat");
    private final static QName _GetRowsWhereClauseOrderRelTax_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseOrderRelTax");
    private final static QName _GetRowsWhereClauseOrderHed_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseOrderHed");
    private final static QName _GetRowsWhereClauseOrderRel_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseOrderRel");
    private final static QName _GetRowsWhereClauseOrderDtl_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseOrderDtl");
    private final static QName _GetRowsWhereClauseHedTaxSum_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseHedTaxSum");
    private final static QName _GetRowsWhereClauseOrderHedAttch_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseOrderHedAttch");
    private final static QName _GetRowsWhereClauseOrderMsc_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseOrderMsc");
    private final static QName _GetRowsWhereClauseOrderHist_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseOrderHist");
    private final static QName _GetRowsWhereClauseSelectedSerialNumbers_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseSelectedSerialNumbers");
    private final static QName _GetRowsWhereClauseOrderHedUPS_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseOrderHedUPS");
    private final static QName _GetRowsWhereClausePartSubs_QNAME = new QName("Erp:BO:SalesOrder", "whereClausePartSubs");
    private final static QName _GetRowsWhereClauseOrderRepComm_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseOrderRepComm");
    private final static QName _GetRowsWhereClauseOHOrderMsc_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseOHOrderMsc");
    private final static QName _GetRowsWhereClauseOrderDtlAttch_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseOrderDtlAttch");
    private final static QName _GetRowsWhereClauseTaxConnectStatus_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseTaxConnectStatus");
    private final static QName _ChangePartNumMasterResponseQuestionString_QNAME = new QName("Erp:BO:SalesOrder", "questionString");
    private final static QName _ChangePartNumMasterResponseExplodeBOMerrMessage_QNAME = new QName("Erp:BO:SalesOrder", "explodeBOMerrMessage");
    private final static QName _ChangePartNumMasterResponseCWarningMessage_QNAME = new QName("Erp:BO:SalesOrder", "cWarningMessage");
    private final static QName _ChangePartNumMasterResponseCDeleteComponentsMessage_QNAME = new QName("Erp:BO:SalesOrder", "cDeleteComponentsMessage");
    private final static QName _ChangeOrderRelVendorIDIpVendorID_QNAME = new QName("Erp:BO:SalesOrder", "ipVendorID");
    private final static QName _GetJobProdResponseGetJobProdResult_QNAME = new QName("Erp:BO:SalesOrder", "GetJobProdResult");
    private final static QName _GetRowsCustomerTrackerResponseGetRowsCustomerTrackerResult_QNAME = new QName("Erp:BO:SalesOrder", "GetRowsCustomerTrackerResult");
    private final static QName _CCProcessCardInTranType_QNAME = new QName("Erp:BO:SalesOrder", "inTranType");
    private final static QName _MasterUpdateResponseCCompliantMsg_QNAME = new QName("Erp:BO:SalesOrder", "cCompliantMsg");
    private final static QName _MasterUpdateResponseCResponseMsgOrdRel_QNAME = new QName("Erp:BO:SalesOrder", "cResponseMsgOrdRel");
    private final static QName _MasterUpdateResponseCDisplayMsg_QNAME = new QName("Erp:BO:SalesOrder", "cDisplayMsg");
    private final static QName _MasterUpdateResponseCResponseMsg_QNAME = new QName("Erp:BO:SalesOrder", "cResponseMsg");
    private final static QName _GetBySysRowIDsResponseGetBySysRowIDsResult_QNAME = new QName("Erp:BO:SalesOrder", "GetBySysRowIDsResult");
    private final static QName _GetNewOrderRelTaxRateCode_QNAME = new QName("Erp:BO:SalesOrder", "rateCode");
    private final static QName _GetNewOrderRelTaxTaxCode_QNAME = new QName("Erp:BO:SalesOrder", "taxCode");
    private final static QName _OHOrderMscRowQuoting_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Quoting");
    private final static QName _OHOrderMscRowChangeTrackStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ChangeTrackStatus");
    private final static QName _OHOrderMscRowFreqCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FreqCode");
    private final static QName _OHOrderMscRowMiscCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MiscCode");
    private final static QName _OHOrderMscRowDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Description");
    private final static QName _OHOrderMscRowMiscCodeDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "MiscCodeDescription");
    private final static QName _OHOrderMscRowChangeTrackMemoText_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ChangeTrackMemoText");
    private final static QName _OHOrderMscRowChangeTrackMemoDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ChangeTrackMemoDesc");
    private final static QName _OHOrderMscRowType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "Type");
    private final static QName _CheckProjectIDResponseOpProjMsg_QNAME = new QName("Erp:BO:SalesOrder", "opProjMsg");
    private final static QName _GetByIDResponseGetByIDResult_QNAME = new QName("Erp:BO:SalesOrder", "GetByIDResult");
    private final static QName _OnChangeOfFixedAmountRateCode_QNAME = new QName("Erp:BO:SalesOrder", "RateCode");
    private final static QName _ReopenOrderLineResponseReopenOrderLineResult_QNAME = new QName("Erp:BO:SalesOrder", "ReopenOrderLineResult");
    private final static QName _UpdateOrderDtlDiscountPercentOrderLines_QNAME = new QName("Erp:BO:SalesOrder", "orderLines");
    private final static QName _ApplyOrderBasedDiscountsResponseApplyOrderBasedDiscountsResult_QNAME = new QName("Erp:BO:SalesOrder", "ApplyOrderBasedDiscountsResult");
    private final static QName _CheckLtrOfCrdtResponseCheckLtrOfCrdtResult_QNAME = new QName("Erp:BO:SalesOrder", "CheckLtrOfCrdtResult");
    private final static QName _CheckKitRevisionIRevisionNum_QNAME = new QName("Erp:BO:SalesOrder", "iRevisionNum");
    private final static QName _OrderHedRowDocumentTypeID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DocumentTypeID");
    private final static QName _OrderHedRowCardNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CardNumber");
    private final static QName _OrderHedRowSalesRepList_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesRepList");
    private final static QName _OrderHedRowSoldToContactFaxNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SoldToContactFaxNum");
    private final static QName _OrderHedRowFFAddress1_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FFAddress1");
    private final static QName _OrderHedRowFFAddress3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FFAddress3");
    private final static QName _OrderHedRowOurBankBankName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OurBankBankName");
    private final static QName _OrderHedRowFFAddress2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FFAddress2");
    private final static QName _OrderHedRowCardMemberName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CardMemberName");
    private final static QName _OrderHedRowCurrencyCodeCurrencyID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CurrencyCodeCurrencyID");
    private final static QName _OrderHedRowCardID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CardID");
    private final static QName _OrderHedRowCCCSCID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CCCSCID");
    private final static QName _OrderHedRowEntryMethod_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "EntryMethod");
    private final static QName _OrderHedRowSoldToContactEMailAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SoldToContactEMailAddress");
    private final static QName _OrderHedRowFFZip_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FFZip");
    private final static QName _OrderHedRowCardStore_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CardStore");
    private final static QName _OrderHedRowWIUsername_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "WIUsername");
    private final static QName _OrderHedRowRateGrpCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RateGrpCode");
    private final static QName _OrderHedRowBillToCustomerName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BillToCustomerName");
    private final static QName _OrderHedRowCreditOverrideUserID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CreditOverrideUserID");
    private final static QName _OrderHedRowDispatchReasonCodeDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DispatchReasonCodeDescription");
    private final static QName _OrderHedRowFFCompName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FFCompName");
    private final static QName _OrderHedRowSoldToContactName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SoldToContactName");
    private final static QName _OrderHedRowPayAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PayAccount");
    private final static QName _OrderHedRowCardmemberReference_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CardmemberReference");
    private final static QName _OrderHedRowEntryPerson_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "EntryPerson");
    private final static QName _OrderHedRowXRefContractDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "XRefContractDate");
    private final static QName _OrderHedRowCardType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CardType");
    private final static QName _OrderHedRowShipToContactFaxNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipToContactFaxNum");
    private final static QName _OrderHedRowPayBTCountry_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PayBTCountry");
    private final static QName _OrderHedRowShipToCustId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipToCustId");
    private final static QName _OrderHedRowFFID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FFID");
    private final static QName _OrderHedRowCCTranID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CCTranID");
    private final static QName _OrderHedRowCCCSCIDToken_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CCCSCIDToken");
    private final static QName _OrderHedRowCurrencyCodeCurrDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CurrencyCodeCurrDesc");
    private final static QName _OrderHedRowRateGrpDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RateGrpDescription");
    private final static QName _OrderHedRowUPSQVShipFromName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "UPSQVShipFromName");
    private final static QName _OrderHedRowCustTradePartnerName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CustTradePartnerName");
    private final static QName _OrderHedRowDemandProcessDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DemandProcessDate");
    private final static QName _OrderHedRowReservePriDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ReservePriDescription");
    private final static QName _OrderHedRowCCApprovalNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CCApprovalNum");
    private final static QName _OrderHedRowLastTCtrlNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "LastTCtrlNum");
    private final static QName _OrderHedRowPayBTPhone_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PayBTPhone");
    private final static QName _OrderHedRowWIApplication_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "WIApplication");
    private final static QName _OrderHedRowWIUserID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "WIUserID");
    private final static QName _OrderHedRowFFCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FFCity");
    private final static QName _OrderHedRowBTContactEMailAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BTContactEMailAddress");
    private final static QName _OrderHedRowUserDate2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "UserDate2");
    private final static QName _OrderHedRowUserDate1_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "UserDate1");
    private final static QName _OrderHedRowXRefContractNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "XRefContractNum");
    private final static QName _OrderHedRowUserDate4_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "UserDate4");
    private final static QName _OrderHedRowUserDate3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "UserDate3");
    private final static QName _OrderHedRowWebEntryPerson_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "WebEntryPerson");
    private final static QName _OrderHedRowCCStreetAddr_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CCStreetAddr");
    private final static QName _OrderHedRowReferencePNRef_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ReferencePNRef");
    private final static QName _OrderHedRowCardTypeDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CardTypeDescription");
    private final static QName _OrderHedRowPSCurrCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PSCurrCode");
    private final static QName _OrderHedRowReservePriorityCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ReservePriorityCode");
    private final static QName _OrderHedRowCreditOverrideTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CreditOverrideTime");
    private final static QName _OrderHedRowCurrencyCodeCurrName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CurrencyCodeCurrName");
    private final static QName _OrderHedRowCCZip_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CCZip");
    private final static QName _OrderHedRowFFContact_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FFContact");
    private final static QName _OrderHedRowSoldToContactPhoneNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SoldToContactPhoneNum");
    private final static QName _OrderHedRowShipToContactPhoneNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ShipToContactPhoneNum");
    private final static QName _OrderHedRowProcessCard_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ProcessCard");
    private final static QName _OrderHedRowPayBTCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PayBTCity");
    private final static QName _OrderHedRowECCPaymentMethod_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ECCPaymentMethod");
    private final static QName _OrderHedRowLastBatchNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "LastBatchNum");
    private final static QName _OrderHedRowFOB_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FOB");
    private final static QName _OrderHedRowHDCaseDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "HDCaseDescription");
    private final static QName _OrderHedRowFFState_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FFState");
    private final static QName _OrderHedRowOrderCSR_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderCSR");
    private final static QName _OrderHedRowOurBank_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OurBank");
    private final static QName _OrderHedRowSalesRepCode4_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesRepCode4");
    private final static QName _OrderHedRowSalesRepCode5_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesRepCode5");
    private final static QName _OrderHedRowSalesRepCode2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesRepCode2");
    private final static QName _OrderHedRowSalesRepCode3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesRepCode3");
    private final static QName _OrderHedRowSalesRepCode1_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SalesRepCode1");
    private final static QName _OrderHedRowDspBTCustID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "dspBTCustID");
    private final static QName _OrderHedRowBTCustNumBTName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "BTCustNumBTName");
    private final static QName _OrderHedRowTermsCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TermsCode");
    private final static QName _OrderHedRowPSCurrCurrDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PSCurrCurrDesc");
    private final static QName _OrderHedRowAllocPriorityCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "AllocPriorityCode");
    private final static QName _OrderHedRowLinkMsg_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "LinkMsg");
    private final static QName _OrderHedRowUPSQVMemo_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "UPSQVMemo");
    private final static QName _OrderHedRowCCTranType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CCTranType");
    private final static QName _OrderHedRowLegalNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "LegalNumber");
    private final static QName _OrderHedRowFOBDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FOBDescription");
    private final static QName _OrderHedRowDispatchReason_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "DispatchReason");
    private final static QName _OrderHedRowCancelAfterDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CancelAfterDate");
    private final static QName _OrderHedRowTermsCodeDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TermsCodeDescription");
    private final static QName _OrderHedRowCSCResult_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CSCResult");
    private final static QName _OrderHedRowCurrencyCodeCurrSymbol_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CurrencyCodeCurrSymbol");
    private final static QName _OrderHedRowARLOCID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ARLOCID");
    private final static QName _OrderHedRowPayBTState_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PayBTState");
    private final static QName _OrderHedRowECCPONum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ECCPONum");
    private final static QName _OrderHedRowAVSAddr_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "AVSAddr");
    private final static QName _OrderHedRowAvailBTCustList_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "AvailBTCustList");
    private final static QName _OrderHedRowInvCurrCurrDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "InvCurrCurrDesc");
    private final static QName _OrderHedRowFFCountry_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FFCountry");
    private final static QName _OrderHedRowPayBTZip_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PayBTZip");
    private final static QName _OrderHedRowCurrencyCodeDocumentDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CurrencyCodeDocumentDesc");
    private final static QName _OrderHedRowOurBankDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OurBankDescription");
    private final static QName _OrderHedRowLastScheduleNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "LastScheduleNumber");
    private final static QName _OrderHedRowPayBTAddress2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PayBTAddress2");
    private final static QName _OrderHedRowPayBTAddress3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PayBTAddress3");
    private final static QName _OrderHedRowAVSZip_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "AVSZip");
    private final static QName _OrderHedRowFFPhoneNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "FFPhoneNum");
    private final static QName _OrderHedRowPayBTAddress1_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PayBTAddress1");
    private final static QName _OrderHedRowTaxPoint_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TaxPoint");
    private final static QName _OrderHedRowOrderStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderStatus");
    private final static QName _OrderHedRowInvCurrCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "InvCurrCode");
    private final static QName _OrderHedRowPayFlag_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PayFlag");
    private final static QName _OrderHedRowUserChar3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "UserChar3");
    private final static QName _OrderHedRowUserChar4_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "UserChar4");
    private final static QName _OrderHedRowUserChar1_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "UserChar1");
    private final static QName _OrderHedRowUserChar2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "UserChar2");
    private final static QName _OrderHedRowSoldToAddressList_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SoldToAddressList");
    private final static QName _OrderHedRowTranDocTypeID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TranDocTypeID");
    private final static QName _OrderHedRowCreditOverrideDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CreditOverrideDate");
    private final static QName _OrderHedRowCCResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "CCResponse");
    private final static QName _OrdDtlQuoteQtyTablesetQuoteQty_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "QuoteQty");
    private final static QName _CopyOrderIpPONum_QNAME = new QName("Erp:BO:SalesOrder", "ipPONum");
    private final static QName _GetSelectSerialNumbersParamsResponseGetSelectSerialNumbersParamsResult_QNAME = new QName("Erp:BO:SalesOrder", "GetSelectSerialNumbersParamsResult");
    private final static QName _CheckConfigurationResponseConfigureRevision_QNAME = new QName("Erp:BO:SalesOrder", "configureRevision");
    private final static QName _CheckConfigurationResponseReasonMessage_QNAME = new QName("Erp:BO:SalesOrder", "reasonMessage");
    private final static QName _CheckRateGrpCodeIpRateGrpCode_QNAME = new QName("Erp:BO:SalesOrder", "ipRateGrpCode");
    private final static QName _GetListCustomResponseGetListCustomResult_QNAME = new QName("Erp:BO:SalesOrder", "GetListCustomResult");
    private final static QName _OrderHistRowPartDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "PartDescription");
    private final static QName _OrderHistRowUOM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "UOM");
    private final static QName _ChangeOrderRelMarkForNumProposedMarkForNum_QNAME = new QName("Erp:BO:SalesOrder", "ProposedMarkForNum");
    private final static QName _GetGlbSugPOChgResponseGetGlbSugPOChgResult_QNAME = new QName("Erp:BO:SalesOrder", "GetGlbSugPOChgResult");
    private final static QName _CheckOrderHedDispatchReasonIpDispatchReason_QNAME = new QName("Erp:BO:SalesOrder", "ipDispatchReason");
    private final static QName _CloseReleaseResponseCloseReleaseResult_QNAME = new QName("Erp:BO:SalesOrder", "CloseReleaseResult");
    private final static QName _BOUpdErrorRowTableName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice", "TableName");
    private final static QName _BOUpdErrorRowErrorText_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice", "ErrorText");
    private final static QName _BOUpdErrorRowErrorType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice", "ErrorType");
    private final static QName _BOUpdErrorRowErrorLevel_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice", "ErrorLevel");
    private final static QName _GetKitComponentsResponseErrorMsg_QNAME = new QName("Erp:BO:SalesOrder", "errorMsg");
    private final static QName _HedTaxSumRowGroupID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "GroupID");
    private final static QName _HedTaxSumRowTaxDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "TaxDescription");
    private final static QName _ChangePlantProposedPlant_QNAME = new QName("Erp:BO:SalesOrder", "ProposedPlant");
    private final static QName _PartSubsRowSubType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SubType");
    private final static QName _PartSubsRowSubPart_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SubPart");
    private final static QName _PartSubsRowRecType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "RecType");
    private final static QName _PartSubsRowSubPartPartDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SubPartPartDescription");
    private final static QName _PartSubsRowSubPartIUM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SubPartIUM");
    private final static QName _PartSubsRowSubPartPricePerCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SubPartPricePerCode");
    private final static QName _PartSubsRowSubPartSalesUM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SubPartSalesUM");
    private final static QName _OnChangeARLOCIDIpARLOCID_QNAME = new QName("Erp:BO:SalesOrder", "ipARLOCID");
    private final static QName _ChangeSalesRepResponseCRepName_QNAME = new QName("Erp:BO:SalesOrder", "cRepName");
    private final static QName _PhantomComponentsPhPartNum_QNAME = new QName("Erp:BO:SalesOrder", "phPartNum");
    private final static QName _IceRowUserDefinedColumns_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice", "UserDefinedColumns");
    private final static QName _IceRowSpecifiedProperties_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice", "SpecifiedProperties");
    private final static QName _IceRowRowMod_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice", "RowMod");
    private final static QName _CreateOrderFromQuoteSaveOTSResponseCreateOrderFromQuoteSaveOTSResult_QNAME = new QName("Erp:BO:SalesOrder", "CreateOrderFromQuoteSaveOTSResult");
    private final static QName _ChangeOrderRelMFCustIDIpMFCustID_QNAME = new QName("Erp:BO:SalesOrder", "ipMFCustID");
    private final static QName _ChangeSalesRepCSalesRepCode_QNAME = new QName("Erp:BO:SalesOrder", "cSalesRepCode");
    private final static QName _RebuildShipUPSIpShipToNum_QNAME = new QName("Erp:BO:SalesOrder", "ipShipToNum");
    private final static QName _OrderHedListTablesetOrderHedList_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "OrderHedList");
    private final static QName _SaveOTSParamsTablesetSaveOTSParams_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "SaveOTSParams");
    private final static QName _GlbSugPOChgDeleteResponseGlbSugPOChgDeleteResult_QNAME = new QName("Erp:BO:SalesOrder", "GlbSugPOChgDeleteResult");
    private final static QName _GetRowsCustomerTrackerWhereClauseSerialNumberSearch_QNAME = new QName("Erp:BO:SalesOrder", "whereClauseSerialNumberSearch");
    private final static QName _EpicorFaultDetailMExceptionQualifiedName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice.Common", "m_exceptionQualifiedName");
    private final static QName _EpicorFaultDetailExceptionKindValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice.Common", "ExceptionKindValue");
    private final static QName _EpicorFaultDetailMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice.Common", "Message");
    private final static QName _EpicorFaultDetailMExKindQualifiedName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice.Common", "m_exKindQualifiedName");
    private final static QName _EpicorFaultDetailDataList_QNAME = new QName("http://schemas.datacontract.org/2004/07/Ice.Common", "DataList");
    private final static QName _ReopenOrderResponseReopenOrderResult_QNAME = new QName("Erp:BO:SalesOrder", "ReopenOrderResult");
    private final static QName _ProcessCounterSaleResponseCPackNum_QNAME = new QName("Erp:BO:SalesOrder", "cPackNum");
    private final static QName _CheckPartRevisionChangeCFieldName_QNAME = new QName("Erp:BO:SalesOrder", "cFieldName");
    private final static QName _RemoveICPOLinkResponseRemoveICPOLinkResult_QNAME = new QName("Erp:BO:SalesOrder", "RemoveICPOLinkResult");
    private final static QName _GetCodeDescListFieldName_QNAME = new QName("Erp:BO:SalesOrder", "fieldName");
    private final static QName _GetBySysRowIDResponseGetBySysRowIDResult_QNAME = new QName("Erp:BO:SalesOrder", "GetBySysRowIDResult");
    private final static QName _CheckOrderLinkToInterCompanyPOResponseCICPOLinkMessage_QNAME = new QName("Erp:BO:SalesOrder", "cICPOLinkMessage");
    private final static QName _OnChangeARLOCIDResponseOpMsg_QNAME = new QName("Erp:BO:SalesOrder", "opMsg");
    private final static QName _OnChangeARLOCIDResponseOpOverwriteValue_QNAME = new QName("Erp:BO:SalesOrder", "opOverwriteValue");
    private final static QName _OnChangeARLOCIDResponseOpFieldName_QNAME = new QName("Erp:BO:SalesOrder", "opFieldName");
    private final static QName _GetCodeDescListResponseGetCodeDescListResult_QNAME = new QName("Erp:BO:SalesOrder", "GetCodeDescListResult");
    private final static QName _CheckQuoteLinesNoQuantityResponseCQuoteLineWOQtyMsgText_QNAME = new QName("Erp:BO:SalesOrder", "cQuoteLineWOQtyMsgText");
    private final static QName _ETCValidateAddressResponseErrorMsg_QNAME = new QName("Erp:BO:SalesOrder", "ErrorMsg");
    private final static QName _ETCValidateAddressResponseETCValidateAddressResult_QNAME = new QName("Erp:BO:SalesOrder", "ETCValidateAddressResult");
    private final static QName _ETCAddrValidationTablesetETCAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ETCAddress");
    private final static QName _ETCAddrValidationTablesetETCMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Erp.Tablesets", "ETCMessage");
    private final static QName _OnChangeofPhaseIDIpPhaseID_QNAME = new QName("Erp:BO:SalesOrder", "ipPhaseID");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cpm.tesamm.SalesOrder
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UserDefinedColumns }
     * 
     */
    public UserDefinedColumns createUserDefinedColumns() {
        return new UserDefinedColumns();
    }

    /**
     * Create an instance of {@link ArrayOfKeyValueOfstringstring }
     * 
     */
    public ArrayOfKeyValueOfstringstring createArrayOfKeyValueOfstringstring() {
        return new ArrayOfKeyValueOfstringstring();
    }

    /**
     * Create an instance of {@link ArrayOfstring }
     * 
     */
    public ArrayOfstring createArrayOfstring() {
        return new ArrayOfstring();
    }

    /**
     * Create an instance of {@link ArrayOfguid }
     * 
     */
    public ArrayOfguid createArrayOfguid() {
        return new ArrayOfguid();
    }

    /**
     * Create an instance of {@link EpicorFaultDetail }
     * 
     */
    public EpicorFaultDetail createEpicorFaultDetail() {
        return new EpicorFaultDetail();
    }

    /**
     * Create an instance of {@link EpicorExceptionData }
     * 
     */
    public EpicorExceptionData createEpicorExceptionData() {
        return new EpicorExceptionData();
    }

    /**
     * Create an instance of {@link ArrayOfEpicorExceptionData }
     * 
     */
    public ArrayOfEpicorExceptionData createArrayOfEpicorExceptionData() {
        return new ArrayOfEpicorExceptionData();
    }

    /**
     * Create an instance of {@link TempRowBase }
     * 
     */
    public TempRowBase createTempRowBase() {
        return new TempRowBase();
    }

    /**
     * Create an instance of {@link BOUpdErrorTable }
     * 
     */
    public BOUpdErrorTable createBOUpdErrorTable() {
        return new BOUpdErrorTable();
    }

    /**
     * Create an instance of {@link IceTableset }
     * 
     */
    public IceTableset createIceTableset() {
        return new IceTableset();
    }

    /**
     * Create an instance of {@link BOUpdErrorTableset }
     * 
     */
    public BOUpdErrorTableset createBOUpdErrorTableset() {
        return new BOUpdErrorTableset();
    }

    /**
     * Create an instance of {@link BOUpdErrorRow }
     * 
     */
    public BOUpdErrorRow createBOUpdErrorRow() {
        return new BOUpdErrorRow();
    }

    /**
     * Create an instance of {@link IceRow }
     * 
     */
    public IceRow createIceRow() {
        return new IceRow();
    }

    /**
     * Create an instance of {@link OrderCustTrkTableset }
     * 
     */
    public OrderCustTrkTableset createOrderCustTrkTableset() {
        return new OrderCustTrkTableset();
    }

    /**
     * Create an instance of {@link OrderDtlAttchRow }
     * 
     */
    public OrderDtlAttchRow createOrderDtlAttchRow() {
        return new OrderDtlAttchRow();
    }

    /**
     * Create an instance of {@link SalesOrderTableset }
     * 
     */
    public SalesOrderTableset createSalesOrderTableset() {
        return new SalesOrderTableset();
    }

    /**
     * Create an instance of {@link OrderHedAttchRow }
     * 
     */
    public OrderHedAttchRow createOrderHedAttchRow() {
        return new OrderHedAttchRow();
    }

    /**
     * Create an instance of {@link SaveOTSParamsTableset }
     * 
     */
    public SaveOTSParamsTableset createSaveOTSParamsTableset() {
        return new SaveOTSParamsTableset();
    }

    /**
     * Create an instance of {@link QuoteQtyRow }
     * 
     */
    public QuoteQtyRow createQuoteQtyRow() {
        return new QuoteQtyRow();
    }

    /**
     * Create an instance of {@link GlbSugPOChgRow }
     * 
     */
    public GlbSugPOChgRow createGlbSugPOChgRow() {
        return new GlbSugPOChgRow();
    }

    /**
     * Create an instance of {@link SelectSerialNumbersParamsTableset }
     * 
     */
    public SelectSerialNumbersParamsTableset createSelectSerialNumbersParamsTableset() {
        return new SelectSerialNumbersParamsTableset();
    }

    /**
     * Create an instance of {@link GlbSugPOChgTableset }
     * 
     */
    public GlbSugPOChgTableset createGlbSugPOChgTableset() {
        return new GlbSugPOChgTableset();
    }

    /**
     * Create an instance of {@link JobProdTable }
     * 
     */
    public JobProdTable createJobProdTable() {
        return new JobProdTable();
    }

    /**
     * Create an instance of {@link SelectedSerialNumbersTable }
     * 
     */
    public SelectedSerialNumbersTable createSelectedSerialNumbersTable() {
        return new SelectedSerialNumbersTable();
    }

    /**
     * Create an instance of {@link OrderHedListTableset }
     * 
     */
    public OrderHedListTableset createOrderHedListTableset() {
        return new OrderHedListTableset();
    }

    /**
     * Create an instance of {@link PartSubsRow }
     * 
     */
    public PartSubsRow createPartSubsRow() {
        return new PartSubsRow();
    }

    /**
     * Create an instance of {@link OrderCustTrkTable }
     * 
     */
    public OrderCustTrkTable createOrderCustTrkTable() {
        return new OrderCustTrkTable();
    }

    /**
     * Create an instance of {@link SelectSerialNumbersParamsTable }
     * 
     */
    public SelectSerialNumbersParamsTable createSelectSerialNumbersParamsTable() {
        return new SelectSerialNumbersParamsTable();
    }

    /**
     * Create an instance of {@link OHOrderMscTable }
     * 
     */
    public OHOrderMscTable createOHOrderMscTable() {
        return new OHOrderMscTable();
    }

    /**
     * Create an instance of {@link OrderMscTable }
     * 
     */
    public OrderMscTable createOrderMscTable() {
        return new OrderMscTable();
    }

    /**
     * Create an instance of {@link OrderDtlAttchTable }
     * 
     */
    public OrderDtlAttchTable createOrderDtlAttchTable() {
        return new OrderDtlAttchTable();
    }

    /**
     * Create an instance of {@link HedTaxSumRow }
     * 
     */
    public HedTaxSumRow createHedTaxSumRow() {
        return new HedTaxSumRow();
    }

    /**
     * Create an instance of {@link OrderMscRow }
     * 
     */
    public OrderMscRow createOrderMscRow() {
        return new OrderMscRow();
    }

    /**
     * Create an instance of {@link OrderDtlTable }
     * 
     */
    public OrderDtlTable createOrderDtlTable() {
        return new OrderDtlTable();
    }

    /**
     * Create an instance of {@link SelectedSerialNumbersRow }
     * 
     */
    public SelectedSerialNumbersRow createSelectedSerialNumbersRow() {
        return new SelectedSerialNumbersRow();
    }

    /**
     * Create an instance of {@link SOEntryUIParamsTable }
     * 
     */
    public SOEntryUIParamsTable createSOEntryUIParamsTable() {
        return new SOEntryUIParamsTable();
    }

    /**
     * Create an instance of {@link PartSubsTable }
     * 
     */
    public PartSubsTable createPartSubsTable() {
        return new PartSubsTable();
    }

    /**
     * Create an instance of {@link OrdDtlQuoteQtyTableset }
     * 
     */
    public OrdDtlQuoteQtyTableset createOrdDtlQuoteQtyTableset() {
        return new OrdDtlQuoteQtyTableset();
    }

    /**
     * Create an instance of {@link ETCMessageTable }
     * 
     */
    public ETCMessageTable createETCMessageTable() {
        return new ETCMessageTable();
    }

    /**
     * Create an instance of {@link OrderCustTrkRow }
     * 
     */
    public OrderCustTrkRow createOrderCustTrkRow() {
        return new OrderCustTrkRow();
    }

    /**
     * Create an instance of {@link OrdRelJobProdTableset }
     * 
     */
    public OrdRelJobProdTableset createOrdRelJobProdTableset() {
        return new OrdRelJobProdTableset();
    }

    /**
     * Create an instance of {@link OrderHedTable }
     * 
     */
    public OrderHedTable createOrderHedTable() {
        return new OrderHedTable();
    }

    /**
     * Create an instance of {@link SNFormatRow }
     * 
     */
    public SNFormatRow createSNFormatRow() {
        return new SNFormatRow();
    }

    /**
     * Create an instance of {@link OrderHedAttchTable }
     * 
     */
    public OrderHedAttchTable createOrderHedAttchTable() {
        return new OrderHedAttchTable();
    }

    /**
     * Create an instance of {@link OrderRelRow }
     * 
     */
    public OrderRelRow createOrderRelRow() {
        return new OrderRelRow();
    }

    /**
     * Create an instance of {@link OrderRepCommTable }
     * 
     */
    public OrderRepCommTable createOrderRepCommTable() {
        return new OrderRepCommTable();
    }

    /**
     * Create an instance of {@link GlbSugPOChgTable }
     * 
     */
    public GlbSugPOChgTable createGlbSugPOChgTable() {
        return new GlbSugPOChgTable();
    }

    /**
     * Create an instance of {@link SelectSerialNumbersParamsRow }
     * 
     */
    public SelectSerialNumbersParamsRow createSelectSerialNumbersParamsRow() {
        return new SelectSerialNumbersParamsRow();
    }

    /**
     * Create an instance of {@link OrderDtlRow }
     * 
     */
    public OrderDtlRow createOrderDtlRow() {
        return new OrderDtlRow();
    }

    /**
     * Create an instance of {@link ETCAddressTable }
     * 
     */
    public ETCAddressTable createETCAddressTable() {
        return new ETCAddressTable();
    }

    /**
     * Create an instance of {@link HedTaxSumTable }
     * 
     */
    public HedTaxSumTable createHedTaxSumTable() {
        return new HedTaxSumTable();
    }

    /**
     * Create an instance of {@link ETCAddrValidationTableset }
     * 
     */
    public ETCAddrValidationTableset createETCAddrValidationTableset() {
        return new ETCAddrValidationTableset();
    }

    /**
     * Create an instance of {@link OrderHedUPSTable }
     * 
     */
    public OrderHedUPSTable createOrderHedUPSTable() {
        return new OrderHedUPSTable();
    }

    /**
     * Create an instance of {@link OrderHistTable }
     * 
     */
    public OrderHistTable createOrderHistTable() {
        return new OrderHistTable();
    }

    /**
     * Create an instance of {@link OrderHedRow }
     * 
     */
    public OrderHedRow createOrderHedRow() {
        return new OrderHedRow();
    }

    /**
     * Create an instance of {@link QuoteQtyTable }
     * 
     */
    public QuoteQtyTable createQuoteQtyTable() {
        return new QuoteQtyTable();
    }

    /**
     * Create an instance of {@link OrderHistRow }
     * 
     */
    public OrderHistRow createOrderHistRow() {
        return new OrderHistRow();
    }

    /**
     * Create an instance of {@link SaveOTSParamsRow }
     * 
     */
    public SaveOTSParamsRow createSaveOTSParamsRow() {
        return new SaveOTSParamsRow();
    }

    /**
     * Create an instance of {@link OHOrderMscRow }
     * 
     */
    public OHOrderMscRow createOHOrderMscRow() {
        return new OHOrderMscRow();
    }

    /**
     * Create an instance of {@link TaxConnectStatusTable }
     * 
     */
    public TaxConnectStatusTable createTaxConnectStatusTable() {
        return new TaxConnectStatusTable();
    }

    /**
     * Create an instance of {@link OrderRelTable }
     * 
     */
    public OrderRelTable createOrderRelTable() {
        return new OrderRelTable();
    }

    /**
     * Create an instance of {@link OrderRepCommRow }
     * 
     */
    public OrderRepCommRow createOrderRepCommRow() {
        return new OrderRepCommRow();
    }

    /**
     * Create an instance of {@link ETCMessageRow }
     * 
     */
    public ETCMessageRow createETCMessageRow() {
        return new ETCMessageRow();
    }

    /**
     * Create an instance of {@link OrderHedUPSRow }
     * 
     */
    public OrderHedUPSRow createOrderHedUPSRow() {
        return new OrderHedUPSRow();
    }

    /**
     * Create an instance of {@link OrderHedListRow }
     * 
     */
    public OrderHedListRow createOrderHedListRow() {
        return new OrderHedListRow();
    }

    /**
     * Create an instance of {@link SOEntryUIParamsTableset }
     * 
     */
    public SOEntryUIParamsTableset createSOEntryUIParamsTableset() {
        return new SOEntryUIParamsTableset();
    }

    /**
     * Create an instance of {@link ETCAddressRow }
     * 
     */
    public ETCAddressRow createETCAddressRow() {
        return new ETCAddressRow();
    }

    /**
     * Create an instance of {@link SNFormatTable }
     * 
     */
    public SNFormatTable createSNFormatTable() {
        return new SNFormatTable();
    }

    /**
     * Create an instance of {@link SaveOTSParamsTable }
     * 
     */
    public SaveOTSParamsTable createSaveOTSParamsTable() {
        return new SaveOTSParamsTable();
    }

    /**
     * Create an instance of {@link JobProdRow }
     * 
     */
    public JobProdRow createJobProdRow() {
        return new JobProdRow();
    }

    /**
     * Create an instance of {@link OrderRelTaxTable }
     * 
     */
    public OrderRelTaxTable createOrderRelTaxTable() {
        return new OrderRelTaxTable();
    }

    /**
     * Create an instance of {@link UpdExtSalesOrderTableset }
     * 
     */
    public UpdExtSalesOrderTableset createUpdExtSalesOrderTableset() {
        return new UpdExtSalesOrderTableset();
    }

    /**
     * Create an instance of {@link OrderRelTaxRow }
     * 
     */
    public OrderRelTaxRow createOrderRelTaxRow() {
        return new OrderRelTaxRow();
    }

    /**
     * Create an instance of {@link TaxConnectStatusRow }
     * 
     */
    public TaxConnectStatusRow createTaxConnectStatusRow() {
        return new TaxConnectStatusRow();
    }

    /**
     * Create an instance of {@link SOEntryUIParamsRow }
     * 
     */
    public SOEntryUIParamsRow createSOEntryUIParamsRow() {
        return new SOEntryUIParamsRow();
    }

    /**
     * Create an instance of {@link OrderHedListTable }
     * 
     */
    public OrderHedListTable createOrderHedListTable() {
        return new OrderHedListTable();
    }

    /**
     * Create an instance of {@link CreateOrderDtlComplementsResponse }
     * 
     */
    public CreateOrderDtlComplementsResponse createCreateOrderDtlComplementsResponse() {
        return new CreateOrderDtlComplementsResponse();
    }

    /**
     * Create an instance of {@link GetKitComponentsResponse }
     * 
     */
    public GetKitComponentsResponse createGetKitComponentsResponse() {
        return new GetKitComponentsResponse();
    }

    /**
     * Create an instance of {@link OnChangeTaxCodeResponse }
     * 
     */
    public OnChangeTaxCodeResponse createOnChangeTaxCodeResponse() {
        return new OnChangeTaxCodeResponse();
    }

    /**
     * Create an instance of {@link OrderDtlGetNewContracts }
     * 
     */
    public OrderDtlGetNewContracts createOrderDtlGetNewContracts() {
        return new OrderDtlGetNewContracts();
    }

    /**
     * Create an instance of {@link ChangeOrderRelBuyToOrderResponse }
     * 
     */
    public ChangeOrderRelBuyToOrderResponse createChangeOrderRelBuyToOrderResponse() {
        return new ChangeOrderRelBuyToOrderResponse();
    }

    /**
     * Create an instance of {@link CreateLineMiscChargesFromQuote }
     * 
     */
    public CreateLineMiscChargesFromQuote createCreateLineMiscChargesFromQuote() {
        return new CreateLineMiscChargesFromQuote();
    }

    /**
     * Create an instance of {@link ChangeHedUseOTS }
     * 
     */
    public ChangeHedUseOTS createChangeHedUseOTS() {
        return new ChangeHedUseOTS();
    }

    /**
     * Create an instance of {@link ChangeOverridePriceList }
     * 
     */
    public ChangeOverridePriceList createChangeOverridePriceList() {
        return new ChangeOverridePriceList();
    }

    /**
     * Create an instance of {@link ChangeERSOrderResponse }
     * 
     */
    public ChangeERSOrderResponse createChangeERSOrderResponse() {
        return new ChangeERSOrderResponse();
    }

    /**
     * Create an instance of {@link OnChangeofBTCustIDResponse }
     * 
     */
    public OnChangeofBTCustIDResponse createOnChangeofBTCustIDResponse() {
        return new OnChangeofBTCustIDResponse();
    }

    /**
     * Create an instance of {@link OrderDtlGetNewContractsResponse }
     * 
     */
    public OrderDtlGetNewContractsResponse createOrderDtlGetNewContractsResponse() {
        return new OrderDtlGetNewContractsResponse();
    }

    /**
     * Create an instance of {@link ChangeMiscPercentResponse }
     * 
     */
    public ChangeMiscPercentResponse createChangeMiscPercentResponse() {
        return new ChangeMiscPercentResponse();
    }

    /**
     * Create an instance of {@link ChangeSalesUOMResponse }
     * 
     */
    public ChangeSalesUOMResponse createChangeSalesUOMResponse() {
        return new ChangeSalesUOMResponse();
    }

    /**
     * Create an instance of {@link CheckCustomerCreditRelease }
     * 
     */
    public CheckCustomerCreditRelease createCheckCustomerCreditRelease() {
        return new CheckCustomerCreditRelease();
    }

    /**
     * Create an instance of {@link ChangeOrderRelShipToContactResponse }
     * 
     */
    public ChangeOrderRelShipToContactResponse createChangeOrderRelShipToContactResponse() {
        return new ChangeOrderRelShipToContactResponse();
    }

    /**
     * Create an instance of {@link PhantomComponentsResponse }
     * 
     */
    public PhantomComponentsResponse createPhantomComponentsResponse() {
        return new PhantomComponentsResponse();
    }

    /**
     * Create an instance of {@link GetCalcQtyPref }
     * 
     */
    public GetCalcQtyPref createGetCalcQtyPref() {
        return new GetCalcQtyPref();
    }

    /**
     * Create an instance of {@link GetNewOrderDtlAttch }
     * 
     */
    public GetNewOrderDtlAttch createGetNewOrderDtlAttch() {
        return new GetNewOrderDtlAttch();
    }

    /**
     * Create an instance of {@link SetUPSQVEnable }
     * 
     */
    public SetUPSQVEnable createSetUPSQVEnable() {
        return new SetUPSQVEnable();
    }

    /**
     * Create an instance of {@link ChangeNeedByDate }
     * 
     */
    public ChangeNeedByDate createChangeNeedByDate() {
        return new ChangeNeedByDate();
    }

    /**
     * Create an instance of {@link CreateOrderFromQuoteSaveOTSResponse }
     * 
     */
    public CreateOrderFromQuoteSaveOTSResponse createCreateOrderFromQuoteSaveOTSResponse() {
        return new CreateOrderFromQuoteSaveOTSResponse();
    }

    /**
     * Create an instance of {@link GetByIDResponse }
     * 
     */
    public GetByIDResponse createGetByIDResponse() {
        return new GetByIDResponse();
    }

    /**
     * Create an instance of {@link ChangeQuoteLine }
     * 
     */
    public ChangeQuoteLine createChangeQuoteLine() {
        return new ChangeQuoteLine();
    }

    /**
     * Create an instance of {@link ChangeXPartNum }
     * 
     */
    public ChangeXPartNum createChangeXPartNum() {
        return new ChangeXPartNum();
    }

    /**
     * Create an instance of {@link OnChangeRateCode }
     * 
     */
    public OnChangeRateCode createOnChangeRateCode() {
        return new OnChangeRateCode();
    }

    /**
     * Create an instance of {@link ChangeODtlWarehouse }
     * 
     */
    public ChangeODtlWarehouse createChangeODtlWarehouse() {
        return new ChangeODtlWarehouse();
    }

    /**
     * Create an instance of {@link OnChangeARLOCIDResponse }
     * 
     */
    public OnChangeARLOCIDResponse createOnChangeARLOCIDResponse() {
        return new OnChangeARLOCIDResponse();
    }

    /**
     * Create an instance of {@link OnChangeOfTaxPercentResponse }
     * 
     */
    public OnChangeOfTaxPercentResponse createOnChangeOfTaxPercentResponse() {
        return new OnChangeOfTaxPercentResponse();
    }

    /**
     * Create an instance of {@link ChangeContractNumResponse }
     * 
     */
    public ChangeContractNumResponse createChangeContractNumResponse() {
        return new ChangeContractNumResponse();
    }

    /**
     * Create an instance of {@link CheckQuoteForCreditLimitResponse }
     * 
     */
    public CheckQuoteForCreditLimitResponse createCheckQuoteForCreditLimitResponse() {
        return new CheckQuoteForCreditLimitResponse();
    }

    /**
     * Create an instance of {@link ValidateInvQtyResponse }
     * 
     */
    public ValidateInvQtyResponse createValidateInvQtyResponse() {
        return new ValidateInvQtyResponse();
    }

    /**
     * Create an instance of {@link ChangeOrderRelUseOTMFResponse }
     * 
     */
    public ChangeOrderRelUseOTMFResponse createChangeOrderRelUseOTMFResponse() {
        return new ChangeOrderRelUseOTMFResponse();
    }

    /**
     * Create an instance of {@link GetQuoteQtyResponse }
     * 
     */
    public GetQuoteQtyResponse createGetQuoteQtyResponse() {
        return new GetQuoteQtyResponse();
    }

    /**
     * Create an instance of {@link SetCalcQtysPref }
     * 
     */
    public SetCalcQtysPref createSetCalcQtysPref() {
        return new SetCalcQtysPref();
    }

    /**
     * Create an instance of {@link NegativeInventoryTestResponse }
     * 
     */
    public NegativeInventoryTestResponse createNegativeInventoryTestResponse() {
        return new NegativeInventoryTestResponse();
    }

    /**
     * Create an instance of {@link OnChangeofSoldToCreditCheck }
     * 
     */
    public OnChangeofSoldToCreditCheck createOnChangeofSoldToCreditCheck() {
        return new OnChangeofSoldToCreditCheck();
    }

    /**
     * Create an instance of {@link ChangeOrderRelMarkForNumResponse }
     * 
     */
    public ChangeOrderRelMarkForNumResponse createChangeOrderRelMarkForNumResponse() {
        return new ChangeOrderRelMarkForNumResponse();
    }

    /**
     * Create an instance of {@link REVISIONHISTORY06Response }
     * 
     */
    public REVISIONHISTORY06Response createREVISIONHISTORY06Response() {
        return new REVISIONHISTORY06Response();
    }

    /**
     * Create an instance of {@link ChangeMakeResponse }
     * 
     */
    public ChangeMakeResponse createChangeMakeResponse() {
        return new ChangeMakeResponse();
    }

    /**
     * Create an instance of {@link ProcessQuickEntryResponse }
     * 
     */
    public ProcessQuickEntryResponse createProcessQuickEntryResponse() {
        return new ProcessQuickEntryResponse();
    }

    /**
     * Create an instance of {@link ChangeCurrencyCodeResponse }
     * 
     */
    public ChangeCurrencyCodeResponse createChangeCurrencyCodeResponse() {
        return new ChangeCurrencyCodeResponse();
    }

    /**
     * Create an instance of {@link CheckRateGrpCode }
     * 
     */
    public CheckRateGrpCode createCheckRateGrpCode() {
        return new CheckRateGrpCode();
    }

    /**
     * Create an instance of {@link GetBreakListCodeDescResponse }
     * 
     */
    public GetBreakListCodeDescResponse createGetBreakListCodeDescResponse() {
        return new GetBreakListCodeDescResponse();
    }

    /**
     * Create an instance of {@link CreateLinesFromHistoryResponse }
     * 
     */
    public CreateLinesFromHistoryResponse createCreateLinesFromHistoryResponse() {
        return new CreateLinesFromHistoryResponse();
    }

    /**
     * Create an instance of {@link ChangeVendorChange }
     * 
     */
    public ChangeVendorChange createChangeVendorChange() {
        return new ChangeVendorChange();
    }

    /**
     * Create an instance of {@link ChangeRMANum }
     * 
     */
    public ChangeRMANum createChangeRMANum() {
        return new ChangeRMANum();
    }

    /**
     * Create an instance of {@link OnChangeofBTCustID }
     * 
     */
    public OnChangeofBTCustID createOnChangeofBTCustID() {
        return new OnChangeofBTCustID();
    }

    /**
     * Create an instance of {@link ChangeOrderRelMFCustID }
     * 
     */
    public ChangeOrderRelMFCustID createChangeOrderRelMFCustID() {
        return new ChangeOrderRelMFCustID();
    }

    /**
     * Create an instance of {@link GetInventoryQuantitiesResponse }
     * 
     */
    public GetInventoryQuantitiesResponse createGetInventoryQuantitiesResponse() {
        return new GetInventoryQuantitiesResponse();
    }

    /**
     * Create an instance of {@link GetList }
     * 
     */
    public GetList createGetList() {
        return new GetList();
    }

    /**
     * Create an instance of {@link CheckMakeDirectReleasesResponse }
     * 
     */
    public CheckMakeDirectReleasesResponse createCheckMakeDirectReleasesResponse() {
        return new CheckMakeDirectReleasesResponse();
    }

    /**
     * Create an instance of {@link CheckICPOReadyToSend }
     * 
     */
    public CheckICPOReadyToSend createCheckICPOReadyToSend() {
        return new CheckICPOReadyToSend();
    }

    /**
     * Create an instance of {@link GetNewSalesKitResponse }
     * 
     */
    public GetNewSalesKitResponse createGetNewSalesKitResponse() {
        return new GetNewSalesKitResponse();
    }

    /**
     * Create an instance of {@link ChangeNewNeedByDateResponse }
     * 
     */
    public ChangeNewNeedByDateResponse createChangeNewNeedByDateResponse() {
        return new ChangeNewNeedByDateResponse();
    }

    /**
     * Create an instance of {@link GetPayBTFlagDefaultsResponse }
     * 
     */
    public GetPayBTFlagDefaultsResponse createGetPayBTFlagDefaultsResponse() {
        return new GetPayBTFlagDefaultsResponse();
    }

    /**
     * Create an instance of {@link OnChangeOfTaxAmt }
     * 
     */
    public OnChangeOfTaxAmt createOnChangeOfTaxAmt() {
        return new OnChangeOfTaxAmt();
    }

    /**
     * Create an instance of {@link OnChangeofBTConNumResponse }
     * 
     */
    public OnChangeofBTConNumResponse createOnChangeofBTConNumResponse() {
        return new OnChangeofBTConNumResponse();
    }

    /**
     * Create an instance of {@link CheckPartRevisionChange }
     * 
     */
    public CheckPartRevisionChange createCheckPartRevisionChange() {
        return new CheckPartRevisionChange();
    }

    /**
     * Create an instance of {@link CheckQuoteLinesNoQuantity }
     * 
     */
    public CheckQuoteLinesNoQuantity createCheckQuoteLinesNoQuantity() {
        return new CheckQuoteLinesNoQuantity();
    }

    /**
     * Create an instance of {@link ProcessCounterSaleResponse }
     * 
     */
    public ProcessCounterSaleResponse createProcessCounterSaleResponse() {
        return new ProcessCounterSaleResponse();
    }

    /**
     * Create an instance of {@link OnChangeTaxableAmtResponse }
     * 
     */
    public OnChangeTaxableAmtResponse createOnChangeTaxableAmtResponse() {
        return new OnChangeTaxableAmtResponse();
    }

    /**
     * Create an instance of {@link GetNewOrderHedUPS }
     * 
     */
    public GetNewOrderHedUPS createGetNewOrderHedUPS() {
        return new GetNewOrderHedUPS();
    }

    /**
     * Create an instance of {@link ChangeDiscountPercentResponse }
     * 
     */
    public ChangeDiscountPercentResponse createChangeDiscountPercentResponse() {
        return new ChangeDiscountPercentResponse();
    }

    /**
     * Create an instance of {@link ReopenOrderLineResponse }
     * 
     */
    public ReopenOrderLineResponse createReopenOrderLineResponse() {
        return new ReopenOrderLineResponse();
    }

    /**
     * Create an instance of {@link ChangeMake }
     * 
     */
    public ChangeMake createChangeMake() {
        return new ChangeMake();
    }

    /**
     * Create an instance of {@link JobProdDelete }
     * 
     */
    public JobProdDelete createJobProdDelete() {
        return new JobProdDelete();
    }

    /**
     * Create an instance of {@link VerifySendICPOSugg }
     * 
     */
    public VerifySendICPOSugg createVerifySendICPOSugg() {
        return new VerifySendICPOSugg();
    }

    /**
     * Create an instance of {@link ChangeBTCustIDMaster }
     * 
     */
    public ChangeBTCustIDMaster createChangeBTCustIDMaster() {
        return new ChangeBTCustIDMaster();
    }

    /**
     * Create an instance of {@link CreateOrderFromQuoteSaveOTS }
     * 
     */
    public CreateOrderFromQuoteSaveOTS createCreateOrderFromQuoteSaveOTS() {
        return new CreateOrderFromQuoteSaveOTS();
    }

    /**
     * Create an instance of {@link OnChangeCreditCardOrder }
     * 
     */
    public OnChangeCreditCardOrder createOnChangeCreditCardOrder() {
        return new OnChangeCreditCardOrder();
    }

    /**
     * Create an instance of {@link CreateOrderFromQuoteResponse }
     * 
     */
    public CreateOrderFromQuoteResponse createCreateOrderFromQuoteResponse() {
        return new CreateOrderFromQuoteResponse();
    }

    /**
     * Create an instance of {@link ChangeKitPricing }
     * 
     */
    public ChangeKitPricing createChangeKitPricing() {
        return new ChangeKitPricing();
    }

    /**
     * Create an instance of {@link ValidateSNs }
     * 
     */
    public ValidateSNs createValidateSNs() {
        return new ValidateSNs();
    }

    /**
     * Create an instance of {@link GetNewOrderDtlResponse }
     * 
     */
    public GetNewOrderDtlResponse createGetNewOrderDtlResponse() {
        return new GetNewOrderDtlResponse();
    }

    /**
     * Create an instance of {@link ChangeODtlWarehouseResponse }
     * 
     */
    public ChangeODtlWarehouseResponse createChangeODtlWarehouseResponse() {
        return new ChangeODtlWarehouseResponse();
    }

    /**
     * Create an instance of {@link CheckOrderLinkToInterCompanyPO }
     * 
     */
    public CheckOrderLinkToInterCompanyPO createCheckOrderLinkToInterCompanyPO() {
        return new CheckOrderLinkToInterCompanyPO();
    }

    /**
     * Create an instance of {@link ChangeERSOrder }
     * 
     */
    public ChangeERSOrder createChangeERSOrder() {
        return new ChangeERSOrder();
    }

    /**
     * Create an instance of {@link CheckOrderHedDispatchReasonResponse }
     * 
     */
    public CheckOrderHedDispatchReasonResponse createCheckOrderHedDispatchReasonResponse() {
        return new CheckOrderHedDispatchReasonResponse();
    }

    /**
     * Create an instance of {@link CheckSellingFactorDirectionResponse }
     * 
     */
    public CheckSellingFactorDirectionResponse createCheckSellingFactorDirectionResponse() {
        return new CheckSellingFactorDirectionResponse();
    }

    /**
     * Create an instance of {@link GetGlbSugPOChgResponse }
     * 
     */
    public GetGlbSugPOChgResponse createGetGlbSugPOChgResponse() {
        return new GetGlbSugPOChgResponse();
    }

    /**
     * Create an instance of {@link GetJobProd }
     * 
     */
    public GetJobProd createGetJobProd() {
        return new GetJobProd();
    }

    /**
     * Create an instance of {@link ChangeRevNumMaster }
     * 
     */
    public ChangeRevNumMaster createChangeRevNumMaster() {
        return new ChangeRevNumMaster();
    }

    /**
     * Create an instance of {@link CopyOrderResponse }
     * 
     */
    public CopyOrderResponse createCopyOrderResponse() {
        return new CopyOrderResponse();
    }

    /**
     * Create an instance of {@link UpdateOrderDtlDiscountPercentResponse }
     * 
     */
    public UpdateOrderDtlDiscountPercentResponse createUpdateOrderDtlDiscountPercentResponse() {
        return new UpdateOrderDtlDiscountPercentResponse();
    }

    /**
     * Create an instance of {@link GetNewOHOrderMsc }
     * 
     */
    public GetNewOHOrderMsc createGetNewOHOrderMsc() {
        return new GetNewOHOrderMsc();
    }

    /**
     * Create an instance of {@link ChangeQuickEntryOption }
     * 
     */
    public ChangeQuickEntryOption createChangeQuickEntryOption() {
        return new ChangeQuickEntryOption();
    }

    /**
     * Create an instance of {@link ChangeSellingReqQtyResponse }
     * 
     */
    public ChangeSellingReqQtyResponse createChangeSellingReqQtyResponse() {
        return new ChangeSellingReqQtyResponse();
    }

    /**
     * Create an instance of {@link GlbSugPOChgDelete }
     * 
     */
    public GlbSugPOChgDelete createGlbSugPOChgDelete() {
        return new GlbSugPOChgDelete();
    }

    /**
     * Create an instance of {@link ReopenRelease }
     * 
     */
    public ReopenRelease createReopenRelease() {
        return new ReopenRelease();
    }

    /**
     * Create an instance of {@link GetPayBTFlagDefaults }
     * 
     */
    public GetPayBTFlagDefaults createGetPayBTFlagDefaults() {
        return new GetPayBTFlagDefaults();
    }

    /**
     * Create an instance of {@link ChangeOrderRelMFCustIDResponse }
     * 
     */
    public ChangeOrderRelMFCustIDResponse createChangeOrderRelMFCustIDResponse() {
        return new ChangeOrderRelMFCustIDResponse();
    }

    /**
     * Create an instance of {@link GetSelectSerialNumbersParamsResponse }
     * 
     */
    public GetSelectSerialNumbersParamsResponse createGetSelectSerialNumbersParamsResponse() {
        return new GetSelectSerialNumbersParamsResponse();
    }

    /**
     * Create an instance of {@link GetListResponse }
     * 
     */
    public GetListResponse createGetListResponse() {
        return new GetListResponse();
    }

    /**
     * Create an instance of {@link ChkLtrOfCrdt }
     * 
     */
    public ChkLtrOfCrdt createChkLtrOfCrdt() {
        return new ChkLtrOfCrdt();
    }

    /**
     * Create an instance of {@link OnChangeofTaxRgnResponse }
     * 
     */
    public OnChangeofTaxRgnResponse createOnChangeofTaxRgnResponse() {
        return new OnChangeofTaxRgnResponse();
    }

    /**
     * Create an instance of {@link ChangeMiscAmount }
     * 
     */
    public ChangeMiscAmount createChangeMiscAmount() {
        return new ChangeMiscAmount();
    }

    /**
     * Create an instance of {@link ChangeRenewalNbrMasterResponse }
     * 
     */
    public ChangeRenewalNbrMasterResponse createChangeRenewalNbrMasterResponse() {
        return new ChangeRenewalNbrMasterResponse();
    }

    /**
     * Create an instance of {@link CheckCustOnCreditHoldResponse }
     * 
     */
    public CheckCustOnCreditHoldResponse createCheckCustOnCreditHoldResponse() {
        return new CheckCustOnCreditHoldResponse();
    }

    /**
     * Create an instance of {@link ChangeMiscCode }
     * 
     */
    public ChangeMiscCode createChangeMiscCode() {
        return new ChangeMiscCode();
    }

    /**
     * Create an instance of {@link ChangeHedUseOTSResponse }
     * 
     */
    public ChangeHedUseOTSResponse createChangeHedUseOTSResponse() {
        return new ChangeHedUseOTSResponse();
    }

    /**
     * Create an instance of {@link ChangeSoldToID }
     * 
     */
    public ChangeSoldToID createChangeSoldToID() {
        return new ChangeSoldToID();
    }

    /**
     * Create an instance of {@link SendLinkedICPOSuggestionResponse }
     * 
     */
    public SendLinkedICPOSuggestionResponse createSendLinkedICPOSuggestionResponse() {
        return new SendLinkedICPOSuggestionResponse();
    }

    /**
     * Create an instance of {@link CheckCustOnCreditHold }
     * 
     */
    public CheckCustOnCreditHold createCheckCustOnCreditHold() {
        return new CheckCustOnCreditHold();
    }

    /**
     * Create an instance of {@link RebuildShipUPSResponse }
     * 
     */
    public RebuildShipUPSResponse createRebuildShipUPSResponse() {
        return new RebuildShipUPSResponse();
    }

    /**
     * Create an instance of {@link CheckSONum }
     * 
     */
    public CheckSONum createCheckSONum() {
        return new CheckSONum();
    }

    /**
     * Create an instance of {@link GetRows }
     * 
     */
    public GetRows createGetRows() {
        return new GetRows();
    }

    /**
     * Create an instance of {@link GetUIParams }
     * 
     */
    public GetUIParams createGetUIParams() {
        return new GetUIParams();
    }

    /**
     * Create an instance of {@link MasterUpdateResponse }
     * 
     */
    public MasterUpdateResponse createMasterUpdateResponse() {
        return new MasterUpdateResponse();
    }

    /**
     * Create an instance of {@link OnChangeOfTaxPercent }
     * 
     */
    public OnChangeOfTaxPercent createOnChangeOfTaxPercent() {
        return new OnChangeOfTaxPercent();
    }

    /**
     * Create an instance of {@link CheckContractNum }
     * 
     */
    public CheckContractNum createCheckContractNum() {
        return new CheckContractNum();
    }

    /**
     * Create an instance of {@link CheckCustomerCreditLimitResponse }
     * 
     */
    public CheckCustomerCreditLimitResponse createCheckCustomerCreditLimitResponse() {
        return new CheckCustomerCreditLimitResponse();
    }

    /**
     * Create an instance of {@link GetByIDLinkedOrder }
     * 
     */
    public GetByIDLinkedOrder createGetByIDLinkedOrder() {
        return new GetByIDLinkedOrder();
    }

    /**
     * Create an instance of {@link ChangeCCAmountsResponse }
     * 
     */
    public ChangeCCAmountsResponse createChangeCCAmountsResponse() {
        return new ChangeCCAmountsResponse();
    }

    /**
     * Create an instance of {@link ChangeXPartNumResponse }
     * 
     */
    public ChangeXPartNumResponse createChangeXPartNumResponse() {
        return new ChangeXPartNumResponse();
    }

    /**
     * Create an instance of {@link OnChangeofPhaseID }
     * 
     */
    public OnChangeofPhaseID createOnChangeofPhaseID() {
        return new OnChangeofPhaseID();
    }

    /**
     * Create an instance of {@link CCProcessCard }
     * 
     */
    public CCProcessCard createCCProcessCard() {
        return new CCProcessCard();
    }

    /**
     * Create an instance of {@link ChangePartNum }
     * 
     */
    public ChangePartNum createChangePartNum() {
        return new ChangePartNum();
    }

    /**
     * Create an instance of {@link ChangeQuoteQtyNum }
     * 
     */
    public ChangeQuoteQtyNum createChangeQuoteQtyNum() {
        return new ChangeQuoteQtyNum();
    }

    /**
     * Create an instance of {@link ChangeSoldToContact }
     * 
     */
    public ChangeSoldToContact createChangeSoldToContact() {
        return new ChangeSoldToContact();
    }

    /**
     * Create an instance of {@link IsRunOutOnHandResponse }
     * 
     */
    public IsRunOutOnHandResponse createIsRunOutOnHandResponse() {
        return new IsRunOutOnHandResponse();
    }

    /**
     * Create an instance of {@link CheckConfigurationResponse }
     * 
     */
    public CheckConfigurationResponse createCheckConfigurationResponse() {
        return new CheckConfigurationResponse();
    }

    /**
     * Create an instance of {@link GlbSugPOChgUpdateResponse }
     * 
     */
    public GlbSugPOChgUpdateResponse createGlbSugPOChgUpdateResponse() {
        return new GlbSugPOChgUpdateResponse();
    }

    /**
     * Create an instance of {@link ChangeGroupResponse }
     * 
     */
    public ChangeGroupResponse createChangeGroupResponse() {
        return new ChangeGroupResponse();
    }

    /**
     * Create an instance of {@link GetNewSalesKit }
     * 
     */
    public GetNewSalesKit createGetNewSalesKit() {
        return new GetNewSalesKit();
    }

    /**
     * Create an instance of {@link ChangeCustomer }
     * 
     */
    public ChangeCustomer createChangeCustomer() {
        return new ChangeCustomer();
    }

    /**
     * Create an instance of {@link RebuildShipUPS }
     * 
     */
    public RebuildShipUPS createRebuildShipUPS() {
        return new RebuildShipUPS();
    }

    /**
     * Create an instance of {@link CheckKitRevisionResponse }
     * 
     */
    public CheckKitRevisionResponse createCheckKitRevisionResponse() {
        return new CheckKitRevisionResponse();
    }

    /**
     * Create an instance of {@link CheckICPO }
     * 
     */
    public CheckICPO createCheckICPO() {
        return new CheckICPO();
    }

    /**
     * Create an instance of {@link ChangeQuickEntryOptionResponse }
     * 
     */
    public ChangeQuickEntryOptionResponse createChangeQuickEntryOptionResponse() {
        return new ChangeQuickEntryOptionResponse();
    }

    /**
     * Create an instance of {@link ChangeOrderRelShipTo }
     * 
     */
    public ChangeOrderRelShipTo createChangeOrderRelShipTo() {
        return new ChangeOrderRelShipTo();
    }

    /**
     * Create an instance of {@link ChangeQuoteQtyNumResponse }
     * 
     */
    public ChangeQuoteQtyNumResponse createChangeQuoteQtyNumResponse() {
        return new ChangeQuoteQtyNumResponse();
    }

    /**
     * Create an instance of {@link ChangePartNumMasterResponse }
     * 
     */
    public ChangePartNumMasterResponse createChangePartNumMasterResponse() {
        return new ChangePartNumMasterResponse();
    }

    /**
     * Create an instance of {@link GetNewOHOrderMscResponse }
     * 
     */
    public GetNewOHOrderMscResponse createGetNewOHOrderMscResponse() {
        return new GetNewOHOrderMscResponse();
    }

    /**
     * Create an instance of {@link ChangeMiscPercent }
     * 
     */
    public ChangeMiscPercent createChangeMiscPercent() {
        return new ChangeMiscPercent();
    }

    /**
     * Create an instance of {@link ChangeNewSellingQuantity }
     * 
     */
    public ChangeNewSellingQuantity createChangeNewSellingQuantity() {
        return new ChangeNewSellingQuantity();
    }

    /**
     * Create an instance of {@link ChangeShipToCustID }
     * 
     */
    public ChangeShipToCustID createChangeShipToCustID() {
        return new ChangeShipToCustID();
    }

    /**
     * Create an instance of {@link GetCodeDescList }
     * 
     */
    public GetCodeDescList createGetCodeDescList() {
        return new GetCodeDescList();
    }

    /**
     * Create an instance of {@link ChangeWhseCodeMaster }
     * 
     */
    public ChangeWhseCodeMaster createChangeWhseCodeMaster() {
        return new ChangeWhseCodeMaster();
    }

    /**
     * Create an instance of {@link CreateLinesFromHistory }
     * 
     */
    public CreateLinesFromHistory createCreateLinesFromHistory() {
        return new CreateLinesFromHistory();
    }

    /**
     * Create an instance of {@link GetPlantConfCtrlInfoResponse }
     * 
     */
    public GetPlantConfCtrlInfoResponse createGetPlantConfCtrlInfoResponse() {
        return new GetPlantConfCtrlInfoResponse();
    }

    /**
     * Create an instance of {@link CloseOrderResponse }
     * 
     */
    public CloseOrderResponse createCloseOrderResponse() {
        return new CloseOrderResponse();
    }

    /**
     * Create an instance of {@link ChangeShipToIDResponse }
     * 
     */
    public ChangeShipToIDResponse createChangeShipToIDResponse() {
        return new ChangeShipToIDResponse();
    }

    /**
     * Create an instance of {@link GetByID }
     * 
     */
    public GetByID createGetByID() {
        return new GetByID();
    }

    /**
     * Create an instance of {@link GetBasePartForConfigResponse }
     * 
     */
    public GetBasePartForConfigResponse createGetBasePartForConfigResponse() {
        return new GetBasePartForConfigResponse();
    }

    /**
     * Create an instance of {@link GetRowsCustomerTracker }
     * 
     */
    public GetRowsCustomerTracker createGetRowsCustomerTracker() {
        return new GetRowsCustomerTracker();
    }

    /**
     * Create an instance of {@link SetCalcQtysPrefResponse }
     * 
     */
    public SetCalcQtysPrefResponse createSetCalcQtysPrefResponse() {
        return new SetCalcQtysPrefResponse();
    }

    /**
     * Create an instance of {@link GetUIParamsResponse }
     * 
     */
    public GetUIParamsResponse createGetUIParamsResponse() {
        return new GetUIParamsResponse();
    }

    /**
     * Create an instance of {@link CloseReleaseResponse }
     * 
     */
    public CloseReleaseResponse createCloseReleaseResponse() {
        return new CloseReleaseResponse();
    }

    /**
     * Create an instance of {@link OnChangeOfMktgCampaignResponse }
     * 
     */
    public OnChangeOfMktgCampaignResponse createOnChangeOfMktgCampaignResponse() {
        return new OnChangeOfMktgCampaignResponse();
    }

    /**
     * Create an instance of {@link ChangeOrderRelShipToCustID }
     * 
     */
    public ChangeOrderRelShipToCustID createChangeOrderRelShipToCustID() {
        return new ChangeOrderRelShipToCustID();
    }

    /**
     * Create an instance of {@link ProcessCounterSale }
     * 
     */
    public ProcessCounterSale createProcessCounterSale() {
        return new ProcessCounterSale();
    }

    /**
     * Create an instance of {@link ChangeShipToCustIDResponse }
     * 
     */
    public ChangeShipToCustIDResponse createChangeShipToCustIDResponse() {
        return new ChangeShipToCustIDResponse();
    }

    /**
     * Create an instance of {@link UpdateExtResponse }
     * 
     */
    public UpdateExtResponse createUpdateExtResponse() {
        return new UpdateExtResponse();
    }

    /**
     * Create an instance of {@link GetNewOrderHedAttch }
     * 
     */
    public GetNewOrderHedAttch createGetNewOrderHedAttch() {
        return new GetNewOrderHedAttch();
    }

    /**
     * Create an instance of {@link CheckLtrOfCrdtResponse }
     * 
     */
    public CheckLtrOfCrdtResponse createCheckLtrOfCrdtResponse() {
        return new CheckLtrOfCrdtResponse();
    }

    /**
     * Create an instance of {@link SetReadyToCalc }
     * 
     */
    public SetReadyToCalc createSetReadyToCalc() {
        return new SetReadyToCalc();
    }

    /**
     * Create an instance of {@link GetBySysRowIDsResponse }
     * 
     */
    public GetBySysRowIDsResponse createGetBySysRowIDsResponse() {
        return new GetBySysRowIDsResponse();
    }

    /**
     * Create an instance of {@link GetNewOrderDtl }
     * 
     */
    public GetNewOrderDtl createGetNewOrderDtl() {
        return new GetNewOrderDtl();
    }

    /**
     * Create an instance of {@link InvoiceExistsResponse }
     * 
     */
    public InvoiceExistsResponse createInvoiceExistsResponse() {
        return new InvoiceExistsResponse();
    }

    /**
     * Create an instance of {@link ChangeOrderRelShipToResponse }
     * 
     */
    public ChangeOrderRelShipToResponse createChangeOrderRelShipToResponse() {
        return new ChangeOrderRelShipToResponse();
    }

    /**
     * Create an instance of {@link ChangeORelWarehouse }
     * 
     */
    public ChangeORelWarehouse createChangeORelWarehouse() {
        return new ChangeORelWarehouse();
    }

    /**
     * Create an instance of {@link ChangeRevisionNumResponse }
     * 
     */
    public ChangeRevisionNumResponse createChangeRevisionNumResponse() {
        return new ChangeRevisionNumResponse();
    }

    /**
     * Create an instance of {@link ChangeCCAmounts }
     * 
     */
    public ChangeCCAmounts createChangeCCAmounts() {
        return new ChangeCCAmounts();
    }

    /**
     * Create an instance of {@link ChangeContractNumMasterResponse }
     * 
     */
    public ChangeContractNumMasterResponse createChangeContractNumMasterResponse() {
        return new ChangeContractNumMasterResponse();
    }

    /**
     * Create an instance of {@link OverCreditLimitUpdateAllowedResponse }
     * 
     */
    public OverCreditLimitUpdateAllowedResponse createOverCreditLimitUpdateAllowedResponse() {
        return new OverCreditLimitUpdateAllowedResponse();
    }

    /**
     * Create an instance of {@link ChangeCurrencyCode }
     * 
     */
    public ChangeCurrencyCode createChangeCurrencyCode() {
        return new ChangeCurrencyCode();
    }

    /**
     * Create an instance of {@link ChangeUnitPriceResponse }
     * 
     */
    public ChangeUnitPriceResponse createChangeUnitPriceResponse() {
        return new ChangeUnitPriceResponse();
    }

    /**
     * Create an instance of {@link ChkLtrOfCrdtResponse }
     * 
     */
    public ChkLtrOfCrdtResponse createChkLtrOfCrdtResponse() {
        return new ChkLtrOfCrdtResponse();
    }

    /**
     * Create an instance of {@link DeleteByIDResponse }
     * 
     */
    public DeleteByIDResponse createDeleteByIDResponse() {
        return new DeleteByIDResponse();
    }

    /**
     * Create an instance of {@link OnChangeOfTaxAmtResponse }
     * 
     */
    public OnChangeOfTaxAmtResponse createOnChangeOfTaxAmtResponse() {
        return new OnChangeOfTaxAmtResponse();
    }

    /**
     * Create an instance of {@link ChangeRelUseOTSResponse }
     * 
     */
    public ChangeRelUseOTSResponse createChangeRelUseOTSResponse() {
        return new ChangeRelUseOTSResponse();
    }

    /**
     * Create an instance of {@link ChangeSalesUOM }
     * 
     */
    public ChangeSalesUOM createChangeSalesUOM() {
        return new ChangeSalesUOM();
    }

    /**
     * Create an instance of {@link CreateOrderDtlComplements }
     * 
     */
    public CreateOrderDtlComplements createCreateOrderDtlComplements() {
        return new CreateOrderDtlComplements();
    }

    /**
     * Create an instance of {@link ValidateInvQty }
     * 
     */
    public ValidateInvQty createValidateInvQty() {
        return new ValidateInvQty();
    }

    /**
     * Create an instance of {@link ChangeCardNumberResponse }
     * 
     */
    public ChangeCardNumberResponse createChangeCardNumberResponse() {
        return new ChangeCardNumberResponse();
    }

    /**
     * Create an instance of {@link ChangeWhseCodeMasterResponse }
     * 
     */
    public ChangeWhseCodeMasterResponse createChangeWhseCodeMasterResponse() {
        return new ChangeWhseCodeMasterResponse();
    }

    /**
     * Create an instance of {@link ChangeContractNum }
     * 
     */
    public ChangeContractNum createChangeContractNum() {
        return new ChangeContractNum();
    }

    /**
     * Create an instance of {@link UpdateExistingOrder }
     * 
     */
    public UpdateExistingOrder createUpdateExistingOrder() {
        return new UpdateExistingOrder();
    }

    /**
     * Create an instance of {@link GetBySysRowID }
     * 
     */
    public GetBySysRowID createGetBySysRowID() {
        return new GetBySysRowID();
    }

    /**
     * Create an instance of {@link ChangeRMANumResponse }
     * 
     */
    public ChangeRMANumResponse createChangeRMANumResponse() {
        return new ChangeRMANumResponse();
    }

    /**
     * Create an instance of {@link CheckComplianceFailResponse }
     * 
     */
    public CheckComplianceFailResponse createCheckComplianceFailResponse() {
        return new CheckComplianceFailResponse();
    }

    /**
     * Create an instance of {@link CheckConfiguration }
     * 
     */
    public CheckConfiguration createCheckConfiguration() {
        return new CheckConfiguration();
    }

    /**
     * Create an instance of {@link ChangeOrderRelMarkForNum }
     * 
     */
    public ChangeOrderRelMarkForNum createChangeOrderRelMarkForNum() {
        return new ChangeOrderRelMarkForNum();
    }

    /**
     * Create an instance of {@link ReopenOrderLine }
     * 
     */
    public ReopenOrderLine createReopenOrderLine() {
        return new ReopenOrderLine();
    }

    /**
     * Create an instance of {@link ChangeOverridePriceListResponse }
     * 
     */
    public ChangeOverridePriceListResponse createChangeOverridePriceListResponse() {
        return new ChangeOverridePriceListResponse();
    }

    /**
     * Create an instance of {@link ChangeRevNumMasterResponse }
     * 
     */
    public ChangeRevNumMasterResponse createChangeRevNumMasterResponse() {
        return new ChangeRevNumMasterResponse();
    }

    /**
     * Create an instance of {@link CopyOrder }
     * 
     */
    public CopyOrder createCopyOrder() {
        return new CopyOrder();
    }

    /**
     * Create an instance of {@link OnChangeCreditCardOrderResponse }
     * 
     */
    public OnChangeCreditCardOrderResponse createOnChangeCreditCardOrderResponse() {
        return new OnChangeCreditCardOrderResponse();
    }

    /**
     * Create an instance of {@link GetSmartString }
     * 
     */
    public GetSmartString createGetSmartString() {
        return new GetSmartString();
    }

    /**
     * Create an instance of {@link ChangeOrderRelShipToContact }
     * 
     */
    public ChangeOrderRelShipToContact createChangeOrderRelShipToContact() {
        return new ChangeOrderRelShipToContact();
    }

    /**
     * Create an instance of {@link ChangeKitQtyPer }
     * 
     */
    public ChangeKitQtyPer createChangeKitQtyPer() {
        return new ChangeKitQtyPer();
    }

    /**
     * Create an instance of {@link OnChangeOfFixedAmount }
     * 
     */
    public OnChangeOfFixedAmount createOnChangeOfFixedAmount() {
        return new OnChangeOfFixedAmount();
    }

    /**
     * Create an instance of {@link ChangeOrderRelUseOTMF }
     * 
     */
    public ChangeOrderRelUseOTMF createChangeOrderRelUseOTMF() {
        return new ChangeOrderRelUseOTMF();
    }

    /**
     * Create an instance of {@link GetNewOrderHedUPSResponse }
     * 
     */
    public GetNewOrderHedUPSResponse createGetNewOrderHedUPSResponse() {
        return new GetNewOrderHedUPSResponse();
    }

    /**
     * Create an instance of {@link RemoveICPOLink }
     * 
     */
    public RemoveICPOLink createRemoveICPOLink() {
        return new RemoveICPOLink();
    }

    /**
     * Create an instance of {@link CreateGlbSugPOChgResponse }
     * 
     */
    public CreateGlbSugPOChgResponse createCreateGlbSugPOChgResponse() {
        return new CreateGlbSugPOChgResponse();
    }

    /**
     * Create an instance of {@link ChangeSellingQtyMaster }
     * 
     */
    public ChangeSellingQtyMaster createChangeSellingQtyMaster() {
        return new ChangeSellingQtyMaster();
    }

    /**
     * Create an instance of {@link GetNewOrderHedResponse }
     * 
     */
    public GetNewOrderHedResponse createGetNewOrderHedResponse() {
        return new GetNewOrderHedResponse();
    }

    /**
     * Create an instance of {@link ChangeShipToContact }
     * 
     */
    public ChangeShipToContact createChangeShipToContact() {
        return new ChangeShipToContact();
    }

    /**
     * Create an instance of {@link CheckCustomerCreditLimit }
     * 
     */
    public CheckCustomerCreditLimit createCheckCustomerCreditLimit() {
        return new CheckCustomerCreditLimit();
    }

    /**
     * Create an instance of {@link CheckQuoteLinesNoQuantityResponse }
     * 
     */
    public CheckQuoteLinesNoQuantityResponse createCheckQuoteLinesNoQuantityResponse() {
        return new CheckQuoteLinesNoQuantityResponse();
    }

    /**
     * Create an instance of {@link ReopenReleaseResponse }
     * 
     */
    public ReopenReleaseResponse createReopenReleaseResponse() {
        return new ReopenReleaseResponse();
    }

    /**
     * Create an instance of {@link CCLoadTranData }
     * 
     */
    public CCLoadTranData createCCLoadTranData() {
        return new CCLoadTranData();
    }

    /**
     * Create an instance of {@link GetNewOrderHed }
     * 
     */
    public GetNewOrderHed createGetNewOrderHed() {
        return new GetNewOrderHed();
    }

    /**
     * Create an instance of {@link ApplyOrderBasedDiscounts }
     * 
     */
    public ApplyOrderBasedDiscounts createApplyOrderBasedDiscounts() {
        return new ApplyOrderBasedDiscounts();
    }

    /**
     * Create an instance of {@link CheckICPOResponse }
     * 
     */
    public CheckICPOResponse createCheckICPOResponse() {
        return new CheckICPOResponse();
    }

    /**
     * Create an instance of {@link ChangePricePerCodeResponse }
     * 
     */
    public ChangePricePerCodeResponse createChangePricePerCodeResponse() {
        return new ChangePricePerCodeResponse();
    }

    /**
     * Create an instance of {@link ETCValidateAddress }
     * 
     */
    public ETCValidateAddress createETCValidateAddress() {
        return new ETCValidateAddress();
    }

    /**
     * Create an instance of {@link ChangeOrderRelBuyToOrder }
     * 
     */
    public ChangeOrderRelBuyToOrder createChangeOrderRelBuyToOrder() {
        return new ChangeOrderRelBuyToOrder();
    }

    /**
     * Create an instance of {@link UpdateExistingOrderResponse }
     * 
     */
    public UpdateExistingOrderResponse createUpdateExistingOrderResponse() {
        return new UpdateExistingOrderResponse();
    }

    /**
     * Create an instance of {@link ChangeCardNumber }
     * 
     */
    public ChangeCardNumber createChangeCardNumber() {
        return new ChangeCardNumber();
    }

    /**
     * Create an instance of {@link ChangeORelWarehouseResponse }
     * 
     */
    public ChangeORelWarehouseResponse createChangeORelWarehouseResponse() {
        return new ChangeORelWarehouseResponse();
    }

    /**
     * Create an instance of {@link ChangeContractNumMaster }
     * 
     */
    public ChangeContractNumMaster createChangeContractNumMaster() {
        return new ChangeContractNumMaster();
    }

    /**
     * Create an instance of {@link ChangeNewNeedByDate }
     * 
     */
    public ChangeNewNeedByDate createChangeNewNeedByDate() {
        return new ChangeNewNeedByDate();
    }

    /**
     * Create an instance of {@link ChangeCommissionableResponse }
     * 
     */
    public ChangeCommissionableResponse createChangeCommissionableResponse() {
        return new ChangeCommissionableResponse();
    }

    /**
     * Create an instance of {@link GetJobProdResponse }
     * 
     */
    public GetJobProdResponse createGetJobProdResponse() {
        return new GetJobProdResponse();
    }

    /**
     * Create an instance of {@link GetQuoteQty }
     * 
     */
    public GetQuoteQty createGetQuoteQty() {
        return new GetQuoteQty();
    }

    /**
     * Create an instance of {@link ChangePartNumMaster }
     * 
     */
    public ChangePartNumMaster createChangePartNumMaster() {
        return new ChangePartNumMaster();
    }

    /**
     * Create an instance of {@link GetSmartStringResponse }
     * 
     */
    public GetSmartStringResponse createGetSmartStringResponse() {
        return new GetSmartStringResponse();
    }

    /**
     * Create an instance of {@link MasterUpdate }
     * 
     */
    public MasterUpdate createMasterUpdate() {
        return new MasterUpdate();
    }

    /**
     * Create an instance of {@link OrderDtlGetNewCounterSale }
     * 
     */
    public OrderDtlGetNewCounterSale createOrderDtlGetNewCounterSale() {
        return new OrderDtlGetNewCounterSale();
    }

    /**
     * Create an instance of {@link ChangePricePerCode }
     * 
     */
    public ChangePricePerCode createChangePricePerCode() {
        return new ChangePricePerCode();
    }

    /**
     * Create an instance of {@link ChangeRenewalNbrResponse }
     * 
     */
    public ChangeRenewalNbrResponse createChangeRenewalNbrResponse() {
        return new ChangeRenewalNbrResponse();
    }

    /**
     * Create an instance of {@link UpdateExt }
     * 
     */
    public UpdateExt createUpdateExt() {
        return new UpdateExt();
    }

    /**
     * Create an instance of {@link RecalcKitPriceAfterConfigResponse }
     * 
     */
    public RecalcKitPriceAfterConfigResponse createRecalcKitPriceAfterConfigResponse() {
        return new RecalcKitPriceAfterConfigResponse();
    }

    /**
     * Create an instance of {@link ChangeMiscCodeResponse }
     * 
     */
    public ChangeMiscCodeResponse createChangeMiscCodeResponse() {
        return new ChangeMiscCodeResponse();
    }

    /**
     * Create an instance of {@link CCLoadTranDataResponse }
     * 
     */
    public CCLoadTranDataResponse createCCLoadTranDataResponse() {
        return new CCLoadTranDataResponse();
    }

    /**
     * Create an instance of {@link KitCompPartCreate }
     * 
     */
    public KitCompPartCreate createKitCompPartCreate() {
        return new KitCompPartCreate();
    }

    /**
     * Create an instance of {@link CheckSellingQuantityChange }
     * 
     */
    public CheckSellingQuantityChange createCheckSellingQuantityChange() {
        return new CheckSellingQuantityChange();
    }

    /**
     * Create an instance of {@link ChangeRevisionNum }
     * 
     */
    public ChangeRevisionNum createChangeRevisionNum() {
        return new ChangeRevisionNum();
    }

    /**
     * Create an instance of {@link CheckCustomerCreditReleaseResponse }
     * 
     */
    public CheckCustomerCreditReleaseResponse createCheckCustomerCreditReleaseResponse() {
        return new CheckCustomerCreditReleaseResponse();
    }

    /**
     * Create an instance of {@link REVISIONHISTORY06 }
     * 
     */
    public REVISIONHISTORY06 createREVISIONHISTORY06() {
        return new REVISIONHISTORY06();
    }

    /**
     * Create an instance of {@link ChangedCardNumber }
     * 
     */
    public ChangedCardNumber createChangedCardNumber() {
        return new ChangedCardNumber();
    }

    /**
     * Create an instance of {@link GlbSugPOChgDeleteResponse }
     * 
     */
    public GlbSugPOChgDeleteResponse createGlbSugPOChgDeleteResponse() {
        return new GlbSugPOChgDeleteResponse();
    }

    /**
     * Create an instance of {@link SetReadyToCalcResponse }
     * 
     */
    public SetReadyToCalcResponse createSetReadyToCalcResponse() {
        return new SetReadyToCalcResponse();
    }

    /**
     * Create an instance of {@link CheckMakeDirectReleases }
     * 
     */
    public CheckMakeDirectReleases createCheckMakeDirectReleases() {
        return new CheckMakeDirectReleases();
    }

    /**
     * Create an instance of {@link CloseOrderLine }
     * 
     */
    public CloseOrderLine createCloseOrderLine() {
        return new CloseOrderLine();
    }

    /**
     * Create an instance of {@link ChangeShipToID }
     * 
     */
    public ChangeShipToID createChangeShipToID() {
        return new ChangeShipToID();
    }

    /**
     * Create an instance of {@link CheckICPOReadyToSendResponse }
     * 
     */
    public CheckICPOReadyToSendResponse createCheckICPOReadyToSendResponse() {
        return new CheckICPOReadyToSendResponse();
    }

    /**
     * Create an instance of {@link ChangeSellingQtyMasterResponse }
     * 
     */
    public ChangeSellingQtyMasterResponse createChangeSellingQtyMasterResponse() {
        return new ChangeSellingQtyMasterResponse();
    }

    /**
     * Create an instance of {@link PhantomComponents }
     * 
     */
    public PhantomComponents createPhantomComponents() {
        return new PhantomComponents();
    }

    /**
     * Create an instance of {@link ChangeBTCustIDMasterResponse }
     * 
     */
    public ChangeBTCustIDMasterResponse createChangeBTCustIDMasterResponse() {
        return new ChangeBTCustIDMasterResponse();
    }

    /**
     * Create an instance of {@link ChangeQuoteLineResponse }
     * 
     */
    public ChangeQuoteLineResponse createChangeQuoteLineResponse() {
        return new ChangeQuoteLineResponse();
    }

    /**
     * Create an instance of {@link ChangeCreditExp }
     * 
     */
    public ChangeCreditExp createChangeCreditExp() {
        return new ChangeCreditExp();
    }

    /**
     * Create an instance of {@link ChangeOrderRelShipToCustIDResponse }
     * 
     */
    public ChangeOrderRelShipToCustIDResponse createChangeOrderRelShipToCustIDResponse() {
        return new ChangeOrderRelShipToCustIDResponse();
    }

    /**
     * Create an instance of {@link ChangePartNumResponse }
     * 
     */
    public ChangePartNumResponse createChangePartNumResponse() {
        return new ChangePartNumResponse();
    }

    /**
     * Create an instance of {@link ChangeSalesRep }
     * 
     */
    public ChangeSalesRep createChangeSalesRep() {
        return new ChangeSalesRep();
    }

    /**
     * Create an instance of {@link ChangeNewSellingQuantityResponse }
     * 
     */
    public ChangeNewSellingQuantityResponse createChangeNewSellingQuantityResponse() {
        return new ChangeNewSellingQuantityResponse();
    }

    /**
     * Create an instance of {@link GetBySysRowIDResponse }
     * 
     */
    public GetBySysRowIDResponse createGetBySysRowIDResponse() {
        return new GetBySysRowIDResponse();
    }

    /**
     * Create an instance of {@link ChangeOrderRelVendorID }
     * 
     */
    public ChangeOrderRelVendorID createChangeOrderRelVendorID() {
        return new ChangeOrderRelVendorID();
    }

    /**
     * Create an instance of {@link CheckOrderLinkToInterCompanyPOResponse }
     * 
     */
    public CheckOrderLinkToInterCompanyPOResponse createCheckOrderLinkToInterCompanyPOResponse() {
        return new CheckOrderLinkToInterCompanyPOResponse();
    }

    /**
     * Create an instance of {@link HasMultipleSubs }
     * 
     */
    public HasMultipleSubs createHasMultipleSubs() {
        return new HasMultipleSubs();
    }

    /**
     * Create an instance of {@link CloseOrder }
     * 
     */
    public CloseOrder createCloseOrder() {
        return new CloseOrder();
    }

    /**
     * Create an instance of {@link HasMultipleSubsResponse }
     * 
     */
    public HasMultipleSubsResponse createHasMultipleSubsResponse() {
        return new HasMultipleSubsResponse();
    }

    /**
     * Create an instance of {@link GetCodeDescListResponse }
     * 
     */
    public GetCodeDescListResponse createGetCodeDescListResponse() {
        return new GetCodeDescListResponse();
    }

    /**
     * Create an instance of {@link SetUPSQVEnableResponse }
     * 
     */
    public SetUPSQVEnableResponse createSetUPSQVEnableResponse() {
        return new SetUPSQVEnableResponse();
    }

    /**
     * Create an instance of {@link SendLinkedICPOSuggestion }
     * 
     */
    public SendLinkedICPOSuggestion createSendLinkedICPOSuggestion() {
        return new SendLinkedICPOSuggestion();
    }

    /**
     * Create an instance of {@link ChangeSellingReqQty }
     * 
     */
    public ChangeSellingReqQty createChangeSellingReqQty() {
        return new ChangeSellingReqQty();
    }

    /**
     * Create an instance of {@link ChangeKitPricingResponse }
     * 
     */
    public ChangeKitPricingResponse createChangeKitPricingResponse() {
        return new ChangeKitPricingResponse();
    }

    /**
     * Create an instance of {@link OrderDtlGetNewCounterSaleResponse }
     * 
     */
    public OrderDtlGetNewCounterSaleResponse createOrderDtlGetNewCounterSaleResponse() {
        return new OrderDtlGetNewCounterSaleResponse();
    }

    /**
     * Create an instance of {@link ChangeCustomerResponse }
     * 
     */
    public ChangeCustomerResponse createChangeCustomerResponse() {
        return new ChangeCustomerResponse();
    }

    /**
     * Create an instance of {@link GetNewOrderMsc }
     * 
     */
    public GetNewOrderMsc createGetNewOrderMsc() {
        return new GetNewOrderMsc();
    }

    /**
     * Create an instance of {@link GetNewOrderRelResponse }
     * 
     */
    public GetNewOrderRelResponse createGetNewOrderRelResponse() {
        return new GetNewOrderRelResponse();
    }

    /**
     * Create an instance of {@link ChangeDiscountPercent }
     * 
     */
    public ChangeDiscountPercent createChangeDiscountPercent() {
        return new ChangeDiscountPercent();
    }

    /**
     * Create an instance of {@link CheckProjectID }
     * 
     */
    public CheckProjectID createCheckProjectID() {
        return new CheckProjectID();
    }

    /**
     * Create an instance of {@link GetNewOrderMscResponse }
     * 
     */
    public GetNewOrderMscResponse createGetNewOrderMscResponse() {
        return new GetNewOrderMscResponse();
    }

    /**
     * Create an instance of {@link ChangeVendorChangeResponse }
     * 
     */
    public ChangeVendorChangeResponse createChangeVendorChangeResponse() {
        return new ChangeVendorChangeResponse();
    }

    /**
     * Create an instance of {@link OnChangeTaxCode }
     * 
     */
    public OnChangeTaxCode createOnChangeTaxCode() {
        return new OnChangeTaxCode();
    }

    /**
     * Create an instance of {@link ChangeOverrideDiscPriceList }
     * 
     */
    public ChangeOverrideDiscPriceList createChangeOverrideDiscPriceList() {
        return new ChangeOverrideDiscPriceList();
    }

    /**
     * Create an instance of {@link OnChangeofSoldToCreditCheckResponse }
     * 
     */
    public OnChangeofSoldToCreditCheckResponse createOnChangeofSoldToCreditCheckResponse() {
        return new OnChangeofSoldToCreditCheckResponse();
    }

    /**
     * Create an instance of {@link RemoveICPOLinkResponse }
     * 
     */
    public RemoveICPOLinkResponse createRemoveICPOLinkResponse() {
        return new RemoveICPOLinkResponse();
    }

    /**
     * Create an instance of {@link OnChangeTaxableAmt }
     * 
     */
    public OnChangeTaxableAmt createOnChangeTaxableAmt() {
        return new OnChangeTaxableAmt();
    }

    /**
     * Create an instance of {@link ChangePriceListResponse }
     * 
     */
    public ChangePriceListResponse createChangePriceListResponse() {
        return new ChangePriceListResponse();
    }

    /**
     * Create an instance of {@link GetKitComponents }
     * 
     */
    public GetKitComponents createGetKitComponents() {
        return new GetKitComponents();
    }

    /**
     * Create an instance of {@link ChangeManualTaxCalcResponse }
     * 
     */
    public ChangeManualTaxCalcResponse createChangeManualTaxCalcResponse() {
        return new ChangeManualTaxCalcResponse();
    }

    /**
     * Create an instance of {@link ChangePlantResponse }
     * 
     */
    public ChangePlantResponse createChangePlantResponse() {
        return new ChangePlantResponse();
    }

    /**
     * Create an instance of {@link OnChangeofTaxRgn }
     * 
     */
    public OnChangeofTaxRgn createOnChangeofTaxRgn() {
        return new OnChangeofTaxRgn();
    }

    /**
     * Create an instance of {@link GetNewOrderRel }
     * 
     */
    public GetNewOrderRel createGetNewOrderRel() {
        return new GetNewOrderRel();
    }

    /**
     * Create an instance of {@link ChangeCreditExpResponse }
     * 
     */
    public ChangeCreditExpResponse createChangeCreditExpResponse() {
        return new ChangeCreditExpResponse();
    }

    /**
     * Create an instance of {@link ChangeDiscBreakListCode }
     * 
     */
    public ChangeDiscBreakListCode createChangeDiscBreakListCode() {
        return new ChangeDiscBreakListCode();
    }

    /**
     * Create an instance of {@link ChangeKitQtyPerResponse }
     * 
     */
    public ChangeKitQtyPerResponse createChangeKitQtyPerResponse() {
        return new ChangeKitQtyPerResponse();
    }

    /**
     * Create an instance of {@link CheckContractNumResponse }
     * 
     */
    public CheckContractNumResponse createCheckContractNumResponse() {
        return new CheckContractNumResponse();
    }

    /**
     * Create an instance of {@link ChangeRenewalNbr }
     * 
     */
    public ChangeRenewalNbr createChangeRenewalNbr() {
        return new ChangeRenewalNbr();
    }

    /**
     * Create an instance of {@link ChangeShipToContactResponse }
     * 
     */
    public ChangeShipToContactResponse createChangeShipToContactResponse() {
        return new ChangeShipToContactResponse();
    }

    /**
     * Create an instance of {@link CreateLineMiscChargesFromQuoteResponse }
     * 
     */
    public CreateLineMiscChargesFromQuoteResponse createCreateLineMiscChargesFromQuoteResponse() {
        return new CreateLineMiscChargesFromQuoteResponse();
    }

    /**
     * Create an instance of {@link CheckSONumResponse }
     * 
     */
    public CheckSONumResponse createCheckSONumResponse() {
        return new CheckSONumResponse();
    }

    /**
     * Create an instance of {@link OnChangeofBTConNum }
     * 
     */
    public OnChangeofBTConNum createOnChangeofBTConNum() {
        return new OnChangeofBTConNum();
    }

    /**
     * Create an instance of {@link Update }
     * 
     */
    public Update createUpdate() {
        return new Update();
    }

    /**
     * Create an instance of {@link GetPlantConfCtrlInfo }
     * 
     */
    public GetPlantConfCtrlInfo createGetPlantConfCtrlInfo() {
        return new GetPlantConfCtrlInfo();
    }

    /**
     * Create an instance of {@link ChangePlant }
     * 
     */
    public ChangePlant createChangePlant() {
        return new ChangePlant();
    }

    /**
     * Create an instance of {@link GetNewOrderRelTaxResponse }
     * 
     */
    public GetNewOrderRelTaxResponse createGetNewOrderRelTaxResponse() {
        return new GetNewOrderRelTaxResponse();
    }

    /**
     * Create an instance of {@link UpdateResponse }
     * 
     */
    public UpdateResponse createUpdateResponse() {
        return new UpdateResponse();
    }

    /**
     * Create an instance of {@link GetNewOrderDtlAttchResponse }
     * 
     */
    public GetNewOrderDtlAttchResponse createGetNewOrderDtlAttchResponse() {
        return new GetNewOrderDtlAttchResponse();
    }

    /**
     * Create an instance of {@link IsRunOutOnHand }
     * 
     */
    public IsRunOutOnHand createIsRunOutOnHand() {
        return new IsRunOutOnHand();
    }

    /**
     * Create an instance of {@link GetCalcQtyPrefResponse }
     * 
     */
    public GetCalcQtyPrefResponse createGetCalcQtyPrefResponse() {
        return new GetCalcQtyPrefResponse();
    }

    /**
     * Create an instance of {@link CheckQuoteForCreditLimit }
     * 
     */
    public CheckQuoteForCreditLimit createCheckQuoteForCreditLimit() {
        return new CheckQuoteForCreditLimit();
    }

    /**
     * Create an instance of {@link CheckRenewalNbr }
     * 
     */
    public CheckRenewalNbr createCheckRenewalNbr() {
        return new CheckRenewalNbr();
    }

    /**
     * Create an instance of {@link ChangeDiscBreakListCodeResponse }
     * 
     */
    public ChangeDiscBreakListCodeResponse createChangeDiscBreakListCodeResponse() {
        return new ChangeDiscBreakListCodeResponse();
    }

    /**
     * Create an instance of {@link CheckComplianceFail }
     * 
     */
    public CheckComplianceFail createCheckComplianceFail() {
        return new CheckComplianceFail();
    }

    /**
     * Create an instance of {@link CheckRateGrpCodeResponse }
     * 
     */
    public CheckRateGrpCodeResponse createCheckRateGrpCodeResponse() {
        return new CheckRateGrpCodeResponse();
    }

    /**
     * Create an instance of {@link GetSelectSerialNumbersParams }
     * 
     */
    public GetSelectSerialNumbersParams createGetSelectSerialNumbersParams() {
        return new GetSelectSerialNumbersParams();
    }

    /**
     * Create an instance of {@link ProcessQuickEntry }
     * 
     */
    public ProcessQuickEntry createProcessQuickEntry() {
        return new ProcessQuickEntry();
    }

    /**
     * Create an instance of {@link OnChangeOfMktgEvntResponse }
     * 
     */
    public OnChangeOfMktgEvntResponse createOnChangeOfMktgEvntResponse() {
        return new OnChangeOfMktgEvntResponse();
    }

    /**
     * Create an instance of {@link ChangeDiscountAmount }
     * 
     */
    public ChangeDiscountAmount createChangeDiscountAmount() {
        return new ChangeDiscountAmount();
    }

    /**
     * Create an instance of {@link ChangeSellingQuantityResponse }
     * 
     */
    public ChangeSellingQuantityResponse createChangeSellingQuantityResponse() {
        return new ChangeSellingQuantityResponse();
    }

    /**
     * Create an instance of {@link ChangeSoldToIDResponse }
     * 
     */
    public ChangeSoldToIDResponse createChangeSoldToIDResponse() {
        return new ChangeSoldToIDResponse();
    }

    /**
     * Create an instance of {@link CheckPartRevisionChangeResponse }
     * 
     */
    public CheckPartRevisionChangeResponse createCheckPartRevisionChangeResponse() {
        return new CheckPartRevisionChangeResponse();
    }

    /**
     * Create an instance of {@link GetNewOrderRelTax }
     * 
     */
    public GetNewOrderRelTax createGetNewOrderRelTax() {
        return new GetNewOrderRelTax();
    }

    /**
     * Create an instance of {@link JobProdDeleteResponse }
     * 
     */
    public JobProdDeleteResponse createJobProdDeleteResponse() {
        return new JobProdDeleteResponse();
    }

    /**
     * Create an instance of {@link KitCompPartCreateResponse }
     * 
     */
    public KitCompPartCreateResponse createKitCompPartCreateResponse() {
        return new KitCompPartCreateResponse();
    }

    /**
     * Create an instance of {@link GetRowsResponse }
     * 
     */
    public GetRowsResponse createGetRowsResponse() {
        return new GetRowsResponse();
    }

    /**
     * Create an instance of {@link CloseOrderLineResponse }
     * 
     */
    public CloseOrderLineResponse createCloseOrderLineResponse() {
        return new CloseOrderLineResponse();
    }

    /**
     * Create an instance of {@link GetInventoryQuantities }
     * 
     */
    public GetInventoryQuantities createGetInventoryQuantities() {
        return new GetInventoryQuantities();
    }

    /**
     * Create an instance of {@link ChangePriceList }
     * 
     */
    public ChangePriceList createChangePriceList() {
        return new ChangePriceList();
    }

    /**
     * Create an instance of {@link CloseRelease }
     * 
     */
    public CloseRelease createCloseRelease() {
        return new CloseRelease();
    }

    /**
     * Create an instance of {@link GlbSugPOChgUpdate }
     * 
     */
    public GlbSugPOChgUpdate createGlbSugPOChgUpdate() {
        return new GlbSugPOChgUpdate();
    }

    /**
     * Create an instance of {@link OnChangeOfMktgEvnt }
     * 
     */
    public OnChangeOfMktgEvnt createOnChangeOfMktgEvnt() {
        return new OnChangeOfMktgEvnt();
    }

    /**
     * Create an instance of {@link OrderDtlGetNewFromQuote }
     * 
     */
    public OrderDtlGetNewFromQuote createOrderDtlGetNewFromQuote() {
        return new OrderDtlGetNewFromQuote();
    }

    /**
     * Create an instance of {@link ReopenOrder }
     * 
     */
    public ReopenOrder createReopenOrder() {
        return new ReopenOrder();
    }

    /**
     * Create an instance of {@link CheckComplianceOrderFailResponse }
     * 
     */
    public CheckComplianceOrderFailResponse createCheckComplianceOrderFailResponse() {
        return new CheckComplianceOrderFailResponse();
    }

    /**
     * Create an instance of {@link OnChangeOfMktgCampaign }
     * 
     */
    public OnChangeOfMktgCampaign createOnChangeOfMktgCampaign() {
        return new OnChangeOfMktgCampaign();
    }

    /**
     * Create an instance of {@link InvoiceExists }
     * 
     */
    public InvoiceExists createInvoiceExists() {
        return new InvoiceExists();
    }

    /**
     * Create an instance of {@link GetBySysRowIDs }
     * 
     */
    public GetBySysRowIDs createGetBySysRowIDs() {
        return new GetBySysRowIDs();
    }

    /**
     * Create an instance of {@link CCClear }
     * 
     */
    public CCClear createCCClear() {
        return new CCClear();
    }

    /**
     * Create an instance of {@link ChangeMiscAmountResponse }
     * 
     */
    public ChangeMiscAmountResponse createChangeMiscAmountResponse() {
        return new ChangeMiscAmountResponse();
    }

    /**
     * Create an instance of {@link CheckComplianceOrderFail }
     * 
     */
    public CheckComplianceOrderFail createCheckComplianceOrderFail() {
        return new CheckComplianceOrderFail();
    }

    /**
     * Create an instance of {@link GetListCustomResponse }
     * 
     */
    public GetListCustomResponse createGetListCustomResponse() {
        return new GetListCustomResponse();
    }

    /**
     * Create an instance of {@link ChangeCommissionable }
     * 
     */
    public ChangeCommissionable createChangeCommissionable() {
        return new ChangeCommissionable();
    }

    /**
     * Create an instance of {@link ChangeCounterSale }
     * 
     */
    public ChangeCounterSale createChangeCounterSale() {
        return new ChangeCounterSale();
    }

    /**
     * Create an instance of {@link SubmitNewOrder }
     * 
     */
    public SubmitNewOrder createSubmitNewOrder() {
        return new SubmitNewOrder();
    }

    /**
     * Create an instance of {@link RecalcKitPriceAfterConfig }
     * 
     */
    public RecalcKitPriceAfterConfig createRecalcKitPriceAfterConfig() {
        return new RecalcKitPriceAfterConfig();
    }

    /**
     * Create an instance of {@link ChangeRelUseOTS }
     * 
     */
    public ChangeRelUseOTS createChangeRelUseOTS() {
        return new ChangeRelUseOTS();
    }

    /**
     * Create an instance of {@link UpdateOrderDtlDiscountPercent }
     * 
     */
    public UpdateOrderDtlDiscountPercent createUpdateOrderDtlDiscountPercent() {
        return new UpdateOrderDtlDiscountPercent();
    }

    /**
     * Create an instance of {@link ChangeUnitPrice }
     * 
     */
    public ChangeUnitPrice createChangeUnitPrice() {
        return new ChangeUnitPrice();
    }

    /**
     * Create an instance of {@link ChangeDiscountAmountResponse }
     * 
     */
    public ChangeDiscountAmountResponse createChangeDiscountAmountResponse() {
        return new ChangeDiscountAmountResponse();
    }

    /**
     * Create an instance of {@link OrderDtlGetNewFromQuoteResponse }
     * 
     */
    public OrderDtlGetNewFromQuoteResponse createOrderDtlGetNewFromQuoteResponse() {
        return new OrderDtlGetNewFromQuoteResponse();
    }

    /**
     * Create an instance of {@link CheckOrderHedDispatchReason }
     * 
     */
    public CheckOrderHedDispatchReason createCheckOrderHedDispatchReason() {
        return new CheckOrderHedDispatchReason();
    }

    /**
     * Create an instance of {@link CheckSellingQuantityChangeResponse }
     * 
     */
    public CheckSellingQuantityChangeResponse createCheckSellingQuantityChangeResponse() {
        return new CheckSellingQuantityChangeResponse();
    }

    /**
     * Create an instance of {@link ReopenOrderResponse }
     * 
     */
    public ReopenOrderResponse createReopenOrderResponse() {
        return new ReopenOrderResponse();
    }

    /**
     * Create an instance of {@link NegativeInventoryTest }
     * 
     */
    public NegativeInventoryTest createNegativeInventoryTest() {
        return new NegativeInventoryTest();
    }

    /**
     * Create an instance of {@link CheckLtrOfCrdt }
     * 
     */
    public CheckLtrOfCrdt createCheckLtrOfCrdt() {
        return new CheckLtrOfCrdt();
    }

    /**
     * Create an instance of {@link SubmitNewOrderResponse }
     * 
     */
    public SubmitNewOrderResponse createSubmitNewOrderResponse() {
        return new SubmitNewOrderResponse();
    }

    /**
     * Create an instance of {@link OnChangeOfFixedAmountResponse }
     * 
     */
    public OnChangeOfFixedAmountResponse createOnChangeOfFixedAmountResponse() {
        return new OnChangeOfFixedAmountResponse();
    }

    /**
     * Create an instance of {@link ChangedCardNumberResponse }
     * 
     */
    public ChangedCardNumberResponse createChangedCardNumberResponse() {
        return new ChangedCardNumberResponse();
    }

    /**
     * Create an instance of {@link CheckOrderHedChangesResponse }
     * 
     */
    public CheckOrderHedChangesResponse createCheckOrderHedChangesResponse() {
        return new CheckOrderHedChangesResponse();
    }

    /**
     * Create an instance of {@link ETCValidateAddressResponse }
     * 
     */
    public ETCValidateAddressResponse createETCValidateAddressResponse() {
        return new ETCValidateAddressResponse();
    }

    /**
     * Create an instance of {@link OnChangeRateCodeResponse }
     * 
     */
    public OnChangeRateCodeResponse createOnChangeRateCodeResponse() {
        return new OnChangeRateCodeResponse();
    }

    /**
     * Create an instance of {@link CCProcessCardResponse }
     * 
     */
    public CCProcessCardResponse createCCProcessCardResponse() {
        return new CCProcessCardResponse();
    }

    /**
     * Create an instance of {@link GetNewOrderHedAttchResponse }
     * 
     */
    public GetNewOrderHedAttchResponse createGetNewOrderHedAttchResponse() {
        return new GetNewOrderHedAttchResponse();
    }

    /**
     * Create an instance of {@link ChangeSalesRepResponse }
     * 
     */
    public ChangeSalesRepResponse createChangeSalesRepResponse() {
        return new ChangeSalesRepResponse();
    }

    /**
     * Create an instance of {@link CheckProjectIDResponse }
     * 
     */
    public CheckProjectIDResponse createCheckProjectIDResponse() {
        return new CheckProjectIDResponse();
    }

    /**
     * Create an instance of {@link CCClearResponse }
     * 
     */
    public CCClearResponse createCCClearResponse() {
        return new CCClearResponse();
    }

    /**
     * Create an instance of {@link CreateGlbSugPOChg }
     * 
     */
    public CreateGlbSugPOChg createCreateGlbSugPOChg() {
        return new CreateGlbSugPOChg();
    }

    /**
     * Create an instance of {@link ChangeOrderRelDropShipResponse }
     * 
     */
    public ChangeOrderRelDropShipResponse createChangeOrderRelDropShipResponse() {
        return new ChangeOrderRelDropShipResponse();
    }

    /**
     * Create an instance of {@link CheckOrderHedChanges }
     * 
     */
    public CheckOrderHedChanges createCheckOrderHedChanges() {
        return new CheckOrderHedChanges();
    }

    /**
     * Create an instance of {@link ChangeNeedByDateResponse }
     * 
     */
    public ChangeNeedByDateResponse createChangeNeedByDateResponse() {
        return new ChangeNeedByDateResponse();
    }

    /**
     * Create an instance of {@link CheckKitRevision }
     * 
     */
    public CheckKitRevision createCheckKitRevision() {
        return new CheckKitRevision();
    }

    /**
     * Create an instance of {@link ChangeSellingQuantity }
     * 
     */
    public ChangeSellingQuantity createChangeSellingQuantity() {
        return new ChangeSellingQuantity();
    }

    /**
     * Create an instance of {@link GetListCustom }
     * 
     */
    public GetListCustom createGetListCustom() {
        return new GetListCustom();
    }

    /**
     * Create an instance of {@link GetRowsCustomerTrackerResponse }
     * 
     */
    public GetRowsCustomerTrackerResponse createGetRowsCustomerTrackerResponse() {
        return new GetRowsCustomerTrackerResponse();
    }

    /**
     * Create an instance of {@link ValidateSNsResponse }
     * 
     */
    public ValidateSNsResponse createValidateSNsResponse() {
        return new ValidateSNsResponse();
    }

    /**
     * Create an instance of {@link CreateOrderFromQuote }
     * 
     */
    public CreateOrderFromQuote createCreateOrderFromQuote() {
        return new CreateOrderFromQuote();
    }

    /**
     * Create an instance of {@link GetBasePartForConfig }
     * 
     */
    public GetBasePartForConfig createGetBasePartForConfig() {
        return new GetBasePartForConfig();
    }

    /**
     * Create an instance of {@link DeleteByID }
     * 
     */
    public DeleteByID createDeleteByID() {
        return new DeleteByID();
    }

    /**
     * Create an instance of {@link GetBreakListCodeDesc }
     * 
     */
    public GetBreakListCodeDesc createGetBreakListCodeDesc() {
        return new GetBreakListCodeDesc();
    }

    /**
     * Create an instance of {@link VerifySendICPOSuggResponse }
     * 
     */
    public VerifySendICPOSuggResponse createVerifySendICPOSuggResponse() {
        return new VerifySendICPOSuggResponse();
    }

    /**
     * Create an instance of {@link CheckSellingFactorDirection }
     * 
     */
    public CheckSellingFactorDirection createCheckSellingFactorDirection() {
        return new CheckSellingFactorDirection();
    }

    /**
     * Create an instance of {@link ChangeOrderRelDropShip }
     * 
     */
    public ChangeOrderRelDropShip createChangeOrderRelDropShip() {
        return new ChangeOrderRelDropShip();
    }

    /**
     * Create an instance of {@link ChangeSoldToContactResponse }
     * 
     */
    public ChangeSoldToContactResponse createChangeSoldToContactResponse() {
        return new ChangeSoldToContactResponse();
    }

    /**
     * Create an instance of {@link ChangeCounterSaleResponse }
     * 
     */
    public ChangeCounterSaleResponse createChangeCounterSaleResponse() {
        return new ChangeCounterSaleResponse();
    }

    /**
     * Create an instance of {@link ChangeOrderRelVendorIDResponse }
     * 
     */
    public ChangeOrderRelVendorIDResponse createChangeOrderRelVendorIDResponse() {
        return new ChangeOrderRelVendorIDResponse();
    }

    /**
     * Create an instance of {@link GetByIDLinkedOrderResponse }
     * 
     */
    public GetByIDLinkedOrderResponse createGetByIDLinkedOrderResponse() {
        return new GetByIDLinkedOrderResponse();
    }

    /**
     * Create an instance of {@link ChangeGroup }
     * 
     */
    public ChangeGroup createChangeGroup() {
        return new ChangeGroup();
    }

    /**
     * Create an instance of {@link ChangeOverrideDiscPriceListResponse }
     * 
     */
    public ChangeOverrideDiscPriceListResponse createChangeOverrideDiscPriceListResponse() {
        return new ChangeOverrideDiscPriceListResponse();
    }

    /**
     * Create an instance of {@link CheckRenewalNbrResponse }
     * 
     */
    public CheckRenewalNbrResponse createCheckRenewalNbrResponse() {
        return new CheckRenewalNbrResponse();
    }

    /**
     * Create an instance of {@link OnChangeofPhaseIDResponse }
     * 
     */
    public OnChangeofPhaseIDResponse createOnChangeofPhaseIDResponse() {
        return new OnChangeofPhaseIDResponse();
    }

    /**
     * Create an instance of {@link GetGlbSugPOChg }
     * 
     */
    public GetGlbSugPOChg createGetGlbSugPOChg() {
        return new GetGlbSugPOChg();
    }

    /**
     * Create an instance of {@link ApplyOrderBasedDiscountsResponse }
     * 
     */
    public ApplyOrderBasedDiscountsResponse createApplyOrderBasedDiscountsResponse() {
        return new ApplyOrderBasedDiscountsResponse();
    }

    /**
     * Create an instance of {@link OverCreditLimitUpdateAllowed }
     * 
     */
    public OverCreditLimitUpdateAllowed createOverCreditLimitUpdateAllowed() {
        return new OverCreditLimitUpdateAllowed();
    }

    /**
     * Create an instance of {@link OnChangeARLOCID }
     * 
     */
    public OnChangeARLOCID createOnChangeARLOCID() {
        return new OnChangeARLOCID();
    }

    /**
     * Create an instance of {@link ChangeManualTaxCalc }
     * 
     */
    public ChangeManualTaxCalc createChangeManualTaxCalc() {
        return new ChangeManualTaxCalc();
    }

    /**
     * Create an instance of {@link ChangeRenewalNbrMaster }
     * 
     */
    public ChangeRenewalNbrMaster createChangeRenewalNbrMaster() {
        return new ChangeRenewalNbrMaster();
    }

    /**
     * Create an instance of {@link UserDefinedColumns.Column }
     * 
     */
    public UserDefinedColumns.Column createUserDefinedColumnsColumn() {
        return new UserDefinedColumns.Column();
    }

    /**
     * Create an instance of {@link ArrayOfKeyValueOfstringstring.KeyValueOfstringstring }
     * 
     */
    public ArrayOfKeyValueOfstringstring.KeyValueOfstringstring createArrayOfKeyValueOfstringstringKeyValueOfstringstring() {
        return new ArrayOfKeyValueOfstringstring.KeyValueOfstringstring();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveOTSParamsTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SaveOTSParamsTableset")
    public JAXBElement<SaveOTSParamsTableset> createSaveOTSParamsTableset(SaveOTSParamsTableset value) {
        return new JAXBElement<SaveOTSParamsTableset>(_SaveOTSParamsTableset_QNAME, SaveOTSParamsTableset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuoteQtyRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "QuoteQtyRow")
    public JAXBElement<QuoteQtyRow> createQuoteQtyRow(QuoteQtyRow value) {
        return new JAXBElement<QuoteQtyRow>(_QuoteQtyRow_QNAME, QuoteQtyRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderCustTrkTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderCustTrkTableset")
    public JAXBElement<OrderCustTrkTableset> createOrderCustTrkTableset(OrderCustTrkTableset value) {
        return new JAXBElement<OrderCustTrkTableset>(_OrderCustTrkTableset_QNAME, OrderCustTrkTableset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesOrderTableset")
    public JAXBElement<SalesOrderTableset> createSalesOrderTableset(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SalesOrderTableset_QNAME, SalesOrderTableset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BOUpdErrorRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice", name = "BOUpdErrorRow")
    public JAXBElement<BOUpdErrorRow> createBOUpdErrorRow(BOUpdErrorRow value) {
        return new JAXBElement<BOUpdErrorRow>(_BOUpdErrorRow_QNAME, BOUpdErrorRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectedSerialNumbersTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SelectedSerialNumbersTable")
    public JAXBElement<SelectedSerialNumbersTable> createSelectedSerialNumbersTable(SelectedSerialNumbersTable value) {
        return new JAXBElement<SelectedSerialNumbersTable>(_SelectedSerialNumbersTable_QNAME, SelectedSerialNumbersTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderMscTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderMscTable")
    public JAXBElement<OrderMscTable> createOrderMscTable(OrderMscTable value) {
        return new JAXBElement<OrderMscTable>(_OrderMscTable_QNAME, OrderMscTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderDtlAttchTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDtlAttchTable")
    public JAXBElement<OrderDtlAttchTable> createOrderDtlAttchTable(OrderDtlAttchTable value) {
        return new JAXBElement<OrderDtlAttchTable>(_OrderDtlAttchTable_QNAME, OrderDtlAttchTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BOUpdErrorTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice", name = "BOUpdErrorTable")
    public JAXBElement<BOUpdErrorTable> createBOUpdErrorTable(BOUpdErrorTable value) {
        return new JAXBElement<BOUpdErrorTable>(_BOUpdErrorTable_QNAME, BOUpdErrorTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderMscRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderMscRow")
    public JAXBElement<OrderMscRow> createOrderMscRow(OrderMscRow value) {
        return new JAXBElement<OrderMscRow>(_OrderMscRow_QNAME, OrderMscRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOEntryUIParamsTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SOEntryUIParamsTable")
    public JAXBElement<SOEntryUIParamsTable> createSOEntryUIParamsTable(SOEntryUIParamsTable value) {
        return new JAXBElement<SOEntryUIParamsTable>(_SOEntryUIParamsTable_QNAME, SOEntryUIParamsTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartSubsRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartSubsRow")
    public JAXBElement<PartSubsRow> createPartSubsRow(PartSubsRow value) {
        return new JAXBElement<PartSubsRow>(_PartSubsRow_QNAME, PartSubsRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectSerialNumbersParamsTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SelectSerialNumbersParamsTable")
    public JAXBElement<SelectSerialNumbersParamsTable> createSelectSerialNumbersParamsTable(SelectSerialNumbersParamsTable value) {
        return new JAXBElement<SelectSerialNumbersParamsTable>(_SelectSerialNumbersParamsTable_QNAME, SelectSerialNumbersParamsTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedTable")
    public JAXBElement<OrderHedTable> createOrderHedTable(OrderHedTable value) {
        return new JAXBElement<OrderHedTable>(_OrderHedTable_QNAME, OrderHedTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SNFormatRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNFormatRow")
    public JAXBElement<SNFormatRow> createSNFormatRow(SNFormatRow value) {
        return new JAXBElement<SNFormatRow>(_SNFormatRow_QNAME, SNFormatRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedAttchTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedAttchTable")
    public JAXBElement<OrderHedAttchTable> createOrderHedAttchTable(OrderHedAttchTable value) {
        return new JAXBElement<OrderHedAttchTable>(_OrderHedAttchTable_QNAME, OrderHedAttchTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderCustTrkRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderCustTrkRow")
    public JAXBElement<OrderCustTrkRow> createOrderCustTrkRow(OrderCustTrkRow value) {
        return new JAXBElement<OrderCustTrkRow>(_OrderCustTrkRow_QNAME, OrderCustTrkRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfKeyValueOfstringstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", name = "ArrayOfKeyValueOfstringstring")
    public JAXBElement<ArrayOfKeyValueOfstringstring> createArrayOfKeyValueOfstringstring(ArrayOfKeyValueOfstringstring value) {
        return new JAXBElement<ArrayOfKeyValueOfstringstring>(_ArrayOfKeyValueOfstringstring_QNAME, ArrayOfKeyValueOfstringstring.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IceTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice", name = "IceTableset")
    public JAXBElement<IceTableset> createIceTableset(IceTableset value) {
        return new JAXBElement<IceTableset>(_IceTableset_QNAME, IceTableset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderRepCommTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderRepCommTable")
    public JAXBElement<OrderRepCommTable> createOrderRepCommTable(OrderRepCommTable value) {
        return new JAXBElement<OrderRepCommTable>(_OrderRepCommTable_QNAME, OrderRepCommTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "GlbSugPOChgTable")
    public JAXBElement<GlbSugPOChgTable> createGlbSugPOChgTable(GlbSugPOChgTable value) {
        return new JAXBElement<GlbSugPOChgTable>(_GlbSugPOChgTable_QNAME, GlbSugPOChgTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectSerialNumbersParamsRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SelectSerialNumbersParamsRow")
    public JAXBElement<SelectSerialNumbersParamsRow> createSelectSerialNumbersParamsRow(SelectSerialNumbersParamsRow value) {
        return new JAXBElement<SelectSerialNumbersParamsRow>(_SelectSerialNumbersParamsRow_QNAME, SelectSerialNumbersParamsRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedUPSTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedUPSTable")
    public JAXBElement<OrderHedUPSTable> createOrderHedUPSTable(OrderHedUPSTable value) {
        return new JAXBElement<OrderHedUPSTable>(_OrderHedUPSTable_QNAME, OrderHedUPSTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHistTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHistTable")
    public JAXBElement<OrderHistTable> createOrderHistTable(OrderHistTable value) {
        return new JAXBElement<OrderHistTable>(_OrderHistTable_QNAME, OrderHistTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedRow")
    public JAXBElement<OrderHedRow> createOrderHedRow(OrderHedRow value) {
        return new JAXBElement<OrderHedRow>(_OrderHedRow_QNAME, OrderHedRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuoteQtyTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "QuoteQtyTable")
    public JAXBElement<QuoteQtyTable> createQuoteQtyTable(QuoteQtyTable value) {
        return new JAXBElement<QuoteQtyTable>(_QuoteQtyTable_QNAME, QuoteQtyTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveOTSParamsRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SaveOTSParamsRow")
    public JAXBElement<SaveOTSParamsRow> createSaveOTSParamsRow(SaveOTSParamsRow value) {
        return new JAXBElement<SaveOTSParamsRow>(_SaveOTSParamsRow_QNAME, SaveOTSParamsRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OHOrderMscRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OHOrderMscRow")
    public JAXBElement<OHOrderMscRow> createOHOrderMscRow(OHOrderMscRow value) {
        return new JAXBElement<OHOrderMscRow>(_OHOrderMscRow_QNAME, OHOrderMscRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderDtlRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDtlRow")
    public JAXBElement<OrderDtlRow> createOrderDtlRow(OrderDtlRow value) {
        return new JAXBElement<OrderDtlRow>(_OrderDtlRow_QNAME, OrderDtlRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ETCAddressTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ETCAddressTable")
    public JAXBElement<ETCAddressTable> createETCAddressTable(ETCAddressTable value) {
        return new JAXBElement<ETCAddressTable>(_ETCAddressTable_QNAME, ETCAddressTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IceRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice", name = "IceRow")
    public JAXBElement<IceRow> createIceRow(IceRow value) {
        return new JAXBElement<IceRow>(_IceRow_QNAME, IceRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HedTaxSumTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "HedTaxSumTable")
    public JAXBElement<HedTaxSumTable> createHedTaxSumTable(HedTaxSumTable value) {
        return new JAXBElement<HedTaxSumTable>(_HedTaxSumTable_QNAME, HedTaxSumTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ETCAddressRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ETCAddressRow")
    public JAXBElement<ETCAddressRow> createETCAddressRow(ETCAddressRow value) {
        return new JAXBElement<ETCAddressRow>(_ETCAddressRow_QNAME, ETCAddressRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SNFormatTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNFormatTable")
    public JAXBElement<SNFormatTable> createSNFormatTable(SNFormatTable value) {
        return new JAXBElement<SNFormatTable>(_SNFormatTable_QNAME, SNFormatTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxConnectStatusTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxConnectStatusTable")
    public JAXBElement<TaxConnectStatusTable> createTaxConnectStatusTable(TaxConnectStatusTable value) {
        return new JAXBElement<TaxConnectStatusTable>(_TaxConnectStatusTable_QNAME, TaxConnectStatusTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderRepCommRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderRepCommRow")
    public JAXBElement<OrderRepCommRow> createOrderRepCommRow(OrderRepCommRow value) {
        return new JAXBElement<OrderRepCommRow>(_OrderRepCommRow_QNAME, OrderRepCommRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdExtSalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "UpdExtSalesOrderTableset")
    public JAXBElement<UpdExtSalesOrderTableset> createUpdExtSalesOrderTableset(UpdExtSalesOrderTableset value) {
        return new JAXBElement<UpdExtSalesOrderTableset>(_UpdExtSalesOrderTableset_QNAME, UpdExtSalesOrderTableset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxConnectStatusRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxConnectStatusRow")
    public JAXBElement<TaxConnectStatusRow> createTaxConnectStatusRow(TaxConnectStatusRow value) {
        return new JAXBElement<TaxConnectStatusRow>(_TaxConnectStatusRow_QNAME, TaxConnectStatusRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOEntryUIParamsRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SOEntryUIParamsRow")
    public JAXBElement<SOEntryUIParamsRow> createSOEntryUIParamsRow(SOEntryUIParamsRow value) {
        return new JAXBElement<SOEntryUIParamsRow>(_SOEntryUIParamsRow_QNAME, SOEntryUIParamsRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedAttchRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedAttchRow")
    public JAXBElement<OrderHedAttchRow> createOrderHedAttchRow(OrderHedAttchRow value) {
        return new JAXBElement<OrderHedAttchRow>(_OrderHedAttchRow_QNAME, OrderHedAttchRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "GlbSugPOChgRow")
    public JAXBElement<GlbSugPOChgRow> createGlbSugPOChgRow(GlbSugPOChgRow value) {
        return new JAXBElement<GlbSugPOChgRow>(_GlbSugPOChgRow_QNAME, GlbSugPOChgRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderDtlAttchRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDtlAttchRow")
    public JAXBElement<OrderDtlAttchRow> createOrderDtlAttchRow(OrderDtlAttchRow value) {
        return new JAXBElement<OrderDtlAttchRow>(_OrderDtlAttchRow_QNAME, OrderDtlAttchRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedListTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedListTableset")
    public JAXBElement<OrderHedListTableset> createOrderHedListTableset(OrderHedListTableset value) {
        return new JAXBElement<OrderHedListTableset>(_OrderHedListTableset_QNAME, OrderHedListTableset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", name = "ArrayOfstring")
    public JAXBElement<ArrayOfstring> createArrayOfstring(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_ArrayOfstring_QNAME, ArrayOfstring.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectSerialNumbersParamsTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SelectSerialNumbersParamsTableset")
    public JAXBElement<SelectSerialNumbersParamsTableset> createSelectSerialNumbersParamsTableset(SelectSerialNumbersParamsTableset value) {
        return new JAXBElement<SelectSerialNumbersParamsTableset>(_SelectSerialNumbersParamsTableset_QNAME, SelectSerialNumbersParamsTableset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfguid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", name = "ArrayOfguid")
    public JAXBElement<ArrayOfguid> createArrayOfguid(ArrayOfguid value) {
        return new JAXBElement<ArrayOfguid>(_ArrayOfguid_QNAME, ArrayOfguid.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "GlbSugPOChgTableset")
    public JAXBElement<GlbSugPOChgTableset> createGlbSugPOChgTableset(GlbSugPOChgTableset value) {
        return new JAXBElement<GlbSugPOChgTableset>(_GlbSugPOChgTableset_QNAME, GlbSugPOChgTableset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JobProdTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "JobProdTable")
    public JAXBElement<JobProdTable> createJobProdTable(JobProdTable value) {
        return new JAXBElement<JobProdTable>(_JobProdTable_QNAME, JobProdTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OHOrderMscTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OHOrderMscTable")
    public JAXBElement<OHOrderMscTable> createOHOrderMscTable(OHOrderMscTable value) {
        return new JAXBElement<OHOrderMscTable>(_OHOrderMscTable_QNAME, OHOrderMscTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HedTaxSumRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "HedTaxSumRow")
    public JAXBElement<HedTaxSumRow> createHedTaxSumRow(HedTaxSumRow value) {
        return new JAXBElement<HedTaxSumRow>(_HedTaxSumRow_QNAME, HedTaxSumRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderDtlTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDtlTable")
    public JAXBElement<OrderDtlTable> createOrderDtlTable(OrderDtlTable value) {
        return new JAXBElement<OrderDtlTable>(_OrderDtlTable_QNAME, OrderDtlTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectedSerialNumbersRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SelectedSerialNumbersRow")
    public JAXBElement<SelectedSerialNumbersRow> createSelectedSerialNumbersRow(SelectedSerialNumbersRow value) {
        return new JAXBElement<SelectedSerialNumbersRow>(_SelectedSerialNumbersRow_QNAME, SelectedSerialNumbersRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartSubsTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartSubsTable")
    public JAXBElement<PartSubsTable> createPartSubsTable(PartSubsTable value) {
        return new JAXBElement<PartSubsTable>(_PartSubsTable_QNAME, PartSubsTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrdDtlQuoteQtyTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrdDtlQuoteQtyTableset")
    public JAXBElement<OrdDtlQuoteQtyTableset> createOrdDtlQuoteQtyTableset(OrdDtlQuoteQtyTableset value) {
        return new JAXBElement<OrdDtlQuoteQtyTableset>(_OrdDtlQuoteQtyTableset_QNAME, OrdDtlQuoteQtyTableset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ETCMessageTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ETCMessageTable")
    public JAXBElement<ETCMessageTable> createETCMessageTable(ETCMessageTable value) {
        return new JAXBElement<ETCMessageTable>(_ETCMessageTable_QNAME, ETCMessageTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserDefinedColumns }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://epicor.com/UserDefinedColumns", name = "UserDefinedColumns")
    public JAXBElement<UserDefinedColumns> createUserDefinedColumns(UserDefinedColumns value) {
        return new JAXBElement<UserDefinedColumns>(_UserDefinedColumns_QNAME, UserDefinedColumns.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderCustTrkTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderCustTrkTable")
    public JAXBElement<OrderCustTrkTable> createOrderCustTrkTable(OrderCustTrkTable value) {
        return new JAXBElement<OrderCustTrkTable>(_OrderCustTrkTable_QNAME, OrderCustTrkTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrdRelJobProdTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrdRelJobProdTableset")
    public JAXBElement<OrdRelJobProdTableset> createOrdRelJobProdTableset(OrdRelJobProdTableset value) {
        return new JAXBElement<OrdRelJobProdTableset>(_OrdRelJobProdTableset_QNAME, OrdRelJobProdTableset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderRelRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderRelRow")
    public JAXBElement<OrderRelRow> createOrderRelRow(OrderRelRow value) {
        return new JAXBElement<OrderRelRow>(_OrderRelRow_QNAME, OrderRelRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEpicorExceptionData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", name = "ArrayOfEpicorExceptionData")
    public JAXBElement<ArrayOfEpicorExceptionData> createArrayOfEpicorExceptionData(ArrayOfEpicorExceptionData value) {
        return new JAXBElement<ArrayOfEpicorExceptionData>(_ArrayOfEpicorExceptionData_QNAME, ArrayOfEpicorExceptionData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EpicorExceptionData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", name = "EpicorExceptionData")
    public JAXBElement<EpicorExceptionData> createEpicorExceptionData(EpicorExceptionData value) {
        return new JAXBElement<EpicorExceptionData>(_EpicorExceptionData_QNAME, EpicorExceptionData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHistRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHistRow")
    public JAXBElement<OrderHistRow> createOrderHistRow(OrderHistRow value) {
        return new JAXBElement<OrderHistRow>(_OrderHistRow_QNAME, OrderHistRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TempRowBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Epicor.Data", name = "TempRowBase")
    public JAXBElement<TempRowBase> createTempRowBase(TempRowBase value) {
        return new JAXBElement<TempRowBase>(_TempRowBase_QNAME, TempRowBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ETCAddrValidationTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ETCAddrValidationTableset")
    public JAXBElement<ETCAddrValidationTableset> createETCAddrValidationTableset(ETCAddrValidationTableset value) {
        return new JAXBElement<ETCAddrValidationTableset>(_ETCAddrValidationTableset_QNAME, ETCAddrValidationTableset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOEntryUIParamsTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SOEntryUIParamsTableset")
    public JAXBElement<SOEntryUIParamsTableset> createSOEntryUIParamsTableset(SOEntryUIParamsTableset value) {
        return new JAXBElement<SOEntryUIParamsTableset>(_SOEntryUIParamsTableset_QNAME, SOEntryUIParamsTableset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BOUpdErrorTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice", name = "BOUpdErrorTableset")
    public JAXBElement<BOUpdErrorTableset> createBOUpdErrorTableset(BOUpdErrorTableset value) {
        return new JAXBElement<BOUpdErrorTableset>(_BOUpdErrorTableset_QNAME, BOUpdErrorTableset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderRelTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderRelTable")
    public JAXBElement<OrderRelTable> createOrderRelTable(OrderRelTable value) {
        return new JAXBElement<OrderRelTable>(_OrderRelTable_QNAME, OrderRelTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ETCMessageRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ETCMessageRow")
    public JAXBElement<ETCMessageRow> createETCMessageRow(ETCMessageRow value) {
        return new JAXBElement<ETCMessageRow>(_ETCMessageRow_QNAME, ETCMessageRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedUPSRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedUPSRow")
    public JAXBElement<OrderHedUPSRow> createOrderHedUPSRow(OrderHedUPSRow value) {
        return new JAXBElement<OrderHedUPSRow>(_OrderHedUPSRow_QNAME, OrderHedUPSRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedListRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedListRow")
    public JAXBElement<OrderHedListRow> createOrderHedListRow(OrderHedListRow value) {
        return new JAXBElement<OrderHedListRow>(_OrderHedListRow_QNAME, OrderHedListRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JobProdRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "JobProdRow")
    public JAXBElement<JobProdRow> createJobProdRow(JobProdRow value) {
        return new JAXBElement<JobProdRow>(_JobProdRow_QNAME, JobProdRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderRelTaxTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderRelTaxTable")
    public JAXBElement<OrderRelTaxTable> createOrderRelTaxTable(OrderRelTaxTable value) {
        return new JAXBElement<OrderRelTaxTable>(_OrderRelTaxTable_QNAME, OrderRelTaxTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderRelTaxRow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderRelTaxRow")
    public JAXBElement<OrderRelTaxRow> createOrderRelTaxRow(OrderRelTaxRow value) {
        return new JAXBElement<OrderRelTaxRow>(_OrderRelTaxRow_QNAME, OrderRelTaxRow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedListTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedListTable")
    public JAXBElement<OrderHedListTable> createOrderHedListTable(OrderHedListTable value) {
        return new JAXBElement<OrderHedListTable>(_OrderHedListTable_QNAME, OrderHedListTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveOTSParamsTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SaveOTSParamsTable")
    public JAXBElement<SaveOTSParamsTable> createSaveOTSParamsTable(SaveOTSParamsTable value) {
        return new JAXBElement<SaveOTSParamsTable>(_SaveOTSParamsTable_QNAME, SaveOTSParamsTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EpicorFaultDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", name = "EpicorFaultDetail")
    public JAXBElement<EpicorFaultDetail> createEpicorFaultDetail(EpicorFaultDetail value) {
        return new JAXBElement<EpicorFaultDetail>(_EpicorFaultDetail_QNAME, EpicorFaultDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cARLOCID", scope = ChkLtrOfCrdt.class)
    public JAXBElement<String> createChkLtrOfCrdtCARLOCID(String value) {
        return new JAXBElement<String>(_ChkLtrOfCrdtCARLOCID_QNAME, String.class, ChkLtrOfCrdt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = SetUPSQVEnableResponse.class)
    public JAXBElement<SalesOrderTableset> createSetUPSQVEnableResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, SetUPSQVEnableResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MaterialMod", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowMaterialMod(String value) {
        return new JAXBElement<String>(_OrderDtlRowMaterialMod_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "KitFlagDescription", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowKitFlagDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowKitFlagDescription_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNumPricePerCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPartNumPricePerCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNumPricePerCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "JobTypeMFG", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowJobTypeMFG(String value) {
        return new JAXBElement<String>(_OrderDtlRowJobTypeMFG_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LineStatus", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowLineStatus(String value) {
        return new JAXBElement<String>(_OrderDtlRowLineStatus_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RevisionNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowRevisionNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowRevisionNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AvailPriceLists", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowAvailPriceLists(String value) {
        return new JAXBElement<String>(_OrderDtlRowAvailPriceLists_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BinNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowBinNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowBinNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PriceListCodeDesc", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPriceListCodeDesc(String value) {
        return new JAXBElement<String>(_OrderDtlRowPriceListCodeDesc_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MktgCampaignID", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowMktgCampaignID(String value) {
        return new JAXBElement<String>(_OrderDtlRowMktgCampaignID_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MktgCampaignIDCampDescription", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowMktgCampaignIDCampDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowMktgCampaignIDCampDescription_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrigWhyNoTax", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowOrigWhyNoTax(String value) {
        return new JAXBElement<String>(_OrderDtlRowOrigWhyNoTax_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BaseCurrSymbol", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowBaseCurrSymbol(String value) {
        return new JAXBElement<String>(_OrderDtlRowBaseCurrSymbol_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "POLine", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPOLine(String value) {
        return new JAXBElement<String>(_OrderDtlRowPOLine_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ProjectIDDescription", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowProjectIDDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowProjectIDDescription_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ProFormaInvComment", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowProFormaInvComment(String value) {
        return new JAXBElement<String>(_OrderDtlRowProFormaInvComment_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LotNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowLotNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowLotNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CumeDate", scope = OrderDtlRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderDtlRowCumeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowCumeDate_QNAME, XMLGregorianCalendar.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderNumCardMemberName", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowOrderNumCardMemberName(String value) {
        return new JAXBElement<String>(_OrderDtlRowOrderNumCardMemberName_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "IUM", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowIUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowIUM_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "POLineRef", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPOLineRef(String value) {
        return new JAXBElement<String>(_OrderDtlRowPOLineRef_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "KitCompOrigPart", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowKitCompOrigPart(String value) {
        return new JAXBElement<String>(_OrderDtlRowKitCompOrigPart_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "EntryProcess", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowEntryProcess(String value) {
        return new JAXBElement<String>(_OrderDtlRowEntryProcess_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LastConfigUserID", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowLastConfigUserID(String value) {
        return new JAXBElement<String>(_OrderDtlRowLastConfigUserID_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxCatIDDescription", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowTaxCatIDDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowTaxCatIDDescription_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DoNotShipAfterDate", scope = OrderDtlRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderDtlRowDoNotShipAfterDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowDoNotShipAfterDate_QNAME, XMLGregorianCalendar.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PriceListCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPriceListCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowPriceListCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "KitOrderQtyUOM", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowKitOrderQtyUOM(String value) {
        return new JAXBElement<String>(_OrderDtlRowKitOrderQtyUOM_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CounterSaleLotNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowCounterSaleLotNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowCounterSaleLotNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ProjectID", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowProjectID(String value) {
        return new JAXBElement<String>(_OrderDtlRowProjectID_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesUM", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowSalesUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesUM_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DiscBreakListCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowDiscBreakListCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowDiscBreakListCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DoNotShipBeforeDate", scope = OrderDtlRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderDtlRowDoNotShipBeforeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowDoNotShipBeforeDate_QNAME, XMLGregorianCalendar.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "QuoteNumCurrencyCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowQuoteNumCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowQuoteNumCurrencyCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DiscBreakListCodeEndDate", scope = OrderDtlRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderDtlRowDiscBreakListCodeEndDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowDiscBreakListCodeEndDate_QNAME, XMLGregorianCalendar.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MktgEvntEvntDescription", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowMktgEvntEvntDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowMktgEvntEvntDescription_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DUM", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowDUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowDUM_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LineType", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowLineType(String value) {
        return new JAXBElement<String>(_OrderDtlRowLineType_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ECCQuoteNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowECCQuoteNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowECCQuoteNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WarehouseDesc", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowWarehouseDesc(String value) {
        return new JAXBElement<String>(_OrderDtlRowWarehouseDesc_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CounterSaleDimCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowCounterSaleDimCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowCounterSaleDimCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ProdCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowProdCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowProdCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ECCOrderNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowECCOrderNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowECCOrderNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyID", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowCurrencyID(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrencyID_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CounterSaleBinNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowCounterSaleBinNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowCounterSaleBinNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustNumCustID", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowCustNumCustID(String value) {
        return new JAXBElement<String>(_OrderDtlRowCustNumCustID_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MOMsourceType", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowMOMsourceType(String value) {
        return new JAXBElement<String>(_OrderDtlRowMOMsourceType_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ECCPlant", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowECCPlant(String value) {
        return new JAXBElement<String>(_OrderDtlRowECCPlant_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ExtCompany", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowExtCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowExtCompany_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepName3", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowSalesRepName3(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesRepName3_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CreditLimitMessage", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowCreditLimitMessage(String value) {
        return new JAXBElement<String>(_OrderDtlRowCreditLimitMessage_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PickListComment", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPickListComment(String value) {
        return new JAXBElement<String>(_OrderDtlRowPickListComment_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepName2", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowSalesRepName2(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesRepName2_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepName1", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowSalesRepName1(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesRepName1_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DspJobType", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowDspJobType(String value) {
        return new JAXBElement<String>(_OrderDtlRowDspJobType_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepName5", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowSalesRepName5(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesRepName5_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepName4", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowSalesRepName4(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesRepName4_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ProdCodeDescription", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowProdCodeDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowProdCodeDescription_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipComment", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowShipComment(String value) {
        return new JAXBElement<String>(_OrderDtlRowShipComment_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WarehouseCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowWarehouseCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowWarehouseCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BaseCurrencyID", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowBaseCurrencyID(String value) {
        return new JAXBElement<String>(_OrderDtlRowBaseCurrencyID_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DimCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowDimCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowDimCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WarrantyCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowWarrantyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowWarrantyCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "InvtyUOM", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowInvtyUOM(String value) {
        return new JAXBElement<String>(_OrderDtlRowInvtyUOM_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustNumName", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowCustNumName(String value) {
        return new JAXBElement<String>(_OrderDtlRowCustNumName_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrencyCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ContractCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowContractCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowContractCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PriceGroupCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPriceGroupCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowPriceGroupCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "XRevisionNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowXRevisionNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowXRevisionNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangeDate", scope = OrderDtlRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderDtlRowChangeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowChangeDate_QNAME, XMLGregorianCalendar.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DefaultOversPricing", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowDefaultOversPricing(String value) {
        return new JAXBElement<String>(_OrderDtlRowDefaultOversPricing_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PrevPartNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPrevPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowPrevPartNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Configured", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowConfigured(String value) {
        return new JAXBElement<String>(_OrderDtlRowConfigured_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderComment", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowOrderComment(String value) {
        return new JAXBElement<String>(_OrderDtlRowOrderComment_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SellingFactorDirection", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowSellingFactorDirection(String value) {
        return new JAXBElement<String>(_OrderDtlRowSellingFactorDirection_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LastConfigDate", scope = OrderDtlRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderDtlRowLastConfigDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowLastConfigDate_QNAME, XMLGregorianCalendar.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangedBy", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowChangedBy(String value) {
        return new JAXBElement<String>(_OrderDtlRowChangedBy_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BreakListCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowBreakListCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowBreakListCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PricePerCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPricePerCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowPricePerCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MOMsourceEst", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowMOMsourceEst(String value) {
        return new JAXBElement<String>(_OrderDtlRowMOMsourceEst_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "KitPricing", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowKitPricing(String value) {
        return new JAXBElement<String>(_OrderDtlRowKitPricing_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WarrantyCodeWarrDescription", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowWarrantyCodeWarrDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowWarrantyCodeWarrDescription_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BasePartNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowBasePartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowBasePartNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OldProdCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowOldProdCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowOldProdCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CounterSaleWarehouse", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowCounterSaleWarehouse(String value) {
        return new JAXBElement<String>(_OrderDtlRowCounterSaleWarehouse_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNumSalesUM", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPartNumSalesUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNumSalesUM_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SmartString", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowSmartString(String value) {
        return new JAXBElement<String>(_OrderDtlRowSmartString_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WarrantyComment", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowWarrantyComment(String value) {
        return new JAXBElement<String>(_OrderDtlRowWarrantyComment_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesCatID", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowSalesCatID(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesCatID_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DiscBreakListCodeStartDate", scope = OrderDtlRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderDtlRowDiscBreakListCodeStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowDiscBreakListCodeStartDate_QNAME, XMLGregorianCalendar.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DiscBreakListCodeListDescription", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowDiscBreakListCodeListDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowDiscBreakListCodeListDescription_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrSymbol", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowCurrSymbol(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrSymbol_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderNumCurrencyCode", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowOrderNumCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowOrderNumCurrencyCode_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "XPartNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowXPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowXPartNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNumPartDescription", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPartNumPartDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNumPartDescription_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PrevXPartNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPrevXPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowPrevXPartNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RequestDate", scope = OrderDtlRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderDtlRowRequestDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowRequestDate_QNAME, XMLGregorianCalendar.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RespMessage", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowRespMessage(String value) {
        return new JAXBElement<String>(_OrderDtlRowRespMessage_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LineDesc", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowLineDesc(String value) {
        return new JAXBElement<String>(_OrderDtlRowLineDesc_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BaseRevisionNum", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowBaseRevisionNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowBaseRevisionNum_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PlanUserID", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPlanUserID(String value) {
        return new JAXBElement<String>(_OrderDtlRowPlanUserID_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AvailUMFromQuote", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowAvailUMFromQuote(String value) {
        return new JAXBElement<String>(_OrderDtlRowAvailUMFromQuote_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxCatID", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowTaxCatID(String value) {
        return new JAXBElement<String>(_OrderDtlRowTaxCatID_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "KitFlag", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowKitFlag(String value) {
        return new JAXBElement<String>(_OrderDtlRowKitFlag_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PlanGUID", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPlanGUID(String value) {
        return new JAXBElement<String>(_OrderDtlRowPlanGUID_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Reference", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowReference(String value) {
        return new JAXBElement<String>(_OrderDtlRowReference_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CreditLimitSource", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowCreditLimitSource(String value) {
        return new JAXBElement<String>(_OrderDtlRowCreditLimitSource_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ContractCodeContractDescription", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowContractCodeContractDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowContractCodeContractDescription_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNumIUM", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowPartNumIUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNumIUM_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustNumBTName", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowCustNumBTName(String value) {
        return new JAXBElement<String>(_OrderDtlRowCustNumBTName_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "InvoiceComment", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowInvoiceComment(String value) {
        return new JAXBElement<String>(_OrderDtlRowInvoiceComment_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LaborMod", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowLaborMod(String value) {
        return new JAXBElement<String>(_OrderDtlRowLaborMod_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "NeedByDate", scope = OrderDtlRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderDtlRowNeedByDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowNeedByDate_QNAME, XMLGregorianCalendar.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesCatIDDescription", scope = OrderDtlRow.class)
    public JAXBElement<String> createOrderDtlRowSalesCatIDDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesCatIDDescription_QNAME, String.class, OrderDtlRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cCustID", scope = CheckCustOnCreditHold.class)
    public JAXBElement<String> createCheckCustOnCreditHoldCCustID(String value) {
        return new JAXBElement<String>(_CheckCustOnCreditHoldCCustID_QNAME, String.class, CheckCustOnCreditHold.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCurrencyCode.class)
    public JAXBElement<SalesOrderTableset> createChangeCurrencyCodeDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCurrencyCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelBuyToOrder.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelBuyToOrderDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelBuyToOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeGroup.class)
    public JAXBElement<SalesOrderTableset> createChangeGroupDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOverridePriceListResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeOverridePriceListResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOverridePriceListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeofTaxRgn.class)
    public JAXBElement<SalesOrderTableset> createOnChangeofTaxRgnDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeofTaxRgn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "iTaxRegionCode", scope = OnChangeofTaxRgn.class)
    public JAXBElement<String> createOnChangeofTaxRgnITaxRegionCode(String value) {
        return new JAXBElement<String>(_OnChangeofTaxRgnITaxRegionCode_QNAME, String.class, OnChangeofTaxRgn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "errMessage", scope = PhantomComponentsResponse.class)
    public JAXBElement<String> createPhantomComponentsResponseErrMessage(String value) {
        return new JAXBElement<String>(_PhantomComponentsResponseErrMessage_QNAME, String.class, PhantomComponentsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cJobNum", scope = JobProdDelete.class)
    public JAXBElement<String> createJobProdDeleteCJobNum(String value) {
        return new JAXBElement<String>(_JobProdDeleteCJobNum_QNAME, String.class, JobProdDelete.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcNeqQtyAction", scope = ChangeSellingQtyMasterResponse.class)
    public JAXBElement<String> createChangeSellingQtyMasterResponsePcNeqQtyAction(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterResponsePcNeqQtyAction_QNAME, String.class, ChangeSellingQtyMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cSellingQuantityChangedMsgText", scope = ChangeSellingQtyMasterResponse.class)
    public JAXBElement<String> createChangeSellingQtyMasterResponseCSellingQuantityChangedMsgText(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterResponseCSellingQuantityChangedMsgText_QNAME, String.class, ChangeSellingQtyMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeSellingQtyMasterResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeSellingQtyMasterResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeSellingQtyMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcMessage", scope = ChangeSellingQtyMasterResponse.class)
    public JAXBElement<String> createChangeSellingQtyMasterResponsePcMessage(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterResponsePcMessage_QNAME, String.class, ChangeSellingQtyMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "opWarningMsg", scope = ChangeSellingQtyMasterResponse.class)
    public JAXBElement<String> createChangeSellingQtyMasterResponseOpWarningMsg(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterResponseOpWarningMsg_QNAME, String.class, ChangeSellingQtyMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ValidateInvQtyResponse.class)
    public JAXBElement<SalesOrderTableset> createValidateInvQtyResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ValidateInvQtyResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "opNegInvMessage", scope = ValidateInvQtyResponse.class)
    public JAXBElement<String> createValidateInvQtyResponseOpNegInvMessage(String value) {
        return new JAXBElement<String>(_ValidateInvQtyResponseOpNegInvMessage_QNAME, String.class, ValidateInvQtyResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "opNegQtyAction", scope = ValidateInvQtyResponse.class)
    public JAXBElement<String> createValidateInvQtyResponseOpNegQtyAction(String value) {
        return new JAXBElement<String>(_ValidateInvQtyResponseOpNegQtyAction_QNAME, String.class, ValidateInvQtyResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "GlbSugPOChg", scope = GlbSugPOChgTableset.class)
    public JAXBElement<GlbSugPOChgTable> createGlbSugPOChgTablesetGlbSugPOChg(GlbSugPOChgTable value) {
        return new JAXBElement<GlbSugPOChgTable>(_GlbSugPOChgTablesetGlbSugPOChg_QNAME, GlbSugPOChgTable.class, GlbSugPOChgTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetRowsResult", scope = GetRowsResponse.class)
    public JAXBElement<SalesOrderTableset> createGetRowsResponseGetRowsResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_GetRowsResponseGetRowsResult_QNAME, SalesOrderTableset.class, GetRowsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "NewCustPartNum", scope = GetSmartStringResponse.class)
    public JAXBElement<String> createGetSmartStringResponseNewCustPartNum(String value) {
        return new JAXBElement<String>(_GetSmartStringResponseNewCustPartNum_QNAME, String.class, GetSmartStringResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "NewPartNum", scope = GetSmartStringResponse.class)
    public JAXBElement<String> createGetSmartStringResponseNewPartNum(String value) {
        return new JAXBElement<String>(_GetSmartStringResponseNewPartNum_QNAME, String.class, GetSmartStringResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "SmartString", scope = GetSmartStringResponse.class)
    public JAXBElement<String> createGetSmartStringResponseSmartString(String value) {
        return new JAXBElement<String>(_GetSmartStringResponseSmartString_QNAME, String.class, GetSmartStringResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Localization", scope = SOEntryUIParamsRow.class)
    public JAXBElement<String> createSOEntryUIParamsRowLocalization(String value) {
        return new JAXBElement<String>(_SOEntryUIParamsRowLocalization_QNAME, String.class, SOEntryUIParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepCode", scope = OrderRepCommRow.class)
    public JAXBElement<String> createOrderRepCommRowSalesRepCode(String value) {
        return new JAXBElement<String>(_OrderRepCommRowSalesRepCode_QNAME, String.class, OrderRepCommRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Seq", scope = OrderRepCommRow.class)
    public JAXBElement<String> createOrderRepCommRowSeq(String value) {
        return new JAXBElement<String>(_OrderRepCommRowSeq_QNAME, String.class, OrderRepCommRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Name", scope = OrderRepCommRow.class)
    public JAXBElement<String> createOrderRepCommRowName(String value) {
        return new JAXBElement<String>(_OrderRepCommRowName_QNAME, String.class, OrderRepCommRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = OrderRepCommRow.class)
    public JAXBElement<String> createOrderRepCommRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, OrderRepCommRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "RevisionNum", scope = GetSmartString.class)
    public JAXBElement<String> createGetSmartStringRevisionNum(String value) {
        return new JAXBElement<String>(_GetSmartStringRevisionNum_QNAME, String.class, GetSmartString.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "PartNum", scope = GetSmartString.class)
    public JAXBElement<String> createGetSmartStringPartNum(String value) {
        return new JAXBElement<String>(_GetSmartStringPartNum_QNAME, String.class, GetSmartString.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangePricePerCode.class)
    public JAXBElement<SalesOrderTableset> createChangePricePerCodeDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangePricePerCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderRelTaxResponse.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderRelTaxResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderRelTaxResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeSoldToIDResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeSoldToIDResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeSoldToIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeOfFixedAmountResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeOfFixedAmountResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeOfFixedAmountResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeNeedByDateResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeNeedByDateResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeNeedByDateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PullFromStockWarehouseDesc", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowPullFromStockWarehouseDesc(String value) {
        return new JAXBElement<String>(_JobProdRowPullFromStockWarehouseDesc_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DemandLinkStatus", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowDemandLinkStatus(String value) {
        return new JAXBElement<String>(_JobProdRowDemandLinkStatus_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartSalesUM", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowPartSalesUM(String value) {
        return new JAXBElement<String>(_JobProdRowPartSalesUM_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WarehouseCode", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowWarehouseCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowWarehouseCode_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AsmPartNum", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowAsmPartNum(String value) {
        return new JAXBElement<String>(_JobProdRowAsmPartNum_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderNumCurrencyCode", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowOrderNumCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowOrderNumCurrencyCode_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartPricePerCode", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowPartPricePerCode(String value) {
        return new JAXBElement<String>(_JobProdRowPartPricePerCode_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MtlPartNum", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowMtlPartNum(String value) {
        return new JAXBElement<String>(_JobProdRowMtlPartNum_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AsmPartDesc", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowAsmPartDesc(String value) {
        return new JAXBElement<String>(_JobProdRowAsmPartDesc_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNum", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNum_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PlanID", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowPlanID(String value) {
        return new JAXBElement<String>(_JobProdRowPlanID_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartIUM", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowPartIUM(String value) {
        return new JAXBElement<String>(_JobProdRowPartIUM_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MtlPartDesc", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowMtlPartDesc(String value) {
        return new JAXBElement<String>(_JobProdRowMtlPartDesc_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "JHPartDesc", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowJHPartDesc(String value) {
        return new JAXBElement<String>(_JobProdRowJHPartDesc_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipBy", scope = JobProdRow.class)
    public JAXBElement<XMLGregorianCalendar> createJobProdRowShipBy(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_JobProdRowShipBy_QNAME, XMLGregorianCalendar.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DemandLinkSource", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowDemandLinkSource(String value) {
        return new JAXBElement<String>(_JobProdRowDemandLinkSource_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MakeToType", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowMakeToType(String value) {
        return new JAXBElement<String>(_JobProdRowMakeToType_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Plant", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowPlant(String value) {
        return new JAXBElement<String>(_JobProdRowPlant_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TFLineNum", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowTFLineNum(String value) {
        return new JAXBElement<String>(_JobProdRowTFLineNum_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TFOrdNum", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowTFOrdNum(String value) {
        return new JAXBElement<String>(_JobProdRowTFOrdNum_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PlanUserID", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowPlanUserID(String value) {
        return new JAXBElement<String>(_OrderDtlRowPlanUserID_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderNumCardMemberName", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowOrderNumCardMemberName(String value) {
        return new JAXBElement<String>(_OrderDtlRowOrderNumCardMemberName_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PullFromStockWarehouseCode", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowPullFromStockWarehouseCode(String value) {
        return new JAXBElement<String>(_JobProdRowPullFromStockWarehouseCode_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "IUM", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowIUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowIUM_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartPartDescription", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowPartPartDescription(String value) {
        return new JAXBElement<String>(_JobProdRowPartPartDescription_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "JHPartNum", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowJHPartNum(String value) {
        return new JAXBElement<String>(_JobProdRowJHPartNum_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "JobNum", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowJobNum(String value) {
        return new JAXBElement<String>(_JobProdRowJobNum_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CallLineLineDesc", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowCallLineLineDesc(String value) {
        return new JAXBElement<String>(_JobProdRowCallLineLineDesc_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "JobNumPartDescription", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowJobNumPartDescription(String value) {
        return new JAXBElement<String>(_JobProdRowJobNumPartDescription_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustName", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowCustName(String value) {
        return new JAXBElement<String>(_JobProdRowCustName_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderLineLineDesc", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowOrderLineLineDesc(String value) {
        return new JAXBElement<String>(_JobProdRowOrderLineLineDesc_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TargetJobNum", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowTargetJobNum(String value) {
        return new JAXBElement<String>(_JobProdRowTargetJobNum_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustID", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowCustID(String value) {
        return new JAXBElement<String>(_JobProdRowCustID_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WarehouseCodeDescription", scope = JobProdRow.class)
    public JAXBElement<String> createJobProdRowWarehouseCodeDescription(String value) {
        return new JAXBElement<String>(_JobProdRowWarehouseCodeDescription_QNAME, String.class, JobProdRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeRelUseOTSResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeRelUseOTSResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeRelUseOTSResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeCreditCardOrder.class)
    public JAXBElement<SalesOrderTableset> createOnChangeCreditCardOrderDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeCreditCardOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ValidateInvQty.class)
    public JAXBElement<SalesOrderTableset> createValidateInvQtyDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ValidateInvQty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DrawDesc", scope = OrderDtlAttchRow.class)
    public JAXBElement<String> createOrderDtlAttchRowDrawDesc(String value) {
        return new JAXBElement<String>(_OrderDtlAttchRowDrawDesc_QNAME, String.class, OrderDtlAttchRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DocTypeID", scope = OrderDtlAttchRow.class)
    public JAXBElement<String> createOrderDtlAttchRowDocTypeID(String value) {
        return new JAXBElement<String>(_OrderDtlAttchRowDocTypeID_QNAME, String.class, OrderDtlAttchRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = OrderDtlAttchRow.class)
    public JAXBElement<String> createOrderDtlAttchRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, OrderDtlAttchRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FileName", scope = OrderDtlAttchRow.class)
    public JAXBElement<String> createOrderDtlAttchRowFileName(String value) {
        return new JAXBElement<String>(_OrderDtlAttchRowFileName_QNAME, String.class, OrderDtlAttchRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PDMDocID", scope = OrderDtlAttchRow.class)
    public JAXBElement<String> createOrderDtlAttchRowPDMDocID(String value) {
        return new JAXBElement<String>(_OrderDtlAttchRowPDMDocID_QNAME, String.class, OrderDtlAttchRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxRateDate", scope = OrderRelTaxRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderRelTaxRowTaxRateDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderRelTaxRowTaxRateDate_QNAME, XMLGregorianCalendar.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangeDate", scope = OrderRelTaxRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderRelTaxRowChangeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowChangeDate_QNAME, XMLGregorianCalendar.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ResolutionNum", scope = OrderRelTaxRow.class)
    public JAXBElement<String> createOrderRelTaxRowResolutionNum(String value) {
        return new JAXBElement<String>(_OrderRelTaxRowResolutionNum_QNAME, String.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = OrderRelTaxRow.class)
    public JAXBElement<String> createOrderRelTaxRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangedBy", scope = OrderRelTaxRow.class)
    public JAXBElement<String> createOrderRelTaxRowChangedBy(String value) {
        return new JAXBElement<String>(_OrderDtlRowChangedBy_QNAME, String.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "EntryProcess", scope = OrderRelTaxRow.class)
    public JAXBElement<String> createOrderRelTaxRowEntryProcess(String value) {
        return new JAXBElement<String>(_OrderDtlRowEntryProcess_QNAME, String.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TextCode", scope = OrderRelTaxRow.class)
    public JAXBElement<String> createOrderRelTaxRowTextCode(String value) {
        return new JAXBElement<String>(_OrderRelTaxRowTextCode_QNAME, String.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ResolutionDate", scope = OrderRelTaxRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderRelTaxRowResolutionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderRelTaxRowResolutionDate_QNAME, XMLGregorianCalendar.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCode", scope = OrderRelTaxRow.class)
    public JAXBElement<String> createOrderRelTaxRowCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrencyCode_QNAME, String.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RateCodeDescDescription", scope = OrderRelTaxRow.class)
    public JAXBElement<String> createOrderRelTaxRowRateCodeDescDescription(String value) {
        return new JAXBElement<String>(_OrderRelTaxRowRateCodeDescDescription_QNAME, String.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesTaxDescDescription", scope = OrderRelTaxRow.class)
    public JAXBElement<String> createOrderRelTaxRowSalesTaxDescDescription(String value) {
        return new JAXBElement<String>(_OrderRelTaxRowSalesTaxDescDescription_QNAME, String.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DocDisplaySymbol", scope = OrderRelTaxRow.class)
    public JAXBElement<String> createOrderRelTaxRowDocDisplaySymbol(String value) {
        return new JAXBElement<String>(_OrderRelTaxRowDocDisplaySymbol_QNAME, String.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DisplaySymbol", scope = OrderRelTaxRow.class)
    public JAXBElement<String> createOrderRelTaxRowDisplaySymbol(String value) {
        return new JAXBElement<String>(_OrderRelTaxRowDisplaySymbol_QNAME, String.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RateCode", scope = OrderRelTaxRow.class)
    public JAXBElement<String> createOrderRelTaxRowRateCode(String value) {
        return new JAXBElement<String>(_OrderRelTaxRowRateCode_QNAME, String.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxCode", scope = OrderRelTaxRow.class)
    public JAXBElement<String> createOrderRelTaxRowTaxCode(String value) {
        return new JAXBElement<String>(_OrderRelTaxRowTaxCode_QNAME, String.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CollectionTypeDescription", scope = OrderRelTaxRow.class)
    public JAXBElement<String> createOrderRelTaxRowCollectionTypeDescription(String value) {
        return new JAXBElement<String>(_OrderRelTaxRowCollectionTypeDescription_QNAME, String.class, OrderRelTaxRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderCustTrkTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderCustTrk", scope = OrderCustTrkTableset.class)
    public JAXBElement<OrderCustTrkTable> createOrderCustTrkTablesetOrderCustTrk(OrderCustTrkTable value) {
        return new JAXBElement<OrderCustTrkTable>(_OrderCustTrkTablesetOrderCustTrk_QNAME, OrderCustTrkTable.class, OrderCustTrkTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangePartNumResponse.class)
    public JAXBElement<SalesOrderTableset> createChangePartNumResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangePartNumResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeNewSellingQuantityResponse.class)
    public JAXBElement<GlbSugPOChgTableset> createChangeNewSellingQuantityResponseDs(GlbSugPOChgTableset value) {
        return new JAXBElement<GlbSugPOChgTableset>(_SetUPSQVEnableResponseDs_QNAME, GlbSugPOChgTableset.class, ChangeNewSellingQuantityResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DisplaySuggestionStatus", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowDisplaySuggestionStatus(String value) {
        return new JAXBElement<String>(_GlbSugPOChgRowDisplaySuggestionStatus_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SourceName", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowSourceName(String value) {
        return new JAXBElement<String>(_GlbSugPOChgRowSourceName_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ABCCode", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowABCCode(String value) {
        return new JAXBElement<String>(_GlbSugPOChgRowABCCode_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RequireDate", scope = GlbSugPOChgRow.class)
    public JAXBElement<XMLGregorianCalendar> createGlbSugPOChgRowRequireDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GlbSugPOChgRowRequireDate_QNAME, XMLGregorianCalendar.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "NewShipByDate", scope = GlbSugPOChgRow.class)
    public JAXBElement<XMLGregorianCalendar> createGlbSugPOChgRowNewShipByDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GlbSugPOChgRowNewShipByDate_QNAME, XMLGregorianCalendar.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "IUM", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowIUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowIUM_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Comment", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowComment(String value) {
        return new JAXBElement<String>(_GlbSugPOChgRowComment_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SuggestionStatus", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowSuggestionStatus(String value) {
        return new JAXBElement<String>(_GlbSugPOChgRowSuggestionStatus_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ExtCompany", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowExtCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowExtCompany_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesIUM", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowSalesIUM(String value) {
        return new JAXBElement<String>(_GlbSugPOChgRowSalesIUM_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SuggestionCode", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowSuggestionCode(String value) {
        return new JAXBElement<String>(_GlbSugPOChgRowSuggestionCode_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "GlbCompany", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowGlbCompany(String value) {
        return new JAXBElement<String>(_GlbSugPOChgRowGlbCompany_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Plant", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowPlant(String value) {
        return new JAXBElement<String>(_JobProdRowPlant_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CancelReason", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowCancelReason(String value) {
        return new JAXBElement<String>(_GlbSugPOChgRowCancelReason_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BuyerID", scope = GlbSugPOChgRow.class)
    public JAXBElement<String> createGlbSugPOChgRowBuyerID(String value) {
        return new JAXBElement<String>(_GlbSugPOChgRowBuyerID_QNAME, String.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TransDate", scope = GlbSugPOChgRow.class)
    public JAXBElement<XMLGregorianCalendar> createGlbSugPOChgRowTransDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GlbSugPOChgRowTransDate_QNAME, XMLGregorianCalendar.class, GlbSugPOChgRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedListTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetListResult", scope = GetListResponse.class)
    public JAXBElement<OrderHedListTableset> createGetListResponseGetListResult(OrderHedListTableset value) {
        return new JAXBElement<OrderHedListTableset>(_GetListResponseGetListResult_QNAME, OrderHedListTableset.class, GetListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeMiscPercentResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeMiscPercentResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeMiscPercentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeSellingQuantity.class)
    public JAXBElement<SalesOrderTableset> createChangeSellingQuantityDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeSellingQuantity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipTranType", scope = GetSelectSerialNumbersParams.class)
    public JAXBElement<String> createGetSelectSerialNumbersParamsIpTranType(String value) {
        return new JAXBElement<String>(_GetSelectSerialNumbersParamsIpTranType_QNAME, String.class, GetSelectSerialNumbersParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipWhseCode", scope = GetSelectSerialNumbersParams.class)
    public JAXBElement<String> createGetSelectSerialNumbersParamsIpWhseCode(String value) {
        return new JAXBElement<String>(_GetSelectSerialNumbersParamsIpWhseCode_QNAME, String.class, GetSelectSerialNumbersParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipPartNum", scope = GetSelectSerialNumbersParams.class)
    public JAXBElement<String> createGetSelectSerialNumbersParamsIpPartNum(String value) {
        return new JAXBElement<String>(_GetSelectSerialNumbersParamsIpPartNum_QNAME, String.class, GetSelectSerialNumbersParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipBinNum", scope = GetSelectSerialNumbersParams.class)
    public JAXBElement<String> createGetSelectSerialNumbersParamsIpBinNum(String value) {
        return new JAXBElement<String>(_GetSelectSerialNumbersParamsIpBinNum_QNAME, String.class, GetSelectSerialNumbersParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "compliantMsg", scope = CheckComplianceOrderFailResponse.class)
    public JAXBElement<String> createCheckComplianceOrderFailResponseCompliantMsg(String value) {
        return new JAXBElement<String>(_CheckComplianceOrderFailResponseCompliantMsg_QNAME, String.class, CheckComplianceOrderFailResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CreateOrderDtlComplements.class)
    public JAXBElement<SalesOrderTableset> createCreateOrderDtlComplementsDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CreateOrderDtlComplements.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeMiscAmountResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeMiscAmountResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeMiscAmountResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ValidateSNs.class)
    public JAXBElement<SalesOrderTableset> createValidateSNsDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ValidateSNs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cReadyToSendMsgText", scope = CheckICPOReadyToSendResponse.class)
    public JAXBElement<String> createCheckICPOReadyToSendResponseCReadyToSendMsgText(String value) {
        return new JAXBElement<String>(_CheckICPOReadyToSendResponseCReadyToSendMsgText_QNAME, String.class, CheckICPOReadyToSendResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "partNum", scope = ChangePartNumMaster.class)
    public JAXBElement<String> createChangePartNumMasterPartNum(String value) {
        return new JAXBElement<String>(_ChangePartNumMasterPartNum_QNAME, String.class, ChangePartNumMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangePartNumMaster.class)
    public JAXBElement<SalesOrderTableset> createChangePartNumMasterDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangePartNumMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "uomCode", scope = ChangePartNumMaster.class)
    public JAXBElement<String> createChangePartNumMasterUomCode(String value) {
        return new JAXBElement<String>(_ChangePartNumMasterUomCode_QNAME, String.class, ChangePartNumMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "rowType", scope = ChangePartNumMaster.class)
    public JAXBElement<String> createChangePartNumMasterRowType(String value) {
        return new JAXBElement<String>(_ChangePartNumMasterRowType_QNAME, String.class, ChangePartNumMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelShipToCustIDResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelShipToCustIDResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelShipToCustIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeofSoldToCreditCheck.class)
    public JAXBElement<SalesOrderTableset> createOnChangeofSoldToCreditCheckDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeofSoldToCreditCheck.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "iCustID", scope = OnChangeofSoldToCreditCheck.class)
    public JAXBElement<String> createOnChangeofSoldToCreditCheckICustID(String value) {
        return new JAXBElement<String>(_OnChangeofSoldToCreditCheckICustID_QNAME, String.class, OnChangeofSoldToCreditCheck.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClause", scope = GetListCustom.class)
    public JAXBElement<String> createGetListCustomWhereClause(String value) {
        return new JAXBElement<String>(_GetListCustomWhereClause_QNAME, String.class, GetListCustom.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "customClause", scope = GetListCustom.class)
    public JAXBElement<String> createGetListCustomCustomClause(String value) {
        return new JAXBElement<String>(_GetListCustomCustomClause_QNAME, String.class, GetListCustom.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "getBreakListCodeDescResult", scope = GetBreakListCodeDescResponse.class)
    public JAXBElement<String> createGetBreakListCodeDescResponseGetBreakListCodeDescResult(String value) {
        return new JAXBElement<String>(_GetBreakListCodeDescResponseGetBreakListCodeDescResult_QNAME, String.class, GetBreakListCodeDescResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeOfTaxPercent.class)
    public JAXBElement<SalesOrderTableset> createOnChangeOfTaxPercentDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeOfTaxPercent.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "TaxCode", scope = OnChangeOfTaxPercent.class)
    public JAXBElement<String> createOnChangeOfTaxPercentTaxCode(String value) {
        return new JAXBElement<String>(_OnChangeOfTaxPercentTaxCode_QNAME, String.class, OnChangeOfTaxPercent.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckProjectID.class)
    public JAXBElement<SalesOrderTableset> createCheckProjectIDDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckProjectID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipProjectID", scope = CheckProjectID.class)
    public JAXBElement<String> createCheckProjectIDIpProjectID(String value) {
        return new JAXBElement<String>(_CheckProjectIDIpProjectID_QNAME, String.class, CheckProjectID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangedCardNumber.class)
    public JAXBElement<SalesOrderTableset> createChangedCardNumberDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangedCardNumber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderMsc.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderMscDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderMsc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "poLinkValues", scope = SelectSerialNumbersParamsRow.class)
    public JAXBElement<String> createSelectSerialNumbersParamsRowPoLinkValues(String value) {
        return new JAXBElement<String>(_SelectSerialNumbersParamsRowPoLinkValues_QNAME, String.class, SelectSerialNumbersParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "xrefPartNum", scope = SelectSerialNumbersParamsRow.class)
    public JAXBElement<String> createSelectSerialNumbersParamsRowXrefPartNum(String value) {
        return new JAXBElement<String>(_SelectSerialNumbersParamsRowXrefPartNum_QNAME, String.class, SelectSerialNumbersParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "partNum", scope = SelectSerialNumbersParamsRow.class)
    public JAXBElement<String> createSelectSerialNumbersParamsRowPartNum(String value) {
        return new JAXBElement<String>(_SelectSerialNumbersParamsRowPartNum_QNAME, String.class, SelectSerialNumbersParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "xrefPartType", scope = SelectSerialNumbersParamsRow.class)
    public JAXBElement<String> createSelectSerialNumbersParamsRowXrefPartType(String value) {
        return new JAXBElement<String>(_SelectSerialNumbersParamsRowXrefPartType_QNAME, String.class, SelectSerialNumbersParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "whereClause", scope = SelectSerialNumbersParamsRow.class)
    public JAXBElement<String> createSelectSerialNumbersParamsRowWhereClause(String value) {
        return new JAXBElement<String>(_SelectSerialNumbersParamsRowWhereClause_QNAME, String.class, SelectSerialNumbersParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "transType", scope = SelectSerialNumbersParamsRow.class)
    public JAXBElement<String> createSelectSerialNumbersParamsRowTransType(String value) {
        return new JAXBElement<String>(_SelectSerialNumbersParamsRowTransType_QNAME, String.class, SelectSerialNumbersParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "plant", scope = SelectSerialNumbersParamsRow.class)
    public JAXBElement<String> createSelectSerialNumbersParamsRowPlant(String value) {
        return new JAXBElement<String>(_SelectSerialNumbersParamsRowPlant_QNAME, String.class, SelectSerialNumbersParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangePartNum.class)
    public JAXBElement<SalesOrderTableset> createChangePartNumDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangePartNum.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "uomCode", scope = ChangePartNum.class)
    public JAXBElement<String> createChangePartNumUomCode(String value) {
        return new JAXBElement<String>(_ChangePartNumMasterUomCode_QNAME, String.class, ChangePartNum.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangedCardNumberResponse.class)
    public JAXBElement<SalesOrderTableset> createChangedCardNumberResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangedCardNumberResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderMscResponse.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderMscResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderMscResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCurrencyCodeResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeCurrencyCodeResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCurrencyCodeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MarkUpID", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowMarkUpID(String value) {
        return new JAXBElement<String>(_QuoteQtyRowMarkUpID_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesUM", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowSalesUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesUM_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangeDate", scope = QuoteQtyRow.class)
    public JAXBElement<XMLGregorianCalendar> createQuoteQtyRowChangeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowChangeDate_QNAME, XMLGregorianCalendar.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PercentType", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowPercentType(String value) {
        return new JAXBElement<String>(_QuoteQtyRowPercentType_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrSymbol", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowCurrSymbol(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrSymbol_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "IUM", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowIUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowIUM_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "QuoteNumCurrencyCode", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowQuoteNumCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowQuoteNumCurrencyCode_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SellingFactorDirection", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowSellingFactorDirection(String value) {
        return new JAXBElement<String>(_OrderDtlRowSellingFactorDirection_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangedBy", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowChangedBy(String value) {
        return new JAXBElement<String>(_OrderDtlRowChangedBy_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PriceSource", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowPriceSource(String value) {
        return new JAXBElement<String>(_QuoteQtyRowPriceSource_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MiscCostDesc", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowMiscCostDesc(String value) {
        return new JAXBElement<String>(_QuoteQtyRowMiscCostDesc_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PricePerCode", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowPricePerCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowPricePerCode_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BaseCurrSymbol", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowBaseCurrSymbol(String value) {
        return new JAXBElement<String>(_OrderDtlRowBaseCurrSymbol_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MiscChrg", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowMiscChrg(String value) {
        return new JAXBElement<String>(_QuoteQtyRowMiscChrg_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCode", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrencyCode_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "QuoteLineLineDesc", scope = QuoteQtyRow.class)
    public JAXBElement<String> createQuoteQtyRowQuoteLineLineDesc(String value) {
        return new JAXBElement<String>(_QuoteQtyRowQuoteLineLineDesc_QNAME, String.class, QuoteQtyRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderRel.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderRelDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderRel.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesUM", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowSalesUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesUM_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ProdCodeDescription", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowProdCodeDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowProdCodeDescription_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PONum", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowPONum(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowPONum_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RevisionNum", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowRevisionNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowRevisionNum_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipToNum", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowShipToNum(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowShipToNum_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNum", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNum_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "XPartNum", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowXPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowXPartNum_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDtlNeedByDate", scope = OrderCustTrkRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderCustTrkRowOrderDtlNeedByDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderCustTrkRowOrderDtlNeedByDate_QNAME, XMLGregorianCalendar.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustomerName", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowCustomerName(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowCustomerName_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SoldToCustName", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowSoldToCustName(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowSoldToCustName_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCode", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrencyCode_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "POLine", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowPOLine(String value) {
        return new JAXBElement<String>(_OrderDtlRowPOLine_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipToName", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowShipToName(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowShipToName_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "XRevisionNum", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowXRevisionNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowXRevisionNum_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LineDesc", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowLineDesc(String value) {
        return new JAXBElement<String>(_OrderDtlRowLineDesc_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedRequestDate", scope = OrderCustTrkRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderCustTrkRowOrderHedRequestDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderCustTrkRowOrderHedRequestDate_QNAME, XMLGregorianCalendar.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ProdCode", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowProdCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowProdCode_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTContactPhoneNum", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowBTContactPhoneNum(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowBTContactPhoneNum_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDate", scope = OrderCustTrkRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderCustTrkRowOrderDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderCustTrkRowOrderDate_QNAME, XMLGregorianCalendar.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTAddressList", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowBTAddressList(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowBTAddressList_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "POLineRef", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowPOLineRef(String value) {
        return new JAXBElement<String>(_OrderDtlRowPOLineRef_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTCustomerName", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowBTCustomerName(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowBTCustomerName_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedNeedByDate", scope = OrderCustTrkRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderCustTrkRowOrderHedNeedByDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderCustTrkRowOrderHedNeedByDate_QNAME, XMLGregorianCalendar.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTContactFaxNum", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowBTContactFaxNum(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowBTContactFaxNum_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDtlRequestDate", scope = OrderCustTrkRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderCustTrkRowOrderDtlRequestDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderCustTrkRowOrderDtlRequestDate_QNAME, XMLGregorianCalendar.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTContactName", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowBTContactName(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowBTContactName_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTCustID", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowBTCustID(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowBTCustID_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustID", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowCustID(String value) {
        return new JAXBElement<String>(_JobProdRowCustID_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SoldToCustID", scope = OrderCustTrkRow.class)
    public JAXBElement<String> createOrderCustTrkRowSoldToCustID(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowSoldToCustID_QNAME, String.class, OrderCustTrkRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeGroupResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeGroupResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeGroupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "opMessage", scope = CheckSONumResponse.class)
    public JAXBElement<String> createCheckSONumResponseOpMessage(String value) {
        return new JAXBElement<String>(_CheckSONumResponseOpMessage_QNAME, String.class, CheckSONumResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOEntryUIParamsTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetUIParamsResult", scope = GetUIParamsResponse.class)
    public JAXBElement<SOEntryUIParamsTableset> createGetUIParamsResponseGetUIParamsResult(SOEntryUIParamsTableset value) {
        return new JAXBElement<SOEntryUIParamsTableset>(_GetUIParamsResponseGetUIParamsResult_QNAME, SOEntryUIParamsTableset.class, GetUIParamsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckOrderHedChangesResponse.class)
    public JAXBElement<SalesOrderTableset> createCheckOrderHedChangesResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckOrderHedChangesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cOrderChangedMsgText", scope = CheckOrderHedChangesResponse.class)
    public JAXBElement<String> createCheckOrderHedChangesResponseCOrderChangedMsgText(String value) {
        return new JAXBElement<String>(_CheckOrderHedChangesResponseCOrderChangedMsgText_QNAME, String.class, CheckOrderHedChangesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "sourceRev", scope = CheckConfiguration.class)
    public JAXBElement<String> createCheckConfigurationSourceRev(String value) {
        return new JAXBElement<String>(_CheckConfigurationSourceRev_QNAME, String.class, CheckConfiguration.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "sourcePart", scope = CheckConfiguration.class)
    public JAXBElement<String> createCheckConfigurationSourcePart(String value) {
        return new JAXBElement<String>(_CheckConfigurationSourcePart_QNAME, String.class, CheckConfiguration.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelVendorIDResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelVendorIDResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelVendorIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "iPoNum", scope = CreateOrderFromQuote.class)
    public JAXBElement<String> createCreateOrderFromQuoteIPoNum(String value) {
        return new JAXBElement<String>(_CreateOrderFromQuoteIPoNum_QNAME, String.class, CreateOrderFromQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "iCreditStatus", scope = CreateOrderFromQuote.class)
    public JAXBElement<String> createCreateOrderFromQuoteICreditStatus(String value) {
        return new JAXBElement<String>(_CreateOrderFromQuoteICreditStatus_QNAME, String.class, CreateOrderFromQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckCustomerCreditLimitResponse.class)
    public JAXBElement<SalesOrderTableset> createCheckCustomerCreditLimitResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckCustomerCreditLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cCreditLimitMessage", scope = CheckCustomerCreditLimitResponse.class)
    public JAXBElement<String> createCheckCustomerCreditLimitResponseCCreditLimitMessage(String value) {
        return new JAXBElement<String>(_CheckCustomerCreditLimitResponseCCreditLimitMessage_QNAME, String.class, CheckCustomerCreditLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOverrideDiscPriceListResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeOverrideDiscPriceListResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOverrideDiscPriceListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewSalesKitResponse.class)
    public JAXBElement<SalesOrderTableset> createGetNewSalesKitResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewSalesKitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "NewBillToCustID", scope = ChangeBTCustIDMaster.class)
    public JAXBElement<String> createChangeBTCustIDMasterNewBillToCustID(String value) {
        return new JAXBElement<String>(_ChangeBTCustIDMasterNewBillToCustID_QNAME, String.class, ChangeBTCustIDMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeBTCustIDMaster.class)
    public JAXBElement<SalesOrderTableset> createChangeBTCustIDMasterDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeBTCustIDMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOHOrderMscResponse.class)
    public JAXBElement<SalesOrderTableset> createGetNewOHOrderMscResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOHOrderMscResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Helplink", scope = ETCMessageRow.class)
    public JAXBElement<String> createETCMessageRowHelplink(String value) {
        return new JAXBElement<String>(_ETCMessageRowHelplink_QNAME, String.class, ETCMessageRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Summary", scope = ETCMessageRow.class)
    public JAXBElement<String> createETCMessageRowSummary(String value) {
        return new JAXBElement<String>(_ETCMessageRowSummary_QNAME, String.class, ETCMessageRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Severity", scope = ETCMessageRow.class)
    public JAXBElement<String> createETCMessageRowSeverity(String value) {
        return new JAXBElement<String>(_ETCMessageRowSeverity_QNAME, String.class, ETCMessageRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AddrSourceID", scope = ETCMessageRow.class)
    public JAXBElement<String> createETCMessageRowAddrSourceID(String value) {
        return new JAXBElement<String>(_ETCMessageRowAddrSourceID_QNAME, String.class, ETCMessageRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AddrSource", scope = ETCMessageRow.class)
    public JAXBElement<String> createETCMessageRowAddrSource(String value) {
        return new JAXBElement<String>(_ETCMessageRowAddrSource_QNAME, String.class, ETCMessageRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Source", scope = ETCMessageRow.class)
    public JAXBElement<String> createETCMessageRowSource(String value) {
        return new JAXBElement<String>(_ETCMessageRowSource_QNAME, String.class, ETCMessageRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Name", scope = ETCMessageRow.class)
    public JAXBElement<String> createETCMessageRowName(String value) {
        return new JAXBElement<String>(_OrderRepCommRowName_QNAME, String.class, ETCMessageRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TransactionID", scope = ETCMessageRow.class)
    public JAXBElement<String> createETCMessageRowTransactionID(String value) {
        return new JAXBElement<String>(_ETCMessageRowTransactionID_QNAME, String.class, ETCMessageRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = ETCMessageRow.class)
    public JAXBElement<String> createETCMessageRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, ETCMessageRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Details", scope = ETCMessageRow.class)
    public JAXBElement<String> createETCMessageRowDetails(String value) {
        return new JAXBElement<String>(_ETCMessageRowDetails_QNAME, String.class, ETCMessageRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RequestID", scope = ETCMessageRow.class)
    public JAXBElement<String> createETCMessageRowRequestID(String value) {
        return new JAXBElement<String>(_ETCMessageRowRequestID_QNAME, String.class, ETCMessageRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RefersTo", scope = ETCMessageRow.class)
    public JAXBElement<String> createETCMessageRowRefersTo(String value) {
        return new JAXBElement<String>(_ETCMessageRowRefersTo_QNAME, String.class, ETCMessageRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeDiscountAmount.class)
    public JAXBElement<SalesOrderTableset> createChangeDiscountAmountDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeDiscountAmount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CCClear.class)
    public JAXBElement<SalesOrderTableset> createCCClearDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CCClear.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckCustomerCreditRelease.class)
    public JAXBElement<SalesOrderTableset> createCheckCustomerCreditReleaseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckCustomerCreditRelease.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JobProdTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "JobProd", scope = OrdRelJobProdTableset.class)
    public JAXBElement<JobProdTable> createOrdRelJobProdTablesetJobProd(JobProdTable value) {
        return new JAXBElement<JobProdTable>(_OrdRelJobProdTablesetJobProd_QNAME, JobProdTable.class, OrdRelJobProdTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cTableName", scope = ChangeNeedByDate.class)
    public JAXBElement<String> createChangeNeedByDateCTableName(String value) {
        return new JAXBElement<String>(_ChangeNeedByDateCTableName_QNAME, String.class, ChangeNeedByDate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeNeedByDate.class)
    public JAXBElement<SalesOrderTableset> createChangeNeedByDateDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeNeedByDate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderHedUPS.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderHedUPSDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderHedUPS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeSellingReqQtyResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeSellingReqQtyResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeSellingReqQtyResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "opWarningMsg", scope = ChangeSellingReqQtyResponse.class)
    public JAXBElement<String> createChangeSellingReqQtyResponseOpWarningMsg(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterResponseOpWarningMsg_QNAME, String.class, ChangeSellingReqQtyResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTCustNumName", scope = OrderHedListRow.class)
    public JAXBElement<String> createOrderHedListRowBTCustNumName(String value) {
        return new JAXBElement<String>(_OrderHedListRowBTCustNumName_QNAME, String.class, OrderHedListRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustomerName", scope = OrderHedListRow.class)
    public JAXBElement<String> createOrderHedListRowCustomerName(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowCustomerName_QNAME, String.class, OrderHedListRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PONum", scope = OrderHedListRow.class)
    public JAXBElement<String> createOrderHedListRowPONum(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowPONum_QNAME, String.class, OrderHedListRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDate", scope = OrderHedListRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedListRowOrderDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderCustTrkRowOrderDate_QNAME, XMLGregorianCalendar.class, OrderHedListRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCode", scope = OrderHedListRow.class)
    public JAXBElement<String> createOrderHedListRowCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrencyCode_QNAME, String.class, OrderHedListRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = OrderHedListRow.class)
    public JAXBElement<String> createOrderHedListRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, OrderHedListRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DemandContract", scope = OrderHedListRow.class)
    public JAXBElement<String> createOrderHedListRowDemandContract(String value) {
        return new JAXBElement<String>(_OrderHedListRowDemandContract_QNAME, String.class, OrderHedListRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTCustNumCustID", scope = OrderHedListRow.class)
    public JAXBElement<String> createOrderHedListRowBTCustNumCustID(String value) {
        return new JAXBElement<String>(_OrderHedListRowBTCustNumCustID_QNAME, String.class, OrderHedListRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustomerBTName", scope = OrderHedListRow.class)
    public JAXBElement<String> createOrderHedListRowCustomerBTName(String value) {
        return new JAXBElement<String>(_OrderHedListRowCustomerBTName_QNAME, String.class, OrderHedListRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustomerCustID", scope = OrderHedListRow.class)
    public JAXBElement<String> createOrderHedListRowCustomerCustID(String value) {
        return new JAXBElement<String>(_OrderHedListRowCustomerCustID_QNAME, String.class, OrderHedListRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "NeedByDate", scope = OrderHedListRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedListRowNeedByDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowNeedByDate_QNAME, XMLGregorianCalendar.class, OrderHedListRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BOUpdErrorTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice", name = "BOUpdError", scope = BOUpdErrorTableset.class)
    public JAXBElement<BOUpdErrorTable> createBOUpdErrorTablesetBOUpdError(BOUpdErrorTable value) {
        return new JAXBElement<BOUpdErrorTable>(_BOUpdErrorTablesetBOUpdError_QNAME, BOUpdErrorTable.class, BOUpdErrorTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelShipToResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelShipToResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelShipToResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSSaveAs", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSSaveAs(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSSaveAs_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSCity", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSCity(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSCity_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSPhoneNum", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSPhoneNum(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSPhoneNum_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSTaxRegionCode", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSTaxRegionCode(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSTaxRegionCode_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSName", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSName(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSName_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSSaveCustID", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSSaveCustID(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSSaveCustID_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSZIP", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSZIP(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSZIP_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSAddress1", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSAddress1(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSAddress1_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSResaleID", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSResaleID(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSResaleID_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSContact", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSContact(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSContact_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSAddress2", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSAddress2(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSAddress2_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSAddress3", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSAddress3(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSAddress3_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSFaxNum", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSFaxNum(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSFaxNum_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSState", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSState(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSState_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSShipToNum", scope = SaveOTSParamsRow.class)
    public JAXBElement<String> createSaveOTSParamsRowOTSShipToNum(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSShipToNum_QNAME, String.class, SaveOTSParamsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelMFCustIDResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelMFCustIDResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelMFCustIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "proposedRateCode", scope = OnChangeRateCode.class)
    public JAXBElement<String> createOnChangeRateCodeProposedRateCode(String value) {
        return new JAXBElement<String>(_OnChangeRateCodeProposedRateCode_QNAME, String.class, OnChangeRateCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeRateCode.class)
    public JAXBElement<SalesOrderTableset> createOnChangeRateCodeDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeRateCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CreateLinesFromHistoryResponse.class)
    public JAXBElement<SalesOrderTableset> createCreateLinesFromHistoryResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CreateLinesFromHistoryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeSalesUOMResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeSalesUOMResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeSalesUOMResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeRenewalNbr.class)
    public JAXBElement<SalesOrderTableset> createChangeRenewalNbrDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeRenewalNbr.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "OutMessage", scope = CopyOrderResponse.class)
    public JAXBElement<String> createCopyOrderResponseOutMessage(String value) {
        return new JAXBElement<String>(_CopyOrderResponseOutMessage_QNAME, String.class, CopyOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cTableName", scope = MasterUpdate.class)
    public JAXBElement<String> createMasterUpdateCTableName(String value) {
        return new JAXBElement<String>(_ChangeNeedByDateCTableName_QNAME, String.class, MasterUpdate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = MasterUpdate.class)
    public JAXBElement<SalesOrderTableset> createMasterUpdateDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, MasterUpdate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeMake.class)
    public JAXBElement<SalesOrderTableset> createChangeMakeDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeMake.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangePriceList.class)
    public JAXBElement<SalesOrderTableset> createChangePriceListDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangePriceList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ReopenReleaseResult", scope = ReopenReleaseResponse.class)
    public JAXBElement<SalesOrderTableset> createReopenReleaseResponseReopenReleaseResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_ReopenReleaseResponseReopenReleaseResult_QNAME, SalesOrderTableset.class, ReopenReleaseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelMarkForNumResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelMarkForNumResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelMarkForNumResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = SetUPSQVEnable.class)
    public JAXBElement<SalesOrderTableset> createSetUPSQVEnableDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, SetUPSQVEnable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ProcessCounterSale.class)
    public JAXBElement<SalesOrderTableset> createProcessCounterSaleDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ProcessCounterSale.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = RebuildShipUPSResponse.class)
    public JAXBElement<SalesOrderTableset> createRebuildShipUPSResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, RebuildShipUPSResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = SetReadyToCalc.class)
    public JAXBElement<SalesOrderTableset> createSetReadyToCalcDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, SetReadyToCalc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangePriceListResponse.class)
    public JAXBElement<SalesOrderTableset> createChangePriceListResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangePriceListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcLineWarehouse", scope = GetBreakListCodeDesc.class)
    public JAXBElement<String> createGetBreakListCodeDescPcLineWarehouse(String value) {
        return new JAXBElement<String>(_GetBreakListCodeDescPcLineWarehouse_QNAME, String.class, GetBreakListCodeDesc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcPartNum", scope = GetBreakListCodeDesc.class)
    public JAXBElement<String> createGetBreakListCodeDescPcPartNum(String value) {
        return new JAXBElement<String>(_GetBreakListCodeDescPcPartNum_QNAME, String.class, GetBreakListCodeDesc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcListCode", scope = GetBreakListCodeDesc.class)
    public JAXBElement<String> createGetBreakListCodeDescPcListCode(String value) {
        return new JAXBElement<String>(_GetBreakListCodeDescPcListCode_QNAME, String.class, GetBreakListCodeDesc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcCurrencyCode", scope = GetBreakListCodeDesc.class)
    public JAXBElement<String> createGetBreakListCodeDescPcCurrencyCode(String value) {
        return new JAXBElement<String>(_GetBreakListCodeDescPcCurrencyCode_QNAME, String.class, GetBreakListCodeDesc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pdtOrderDate", scope = GetBreakListCodeDesc.class)
    public JAXBElement<XMLGregorianCalendar> createGetBreakListCodeDescPdtOrderDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetBreakListCodeDescPdtOrderDate_QNAME, XMLGregorianCalendar.class, GetBreakListCodeDesc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ErrorMessage", scope = TaxConnectStatusRow.class)
    public JAXBElement<String> createTaxConnectStatusRowErrorMessage(String value) {
        return new JAXBElement<String>(_TaxConnectStatusRowErrorMessage_QNAME, String.class, TaxConnectStatusRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = TaxConnectStatusRow.class)
    public JAXBElement<String> createTaxConnectStatusRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, TaxConnectStatusRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "CloseOrderResult", scope = CloseOrderResponse.class)
    public JAXBElement<SalesOrderTableset> createCloseOrderResponseCloseOrderResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_CloseOrderResponseCloseOrderResult_QNAME, SalesOrderTableset.class, CloseOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ProcessQuickEntry.class)
    public JAXBElement<SalesOrderTableset> createProcessQuickEntryDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ProcessQuickEntry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeSellingQuantityResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeSellingQuantityResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeSellingQuantityResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "opWarningMsg", scope = ChangeSellingQuantityResponse.class)
    public JAXBElement<String> createChangeSellingQuantityResponseOpWarningMsg(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterResponseOpWarningMsg_QNAME, String.class, ChangeSellingQuantityResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTMFState", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTMFState(String value) {
        return new JAXBElement<String>(_OrderRelRowOTMFState_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNumPricePerCode", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPartNumPricePerCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNumPricePerCode_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTMFCountryDescription", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTMFCountryDescription(String value) {
        return new JAXBElement<String>(_OrderRelRowOTMFCountryDescription_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSCity", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSCity(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSCity_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RefNotes", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowRefNotes(String value) {
        return new JAXBElement<String>(_OrderRelRowRefNotes_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RevisionNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowRevisionNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowRevisionNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipViaCodeDescription", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowShipViaCodeDescription(String value) {
        return new JAXBElement<String>(_OrderRelRowShipViaCodeDescription_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTMFName", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTMFName(String value) {
        return new JAXBElement<String>(_OrderRelRowOTMFName_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServInstruct", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowServInstruct(String value) {
        return new JAXBElement<String>(_OrderRelRowServInstruct_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "GroundType", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowGroundType(String value) {
        return new JAXBElement<String>(_OrderRelRowGroundType_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TPShipToName", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowTPShipToName(String value) {
        return new JAXBElement<String>(_OrderRelRowTPShipToName_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipViaCode", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowShipViaCode(String value) {
        return new JAXBElement<String>(_OrderRelRowShipViaCode_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipToAddressList", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowShipToAddressList(String value) {
        return new JAXBElement<String>(_OrderRelRowShipToAddressList_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "VendorNumTermsCode", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowVendorNumTermsCode(String value) {
        return new JAXBElement<String>(_OrderRelRowVendorNumTermsCode_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipRouting", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowShipRouting(String value) {
        return new JAXBElement<String>(_OrderRelRowShipRouting_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PlantName", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPlantName(String value) {
        return new JAXBElement<String>(_OrderRelRowPlantName_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PurPointAddress2", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPurPointAddress2(String value) {
        return new JAXBElement<String>(_OrderRelRowPurPointAddress2_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PurPointAddress1", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPurPointAddress1(String value) {
        return new JAXBElement<String>(_OrderRelRowPurPointAddress1_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTMFCity", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTMFCity(String value) {
        return new JAXBElement<String>(_OrderRelRowOTMFCity_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CumeDate", scope = OrderRelRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderRelRowCumeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowCumeDate_QNAME, XMLGregorianCalendar.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderNumCardMemberName", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOrderNumCardMemberName(String value) {
        return new JAXBElement<String>(_OrderDtlRowOrderNumCardMemberName_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PurPointAddress3", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPurPointAddress3(String value) {
        return new JAXBElement<String>(_OrderRelRowPurPointAddress3_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TransportID", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowTransportID(String value) {
        return new JAXBElement<String>(_OrderRelRowTransportID_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "IUM", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowIUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowIUM_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSName", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSName(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSName_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PurPointCountry", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPurPointCountry(String value) {
        return new JAXBElement<String>(_OrderRelRowPurPointCountry_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTMFPhoneNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTMFPhoneNum(String value) {
        return new JAXBElement<String>(_OrderRelRowOTMFPhoneNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PurPointName", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPurPointName(String value) {
        return new JAXBElement<String>(_OrderRelRowPurPointName_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "EntityUseCode", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowEntityUseCode(String value) {
        return new JAXBElement<String>(_OrderRelRowEntityUseCode_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSZIP", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSZIP(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSZIP_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "EntryProcess", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowEntryProcess(String value) {
        return new JAXBElement<String>(_OrderDtlRowEntryProcess_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServAuthNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowServAuthNum(String value) {
        return new JAXBElement<String>(_OrderRelRowServAuthNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderLineLineDesc", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOrderLineLineDesc(String value) {
        return new JAXBElement<String>(_JobProdRowOrderLineLineDesc_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ProjectID", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowProjectID(String value) {
        return new JAXBElement<String>(_OrderDtlRowProjectID_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesUM", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowSalesUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesUM_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTMFFaxNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTMFFaxNum(String value) {
        return new JAXBElement<String>(_OrderRelRowOTMFFaxNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTMFContact", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTMFContact(String value) {
        return new JAXBElement<String>(_OrderRelRowOTMFContact_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DockingStation", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowDockingStation(String value) {
        return new JAXBElement<String>(_OrderRelRowDockingStation_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTMFZIP", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTMFZIP(String value) {
        return new JAXBElement<String>(_OrderRelRowOTMFZIP_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WIOrder", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowWIOrder(String value) {
        return new JAXBElement<String>(_OrderRelRowWIOrder_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DropShipName", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowDropShipName(String value) {
        return new JAXBElement<String>(_OrderRelRowDropShipName_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PurPointCity", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPurPointCity(String value) {
        return new JAXBElement<String>(_OrderRelRowPurPointCity_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "StagingBinNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowStagingBinNum(String value) {
        return new JAXBElement<String>(_OrderRelRowStagingBinNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "NotifyEMail", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowNotifyEMail(String value) {
        return new JAXBElement<String>(_OrderRelRowNotifyEMail_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DspRevMethod", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowDspRevMethod(String value) {
        return new JAXBElement<String>(_OrderRelRowDspRevMethod_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustomerName", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowCustomerName(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowCustomerName_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LineType", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowLineType(String value) {
        return new JAXBElement<String>(_OrderDtlRowLineType_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RelStatus", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowRelStatus(String value) {
        return new JAXBElement<String>(_OrderRelRowRelStatus_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SubShipTo", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowSubShipTo(String value) {
        return new JAXBElement<String>(_OrderRelRowSubShipTo_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "VendorNumAddress1", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowVendorNumAddress1(String value) {
        return new JAXBElement<String>(_OrderRelRowVendorNumAddress1_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Plant", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPlant(String value) {
        return new JAXBElement<String>(_JobProdRowPlant_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "VendorNumAddress3", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowVendorNumAddress3(String value) {
        return new JAXBElement<String>(_OrderRelRowVendorNumAddress3_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "VendorNumAddress2", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowVendorNumAddress2(String value) {
        return new JAXBElement<String>(_OrderRelRowVendorNumAddress2_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSSaveAs", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSSaveAs(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSSaveAs_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustomerCustID", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowCustomerCustID(String value) {
        return new JAXBElement<String>(_OrderHedListRowCustomerCustID_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ComplianceMsg", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowComplianceMsg(String value) {
        return new JAXBElement<String>(_OrderRelRowComplianceMsg_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ECCPlant", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowECCPlant(String value) {
        return new JAXBElement<String>(_OrderDtlRowECCPlant_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSAddress1", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSAddress1(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSAddress1_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TPShipToCustID", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowTPShipToCustID(String value) {
        return new JAXBElement<String>(_OrderRelRowTPShipToCustID_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ExtCompany", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowExtCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowExtCompany_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTMFAddress1", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTMFAddress1(String value) {
        return new JAXBElement<String>(_OrderRelRowOTMFAddress1_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTMFAddress2", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTMFAddress2(String value) {
        return new JAXBElement<String>(_OrderRelRowOTMFAddress2_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CreditLimitMessage", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowCreditLimitMessage(String value) {
        return new JAXBElement<String>(_OrderDtlRowCreditLimitMessage_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTMFAddress3", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTMFAddress3(String value) {
        return new JAXBElement<String>(_OrderRelRowOTMFAddress3_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSAddress2", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSAddress2(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSAddress2_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PrevReqDate", scope = OrderRelRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderRelRowPrevReqDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderRelRowPrevReqDate_QNAME, XMLGregorianCalendar.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RAN", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowRAN(String value) {
        return new JAXBElement<String>(_OrderRelRowRAN_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Location", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowLocation(String value) {
        return new JAXBElement<String>(_OrderRelRowLocation_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSAddress3", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSAddress3(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSAddress3_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "VendorNumState", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowVendorNumState(String value) {
        return new JAXBElement<String>(_OrderRelRowVendorNumState_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSCntryDescription", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSCntryDescription(String value) {
        return new JAXBElement<String>(_OrderRelRowOTSCntryDescription_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipViaCodeWebDesc", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowShipViaCodeWebDesc(String value) {
        return new JAXBElement<String>(_OrderRelRowShipViaCodeWebDesc_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MFCustID", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowMFCustID(String value) {
        return new JAXBElement<String>(_OrderRelRowMFCustID_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WarehouseCode", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowWarehouseCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowWarehouseCode_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSPhoneNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSPhoneNum(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSPhoneNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSTaxRegionCode", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSTaxRegionCode(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSTaxRegionCode_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MarkForNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowMarkForNum(String value) {
        return new JAXBElement<String>(_OrderRelRowMarkForNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSSaveCustID", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSSaveCustID(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSSaveCustID_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DemandReference", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowDemandReference(String value) {
        return new JAXBElement<String>(_OrderRelRowDemandReference_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipToNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowShipToNum(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowShipToNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "VendorNumCurrencyCode", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowVendorNumCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderRelRowVendorNumCurrencyCode_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PickError", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPickError(String value) {
        return new JAXBElement<String>(_OrderRelRowPickError_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PurPoint", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPurPoint(String value) {
        return new JAXBElement<String>(_OrderRelRowPurPoint_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ReleaseStatus", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowReleaseStatus(String value) {
        return new JAXBElement<String>(_OrderRelRowReleaseStatus_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DeliveryType", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowDeliveryType(String value) {
        return new JAXBElement<String>(_OrderRelRowDeliveryType_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCode", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrencyCode_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "StagingWarehouseCode", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowStagingWarehouseCode(String value) {
        return new JAXBElement<String>(_OrderRelRowStagingWarehouseCode_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangeDate", scope = OrderRelRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderRelRowChangeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowChangeDate_QNAME, XMLGregorianCalendar.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PrevPartNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPrevPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowPrevPartNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SellingFactorDirection", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowSellingFactorDirection(String value) {
        return new JAXBElement<String>(_OrderDtlRowSellingFactorDirection_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangedBy", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowChangedBy(String value) {
        return new JAXBElement<String>(_OrderDtlRowChangedBy_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DatePickTicketPrinted", scope = OrderRelRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderRelRowDatePickTicketPrinted(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderRelRowDatePickTicketPrinted_QNAME, XMLGregorianCalendar.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PrevNeedByDate", scope = OrderRelRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderRelRowPrevNeedByDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderRelRowPrevNeedByDate_QNAME, XMLGregorianCalendar.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "VendorNumVendorID", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowVendorNumVendorID(String value) {
        return new JAXBElement<String>(_OrderRelRowVendorNumVendorID_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSContact", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSContact(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSContact_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ReqDate", scope = OrderRelRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderRelRowReqDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderRelRowReqDate_QNAME, XMLGregorianCalendar.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "VendorNumDefaultFOB", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowVendorNumDefaultFOB(String value) {
        return new JAXBElement<String>(_OrderRelRowVendorNumDefaultFOB_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PrevShipToNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPrevShipToNum(String value) {
        return new JAXBElement<String>(_OrderRelRowPrevShipToNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PurPointState", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPurPointState(String value) {
        return new JAXBElement<String>(_OrderRelRowPurPointState_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNumSalesUM", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPartNumSalesUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNumSalesUM_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxRegionCode", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowTaxRegionCode(String value) {
        return new JAXBElement<String>(_OrderRelRowTaxRegionCode_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MarkForAddrList", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowMarkForAddrList(String value) {
        return new JAXBElement<String>(_OrderRelRowMarkForAddrList_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderNumCurrencyCode", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOrderNumCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowOrderNumCurrencyCode_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PhaseID", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPhaseID(String value) {
        return new JAXBElement<String>(_OrderRelRowPhaseID_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServPhone", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowServPhone(String value) {
        return new JAXBElement<String>(_OrderRelRowServPhone_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PurPointZip", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPurPointZip(String value) {
        return new JAXBElement<String>(_OrderRelRowPurPointZip_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "VendorNumName", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowVendorNumName(String value) {
        return new JAXBElement<String>(_OrderRelRowVendorNumName_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSResaleID", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSResaleID(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSResaleID_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "VendorNumZIP", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowVendorNumZIP(String value) {
        return new JAXBElement<String>(_OrderRelRowVendorNumZIP_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServRef5", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowServRef5(String value) {
        return new JAXBElement<String>(_OrderRelRowServRef5_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServRef2", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowServRef2(String value) {
        return new JAXBElement<String>(_OrderRelRowServRef2_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSState", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSState(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSState_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNumPartDescription", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPartNumPartDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNumPartDescription_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServRef1", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowServRef1(String value) {
        return new JAXBElement<String>(_OrderRelRowServRef1_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PrevXPartNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPrevXPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowPrevXPartNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServRef4", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowServRef4(String value) {
        return new JAXBElement<String>(_OrderRelRowServRef4_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServRef3", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowServRef3(String value) {
        return new JAXBElement<String>(_OrderRelRowServRef3_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TPShipToBTName", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowTPShipToBTName(String value) {
        return new JAXBElement<String>(_OrderRelRowTPShipToBTName_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ScheduleNumber", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowScheduleNumber(String value) {
        return new JAXBElement<String>(_OrderRelRowScheduleNumber_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipToContactName", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowShipToContactName(String value) {
        return new JAXBElement<String>(_OrderRelRowShipToContactName_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Reference", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowReference(String value) {
        return new JAXBElement<String>(_OrderDtlRowReference_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "VendorNumCity", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowVendorNumCity(String value) {
        return new JAXBElement<String>(_OrderRelRowVendorNumCity_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CreditLimitSource", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowCreditLimitSource(String value) {
        return new JAXBElement<String>(_OrderDtlRowCreditLimitSource_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxRegionCodeDescription", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowTaxRegionCodeDescription(String value) {
        return new JAXBElement<String>(_OrderRelRowTaxRegionCodeDescription_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNumIUM", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowPartNumIUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNumIUM_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "VendorNumCountry", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowVendorNumCountry(String value) {
        return new JAXBElement<String>(_OrderRelRowVendorNumCountry_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WebSKU", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowWebSKU(String value) {
        return new JAXBElement<String>(_OrderRelRowWebSKU_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DspInvMeth", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowDspInvMeth(String value) {
        return new JAXBElement<String>(_OrderRelRowDspInvMeth_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSFaxNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSFaxNum(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSFaxNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxExempt", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowTaxExempt(String value) {
        return new JAXBElement<String>(_OrderRelRowTaxExempt_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WIOrderLine", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowWIOrderLine(String value) {
        return new JAXBElement<String>(_OrderRelRowWIOrderLine_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "NeedByDate", scope = OrderRelRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderRelRowNeedByDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowNeedByDate_QNAME, XMLGregorianCalendar.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSShipToNum", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowOTSShipToNum(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSShipToNum_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServDeliveryDate", scope = OrderRelRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderRelRowServDeliveryDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderRelRowServDeliveryDate_QNAME, XMLGregorianCalendar.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WarehouseCodeDescription", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowWarehouseCodeDescription(String value) {
        return new JAXBElement<String>(_JobProdRowWarehouseCodeDescription_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipToContactEMailAddress", scope = OrderRelRow.class)
    public JAXBElement<String> createOrderRelRowShipToContactEMailAddress(String value) {
        return new JAXBElement<String>(_OrderRelRowShipToContactEMailAddress_QNAME, String.class, OrderRelRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartSubsTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartSubs", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<PartSubsTable> createUpdExtSalesOrderTablesetPartSubs(PartSubsTable value) {
        return new JAXBElement<PartSubsTable>(_UpdExtSalesOrderTablesetPartSubs_QNAME, PartSubsTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HedTaxSumTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "HedTaxSum", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<HedTaxSumTable> createUpdExtSalesOrderTablesetHedTaxSum(HedTaxSumTable value) {
        return new JAXBElement<HedTaxSumTable>(_UpdExtSalesOrderTablesetHedTaxSum_QNAME, HedTaxSumTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderRelTaxTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderRelTax", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<OrderRelTaxTable> createUpdExtSalesOrderTablesetOrderRelTax(OrderRelTaxTable value) {
        return new JAXBElement<OrderRelTaxTable>(_UpdExtSalesOrderTablesetOrderRelTax_QNAME, OrderRelTaxTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedAttchTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedAttch", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<OrderHedAttchTable> createUpdExtSalesOrderTablesetOrderHedAttch(OrderHedAttchTable value) {
        return new JAXBElement<OrderHedAttchTable>(_UpdExtSalesOrderTablesetOrderHedAttch_QNAME, OrderHedAttchTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderMscTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderMsc", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<OrderMscTable> createUpdExtSalesOrderTablesetOrderMsc(OrderMscTable value) {
        return new JAXBElement<OrderMscTable>(_UpdExtSalesOrderTablesetOrderMsc_QNAME, OrderMscTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxConnectStatusTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxConnectStatus", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<TaxConnectStatusTable> createUpdExtSalesOrderTablesetTaxConnectStatus(TaxConnectStatusTable value) {
        return new JAXBElement<TaxConnectStatusTable>(_UpdExtSalesOrderTablesetTaxConnectStatus_QNAME, TaxConnectStatusTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderDtlTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDtl", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<OrderDtlTable> createUpdExtSalesOrderTablesetOrderDtl(OrderDtlTable value) {
        return new JAXBElement<OrderDtlTable>(_UpdExtSalesOrderTablesetOrderDtl_QNAME, OrderDtlTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedUPSTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedUPS", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<OrderHedUPSTable> createUpdExtSalesOrderTablesetOrderHedUPS(OrderHedUPSTable value) {
        return new JAXBElement<OrderHedUPSTable>(_UpdExtSalesOrderTablesetOrderHedUPS_QNAME, OrderHedUPSTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHistTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHist", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<OrderHistTable> createUpdExtSalesOrderTablesetOrderHist(OrderHistTable value) {
        return new JAXBElement<OrderHistTable>(_UpdExtSalesOrderTablesetOrderHist_QNAME, OrderHistTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderRelTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderRel", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<OrderRelTable> createUpdExtSalesOrderTablesetOrderRel(OrderRelTable value) {
        return new JAXBElement<OrderRelTable>(_UpdExtSalesOrderTablesetOrderRel_QNAME, OrderRelTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderRepCommTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderRepComm", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<OrderRepCommTable> createUpdExtSalesOrderTablesetOrderRepComm(OrderRepCommTable value) {
        return new JAXBElement<OrderRepCommTable>(_UpdExtSalesOrderTablesetOrderRepComm_QNAME, OrderRepCommTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OHOrderMscTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OHOrderMsc", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<OHOrderMscTable> createUpdExtSalesOrderTablesetOHOrderMsc(OHOrderMscTable value) {
        return new JAXBElement<OHOrderMscTable>(_UpdExtSalesOrderTablesetOHOrderMsc_QNAME, OHOrderMscTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SNFormatTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNFormat", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<SNFormatTable> createUpdExtSalesOrderTablesetSNFormat(SNFormatTable value) {
        return new JAXBElement<SNFormatTable>(_UpdExtSalesOrderTablesetSNFormat_QNAME, SNFormatTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderDtlAttchTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDtlAttch", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<OrderDtlAttchTable> createUpdExtSalesOrderTablesetOrderDtlAttch(OrderDtlAttchTable value) {
        return new JAXBElement<OrderDtlAttchTable>(_UpdExtSalesOrderTablesetOrderDtlAttch_QNAME, OrderDtlAttchTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHed", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<OrderHedTable> createUpdExtSalesOrderTablesetOrderHed(OrderHedTable value) {
        return new JAXBElement<OrderHedTable>(_UpdExtSalesOrderTablesetOrderHed_QNAME, OrderHedTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectedSerialNumbersTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SelectedSerialNumbers", scope = UpdExtSalesOrderTableset.class)
    public JAXBElement<SelectedSerialNumbersTable> createUpdExtSalesOrderTablesetSelectedSerialNumbers(SelectedSerialNumbersTable value) {
        return new JAXBElement<SelectedSerialNumbersTable>(_UpdExtSalesOrderTablesetSelectedSerialNumbers_QNAME, SelectedSerialNumbersTable.class, UpdExtSalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCCAmountsResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeCCAmountsResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCCAmountsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "inTranDate", scope = CCLoadTranData.class)
    public JAXBElement<XMLGregorianCalendar> createCCLoadTranDataInTranDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CCLoadTranDataInTranDate_QNAME, XMLGregorianCalendar.class, CCLoadTranData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CCLoadTranData.class)
    public JAXBElement<SalesOrderTableset> createCCLoadTranDataDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CCLoadTranData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfguid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ids", scope = GetBySysRowIDs.class)
    public JAXBElement<ArrayOfguid> createGetBySysRowIDsIds(ArrayOfguid value) {
        return new JAXBElement<ArrayOfguid>(_GetBySysRowIDsIds_QNAME, ArrayOfguid.class, GetBySysRowIDs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNMaskSuffix", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowSNMaskSuffix(String value) {
        return new JAXBElement<String>(_SNFormatRowSNMaskSuffix_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartSalesUM", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowPartSalesUM(String value) {
        return new JAXBElement<String>(_JobProdRowPartSalesUM_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNPrefix", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowSNPrefix(String value) {
        return new JAXBElement<String>(_SNFormatRowSNPrefix_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SerialMaskDescription", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowSerialMaskDescription(String value) {
        return new JAXBElement<String>(_SNFormatRowSerialMaskDescription_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartPartDescription", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowPartPartDescription(String value) {
        return new JAXBElement<String>(_JobProdRowPartPartDescription_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartPricePerCode", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowPartPricePerCode(String value) {
        return new JAXBElement<String>(_JobProdRowPartPricePerCode_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNum", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNum_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartIUM", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowPartIUM(String value) {
        return new JAXBElement<String>(_JobProdRowPartIUM_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNFormat", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowSNFormat(String value) {
        return new JAXBElement<String>(_UpdExtSalesOrderTablesetSNFormat_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNMaskPrefix", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowSNMaskPrefix(String value) {
        return new JAXBElement<String>(_SNFormatRowSNMaskPrefix_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SerialMaskMask", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowSerialMaskMask(String value) {
        return new JAXBElement<String>(_SNFormatRowSerialMaskMask_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SerialMaskExample", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowSerialMaskExample(String value) {
        return new JAXBElement<String>(_SNFormatRowSerialMaskExample_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Plant", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowPlant(String value) {
        return new JAXBElement<String>(_JobProdRowPlant_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNBaseDataType", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowSNBaseDataType(String value) {
        return new JAXBElement<String>(_SNFormatRowSNBaseDataType_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNLastUsedSeq", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowSNLastUsedSeq(String value) {
        return new JAXBElement<String>(_SNFormatRowSNLastUsedSeq_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNMask", scope = SNFormatRow.class)
    public JAXBElement<String> createSNFormatRowSNMask(String value) {
        return new JAXBElement<String>(_SNFormatRowSNMask_QNAME, String.class, SNFormatRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckSellingFactorDirection.class)
    public JAXBElement<SalesOrderTableset> createCheckSellingFactorDirectionDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckSellingFactorDirection.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ip_SellingFactorDirection", scope = CheckSellingFactorDirection.class)
    public JAXBElement<String> createCheckSellingFactorDirectionIpSellingFactorDirection(String value) {
        return new JAXBElement<String>(_CheckSellingFactorDirectionIpSellingFactorDirection_QNAME, String.class, CheckSellingFactorDirection.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BOUpdErrorTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "UpdateExtResult", scope = UpdateExtResponse.class)
    public JAXBElement<BOUpdErrorTableset> createUpdateExtResponseUpdateExtResult(BOUpdErrorTableset value) {
        return new JAXBElement<BOUpdErrorTableset>(_UpdateExtResponseUpdateExtResult_QNAME, BOUpdErrorTableset.class, UpdateExtResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdExtSalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = UpdateExtResponse.class)
    public JAXBElement<UpdExtSalesOrderTableset> createUpdateExtResponseDs(UpdExtSalesOrderTableset value) {
        return new JAXBElement<UpdExtSalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, UpdExtSalesOrderTableset.class, UpdateExtResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "NewCustPartNum", scope = KitCompPartCreate.class)
    public JAXBElement<String> createKitCompPartCreateNewCustPartNum(String value) {
        return new JAXBElement<String>(_GetSmartStringResponseNewCustPartNum_QNAME, String.class, KitCompPartCreate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "NewPartNum", scope = KitCompPartCreate.class)
    public JAXBElement<String> createKitCompPartCreateNewPartNum(String value) {
        return new JAXBElement<String>(_GetSmartStringResponseNewPartNum_QNAME, String.class, KitCompPartCreate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "RevisionNum", scope = KitCompPartCreate.class)
    public JAXBElement<String> createKitCompPartCreateRevisionNum(String value) {
        return new JAXBElement<String>(_GetSmartStringRevisionNum_QNAME, String.class, KitCompPartCreate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "PartNum", scope = KitCompPartCreate.class)
    public JAXBElement<String> createKitCompPartCreatePartNum(String value) {
        return new JAXBElement<String>(_GetSmartStringPartNum_QNAME, String.class, KitCompPartCreate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "SmartString", scope = KitCompPartCreate.class)
    public JAXBElement<String> createKitCompPartCreateSmartString(String value) {
        return new JAXBElement<String>(_GetSmartStringResponseSmartString_QNAME, String.class, KitCompPartCreate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeERSOrder.class)
    public JAXBElement<SalesOrderTableset> createChangeERSOrderDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeERSOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cCreditStatus", scope = CheckQuoteForCreditLimitResponse.class)
    public JAXBElement<String> createCheckQuoteForCreditLimitResponseCCreditStatus(String value) {
        return new JAXBElement<String>(_CheckQuoteForCreditLimitResponseCCreditStatus_QNAME, String.class, CheckQuoteForCreditLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cCreditLimitMessage", scope = CheckQuoteForCreditLimitResponse.class)
    public JAXBElement<String> createCheckQuoteForCreditLimitResponseCCreditLimitMessage(String value) {
        return new JAXBElement<String>(_CheckCustomerCreditLimitResponseCCreditLimitMessage_QNAME, String.class, CheckQuoteForCreditLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCCAmounts.class)
    public JAXBElement<SalesOrderTableset> createChangeCCAmountsDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCCAmounts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckRateGrpCodeResponse.class)
    public JAXBElement<SalesOrderTableset> createCheckRateGrpCodeResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckRateGrpCodeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderDtlResponse.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderDtlResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderDtlResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CreateLineMiscChargesFromQuote.class)
    public JAXBElement<SalesOrderTableset> createCreateLineMiscChargesFromQuoteDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CreateLineMiscChargesFromQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetPayBTFlagDefaultsResponse.class)
    public JAXBElement<SalesOrderTableset> createGetPayBTFlagDefaultsResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetPayBTFlagDefaultsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "CloseOrderLineResult", scope = CloseOrderLineResponse.class)
    public JAXBElement<SalesOrderTableset> createCloseOrderLineResponseCloseOrderLineResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_CloseOrderLineResponseCloseOrderLineResult_QNAME, SalesOrderTableset.class, CloseOrderLineResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeRMANumResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeRMANumResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeRMANumResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcDimCode", scope = ChangeSellingQtyMaster.class)
    public JAXBElement<String> createChangeSellingQtyMasterPcDimCode(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterPcDimCode_QNAME, String.class, ChangeSellingQtyMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcPartNum", scope = ChangeSellingQtyMaster.class)
    public JAXBElement<String> createChangeSellingQtyMasterPcPartNum(String value) {
        return new JAXBElement<String>(_GetBreakListCodeDescPcPartNum_QNAME, String.class, ChangeSellingQtyMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeSellingQtyMaster.class)
    public JAXBElement<SalesOrderTableset> createChangeSellingQtyMasterDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeSellingQtyMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcWhseCode", scope = ChangeSellingQtyMaster.class)
    public JAXBElement<String> createChangeSellingQtyMasterPcWhseCode(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterPcWhseCode_QNAME, String.class, ChangeSellingQtyMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcLotNum", scope = ChangeSellingQtyMaster.class)
    public JAXBElement<String> createChangeSellingQtyMasterPcLotNum(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterPcLotNum_QNAME, String.class, ChangeSellingQtyMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcBinNum", scope = ChangeSellingQtyMaster.class)
    public JAXBElement<String> createChangeSellingQtyMasterPcBinNum(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterPcBinNum_QNAME, String.class, ChangeSellingQtyMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OrderDtlGetNewFromQuote.class)
    public JAXBElement<SalesOrderTableset> createOrderDtlGetNewFromQuoteDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OrderDtlGetNewFromQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeContractNumMasterResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeContractNumMasterResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeContractNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "outMsg", scope = ChangeContractNumMasterResponse.class)
    public JAXBElement<String> createChangeContractNumMasterResponseOutMsg(String value) {
        return new JAXBElement<String>(_ChangeContractNumMasterResponseOutMsg_QNAME, String.class, ChangeContractNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckOrderHedDispatchReasonResponse.class)
    public JAXBElement<SalesOrderTableset> createCheckOrderHedDispatchReasonResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckOrderHedDispatchReasonResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "outMsg", scope = ChkLtrOfCrdtResponse.class)
    public JAXBElement<String> createChkLtrOfCrdtResponseOutMsg(String value) {
        return new JAXBElement<String>(_ChangeContractNumMasterResponseOutMsg_QNAME, String.class, ChkLtrOfCrdtResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCreditExpResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeCreditExpResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCreditExpResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeQuoteLine.class)
    public JAXBElement<SalesOrderTableset> createChangeQuoteLineDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeQuoteLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrdDtlQuoteQtyTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetQuoteQtyResult", scope = GetQuoteQtyResponse.class)
    public JAXBElement<OrdDtlQuoteQtyTableset> createGetQuoteQtyResponseGetQuoteQtyResult(OrdDtlQuoteQtyTableset value) {
        return new JAXBElement<OrdDtlQuoteQtyTableset>(_GetQuoteQtyResponseGetQuoteQtyResult_QNAME, OrdDtlQuoteQtyTableset.class, GetQuoteQtyResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckCustomerCreditLimit.class)
    public JAXBElement<SalesOrderTableset> createCheckCustomerCreditLimitDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckCustomerCreditLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCardNumber.class)
    public JAXBElement<SalesOrderTableset> createChangeCardNumberDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCardNumber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "inCardNumber", scope = ChangeCardNumber.class)
    public JAXBElement<String> createChangeCardNumberInCardNumber(String value) {
        return new JAXBElement<String>(_ChangeCardNumberInCardNumber_QNAME, String.class, ChangeCardNumber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "UpdateOrderDtlDiscountPercentResult", scope = UpdateOrderDtlDiscountPercentResponse.class)
    public JAXBElement<SalesOrderTableset> createUpdateOrderDtlDiscountPercentResponseUpdateOrderDtlDiscountPercentResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_UpdateOrderDtlDiscountPercentResponseUpdateOrderDtlDiscountPercentResult_QNAME, SalesOrderTableset.class, UpdateOrderDtlDiscountPercentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetPayBTFlagDefaults.class)
    public JAXBElement<SalesOrderTableset> createGetPayBTFlagDefaultsDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetPayBTFlagDefaults.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CreateLineMiscChargesFromQuoteResponse.class)
    public JAXBElement<SalesOrderTableset> createCreateLineMiscChargesFromQuoteResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CreateLineMiscChargesFromQuoteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartSubsTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartSubs", scope = SalesOrderTableset.class)
    public JAXBElement<PartSubsTable> createSalesOrderTablesetPartSubs(PartSubsTable value) {
        return new JAXBElement<PartSubsTable>(_UpdExtSalesOrderTablesetPartSubs_QNAME, PartSubsTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HedTaxSumTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "HedTaxSum", scope = SalesOrderTableset.class)
    public JAXBElement<HedTaxSumTable> createSalesOrderTablesetHedTaxSum(HedTaxSumTable value) {
        return new JAXBElement<HedTaxSumTable>(_UpdExtSalesOrderTablesetHedTaxSum_QNAME, HedTaxSumTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderRelTaxTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderRelTax", scope = SalesOrderTableset.class)
    public JAXBElement<OrderRelTaxTable> createSalesOrderTablesetOrderRelTax(OrderRelTaxTable value) {
        return new JAXBElement<OrderRelTaxTable>(_UpdExtSalesOrderTablesetOrderRelTax_QNAME, OrderRelTaxTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedAttchTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedAttch", scope = SalesOrderTableset.class)
    public JAXBElement<OrderHedAttchTable> createSalesOrderTablesetOrderHedAttch(OrderHedAttchTable value) {
        return new JAXBElement<OrderHedAttchTable>(_UpdExtSalesOrderTablesetOrderHedAttch_QNAME, OrderHedAttchTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderMscTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderMsc", scope = SalesOrderTableset.class)
    public JAXBElement<OrderMscTable> createSalesOrderTablesetOrderMsc(OrderMscTable value) {
        return new JAXBElement<OrderMscTable>(_UpdExtSalesOrderTablesetOrderMsc_QNAME, OrderMscTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxConnectStatusTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxConnectStatus", scope = SalesOrderTableset.class)
    public JAXBElement<TaxConnectStatusTable> createSalesOrderTablesetTaxConnectStatus(TaxConnectStatusTable value) {
        return new JAXBElement<TaxConnectStatusTable>(_UpdExtSalesOrderTablesetTaxConnectStatus_QNAME, TaxConnectStatusTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderDtlTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDtl", scope = SalesOrderTableset.class)
    public JAXBElement<OrderDtlTable> createSalesOrderTablesetOrderDtl(OrderDtlTable value) {
        return new JAXBElement<OrderDtlTable>(_UpdExtSalesOrderTablesetOrderDtl_QNAME, OrderDtlTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedUPSTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedUPS", scope = SalesOrderTableset.class)
    public JAXBElement<OrderHedUPSTable> createSalesOrderTablesetOrderHedUPS(OrderHedUPSTable value) {
        return new JAXBElement<OrderHedUPSTable>(_UpdExtSalesOrderTablesetOrderHedUPS_QNAME, OrderHedUPSTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHistTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHist", scope = SalesOrderTableset.class)
    public JAXBElement<OrderHistTable> createSalesOrderTablesetOrderHist(OrderHistTable value) {
        return new JAXBElement<OrderHistTable>(_UpdExtSalesOrderTablesetOrderHist_QNAME, OrderHistTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderRelTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderRel", scope = SalesOrderTableset.class)
    public JAXBElement<OrderRelTable> createSalesOrderTablesetOrderRel(OrderRelTable value) {
        return new JAXBElement<OrderRelTable>(_UpdExtSalesOrderTablesetOrderRel_QNAME, OrderRelTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderRepCommTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderRepComm", scope = SalesOrderTableset.class)
    public JAXBElement<OrderRepCommTable> createSalesOrderTablesetOrderRepComm(OrderRepCommTable value) {
        return new JAXBElement<OrderRepCommTable>(_UpdExtSalesOrderTablesetOrderRepComm_QNAME, OrderRepCommTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OHOrderMscTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OHOrderMsc", scope = SalesOrderTableset.class)
    public JAXBElement<OHOrderMscTable> createSalesOrderTablesetOHOrderMsc(OHOrderMscTable value) {
        return new JAXBElement<OHOrderMscTable>(_UpdExtSalesOrderTablesetOHOrderMsc_QNAME, OHOrderMscTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SNFormatTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNFormat", scope = SalesOrderTableset.class)
    public JAXBElement<SNFormatTable> createSalesOrderTablesetSNFormat(SNFormatTable value) {
        return new JAXBElement<SNFormatTable>(_UpdExtSalesOrderTablesetSNFormat_QNAME, SNFormatTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderDtlAttchTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDtlAttch", scope = SalesOrderTableset.class)
    public JAXBElement<OrderDtlAttchTable> createSalesOrderTablesetOrderDtlAttch(OrderDtlAttchTable value) {
        return new JAXBElement<OrderDtlAttchTable>(_UpdExtSalesOrderTablesetOrderDtlAttch_QNAME, OrderDtlAttchTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHed", scope = SalesOrderTableset.class)
    public JAXBElement<OrderHedTable> createSalesOrderTablesetOrderHed(OrderHedTable value) {
        return new JAXBElement<OrderHedTable>(_UpdExtSalesOrderTablesetOrderHed_QNAME, OrderHedTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectedSerialNumbersTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SelectedSerialNumbers", scope = SalesOrderTableset.class)
    public JAXBElement<SelectedSerialNumbersTable> createSalesOrderTablesetSelectedSerialNumbers(SelectedSerialNumbersTable value) {
        return new JAXBElement<SelectedSerialNumbersTable>(_UpdExtSalesOrderTablesetSelectedSerialNumbers_QNAME, SelectedSerialNumbersTable.class, SalesOrderTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SOEntryUIParamsTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SOEntryUIParams", scope = SOEntryUIParamsTableset.class)
    public JAXBElement<SOEntryUIParamsTable> createSOEntryUIParamsTablesetSOEntryUIParams(SOEntryUIParamsTable value) {
        return new JAXBElement<SOEntryUIParamsTable>(_SOEntryUIParamsTablesetSOEntryUIParams_QNAME, SOEntryUIParamsTable.class, SOEntryUIParamsTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderDtl.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderDtlDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderDtl.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelShipToCustID.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelShipToCustIDDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelShipToCustID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "iShipToCustID", scope = ChangeOrderRelShipToCustID.class)
    public JAXBElement<String> createChangeOrderRelShipToCustIDIShipToCustID(String value) {
        return new JAXBElement<String>(_ChangeOrderRelShipToCustIDIShipToCustID_QNAME, String.class, ChangeOrderRelShipToCustID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeSellingReqQty.class)
    public JAXBElement<SalesOrderTableset> createChangeSellingReqQtyDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeSellingReqQty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelBuyToOrderResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelBuyToOrderResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelBuyToOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "iPartNum", scope = HasMultipleSubs.class)
    public JAXBElement<String> createHasMultipleSubsIPartNum(String value) {
        return new JAXBElement<String>(_HasMultipleSubsIPartNum_QNAME, String.class, HasMultipleSubs.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClause", scope = GetList.class)
    public JAXBElement<String> createGetListWhereClause(String value) {
        return new JAXBElement<String>(_GetListCustomWhereClause_QNAME, String.class, GetList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeRevNumMaster.class)
    public JAXBElement<SalesOrderTableset> createChangeRevNumMasterDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeRevNumMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "proposedRev", scope = ChangeRevNumMaster.class)
    public JAXBElement<String> createChangeRevNumMasterProposedRev(String value) {
        return new JAXBElement<String>(_ChangeRevNumMasterProposedRev_QNAME, String.class, ChangeRevNumMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeMiscAmount.class)
    public JAXBElement<SalesOrderTableset> createChangeMiscAmountDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeMiscAmount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "tableName", scope = ChangeMiscAmount.class)
    public JAXBElement<String> createChangeMiscAmountTableName(String value) {
        return new JAXBElement<String>(_ChangeMiscAmountTableName_QNAME, String.class, ChangeMiscAmount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cRowIdent", scope = GlbSugPOChgDelete.class)
    public JAXBElement<String> createGlbSugPOChgDeleteCRowIdent(String value) {
        return new JAXBElement<String>(_GlbSugPOChgDeleteCRowIdent_QNAME, String.class, GlbSugPOChgDelete.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeRenewalNbrMaster.class)
    public JAXBElement<SalesOrderTableset> createChangeRenewalNbrMasterDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeRenewalNbrMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "EmailAddress", scope = OrderHedUPSRow.class)
    public JAXBElement<String> createOrderHedUPSRowEmailAddress(String value) {
        return new JAXBElement<String>(_OrderHedUPSRowEmailAddress_QNAME, String.class, OrderHedUPSRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = OrderHedUPSRow.class)
    public JAXBElement<String> createOrderHedUPSRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, OrderHedUPSRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "EntryProcess", scope = OrderHedUPSRow.class)
    public JAXBElement<String> createOrderHedUPSRowEntryProcess(String value) {
        return new JAXBElement<String>(_OrderDtlRowEntryProcess_QNAME, String.class, OrderHedUPSRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectSerialNumbersParamsTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SelectSerialNumbersParams", scope = SelectSerialNumbersParamsTableset.class)
    public JAXBElement<SelectSerialNumbersParamsTable> createSelectSerialNumbersParamsTablesetSelectSerialNumbersParams(SelectSerialNumbersParamsTable value) {
        return new JAXBElement<SelectSerialNumbersParamsTable>(_SelectSerialNumbersParamsTablesetSelectSerialNumbersParams_QNAME, SelectSerialNumbersParamsTable.class, SelectSerialNumbersParamsTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeSalesUOM.class)
    public JAXBElement<SalesOrderTableset> createChangeSalesUOMDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeSalesUOM.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetByIDLinkedOrderResult", scope = GetByIDLinkedOrderResponse.class)
    public JAXBElement<SalesOrderTableset> createGetByIDLinkedOrderResponseGetByIDLinkedOrderResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_GetByIDLinkedOrderResponseGetByIDLinkedOrderResult_QNAME, SalesOrderTableset.class, GetByIDLinkedOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "CreateOrderFromQuoteResult", scope = CreateOrderFromQuoteResponse.class)
    public JAXBElement<SalesOrderTableset> createCreateOrderFromQuoteResponseCreateOrderFromQuoteResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_CreateOrderFromQuoteResponseCreateOrderFromQuoteResult_QNAME, SalesOrderTableset.class, CreateOrderFromQuoteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCardNumberResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeCardNumberResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCardNumberResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeShipToCustIDResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeShipToCustIDResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeShipToCustIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeContractNum.class)
    public JAXBElement<SalesOrderTableset> createChangeContractNumDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeContractNum.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeKitQtyPerResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeKitQtyPerResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeKitQtyPerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cVerifySendMsgText", scope = VerifySendICPOSuggResponse.class)
    public JAXBElement<String> createVerifySendICPOSuggResponseCVerifySendMsgText(String value) {
        return new JAXBElement<String>(_VerifySendICPOSuggResponseCVerifySendMsgText_QNAME, String.class, VerifySendICPOSuggResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeOfTaxAmt.class)
    public JAXBElement<SalesOrderTableset> createOnChangeOfTaxAmtDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeOfTaxAmt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "TaxCode", scope = OnChangeOfTaxAmt.class)
    public JAXBElement<String> createOnChangeOfTaxAmtTaxCode(String value) {
        return new JAXBElement<String>(_OnChangeOfTaxPercentTaxCode_QNAME, String.class, OnChangeOfTaxAmt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GlbSugPOChgUpdate.class)
    public JAXBElement<GlbSugPOChgTableset> createGlbSugPOChgUpdateDs(GlbSugPOChgTableset value) {
        return new JAXBElement<GlbSugPOChgTableset>(_SetUPSQVEnableResponseDs_QNAME, GlbSugPOChgTableset.class, GlbSugPOChgUpdate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ValidLine4", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowValidLine4(String value) {
        return new JAXBElement<String>(_ETCAddressRowValidLine4_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PostalCode", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowPostalCode(String value) {
        return new JAXBElement<String>(_ETCAddressRowPostalCode_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "County", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowCounty(String value) {
        return new JAXBElement<String>(_ETCAddressRowCounty_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ValidLine1", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowValidLine1(String value) {
        return new JAXBElement<String>(_ETCAddressRowValidLine1_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ValidLine2", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowValidLine2(String value) {
        return new JAXBElement<String>(_ETCAddressRowValidLine2_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ValidRegion", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowValidRegion(String value) {
        return new JAXBElement<String>(_ETCAddressRowValidRegion_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TransactionID", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowTransactionID(String value) {
        return new JAXBElement<String>(_ETCMessageRowTransactionID_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ValidLine3", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowValidLine3(String value) {
        return new JAXBElement<String>(_ETCAddressRowValidLine3_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AddressTypeDesc", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowAddressTypeDesc(String value) {
        return new JAXBElement<String>(_ETCAddressRowAddressTypeDesc_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RequestID", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowRequestID(String value) {
        return new JAXBElement<String>(_ETCMessageRowRequestID_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSCountry", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowOTSCountry(String value) {
        return new JAXBElement<String>(_ETCAddressRowOTSCountry_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ValidCity", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowValidCity(String value) {
        return new JAXBElement<String>(_ETCAddressRowValidCity_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PostNet", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowPostNet(String value) {
        return new JAXBElement<String>(_ETCAddressRowPostNet_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AddrSource", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowAddrSource(String value) {
        return new JAXBElement<String>(_ETCMessageRowAddrSource_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CarrierRoute", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowCarrierRoute(String value) {
        return new JAXBElement<String>(_ETCAddressRowCarrierRoute_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AddressCode", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowAddressCode(String value) {
        return new JAXBElement<String>(_ETCAddressRowAddressCode_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "City", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowCity(String value) {
        return new JAXBElement<String>(_ETCAddressRowCity_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AddrSourceID", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowAddrSourceID(String value) {
        return new JAXBElement<String>(_ETCMessageRowAddrSourceID_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CarrierRouteDesc", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowCarrierRouteDesc(String value) {
        return new JAXBElement<String>(_ETCAddressRowCarrierRouteDesc_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FipsCode", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowFipsCode(String value) {
        return new JAXBElement<String>(_ETCAddressRowFipsCode_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Country", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowCountry(String value) {
        return new JAXBElement<String>(_ETCAddressRowCountry_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Region", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowRegion(String value) {
        return new JAXBElement<String>(_ETCAddressRowRegion_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AddressType", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowAddressType(String value) {
        return new JAXBElement<String>(_ETCAddressRowAddressType_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Line1", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowLine1(String value) {
        return new JAXBElement<String>(_ETCAddressRowLine1_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Line2", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowLine2(String value) {
        return new JAXBElement<String>(_ETCAddressRowLine2_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ResultCode", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowResultCode(String value) {
        return new JAXBElement<String>(_ETCAddressRowResultCode_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ValidPostalCode", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowValidPostalCode(String value) {
        return new JAXBElement<String>(_ETCAddressRowValidPostalCode_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Line3", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowLine3(String value) {
        return new JAXBElement<String>(_ETCAddressRowLine3_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ValidCountry", scope = ETCAddressRow.class)
    public JAXBElement<String> createETCAddressRowValidCountry(String value) {
        return new JAXBElement<String>(_ETCAddressRowValidCountry_QNAME, String.class, ETCAddressRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCounterSaleResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeCounterSaleResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCounterSaleResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeDiscountPercentResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeDiscountPercentResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeDiscountPercentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "basePartNum", scope = GetBasePartForConfigResponse.class)
    public JAXBElement<String> createGetBasePartForConfigResponseBasePartNum(String value) {
        return new JAXBElement<String>(_GetBasePartForConfigResponseBasePartNum_QNAME, String.class, GetBasePartForConfigResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "baseRevisionNum", scope = GetBasePartForConfigResponse.class)
    public JAXBElement<String> createGetBasePartForConfigResponseBaseRevisionNum(String value) {
        return new JAXBElement<String>(_GetBasePartForConfigResponseBaseRevisionNum_QNAME, String.class, GetBasePartForConfigResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcNeqQtyAction", scope = NegativeInventoryTestResponse.class)
    public JAXBElement<String> createNegativeInventoryTestResponsePcNeqQtyAction(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterResponsePcNeqQtyAction_QNAME, String.class, NegativeInventoryTestResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcMessage", scope = NegativeInventoryTestResponse.class)
    public JAXBElement<String> createNegativeInventoryTestResponsePcMessage(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterResponsePcMessage_QNAME, String.class, NegativeInventoryTestResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "XRefPartNum", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowXRefPartNum(String value) {
        return new JAXBElement<String>(_SelectedSerialNumbersRowXRefPartNum_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TransType", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowTransType(String value) {
        return new JAXBElement<String>(_SelectedSerialNumbersRowTransType_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNBaseNumber", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowSNBaseNumber(String value) {
        return new JAXBElement<String>(_SelectedSerialNumbersRowSNBaseNumber_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "KBLbrActionDesc", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowKBLbrActionDesc(String value) {
        return new JAXBElement<String>(_SelectedSerialNumbersRowKBLbrActionDesc_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNPrefix", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowSNPrefix(String value) {
        return new JAXBElement<String>(_SNFormatRowSNPrefix_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ReasonCodeType", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowReasonCodeType(String value) {
        return new JAXBElement<String>(_SelectedSerialNumbersRowReasonCodeType_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Reference", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowReference(String value) {
        return new JAXBElement<String>(_OrderDtlRowReference_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNum", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNum_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ReasonCodeDesc", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowReasonCodeDesc(String value) {
        return new JAXBElement<String>(_SelectedSerialNumbersRowReasonCodeDesc_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "poLinkValues", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowPoLinkValues(String value) {
        return new JAXBElement<String>(_SelectSerialNumbersParamsRowPoLinkValues_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RawSerialNum", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowRawSerialNum(String value) {
        return new JAXBElement<String>(_SelectedSerialNumbersRowRawSerialNum_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SerialNumber", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowSerialNumber(String value) {
        return new JAXBElement<String>(_SelectedSerialNumbersRowSerialNumber_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "XRefPartType", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowXRefPartType(String value) {
        return new JAXBElement<String>(_SelectedSerialNumbersRowXRefPartType_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "KitWhseList", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowKitWhseList(String value) {
        return new JAXBElement<String>(_SelectedSerialNumbersRowKitWhseList_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SNMask", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowSNMask(String value) {
        return new JAXBElement<String>(_SNFormatRowSNMask_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ScrappedReasonCode", scope = SelectedSerialNumbersRow.class)
    public JAXBElement<String> createSelectedSerialNumbersRowScrappedReasonCode(String value) {
        return new JAXBElement<String>(_SelectedSerialNumbersRowScrappedReasonCode_QNAME, String.class, SelectedSerialNumbersRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeSoldToContact.class)
    public JAXBElement<SalesOrderTableset> createChangeSoldToContactDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeSoldToContact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cPartNum", scope = CheckPartRevisionChangeResponse.class)
    public JAXBElement<String> createCheckPartRevisionChangeResponseCPartNum(String value) {
        return new JAXBElement<String>(_CheckPartRevisionChangeResponseCPartNum_QNAME, String.class, CheckPartRevisionChangeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cMsgType", scope = CheckPartRevisionChangeResponse.class)
    public JAXBElement<String> createCheckPartRevisionChangeResponseCMsgType(String value) {
        return new JAXBElement<String>(_CheckPartRevisionChangeResponseCMsgType_QNAME, String.class, CheckPartRevisionChangeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cConfigPartMessage", scope = CheckPartRevisionChangeResponse.class)
    public JAXBElement<String> createCheckPartRevisionChangeResponseCConfigPartMessage(String value) {
        return new JAXBElement<String>(_CheckPartRevisionChangeResponseCConfigPartMessage_QNAME, String.class, CheckPartRevisionChangeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cSubPartMessage", scope = CheckPartRevisionChangeResponse.class)
    public JAXBElement<String> createCheckPartRevisionChangeResponseCSubPartMessage(String value) {
        return new JAXBElement<String>(_CheckPartRevisionChangeResponseCSubPartMessage_QNAME, String.class, CheckPartRevisionChangeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeOfMktgEvnt.class)
    public JAXBElement<SalesOrderTableset> createOnChangeOfMktgEvntDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeOfMktgEvnt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "MktgCampaignID", scope = OnChangeOfMktgEvnt.class)
    public JAXBElement<String> createOnChangeOfMktgEvntMktgCampaignID(String value) {
        return new JAXBElement<String>(_OnChangeOfMktgEvntMktgCampaignID_QNAME, String.class, OnChangeOfMktgEvnt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeMiscCode.class)
    public JAXBElement<SalesOrderTableset> createChangeMiscCodeDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeMiscCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "tableName", scope = ChangeMiscCode.class)
    public JAXBElement<String> createChangeMiscCodeTableName(String value) {
        return new JAXBElement<String>(_ChangeMiscAmountTableName_QNAME, String.class, ChangeMiscCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeORelWarehouseResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeORelWarehouseResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeORelWarehouseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipRateCode", scope = ChangeManualTaxCalc.class)
    public JAXBElement<String> createChangeManualTaxCalcIpRateCode(String value) {
        return new JAXBElement<String>(_ChangeManualTaxCalcIpRateCode_QNAME, String.class, ChangeManualTaxCalc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeManualTaxCalc.class)
    public JAXBElement<SalesOrderTableset> createChangeManualTaxCalcDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeManualTaxCalc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipTaxCode", scope = ChangeManualTaxCalc.class)
    public JAXBElement<String> createChangeManualTaxCalcIpTaxCode(String value) {
        return new JAXBElement<String>(_ChangeManualTaxCalcIpTaxCode_QNAME, String.class, ChangeManualTaxCalc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeODtlWarehouseResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeODtlWarehouseResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeODtlWarehouseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OrderDtlGetNewFromQuoteResponse.class)
    public JAXBElement<SalesOrderTableset> createOrderDtlGetNewFromQuoteResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OrderDtlGetNewFromQuoteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseSNFormat", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseSNFormat(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseSNFormat_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderRelTax", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseOrderRelTax(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderRelTax_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderHed", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseOrderHed(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderHed_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderRel", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseOrderRel(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderRel_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderDtl", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseOrderDtl(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderDtl_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseHedTaxSum", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseHedTaxSum(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseHedTaxSum_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderHedAttch", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseOrderHedAttch(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderHedAttch_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderMsc", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseOrderMsc(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderMsc_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderHist", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseOrderHist(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderHist_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseSelectedSerialNumbers", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseSelectedSerialNumbers(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseSelectedSerialNumbers_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderHedUPS", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseOrderHedUPS(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderHedUPS_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClausePartSubs", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClausePartSubs(String value) {
        return new JAXBElement<String>(_GetRowsWhereClausePartSubs_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderRepComm", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseOrderRepComm(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderRepComm_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOHOrderMsc", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseOHOrderMsc(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOHOrderMsc_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderDtlAttch", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseOrderDtlAttch(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderDtlAttch_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseTaxConnectStatus", scope = GetRows.class)
    public JAXBElement<String> createGetRowsWhereClauseTaxConnectStatus(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseTaxConnectStatus_QNAME, String.class, GetRows.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cSellingQuantityChangedMsgText", scope = CheckSellingQuantityChangeResponse.class)
    public JAXBElement<String> createCheckSellingQuantityChangeResponseCSellingQuantityChangedMsgText(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterResponseCSellingQuantityChangedMsgText_QNAME, String.class, CheckSellingQuantityChangeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeKitPricing.class)
    public JAXBElement<SalesOrderTableset> createChangeKitPricingDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeKitPricing.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "questionString", scope = ChangePartNumMasterResponse.class)
    public JAXBElement<String> createChangePartNumMasterResponseQuestionString(String value) {
        return new JAXBElement<String>(_ChangePartNumMasterResponseQuestionString_QNAME, String.class, ChangePartNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "explodeBOMerrMessage", scope = ChangePartNumMasterResponse.class)
    public JAXBElement<String> createChangePartNumMasterResponseExplodeBOMerrMessage(String value) {
        return new JAXBElement<String>(_ChangePartNumMasterResponseExplodeBOMerrMessage_QNAME, String.class, ChangePartNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "partNum", scope = ChangePartNumMasterResponse.class)
    public JAXBElement<String> createChangePartNumMasterResponsePartNum(String value) {
        return new JAXBElement<String>(_ChangePartNumMasterPartNum_QNAME, String.class, ChangePartNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangePartNumMasterResponse.class)
    public JAXBElement<SalesOrderTableset> createChangePartNumMasterResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangePartNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cWarningMessage", scope = ChangePartNumMasterResponse.class)
    public JAXBElement<String> createChangePartNumMasterResponseCWarningMessage(String value) {
        return new JAXBElement<String>(_ChangePartNumMasterResponseCWarningMessage_QNAME, String.class, ChangePartNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cMsgType", scope = ChangePartNumMasterResponse.class)
    public JAXBElement<String> createChangePartNumMasterResponseCMsgType(String value) {
        return new JAXBElement<String>(_CheckPartRevisionChangeResponseCMsgType_QNAME, String.class, ChangePartNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "uomCode", scope = ChangePartNumMasterResponse.class)
    public JAXBElement<String> createChangePartNumMasterResponseUomCode(String value) {
        return new JAXBElement<String>(_ChangePartNumMasterUomCode_QNAME, String.class, ChangePartNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cDeleteComponentsMessage", scope = ChangePartNumMasterResponse.class)
    public JAXBElement<String> createChangePartNumMasterResponseCDeleteComponentsMessage(String value) {
        return new JAXBElement<String>(_ChangePartNumMasterResponseCDeleteComponentsMessage_QNAME, String.class, ChangePartNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cConfigPartMessage", scope = ChangePartNumMasterResponse.class)
    public JAXBElement<String> createChangePartNumMasterResponseCConfigPartMessage(String value) {
        return new JAXBElement<String>(_CheckPartRevisionChangeResponseCConfigPartMessage_QNAME, String.class, ChangePartNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cSubPartMessage", scope = ChangePartNumMasterResponse.class)
    public JAXBElement<String> createChangePartNumMasterResponseCSubPartMessage(String value) {
        return new JAXBElement<String>(_CheckPartRevisionChangeResponseCSubPartMessage_QNAME, String.class, ChangePartNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelVendorID.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelVendorIDDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelVendorID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipVendorID", scope = ChangeOrderRelVendorID.class)
    public JAXBElement<String> createChangeOrderRelVendorIDIpVendorID(String value) {
        return new JAXBElement<String>(_ChangeOrderRelVendorIDIpVendorID_QNAME, String.class, ChangeOrderRelVendorID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeNewNeedByDateResponse.class)
    public JAXBElement<GlbSugPOChgTableset> createChangeNewNeedByDateResponseDs(GlbSugPOChgTableset value) {
        return new JAXBElement<GlbSugPOChgTableset>(_SetUPSQVEnableResponseDs_QNAME, GlbSugPOChgTableset.class, ChangeNewNeedByDateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrdRelJobProdTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetJobProdResult", scope = GetJobProdResponse.class)
    public JAXBElement<OrdRelJobProdTableset> createGetJobProdResponseGetJobProdResult(OrdRelJobProdTableset value) {
        return new JAXBElement<OrdRelJobProdTableset>(_GetJobProdResponseGetJobProdResult_QNAME, OrdRelJobProdTableset.class, GetJobProdResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "NewBillToCustID", scope = OnChangeofBTCustID.class)
    public JAXBElement<String> createOnChangeofBTCustIDNewBillToCustID(String value) {
        return new JAXBElement<String>(_ChangeBTCustIDMasterNewBillToCustID_QNAME, String.class, OnChangeofBTCustID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeofBTCustID.class)
    public JAXBElement<SalesOrderTableset> createOnChangeofBTCustIDDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeofBTCustID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeKitQtyPer.class)
    public JAXBElement<SalesOrderTableset> createChangeKitQtyPerDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeKitQtyPer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OrderDtlGetNewCounterSaleResponse.class)
    public JAXBElement<SalesOrderTableset> createOrderDtlGetNewCounterSaleResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OrderDtlGetNewCounterSaleResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeOfTaxPercentResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeOfTaxPercentResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeOfTaxPercentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeMakeResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeMakeResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeMakeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = SetReadyToCalcResponse.class)
    public JAXBElement<SalesOrderTableset> createSetReadyToCalcResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, SetReadyToCalcResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeSoldToID.class)
    public JAXBElement<SalesOrderTableset> createChangeSoldToIDDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeSoldToID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderCustTrkTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetRowsCustomerTrackerResult", scope = GetRowsCustomerTrackerResponse.class)
    public JAXBElement<OrderCustTrkTableset> createGetRowsCustomerTrackerResponseGetRowsCustomerTrackerResult(OrderCustTrkTableset value) {
        return new JAXBElement<OrderCustTrkTableset>(_GetRowsCustomerTrackerResponseGetRowsCustomerTrackerResult_QNAME, OrderCustTrkTableset.class, GetRowsCustomerTrackerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeOfMktgCampaignResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeOfMktgCampaignResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeOfMktgCampaignResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelUseOTMFResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelUseOTMFResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelUseOTMFResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeNewNeedByDate.class)
    public JAXBElement<GlbSugPOChgTableset> createChangeNewNeedByDateDs(GlbSugPOChgTableset value) {
        return new JAXBElement<GlbSugPOChgTableset>(_SetUPSQVEnableResponseDs_QNAME, GlbSugPOChgTableset.class, ChangeNewNeedByDate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelShipToContact.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelShipToContactDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelShipToContact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CCProcessCard.class)
    public JAXBElement<SalesOrderTableset> createCCProcessCardDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CCProcessCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "inTranType", scope = CCProcessCard.class)
    public JAXBElement<String> createCCProcessCardInTranType(String value) {
        return new JAXBElement<String>(_CCProcessCardInTranType_QNAME, String.class, CCProcessCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeQuoteQtyNum.class)
    public JAXBElement<SalesOrderTableset> createChangeQuoteQtyNumDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeQuoteQtyNum.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeQuoteLineResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeQuoteLineResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeQuoteLineResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cCompliantMsg", scope = MasterUpdateResponse.class)
    public JAXBElement<String> createMasterUpdateResponseCCompliantMsg(String value) {
        return new JAXBElement<String>(_MasterUpdateResponseCCompliantMsg_QNAME, String.class, MasterUpdateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = MasterUpdateResponse.class)
    public JAXBElement<SalesOrderTableset> createMasterUpdateResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, MasterUpdateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cResponseMsgOrdRel", scope = MasterUpdateResponse.class)
    public JAXBElement<String> createMasterUpdateResponseCResponseMsgOrdRel(String value) {
        return new JAXBElement<String>(_MasterUpdateResponseCResponseMsgOrdRel_QNAME, String.class, MasterUpdateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cDisplayMsg", scope = MasterUpdateResponse.class)
    public JAXBElement<String> createMasterUpdateResponseCDisplayMsg(String value) {
        return new JAXBElement<String>(_MasterUpdateResponseCDisplayMsg_QNAME, String.class, MasterUpdateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cResponseMsg", scope = MasterUpdateResponse.class)
    public JAXBElement<String> createMasterUpdateResponseCResponseMsg(String value) {
        return new JAXBElement<String>(_MasterUpdateResponseCResponseMsg_QNAME, String.class, MasterUpdateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetBySysRowIDsResult", scope = GetBySysRowIDsResponse.class)
    public JAXBElement<SalesOrderTableset> createGetBySysRowIDsResponseGetBySysRowIDsResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_GetBySysRowIDsResponseGetBySysRowIDsResult_QNAME, SalesOrderTableset.class, GetBySysRowIDsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CreateGlbSugPOChg.class)
    public JAXBElement<GlbSugPOChgTableset> createCreateGlbSugPOChgDs(GlbSugPOChgTableset value) {
        return new JAXBElement<GlbSugPOChgTableset>(_SetUPSQVEnableResponseDs_QNAME, GlbSugPOChgTableset.class, CreateGlbSugPOChg.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeRenewalNbrResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeRenewalNbrResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeRenewalNbrResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderRelTax.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderRelTaxDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderRelTax.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "rateCode", scope = GetNewOrderRelTax.class)
    public JAXBElement<String> createGetNewOrderRelTaxRateCode(String value) {
        return new JAXBElement<String>(_GetNewOrderRelTaxRateCode_QNAME, String.class, GetNewOrderRelTax.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "taxCode", scope = GetNewOrderRelTax.class)
    public JAXBElement<String> createGetNewOrderRelTaxTaxCode(String value) {
        return new JAXBElement<String>(_GetNewOrderRelTaxTaxCode_QNAME, String.class, GetNewOrderRelTax.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCounterSale.class)
    public JAXBElement<SalesOrderTableset> createChangeCounterSaleDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCounterSale.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangeDate", scope = OHOrderMscRow.class)
    public JAXBElement<XMLGregorianCalendar> createOHOrderMscRowChangeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowChangeDate_QNAME, XMLGregorianCalendar.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Quoting", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowQuoting(String value) {
        return new JAXBElement<String>(_OHOrderMscRowQuoting_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangeTrackStatus", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowChangeTrackStatus(String value) {
        return new JAXBElement<String>(_OHOrderMscRowChangeTrackStatus_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FreqCode", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowFreqCode(String value) {
        return new JAXBElement<String>(_OHOrderMscRowFreqCode_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderNumCardMemberName", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowOrderNumCardMemberName(String value) {
        return new JAXBElement<String>(_OrderDtlRowOrderNumCardMemberName_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrSymbol", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowCurrSymbol(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrSymbol_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MiscCode", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowMiscCode(String value) {
        return new JAXBElement<String>(_OHOrderMscRowMiscCode_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderNumCurrencyCode", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowOrderNumCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowOrderNumCurrencyCode_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Description", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowDescription(String value) {
        return new JAXBElement<String>(_OHOrderMscRowDescription_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangedBy", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowChangedBy(String value) {
        return new JAXBElement<String>(_OrderDtlRowChangedBy_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MiscCodeDescription", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowMiscCodeDescription(String value) {
        return new JAXBElement<String>(_OHOrderMscRowMiscCodeDescription_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangeTrackMemoText", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowChangeTrackMemoText(String value) {
        return new JAXBElement<String>(_OHOrderMscRowChangeTrackMemoText_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "EntryProcess", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowEntryProcess(String value) {
        return new JAXBElement<String>(_OrderDtlRowEntryProcess_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangeTrackMemoDesc", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowChangeTrackMemoDesc(String value) {
        return new JAXBElement<String>(_OHOrderMscRowChangeTrackMemoDesc_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ExtCompany", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowExtCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowExtCompany_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BaseCurrSymbol", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowBaseCurrSymbol(String value) {
        return new JAXBElement<String>(_OrderDtlRowBaseCurrSymbol_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCode", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrencyCode_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Type", scope = OHOrderMscRow.class)
    public JAXBElement<String> createOHOrderMscRowType(String value) {
        return new JAXBElement<String>(_OHOrderMscRowType_QNAME, String.class, OHOrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeRevNumMasterResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeRevNumMasterResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeRevNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cConfigPartMessage", scope = ChangeRevNumMasterResponse.class)
    public JAXBElement<String> createChangeRevNumMasterResponseCConfigPartMessage(String value) {
        return new JAXBElement<String>(_CheckPartRevisionChangeResponseCConfigPartMessage_QNAME, String.class, ChangeRevNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cDeleteComponentsMessage", scope = ChangeRevNumMasterResponse.class)
    public JAXBElement<String> createChangeRevNumMasterResponseCDeleteComponentsMessage(String value) {
        return new JAXBElement<String>(_ChangePartNumMasterResponseCDeleteComponentsMessage_QNAME, String.class, ChangeRevNumMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeORelWarehouse.class)
    public JAXBElement<SalesOrderTableset> createChangeORelWarehouseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeORelWarehouse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCommissionableResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeCommissionableResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCommissionableResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CreateLinesFromHistory.class)
    public JAXBElement<SalesOrderTableset> createCreateLinesFromHistoryDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CreateLinesFromHistory.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeRMANum.class)
    public JAXBElement<SalesOrderTableset> createChangeRMANumDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeRMANum.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "opProjMsg", scope = CheckProjectIDResponse.class)
    public JAXBElement<String> createCheckProjectIDResponseOpProjMsg(String value) {
        return new JAXBElement<String>(_CheckProjectIDResponseOpProjMsg_QNAME, String.class, CheckProjectIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckProjectIDResponse.class)
    public JAXBElement<SalesOrderTableset> createCheckProjectIDResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckProjectIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeofSoldToCreditCheckResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeofSoldToCreditCheckResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeofSoldToCreditCheckResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cCreditLimitMessage", scope = OnChangeofSoldToCreditCheckResponse.class)
    public JAXBElement<String> createOnChangeofSoldToCreditCheckResponseCCreditLimitMessage(String value) {
        return new JAXBElement<String>(_CheckCustomerCreditLimitResponseCCreditLimitMessage_QNAME, String.class, OnChangeofSoldToCreditCheckResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeShipToCustID.class)
    public JAXBElement<SalesOrderTableset> createChangeShipToCustIDDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeShipToCustID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "iShipToCustID", scope = ChangeShipToCustID.class)
    public JAXBElement<String> createChangeShipToCustIDIShipToCustID(String value) {
        return new JAXBElement<String>(_ChangeOrderRelShipToCustIDIShipToCustID_QNAME, String.class, ChangeShipToCustID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OrderDtlGetNewContracts.class)
    public JAXBElement<SalesOrderTableset> createOrderDtlGetNewContractsDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OrderDtlGetNewContracts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetByIDResult", scope = GetByIDResponse.class)
    public JAXBElement<SalesOrderTableset> createGetByIDResponseGetByIDResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_GetByIDResponseGetByIDResult_QNAME, SalesOrderTableset.class, GetByIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeOfFixedAmount.class)
    public JAXBElement<SalesOrderTableset> createOnChangeOfFixedAmountDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeOfFixedAmount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "RateCode", scope = OnChangeOfFixedAmount.class)
    public JAXBElement<String> createOnChangeOfFixedAmountRateCode(String value) {
        return new JAXBElement<String>(_OnChangeOfFixedAmountRateCode_QNAME, String.class, OnChangeOfFixedAmount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "TaxCode", scope = OnChangeOfFixedAmount.class)
    public JAXBElement<String> createOnChangeOfFixedAmountTaxCode(String value) {
        return new JAXBElement<String>(_OnChangeOfTaxPercentTaxCode_QNAME, String.class, OnChangeOfFixedAmount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeMiscCodeResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeMiscCodeResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeMiscCodeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ReopenOrderLineResult", scope = ReopenOrderLineResponse.class)
    public JAXBElement<SalesOrderTableset> createReopenOrderLineResponseReopenOrderLineResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_ReopenOrderLineResponseReopenOrderLineResult_QNAME, SalesOrderTableset.class, ReopenOrderLineResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "orderLines", scope = UpdateOrderDtlDiscountPercent.class)
    public JAXBElement<String> createUpdateOrderDtlDiscountPercentOrderLines(String value) {
        return new JAXBElement<String>(_UpdateOrderDtlDiscountPercentOrderLines_QNAME, String.class, UpdateOrderDtlDiscountPercent.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CCClearResponse.class)
    public JAXBElement<SalesOrderTableset> createCCClearResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CCClearResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CreateOrderDtlComplementsResponse.class)
    public JAXBElement<SalesOrderTableset> createCreateOrderDtlComplementsResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CreateOrderDtlComplementsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeKitPricingResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeKitPricingResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeKitPricingResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ApplyOrderBasedDiscountsResult", scope = ApplyOrderBasedDiscountsResponse.class)
    public JAXBElement<SalesOrderTableset> createApplyOrderBasedDiscountsResponseApplyOrderBasedDiscountsResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_ApplyOrderBasedDiscountsResponseApplyOrderBasedDiscountsResult_QNAME, SalesOrderTableset.class, ApplyOrderBasedDiscountsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckOrderHedChanges.class)
    public JAXBElement<SalesOrderTableset> createCheckOrderHedChangesDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckOrderHedChanges.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "outMsg", scope = CheckLtrOfCrdtResponse.class)
    public JAXBElement<String> createCheckLtrOfCrdtResponseOutMsg(String value) {
        return new JAXBElement<String>(_ChangeContractNumMasterResponseOutMsg_QNAME, String.class, CheckLtrOfCrdtResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "CheckLtrOfCrdtResult", scope = CheckLtrOfCrdtResponse.class)
    public JAXBElement<SalesOrderTableset> createCheckLtrOfCrdtResponseCheckLtrOfCrdtResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_CheckLtrOfCrdtResponseCheckLtrOfCrdtResult_QNAME, SalesOrderTableset.class, CheckLtrOfCrdtResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeUnitPriceResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeUnitPriceResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeUnitPriceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderHedAttch.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderHedAttchDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderHedAttch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "iPartNum", scope = CheckKitRevision.class)
    public JAXBElement<String> createCheckKitRevisionIPartNum(String value) {
        return new JAXBElement<String>(_HasMultipleSubsIPartNum_QNAME, String.class, CheckKitRevision.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "iRevisionNum", scope = CheckKitRevision.class)
    public JAXBElement<String> createCheckKitRevisionIRevisionNum(String value) {
        return new JAXBElement<String>(_CheckKitRevisionIRevisionNum_QNAME, String.class, CheckKitRevision.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetInventoryQuantities.class)
    public JAXBElement<SalesOrderTableset> createGetInventoryQuantitiesDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetInventoryQuantities.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PONum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPONum(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowPONum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RefNotes", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowRefNotes(String value) {
        return new JAXBElement<String>(_OrderRelRowRefNotes_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DocumentTypeID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowDocumentTypeID(String value) {
        return new JAXBElement<String>(_OrderHedRowDocumentTypeID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CardNumber", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCardNumber(String value) {
        return new JAXBElement<String>(_OrderHedRowCardNumber_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipViaCodeDescription", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowShipViaCodeDescription(String value) {
        return new JAXBElement<String>(_OrderRelRowShipViaCodeDescription_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepList", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSalesRepList(String value) {
        return new JAXBElement<String>(_OrderHedRowSalesRepList_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTCustNumName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowBTCustNumName(String value) {
        return new JAXBElement<String>(_OrderHedListRowBTCustNumName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SoldToContactFaxNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSoldToContactFaxNum(String value) {
        return new JAXBElement<String>(_OrderHedRowSoldToContactFaxNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServInstruct", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowServInstruct(String value) {
        return new JAXBElement<String>(_OrderRelRowServInstruct_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FFAddress1", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowFFAddress1(String value) {
        return new JAXBElement<String>(_OrderHedRowFFAddress1_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BaseCurrSymbol", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowBaseCurrSymbol(String value) {
        return new JAXBElement<String>(_OrderDtlRowBaseCurrSymbol_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FFAddress3", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowFFAddress3(String value) {
        return new JAXBElement<String>(_OrderHedRowFFAddress3_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OurBankBankName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOurBankBankName(String value) {
        return new JAXBElement<String>(_OrderHedRowOurBankBankName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FFAddress2", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowFFAddress2(String value) {
        return new JAXBElement<String>(_OrderHedRowFFAddress2_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "GroundType", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowGroundType(String value) {
        return new JAXBElement<String>(_OrderRelRowGroundType_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipViaCode", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowShipViaCode(String value) {
        return new JAXBElement<String>(_OrderRelRowShipViaCode_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CardMemberName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCardMemberName(String value) {
        return new JAXBElement<String>(_OrderHedRowCardMemberName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCodeCurrencyID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCurrencyCodeCurrencyID(String value) {
        return new JAXBElement<String>(_OrderHedRowCurrencyCodeCurrencyID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CardID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCardID(String value) {
        return new JAXBElement<String>(_OrderHedRowCardID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSName(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CCCSCID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCCCSCID(String value) {
        return new JAXBElement<String>(_OrderHedRowCCCSCID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "EntryMethod", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowEntryMethod(String value) {
        return new JAXBElement<String>(_OrderHedRowEntryMethod_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SoldToContactEMailAddress", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSoldToContactEMailAddress(String value) {
        return new JAXBElement<String>(_OrderHedRowSoldToContactEMailAddress_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FFZip", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowFFZip(String value) {
        return new JAXBElement<String>(_OrderHedRowFFZip_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServAuthNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowServAuthNum(String value) {
        return new JAXBElement<String>(_OrderRelRowServAuthNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DoNotShipAfterDate", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowDoNotShipAfterDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowDoNotShipAfterDate_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CardStore", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCardStore(String value) {
        return new JAXBElement<String>(_OrderHedRowCardStore_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WIUsername", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowWIUsername(String value) {
        return new JAXBElement<String>(_OrderHedRowWIUsername_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RateGrpCode", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowRateGrpCode(String value) {
        return new JAXBElement<String>(_OrderHedRowRateGrpCode_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BillToCustomerName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowBillToCustomerName(String value) {
        return new JAXBElement<String>(_OrderHedRowBillToCustomerName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CreditOverrideUserID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCreditOverrideUserID(String value) {
        return new JAXBElement<String>(_OrderHedRowCreditOverrideUserID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DispatchReasonCodeDescription", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowDispatchReasonCodeDescription(String value) {
        return new JAXBElement<String>(_OrderHedRowDispatchReasonCodeDescription_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FFCompName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowFFCompName(String value) {
        return new JAXBElement<String>(_OrderHedRowFFCompName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "NotifyEMail", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowNotifyEMail(String value) {
        return new JAXBElement<String>(_OrderRelRowNotifyEMail_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SoldToContactName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSoldToContactName(String value) {
        return new JAXBElement<String>(_OrderHedRowSoldToContactName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PayAccount", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPayAccount(String value) {
        return new JAXBElement<String>(_OrderHedRowPayAccount_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CardmemberReference", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCardmemberReference(String value) {
        return new JAXBElement<String>(_OrderHedRowCardmemberReference_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "EntryPerson", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowEntryPerson(String value) {
        return new JAXBElement<String>(_OrderHedRowEntryPerson_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "XRefContractDate", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowXRefContractDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderHedRowXRefContractDate_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CardType", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCardType(String value) {
        return new JAXBElement<String>(_OrderHedRowCardType_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSSaveAs", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSSaveAs(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSSaveAs_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ECCOrderNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowECCOrderNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowECCOrderNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipToContactFaxNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowShipToContactFaxNum(String value) {
        return new JAXBElement<String>(_OrderHedRowShipToContactFaxNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDate", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowOrderDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderCustTrkRowOrderDate_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustomerCustID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCustomerCustID(String value) {
        return new JAXBElement<String>(_OrderHedListRowCustomerCustID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PayBTCountry", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPayBTCountry(String value) {
        return new JAXBElement<String>(_OrderHedRowPayBTCountry_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipToCustId", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowShipToCustId(String value) {
        return new JAXBElement<String>(_OrderHedRowShipToCustId_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSAddress1", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSAddress1(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSAddress1_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FFID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowFFID(String value) {
        return new JAXBElement<String>(_OrderHedRowFFID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CCTranID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCCTranID(String value) {
        return new JAXBElement<String>(_OrderHedRowCCTranID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PickListComment", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPickListComment(String value) {
        return new JAXBElement<String>(_OrderDtlRowPickListComment_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSAddress2", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSAddress2(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSAddress2_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CCCSCIDToken", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCCCSCIDToken(String value) {
        return new JAXBElement<String>(_OrderHedRowCCCSCIDToken_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSAddress3", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSAddress3(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSAddress3_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSCntryDescription", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSCntryDescription(String value) {
        return new JAXBElement<String>(_OrderRelRowOTSCntryDescription_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipViaCodeWebDesc", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowShipViaCodeWebDesc(String value) {
        return new JAXBElement<String>(_OrderRelRowShipViaCodeWebDesc_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCodeCurrDesc", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCurrencyCodeCurrDesc(String value) {
        return new JAXBElement<String>(_OrderHedRowCurrencyCodeCurrDesc_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSTaxRegionCode", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSTaxRegionCode(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSTaxRegionCode_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BaseCurrencyID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowBaseCurrencyID(String value) {
        return new JAXBElement<String>(_OrderDtlRowBaseCurrencyID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RateGrpDescription", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowRateGrpDescription(String value) {
        return new JAXBElement<String>(_OrderHedRowRateGrpDescription_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "UPSQVShipFromName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowUPSQVShipFromName(String value) {
        return new JAXBElement<String>(_OrderHedRowUPSQVShipFromName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustTradePartnerName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCustTradePartnerName(String value) {
        return new JAXBElement<String>(_OrderHedRowCustTradePartnerName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DeliveryType", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowDeliveryType(String value) {
        return new JAXBElement<String>(_OrderRelRowDeliveryType_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DemandProcessDate", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowDemandProcessDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderHedRowDemandProcessDate_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ReservePriDescription", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowReservePriDescription(String value) {
        return new JAXBElement<String>(_OrderHedRowReservePriDescription_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CCApprovalNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCCApprovalNum(String value) {
        return new JAXBElement<String>(_OrderHedRowCCApprovalNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LastTCtrlNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowLastTCtrlNum(String value) {
        return new JAXBElement<String>(_OrderHedRowLastTCtrlNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PayBTPhone", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPayBTPhone(String value) {
        return new JAXBElement<String>(_OrderHedRowPayBTPhone_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WIApplication", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowWIApplication(String value) {
        return new JAXBElement<String>(_OrderHedRowWIApplication_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WIUserID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowWIUserID(String value) {
        return new JAXBElement<String>(_OrderHedRowWIUserID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FFCity", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowFFCity(String value) {
        return new JAXBElement<String>(_OrderHedRowFFCity_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTContactEMailAddress", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowBTContactEMailAddress(String value) {
        return new JAXBElement<String>(_OrderHedRowBTContactEMailAddress_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "UserDate2", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowUserDate2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderHedRowUserDate2_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "UserDate1", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowUserDate1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderHedRowUserDate1_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "XRefContractNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowXRefContractNum(String value) {
        return new JAXBElement<String>(_OrderHedRowXRefContractNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "UserDate4", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowUserDate4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderHedRowUserDate4_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "UserDate3", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowUserDate3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderHedRowUserDate3_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTCustID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowBTCustID(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowBTCustID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WebEntryPerson", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowWebEntryPerson(String value) {
        return new JAXBElement<String>(_OrderHedRowWebEntryPerson_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CCStreetAddr", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCCStreetAddr(String value) {
        return new JAXBElement<String>(_OrderHedRowCCStreetAddr_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ReferencePNRef", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowReferencePNRef(String value) {
        return new JAXBElement<String>(_OrderHedRowReferencePNRef_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CardTypeDescription", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCardTypeDescription(String value) {
        return new JAXBElement<String>(_OrderHedRowCardTypeDescription_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PSCurrCode", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPSCurrCode(String value) {
        return new JAXBElement<String>(_OrderHedRowPSCurrCode_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ReservePriorityCode", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowReservePriorityCode(String value) {
        return new JAXBElement<String>(_OrderHedRowReservePriorityCode_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CreditOverrideTime", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCreditOverrideTime(String value) {
        return new JAXBElement<String>(_OrderHedRowCreditOverrideTime_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSResaleID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSResaleID(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSResaleID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServRef5", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowServRef5(String value) {
        return new JAXBElement<String>(_OrderRelRowServRef5_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServRef2", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowServRef2(String value) {
        return new JAXBElement<String>(_OrderRelRowServRef2_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServRef1", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowServRef1(String value) {
        return new JAXBElement<String>(_OrderRelRowServRef1_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RequestDate", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowRequestDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowRequestDate_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServRef4", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowServRef4(String value) {
        return new JAXBElement<String>(_OrderRelRowServRef4_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServRef3", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowServRef3(String value) {
        return new JAXBElement<String>(_OrderRelRowServRef3_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCodeCurrName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCurrencyCodeCurrName(String value) {
        return new JAXBElement<String>(_OrderHedRowCurrencyCodeCurrName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CCZip", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCCZip(String value) {
        return new JAXBElement<String>(_OrderHedRowCCZip_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTContactPhoneNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowBTContactPhoneNum(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowBTContactPhoneNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustomerBTName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCustomerBTName(String value) {
        return new JAXBElement<String>(_OrderHedListRowCustomerBTName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipToContactName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowShipToContactName(String value) {
        return new JAXBElement<String>(_OrderRelRowShipToContactName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FFContact", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowFFContact(String value) {
        return new JAXBElement<String>(_OrderHedRowFFContact_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxRegionCodeDescription", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowTaxRegionCodeDescription(String value) {
        return new JAXBElement<String>(_OrderRelRowTaxRegionCodeDescription_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSFaxNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSFaxNum(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSFaxNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "InvoiceComment", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowInvoiceComment(String value) {
        return new JAXBElement<String>(_OrderDtlRowInvoiceComment_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipToContactEMailAddress", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowShipToContactEMailAddress(String value) {
        return new JAXBElement<String>(_OrderRelRowShipToContactEMailAddress_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SoldToContactPhoneNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSoldToContactPhoneNum(String value) {
        return new JAXBElement<String>(_OrderHedRowSoldToContactPhoneNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipToContactPhoneNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowShipToContactPhoneNum(String value) {
        return new JAXBElement<String>(_OrderHedRowShipToContactPhoneNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSCity", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSCity(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSCity_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ProcessCard", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowProcessCard(String value) {
        return new JAXBElement<String>(_OrderHedRowProcessCard_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PayBTCity", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPayBTCity(String value) {
        return new JAXBElement<String>(_OrderHedRowPayBTCity_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ECCPaymentMethod", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowECCPaymentMethod(String value) {
        return new JAXBElement<String>(_OrderHedRowECCPaymentMethod_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LastBatchNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowLastBatchNum(String value) {
        return new JAXBElement<String>(_OrderHedRowLastBatchNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FOB", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowFOB(String value) {
        return new JAXBElement<String>(_OrderHedRowFOB_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "HDCaseDescription", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowHDCaseDescription(String value) {
        return new JAXBElement<String>(_OrderHedRowHDCaseDescription_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FFState", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowFFState(String value) {
        return new JAXBElement<String>(_OrderHedRowFFState_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderCSR", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOrderCSR(String value) {
        return new JAXBElement<String>(_OrderHedRowOrderCSR_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OurBank", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOurBank(String value) {
        return new JAXBElement<String>(_OrderHedRowOurBank_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ProFormaInvComment", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowProFormaInvComment(String value) {
        return new JAXBElement<String>(_OrderDtlRowProFormaInvComment_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipToAddressList", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowShipToAddressList(String value) {
        return new JAXBElement<String>(_OrderRelRowShipToAddressList_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxRateDate", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowTaxRateDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderRelTaxRowTaxRateDate_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepCode4", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSalesRepCode4(String value) {
        return new JAXBElement<String>(_OrderHedRowSalesRepCode4_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepCode5", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSalesRepCode5(String value) {
        return new JAXBElement<String>(_OrderHedRowSalesRepCode5_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DemandContract", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowDemandContract(String value) {
        return new JAXBElement<String>(_OrderHedListRowDemandContract_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepCode2", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSalesRepCode2(String value) {
        return new JAXBElement<String>(_OrderHedRowSalesRepCode2_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepCode3", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSalesRepCode3(String value) {
        return new JAXBElement<String>(_OrderHedRowSalesRepCode3_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepCode1", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSalesRepCode1(String value) {
        return new JAXBElement<String>(_OrderHedRowSalesRepCode1_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSZIP", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSZIP(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSZIP_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "EntryProcess", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowEntryProcess(String value) {
        return new JAXBElement<String>(_OrderDtlRowEntryProcess_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "dspBTCustID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowDspBTCustID(String value) {
        return new JAXBElement<String>(_OrderHedRowDspBTCustID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTContactFaxNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowBTContactFaxNum(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowBTContactFaxNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTCustNumBTName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowBTCustNumBTName(String value) {
        return new JAXBElement<String>(_OrderHedRowBTCustNumBTName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TermsCode", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowTermsCode(String value) {
        return new JAXBElement<String>(_OrderHedRowTermsCode_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PSCurrCurrDesc", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPSCurrCurrDesc(String value) {
        return new JAXBElement<String>(_OrderHedRowPSCurrCurrDesc_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AllocPriorityCode", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowAllocPriorityCode(String value) {
        return new JAXBElement<String>(_OrderHedRowAllocPriorityCode_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DoNotShipBeforeDate", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowDoNotShipBeforeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowDoNotShipBeforeDate_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LinkMsg", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowLinkMsg(String value) {
        return new JAXBElement<String>(_OrderHedRowLinkMsg_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "UPSQVMemo", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowUPSQVMemo(String value) {
        return new JAXBElement<String>(_OrderHedRowUPSQVMemo_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "WIOrder", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowWIOrder(String value) {
        return new JAXBElement<String>(_OrderRelRowWIOrder_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CCTranType", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCCTranType(String value) {
        return new JAXBElement<String>(_OrderHedRowCCTranType_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LegalNumber", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowLegalNumber(String value) {
        return new JAXBElement<String>(_OrderHedRowLegalNumber_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CustomerName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCustomerName(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowCustomerName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FOBDescription", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowFOBDescription(String value) {
        return new JAXBElement<String>(_OrderHedRowFOBDescription_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Plant", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPlant(String value) {
        return new JAXBElement<String>(_JobProdRowPlant_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DispatchReason", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowDispatchReason(String value) {
        return new JAXBElement<String>(_OrderHedRowDispatchReason_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CancelAfterDate", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowCancelAfterDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderHedRowCancelAfterDate_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ExtCompany", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowExtCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowExtCompany_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepName3", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSalesRepName3(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesRepName3_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepName2", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSalesRepName2(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesRepName2_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepName1", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSalesRepName1(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesRepName1_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTContactName", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowBTContactName(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowBTContactName_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepName5", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSalesRepName5(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesRepName5_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesRepName4", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSalesRepName4(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesRepName4_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TermsCodeDescription", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowTermsCodeDescription(String value) {
        return new JAXBElement<String>(_OrderHedRowTermsCodeDescription_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipComment", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowShipComment(String value) {
        return new JAXBElement<String>(_OrderDtlRowShipComment_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CSCResult", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCSCResult(String value) {
        return new JAXBElement<String>(_OrderHedRowCSCResult_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCodeCurrSymbol", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCurrencyCodeCurrSymbol(String value) {
        return new JAXBElement<String>(_OrderHedRowCurrencyCodeCurrSymbol_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSPhoneNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSPhoneNum(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSPhoneNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSSaveCustID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSSaveCustID(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSSaveCustID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ShipToNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowShipToNum(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowShipToNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ARLOCID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowARLOCID(String value) {
        return new JAXBElement<String>(_OrderHedRowARLOCID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PayBTState", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPayBTState(String value) {
        return new JAXBElement<String>(_OrderHedRowPayBTState_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCode", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrencyCode_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ECCPONum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowECCPONum(String value) {
        return new JAXBElement<String>(_OrderHedRowECCPONum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTCustNumCustID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowBTCustNumCustID(String value) {
        return new JAXBElement<String>(_OrderHedListRowBTCustNumCustID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AVSAddr", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowAVSAddr(String value) {
        return new JAXBElement<String>(_OrderHedRowAVSAddr_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangeDate", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowChangeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowChangeDate_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AvailBTCustList", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowAvailBTCustList(String value) {
        return new JAXBElement<String>(_OrderHedRowAvailBTCustList_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "InvCurrCurrDesc", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowInvCurrCurrDesc(String value) {
        return new JAXBElement<String>(_OrderHedRowInvCurrCurrDesc_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FFCountry", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowFFCountry(String value) {
        return new JAXBElement<String>(_OrderHedRowFFCountry_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderComment", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOrderComment(String value) {
        return new JAXBElement<String>(_OrderDtlRowOrderComment_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangedBy", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowChangedBy(String value) {
        return new JAXBElement<String>(_OrderDtlRowChangedBy_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PayBTZip", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPayBTZip(String value) {
        return new JAXBElement<String>(_OrderHedRowPayBTZip_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSContact", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSContact(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSContact_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCodeDocumentDesc", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCurrencyCodeDocumentDesc(String value) {
        return new JAXBElement<String>(_OrderHedRowCurrencyCodeDocumentDesc_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OurBankDescription", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOurBankDescription(String value) {
        return new JAXBElement<String>(_OrderHedRowOurBankDescription_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "LastScheduleNumber", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowLastScheduleNumber(String value) {
        return new JAXBElement<String>(_OrderHedRowLastScheduleNumber_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxRegionCode", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowTaxRegionCode(String value) {
        return new JAXBElement<String>(_OrderRelRowTaxRegionCode_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PayBTAddress2", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPayBTAddress2(String value) {
        return new JAXBElement<String>(_OrderHedRowPayBTAddress2_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PayBTAddress3", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPayBTAddress3(String value) {
        return new JAXBElement<String>(_OrderHedRowPayBTAddress3_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "AVSZip", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowAVSZip(String value) {
        return new JAXBElement<String>(_OrderHedRowAVSZip_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FFPhoneNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowFFPhoneNum(String value) {
        return new JAXBElement<String>(_OrderHedRowFFPhoneNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PayBTAddress1", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPayBTAddress1(String value) {
        return new JAXBElement<String>(_OrderHedRowPayBTAddress1_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxPoint", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowTaxPoint(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderHedRowTaxPoint_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServPhone", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowServPhone(String value) {
        return new JAXBElement<String>(_OrderRelRowServPhone_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSState", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSState(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSState_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderStatus", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOrderStatus(String value) {
        return new JAXBElement<String>(_OrderHedRowOrderStatus_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "InvCurrCode", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowInvCurrCode(String value) {
        return new JAXBElement<String>(_OrderHedRowInvCurrCode_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PayFlag", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowPayFlag(String value) {
        return new JAXBElement<String>(_OrderHedRowPayFlag_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "UserChar3", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowUserChar3(String value) {
        return new JAXBElement<String>(_OrderHedRowUserChar3_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "UserChar4", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowUserChar4(String value) {
        return new JAXBElement<String>(_OrderHedRowUserChar4_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "UserChar1", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowUserChar1(String value) {
        return new JAXBElement<String>(_OrderHedRowUserChar1_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "UserChar2", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowUserChar2(String value) {
        return new JAXBElement<String>(_OrderHedRowUserChar2_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BTAddressList", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowBTAddressList(String value) {
        return new JAXBElement<String>(_OrderCustTrkRowBTAddressList_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SoldToAddressList", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowSoldToAddressList(String value) {
        return new JAXBElement<String>(_OrderHedRowSoldToAddressList_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TranDocTypeID", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowTranDocTypeID(String value) {
        return new JAXBElement<String>(_OrderHedRowTranDocTypeID_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CreditOverrideDate", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowCreditOverrideDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderHedRowCreditOverrideDate_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CCResponse", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowCCResponse(String value) {
        return new JAXBElement<String>(_OrderHedRowCCResponse_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "NeedByDate", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowNeedByDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowNeedByDate_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OTSShipToNum", scope = OrderHedRow.class)
    public JAXBElement<String> createOrderHedRowOTSShipToNum(String value) {
        return new JAXBElement<String>(_SaveOTSParamsRowOTSShipToNum_QNAME, String.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ServDeliveryDate", scope = OrderHedRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHedRowServDeliveryDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderRelRowServDeliveryDate_QNAME, XMLGregorianCalendar.class, OrderHedRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeDiscBreakListCodeResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeDiscBreakListCodeResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeDiscBreakListCodeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelDropShip.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelDropShipDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelDropShip.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CCLoadTranDataResponse.class)
    public JAXBElement<SalesOrderTableset> createCCLoadTranDataResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CCLoadTranDataResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeContractNumResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeContractNumResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeContractNumResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cCreditLimitMessage", scope = CheckCustOnCreditHoldResponse.class)
    public JAXBElement<String> createCheckCustOnCreditHoldResponseCCreditLimitMessage(String value) {
        return new JAXBElement<String>(_CheckCustomerCreditLimitResponseCCreditLimitMessage_QNAME, String.class, CheckCustOnCreditHoldResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeODtlWarehouse.class)
    public JAXBElement<SalesOrderTableset> createChangeODtlWarehouseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeODtlWarehouse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeShipToIDResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeShipToIDResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeShipToIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuoteQtyTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "QuoteQty", scope = OrdDtlQuoteQtyTableset.class)
    public JAXBElement<QuoteQtyTable> createOrdDtlQuoteQtyTablesetQuoteQty(QuoteQtyTable value) {
        return new JAXBElement<QuoteQtyTable>(_OrdDtlQuoteQtyTablesetQuoteQty_QNAME, QuoteQtyTable.class, OrdDtlQuoteQtyTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipPONum", scope = CopyOrder.class)
    public JAXBElement<String> createCopyOrderIpPONum(String value) {
        return new JAXBElement<String>(_CopyOrderIpPONum_QNAME, String.class, CopyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveOTSParamsTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CreateOrderFromQuoteSaveOTS.class)
    public JAXBElement<SaveOTSParamsTableset> createCreateOrderFromQuoteSaveOTSDs(SaveOTSParamsTableset value) {
        return new JAXBElement<SaveOTSParamsTableset>(_SetUPSQVEnableResponseDs_QNAME, SaveOTSParamsTableset.class, CreateOrderFromQuoteSaveOTS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOverridePriceList.class)
    public JAXBElement<SalesOrderTableset> createChangeOverridePriceListDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOverridePriceList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeRevisionNum.class)
    public JAXBElement<SalesOrderTableset> createChangeRevisionNumDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeRevisionNum.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeTaxableAmt.class)
    public JAXBElement<SalesOrderTableset> createOnChangeTaxableAmtDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeTaxableAmt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "RateCode", scope = OnChangeTaxableAmt.class)
    public JAXBElement<String> createOnChangeTaxableAmtRateCode(String value) {
        return new JAXBElement<String>(_OnChangeOfFixedAmountRateCode_QNAME, String.class, OnChangeTaxableAmt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "TaxCode", scope = OnChangeTaxableAmt.class)
    public JAXBElement<String> createOnChangeTaxableAmtTaxCode(String value) {
        return new JAXBElement<String>(_OnChangeOfTaxPercentTaxCode_QNAME, String.class, OnChangeTaxableAmt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeDiscountAmountResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeDiscountAmountResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeDiscountAmountResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCommissionable.class)
    public JAXBElement<SalesOrderTableset> createChangeCommissionableDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCommissionable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCreditExp.class)
    public JAXBElement<SalesOrderTableset> createChangeCreditExpDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCreditExp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectSerialNumbersParamsTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetSelectSerialNumbersParamsResult", scope = GetSelectSerialNumbersParamsResponse.class)
    public JAXBElement<SelectSerialNumbersParamsTableset> createGetSelectSerialNumbersParamsResponseGetSelectSerialNumbersParamsResult(SelectSerialNumbersParamsTableset value) {
        return new JAXBElement<SelectSerialNumbersParamsTableset>(_GetSelectSerialNumbersParamsResponseGetSelectSerialNumbersParamsResult_QNAME, SelectSerialNumbersParamsTableset.class, GetSelectSerialNumbersParamsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "iPartNum", scope = IsRunOutOnHand.class)
    public JAXBElement<String> createIsRunOutOnHandIPartNum(String value) {
        return new JAXBElement<String>(_HasMultipleSubsIPartNum_QNAME, String.class, IsRunOutOnHand.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeXPartNumResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeXPartNumResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeXPartNumResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GlbSugPOChgUpdateResponse.class)
    public JAXBElement<GlbSugPOChgTableset> createGlbSugPOChgUpdateResponseDs(GlbSugPOChgTableset value) {
        return new JAXBElement<GlbSugPOChgTableset>(_SetUPSQVEnableResponseDs_QNAME, GlbSugPOChgTableset.class, GlbSugPOChgUpdateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeTaxCodeResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeTaxCodeResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeTaxCodeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckSellingFactorDirectionResponse.class)
    public JAXBElement<SalesOrderTableset> createCheckSellingFactorDirectionResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckSellingFactorDirectionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "outMsg", scope = CheckKitRevisionResponse.class)
    public JAXBElement<String> createCheckKitRevisionResponseOutMsg(String value) {
        return new JAXBElement<String>(_ChangeContractNumMasterResponseOutMsg_QNAME, String.class, CheckKitRevisionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderHedResponse.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderHedResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderHedResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "configureRevision", scope = CheckConfigurationResponse.class)
    public JAXBElement<String> createCheckConfigurationResponseConfigureRevision(String value) {
        return new JAXBElement<String>(_CheckConfigurationResponseConfigureRevision_QNAME, String.class, CheckConfigurationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "reasonMessage", scope = CheckConfigurationResponse.class)
    public JAXBElement<String> createCheckConfigurationResponseReasonMessage(String value) {
        return new JAXBElement<String>(_CheckConfigurationResponseReasonMessage_QNAME, String.class, CheckConfigurationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipRateGrpCode", scope = CheckRateGrpCode.class)
    public JAXBElement<String> createCheckRateGrpCodeIpRateGrpCode(String value) {
        return new JAXBElement<String>(_CheckRateGrpCodeIpRateGrpCode_QNAME, String.class, CheckRateGrpCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckRateGrpCode.class)
    public JAXBElement<SalesOrderTableset> createCheckRateGrpCodeDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckRateGrpCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedListTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetListCustomResult", scope = GetListCustomResponse.class)
    public JAXBElement<OrderHedListTableset> createGetListCustomResponseGetListCustomResult(OrderHedListTableset value) {
        return new JAXBElement<OrderHedListTableset>(_GetListCustomResponseGetListCustomResult_QNAME, OrderHedListTableset.class, GetListCustomResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeHedUseOTSResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeHedUseOTSResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeHedUseOTSResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartDescription", scope = OrderHistRow.class)
    public JAXBElement<String> createOrderHistRowPartDescription(String value) {
        return new JAXBElement<String>(_OrderHistRowPartDescription_QNAME, String.class, OrderHistRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderDate", scope = OrderHistRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderHistRowOrderDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderCustTrkRowOrderDate_QNAME, XMLGregorianCalendar.class, OrderHistRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = OrderHistRow.class)
    public JAXBElement<String> createOrderHistRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, OrderHistRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "UOM", scope = OrderHistRow.class)
    public JAXBElement<String> createOrderHistRowUOM(String value) {
        return new JAXBElement<String>(_OrderHistRowUOM_QNAME, String.class, OrderHistRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNum", scope = OrderHistRow.class)
    public JAXBElement<String> createOrderHistRowPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNum_QNAME, String.class, OrderHistRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcNeqQtyAction", scope = ChangeWhseCodeMasterResponse.class)
    public JAXBElement<String> createChangeWhseCodeMasterResponsePcNeqQtyAction(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterResponsePcNeqQtyAction_QNAME, String.class, ChangeWhseCodeMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeWhseCodeMasterResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeWhseCodeMasterResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeWhseCodeMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcMessage", scope = ChangeWhseCodeMasterResponse.class)
    public JAXBElement<String> createChangeWhseCodeMasterResponsePcMessage(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterResponsePcMessage_QNAME, String.class, ChangeWhseCodeMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderDtlAttch.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderDtlAttchDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderDtlAttch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelMarkForNum.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelMarkForNumDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelMarkForNum.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ProposedMarkForNum", scope = ChangeOrderRelMarkForNum.class)
    public JAXBElement<String> createChangeOrderRelMarkForNumProposedMarkForNum(String value) {
        return new JAXBElement<String>(_ChangeOrderRelMarkForNumProposedMarkForNum_QNAME, String.class, ChangeOrderRelMarkForNum.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetGlbSugPOChgResult", scope = GetGlbSugPOChgResponse.class)
    public JAXBElement<GlbSugPOChgTableset> createGetGlbSugPOChgResponseGetGlbSugPOChgResult(GlbSugPOChgTableset value) {
        return new JAXBElement<GlbSugPOChgTableset>(_GetGlbSugPOChgResponseGetGlbSugPOChgResult_QNAME, GlbSugPOChgTableset.class, GetGlbSugPOChgResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeSoldToContactResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeSoldToContactResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeSoldToContactResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeVendorChangeResponse.class)
    public JAXBElement<GlbSugPOChgTableset> createChangeVendorChangeResponseDs(GlbSugPOChgTableset value) {
        return new JAXBElement<GlbSugPOChgTableset>(_SetUPSQVEnableResponseDs_QNAME, GlbSugPOChgTableset.class, ChangeVendorChangeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckOrderHedDispatchReason.class)
    public JAXBElement<SalesOrderTableset> createCheckOrderHedDispatchReasonDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckOrderHedDispatchReason.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipDispatchReason", scope = CheckOrderHedDispatchReason.class)
    public JAXBElement<String> createCheckOrderHedDispatchReasonIpDispatchReason(String value) {
        return new JAXBElement<String>(_CheckOrderHedDispatchReasonIpDispatchReason_QNAME, String.class, CheckOrderHedDispatchReason.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeofBTCustIDResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeofBTCustIDResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeofBTCustIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeContractNumMaster.class)
    public JAXBElement<SalesOrderTableset> createChangeContractNumMasterDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeContractNumMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeManualTaxCalcResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeManualTaxCalcResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeManualTaxCalcResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelDropShipResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelDropShipResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelDropShipResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DrawDesc", scope = OrderHedAttchRow.class)
    public JAXBElement<String> createOrderHedAttchRowDrawDesc(String value) {
        return new JAXBElement<String>(_OrderDtlAttchRowDrawDesc_QNAME, String.class, OrderHedAttchRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DocTypeID", scope = OrderHedAttchRow.class)
    public JAXBElement<String> createOrderHedAttchRowDocTypeID(String value) {
        return new JAXBElement<String>(_OrderDtlAttchRowDocTypeID_QNAME, String.class, OrderHedAttchRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = OrderHedAttchRow.class)
    public JAXBElement<String> createOrderHedAttchRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, OrderHedAttchRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FileName", scope = OrderHedAttchRow.class)
    public JAXBElement<String> createOrderHedAttchRowFileName(String value) {
        return new JAXBElement<String>(_OrderDtlAttchRowFileName_QNAME, String.class, OrderHedAttchRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PDMDocID", scope = OrderHedAttchRow.class)
    public JAXBElement<String> createOrderHedAttchRowPDMDocID(String value) {
        return new JAXBElement<String>(_OrderDtlAttchRowPDMDocID_QNAME, String.class, OrderHedAttchRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "CloseReleaseResult", scope = CloseReleaseResponse.class)
    public JAXBElement<SalesOrderTableset> createCloseReleaseResponseCloseReleaseResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_CloseReleaseResponseCloseReleaseResult_QNAME, SalesOrderTableset.class, CloseReleaseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeNewSellingQuantity.class)
    public JAXBElement<GlbSugPOChgTableset> createChangeNewSellingQuantityDs(GlbSugPOChgTableset value) {
        return new JAXBElement<GlbSugPOChgTableset>(_SetUPSQVEnableResponseDs_QNAME, GlbSugPOChgTableset.class, ChangeNewSellingQuantity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeQuickEntryOption.class)
    public JAXBElement<SalesOrderTableset> createChangeQuickEntryOptionDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeQuickEntryOption.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeofBTConNum.class)
    public JAXBElement<SalesOrderTableset> createOnChangeofBTConNumDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeofBTConNum.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice", name = "TableName", scope = BOUpdErrorRow.class)
    public JAXBElement<String> createBOUpdErrorRowTableName(String value) {
        return new JAXBElement<String>(_BOUpdErrorRowTableName_QNAME, String.class, BOUpdErrorRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice", name = "ErrorText", scope = BOUpdErrorRow.class)
    public JAXBElement<String> createBOUpdErrorRowErrorText(String value) {
        return new JAXBElement<String>(_BOUpdErrorRowErrorText_QNAME, String.class, BOUpdErrorRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice", name = "ErrorType", scope = BOUpdErrorRow.class)
    public JAXBElement<String> createBOUpdErrorRowErrorType(String value) {
        return new JAXBElement<String>(_BOUpdErrorRowErrorType_QNAME, String.class, BOUpdErrorRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice", name = "ErrorLevel", scope = BOUpdErrorRow.class)
    public JAXBElement<String> createBOUpdErrorRowErrorLevel(String value) {
        return new JAXBElement<String>(_BOUpdErrorRowErrorLevel_QNAME, String.class, BOUpdErrorRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "errorMsg", scope = GetKitComponentsResponse.class)
    public JAXBElement<String> createGetKitComponentsResponseErrorMsg(String value) {
        return new JAXBElement<String>(_GetKitComponentsResponseErrorMsg_QNAME, String.class, GetKitComponentsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetKitComponentsResponse.class)
    public JAXBElement<SalesOrderTableset> createGetKitComponentsResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetKitComponentsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeERSOrderResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeERSOrderResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeERSOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeXPartNum.class)
    public JAXBElement<SalesOrderTableset> createChangeXPartNumDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeXPartNum.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "GroupID", scope = HedTaxSumRow.class)
    public JAXBElement<String> createHedTaxSumRowGroupID(String value) {
        return new JAXBElement<String>(_HedTaxSumRowGroupID_QNAME, String.class, HedTaxSumRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCode", scope = HedTaxSumRow.class)
    public JAXBElement<String> createHedTaxSumRowCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrencyCode_QNAME, String.class, HedTaxSumRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxDescription", scope = HedTaxSumRow.class)
    public JAXBElement<String> createHedTaxSumRowTaxDescription(String value) {
        return new JAXBElement<String>(_HedTaxSumRowTaxDescription_QNAME, String.class, HedTaxSumRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = HedTaxSumRow.class)
    public JAXBElement<String> createHedTaxSumRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, HedTaxSumRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DocDisplaySymbol", scope = HedTaxSumRow.class)
    public JAXBElement<String> createHedTaxSumRowDocDisplaySymbol(String value) {
        return new JAXBElement<String>(_OrderRelTaxRowDocDisplaySymbol_QNAME, String.class, HedTaxSumRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "DisplaySymbol", scope = HedTaxSumRow.class)
    public JAXBElement<String> createHedTaxSumRowDisplaySymbol(String value) {
        return new JAXBElement<String>(_OrderRelTaxRowDisplaySymbol_QNAME, String.class, HedTaxSumRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RateCode", scope = HedTaxSumRow.class)
    public JAXBElement<String> createHedTaxSumRowRateCode(String value) {
        return new JAXBElement<String>(_OrderRelTaxRowRateCode_QNAME, String.class, HedTaxSumRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "TaxCode", scope = HedTaxSumRow.class)
    public JAXBElement<String> createHedTaxSumRowTaxCode(String value) {
        return new JAXBElement<String>(_OrderRelTaxRowTaxCode_QNAME, String.class, HedTaxSumRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeTaxCode.class)
    public JAXBElement<SalesOrderTableset> createOnChangeTaxCodeDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeTaxCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipTaxCode", scope = OnChangeTaxCode.class)
    public JAXBElement<String> createOnChangeTaxCodeIpTaxCode(String value) {
        return new JAXBElement<String>(_ChangeManualTaxCalcIpTaxCode_QNAME, String.class, OnChangeTaxCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ProposedPlant", scope = ChangePlant.class)
    public JAXBElement<String> createChangePlantProposedPlant(String value) {
        return new JAXBElement<String>(_ChangePlantProposedPlant_QNAME, String.class, ChangePlant.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangePlant.class)
    public JAXBElement<SalesOrderTableset> createChangePlantDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangePlant.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcDimCode", scope = NegativeInventoryTest.class)
    public JAXBElement<String> createNegativeInventoryTestPcDimCode(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterPcDimCode_QNAME, String.class, NegativeInventoryTest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcPartNum", scope = NegativeInventoryTest.class)
    public JAXBElement<String> createNegativeInventoryTestPcPartNum(String value) {
        return new JAXBElement<String>(_GetBreakListCodeDescPcPartNum_QNAME, String.class, NegativeInventoryTest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcWhseCode", scope = NegativeInventoryTest.class)
    public JAXBElement<String> createNegativeInventoryTestPcWhseCode(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterPcWhseCode_QNAME, String.class, NegativeInventoryTest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcLotNum", scope = NegativeInventoryTest.class)
    public JAXBElement<String> createNegativeInventoryTestPcLotNum(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterPcLotNum_QNAME, String.class, NegativeInventoryTest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcBinNum", scope = NegativeInventoryTest.class)
    public JAXBElement<String> createNegativeInventoryTestPcBinNum(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterPcBinNum_QNAME, String.class, NegativeInventoryTest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangeDate", scope = OrderMscRow.class)
    public JAXBElement<XMLGregorianCalendar> createOrderMscRowChangeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderDtlRowChangeDate_QNAME, XMLGregorianCalendar.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Quoting", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowQuoting(String value) {
        return new JAXBElement<String>(_OHOrderMscRowQuoting_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangeTrackStatus", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowChangeTrackStatus(String value) {
        return new JAXBElement<String>(_OHOrderMscRowChangeTrackStatus_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "FreqCode", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowFreqCode(String value) {
        return new JAXBElement<String>(_OHOrderMscRowFreqCode_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderNumCardMemberName", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowOrderNumCardMemberName(String value) {
        return new JAXBElement<String>(_OrderDtlRowOrderNumCardMemberName_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrSymbol", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowCurrSymbol(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrSymbol_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MiscCode", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowMiscCode(String value) {
        return new JAXBElement<String>(_OHOrderMscRowMiscCode_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderNumCurrencyCode", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowOrderNumCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowOrderNumCurrencyCode_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Description", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowDescription(String value) {
        return new JAXBElement<String>(_OHOrderMscRowDescription_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangedBy", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowChangedBy(String value) {
        return new JAXBElement<String>(_OrderDtlRowChangedBy_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "MiscCodeDescription", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowMiscCodeDescription(String value) {
        return new JAXBElement<String>(_OHOrderMscRowMiscCodeDescription_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangeTrackMemoText", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowChangeTrackMemoText(String value) {
        return new JAXBElement<String>(_OHOrderMscRowChangeTrackMemoText_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "EntryProcess", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowEntryProcess(String value) {
        return new JAXBElement<String>(_OrderDtlRowEntryProcess_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ChangeTrackMemoDesc", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowChangeTrackMemoDesc(String value) {
        return new JAXBElement<String>(_OHOrderMscRowChangeTrackMemoDesc_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ExtCompany", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowExtCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowExtCompany_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "BaseCurrSymbol", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowBaseCurrSymbol(String value) {
        return new JAXBElement<String>(_OrderDtlRowBaseCurrSymbol_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "CurrencyCode", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowCurrencyCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowCurrencyCode_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Type", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowType(String value) {
        return new JAXBElement<String>(_OHOrderMscRowType_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderLineLineDesc", scope = OrderMscRow.class)
    public JAXBElement<String> createOrderMscRowOrderLineLineDesc(String value) {
        return new JAXBElement<String>(_JobProdRowOrderLineLineDesc_QNAME, String.class, OrderMscRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OrderDtlGetNewContractsResponse.class)
    public JAXBElement<SalesOrderTableset> createOrderDtlGetNewContractsResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OrderDtlGetNewContractsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeRelUseOTS.class)
    public JAXBElement<SalesOrderTableset> createChangeRelUseOTSDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeRelUseOTS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeVendorChange.class)
    public JAXBElement<GlbSugPOChgTableset> createChangeVendorChangeDs(GlbSugPOChgTableset value) {
        return new JAXBElement<GlbSugPOChgTableset>(_SetUPSQVEnableResponseDs_QNAME, GlbSugPOChgTableset.class, ChangeVendorChange.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCustomerResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeCustomerResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = Update.class)
    public JAXBElement<SalesOrderTableset> createUpdateDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, Update.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelShipTo.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelShipToDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelShipTo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangePricePerCodeResponse.class)
    public JAXBElement<SalesOrderTableset> createChangePricePerCodeResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangePricePerCodeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeofBTConNumResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeofBTConNumResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeofBTConNumResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SalesUM", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowSalesUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowSalesUM_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNumPricePerCode", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowPartNumPricePerCode(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNumPricePerCode_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Comment", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowComment(String value) {
        return new JAXBElement<String>(_GlbSugPOChgRowComment_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "Company", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowCompany(String value) {
        return new JAXBElement<String>(_OrderDtlRowCompany_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SubType", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowSubType(String value) {
        return new JAXBElement<String>(_PartSubsRowSubType_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SubPart", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowSubPart(String value) {
        return new JAXBElement<String>(_PartSubsRowSubPart_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNum", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowPartNum(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNum_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNumIUM", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowPartNumIUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNumIUM_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "RecType", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowRecType(String value) {
        return new JAXBElement<String>(_PartSubsRowRecType_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SubPartPartDescription", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowSubPartPartDescription(String value) {
        return new JAXBElement<String>(_PartSubsRowSubPartPartDescription_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SubPartIUM", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowSubPartIUM(String value) {
        return new JAXBElement<String>(_PartSubsRowSubPartIUM_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNumPartDescription", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowPartNumPartDescription(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNumPartDescription_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SubPartPricePerCode", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowSubPartPricePerCode(String value) {
        return new JAXBElement<String>(_PartSubsRowSubPartPricePerCode_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "PartNumSalesUM", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowPartNumSalesUM(String value) {
        return new JAXBElement<String>(_OrderDtlRowPartNumSalesUM_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SubPartSalesUM", scope = PartSubsRow.class)
    public JAXBElement<String> createPartSubsRowSubPartSalesUM(String value) {
        return new JAXBElement<String>(_PartSubsRowSubPartSalesUM_QNAME, String.class, PartSubsRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipARLOCID", scope = OnChangeARLOCID.class)
    public JAXBElement<String> createOnChangeARLOCIDIpARLOCID(String value) {
        return new JAXBElement<String>(_OnChangeARLOCIDIpARLOCID_QNAME, String.class, OnChangeARLOCID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeARLOCID.class)
    public JAXBElement<SalesOrderTableset> createOnChangeARLOCIDDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeARLOCID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeDiscBreakListCode.class)
    public JAXBElement<SalesOrderTableset> createChangeDiscBreakListCodeDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeDiscBreakListCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cRepName", scope = ChangeSalesRepResponse.class)
    public JAXBElement<String> createChangeSalesRepResponseCRepName(String value) {
        return new JAXBElement<String>(_ChangeSalesRepResponseCRepName_QNAME, String.class, ChangeSalesRepResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "errMessage", scope = PhantomComponents.class)
    public JAXBElement<String> createPhantomComponentsErrMessage(String value) {
        return new JAXBElement<String>(_PhantomComponentsResponseErrMessage_QNAME, String.class, PhantomComponents.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "phPartNum", scope = PhantomComponents.class)
    public JAXBElement<String> createPhantomComponentsPhPartNum(String value) {
        return new JAXBElement<String>(_PhantomComponentsPhPartNum_QNAME, String.class, PhantomComponents.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserDefinedColumns }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice", name = "UserDefinedColumns", scope = IceRow.class)
    public JAXBElement<UserDefinedColumns> createIceRowUserDefinedColumns(UserDefinedColumns value) {
        return new JAXBElement<UserDefinedColumns>(_IceRowUserDefinedColumns_QNAME, UserDefinedColumns.class, IceRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice", name = "SpecifiedProperties", scope = IceRow.class)
    public JAXBElement<byte[]> createIceRowSpecifiedProperties(byte[] value) {
        return new JAXBElement<byte[]>(_IceRowSpecifiedProperties_QNAME, byte[].class, IceRow.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice", name = "RowMod", scope = IceRow.class)
    public JAXBElement<String> createIceRowRowMod(String value) {
        return new JAXBElement<String>(_IceRowRowMod_QNAME, String.class, IceRow.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetInventoryQuantitiesResponse.class)
    public JAXBElement<SalesOrderTableset> createGetInventoryQuantitiesResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetInventoryQuantitiesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeBTCustIDMasterResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeBTCustIDMasterResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeBTCustIDMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cCreditLimitMessage", scope = ChangeBTCustIDMasterResponse.class)
    public JAXBElement<String> createChangeBTCustIDMasterResponseCCreditLimitMessage(String value) {
        return new JAXBElement<String>(_CheckCustomerCreditLimitResponseCCreditLimitMessage_QNAME, String.class, ChangeBTCustIDMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "CreateOrderFromQuoteSaveOTSResult", scope = CreateOrderFromQuoteSaveOTSResponse.class)
    public JAXBElement<SalesOrderTableset> createCreateOrderFromQuoteSaveOTSResponseCreateOrderFromQuoteSaveOTSResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_CreateOrderFromQuoteSaveOTSResponseCreateOrderFromQuoteSaveOTSResult_QNAME, SalesOrderTableset.class, CreateOrderFromQuoteSaveOTSResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelMFCustID.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelMFCustIDDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelMFCustID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipMFCustID", scope = ChangeOrderRelMFCustID.class)
    public JAXBElement<String> createChangeOrderRelMFCustIDIpMFCustID(String value) {
        return new JAXBElement<String>(_ChangeOrderRelMFCustIDIpMFCustID_QNAME, String.class, ChangeOrderRelMFCustID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = UpdateResponse.class)
    public JAXBElement<SalesOrderTableset> createUpdateResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, UpdateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "opProjMsg", scope = OnChangeofPhaseIDResponse.class)
    public JAXBElement<String> createOnChangeofPhaseIDResponseOpProjMsg(String value) {
        return new JAXBElement<String>(_CheckProjectIDResponseOpProjMsg_QNAME, String.class, OnChangeofPhaseIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeofPhaseIDResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeofPhaseIDResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeofPhaseIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeTaxableAmtResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeTaxableAmtResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeTaxableAmtResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderHedAttchResponse.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderHedAttchResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderHedAttchResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cSalesRepCode", scope = ChangeSalesRep.class)
    public JAXBElement<String> createChangeSalesRepCSalesRepCode(String value) {
        return new JAXBElement<String>(_ChangeSalesRepCSalesRepCode_QNAME, String.class, ChangeSalesRep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeRevisionNumResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeRevisionNumResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeRevisionNumResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeRateCodeResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeRateCodeResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeRateCodeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeQuickEntryOptionResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeQuickEntryOptionResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeQuickEntryOptionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelShipToContactResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelShipToContactResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelShipToContactResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = RebuildShipUPS.class)
    public JAXBElement<SalesOrderTableset> createRebuildShipUPSDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, RebuildShipUPS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipShipToNum", scope = RebuildShipUPS.class)
    public JAXBElement<String> createRebuildShipUPSIpShipToNum(String value) {
        return new JAXBElement<String>(_RebuildShipUPSIpShipToNum_QNAME, String.class, RebuildShipUPS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = SubmitNewOrderResponse.class)
    public JAXBElement<SalesOrderTableset> createSubmitNewOrderResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, SubmitNewOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangePlantResponse.class)
    public JAXBElement<SalesOrderTableset> createChangePlantResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangePlantResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderHedListTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "OrderHedList", scope = OrderHedListTableset.class)
    public JAXBElement<OrderHedListTable> createOrderHedListTablesetOrderHedList(OrderHedListTable value) {
        return new JAXBElement<OrderHedListTable>(_OrderHedListTablesetOrderHedList_QNAME, OrderHedListTable.class, OrderHedListTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveOTSParamsTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "SaveOTSParams", scope = SaveOTSParamsTableset.class)
    public JAXBElement<SaveOTSParamsTable> createSaveOTSParamsTablesetSaveOTSParams(SaveOTSParamsTable value) {
        return new JAXBElement<SaveOTSParamsTable>(_SaveOTSParamsTablesetSaveOTSParams_QNAME, SaveOTSParamsTable.class, SaveOTSParamsTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckSellingQuantityChange.class)
    public JAXBElement<SalesOrderTableset> createCheckSellingQuantityChangeDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckSellingQuantityChange.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GlbSugPOChgDeleteResult", scope = GlbSugPOChgDeleteResponse.class)
    public JAXBElement<GlbSugPOChgTableset> createGlbSugPOChgDeleteResponseGlbSugPOChgDeleteResult(GlbSugPOChgTableset value) {
        return new JAXBElement<GlbSugPOChgTableset>(_GlbSugPOChgDeleteResponseGlbSugPOChgDeleteResult_QNAME, GlbSugPOChgTableset.class, GlbSugPOChgDeleteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeCustomer.class)
    public JAXBElement<SalesOrderTableset> createChangeCustomerDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeOfMktgCampaign.class)
    public JAXBElement<SalesOrderTableset> createOnChangeOfMktgCampaignDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeOfMktgCampaign.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "MktgCampaignID", scope = OnChangeOfMktgCampaign.class)
    public JAXBElement<String> createOnChangeOfMktgCampaignMktgCampaignID(String value) {
        return new JAXBElement<String>(_OnChangeOfMktgEvntMktgCampaignID_QNAME, String.class, OnChangeOfMktgCampaign.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeQuoteQtyNumResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeQuoteQtyNumResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeQuoteQtyNumResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ProcessQuickEntryResponse.class)
    public JAXBElement<SalesOrderTableset> createProcessQuickEntryResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ProcessQuickEntryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdExtSalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = UpdateExt.class)
    public JAXBElement<UpdExtSalesOrderTableset> createUpdateExtDs(UpdExtSalesOrderTableset value) {
        return new JAXBElement<UpdExtSalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, UpdExtSalesOrderTableset.class, UpdateExt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = SubmitNewOrder.class)
    public JAXBElement<SalesOrderTableset> createSubmitNewOrderDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, SubmitNewOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "iPartNum", scope = GetKitComponents.class)
    public JAXBElement<String> createGetKitComponentsIPartNum(String value) {
        return new JAXBElement<String>(_HasMultipleSubsIPartNum_QNAME, String.class, GetKitComponents.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "errorMsg", scope = GetKitComponents.class)
    public JAXBElement<String> createGetKitComponentsErrorMsg(String value) {
        return new JAXBElement<String>(_GetKitComponentsResponseErrorMsg_QNAME, String.class, GetKitComponents.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetKitComponents.class)
    public JAXBElement<SalesOrderTableset> createGetKitComponentsDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetKitComponents.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "iRevisionNum", scope = GetKitComponents.class)
    public JAXBElement<String> createGetKitComponentsIRevisionNum(String value) {
        return new JAXBElement<String>(_CheckKitRevisionIRevisionNum_QNAME, String.class, GetKitComponents.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeOfMktgEvntResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeOfMktgEvntResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeOfMktgEvntResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ValidateSNsResponse.class)
    public JAXBElement<SalesOrderTableset> createValidateSNsResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ValidateSNsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOverrideDiscPriceList.class)
    public JAXBElement<SalesOrderTableset> createChangeOverrideDiscPriceListDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOverrideDiscPriceList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseSNFormat", scope = GetRowsCustomerTracker.class)
    public JAXBElement<String> createGetRowsCustomerTrackerWhereClauseSNFormat(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseSNFormat_QNAME, String.class, GetRowsCustomerTracker.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseSelectedSerialNumbers", scope = GetRowsCustomerTracker.class)
    public JAXBElement<String> createGetRowsCustomerTrackerWhereClauseSelectedSerialNumbers(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseSelectedSerialNumbers_QNAME, String.class, GetRowsCustomerTracker.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseSerialNumberSearch", scope = GetRowsCustomerTracker.class)
    public JAXBElement<String> createGetRowsCustomerTrackerWhereClauseSerialNumberSearch(String value) {
        return new JAXBElement<String>(_GetRowsCustomerTrackerWhereClauseSerialNumberSearch_QNAME, String.class, GetRowsCustomerTracker.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderRepComm", scope = GetRowsCustomerTracker.class)
    public JAXBElement<String> createGetRowsCustomerTrackerWhereClauseOrderRepComm(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderRepComm_QNAME, String.class, GetRowsCustomerTracker.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOHOrderMsc", scope = GetRowsCustomerTracker.class)
    public JAXBElement<String> createGetRowsCustomerTrackerWhereClauseOHOrderMsc(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOHOrderMsc_QNAME, String.class, GetRowsCustomerTracker.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderHed", scope = GetRowsCustomerTracker.class)
    public JAXBElement<String> createGetRowsCustomerTrackerWhereClauseOrderHed(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderHed_QNAME, String.class, GetRowsCustomerTracker.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderDtlAttch", scope = GetRowsCustomerTracker.class)
    public JAXBElement<String> createGetRowsCustomerTrackerWhereClauseOrderDtlAttch(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderDtlAttch_QNAME, String.class, GetRowsCustomerTracker.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderRel", scope = GetRowsCustomerTracker.class)
    public JAXBElement<String> createGetRowsCustomerTrackerWhereClauseOrderRel(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderRel_QNAME, String.class, GetRowsCustomerTracker.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderDtl", scope = GetRowsCustomerTracker.class)
    public JAXBElement<String> createGetRowsCustomerTrackerWhereClauseOrderDtl(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderDtl_QNAME, String.class, GetRowsCustomerTracker.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderHedAttch", scope = GetRowsCustomerTracker.class)
    public JAXBElement<String> createGetRowsCustomerTrackerWhereClauseOrderHedAttch(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderHedAttch_QNAME, String.class, GetRowsCustomerTracker.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "whereClauseOrderMsc", scope = GetRowsCustomerTracker.class)
    public JAXBElement<String> createGetRowsCustomerTrackerWhereClauseOrderMsc(String value) {
        return new JAXBElement<String>(_GetRowsWhereClauseOrderMsc_QNAME, String.class, GetRowsCustomerTracker.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", name = "m_exceptionQualifiedName", scope = EpicorFaultDetail.class)
    public JAXBElement<String> createEpicorFaultDetailMExceptionQualifiedName(String value) {
        return new JAXBElement<String>(_EpicorFaultDetailMExceptionQualifiedName_QNAME, String.class, EpicorFaultDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", name = "ExceptionKindValue", scope = EpicorFaultDetail.class)
    public JAXBElement<String> createEpicorFaultDetailExceptionKindValue(String value) {
        return new JAXBElement<String>(_EpicorFaultDetailExceptionKindValue_QNAME, String.class, EpicorFaultDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", name = "Message", scope = EpicorFaultDetail.class)
    public JAXBElement<String> createEpicorFaultDetailMessage(String value) {
        return new JAXBElement<String>(_EpicorFaultDetailMessage_QNAME, String.class, EpicorFaultDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", name = "m_exKindQualifiedName", scope = EpicorFaultDetail.class)
    public JAXBElement<String> createEpicorFaultDetailMExKindQualifiedName(String value) {
        return new JAXBElement<String>(_EpicorFaultDetailMExKindQualifiedName_QNAME, String.class, EpicorFaultDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEpicorExceptionData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Ice.Common", name = "DataList", scope = EpicorFaultDetail.class)
    public JAXBElement<ArrayOfEpicorExceptionData> createEpicorFaultDetailDataList(ArrayOfEpicorExceptionData value) {
        return new JAXBElement<ArrayOfEpicorExceptionData>(_EpicorFaultDetailDataList_QNAME, ArrayOfEpicorExceptionData.class, EpicorFaultDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ReopenOrderResult", scope = ReopenOrderResponse.class)
    public JAXBElement<SalesOrderTableset> createReopenOrderResponseReopenOrderResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_ReopenOrderResponseReopenOrderResult_QNAME, SalesOrderTableset.class, ReopenOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cPackNum", scope = ProcessCounterSaleResponse.class)
    public JAXBElement<String> createProcessCounterSaleResponseCPackNum(String value) {
        return new JAXBElement<String>(_ProcessCounterSaleResponseCPackNum_QNAME, String.class, ProcessCounterSaleResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ProcessCounterSaleResponse.class)
    public JAXBElement<SalesOrderTableset> createProcessCounterSaleResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ProcessCounterSaleResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "opMessage", scope = ProcessCounterSaleResponse.class)
    public JAXBElement<String> createProcessCounterSaleResponseOpMessage(String value) {
        return new JAXBElement<String>(_CheckSONumResponseOpMessage_QNAME, String.class, ProcessCounterSaleResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewSalesKit.class)
    public JAXBElement<SalesOrderTableset> createGetNewSalesKitDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewSalesKit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cPartNum", scope = CheckPartRevisionChange.class)
    public JAXBElement<String> createCheckPartRevisionChangeCPartNum(String value) {
        return new JAXBElement<String>(_CheckPartRevisionChangeResponseCPartNum_QNAME, String.class, CheckPartRevisionChange.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cFieldName", scope = CheckPartRevisionChange.class)
    public JAXBElement<String> createCheckPartRevisionChangeCFieldName(String value) {
        return new JAXBElement<String>(_CheckPartRevisionChangeCFieldName_QNAME, String.class, CheckPartRevisionChange.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeOrderRelUseOTMF.class)
    public JAXBElement<SalesOrderTableset> createChangeOrderRelUseOTMFDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeOrderRelUseOTMF.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderHed.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderHedDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderHed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cARLOCID", scope = CheckLtrOfCrdt.class)
    public JAXBElement<String> createCheckLtrOfCrdtCARLOCID(String value) {
        return new JAXBElement<String>(_ChkLtrOfCrdtCARLOCID_QNAME, String.class, CheckLtrOfCrdt.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeDiscountPercent.class)
    public JAXBElement<SalesOrderTableset> createChangeDiscountPercentDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeDiscountPercent.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeRenewalNbrMasterResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeRenewalNbrMasterResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeRenewalNbrMasterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "RemoveICPOLinkResult", scope = RemoveICPOLinkResponse.class)
    public JAXBElement<SalesOrderTableset> createRemoveICPOLinkResponseRemoveICPOLinkResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_RemoveICPOLinkResponseRemoveICPOLinkResult_QNAME, SalesOrderTableset.class, RemoveICPOLinkResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "tableName", scope = GetCodeDescList.class)
    public JAXBElement<String> createGetCodeDescListTableName(String value) {
        return new JAXBElement<String>(_ChangeMiscAmountTableName_QNAME, String.class, GetCodeDescList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "fieldName", scope = GetCodeDescList.class)
    public JAXBElement<String> createGetCodeDescListFieldName(String value) {
        return new JAXBElement<String>(_GetCodeDescListFieldName_QNAME, String.class, GetCodeDescList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetBySysRowIDResult", scope = GetBySysRowIDResponse.class)
    public JAXBElement<SalesOrderTableset> createGetBySysRowIDResponseGetBySysRowIDResult(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_GetBySysRowIDResponseGetBySysRowIDResult_QNAME, SalesOrderTableset.class, GetBySysRowIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeHedUseOTS.class)
    public JAXBElement<SalesOrderTableset> createChangeHedUseOTSDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeHedUseOTS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderHedUPSResponse.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderHedUPSResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderHedUPSResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderRelResponse.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderRelResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderRelResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlbSugPOChgTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CreateGlbSugPOChgResponse.class)
    public JAXBElement<GlbSugPOChgTableset> createCreateGlbSugPOChgResponseDs(GlbSugPOChgTableset value) {
        return new JAXBElement<GlbSugPOChgTableset>(_SetUPSQVEnableResponseDs_QNAME, GlbSugPOChgTableset.class, CreateGlbSugPOChgResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeUnitPrice.class)
    public JAXBElement<SalesOrderTableset> createChangeUnitPriceDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeUnitPrice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOHOrderMsc.class)
    public JAXBElement<SalesOrderTableset> createGetNewOHOrderMscDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOHOrderMsc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cICPOLinkMessage", scope = CheckOrderLinkToInterCompanyPOResponse.class)
    public JAXBElement<String> createCheckOrderLinkToInterCompanyPOResponseCICPOLinkMessage(String value) {
        return new JAXBElement<String>(_CheckOrderLinkToInterCompanyPOResponseCICPOLinkMessage_QNAME, String.class, CheckOrderLinkToInterCompanyPOResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeARLOCIDResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeARLOCIDResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeARLOCIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "opMsg", scope = OnChangeARLOCIDResponse.class)
    public JAXBElement<String> createOnChangeARLOCIDResponseOpMsg(String value) {
        return new JAXBElement<String>(_OnChangeARLOCIDResponseOpMsg_QNAME, String.class, OnChangeARLOCIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "opOverwriteValue", scope = OnChangeARLOCIDResponse.class)
    public JAXBElement<String> createOnChangeARLOCIDResponseOpOverwriteValue(String value) {
        return new JAXBElement<String>(_OnChangeARLOCIDResponseOpOverwriteValue_QNAME, String.class, OnChangeARLOCIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "opFieldName", scope = OnChangeARLOCIDResponse.class)
    public JAXBElement<String> createOnChangeARLOCIDResponseOpFieldName(String value) {
        return new JAXBElement<String>(_OnChangeARLOCIDResponseOpFieldName_QNAME, String.class, OnChangeARLOCIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeOfTaxAmtResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeOfTaxAmtResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeOfTaxAmtResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "GetCodeDescListResult", scope = GetCodeDescListResponse.class)
    public JAXBElement<String> createGetCodeDescListResponseGetCodeDescListResult(String value) {
        return new JAXBElement<String>(_GetCodeDescListResponseGetCodeDescListResult_QNAME, String.class, GetCodeDescListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeCreditCardOrderResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeCreditCardOrderResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeCreditCardOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OrderDtlGetNewCounterSale.class)
    public JAXBElement<SalesOrderTableset> createOrderDtlGetNewCounterSaleDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OrderDtlGetNewCounterSale.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = UpdateExistingOrder.class)
    public JAXBElement<SalesOrderTableset> createUpdateExistingOrderDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, UpdateExistingOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CheckCustomerCreditReleaseResponse.class)
    public JAXBElement<SalesOrderTableset> createCheckCustomerCreditReleaseResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CheckCustomerCreditReleaseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cCreditLimitMessage", scope = CheckCustomerCreditReleaseResponse.class)
    public JAXBElement<String> createCheckCustomerCreditReleaseResponseCCreditLimitMessage(String value) {
        return new JAXBElement<String>(_CheckCustomerCreditLimitResponseCCreditLimitMessage_QNAME, String.class, CheckCustomerCreditReleaseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcDimCode", scope = ChangeWhseCodeMaster.class)
    public JAXBElement<String> createChangeWhseCodeMasterPcDimCode(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterPcDimCode_QNAME, String.class, ChangeWhseCodeMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcPartNum", scope = ChangeWhseCodeMaster.class)
    public JAXBElement<String> createChangeWhseCodeMasterPcPartNum(String value) {
        return new JAXBElement<String>(_GetBreakListCodeDescPcPartNum_QNAME, String.class, ChangeWhseCodeMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeWhseCodeMaster.class)
    public JAXBElement<SalesOrderTableset> createChangeWhseCodeMasterDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeWhseCodeMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcWhseCode", scope = ChangeWhseCodeMaster.class)
    public JAXBElement<String> createChangeWhseCodeMasterPcWhseCode(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterPcWhseCode_QNAME, String.class, ChangeWhseCodeMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcLotNum", scope = ChangeWhseCodeMaster.class)
    public JAXBElement<String> createChangeWhseCodeMasterPcLotNum(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterPcLotNum_QNAME, String.class, ChangeWhseCodeMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "pcBinNum", scope = ChangeWhseCodeMaster.class)
    public JAXBElement<String> createChangeWhseCodeMasterPcBinNum(String value) {
        return new JAXBElement<String>(_ChangeSellingQtyMasterPcBinNum_QNAME, String.class, ChangeWhseCodeMaster.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeMiscPercent.class)
    public JAXBElement<SalesOrderTableset> createChangeMiscPercentDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeMiscPercent.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "tableName", scope = ChangeMiscPercent.class)
    public JAXBElement<String> createChangeMiscPercentTableName(String value) {
        return new JAXBElement<String>(_ChangeMiscAmountTableName_QNAME, String.class, ChangeMiscPercent.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "cQuoteLineWOQtyMsgText", scope = CheckQuoteLinesNoQuantityResponse.class)
    public JAXBElement<String> createCheckQuoteLinesNoQuantityResponseCQuoteLineWOQtyMsgText(String value) {
        return new JAXBElement<String>(_CheckQuoteLinesNoQuantityResponseCQuoteLineWOQtyMsgText_QNAME, String.class, CheckQuoteLinesNoQuantityResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ErrorMsg", scope = ETCValidateAddressResponse.class)
    public JAXBElement<String> createETCValidateAddressResponseErrorMsg(String value) {
        return new JAXBElement<String>(_ETCValidateAddressResponseErrorMsg_QNAME, String.class, ETCValidateAddressResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ETCAddrValidationTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ETCValidateAddressResult", scope = ETCValidateAddressResponse.class)
    public JAXBElement<ETCAddrValidationTableset> createETCValidateAddressResponseETCValidateAddressResult(ETCAddrValidationTableset value) {
        return new JAXBElement<ETCAddrValidationTableset>(_ETCValidateAddressResponseETCValidateAddressResult_QNAME, ETCAddrValidationTableset.class, ETCValidateAddressResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ETCAddressTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ETCAddress", scope = ETCAddrValidationTableset.class)
    public JAXBElement<ETCAddressTable> createETCAddrValidationTablesetETCAddress(ETCAddressTable value) {
        return new JAXBElement<ETCAddressTable>(_ETCAddrValidationTablesetETCAddress_QNAME, ETCAddressTable.class, ETCAddrValidationTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ETCMessageTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", name = "ETCMessage", scope = ETCAddrValidationTableset.class)
    public JAXBElement<ETCMessageTable> createETCAddrValidationTablesetETCMessage(ETCMessageTable value) {
        return new JAXBElement<ETCMessageTable>(_ETCAddrValidationTablesetETCMessage_QNAME, ETCMessageTable.class, ETCAddrValidationTableset.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeofTaxRgnResponse.class)
    public JAXBElement<SalesOrderTableset> createOnChangeofTaxRgnResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeofTaxRgnResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeShipToContact.class)
    public JAXBElement<SalesOrderTableset> createChangeShipToContactDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeShipToContact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = OnChangeofPhaseID.class)
    public JAXBElement<SalesOrderTableset> createOnChangeofPhaseIDDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, OnChangeofPhaseID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ipPhaseID", scope = OnChangeofPhaseID.class)
    public JAXBElement<String> createOnChangeofPhaseIDIpPhaseID(String value) {
        return new JAXBElement<String>(_OnChangeofPhaseIDIpPhaseID_QNAME, String.class, OnChangeofPhaseID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeShipToID.class)
    public JAXBElement<SalesOrderTableset> createChangeShipToIDDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeShipToID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = ChangeShipToContactResponse.class)
    public JAXBElement<SalesOrderTableset> createChangeShipToContactResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, ChangeShipToContactResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = GetNewOrderDtlAttchResponse.class)
    public JAXBElement<SalesOrderTableset> createGetNewOrderDtlAttchResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, GetNewOrderDtlAttchResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "ds", scope = CCProcessCardResponse.class)
    public JAXBElement<SalesOrderTableset> createCCProcessCardResponseDs(SalesOrderTableset value) {
        return new JAXBElement<SalesOrderTableset>(_SetUPSQVEnableResponseDs_QNAME, SalesOrderTableset.class, CCProcessCardResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "Erp:BO:SalesOrder", name = "opMessage", scope = CCProcessCardResponse.class)
    public JAXBElement<String> createCCProcessCardResponseOpMessage(String value) {
        return new JAXBElement<String>(_CheckSONumResponseOpMessage_QNAME, String.class, CCProcessCardResponse.class, value);
    }

}
