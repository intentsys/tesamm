
package cpm.tesamm.SalesOrder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SOEntryUIParamsRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SOEntryUIParamsRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceRow">
 *       &lt;sequence>
 *         &lt;element name="CCAllowSales" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CCEnableAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CCEnableCSC" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CalcQtysPreference" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EnableICPOSugTool" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Localization" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrjAllowWBSPhase" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SOReadyToCalcDflt" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SuppressSOMakeDirWrn" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="UseTerritorySecurity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SOEntryUIParamsRow", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "ccAllowSales",
    "ccEnableAddress",
    "ccEnableCSC",
    "calcQtysPreference",
    "enableICPOSugTool",
    "localization",
    "prjAllowWBSPhase",
    "soReadyToCalcDflt",
    "suppressSOMakeDirWrn",
    "useTerritorySecurity"
})
public class SOEntryUIParamsRow
    extends IceRow
{

    @XmlElement(name = "CCAllowSales")
    protected Boolean ccAllowSales;
    @XmlElement(name = "CCEnableAddress")
    protected Boolean ccEnableAddress;
    @XmlElement(name = "CCEnableCSC")
    protected Boolean ccEnableCSC;
    @XmlElement(name = "CalcQtysPreference")
    protected Boolean calcQtysPreference;
    @XmlElement(name = "EnableICPOSugTool")
    protected Boolean enableICPOSugTool;
    @XmlElementRef(name = "Localization", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> localization;
    @XmlElement(name = "PrjAllowWBSPhase")
    protected Boolean prjAllowWBSPhase;
    @XmlElement(name = "SOReadyToCalcDflt")
    protected Boolean soReadyToCalcDflt;
    @XmlElement(name = "SuppressSOMakeDirWrn")
    protected Boolean suppressSOMakeDirWrn;
    @XmlElement(name = "UseTerritorySecurity")
    protected Boolean useTerritorySecurity;

    /**
     * Gets the value of the ccAllowSales property.
     * This getter has been renamed from isCCAllowSales() to getCCAllowSales() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCCAllowSales() {
        return ccAllowSales;
    }

    /**
     * Sets the value of the ccAllowSales property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCCAllowSales(Boolean value) {
        this.ccAllowSales = value;
    }

    /**
     * Gets the value of the ccEnableAddress property.
     * This getter has been renamed from isCCEnableAddress() to getCCEnableAddress() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCCEnableAddress() {
        return ccEnableAddress;
    }

    /**
     * Sets the value of the ccEnableAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCCEnableAddress(Boolean value) {
        this.ccEnableAddress = value;
    }

    /**
     * Gets the value of the ccEnableCSC property.
     * This getter has been renamed from isCCEnableCSC() to getCCEnableCSC() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCCEnableCSC() {
        return ccEnableCSC;
    }

    /**
     * Sets the value of the ccEnableCSC property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCCEnableCSC(Boolean value) {
        this.ccEnableCSC = value;
    }

    /**
     * Gets the value of the calcQtysPreference property.
     * This getter has been renamed from isCalcQtysPreference() to getCalcQtysPreference() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCalcQtysPreference() {
        return calcQtysPreference;
    }

    /**
     * Sets the value of the calcQtysPreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCalcQtysPreference(Boolean value) {
        this.calcQtysPreference = value;
    }

    /**
     * Gets the value of the enableICPOSugTool property.
     * This getter has been renamed from isEnableICPOSugTool() to getEnableICPOSugTool() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnableICPOSugTool() {
        return enableICPOSugTool;
    }

    /**
     * Sets the value of the enableICPOSugTool property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableICPOSugTool(Boolean value) {
        this.enableICPOSugTool = value;
    }

    /**
     * Gets the value of the localization property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocalization() {
        return localization;
    }

    /**
     * Sets the value of the localization property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocalization(JAXBElement<String> value) {
        this.localization = value;
    }

    /**
     * Gets the value of the prjAllowWBSPhase property.
     * This getter has been renamed from isPrjAllowWBSPhase() to getPrjAllowWBSPhase() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPrjAllowWBSPhase() {
        return prjAllowWBSPhase;
    }

    /**
     * Sets the value of the prjAllowWBSPhase property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrjAllowWBSPhase(Boolean value) {
        this.prjAllowWBSPhase = value;
    }

    /**
     * Gets the value of the soReadyToCalcDflt property.
     * This getter has been renamed from isSOReadyToCalcDflt() to getSOReadyToCalcDflt() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSOReadyToCalcDflt() {
        return soReadyToCalcDflt;
    }

    /**
     * Sets the value of the soReadyToCalcDflt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSOReadyToCalcDflt(Boolean value) {
        this.soReadyToCalcDflt = value;
    }

    /**
     * Gets the value of the suppressSOMakeDirWrn property.
     * This getter has been renamed from isSuppressSOMakeDirWrn() to getSuppressSOMakeDirWrn() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSuppressSOMakeDirWrn() {
        return suppressSOMakeDirWrn;
    }

    /**
     * Sets the value of the suppressSOMakeDirWrn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSuppressSOMakeDirWrn(Boolean value) {
        this.suppressSOMakeDirWrn = value;
    }

    /**
     * Gets the value of the useTerritorySecurity property.
     * This getter has been renamed from isUseTerritorySecurity() to getUseTerritorySecurity() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getUseTerritorySecurity() {
        return useTerritorySecurity;
    }

    /**
     * Sets the value of the useTerritorySecurity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseTerritorySecurity(Boolean value) {
        this.useTerritorySecurity = value;
    }

}
