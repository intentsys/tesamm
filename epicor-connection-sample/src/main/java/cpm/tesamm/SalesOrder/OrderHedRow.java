
package cpm.tesamm.SalesOrder;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OrderHedRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderHedRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceRow">
 *       &lt;sequence>
 *         &lt;element name="ARLOCID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AVSAddr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AVSZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AckEmailSent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AddlHdlgFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AllocPriorityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyChrg" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ApplyOrderBasedDisc" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AutoOrderBasedDisc" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AutoPrintReady" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AvailBTCustList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BTAddressList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BTConNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BTContactEMailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BTContactFaxNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BTContactName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BTContactPhoneNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BTCustID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BTCustNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BTCustNumBTName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BTCustNumCustID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BTCustNumName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BaseCurrSymbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BaseCurrencyID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillToCustomerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BitFlag" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CCAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CCApprovalNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCCSCID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCCSCIDToken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCDocAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CCDocFreight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CCDocTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CCDocTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CCFreight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CCResponse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCRounding" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CCStreetAddr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CCTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CCTranID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCTranType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="COD" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CODAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CODCheck" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CODFreight" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CSCResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CanChangeTaxLiab" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CancelAfterDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CardID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardMemberName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardStore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardmemberReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CertOfOrigin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ChangeTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ChangedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChrgAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CommercialInvoice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CounterSale" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CreateInvoice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CreatePackingSlip" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CreditCardOrder" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CreditOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CreditOverrideDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CreditOverrideLimit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CreditOverrideTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditOverrideUserID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCodeCurrDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCodeCurrName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCodeCurrSymbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCodeCurrencyID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCodeDocumentDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencySwitch" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CustAllowOTS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CustNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CustOnCreditHold" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CustTradePartnerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerAllowShipTo3" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CustomerBTName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerCustID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerPrintAck" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CustomerRequiresPO" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DeclaredAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DeclaredIns" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DeliveryConf" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DeliveryType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DemandContract" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DemandContractNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DemandHeadSeq" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DemandProcessDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DemandProcessTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DemandRejected" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DepositBal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DiscountPercent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DispatchReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DispatchReasonCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DoNotShipAfterDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DoNotShipBeforeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DocCCRounding" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocDepositBal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocInTotalCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocInTotalDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocInTotalMisc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DocOrderAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocRounding" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTotalAdvBill" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTotalCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTotalComm" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTotalDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTotalMisc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTotalNet" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTotalOrder" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTotalSATax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTotalTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTotalWHTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocumentTypeID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DropShip" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ECCOrderNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ECCPONum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ECCPaymentMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EDIAck" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EDIOrder" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EDIReady" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ERSOrder" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ERSOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EnableJobWizard" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EnableSoldToID" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EntryMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntryPerson" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntryProcess" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ExpirationMonth" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ExpirationYear" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ExtCompany" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FFAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FFAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FFAddress3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FFCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FFCompName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FFContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FFCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FFCountryNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FFID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FFPhoneNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FFState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FFZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FOB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FOBDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GroundType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HDCaseDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HDCaseNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="HasMiscCharges" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="HasOrderLines" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Hazmat" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="HoldSetByDemand" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ICPONum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InPrice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="InTotalCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InTotalDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InTotalMisc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="IndividualPackIDs" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IntrntlShip" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="InvCurrCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvCurrCurrDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvcOrderCmp" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="InvoiceComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsCSRSet" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LOCHold" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LastBatchNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastScheduleNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastTCtrlNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LegalNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LetterOfInstr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LinkMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Linked" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LockQty" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LockRate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="NeedByDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="NonStdPkg" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="NotifyEMail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NotifyFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OTSAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSAddress3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSCntryDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSCountryNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OTSCustSaved" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OTSFaxNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSPhoneNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSResaleID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSSaveAs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSSaveCustID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSSaved" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OTSShipToNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSTaxRegionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OTSZIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OpenOrder" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OrderAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OrderCSR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="OrderHeld" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OrderNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OrderStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OurBank" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OurBankBankName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OurBankDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OutboundSalesDocCtr" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OutboundShipDocsCtr" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OverrideCarrier" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OverrideService" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PONum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PSCurrCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PSCurrCurrDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ParentCustNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PayAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayBTAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayBTAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayBTAddress3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayBTCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayBTCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayBTCountryNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PayBTPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayBTState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayBTZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PickListComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Plant" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrcConNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ProFormaInvComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessCard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateGrpCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateGrpDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReadyToCalc" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="RefNotes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferencePNRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RepRate1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RepRate2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RepRate3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RepRate4" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RepRate5" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RepSplit1" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RepSplit2" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RepSplit3" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RepSplit4" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RepSplit5" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RequestDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ResDelivery" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ReservePriDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReservePriorityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Rounding" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1CCAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1CCFreight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1CCRounding" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1CCTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1CCTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1DepositBal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1InTotalCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1InTotalDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1InTotalMisc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1OrderAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1Rounding" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TotalAdvBill" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TotalCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TotalComm" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TotalDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TotalMisc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TotalNet" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TotalSATax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TotalTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1TotalWHTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2CCAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2CCFreight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2CCRounding" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2CCTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2CCTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2DepositBal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2InTotalCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2InTotalDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2InTotalMisc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2OrderAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2Rounding" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TotalAdvBill" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TotalCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TotalComm" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TotalDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TotalMisc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TotalNet" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TotalSATax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TotalTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2TotalWHTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3CCAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3CCFreight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3CCRounding" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3CCTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3CCTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3DepositBal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3InTotalCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3InTotalDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3InTotalMisc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3OrderAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3Rounding" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TotalAdvBill" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TotalCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TotalComm" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TotalDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TotalMisc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TotalNet" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TotalSATax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TotalTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3TotalWHTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SRCommAmt1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SRCommAmt2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SRCommAmt3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SRCommAmt4" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SRCommAmt5" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SRCommableAmt1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SRCommableAmt2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SRCommableAmt3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SRCommableAmt4" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SRCommableAmt5" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SalesRepCode1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepCode2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepCode3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepCode4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepCode5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepName1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepName2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepName3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepName4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SalesRepName5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SatDelivery" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SatPickup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ServAlert" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ServAuthNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServDeliveryDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ServHomeDel" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ServInstruct" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServRef1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServRef2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServRef3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServRef4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServRef5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServRelease" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ServSignature" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ShipComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipExprtDeclartn" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ShipOrderComplete" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ShipToAddressList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToContactEMailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToContactFaxNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToContactName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToContactPhoneNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToCustId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToCustNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShipToNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipViaCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipViaCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipViaCodeWebDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShowApplyOrderDiscountsControl" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ShpConNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SndAlrtShp" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SoldToAddressList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SoldToContactEMailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SoldToContactFaxNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SoldToContactName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SoldToContactPhoneNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SysRevID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="TaxPoint" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TaxRateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TaxRegionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxRegionCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TermsCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TermsCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalAdvBill" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalCharges" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalComm" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalCommLines" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TotalDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalInvoiced" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalLines" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TotalMisc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalNet" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalOrder" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalRelDates" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TotalReleases" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TotalSATax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalShipped" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalWHTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TranDocTypeID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UPSQVMemo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UPSQVShipFromName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UPSQuantumView" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="UpdateDtlAndRelRecords" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="UseOTS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="UserChar1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserChar2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserChar3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserChar4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserDate1" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="UserDate2" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="UserDate3" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="UserDate4" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="UserDecimal1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="UserDecimal2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="UserInteger1" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="UserInteger2" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="VoidOrder" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="WIApplication" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WICreditCardorder" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="WIOrder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WIUserID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WIUsername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WebEntryPerson" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WebOrder" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="XRefContractDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="XRefContractNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dspBTCustID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderHedRow", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "arlocid",
    "avsAddr",
    "avsZip",
    "ackEmailSent",
    "addlHdlgFlag",
    "allocPriorityCode",
    "applyChrg",
    "applyOrderBasedDisc",
    "autoOrderBasedDisc",
    "autoPrintReady",
    "availBTCustList",
    "btAddressList",
    "btConNum",
    "btContactEMailAddress",
    "btContactFaxNum",
    "btContactName",
    "btContactPhoneNum",
    "btCustID",
    "btCustNum",
    "btCustNumBTName",
    "btCustNumCustID",
    "btCustNumName",
    "baseCurrSymbol",
    "baseCurrencyID",
    "billToCustomerName",
    "bitFlag",
    "ccAmount",
    "ccApprovalNum",
    "cccscid",
    "cccscidToken",
    "ccDocAmount",
    "ccDocFreight",
    "ccDocTax",
    "ccDocTotal",
    "ccFreight",
    "ccResponse",
    "ccRounding",
    "ccStreetAddr",
    "ccTax",
    "ccTotal",
    "ccTranID",
    "ccTranType",
    "ccZip",
    "cod",
    "codAmount",
    "codCheck",
    "codFreight",
    "cscResult",
    "canChangeTaxLiab",
    "cancelAfterDate",
    "cardID",
    "cardMemberName",
    "cardNumber",
    "cardStore",
    "cardType",
    "cardTypeDescription",
    "cardmemberReference",
    "certOfOrigin",
    "changeDate",
    "changeTime",
    "changedBy",
    "chrgAmount",
    "commercialInvoice",
    "company",
    "counterSale",
    "createInvoice",
    "createPackingSlip",
    "creditCardOrder",
    "creditOverride",
    "creditOverrideDate",
    "creditOverrideLimit",
    "creditOverrideTime",
    "creditOverrideUserID",
    "currencyCode",
    "currencyCodeCurrDesc",
    "currencyCodeCurrName",
    "currencyCodeCurrSymbol",
    "currencyCodeCurrencyID",
    "currencyCodeDocumentDesc",
    "currencySwitch",
    "custAllowOTS",
    "custNum",
    "custOnCreditHold",
    "custTradePartnerName",
    "customerAllowShipTo3",
    "customerBTName",
    "customerCustID",
    "customerName",
    "customerPrintAck",
    "customerRequiresPO",
    "declaredAmt",
    "declaredIns",
    "deliveryConf",
    "deliveryType",
    "demandContract",
    "demandContractNum",
    "demandHeadSeq",
    "demandProcessDate",
    "demandProcessTime",
    "demandRejected",
    "depositBal",
    "discountPercent",
    "dispatchReason",
    "dispatchReasonCodeDescription",
    "doNotShipAfterDate",
    "doNotShipBeforeDate",
    "docCCRounding",
    "docDepositBal",
    "docInTotalCharges",
    "docInTotalDiscount",
    "docInTotalMisc",
    "docOnly",
    "docOrderAmt",
    "docRounding",
    "docTotalAdvBill",
    "docTotalCharges",
    "docTotalComm",
    "docTotalDiscount",
    "docTotalMisc",
    "docTotalNet",
    "docTotalOrder",
    "docTotalSATax",
    "docTotalTax",
    "docTotalWHTax",
    "documentTypeID",
    "dropShip",
    "eccOrderNum",
    "eccpoNum",
    "eccPaymentMethod",
    "ediAck",
    "ediOrder",
    "ediReady",
    "ersOrder",
    "ersOverride",
    "enableJobWizard",
    "enableSoldToID",
    "entryMethod",
    "entryPerson",
    "entryProcess",
    "exchangeRate",
    "expirationMonth",
    "expirationYear",
    "extCompany",
    "ffAddress1",
    "ffAddress2",
    "ffAddress3",
    "ffCity",
    "ffCompName",
    "ffContact",
    "ffCountry",
    "ffCountryNum",
    "ffid",
    "ffPhoneNum",
    "ffState",
    "ffZip",
    "fob",
    "fobDescription",
    "groundType",
    "hdCaseDescription",
    "hdCaseNum",
    "hasMiscCharges",
    "hasOrderLines",
    "hazmat",
    "holdSetByDemand",
    "icpoNum",
    "inPrice",
    "inTotalCharges",
    "inTotalDiscount",
    "inTotalMisc",
    "individualPackIDs",
    "intrntlShip",
    "invCurrCode",
    "invCurrCurrDesc",
    "invcOrderCmp",
    "invoiceComment",
    "isCSRSet",
    "locHold",
    "lastBatchNum",
    "lastScheduleNumber",
    "lastTCtrlNum",
    "legalNumber",
    "letterOfInstr",
    "linkMsg",
    "linked",
    "lockQty",
    "lockRate",
    "needByDate",
    "nonStdPkg",
    "notifyEMail",
    "notifyFlag",
    "otsAddress1",
    "otsAddress2",
    "otsAddress3",
    "otsCity",
    "otsCntryDescription",
    "otsContact",
    "otsCountryNum",
    "otsCustSaved",
    "otsFaxNum",
    "otsName",
    "otsPhoneNum",
    "otsResaleID",
    "otsSaveAs",
    "otsSaveCustID",
    "otsSaved",
    "otsShipToNum",
    "otsState",
    "otsTaxRegionCode",
    "otszip",
    "openOrder",
    "orderAmt",
    "orderCSR",
    "orderComment",
    "orderDate",
    "orderHeld",
    "orderNum",
    "orderStatus",
    "ourBank",
    "ourBankBankName",
    "ourBankDescription",
    "outboundSalesDocCtr",
    "outboundShipDocsCtr",
    "overrideCarrier",
    "overrideService",
    "poNum",
    "psCurrCode",
    "psCurrCurrDesc",
    "parentCustNum",
    "payAccount",
    "payBTAddress1",
    "payBTAddress2",
    "payBTAddress3",
    "payBTCity",
    "payBTCountry",
    "payBTCountryNum",
    "payBTPhone",
    "payBTState",
    "payBTZip",
    "payFlag",
    "pickListComment",
    "plant",
    "prcConNum",
    "proFormaInvComment",
    "processCard",
    "rateGrpCode",
    "rateGrpDescription",
    "readyToCalc",
    "refNotes",
    "referencePNRef",
    "repRate1",
    "repRate2",
    "repRate3",
    "repRate4",
    "repRate5",
    "repSplit1",
    "repSplit2",
    "repSplit3",
    "repSplit4",
    "repSplit5",
    "requestDate",
    "resDelivery",
    "reservePriDescription",
    "reservePriorityCode",
    "rounding",
    "rpt1CCAmount",
    "rpt1CCFreight",
    "rpt1CCRounding",
    "rpt1CCTax",
    "rpt1CCTotal",
    "rpt1DepositBal",
    "rpt1InTotalCharges",
    "rpt1InTotalDiscount",
    "rpt1InTotalMisc",
    "rpt1OrderAmt",
    "rpt1Rounding",
    "rpt1TotalAdvBill",
    "rpt1TotalCharges",
    "rpt1TotalComm",
    "rpt1TotalDiscount",
    "rpt1TotalMisc",
    "rpt1TotalNet",
    "rpt1TotalSATax",
    "rpt1TotalTax",
    "rpt1TotalWHTax",
    "rpt2CCAmount",
    "rpt2CCFreight",
    "rpt2CCRounding",
    "rpt2CCTax",
    "rpt2CCTotal",
    "rpt2DepositBal",
    "rpt2InTotalCharges",
    "rpt2InTotalDiscount",
    "rpt2InTotalMisc",
    "rpt2OrderAmt",
    "rpt2Rounding",
    "rpt2TotalAdvBill",
    "rpt2TotalCharges",
    "rpt2TotalComm",
    "rpt2TotalDiscount",
    "rpt2TotalMisc",
    "rpt2TotalNet",
    "rpt2TotalSATax",
    "rpt2TotalTax",
    "rpt2TotalWHTax",
    "rpt3CCAmount",
    "rpt3CCFreight",
    "rpt3CCRounding",
    "rpt3CCTax",
    "rpt3CCTotal",
    "rpt3DepositBal",
    "rpt3InTotalCharges",
    "rpt3InTotalDiscount",
    "rpt3InTotalMisc",
    "rpt3OrderAmt",
    "rpt3Rounding",
    "rpt3TotalAdvBill",
    "rpt3TotalCharges",
    "rpt3TotalComm",
    "rpt3TotalDiscount",
    "rpt3TotalMisc",
    "rpt3TotalNet",
    "rpt3TotalSATax",
    "rpt3TotalTax",
    "rpt3TotalWHTax",
    "srCommAmt1",
    "srCommAmt2",
    "srCommAmt3",
    "srCommAmt4",
    "srCommAmt5",
    "srCommableAmt1",
    "srCommableAmt2",
    "srCommableAmt3",
    "srCommableAmt4",
    "srCommableAmt5",
    "salesRepCode1",
    "salesRepCode2",
    "salesRepCode3",
    "salesRepCode4",
    "salesRepCode5",
    "salesRepList",
    "salesRepName1",
    "salesRepName2",
    "salesRepName3",
    "salesRepName4",
    "salesRepName5",
    "satDelivery",
    "satPickup",
    "servAlert",
    "servAuthNum",
    "servDeliveryDate",
    "servHomeDel",
    "servInstruct",
    "servPhone",
    "servRef1",
    "servRef2",
    "servRef3",
    "servRef4",
    "servRef5",
    "servRelease",
    "servSignature",
    "shipComment",
    "shipExprtDeclartn",
    "shipOrderComplete",
    "shipToAddressList",
    "shipToContactEMailAddress",
    "shipToContactFaxNum",
    "shipToContactName",
    "shipToContactPhoneNum",
    "shipToCustId",
    "shipToCustNum",
    "shipToNum",
    "shipViaCode",
    "shipViaCodeDescription",
    "shipViaCodeWebDesc",
    "showApplyOrderDiscountsControl",
    "shpConNum",
    "sndAlrtShp",
    "soldToAddressList",
    "soldToContactEMailAddress",
    "soldToContactFaxNum",
    "soldToContactName",
    "soldToContactPhoneNum",
    "sysRevID",
    "taxPoint",
    "taxRateDate",
    "taxRegionCode",
    "taxRegionCodeDescription",
    "termsCode",
    "termsCodeDescription",
    "totalAdvBill",
    "totalCharges",
    "totalComm",
    "totalCommLines",
    "totalDiscount",
    "totalInvoiced",
    "totalLines",
    "totalMisc",
    "totalNet",
    "totalOrder",
    "totalRelDates",
    "totalReleases",
    "totalSATax",
    "totalShipped",
    "totalTax",
    "totalWHTax",
    "tranDocTypeID",
    "upsqvMemo",
    "upsqvShipFromName",
    "upsQuantumView",
    "updateDtlAndRelRecords",
    "useOTS",
    "userChar1",
    "userChar2",
    "userChar3",
    "userChar4",
    "userDate1",
    "userDate2",
    "userDate3",
    "userDate4",
    "userDecimal1",
    "userDecimal2",
    "userInteger1",
    "userInteger2",
    "voidOrder",
    "wiApplication",
    "wiCreditCardorder",
    "wiOrder",
    "wiUserID",
    "wiUsername",
    "webEntryPerson",
    "webOrder",
    "xRefContractDate",
    "xRefContractNum",
    "dspBTCustID"
})
public class OrderHedRow
    extends IceRow
{

    @XmlElementRef(name = "ARLOCID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> arlocid;
    @XmlElementRef(name = "AVSAddr", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> avsAddr;
    @XmlElementRef(name = "AVSZip", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> avsZip;
    @XmlElement(name = "AckEmailSent")
    protected Boolean ackEmailSent;
    @XmlElement(name = "AddlHdlgFlag")
    protected Boolean addlHdlgFlag;
    @XmlElementRef(name = "AllocPriorityCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> allocPriorityCode;
    @XmlElement(name = "ApplyChrg")
    protected Boolean applyChrg;
    @XmlElement(name = "ApplyOrderBasedDisc")
    protected Boolean applyOrderBasedDisc;
    @XmlElement(name = "AutoOrderBasedDisc")
    protected Boolean autoOrderBasedDisc;
    @XmlElement(name = "AutoPrintReady")
    protected Boolean autoPrintReady;
    @XmlElementRef(name = "AvailBTCustList", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> availBTCustList;
    @XmlElementRef(name = "BTAddressList", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> btAddressList;
    @XmlElement(name = "BTConNum")
    protected Integer btConNum;
    @XmlElementRef(name = "BTContactEMailAddress", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> btContactEMailAddress;
    @XmlElementRef(name = "BTContactFaxNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> btContactFaxNum;
    @XmlElementRef(name = "BTContactName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> btContactName;
    @XmlElementRef(name = "BTContactPhoneNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> btContactPhoneNum;
    @XmlElementRef(name = "BTCustID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> btCustID;
    @XmlElement(name = "BTCustNum")
    protected Integer btCustNum;
    @XmlElementRef(name = "BTCustNumBTName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> btCustNumBTName;
    @XmlElementRef(name = "BTCustNumCustID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> btCustNumCustID;
    @XmlElementRef(name = "BTCustNumName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> btCustNumName;
    @XmlElementRef(name = "BaseCurrSymbol", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> baseCurrSymbol;
    @XmlElementRef(name = "BaseCurrencyID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> baseCurrencyID;
    @XmlElementRef(name = "BillToCustomerName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> billToCustomerName;
    @XmlElement(name = "BitFlag")
    protected Integer bitFlag;
    @XmlElement(name = "CCAmount")
    protected BigDecimal ccAmount;
    @XmlElementRef(name = "CCApprovalNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ccApprovalNum;
    @XmlElementRef(name = "CCCSCID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cccscid;
    @XmlElementRef(name = "CCCSCIDToken", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cccscidToken;
    @XmlElement(name = "CCDocAmount")
    protected BigDecimal ccDocAmount;
    @XmlElement(name = "CCDocFreight")
    protected BigDecimal ccDocFreight;
    @XmlElement(name = "CCDocTax")
    protected BigDecimal ccDocTax;
    @XmlElement(name = "CCDocTotal")
    protected BigDecimal ccDocTotal;
    @XmlElement(name = "CCFreight")
    protected BigDecimal ccFreight;
    @XmlElementRef(name = "CCResponse", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ccResponse;
    @XmlElement(name = "CCRounding")
    protected BigDecimal ccRounding;
    @XmlElementRef(name = "CCStreetAddr", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ccStreetAddr;
    @XmlElement(name = "CCTax")
    protected BigDecimal ccTax;
    @XmlElement(name = "CCTotal")
    protected BigDecimal ccTotal;
    @XmlElementRef(name = "CCTranID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ccTranID;
    @XmlElementRef(name = "CCTranType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ccTranType;
    @XmlElementRef(name = "CCZip", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ccZip;
    @XmlElement(name = "COD")
    protected Boolean cod;
    @XmlElement(name = "CODAmount")
    protected BigDecimal codAmount;
    @XmlElement(name = "CODCheck")
    protected Boolean codCheck;
    @XmlElement(name = "CODFreight")
    protected Boolean codFreight;
    @XmlElementRef(name = "CSCResult", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cscResult;
    @XmlElement(name = "CanChangeTaxLiab")
    protected Boolean canChangeTaxLiab;
    @XmlElementRef(name = "CancelAfterDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> cancelAfterDate;
    @XmlElementRef(name = "CardID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cardID;
    @XmlElementRef(name = "CardMemberName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cardMemberName;
    @XmlElementRef(name = "CardNumber", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cardNumber;
    @XmlElementRef(name = "CardStore", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cardStore;
    @XmlElementRef(name = "CardType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cardType;
    @XmlElementRef(name = "CardTypeDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cardTypeDescription;
    @XmlElementRef(name = "CardmemberReference", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cardmemberReference;
    @XmlElement(name = "CertOfOrigin")
    protected Boolean certOfOrigin;
    @XmlElementRef(name = "ChangeDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> changeDate;
    @XmlElement(name = "ChangeTime")
    protected Integer changeTime;
    @XmlElementRef(name = "ChangedBy", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changedBy;
    @XmlElement(name = "ChrgAmount")
    protected BigDecimal chrgAmount;
    @XmlElement(name = "CommercialInvoice")
    protected Boolean commercialInvoice;
    @XmlElementRef(name = "Company", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> company;
    @XmlElement(name = "CounterSale")
    protected Boolean counterSale;
    @XmlElement(name = "CreateInvoice")
    protected Boolean createInvoice;
    @XmlElement(name = "CreatePackingSlip")
    protected Boolean createPackingSlip;
    @XmlElement(name = "CreditCardOrder")
    protected Boolean creditCardOrder;
    @XmlElement(name = "CreditOverride")
    protected Boolean creditOverride;
    @XmlElementRef(name = "CreditOverrideDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> creditOverrideDate;
    @XmlElement(name = "CreditOverrideLimit")
    protected BigDecimal creditOverrideLimit;
    @XmlElementRef(name = "CreditOverrideTime", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditOverrideTime;
    @XmlElementRef(name = "CreditOverrideUserID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditOverrideUserID;
    @XmlElementRef(name = "CurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElementRef(name = "CurrencyCodeCurrDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCodeCurrDesc;
    @XmlElementRef(name = "CurrencyCodeCurrName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCodeCurrName;
    @XmlElementRef(name = "CurrencyCodeCurrSymbol", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCodeCurrSymbol;
    @XmlElementRef(name = "CurrencyCodeCurrencyID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCodeCurrencyID;
    @XmlElementRef(name = "CurrencyCodeDocumentDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCodeDocumentDesc;
    @XmlElement(name = "CurrencySwitch")
    protected Boolean currencySwitch;
    @XmlElement(name = "CustAllowOTS")
    protected Boolean custAllowOTS;
    @XmlElement(name = "CustNum")
    protected Integer custNum;
    @XmlElement(name = "CustOnCreditHold")
    protected Boolean custOnCreditHold;
    @XmlElementRef(name = "CustTradePartnerName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> custTradePartnerName;
    @XmlElement(name = "CustomerAllowShipTo3")
    protected Boolean customerAllowShipTo3;
    @XmlElementRef(name = "CustomerBTName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerBTName;
    @XmlElementRef(name = "CustomerCustID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerCustID;
    @XmlElementRef(name = "CustomerName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerName;
    @XmlElement(name = "CustomerPrintAck")
    protected Boolean customerPrintAck;
    @XmlElement(name = "CustomerRequiresPO")
    protected Boolean customerRequiresPO;
    @XmlElement(name = "DeclaredAmt")
    protected BigDecimal declaredAmt;
    @XmlElement(name = "DeclaredIns")
    protected Boolean declaredIns;
    @XmlElement(name = "DeliveryConf")
    protected Integer deliveryConf;
    @XmlElementRef(name = "DeliveryType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deliveryType;
    @XmlElementRef(name = "DemandContract", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> demandContract;
    @XmlElement(name = "DemandContractNum")
    protected Integer demandContractNum;
    @XmlElement(name = "DemandHeadSeq")
    protected Integer demandHeadSeq;
    @XmlElementRef(name = "DemandProcessDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> demandProcessDate;
    @XmlElement(name = "DemandProcessTime")
    protected Integer demandProcessTime;
    @XmlElement(name = "DemandRejected")
    protected Boolean demandRejected;
    @XmlElement(name = "DepositBal")
    protected BigDecimal depositBal;
    @XmlElement(name = "DiscountPercent")
    protected BigDecimal discountPercent;
    @XmlElementRef(name = "DispatchReason", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dispatchReason;
    @XmlElementRef(name = "DispatchReasonCodeDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dispatchReasonCodeDescription;
    @XmlElementRef(name = "DoNotShipAfterDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> doNotShipAfterDate;
    @XmlElementRef(name = "DoNotShipBeforeDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> doNotShipBeforeDate;
    @XmlElement(name = "DocCCRounding")
    protected BigDecimal docCCRounding;
    @XmlElement(name = "DocDepositBal")
    protected BigDecimal docDepositBal;
    @XmlElement(name = "DocInTotalCharges")
    protected BigDecimal docInTotalCharges;
    @XmlElement(name = "DocInTotalDiscount")
    protected BigDecimal docInTotalDiscount;
    @XmlElement(name = "DocInTotalMisc")
    protected BigDecimal docInTotalMisc;
    @XmlElement(name = "DocOnly")
    protected Boolean docOnly;
    @XmlElement(name = "DocOrderAmt")
    protected BigDecimal docOrderAmt;
    @XmlElement(name = "DocRounding")
    protected BigDecimal docRounding;
    @XmlElement(name = "DocTotalAdvBill")
    protected BigDecimal docTotalAdvBill;
    @XmlElement(name = "DocTotalCharges")
    protected BigDecimal docTotalCharges;
    @XmlElement(name = "DocTotalComm")
    protected BigDecimal docTotalComm;
    @XmlElement(name = "DocTotalDiscount")
    protected BigDecimal docTotalDiscount;
    @XmlElement(name = "DocTotalMisc")
    protected BigDecimal docTotalMisc;
    @XmlElement(name = "DocTotalNet")
    protected BigDecimal docTotalNet;
    @XmlElement(name = "DocTotalOrder")
    protected BigDecimal docTotalOrder;
    @XmlElement(name = "DocTotalSATax")
    protected BigDecimal docTotalSATax;
    @XmlElement(name = "DocTotalTax")
    protected BigDecimal docTotalTax;
    @XmlElement(name = "DocTotalWHTax")
    protected BigDecimal docTotalWHTax;
    @XmlElementRef(name = "DocumentTypeID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> documentTypeID;
    @XmlElement(name = "DropShip")
    protected Boolean dropShip;
    @XmlElementRef(name = "ECCOrderNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eccOrderNum;
    @XmlElementRef(name = "ECCPONum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eccpoNum;
    @XmlElementRef(name = "ECCPaymentMethod", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eccPaymentMethod;
    @XmlElement(name = "EDIAck")
    protected Boolean ediAck;
    @XmlElement(name = "EDIOrder")
    protected Boolean ediOrder;
    @XmlElement(name = "EDIReady")
    protected Boolean ediReady;
    @XmlElement(name = "ERSOrder")
    protected Boolean ersOrder;
    @XmlElement(name = "ERSOverride")
    protected Boolean ersOverride;
    @XmlElement(name = "EnableJobWizard")
    protected Boolean enableJobWizard;
    @XmlElement(name = "EnableSoldToID")
    protected Boolean enableSoldToID;
    @XmlElementRef(name = "EntryMethod", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> entryMethod;
    @XmlElementRef(name = "EntryPerson", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> entryPerson;
    @XmlElementRef(name = "EntryProcess", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> entryProcess;
    @XmlElement(name = "ExchangeRate")
    protected BigDecimal exchangeRate;
    @XmlElement(name = "ExpirationMonth")
    protected Integer expirationMonth;
    @XmlElement(name = "ExpirationYear")
    protected Integer expirationYear;
    @XmlElementRef(name = "ExtCompany", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> extCompany;
    @XmlElementRef(name = "FFAddress1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ffAddress1;
    @XmlElementRef(name = "FFAddress2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ffAddress2;
    @XmlElementRef(name = "FFAddress3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ffAddress3;
    @XmlElementRef(name = "FFCity", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ffCity;
    @XmlElementRef(name = "FFCompName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ffCompName;
    @XmlElementRef(name = "FFContact", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ffContact;
    @XmlElementRef(name = "FFCountry", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ffCountry;
    @XmlElement(name = "FFCountryNum")
    protected Integer ffCountryNum;
    @XmlElementRef(name = "FFID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ffid;
    @XmlElementRef(name = "FFPhoneNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ffPhoneNum;
    @XmlElementRef(name = "FFState", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ffState;
    @XmlElementRef(name = "FFZip", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ffZip;
    @XmlElementRef(name = "FOB", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fob;
    @XmlElementRef(name = "FOBDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fobDescription;
    @XmlElementRef(name = "GroundType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> groundType;
    @XmlElementRef(name = "HDCaseDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> hdCaseDescription;
    @XmlElement(name = "HDCaseNum")
    protected Integer hdCaseNum;
    @XmlElement(name = "HasMiscCharges")
    protected Boolean hasMiscCharges;
    @XmlElement(name = "HasOrderLines")
    protected Boolean hasOrderLines;
    @XmlElement(name = "Hazmat")
    protected Boolean hazmat;
    @XmlElement(name = "HoldSetByDemand")
    protected Boolean holdSetByDemand;
    @XmlElement(name = "ICPONum")
    protected Integer icpoNum;
    @XmlElement(name = "InPrice")
    protected Boolean inPrice;
    @XmlElement(name = "InTotalCharges")
    protected BigDecimal inTotalCharges;
    @XmlElement(name = "InTotalDiscount")
    protected BigDecimal inTotalDiscount;
    @XmlElement(name = "InTotalMisc")
    protected BigDecimal inTotalMisc;
    @XmlElement(name = "IndividualPackIDs")
    protected Boolean individualPackIDs;
    @XmlElement(name = "IntrntlShip")
    protected Boolean intrntlShip;
    @XmlElementRef(name = "InvCurrCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> invCurrCode;
    @XmlElementRef(name = "InvCurrCurrDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> invCurrCurrDesc;
    @XmlElement(name = "InvcOrderCmp")
    protected Boolean invcOrderCmp;
    @XmlElementRef(name = "InvoiceComment", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> invoiceComment;
    @XmlElement(name = "IsCSRSet")
    protected Boolean isCSRSet;
    @XmlElement(name = "LOCHold")
    protected Boolean locHold;
    @XmlElementRef(name = "LastBatchNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lastBatchNum;
    @XmlElementRef(name = "LastScheduleNumber", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lastScheduleNumber;
    @XmlElementRef(name = "LastTCtrlNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lastTCtrlNum;
    @XmlElementRef(name = "LegalNumber", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> legalNumber;
    @XmlElement(name = "LetterOfInstr")
    protected Boolean letterOfInstr;
    @XmlElementRef(name = "LinkMsg", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> linkMsg;
    @XmlElement(name = "Linked")
    protected Boolean linked;
    @XmlElement(name = "LockQty")
    protected Boolean lockQty;
    @XmlElement(name = "LockRate")
    protected Boolean lockRate;
    @XmlElementRef(name = "NeedByDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> needByDate;
    @XmlElement(name = "NonStdPkg")
    protected Boolean nonStdPkg;
    @XmlElementRef(name = "NotifyEMail", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> notifyEMail;
    @XmlElement(name = "NotifyFlag")
    protected Boolean notifyFlag;
    @XmlElementRef(name = "OTSAddress1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsAddress1;
    @XmlElementRef(name = "OTSAddress2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsAddress2;
    @XmlElementRef(name = "OTSAddress3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsAddress3;
    @XmlElementRef(name = "OTSCity", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsCity;
    @XmlElementRef(name = "OTSCntryDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsCntryDescription;
    @XmlElementRef(name = "OTSContact", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsContact;
    @XmlElement(name = "OTSCountryNum")
    protected Integer otsCountryNum;
    @XmlElement(name = "OTSCustSaved")
    protected Boolean otsCustSaved;
    @XmlElementRef(name = "OTSFaxNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsFaxNum;
    @XmlElementRef(name = "OTSName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsName;
    @XmlElementRef(name = "OTSPhoneNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsPhoneNum;
    @XmlElementRef(name = "OTSResaleID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsResaleID;
    @XmlElementRef(name = "OTSSaveAs", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsSaveAs;
    @XmlElementRef(name = "OTSSaveCustID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsSaveCustID;
    @XmlElement(name = "OTSSaved")
    protected Boolean otsSaved;
    @XmlElementRef(name = "OTSShipToNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsShipToNum;
    @XmlElementRef(name = "OTSState", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsState;
    @XmlElementRef(name = "OTSTaxRegionCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otsTaxRegionCode;
    @XmlElementRef(name = "OTSZIP", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otszip;
    @XmlElement(name = "OpenOrder")
    protected Boolean openOrder;
    @XmlElement(name = "OrderAmt")
    protected BigDecimal orderAmt;
    @XmlElementRef(name = "OrderCSR", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderCSR;
    @XmlElementRef(name = "OrderComment", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderComment;
    @XmlElementRef(name = "OrderDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> orderDate;
    @XmlElement(name = "OrderHeld")
    protected Boolean orderHeld;
    @XmlElement(name = "OrderNum")
    protected Integer orderNum;
    @XmlElementRef(name = "OrderStatus", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderStatus;
    @XmlElementRef(name = "OurBank", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ourBank;
    @XmlElementRef(name = "OurBankBankName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ourBankBankName;
    @XmlElementRef(name = "OurBankDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ourBankDescription;
    @XmlElement(name = "OutboundSalesDocCtr")
    protected Integer outboundSalesDocCtr;
    @XmlElement(name = "OutboundShipDocsCtr")
    protected Integer outboundShipDocsCtr;
    @XmlElement(name = "OverrideCarrier")
    protected Boolean overrideCarrier;
    @XmlElement(name = "OverrideService")
    protected Boolean overrideService;
    @XmlElementRef(name = "PONum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> poNum;
    @XmlElementRef(name = "PSCurrCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> psCurrCode;
    @XmlElementRef(name = "PSCurrCurrDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> psCurrCurrDesc;
    @XmlElement(name = "ParentCustNum")
    protected Integer parentCustNum;
    @XmlElementRef(name = "PayAccount", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> payAccount;
    @XmlElementRef(name = "PayBTAddress1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> payBTAddress1;
    @XmlElementRef(name = "PayBTAddress2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> payBTAddress2;
    @XmlElementRef(name = "PayBTAddress3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> payBTAddress3;
    @XmlElementRef(name = "PayBTCity", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> payBTCity;
    @XmlElementRef(name = "PayBTCountry", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> payBTCountry;
    @XmlElement(name = "PayBTCountryNum")
    protected Integer payBTCountryNum;
    @XmlElementRef(name = "PayBTPhone", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> payBTPhone;
    @XmlElementRef(name = "PayBTState", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> payBTState;
    @XmlElementRef(name = "PayBTZip", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> payBTZip;
    @XmlElementRef(name = "PayFlag", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> payFlag;
    @XmlElementRef(name = "PickListComment", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pickListComment;
    @XmlElementRef(name = "Plant", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> plant;
    @XmlElement(name = "PrcConNum")
    protected Integer prcConNum;
    @XmlElementRef(name = "ProFormaInvComment", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> proFormaInvComment;
    @XmlElementRef(name = "ProcessCard", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> processCard;
    @XmlElementRef(name = "RateGrpCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rateGrpCode;
    @XmlElementRef(name = "RateGrpDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rateGrpDescription;
    @XmlElement(name = "ReadyToCalc")
    protected Boolean readyToCalc;
    @XmlElementRef(name = "RefNotes", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> refNotes;
    @XmlElementRef(name = "ReferencePNRef", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> referencePNRef;
    @XmlElement(name = "RepRate1")
    protected BigDecimal repRate1;
    @XmlElement(name = "RepRate2")
    protected BigDecimal repRate2;
    @XmlElement(name = "RepRate3")
    protected BigDecimal repRate3;
    @XmlElement(name = "RepRate4")
    protected BigDecimal repRate4;
    @XmlElement(name = "RepRate5")
    protected BigDecimal repRate5;
    @XmlElement(name = "RepSplit1")
    protected Integer repSplit1;
    @XmlElement(name = "RepSplit2")
    protected Integer repSplit2;
    @XmlElement(name = "RepSplit3")
    protected Integer repSplit3;
    @XmlElement(name = "RepSplit4")
    protected Integer repSplit4;
    @XmlElement(name = "RepSplit5")
    protected Integer repSplit5;
    @XmlElementRef(name = "RequestDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> requestDate;
    @XmlElement(name = "ResDelivery")
    protected Boolean resDelivery;
    @XmlElementRef(name = "ReservePriDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reservePriDescription;
    @XmlElementRef(name = "ReservePriorityCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reservePriorityCode;
    @XmlElement(name = "Rounding")
    protected BigDecimal rounding;
    @XmlElement(name = "Rpt1CCAmount")
    protected BigDecimal rpt1CCAmount;
    @XmlElement(name = "Rpt1CCFreight")
    protected BigDecimal rpt1CCFreight;
    @XmlElement(name = "Rpt1CCRounding")
    protected BigDecimal rpt1CCRounding;
    @XmlElement(name = "Rpt1CCTax")
    protected BigDecimal rpt1CCTax;
    @XmlElement(name = "Rpt1CCTotal")
    protected BigDecimal rpt1CCTotal;
    @XmlElement(name = "Rpt1DepositBal")
    protected BigDecimal rpt1DepositBal;
    @XmlElement(name = "Rpt1InTotalCharges")
    protected BigDecimal rpt1InTotalCharges;
    @XmlElement(name = "Rpt1InTotalDiscount")
    protected BigDecimal rpt1InTotalDiscount;
    @XmlElement(name = "Rpt1InTotalMisc")
    protected BigDecimal rpt1InTotalMisc;
    @XmlElement(name = "Rpt1OrderAmt")
    protected BigDecimal rpt1OrderAmt;
    @XmlElement(name = "Rpt1Rounding")
    protected BigDecimal rpt1Rounding;
    @XmlElement(name = "Rpt1TotalAdvBill")
    protected BigDecimal rpt1TotalAdvBill;
    @XmlElement(name = "Rpt1TotalCharges")
    protected BigDecimal rpt1TotalCharges;
    @XmlElement(name = "Rpt1TotalComm")
    protected BigDecimal rpt1TotalComm;
    @XmlElement(name = "Rpt1TotalDiscount")
    protected BigDecimal rpt1TotalDiscount;
    @XmlElement(name = "Rpt1TotalMisc")
    protected BigDecimal rpt1TotalMisc;
    @XmlElement(name = "Rpt1TotalNet")
    protected BigDecimal rpt1TotalNet;
    @XmlElement(name = "Rpt1TotalSATax")
    protected BigDecimal rpt1TotalSATax;
    @XmlElement(name = "Rpt1TotalTax")
    protected BigDecimal rpt1TotalTax;
    @XmlElement(name = "Rpt1TotalWHTax")
    protected BigDecimal rpt1TotalWHTax;
    @XmlElement(name = "Rpt2CCAmount")
    protected BigDecimal rpt2CCAmount;
    @XmlElement(name = "Rpt2CCFreight")
    protected BigDecimal rpt2CCFreight;
    @XmlElement(name = "Rpt2CCRounding")
    protected BigDecimal rpt2CCRounding;
    @XmlElement(name = "Rpt2CCTax")
    protected BigDecimal rpt2CCTax;
    @XmlElement(name = "Rpt2CCTotal")
    protected BigDecimal rpt2CCTotal;
    @XmlElement(name = "Rpt2DepositBal")
    protected BigDecimal rpt2DepositBal;
    @XmlElement(name = "Rpt2InTotalCharges")
    protected BigDecimal rpt2InTotalCharges;
    @XmlElement(name = "Rpt2InTotalDiscount")
    protected BigDecimal rpt2InTotalDiscount;
    @XmlElement(name = "Rpt2InTotalMisc")
    protected BigDecimal rpt2InTotalMisc;
    @XmlElement(name = "Rpt2OrderAmt")
    protected BigDecimal rpt2OrderAmt;
    @XmlElement(name = "Rpt2Rounding")
    protected BigDecimal rpt2Rounding;
    @XmlElement(name = "Rpt2TotalAdvBill")
    protected BigDecimal rpt2TotalAdvBill;
    @XmlElement(name = "Rpt2TotalCharges")
    protected BigDecimal rpt2TotalCharges;
    @XmlElement(name = "Rpt2TotalComm")
    protected BigDecimal rpt2TotalComm;
    @XmlElement(name = "Rpt2TotalDiscount")
    protected BigDecimal rpt2TotalDiscount;
    @XmlElement(name = "Rpt2TotalMisc")
    protected BigDecimal rpt2TotalMisc;
    @XmlElement(name = "Rpt2TotalNet")
    protected BigDecimal rpt2TotalNet;
    @XmlElement(name = "Rpt2TotalSATax")
    protected BigDecimal rpt2TotalSATax;
    @XmlElement(name = "Rpt2TotalTax")
    protected BigDecimal rpt2TotalTax;
    @XmlElement(name = "Rpt2TotalWHTax")
    protected BigDecimal rpt2TotalWHTax;
    @XmlElement(name = "Rpt3CCAmount")
    protected BigDecimal rpt3CCAmount;
    @XmlElement(name = "Rpt3CCFreight")
    protected BigDecimal rpt3CCFreight;
    @XmlElement(name = "Rpt3CCRounding")
    protected BigDecimal rpt3CCRounding;
    @XmlElement(name = "Rpt3CCTax")
    protected BigDecimal rpt3CCTax;
    @XmlElement(name = "Rpt3CCTotal")
    protected BigDecimal rpt3CCTotal;
    @XmlElement(name = "Rpt3DepositBal")
    protected BigDecimal rpt3DepositBal;
    @XmlElement(name = "Rpt3InTotalCharges")
    protected BigDecimal rpt3InTotalCharges;
    @XmlElement(name = "Rpt3InTotalDiscount")
    protected BigDecimal rpt3InTotalDiscount;
    @XmlElement(name = "Rpt3InTotalMisc")
    protected BigDecimal rpt3InTotalMisc;
    @XmlElement(name = "Rpt3OrderAmt")
    protected BigDecimal rpt3OrderAmt;
    @XmlElement(name = "Rpt3Rounding")
    protected BigDecimal rpt3Rounding;
    @XmlElement(name = "Rpt3TotalAdvBill")
    protected BigDecimal rpt3TotalAdvBill;
    @XmlElement(name = "Rpt3TotalCharges")
    protected BigDecimal rpt3TotalCharges;
    @XmlElement(name = "Rpt3TotalComm")
    protected BigDecimal rpt3TotalComm;
    @XmlElement(name = "Rpt3TotalDiscount")
    protected BigDecimal rpt3TotalDiscount;
    @XmlElement(name = "Rpt3TotalMisc")
    protected BigDecimal rpt3TotalMisc;
    @XmlElement(name = "Rpt3TotalNet")
    protected BigDecimal rpt3TotalNet;
    @XmlElement(name = "Rpt3TotalSATax")
    protected BigDecimal rpt3TotalSATax;
    @XmlElement(name = "Rpt3TotalTax")
    protected BigDecimal rpt3TotalTax;
    @XmlElement(name = "Rpt3TotalWHTax")
    protected BigDecimal rpt3TotalWHTax;
    @XmlElement(name = "SRCommAmt1")
    protected BigDecimal srCommAmt1;
    @XmlElement(name = "SRCommAmt2")
    protected BigDecimal srCommAmt2;
    @XmlElement(name = "SRCommAmt3")
    protected BigDecimal srCommAmt3;
    @XmlElement(name = "SRCommAmt4")
    protected BigDecimal srCommAmt4;
    @XmlElement(name = "SRCommAmt5")
    protected BigDecimal srCommAmt5;
    @XmlElement(name = "SRCommableAmt1")
    protected BigDecimal srCommableAmt1;
    @XmlElement(name = "SRCommableAmt2")
    protected BigDecimal srCommableAmt2;
    @XmlElement(name = "SRCommableAmt3")
    protected BigDecimal srCommableAmt3;
    @XmlElement(name = "SRCommableAmt4")
    protected BigDecimal srCommableAmt4;
    @XmlElement(name = "SRCommableAmt5")
    protected BigDecimal srCommableAmt5;
    @XmlElementRef(name = "SalesRepCode1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepCode1;
    @XmlElementRef(name = "SalesRepCode2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepCode2;
    @XmlElementRef(name = "SalesRepCode3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepCode3;
    @XmlElementRef(name = "SalesRepCode4", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepCode4;
    @XmlElementRef(name = "SalesRepCode5", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepCode5;
    @XmlElementRef(name = "SalesRepList", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepList;
    @XmlElementRef(name = "SalesRepName1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepName1;
    @XmlElementRef(name = "SalesRepName2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepName2;
    @XmlElementRef(name = "SalesRepName3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepName3;
    @XmlElementRef(name = "SalesRepName4", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepName4;
    @XmlElementRef(name = "SalesRepName5", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesRepName5;
    @XmlElement(name = "SatDelivery")
    protected Boolean satDelivery;
    @XmlElement(name = "SatPickup")
    protected Boolean satPickup;
    @XmlElement(name = "ServAlert")
    protected Boolean servAlert;
    @XmlElementRef(name = "ServAuthNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servAuthNum;
    @XmlElementRef(name = "ServDeliveryDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> servDeliveryDate;
    @XmlElement(name = "ServHomeDel")
    protected Boolean servHomeDel;
    @XmlElementRef(name = "ServInstruct", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servInstruct;
    @XmlElementRef(name = "ServPhone", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servPhone;
    @XmlElementRef(name = "ServRef1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servRef1;
    @XmlElementRef(name = "ServRef2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servRef2;
    @XmlElementRef(name = "ServRef3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servRef3;
    @XmlElementRef(name = "ServRef4", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servRef4;
    @XmlElementRef(name = "ServRef5", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servRef5;
    @XmlElement(name = "ServRelease")
    protected Boolean servRelease;
    @XmlElement(name = "ServSignature")
    protected Boolean servSignature;
    @XmlElementRef(name = "ShipComment", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipComment;
    @XmlElement(name = "ShipExprtDeclartn")
    protected Boolean shipExprtDeclartn;
    @XmlElement(name = "ShipOrderComplete")
    protected Boolean shipOrderComplete;
    @XmlElementRef(name = "ShipToAddressList", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipToAddressList;
    @XmlElementRef(name = "ShipToContactEMailAddress", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipToContactEMailAddress;
    @XmlElementRef(name = "ShipToContactFaxNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipToContactFaxNum;
    @XmlElementRef(name = "ShipToContactName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipToContactName;
    @XmlElementRef(name = "ShipToContactPhoneNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipToContactPhoneNum;
    @XmlElementRef(name = "ShipToCustId", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipToCustId;
    @XmlElement(name = "ShipToCustNum")
    protected Integer shipToCustNum;
    @XmlElementRef(name = "ShipToNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipToNum;
    @XmlElementRef(name = "ShipViaCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipViaCode;
    @XmlElementRef(name = "ShipViaCodeDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipViaCodeDescription;
    @XmlElementRef(name = "ShipViaCodeWebDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipViaCodeWebDesc;
    @XmlElement(name = "ShowApplyOrderDiscountsControl")
    protected Boolean showApplyOrderDiscountsControl;
    @XmlElement(name = "ShpConNum")
    protected Integer shpConNum;
    @XmlElement(name = "SndAlrtShp")
    protected Boolean sndAlrtShp;
    @XmlElementRef(name = "SoldToAddressList", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> soldToAddressList;
    @XmlElementRef(name = "SoldToContactEMailAddress", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> soldToContactEMailAddress;
    @XmlElementRef(name = "SoldToContactFaxNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> soldToContactFaxNum;
    @XmlElementRef(name = "SoldToContactName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> soldToContactName;
    @XmlElementRef(name = "SoldToContactPhoneNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> soldToContactPhoneNum;
    @XmlElement(name = "SysRevID")
    protected Long sysRevID;
    @XmlElementRef(name = "TaxPoint", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> taxPoint;
    @XmlElementRef(name = "TaxRateDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> taxRateDate;
    @XmlElementRef(name = "TaxRegionCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxRegionCode;
    @XmlElementRef(name = "TaxRegionCodeDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxRegionCodeDescription;
    @XmlElementRef(name = "TermsCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> termsCode;
    @XmlElementRef(name = "TermsCodeDescription", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> termsCodeDescription;
    @XmlElement(name = "TotalAdvBill")
    protected BigDecimal totalAdvBill;
    @XmlElement(name = "TotalCharges")
    protected BigDecimal totalCharges;
    @XmlElement(name = "TotalComm")
    protected BigDecimal totalComm;
    @XmlElement(name = "TotalCommLines")
    protected Integer totalCommLines;
    @XmlElement(name = "TotalDiscount")
    protected BigDecimal totalDiscount;
    @XmlElement(name = "TotalInvoiced")
    protected BigDecimal totalInvoiced;
    @XmlElement(name = "TotalLines")
    protected Integer totalLines;
    @XmlElement(name = "TotalMisc")
    protected BigDecimal totalMisc;
    @XmlElement(name = "TotalNet")
    protected BigDecimal totalNet;
    @XmlElement(name = "TotalOrder")
    protected BigDecimal totalOrder;
    @XmlElement(name = "TotalRelDates")
    protected Integer totalRelDates;
    @XmlElement(name = "TotalReleases")
    protected Integer totalReleases;
    @XmlElement(name = "TotalSATax")
    protected BigDecimal totalSATax;
    @XmlElement(name = "TotalShipped")
    protected BigDecimal totalShipped;
    @XmlElement(name = "TotalTax")
    protected BigDecimal totalTax;
    @XmlElement(name = "TotalWHTax")
    protected BigDecimal totalWHTax;
    @XmlElementRef(name = "TranDocTypeID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tranDocTypeID;
    @XmlElementRef(name = "UPSQVMemo", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> upsqvMemo;
    @XmlElementRef(name = "UPSQVShipFromName", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> upsqvShipFromName;
    @XmlElement(name = "UPSQuantumView")
    protected Boolean upsQuantumView;
    @XmlElement(name = "UpdateDtlAndRelRecords")
    protected Boolean updateDtlAndRelRecords;
    @XmlElement(name = "UseOTS")
    protected Boolean useOTS;
    @XmlElementRef(name = "UserChar1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userChar1;
    @XmlElementRef(name = "UserChar2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userChar2;
    @XmlElementRef(name = "UserChar3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userChar3;
    @XmlElementRef(name = "UserChar4", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userChar4;
    @XmlElementRef(name = "UserDate1", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> userDate1;
    @XmlElementRef(name = "UserDate2", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> userDate2;
    @XmlElementRef(name = "UserDate3", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> userDate3;
    @XmlElementRef(name = "UserDate4", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> userDate4;
    @XmlElement(name = "UserDecimal1")
    protected BigDecimal userDecimal1;
    @XmlElement(name = "UserDecimal2")
    protected BigDecimal userDecimal2;
    @XmlElement(name = "UserInteger1")
    protected Integer userInteger1;
    @XmlElement(name = "UserInteger2")
    protected Integer userInteger2;
    @XmlElement(name = "VoidOrder")
    protected Boolean voidOrder;
    @XmlElementRef(name = "WIApplication", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> wiApplication;
    @XmlElement(name = "WICreditCardorder")
    protected Boolean wiCreditCardorder;
    @XmlElementRef(name = "WIOrder", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> wiOrder;
    @XmlElementRef(name = "WIUserID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> wiUserID;
    @XmlElementRef(name = "WIUsername", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> wiUsername;
    @XmlElementRef(name = "WebEntryPerson", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> webEntryPerson;
    @XmlElement(name = "WebOrder")
    protected Boolean webOrder;
    @XmlElementRef(name = "XRefContractDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> xRefContractDate;
    @XmlElementRef(name = "XRefContractNum", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> xRefContractNum;
    @XmlElementRef(name = "dspBTCustID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dspBTCustID;

    /**
     * Gets the value of the arlocid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getARLOCID() {
        return arlocid;
    }

    /**
     * Sets the value of the arlocid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setARLOCID(JAXBElement<String> value) {
        this.arlocid = value;
    }

    /**
     * Gets the value of the avsAddr property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAVSAddr() {
        return avsAddr;
    }

    /**
     * Sets the value of the avsAddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAVSAddr(JAXBElement<String> value) {
        this.avsAddr = value;
    }

    /**
     * Gets the value of the avsZip property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAVSZip() {
        return avsZip;
    }

    /**
     * Sets the value of the avsZip property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAVSZip(JAXBElement<String> value) {
        this.avsZip = value;
    }

    /**
     * Gets the value of the ackEmailSent property.
     * This getter has been renamed from isAckEmailSent() to getAckEmailSent() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAckEmailSent() {
        return ackEmailSent;
    }

    /**
     * Sets the value of the ackEmailSent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAckEmailSent(Boolean value) {
        this.ackEmailSent = value;
    }

    /**
     * Gets the value of the addlHdlgFlag property.
     * This getter has been renamed from isAddlHdlgFlag() to getAddlHdlgFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAddlHdlgFlag() {
        return addlHdlgFlag;
    }

    /**
     * Sets the value of the addlHdlgFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddlHdlgFlag(Boolean value) {
        this.addlHdlgFlag = value;
    }

    /**
     * Gets the value of the allocPriorityCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAllocPriorityCode() {
        return allocPriorityCode;
    }

    /**
     * Sets the value of the allocPriorityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAllocPriorityCode(JAXBElement<String> value) {
        this.allocPriorityCode = value;
    }

    /**
     * Gets the value of the applyChrg property.
     * This getter has been renamed from isApplyChrg() to getApplyChrg() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getApplyChrg() {
        return applyChrg;
    }

    /**
     * Sets the value of the applyChrg property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApplyChrg(Boolean value) {
        this.applyChrg = value;
    }

    /**
     * Gets the value of the applyOrderBasedDisc property.
     * This getter has been renamed from isApplyOrderBasedDisc() to getApplyOrderBasedDisc() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getApplyOrderBasedDisc() {
        return applyOrderBasedDisc;
    }

    /**
     * Sets the value of the applyOrderBasedDisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApplyOrderBasedDisc(Boolean value) {
        this.applyOrderBasedDisc = value;
    }

    /**
     * Gets the value of the autoOrderBasedDisc property.
     * This getter has been renamed from isAutoOrderBasedDisc() to getAutoOrderBasedDisc() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAutoOrderBasedDisc() {
        return autoOrderBasedDisc;
    }

    /**
     * Sets the value of the autoOrderBasedDisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoOrderBasedDisc(Boolean value) {
        this.autoOrderBasedDisc = value;
    }

    /**
     * Gets the value of the autoPrintReady property.
     * This getter has been renamed from isAutoPrintReady() to getAutoPrintReady() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAutoPrintReady() {
        return autoPrintReady;
    }

    /**
     * Sets the value of the autoPrintReady property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoPrintReady(Boolean value) {
        this.autoPrintReady = value;
    }

    /**
     * Gets the value of the availBTCustList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAvailBTCustList() {
        return availBTCustList;
    }

    /**
     * Sets the value of the availBTCustList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAvailBTCustList(JAXBElement<String> value) {
        this.availBTCustList = value;
    }

    /**
     * Gets the value of the btAddressList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBTAddressList() {
        return btAddressList;
    }

    /**
     * Sets the value of the btAddressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBTAddressList(JAXBElement<String> value) {
        this.btAddressList = value;
    }

    /**
     * Gets the value of the btConNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBTConNum() {
        return btConNum;
    }

    /**
     * Sets the value of the btConNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBTConNum(Integer value) {
        this.btConNum = value;
    }

    /**
     * Gets the value of the btContactEMailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBTContactEMailAddress() {
        return btContactEMailAddress;
    }

    /**
     * Sets the value of the btContactEMailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBTContactEMailAddress(JAXBElement<String> value) {
        this.btContactEMailAddress = value;
    }

    /**
     * Gets the value of the btContactFaxNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBTContactFaxNum() {
        return btContactFaxNum;
    }

    /**
     * Sets the value of the btContactFaxNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBTContactFaxNum(JAXBElement<String> value) {
        this.btContactFaxNum = value;
    }

    /**
     * Gets the value of the btContactName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBTContactName() {
        return btContactName;
    }

    /**
     * Sets the value of the btContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBTContactName(JAXBElement<String> value) {
        this.btContactName = value;
    }

    /**
     * Gets the value of the btContactPhoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBTContactPhoneNum() {
        return btContactPhoneNum;
    }

    /**
     * Sets the value of the btContactPhoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBTContactPhoneNum(JAXBElement<String> value) {
        this.btContactPhoneNum = value;
    }

    /**
     * Gets the value of the btCustID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBTCustID() {
        return btCustID;
    }

    /**
     * Sets the value of the btCustID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBTCustID(JAXBElement<String> value) {
        this.btCustID = value;
    }

    /**
     * Gets the value of the btCustNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBTCustNum() {
        return btCustNum;
    }

    /**
     * Sets the value of the btCustNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBTCustNum(Integer value) {
        this.btCustNum = value;
    }

    /**
     * Gets the value of the btCustNumBTName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBTCustNumBTName() {
        return btCustNumBTName;
    }

    /**
     * Sets the value of the btCustNumBTName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBTCustNumBTName(JAXBElement<String> value) {
        this.btCustNumBTName = value;
    }

    /**
     * Gets the value of the btCustNumCustID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBTCustNumCustID() {
        return btCustNumCustID;
    }

    /**
     * Sets the value of the btCustNumCustID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBTCustNumCustID(JAXBElement<String> value) {
        this.btCustNumCustID = value;
    }

    /**
     * Gets the value of the btCustNumName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBTCustNumName() {
        return btCustNumName;
    }

    /**
     * Sets the value of the btCustNumName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBTCustNumName(JAXBElement<String> value) {
        this.btCustNumName = value;
    }

    /**
     * Gets the value of the baseCurrSymbol property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBaseCurrSymbol() {
        return baseCurrSymbol;
    }

    /**
     * Sets the value of the baseCurrSymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBaseCurrSymbol(JAXBElement<String> value) {
        this.baseCurrSymbol = value;
    }

    /**
     * Gets the value of the baseCurrencyID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBaseCurrencyID() {
        return baseCurrencyID;
    }

    /**
     * Sets the value of the baseCurrencyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBaseCurrencyID(JAXBElement<String> value) {
        this.baseCurrencyID = value;
    }

    /**
     * Gets the value of the billToCustomerName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBillToCustomerName() {
        return billToCustomerName;
    }

    /**
     * Sets the value of the billToCustomerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBillToCustomerName(JAXBElement<String> value) {
        this.billToCustomerName = value;
    }

    /**
     * Gets the value of the bitFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBitFlag() {
        return bitFlag;
    }

    /**
     * Sets the value of the bitFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBitFlag(Integer value) {
        this.bitFlag = value;
    }

    /**
     * Gets the value of the ccAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCCAmount() {
        return ccAmount;
    }

    /**
     * Sets the value of the ccAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCCAmount(BigDecimal value) {
        this.ccAmount = value;
    }

    /**
     * Gets the value of the ccApprovalNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCCApprovalNum() {
        return ccApprovalNum;
    }

    /**
     * Sets the value of the ccApprovalNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCCApprovalNum(JAXBElement<String> value) {
        this.ccApprovalNum = value;
    }

    /**
     * Gets the value of the cccscid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCCCSCID() {
        return cccscid;
    }

    /**
     * Sets the value of the cccscid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCCCSCID(JAXBElement<String> value) {
        this.cccscid = value;
    }

    /**
     * Gets the value of the cccscidToken property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCCCSCIDToken() {
        return cccscidToken;
    }

    /**
     * Sets the value of the cccscidToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCCCSCIDToken(JAXBElement<String> value) {
        this.cccscidToken = value;
    }

    /**
     * Gets the value of the ccDocAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCCDocAmount() {
        return ccDocAmount;
    }

    /**
     * Sets the value of the ccDocAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCCDocAmount(BigDecimal value) {
        this.ccDocAmount = value;
    }

    /**
     * Gets the value of the ccDocFreight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCCDocFreight() {
        return ccDocFreight;
    }

    /**
     * Sets the value of the ccDocFreight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCCDocFreight(BigDecimal value) {
        this.ccDocFreight = value;
    }

    /**
     * Gets the value of the ccDocTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCCDocTax() {
        return ccDocTax;
    }

    /**
     * Sets the value of the ccDocTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCCDocTax(BigDecimal value) {
        this.ccDocTax = value;
    }

    /**
     * Gets the value of the ccDocTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCCDocTotal() {
        return ccDocTotal;
    }

    /**
     * Sets the value of the ccDocTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCCDocTotal(BigDecimal value) {
        this.ccDocTotal = value;
    }

    /**
     * Gets the value of the ccFreight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCCFreight() {
        return ccFreight;
    }

    /**
     * Sets the value of the ccFreight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCCFreight(BigDecimal value) {
        this.ccFreight = value;
    }

    /**
     * Gets the value of the ccResponse property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCCResponse() {
        return ccResponse;
    }

    /**
     * Sets the value of the ccResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCCResponse(JAXBElement<String> value) {
        this.ccResponse = value;
    }

    /**
     * Gets the value of the ccRounding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCCRounding() {
        return ccRounding;
    }

    /**
     * Sets the value of the ccRounding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCCRounding(BigDecimal value) {
        this.ccRounding = value;
    }

    /**
     * Gets the value of the ccStreetAddr property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCCStreetAddr() {
        return ccStreetAddr;
    }

    /**
     * Sets the value of the ccStreetAddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCCStreetAddr(JAXBElement<String> value) {
        this.ccStreetAddr = value;
    }

    /**
     * Gets the value of the ccTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCCTax() {
        return ccTax;
    }

    /**
     * Sets the value of the ccTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCCTax(BigDecimal value) {
        this.ccTax = value;
    }

    /**
     * Gets the value of the ccTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCCTotal() {
        return ccTotal;
    }

    /**
     * Sets the value of the ccTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCCTotal(BigDecimal value) {
        this.ccTotal = value;
    }

    /**
     * Gets the value of the ccTranID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCCTranID() {
        return ccTranID;
    }

    /**
     * Sets the value of the ccTranID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCCTranID(JAXBElement<String> value) {
        this.ccTranID = value;
    }

    /**
     * Gets the value of the ccTranType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCCTranType() {
        return ccTranType;
    }

    /**
     * Sets the value of the ccTranType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCCTranType(JAXBElement<String> value) {
        this.ccTranType = value;
    }

    /**
     * Gets the value of the ccZip property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCCZip() {
        return ccZip;
    }

    /**
     * Sets the value of the ccZip property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCCZip(JAXBElement<String> value) {
        this.ccZip = value;
    }

    /**
     * Gets the value of the cod property.
     * This getter has been renamed from isCOD() to getCOD() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCOD() {
        return cod;
    }

    /**
     * Sets the value of the cod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCOD(Boolean value) {
        this.cod = value;
    }

    /**
     * Gets the value of the codAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCODAmount() {
        return codAmount;
    }

    /**
     * Sets the value of the codAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCODAmount(BigDecimal value) {
        this.codAmount = value;
    }

    /**
     * Gets the value of the codCheck property.
     * This getter has been renamed from isCODCheck() to getCODCheck() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCODCheck() {
        return codCheck;
    }

    /**
     * Sets the value of the codCheck property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCODCheck(Boolean value) {
        this.codCheck = value;
    }

    /**
     * Gets the value of the codFreight property.
     * This getter has been renamed from isCODFreight() to getCODFreight() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCODFreight() {
        return codFreight;
    }

    /**
     * Sets the value of the codFreight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCODFreight(Boolean value) {
        this.codFreight = value;
    }

    /**
     * Gets the value of the cscResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCSCResult() {
        return cscResult;
    }

    /**
     * Sets the value of the cscResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCSCResult(JAXBElement<String> value) {
        this.cscResult = value;
    }

    /**
     * Gets the value of the canChangeTaxLiab property.
     * This getter has been renamed from isCanChangeTaxLiab() to getCanChangeTaxLiab() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCanChangeTaxLiab() {
        return canChangeTaxLiab;
    }

    /**
     * Sets the value of the canChangeTaxLiab property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCanChangeTaxLiab(Boolean value) {
        this.canChangeTaxLiab = value;
    }

    /**
     * Gets the value of the cancelAfterDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getCancelAfterDate() {
        return cancelAfterDate;
    }

    /**
     * Sets the value of the cancelAfterDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setCancelAfterDate(JAXBElement<XMLGregorianCalendar> value) {
        this.cancelAfterDate = value;
    }

    /**
     * Gets the value of the cardID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCardID() {
        return cardID;
    }

    /**
     * Sets the value of the cardID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCardID(JAXBElement<String> value) {
        this.cardID = value;
    }

    /**
     * Gets the value of the cardMemberName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCardMemberName() {
        return cardMemberName;
    }

    /**
     * Sets the value of the cardMemberName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCardMemberName(JAXBElement<String> value) {
        this.cardMemberName = value;
    }

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCardNumber(JAXBElement<String> value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the cardStore property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCardStore() {
        return cardStore;
    }

    /**
     * Sets the value of the cardStore property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCardStore(JAXBElement<String> value) {
        this.cardStore = value;
    }

    /**
     * Gets the value of the cardType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCardType() {
        return cardType;
    }

    /**
     * Sets the value of the cardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCardType(JAXBElement<String> value) {
        this.cardType = value;
    }

    /**
     * Gets the value of the cardTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCardTypeDescription() {
        return cardTypeDescription;
    }

    /**
     * Sets the value of the cardTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCardTypeDescription(JAXBElement<String> value) {
        this.cardTypeDescription = value;
    }

    /**
     * Gets the value of the cardmemberReference property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCardmemberReference() {
        return cardmemberReference;
    }

    /**
     * Sets the value of the cardmemberReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCardmemberReference(JAXBElement<String> value) {
        this.cardmemberReference = value;
    }

    /**
     * Gets the value of the certOfOrigin property.
     * This getter has been renamed from isCertOfOrigin() to getCertOfOrigin() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCertOfOrigin() {
        return certOfOrigin;
    }

    /**
     * Sets the value of the certOfOrigin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCertOfOrigin(Boolean value) {
        this.certOfOrigin = value;
    }

    /**
     * Gets the value of the changeDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getChangeDate() {
        return changeDate;
    }

    /**
     * Sets the value of the changeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setChangeDate(JAXBElement<XMLGregorianCalendar> value) {
        this.changeDate = value;
    }

    /**
     * Gets the value of the changeTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChangeTime() {
        return changeTime;
    }

    /**
     * Sets the value of the changeTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChangeTime(Integer value) {
        this.changeTime = value;
    }

    /**
     * Gets the value of the changedBy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangedBy() {
        return changedBy;
    }

    /**
     * Sets the value of the changedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangedBy(JAXBElement<String> value) {
        this.changedBy = value;
    }

    /**
     * Gets the value of the chrgAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChrgAmount() {
        return chrgAmount;
    }

    /**
     * Sets the value of the chrgAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChrgAmount(BigDecimal value) {
        this.chrgAmount = value;
    }

    /**
     * Gets the value of the commercialInvoice property.
     * This getter has been renamed from isCommercialInvoice() to getCommercialInvoice() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCommercialInvoice() {
        return commercialInvoice;
    }

    /**
     * Sets the value of the commercialInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCommercialInvoice(Boolean value) {
        this.commercialInvoice = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompany(JAXBElement<String> value) {
        this.company = value;
    }

    /**
     * Gets the value of the counterSale property.
     * This getter has been renamed from isCounterSale() to getCounterSale() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCounterSale() {
        return counterSale;
    }

    /**
     * Sets the value of the counterSale property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCounterSale(Boolean value) {
        this.counterSale = value;
    }

    /**
     * Gets the value of the createInvoice property.
     * This getter has been renamed from isCreateInvoice() to getCreateInvoice() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCreateInvoice() {
        return createInvoice;
    }

    /**
     * Sets the value of the createInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCreateInvoice(Boolean value) {
        this.createInvoice = value;
    }

    /**
     * Gets the value of the createPackingSlip property.
     * This getter has been renamed from isCreatePackingSlip() to getCreatePackingSlip() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCreatePackingSlip() {
        return createPackingSlip;
    }

    /**
     * Sets the value of the createPackingSlip property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCreatePackingSlip(Boolean value) {
        this.createPackingSlip = value;
    }

    /**
     * Gets the value of the creditCardOrder property.
     * This getter has been renamed from isCreditCardOrder() to getCreditCardOrder() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCreditCardOrder() {
        return creditCardOrder;
    }

    /**
     * Sets the value of the creditCardOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCreditCardOrder(Boolean value) {
        this.creditCardOrder = value;
    }

    /**
     * Gets the value of the creditOverride property.
     * This getter has been renamed from isCreditOverride() to getCreditOverride() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCreditOverride() {
        return creditOverride;
    }

    /**
     * Sets the value of the creditOverride property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCreditOverride(Boolean value) {
        this.creditOverride = value;
    }

    /**
     * Gets the value of the creditOverrideDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getCreditOverrideDate() {
        return creditOverrideDate;
    }

    /**
     * Sets the value of the creditOverrideDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setCreditOverrideDate(JAXBElement<XMLGregorianCalendar> value) {
        this.creditOverrideDate = value;
    }

    /**
     * Gets the value of the creditOverrideLimit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditOverrideLimit() {
        return creditOverrideLimit;
    }

    /**
     * Sets the value of the creditOverrideLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditOverrideLimit(BigDecimal value) {
        this.creditOverrideLimit = value;
    }

    /**
     * Gets the value of the creditOverrideTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCreditOverrideTime() {
        return creditOverrideTime;
    }

    /**
     * Sets the value of the creditOverrideTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCreditOverrideTime(JAXBElement<String> value) {
        this.creditOverrideTime = value;
    }

    /**
     * Gets the value of the creditOverrideUserID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCreditOverrideUserID() {
        return creditOverrideUserID;
    }

    /**
     * Sets the value of the creditOverrideUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCreditOverrideUserID(JAXBElement<String> value) {
        this.creditOverrideUserID = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the currencyCodeCurrDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCodeCurrDesc() {
        return currencyCodeCurrDesc;
    }

    /**
     * Sets the value of the currencyCodeCurrDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCodeCurrDesc(JAXBElement<String> value) {
        this.currencyCodeCurrDesc = value;
    }

    /**
     * Gets the value of the currencyCodeCurrName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCodeCurrName() {
        return currencyCodeCurrName;
    }

    /**
     * Sets the value of the currencyCodeCurrName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCodeCurrName(JAXBElement<String> value) {
        this.currencyCodeCurrName = value;
    }

    /**
     * Gets the value of the currencyCodeCurrSymbol property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCodeCurrSymbol() {
        return currencyCodeCurrSymbol;
    }

    /**
     * Sets the value of the currencyCodeCurrSymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCodeCurrSymbol(JAXBElement<String> value) {
        this.currencyCodeCurrSymbol = value;
    }

    /**
     * Gets the value of the currencyCodeCurrencyID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCodeCurrencyID() {
        return currencyCodeCurrencyID;
    }

    /**
     * Sets the value of the currencyCodeCurrencyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCodeCurrencyID(JAXBElement<String> value) {
        this.currencyCodeCurrencyID = value;
    }

    /**
     * Gets the value of the currencyCodeDocumentDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCodeDocumentDesc() {
        return currencyCodeDocumentDesc;
    }

    /**
     * Sets the value of the currencyCodeDocumentDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCodeDocumentDesc(JAXBElement<String> value) {
        this.currencyCodeDocumentDesc = value;
    }

    /**
     * Gets the value of the currencySwitch property.
     * This getter has been renamed from isCurrencySwitch() to getCurrencySwitch() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCurrencySwitch() {
        return currencySwitch;
    }

    /**
     * Sets the value of the currencySwitch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCurrencySwitch(Boolean value) {
        this.currencySwitch = value;
    }

    /**
     * Gets the value of the custAllowOTS property.
     * This getter has been renamed from isCustAllowOTS() to getCustAllowOTS() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCustAllowOTS() {
        return custAllowOTS;
    }

    /**
     * Sets the value of the custAllowOTS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCustAllowOTS(Boolean value) {
        this.custAllowOTS = value;
    }

    /**
     * Gets the value of the custNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustNum() {
        return custNum;
    }

    /**
     * Sets the value of the custNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustNum(Integer value) {
        this.custNum = value;
    }

    /**
     * Gets the value of the custOnCreditHold property.
     * This getter has been renamed from isCustOnCreditHold() to getCustOnCreditHold() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCustOnCreditHold() {
        return custOnCreditHold;
    }

    /**
     * Sets the value of the custOnCreditHold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCustOnCreditHold(Boolean value) {
        this.custOnCreditHold = value;
    }

    /**
     * Gets the value of the custTradePartnerName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustTradePartnerName() {
        return custTradePartnerName;
    }

    /**
     * Sets the value of the custTradePartnerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustTradePartnerName(JAXBElement<String> value) {
        this.custTradePartnerName = value;
    }

    /**
     * Gets the value of the customerAllowShipTo3 property.
     * This getter has been renamed from isCustomerAllowShipTo3() to getCustomerAllowShipTo3() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCustomerAllowShipTo3() {
        return customerAllowShipTo3;
    }

    /**
     * Sets the value of the customerAllowShipTo3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCustomerAllowShipTo3(Boolean value) {
        this.customerAllowShipTo3 = value;
    }

    /**
     * Gets the value of the customerBTName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerBTName() {
        return customerBTName;
    }

    /**
     * Sets the value of the customerBTName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerBTName(JAXBElement<String> value) {
        this.customerBTName = value;
    }

    /**
     * Gets the value of the customerCustID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerCustID() {
        return customerCustID;
    }

    /**
     * Sets the value of the customerCustID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerCustID(JAXBElement<String> value) {
        this.customerCustID = value;
    }

    /**
     * Gets the value of the customerName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerName() {
        return customerName;
    }

    /**
     * Sets the value of the customerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerName(JAXBElement<String> value) {
        this.customerName = value;
    }

    /**
     * Gets the value of the customerPrintAck property.
     * This getter has been renamed from isCustomerPrintAck() to getCustomerPrintAck() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCustomerPrintAck() {
        return customerPrintAck;
    }

    /**
     * Sets the value of the customerPrintAck property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCustomerPrintAck(Boolean value) {
        this.customerPrintAck = value;
    }

    /**
     * Gets the value of the customerRequiresPO property.
     * This getter has been renamed from isCustomerRequiresPO() to getCustomerRequiresPO() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCustomerRequiresPO() {
        return customerRequiresPO;
    }

    /**
     * Sets the value of the customerRequiresPO property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCustomerRequiresPO(Boolean value) {
        this.customerRequiresPO = value;
    }

    /**
     * Gets the value of the declaredAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDeclaredAmt() {
        return declaredAmt;
    }

    /**
     * Sets the value of the declaredAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDeclaredAmt(BigDecimal value) {
        this.declaredAmt = value;
    }

    /**
     * Gets the value of the declaredIns property.
     * This getter has been renamed from isDeclaredIns() to getDeclaredIns() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDeclaredIns() {
        return declaredIns;
    }

    /**
     * Sets the value of the declaredIns property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeclaredIns(Boolean value) {
        this.declaredIns = value;
    }

    /**
     * Gets the value of the deliveryConf property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDeliveryConf() {
        return deliveryConf;
    }

    /**
     * Sets the value of the deliveryConf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDeliveryConf(Integer value) {
        this.deliveryConf = value;
    }

    /**
     * Gets the value of the deliveryType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeliveryType() {
        return deliveryType;
    }

    /**
     * Sets the value of the deliveryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeliveryType(JAXBElement<String> value) {
        this.deliveryType = value;
    }

    /**
     * Gets the value of the demandContract property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDemandContract() {
        return demandContract;
    }

    /**
     * Sets the value of the demandContract property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDemandContract(JAXBElement<String> value) {
        this.demandContract = value;
    }

    /**
     * Gets the value of the demandContractNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDemandContractNum() {
        return demandContractNum;
    }

    /**
     * Sets the value of the demandContractNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDemandContractNum(Integer value) {
        this.demandContractNum = value;
    }

    /**
     * Gets the value of the demandHeadSeq property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDemandHeadSeq() {
        return demandHeadSeq;
    }

    /**
     * Sets the value of the demandHeadSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDemandHeadSeq(Integer value) {
        this.demandHeadSeq = value;
    }

    /**
     * Gets the value of the demandProcessDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDemandProcessDate() {
        return demandProcessDate;
    }

    /**
     * Sets the value of the demandProcessDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDemandProcessDate(JAXBElement<XMLGregorianCalendar> value) {
        this.demandProcessDate = value;
    }

    /**
     * Gets the value of the demandProcessTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDemandProcessTime() {
        return demandProcessTime;
    }

    /**
     * Sets the value of the demandProcessTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDemandProcessTime(Integer value) {
        this.demandProcessTime = value;
    }

    /**
     * Gets the value of the demandRejected property.
     * This getter has been renamed from isDemandRejected() to getDemandRejected() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDemandRejected() {
        return demandRejected;
    }

    /**
     * Sets the value of the demandRejected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDemandRejected(Boolean value) {
        this.demandRejected = value;
    }

    /**
     * Gets the value of the depositBal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepositBal() {
        return depositBal;
    }

    /**
     * Sets the value of the depositBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepositBal(BigDecimal value) {
        this.depositBal = value;
    }

    /**
     * Gets the value of the discountPercent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountPercent() {
        return discountPercent;
    }

    /**
     * Sets the value of the discountPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountPercent(BigDecimal value) {
        this.discountPercent = value;
    }

    /**
     * Gets the value of the dispatchReason property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDispatchReason() {
        return dispatchReason;
    }

    /**
     * Sets the value of the dispatchReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDispatchReason(JAXBElement<String> value) {
        this.dispatchReason = value;
    }

    /**
     * Gets the value of the dispatchReasonCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDispatchReasonCodeDescription() {
        return dispatchReasonCodeDescription;
    }

    /**
     * Sets the value of the dispatchReasonCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDispatchReasonCodeDescription(JAXBElement<String> value) {
        this.dispatchReasonCodeDescription = value;
    }

    /**
     * Gets the value of the doNotShipAfterDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDoNotShipAfterDate() {
        return doNotShipAfterDate;
    }

    /**
     * Sets the value of the doNotShipAfterDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDoNotShipAfterDate(JAXBElement<XMLGregorianCalendar> value) {
        this.doNotShipAfterDate = value;
    }

    /**
     * Gets the value of the doNotShipBeforeDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDoNotShipBeforeDate() {
        return doNotShipBeforeDate;
    }

    /**
     * Sets the value of the doNotShipBeforeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDoNotShipBeforeDate(JAXBElement<XMLGregorianCalendar> value) {
        this.doNotShipBeforeDate = value;
    }

    /**
     * Gets the value of the docCCRounding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocCCRounding() {
        return docCCRounding;
    }

    /**
     * Sets the value of the docCCRounding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocCCRounding(BigDecimal value) {
        this.docCCRounding = value;
    }

    /**
     * Gets the value of the docDepositBal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocDepositBal() {
        return docDepositBal;
    }

    /**
     * Sets the value of the docDepositBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocDepositBal(BigDecimal value) {
        this.docDepositBal = value;
    }

    /**
     * Gets the value of the docInTotalCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocInTotalCharges() {
        return docInTotalCharges;
    }

    /**
     * Sets the value of the docInTotalCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocInTotalCharges(BigDecimal value) {
        this.docInTotalCharges = value;
    }

    /**
     * Gets the value of the docInTotalDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocInTotalDiscount() {
        return docInTotalDiscount;
    }

    /**
     * Sets the value of the docInTotalDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocInTotalDiscount(BigDecimal value) {
        this.docInTotalDiscount = value;
    }

    /**
     * Gets the value of the docInTotalMisc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocInTotalMisc() {
        return docInTotalMisc;
    }

    /**
     * Sets the value of the docInTotalMisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocInTotalMisc(BigDecimal value) {
        this.docInTotalMisc = value;
    }

    /**
     * Gets the value of the docOnly property.
     * This getter has been renamed from isDocOnly() to getDocOnly() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDocOnly() {
        return docOnly;
    }

    /**
     * Sets the value of the docOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDocOnly(Boolean value) {
        this.docOnly = value;
    }

    /**
     * Gets the value of the docOrderAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocOrderAmt() {
        return docOrderAmt;
    }

    /**
     * Sets the value of the docOrderAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocOrderAmt(BigDecimal value) {
        this.docOrderAmt = value;
    }

    /**
     * Gets the value of the docRounding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocRounding() {
        return docRounding;
    }

    /**
     * Sets the value of the docRounding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocRounding(BigDecimal value) {
        this.docRounding = value;
    }

    /**
     * Gets the value of the docTotalAdvBill property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTotalAdvBill() {
        return docTotalAdvBill;
    }

    /**
     * Sets the value of the docTotalAdvBill property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTotalAdvBill(BigDecimal value) {
        this.docTotalAdvBill = value;
    }

    /**
     * Gets the value of the docTotalCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTotalCharges() {
        return docTotalCharges;
    }

    /**
     * Sets the value of the docTotalCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTotalCharges(BigDecimal value) {
        this.docTotalCharges = value;
    }

    /**
     * Gets the value of the docTotalComm property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTotalComm() {
        return docTotalComm;
    }

    /**
     * Sets the value of the docTotalComm property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTotalComm(BigDecimal value) {
        this.docTotalComm = value;
    }

    /**
     * Gets the value of the docTotalDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTotalDiscount() {
        return docTotalDiscount;
    }

    /**
     * Sets the value of the docTotalDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTotalDiscount(BigDecimal value) {
        this.docTotalDiscount = value;
    }

    /**
     * Gets the value of the docTotalMisc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTotalMisc() {
        return docTotalMisc;
    }

    /**
     * Sets the value of the docTotalMisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTotalMisc(BigDecimal value) {
        this.docTotalMisc = value;
    }

    /**
     * Gets the value of the docTotalNet property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTotalNet() {
        return docTotalNet;
    }

    /**
     * Sets the value of the docTotalNet property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTotalNet(BigDecimal value) {
        this.docTotalNet = value;
    }

    /**
     * Gets the value of the docTotalOrder property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTotalOrder() {
        return docTotalOrder;
    }

    /**
     * Sets the value of the docTotalOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTotalOrder(BigDecimal value) {
        this.docTotalOrder = value;
    }

    /**
     * Gets the value of the docTotalSATax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTotalSATax() {
        return docTotalSATax;
    }

    /**
     * Sets the value of the docTotalSATax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTotalSATax(BigDecimal value) {
        this.docTotalSATax = value;
    }

    /**
     * Gets the value of the docTotalTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTotalTax() {
        return docTotalTax;
    }

    /**
     * Sets the value of the docTotalTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTotalTax(BigDecimal value) {
        this.docTotalTax = value;
    }

    /**
     * Gets the value of the docTotalWHTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTotalWHTax() {
        return docTotalWHTax;
    }

    /**
     * Sets the value of the docTotalWHTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTotalWHTax(BigDecimal value) {
        this.docTotalWHTax = value;
    }

    /**
     * Gets the value of the documentTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDocumentTypeID() {
        return documentTypeID;
    }

    /**
     * Sets the value of the documentTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDocumentTypeID(JAXBElement<String> value) {
        this.documentTypeID = value;
    }

    /**
     * Gets the value of the dropShip property.
     * This getter has been renamed from isDropShip() to getDropShip() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDropShip() {
        return dropShip;
    }

    /**
     * Sets the value of the dropShip property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDropShip(Boolean value) {
        this.dropShip = value;
    }

    /**
     * Gets the value of the eccOrderNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getECCOrderNum() {
        return eccOrderNum;
    }

    /**
     * Sets the value of the eccOrderNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setECCOrderNum(JAXBElement<String> value) {
        this.eccOrderNum = value;
    }

    /**
     * Gets the value of the eccpoNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getECCPONum() {
        return eccpoNum;
    }

    /**
     * Sets the value of the eccpoNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setECCPONum(JAXBElement<String> value) {
        this.eccpoNum = value;
    }

    /**
     * Gets the value of the eccPaymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getECCPaymentMethod() {
        return eccPaymentMethod;
    }

    /**
     * Sets the value of the eccPaymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setECCPaymentMethod(JAXBElement<String> value) {
        this.eccPaymentMethod = value;
    }

    /**
     * Gets the value of the ediAck property.
     * This getter has been renamed from isEDIAck() to getEDIAck() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEDIAck() {
        return ediAck;
    }

    /**
     * Sets the value of the ediAck property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEDIAck(Boolean value) {
        this.ediAck = value;
    }

    /**
     * Gets the value of the ediOrder property.
     * This getter has been renamed from isEDIOrder() to getEDIOrder() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEDIOrder() {
        return ediOrder;
    }

    /**
     * Sets the value of the ediOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEDIOrder(Boolean value) {
        this.ediOrder = value;
    }

    /**
     * Gets the value of the ediReady property.
     * This getter has been renamed from isEDIReady() to getEDIReady() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEDIReady() {
        return ediReady;
    }

    /**
     * Sets the value of the ediReady property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEDIReady(Boolean value) {
        this.ediReady = value;
    }

    /**
     * Gets the value of the ersOrder property.
     * This getter has been renamed from isERSOrder() to getERSOrder() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getERSOrder() {
        return ersOrder;
    }

    /**
     * Sets the value of the ersOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setERSOrder(Boolean value) {
        this.ersOrder = value;
    }

    /**
     * Gets the value of the ersOverride property.
     * This getter has been renamed from isERSOverride() to getERSOverride() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getERSOverride() {
        return ersOverride;
    }

    /**
     * Sets the value of the ersOverride property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setERSOverride(Boolean value) {
        this.ersOverride = value;
    }

    /**
     * Gets the value of the enableJobWizard property.
     * This getter has been renamed from isEnableJobWizard() to getEnableJobWizard() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnableJobWizard() {
        return enableJobWizard;
    }

    /**
     * Sets the value of the enableJobWizard property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableJobWizard(Boolean value) {
        this.enableJobWizard = value;
    }

    /**
     * Gets the value of the enableSoldToID property.
     * This getter has been renamed from isEnableSoldToID() to getEnableSoldToID() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnableSoldToID() {
        return enableSoldToID;
    }

    /**
     * Sets the value of the enableSoldToID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSoldToID(Boolean value) {
        this.enableSoldToID = value;
    }

    /**
     * Gets the value of the entryMethod property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEntryMethod() {
        return entryMethod;
    }

    /**
     * Sets the value of the entryMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEntryMethod(JAXBElement<String> value) {
        this.entryMethod = value;
    }

    /**
     * Gets the value of the entryPerson property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEntryPerson() {
        return entryPerson;
    }

    /**
     * Sets the value of the entryPerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEntryPerson(JAXBElement<String> value) {
        this.entryPerson = value;
    }

    /**
     * Gets the value of the entryProcess property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEntryProcess() {
        return entryProcess;
    }

    /**
     * Sets the value of the entryProcess property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEntryProcess(JAXBElement<String> value) {
        this.entryProcess = value;
    }

    /**
     * Gets the value of the exchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    /**
     * Sets the value of the exchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExchangeRate(BigDecimal value) {
        this.exchangeRate = value;
    }

    /**
     * Gets the value of the expirationMonth property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExpirationMonth() {
        return expirationMonth;
    }

    /**
     * Sets the value of the expirationMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExpirationMonth(Integer value) {
        this.expirationMonth = value;
    }

    /**
     * Gets the value of the expirationYear property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExpirationYear() {
        return expirationYear;
    }

    /**
     * Sets the value of the expirationYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExpirationYear(Integer value) {
        this.expirationYear = value;
    }

    /**
     * Gets the value of the extCompany property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExtCompany() {
        return extCompany;
    }

    /**
     * Sets the value of the extCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExtCompany(JAXBElement<String> value) {
        this.extCompany = value;
    }

    /**
     * Gets the value of the ffAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFFAddress1() {
        return ffAddress1;
    }

    /**
     * Sets the value of the ffAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFFAddress1(JAXBElement<String> value) {
        this.ffAddress1 = value;
    }

    /**
     * Gets the value of the ffAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFFAddress2() {
        return ffAddress2;
    }

    /**
     * Sets the value of the ffAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFFAddress2(JAXBElement<String> value) {
        this.ffAddress2 = value;
    }

    /**
     * Gets the value of the ffAddress3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFFAddress3() {
        return ffAddress3;
    }

    /**
     * Sets the value of the ffAddress3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFFAddress3(JAXBElement<String> value) {
        this.ffAddress3 = value;
    }

    /**
     * Gets the value of the ffCity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFFCity() {
        return ffCity;
    }

    /**
     * Sets the value of the ffCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFFCity(JAXBElement<String> value) {
        this.ffCity = value;
    }

    /**
     * Gets the value of the ffCompName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFFCompName() {
        return ffCompName;
    }

    /**
     * Sets the value of the ffCompName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFFCompName(JAXBElement<String> value) {
        this.ffCompName = value;
    }

    /**
     * Gets the value of the ffContact property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFFContact() {
        return ffContact;
    }

    /**
     * Sets the value of the ffContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFFContact(JAXBElement<String> value) {
        this.ffContact = value;
    }

    /**
     * Gets the value of the ffCountry property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFFCountry() {
        return ffCountry;
    }

    /**
     * Sets the value of the ffCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFFCountry(JAXBElement<String> value) {
        this.ffCountry = value;
    }

    /**
     * Gets the value of the ffCountryNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFFCountryNum() {
        return ffCountryNum;
    }

    /**
     * Sets the value of the ffCountryNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFFCountryNum(Integer value) {
        this.ffCountryNum = value;
    }

    /**
     * Gets the value of the ffid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFFID() {
        return ffid;
    }

    /**
     * Sets the value of the ffid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFFID(JAXBElement<String> value) {
        this.ffid = value;
    }

    /**
     * Gets the value of the ffPhoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFFPhoneNum() {
        return ffPhoneNum;
    }

    /**
     * Sets the value of the ffPhoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFFPhoneNum(JAXBElement<String> value) {
        this.ffPhoneNum = value;
    }

    /**
     * Gets the value of the ffState property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFFState() {
        return ffState;
    }

    /**
     * Sets the value of the ffState property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFFState(JAXBElement<String> value) {
        this.ffState = value;
    }

    /**
     * Gets the value of the ffZip property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFFZip() {
        return ffZip;
    }

    /**
     * Sets the value of the ffZip property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFFZip(JAXBElement<String> value) {
        this.ffZip = value;
    }

    /**
     * Gets the value of the fob property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFOB() {
        return fob;
    }

    /**
     * Sets the value of the fob property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFOB(JAXBElement<String> value) {
        this.fob = value;
    }

    /**
     * Gets the value of the fobDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFOBDescription() {
        return fobDescription;
    }

    /**
     * Sets the value of the fobDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFOBDescription(JAXBElement<String> value) {
        this.fobDescription = value;
    }

    /**
     * Gets the value of the groundType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGroundType() {
        return groundType;
    }

    /**
     * Sets the value of the groundType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGroundType(JAXBElement<String> value) {
        this.groundType = value;
    }

    /**
     * Gets the value of the hdCaseDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHDCaseDescription() {
        return hdCaseDescription;
    }

    /**
     * Sets the value of the hdCaseDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHDCaseDescription(JAXBElement<String> value) {
        this.hdCaseDescription = value;
    }

    /**
     * Gets the value of the hdCaseNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHDCaseNum() {
        return hdCaseNum;
    }

    /**
     * Sets the value of the hdCaseNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHDCaseNum(Integer value) {
        this.hdCaseNum = value;
    }

    /**
     * Gets the value of the hasMiscCharges property.
     * This getter has been renamed from isHasMiscCharges() to getHasMiscCharges() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHasMiscCharges() {
        return hasMiscCharges;
    }

    /**
     * Sets the value of the hasMiscCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasMiscCharges(Boolean value) {
        this.hasMiscCharges = value;
    }

    /**
     * Gets the value of the hasOrderLines property.
     * This getter has been renamed from isHasOrderLines() to getHasOrderLines() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHasOrderLines() {
        return hasOrderLines;
    }

    /**
     * Sets the value of the hasOrderLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasOrderLines(Boolean value) {
        this.hasOrderLines = value;
    }

    /**
     * Gets the value of the hazmat property.
     * This getter has been renamed from isHazmat() to getHazmat() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHazmat() {
        return hazmat;
    }

    /**
     * Sets the value of the hazmat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHazmat(Boolean value) {
        this.hazmat = value;
    }

    /**
     * Gets the value of the holdSetByDemand property.
     * This getter has been renamed from isHoldSetByDemand() to getHoldSetByDemand() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHoldSetByDemand() {
        return holdSetByDemand;
    }

    /**
     * Sets the value of the holdSetByDemand property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHoldSetByDemand(Boolean value) {
        this.holdSetByDemand = value;
    }

    /**
     * Gets the value of the icpoNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getICPONum() {
        return icpoNum;
    }

    /**
     * Sets the value of the icpoNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setICPONum(Integer value) {
        this.icpoNum = value;
    }

    /**
     * Gets the value of the inPrice property.
     * This getter has been renamed from isInPrice() to getInPrice() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getInPrice() {
        return inPrice;
    }

    /**
     * Sets the value of the inPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInPrice(Boolean value) {
        this.inPrice = value;
    }

    /**
     * Gets the value of the inTotalCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInTotalCharges() {
        return inTotalCharges;
    }

    /**
     * Sets the value of the inTotalCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInTotalCharges(BigDecimal value) {
        this.inTotalCharges = value;
    }

    /**
     * Gets the value of the inTotalDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInTotalDiscount() {
        return inTotalDiscount;
    }

    /**
     * Sets the value of the inTotalDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInTotalDiscount(BigDecimal value) {
        this.inTotalDiscount = value;
    }

    /**
     * Gets the value of the inTotalMisc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInTotalMisc() {
        return inTotalMisc;
    }

    /**
     * Sets the value of the inTotalMisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInTotalMisc(BigDecimal value) {
        this.inTotalMisc = value;
    }

    /**
     * Gets the value of the individualPackIDs property.
     * This getter has been renamed from isIndividualPackIDs() to getIndividualPackIDs() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIndividualPackIDs() {
        return individualPackIDs;
    }

    /**
     * Sets the value of the individualPackIDs property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIndividualPackIDs(Boolean value) {
        this.individualPackIDs = value;
    }

    /**
     * Gets the value of the intrntlShip property.
     * This getter has been renamed from isIntrntlShip() to getIntrntlShip() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIntrntlShip() {
        return intrntlShip;
    }

    /**
     * Sets the value of the intrntlShip property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIntrntlShip(Boolean value) {
        this.intrntlShip = value;
    }

    /**
     * Gets the value of the invCurrCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInvCurrCode() {
        return invCurrCode;
    }

    /**
     * Sets the value of the invCurrCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInvCurrCode(JAXBElement<String> value) {
        this.invCurrCode = value;
    }

    /**
     * Gets the value of the invCurrCurrDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInvCurrCurrDesc() {
        return invCurrCurrDesc;
    }

    /**
     * Sets the value of the invCurrCurrDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInvCurrCurrDesc(JAXBElement<String> value) {
        this.invCurrCurrDesc = value;
    }

    /**
     * Gets the value of the invcOrderCmp property.
     * This getter has been renamed from isInvcOrderCmp() to getInvcOrderCmp() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getInvcOrderCmp() {
        return invcOrderCmp;
    }

    /**
     * Sets the value of the invcOrderCmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInvcOrderCmp(Boolean value) {
        this.invcOrderCmp = value;
    }

    /**
     * Gets the value of the invoiceComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInvoiceComment() {
        return invoiceComment;
    }

    /**
     * Sets the value of the invoiceComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInvoiceComment(JAXBElement<String> value) {
        this.invoiceComment = value;
    }

    /**
     * Gets the value of the isCSRSet property.
     * This getter has been renamed from isIsCSRSet() to getIsCSRSet() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsCSRSet() {
        return isCSRSet;
    }

    /**
     * Sets the value of the isCSRSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCSRSet(Boolean value) {
        this.isCSRSet = value;
    }

    /**
     * Gets the value of the locHold property.
     * This getter has been renamed from isLOCHold() to getLOCHold() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLOCHold() {
        return locHold;
    }

    /**
     * Sets the value of the locHold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLOCHold(Boolean value) {
        this.locHold = value;
    }

    /**
     * Gets the value of the lastBatchNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLastBatchNum() {
        return lastBatchNum;
    }

    /**
     * Sets the value of the lastBatchNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLastBatchNum(JAXBElement<String> value) {
        this.lastBatchNum = value;
    }

    /**
     * Gets the value of the lastScheduleNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLastScheduleNumber() {
        return lastScheduleNumber;
    }

    /**
     * Sets the value of the lastScheduleNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLastScheduleNumber(JAXBElement<String> value) {
        this.lastScheduleNumber = value;
    }

    /**
     * Gets the value of the lastTCtrlNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLastTCtrlNum() {
        return lastTCtrlNum;
    }

    /**
     * Sets the value of the lastTCtrlNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLastTCtrlNum(JAXBElement<String> value) {
        this.lastTCtrlNum = value;
    }

    /**
     * Gets the value of the legalNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLegalNumber() {
        return legalNumber;
    }

    /**
     * Sets the value of the legalNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLegalNumber(JAXBElement<String> value) {
        this.legalNumber = value;
    }

    /**
     * Gets the value of the letterOfInstr property.
     * This getter has been renamed from isLetterOfInstr() to getLetterOfInstr() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLetterOfInstr() {
        return letterOfInstr;
    }

    /**
     * Sets the value of the letterOfInstr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLetterOfInstr(Boolean value) {
        this.letterOfInstr = value;
    }

    /**
     * Gets the value of the linkMsg property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLinkMsg() {
        return linkMsg;
    }

    /**
     * Sets the value of the linkMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLinkMsg(JAXBElement<String> value) {
        this.linkMsg = value;
    }

    /**
     * Gets the value of the linked property.
     * This getter has been renamed from isLinked() to getLinked() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLinked() {
        return linked;
    }

    /**
     * Sets the value of the linked property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLinked(Boolean value) {
        this.linked = value;
    }

    /**
     * Gets the value of the lockQty property.
     * This getter has been renamed from isLockQty() to getLockQty() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLockQty() {
        return lockQty;
    }

    /**
     * Sets the value of the lockQty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLockQty(Boolean value) {
        this.lockQty = value;
    }

    /**
     * Gets the value of the lockRate property.
     * This getter has been renamed from isLockRate() to getLockRate() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLockRate() {
        return lockRate;
    }

    /**
     * Sets the value of the lockRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLockRate(Boolean value) {
        this.lockRate = value;
    }

    /**
     * Gets the value of the needByDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getNeedByDate() {
        return needByDate;
    }

    /**
     * Sets the value of the needByDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setNeedByDate(JAXBElement<XMLGregorianCalendar> value) {
        this.needByDate = value;
    }

    /**
     * Gets the value of the nonStdPkg property.
     * This getter has been renamed from isNonStdPkg() to getNonStdPkg() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNonStdPkg() {
        return nonStdPkg;
    }

    /**
     * Sets the value of the nonStdPkg property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNonStdPkg(Boolean value) {
        this.nonStdPkg = value;
    }

    /**
     * Gets the value of the notifyEMail property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNotifyEMail() {
        return notifyEMail;
    }

    /**
     * Sets the value of the notifyEMail property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNotifyEMail(JAXBElement<String> value) {
        this.notifyEMail = value;
    }

    /**
     * Gets the value of the notifyFlag property.
     * This getter has been renamed from isNotifyFlag() to getNotifyFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNotifyFlag() {
        return notifyFlag;
    }

    /**
     * Sets the value of the notifyFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyFlag(Boolean value) {
        this.notifyFlag = value;
    }

    /**
     * Gets the value of the otsAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSAddress1() {
        return otsAddress1;
    }

    /**
     * Sets the value of the otsAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSAddress1(JAXBElement<String> value) {
        this.otsAddress1 = value;
    }

    /**
     * Gets the value of the otsAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSAddress2() {
        return otsAddress2;
    }

    /**
     * Sets the value of the otsAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSAddress2(JAXBElement<String> value) {
        this.otsAddress2 = value;
    }

    /**
     * Gets the value of the otsAddress3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSAddress3() {
        return otsAddress3;
    }

    /**
     * Sets the value of the otsAddress3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSAddress3(JAXBElement<String> value) {
        this.otsAddress3 = value;
    }

    /**
     * Gets the value of the otsCity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSCity() {
        return otsCity;
    }

    /**
     * Sets the value of the otsCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSCity(JAXBElement<String> value) {
        this.otsCity = value;
    }

    /**
     * Gets the value of the otsCntryDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSCntryDescription() {
        return otsCntryDescription;
    }

    /**
     * Sets the value of the otsCntryDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSCntryDescription(JAXBElement<String> value) {
        this.otsCntryDescription = value;
    }

    /**
     * Gets the value of the otsContact property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSContact() {
        return otsContact;
    }

    /**
     * Sets the value of the otsContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSContact(JAXBElement<String> value) {
        this.otsContact = value;
    }

    /**
     * Gets the value of the otsCountryNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOTSCountryNum() {
        return otsCountryNum;
    }

    /**
     * Sets the value of the otsCountryNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOTSCountryNum(Integer value) {
        this.otsCountryNum = value;
    }

    /**
     * Gets the value of the otsCustSaved property.
     * This getter has been renamed from isOTSCustSaved() to getOTSCustSaved() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOTSCustSaved() {
        return otsCustSaved;
    }

    /**
     * Sets the value of the otsCustSaved property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOTSCustSaved(Boolean value) {
        this.otsCustSaved = value;
    }

    /**
     * Gets the value of the otsFaxNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSFaxNum() {
        return otsFaxNum;
    }

    /**
     * Sets the value of the otsFaxNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSFaxNum(JAXBElement<String> value) {
        this.otsFaxNum = value;
    }

    /**
     * Gets the value of the otsName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSName() {
        return otsName;
    }

    /**
     * Sets the value of the otsName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSName(JAXBElement<String> value) {
        this.otsName = value;
    }

    /**
     * Gets the value of the otsPhoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSPhoneNum() {
        return otsPhoneNum;
    }

    /**
     * Sets the value of the otsPhoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSPhoneNum(JAXBElement<String> value) {
        this.otsPhoneNum = value;
    }

    /**
     * Gets the value of the otsResaleID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSResaleID() {
        return otsResaleID;
    }

    /**
     * Sets the value of the otsResaleID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSResaleID(JAXBElement<String> value) {
        this.otsResaleID = value;
    }

    /**
     * Gets the value of the otsSaveAs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSSaveAs() {
        return otsSaveAs;
    }

    /**
     * Sets the value of the otsSaveAs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSSaveAs(JAXBElement<String> value) {
        this.otsSaveAs = value;
    }

    /**
     * Gets the value of the otsSaveCustID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSSaveCustID() {
        return otsSaveCustID;
    }

    /**
     * Sets the value of the otsSaveCustID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSSaveCustID(JAXBElement<String> value) {
        this.otsSaveCustID = value;
    }

    /**
     * Gets the value of the otsSaved property.
     * This getter has been renamed from isOTSSaved() to getOTSSaved() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOTSSaved() {
        return otsSaved;
    }

    /**
     * Sets the value of the otsSaved property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOTSSaved(Boolean value) {
        this.otsSaved = value;
    }

    /**
     * Gets the value of the otsShipToNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSShipToNum() {
        return otsShipToNum;
    }

    /**
     * Sets the value of the otsShipToNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSShipToNum(JAXBElement<String> value) {
        this.otsShipToNum = value;
    }

    /**
     * Gets the value of the otsState property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSState() {
        return otsState;
    }

    /**
     * Sets the value of the otsState property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSState(JAXBElement<String> value) {
        this.otsState = value;
    }

    /**
     * Gets the value of the otsTaxRegionCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSTaxRegionCode() {
        return otsTaxRegionCode;
    }

    /**
     * Sets the value of the otsTaxRegionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSTaxRegionCode(JAXBElement<String> value) {
        this.otsTaxRegionCode = value;
    }

    /**
     * Gets the value of the otszip property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOTSZIP() {
        return otszip;
    }

    /**
     * Sets the value of the otszip property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOTSZIP(JAXBElement<String> value) {
        this.otszip = value;
    }

    /**
     * Gets the value of the openOrder property.
     * This getter has been renamed from isOpenOrder() to getOpenOrder() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOpenOrder() {
        return openOrder;
    }

    /**
     * Sets the value of the openOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpenOrder(Boolean value) {
        this.openOrder = value;
    }

    /**
     * Gets the value of the orderAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrderAmt() {
        return orderAmt;
    }

    /**
     * Sets the value of the orderAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrderAmt(BigDecimal value) {
        this.orderAmt = value;
    }

    /**
     * Gets the value of the orderCSR property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderCSR() {
        return orderCSR;
    }

    /**
     * Sets the value of the orderCSR property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderCSR(JAXBElement<String> value) {
        this.orderCSR = value;
    }

    /**
     * Gets the value of the orderComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderComment() {
        return orderComment;
    }

    /**
     * Sets the value of the orderComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderComment(JAXBElement<String> value) {
        this.orderComment = value;
    }

    /**
     * Gets the value of the orderDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getOrderDate() {
        return orderDate;
    }

    /**
     * Sets the value of the orderDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setOrderDate(JAXBElement<XMLGregorianCalendar> value) {
        this.orderDate = value;
    }

    /**
     * Gets the value of the orderHeld property.
     * This getter has been renamed from isOrderHeld() to getOrderHeld() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOrderHeld() {
        return orderHeld;
    }

    /**
     * Sets the value of the orderHeld property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOrderHeld(Boolean value) {
        this.orderHeld = value;
    }

    /**
     * Gets the value of the orderNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * Sets the value of the orderNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNum(Integer value) {
        this.orderNum = value;
    }

    /**
     * Gets the value of the orderStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrderStatus() {
        return orderStatus;
    }

    /**
     * Sets the value of the orderStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrderStatus(JAXBElement<String> value) {
        this.orderStatus = value;
    }

    /**
     * Gets the value of the ourBank property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOurBank() {
        return ourBank;
    }

    /**
     * Sets the value of the ourBank property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOurBank(JAXBElement<String> value) {
        this.ourBank = value;
    }

    /**
     * Gets the value of the ourBankBankName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOurBankBankName() {
        return ourBankBankName;
    }

    /**
     * Sets the value of the ourBankBankName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOurBankBankName(JAXBElement<String> value) {
        this.ourBankBankName = value;
    }

    /**
     * Gets the value of the ourBankDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOurBankDescription() {
        return ourBankDescription;
    }

    /**
     * Sets the value of the ourBankDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOurBankDescription(JAXBElement<String> value) {
        this.ourBankDescription = value;
    }

    /**
     * Gets the value of the outboundSalesDocCtr property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOutboundSalesDocCtr() {
        return outboundSalesDocCtr;
    }

    /**
     * Sets the value of the outboundSalesDocCtr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOutboundSalesDocCtr(Integer value) {
        this.outboundSalesDocCtr = value;
    }

    /**
     * Gets the value of the outboundShipDocsCtr property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOutboundShipDocsCtr() {
        return outboundShipDocsCtr;
    }

    /**
     * Sets the value of the outboundShipDocsCtr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOutboundShipDocsCtr(Integer value) {
        this.outboundShipDocsCtr = value;
    }

    /**
     * Gets the value of the overrideCarrier property.
     * This getter has been renamed from isOverrideCarrier() to getOverrideCarrier() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOverrideCarrier() {
        return overrideCarrier;
    }

    /**
     * Sets the value of the overrideCarrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverrideCarrier(Boolean value) {
        this.overrideCarrier = value;
    }

    /**
     * Gets the value of the overrideService property.
     * This getter has been renamed from isOverrideService() to getOverrideService() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOverrideService() {
        return overrideService;
    }

    /**
     * Sets the value of the overrideService property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverrideService(Boolean value) {
        this.overrideService = value;
    }

    /**
     * Gets the value of the poNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPONum() {
        return poNum;
    }

    /**
     * Sets the value of the poNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPONum(JAXBElement<String> value) {
        this.poNum = value;
    }

    /**
     * Gets the value of the psCurrCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPSCurrCode() {
        return psCurrCode;
    }

    /**
     * Sets the value of the psCurrCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPSCurrCode(JAXBElement<String> value) {
        this.psCurrCode = value;
    }

    /**
     * Gets the value of the psCurrCurrDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPSCurrCurrDesc() {
        return psCurrCurrDesc;
    }

    /**
     * Sets the value of the psCurrCurrDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPSCurrCurrDesc(JAXBElement<String> value) {
        this.psCurrCurrDesc = value;
    }

    /**
     * Gets the value of the parentCustNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getParentCustNum() {
        return parentCustNum;
    }

    /**
     * Sets the value of the parentCustNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setParentCustNum(Integer value) {
        this.parentCustNum = value;
    }

    /**
     * Gets the value of the payAccount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayAccount() {
        return payAccount;
    }

    /**
     * Sets the value of the payAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayAccount(JAXBElement<String> value) {
        this.payAccount = value;
    }

    /**
     * Gets the value of the payBTAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayBTAddress1() {
        return payBTAddress1;
    }

    /**
     * Sets the value of the payBTAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayBTAddress1(JAXBElement<String> value) {
        this.payBTAddress1 = value;
    }

    /**
     * Gets the value of the payBTAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayBTAddress2() {
        return payBTAddress2;
    }

    /**
     * Sets the value of the payBTAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayBTAddress2(JAXBElement<String> value) {
        this.payBTAddress2 = value;
    }

    /**
     * Gets the value of the payBTAddress3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayBTAddress3() {
        return payBTAddress3;
    }

    /**
     * Sets the value of the payBTAddress3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayBTAddress3(JAXBElement<String> value) {
        this.payBTAddress3 = value;
    }

    /**
     * Gets the value of the payBTCity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayBTCity() {
        return payBTCity;
    }

    /**
     * Sets the value of the payBTCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayBTCity(JAXBElement<String> value) {
        this.payBTCity = value;
    }

    /**
     * Gets the value of the payBTCountry property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayBTCountry() {
        return payBTCountry;
    }

    /**
     * Sets the value of the payBTCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayBTCountry(JAXBElement<String> value) {
        this.payBTCountry = value;
    }

    /**
     * Gets the value of the payBTCountryNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPayBTCountryNum() {
        return payBTCountryNum;
    }

    /**
     * Sets the value of the payBTCountryNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPayBTCountryNum(Integer value) {
        this.payBTCountryNum = value;
    }

    /**
     * Gets the value of the payBTPhone property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayBTPhone() {
        return payBTPhone;
    }

    /**
     * Sets the value of the payBTPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayBTPhone(JAXBElement<String> value) {
        this.payBTPhone = value;
    }

    /**
     * Gets the value of the payBTState property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayBTState() {
        return payBTState;
    }

    /**
     * Sets the value of the payBTState property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayBTState(JAXBElement<String> value) {
        this.payBTState = value;
    }

    /**
     * Gets the value of the payBTZip property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayBTZip() {
        return payBTZip;
    }

    /**
     * Sets the value of the payBTZip property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayBTZip(JAXBElement<String> value) {
        this.payBTZip = value;
    }

    /**
     * Gets the value of the payFlag property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayFlag() {
        return payFlag;
    }

    /**
     * Sets the value of the payFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayFlag(JAXBElement<String> value) {
        this.payFlag = value;
    }

    /**
     * Gets the value of the pickListComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPickListComment() {
        return pickListComment;
    }

    /**
     * Sets the value of the pickListComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPickListComment(JAXBElement<String> value) {
        this.pickListComment = value;
    }

    /**
     * Gets the value of the plant property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlant() {
        return plant;
    }

    /**
     * Sets the value of the plant property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlant(JAXBElement<String> value) {
        this.plant = value;
    }

    /**
     * Gets the value of the prcConNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPrcConNum() {
        return prcConNum;
    }

    /**
     * Sets the value of the prcConNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPrcConNum(Integer value) {
        this.prcConNum = value;
    }

    /**
     * Gets the value of the proFormaInvComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProFormaInvComment() {
        return proFormaInvComment;
    }

    /**
     * Sets the value of the proFormaInvComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProFormaInvComment(JAXBElement<String> value) {
        this.proFormaInvComment = value;
    }

    /**
     * Gets the value of the processCard property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProcessCard() {
        return processCard;
    }

    /**
     * Sets the value of the processCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProcessCard(JAXBElement<String> value) {
        this.processCard = value;
    }

    /**
     * Gets the value of the rateGrpCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRateGrpCode() {
        return rateGrpCode;
    }

    /**
     * Sets the value of the rateGrpCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRateGrpCode(JAXBElement<String> value) {
        this.rateGrpCode = value;
    }

    /**
     * Gets the value of the rateGrpDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRateGrpDescription() {
        return rateGrpDescription;
    }

    /**
     * Sets the value of the rateGrpDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRateGrpDescription(JAXBElement<String> value) {
        this.rateGrpDescription = value;
    }

    /**
     * Gets the value of the readyToCalc property.
     * This getter has been renamed from isReadyToCalc() to getReadyToCalc() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getReadyToCalc() {
        return readyToCalc;
    }

    /**
     * Sets the value of the readyToCalc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReadyToCalc(Boolean value) {
        this.readyToCalc = value;
    }

    /**
     * Gets the value of the refNotes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRefNotes() {
        return refNotes;
    }

    /**
     * Sets the value of the refNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRefNotes(JAXBElement<String> value) {
        this.refNotes = value;
    }

    /**
     * Gets the value of the referencePNRef property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReferencePNRef() {
        return referencePNRef;
    }

    /**
     * Sets the value of the referencePNRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReferencePNRef(JAXBElement<String> value) {
        this.referencePNRef = value;
    }

    /**
     * Gets the value of the repRate1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRepRate1() {
        return repRate1;
    }

    /**
     * Sets the value of the repRate1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRepRate1(BigDecimal value) {
        this.repRate1 = value;
    }

    /**
     * Gets the value of the repRate2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRepRate2() {
        return repRate2;
    }

    /**
     * Sets the value of the repRate2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRepRate2(BigDecimal value) {
        this.repRate2 = value;
    }

    /**
     * Gets the value of the repRate3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRepRate3() {
        return repRate3;
    }

    /**
     * Sets the value of the repRate3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRepRate3(BigDecimal value) {
        this.repRate3 = value;
    }

    /**
     * Gets the value of the repRate4 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRepRate4() {
        return repRate4;
    }

    /**
     * Sets the value of the repRate4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRepRate4(BigDecimal value) {
        this.repRate4 = value;
    }

    /**
     * Gets the value of the repRate5 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRepRate5() {
        return repRate5;
    }

    /**
     * Sets the value of the repRate5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRepRate5(BigDecimal value) {
        this.repRate5 = value;
    }

    /**
     * Gets the value of the repSplit1 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRepSplit1() {
        return repSplit1;
    }

    /**
     * Sets the value of the repSplit1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRepSplit1(Integer value) {
        this.repSplit1 = value;
    }

    /**
     * Gets the value of the repSplit2 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRepSplit2() {
        return repSplit2;
    }

    /**
     * Sets the value of the repSplit2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRepSplit2(Integer value) {
        this.repSplit2 = value;
    }

    /**
     * Gets the value of the repSplit3 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRepSplit3() {
        return repSplit3;
    }

    /**
     * Sets the value of the repSplit3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRepSplit3(Integer value) {
        this.repSplit3 = value;
    }

    /**
     * Gets the value of the repSplit4 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRepSplit4() {
        return repSplit4;
    }

    /**
     * Sets the value of the repSplit4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRepSplit4(Integer value) {
        this.repSplit4 = value;
    }

    /**
     * Gets the value of the repSplit5 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRepSplit5() {
        return repSplit5;
    }

    /**
     * Sets the value of the repSplit5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRepSplit5(Integer value) {
        this.repSplit5 = value;
    }

    /**
     * Gets the value of the requestDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getRequestDate() {
        return requestDate;
    }

    /**
     * Sets the value of the requestDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setRequestDate(JAXBElement<XMLGregorianCalendar> value) {
        this.requestDate = value;
    }

    /**
     * Gets the value of the resDelivery property.
     * This getter has been renamed from isResDelivery() to getResDelivery() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getResDelivery() {
        return resDelivery;
    }

    /**
     * Sets the value of the resDelivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setResDelivery(Boolean value) {
        this.resDelivery = value;
    }

    /**
     * Gets the value of the reservePriDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReservePriDescription() {
        return reservePriDescription;
    }

    /**
     * Sets the value of the reservePriDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReservePriDescription(JAXBElement<String> value) {
        this.reservePriDescription = value;
    }

    /**
     * Gets the value of the reservePriorityCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReservePriorityCode() {
        return reservePriorityCode;
    }

    /**
     * Sets the value of the reservePriorityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReservePriorityCode(JAXBElement<String> value) {
        this.reservePriorityCode = value;
    }

    /**
     * Gets the value of the rounding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRounding() {
        return rounding;
    }

    /**
     * Sets the value of the rounding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRounding(BigDecimal value) {
        this.rounding = value;
    }

    /**
     * Gets the value of the rpt1CCAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1CCAmount() {
        return rpt1CCAmount;
    }

    /**
     * Sets the value of the rpt1CCAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1CCAmount(BigDecimal value) {
        this.rpt1CCAmount = value;
    }

    /**
     * Gets the value of the rpt1CCFreight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1CCFreight() {
        return rpt1CCFreight;
    }

    /**
     * Sets the value of the rpt1CCFreight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1CCFreight(BigDecimal value) {
        this.rpt1CCFreight = value;
    }

    /**
     * Gets the value of the rpt1CCRounding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1CCRounding() {
        return rpt1CCRounding;
    }

    /**
     * Sets the value of the rpt1CCRounding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1CCRounding(BigDecimal value) {
        this.rpt1CCRounding = value;
    }

    /**
     * Gets the value of the rpt1CCTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1CCTax() {
        return rpt1CCTax;
    }

    /**
     * Sets the value of the rpt1CCTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1CCTax(BigDecimal value) {
        this.rpt1CCTax = value;
    }

    /**
     * Gets the value of the rpt1CCTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1CCTotal() {
        return rpt1CCTotal;
    }

    /**
     * Sets the value of the rpt1CCTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1CCTotal(BigDecimal value) {
        this.rpt1CCTotal = value;
    }

    /**
     * Gets the value of the rpt1DepositBal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1DepositBal() {
        return rpt1DepositBal;
    }

    /**
     * Sets the value of the rpt1DepositBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1DepositBal(BigDecimal value) {
        this.rpt1DepositBal = value;
    }

    /**
     * Gets the value of the rpt1InTotalCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1InTotalCharges() {
        return rpt1InTotalCharges;
    }

    /**
     * Sets the value of the rpt1InTotalCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1InTotalCharges(BigDecimal value) {
        this.rpt1InTotalCharges = value;
    }

    /**
     * Gets the value of the rpt1InTotalDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1InTotalDiscount() {
        return rpt1InTotalDiscount;
    }

    /**
     * Sets the value of the rpt1InTotalDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1InTotalDiscount(BigDecimal value) {
        this.rpt1InTotalDiscount = value;
    }

    /**
     * Gets the value of the rpt1InTotalMisc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1InTotalMisc() {
        return rpt1InTotalMisc;
    }

    /**
     * Sets the value of the rpt1InTotalMisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1InTotalMisc(BigDecimal value) {
        this.rpt1InTotalMisc = value;
    }

    /**
     * Gets the value of the rpt1OrderAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1OrderAmt() {
        return rpt1OrderAmt;
    }

    /**
     * Sets the value of the rpt1OrderAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1OrderAmt(BigDecimal value) {
        this.rpt1OrderAmt = value;
    }

    /**
     * Gets the value of the rpt1Rounding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1Rounding() {
        return rpt1Rounding;
    }

    /**
     * Sets the value of the rpt1Rounding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1Rounding(BigDecimal value) {
        this.rpt1Rounding = value;
    }

    /**
     * Gets the value of the rpt1TotalAdvBill property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TotalAdvBill() {
        return rpt1TotalAdvBill;
    }

    /**
     * Sets the value of the rpt1TotalAdvBill property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TotalAdvBill(BigDecimal value) {
        this.rpt1TotalAdvBill = value;
    }

    /**
     * Gets the value of the rpt1TotalCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TotalCharges() {
        return rpt1TotalCharges;
    }

    /**
     * Sets the value of the rpt1TotalCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TotalCharges(BigDecimal value) {
        this.rpt1TotalCharges = value;
    }

    /**
     * Gets the value of the rpt1TotalComm property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TotalComm() {
        return rpt1TotalComm;
    }

    /**
     * Sets the value of the rpt1TotalComm property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TotalComm(BigDecimal value) {
        this.rpt1TotalComm = value;
    }

    /**
     * Gets the value of the rpt1TotalDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TotalDiscount() {
        return rpt1TotalDiscount;
    }

    /**
     * Sets the value of the rpt1TotalDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TotalDiscount(BigDecimal value) {
        this.rpt1TotalDiscount = value;
    }

    /**
     * Gets the value of the rpt1TotalMisc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TotalMisc() {
        return rpt1TotalMisc;
    }

    /**
     * Sets the value of the rpt1TotalMisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TotalMisc(BigDecimal value) {
        this.rpt1TotalMisc = value;
    }

    /**
     * Gets the value of the rpt1TotalNet property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TotalNet() {
        return rpt1TotalNet;
    }

    /**
     * Sets the value of the rpt1TotalNet property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TotalNet(BigDecimal value) {
        this.rpt1TotalNet = value;
    }

    /**
     * Gets the value of the rpt1TotalSATax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TotalSATax() {
        return rpt1TotalSATax;
    }

    /**
     * Sets the value of the rpt1TotalSATax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TotalSATax(BigDecimal value) {
        this.rpt1TotalSATax = value;
    }

    /**
     * Gets the value of the rpt1TotalTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TotalTax() {
        return rpt1TotalTax;
    }

    /**
     * Sets the value of the rpt1TotalTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TotalTax(BigDecimal value) {
        this.rpt1TotalTax = value;
    }

    /**
     * Gets the value of the rpt1TotalWHTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1TotalWHTax() {
        return rpt1TotalWHTax;
    }

    /**
     * Sets the value of the rpt1TotalWHTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1TotalWHTax(BigDecimal value) {
        this.rpt1TotalWHTax = value;
    }

    /**
     * Gets the value of the rpt2CCAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2CCAmount() {
        return rpt2CCAmount;
    }

    /**
     * Sets the value of the rpt2CCAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2CCAmount(BigDecimal value) {
        this.rpt2CCAmount = value;
    }

    /**
     * Gets the value of the rpt2CCFreight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2CCFreight() {
        return rpt2CCFreight;
    }

    /**
     * Sets the value of the rpt2CCFreight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2CCFreight(BigDecimal value) {
        this.rpt2CCFreight = value;
    }

    /**
     * Gets the value of the rpt2CCRounding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2CCRounding() {
        return rpt2CCRounding;
    }

    /**
     * Sets the value of the rpt2CCRounding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2CCRounding(BigDecimal value) {
        this.rpt2CCRounding = value;
    }

    /**
     * Gets the value of the rpt2CCTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2CCTax() {
        return rpt2CCTax;
    }

    /**
     * Sets the value of the rpt2CCTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2CCTax(BigDecimal value) {
        this.rpt2CCTax = value;
    }

    /**
     * Gets the value of the rpt2CCTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2CCTotal() {
        return rpt2CCTotal;
    }

    /**
     * Sets the value of the rpt2CCTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2CCTotal(BigDecimal value) {
        this.rpt2CCTotal = value;
    }

    /**
     * Gets the value of the rpt2DepositBal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2DepositBal() {
        return rpt2DepositBal;
    }

    /**
     * Sets the value of the rpt2DepositBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2DepositBal(BigDecimal value) {
        this.rpt2DepositBal = value;
    }

    /**
     * Gets the value of the rpt2InTotalCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2InTotalCharges() {
        return rpt2InTotalCharges;
    }

    /**
     * Sets the value of the rpt2InTotalCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2InTotalCharges(BigDecimal value) {
        this.rpt2InTotalCharges = value;
    }

    /**
     * Gets the value of the rpt2InTotalDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2InTotalDiscount() {
        return rpt2InTotalDiscount;
    }

    /**
     * Sets the value of the rpt2InTotalDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2InTotalDiscount(BigDecimal value) {
        this.rpt2InTotalDiscount = value;
    }

    /**
     * Gets the value of the rpt2InTotalMisc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2InTotalMisc() {
        return rpt2InTotalMisc;
    }

    /**
     * Sets the value of the rpt2InTotalMisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2InTotalMisc(BigDecimal value) {
        this.rpt2InTotalMisc = value;
    }

    /**
     * Gets the value of the rpt2OrderAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2OrderAmt() {
        return rpt2OrderAmt;
    }

    /**
     * Sets the value of the rpt2OrderAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2OrderAmt(BigDecimal value) {
        this.rpt2OrderAmt = value;
    }

    /**
     * Gets the value of the rpt2Rounding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2Rounding() {
        return rpt2Rounding;
    }

    /**
     * Sets the value of the rpt2Rounding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2Rounding(BigDecimal value) {
        this.rpt2Rounding = value;
    }

    /**
     * Gets the value of the rpt2TotalAdvBill property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TotalAdvBill() {
        return rpt2TotalAdvBill;
    }

    /**
     * Sets the value of the rpt2TotalAdvBill property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TotalAdvBill(BigDecimal value) {
        this.rpt2TotalAdvBill = value;
    }

    /**
     * Gets the value of the rpt2TotalCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TotalCharges() {
        return rpt2TotalCharges;
    }

    /**
     * Sets the value of the rpt2TotalCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TotalCharges(BigDecimal value) {
        this.rpt2TotalCharges = value;
    }

    /**
     * Gets the value of the rpt2TotalComm property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TotalComm() {
        return rpt2TotalComm;
    }

    /**
     * Sets the value of the rpt2TotalComm property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TotalComm(BigDecimal value) {
        this.rpt2TotalComm = value;
    }

    /**
     * Gets the value of the rpt2TotalDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TotalDiscount() {
        return rpt2TotalDiscount;
    }

    /**
     * Sets the value of the rpt2TotalDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TotalDiscount(BigDecimal value) {
        this.rpt2TotalDiscount = value;
    }

    /**
     * Gets the value of the rpt2TotalMisc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TotalMisc() {
        return rpt2TotalMisc;
    }

    /**
     * Sets the value of the rpt2TotalMisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TotalMisc(BigDecimal value) {
        this.rpt2TotalMisc = value;
    }

    /**
     * Gets the value of the rpt2TotalNet property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TotalNet() {
        return rpt2TotalNet;
    }

    /**
     * Sets the value of the rpt2TotalNet property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TotalNet(BigDecimal value) {
        this.rpt2TotalNet = value;
    }

    /**
     * Gets the value of the rpt2TotalSATax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TotalSATax() {
        return rpt2TotalSATax;
    }

    /**
     * Sets the value of the rpt2TotalSATax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TotalSATax(BigDecimal value) {
        this.rpt2TotalSATax = value;
    }

    /**
     * Gets the value of the rpt2TotalTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TotalTax() {
        return rpt2TotalTax;
    }

    /**
     * Sets the value of the rpt2TotalTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TotalTax(BigDecimal value) {
        this.rpt2TotalTax = value;
    }

    /**
     * Gets the value of the rpt2TotalWHTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2TotalWHTax() {
        return rpt2TotalWHTax;
    }

    /**
     * Sets the value of the rpt2TotalWHTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2TotalWHTax(BigDecimal value) {
        this.rpt2TotalWHTax = value;
    }

    /**
     * Gets the value of the rpt3CCAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3CCAmount() {
        return rpt3CCAmount;
    }

    /**
     * Sets the value of the rpt3CCAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3CCAmount(BigDecimal value) {
        this.rpt3CCAmount = value;
    }

    /**
     * Gets the value of the rpt3CCFreight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3CCFreight() {
        return rpt3CCFreight;
    }

    /**
     * Sets the value of the rpt3CCFreight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3CCFreight(BigDecimal value) {
        this.rpt3CCFreight = value;
    }

    /**
     * Gets the value of the rpt3CCRounding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3CCRounding() {
        return rpt3CCRounding;
    }

    /**
     * Sets the value of the rpt3CCRounding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3CCRounding(BigDecimal value) {
        this.rpt3CCRounding = value;
    }

    /**
     * Gets the value of the rpt3CCTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3CCTax() {
        return rpt3CCTax;
    }

    /**
     * Sets the value of the rpt3CCTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3CCTax(BigDecimal value) {
        this.rpt3CCTax = value;
    }

    /**
     * Gets the value of the rpt3CCTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3CCTotal() {
        return rpt3CCTotal;
    }

    /**
     * Sets the value of the rpt3CCTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3CCTotal(BigDecimal value) {
        this.rpt3CCTotal = value;
    }

    /**
     * Gets the value of the rpt3DepositBal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3DepositBal() {
        return rpt3DepositBal;
    }

    /**
     * Sets the value of the rpt3DepositBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3DepositBal(BigDecimal value) {
        this.rpt3DepositBal = value;
    }

    /**
     * Gets the value of the rpt3InTotalCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3InTotalCharges() {
        return rpt3InTotalCharges;
    }

    /**
     * Sets the value of the rpt3InTotalCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3InTotalCharges(BigDecimal value) {
        this.rpt3InTotalCharges = value;
    }

    /**
     * Gets the value of the rpt3InTotalDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3InTotalDiscount() {
        return rpt3InTotalDiscount;
    }

    /**
     * Sets the value of the rpt3InTotalDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3InTotalDiscount(BigDecimal value) {
        this.rpt3InTotalDiscount = value;
    }

    /**
     * Gets the value of the rpt3InTotalMisc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3InTotalMisc() {
        return rpt3InTotalMisc;
    }

    /**
     * Sets the value of the rpt3InTotalMisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3InTotalMisc(BigDecimal value) {
        this.rpt3InTotalMisc = value;
    }

    /**
     * Gets the value of the rpt3OrderAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3OrderAmt() {
        return rpt3OrderAmt;
    }

    /**
     * Sets the value of the rpt3OrderAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3OrderAmt(BigDecimal value) {
        this.rpt3OrderAmt = value;
    }

    /**
     * Gets the value of the rpt3Rounding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3Rounding() {
        return rpt3Rounding;
    }

    /**
     * Sets the value of the rpt3Rounding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3Rounding(BigDecimal value) {
        this.rpt3Rounding = value;
    }

    /**
     * Gets the value of the rpt3TotalAdvBill property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TotalAdvBill() {
        return rpt3TotalAdvBill;
    }

    /**
     * Sets the value of the rpt3TotalAdvBill property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TotalAdvBill(BigDecimal value) {
        this.rpt3TotalAdvBill = value;
    }

    /**
     * Gets the value of the rpt3TotalCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TotalCharges() {
        return rpt3TotalCharges;
    }

    /**
     * Sets the value of the rpt3TotalCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TotalCharges(BigDecimal value) {
        this.rpt3TotalCharges = value;
    }

    /**
     * Gets the value of the rpt3TotalComm property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TotalComm() {
        return rpt3TotalComm;
    }

    /**
     * Sets the value of the rpt3TotalComm property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TotalComm(BigDecimal value) {
        this.rpt3TotalComm = value;
    }

    /**
     * Gets the value of the rpt3TotalDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TotalDiscount() {
        return rpt3TotalDiscount;
    }

    /**
     * Sets the value of the rpt3TotalDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TotalDiscount(BigDecimal value) {
        this.rpt3TotalDiscount = value;
    }

    /**
     * Gets the value of the rpt3TotalMisc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TotalMisc() {
        return rpt3TotalMisc;
    }

    /**
     * Sets the value of the rpt3TotalMisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TotalMisc(BigDecimal value) {
        this.rpt3TotalMisc = value;
    }

    /**
     * Gets the value of the rpt3TotalNet property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TotalNet() {
        return rpt3TotalNet;
    }

    /**
     * Sets the value of the rpt3TotalNet property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TotalNet(BigDecimal value) {
        this.rpt3TotalNet = value;
    }

    /**
     * Gets the value of the rpt3TotalSATax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TotalSATax() {
        return rpt3TotalSATax;
    }

    /**
     * Sets the value of the rpt3TotalSATax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TotalSATax(BigDecimal value) {
        this.rpt3TotalSATax = value;
    }

    /**
     * Gets the value of the rpt3TotalTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TotalTax() {
        return rpt3TotalTax;
    }

    /**
     * Sets the value of the rpt3TotalTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TotalTax(BigDecimal value) {
        this.rpt3TotalTax = value;
    }

    /**
     * Gets the value of the rpt3TotalWHTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3TotalWHTax() {
        return rpt3TotalWHTax;
    }

    /**
     * Sets the value of the rpt3TotalWHTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3TotalWHTax(BigDecimal value) {
        this.rpt3TotalWHTax = value;
    }

    /**
     * Gets the value of the srCommAmt1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSRCommAmt1() {
        return srCommAmt1;
    }

    /**
     * Sets the value of the srCommAmt1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSRCommAmt1(BigDecimal value) {
        this.srCommAmt1 = value;
    }

    /**
     * Gets the value of the srCommAmt2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSRCommAmt2() {
        return srCommAmt2;
    }

    /**
     * Sets the value of the srCommAmt2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSRCommAmt2(BigDecimal value) {
        this.srCommAmt2 = value;
    }

    /**
     * Gets the value of the srCommAmt3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSRCommAmt3() {
        return srCommAmt3;
    }

    /**
     * Sets the value of the srCommAmt3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSRCommAmt3(BigDecimal value) {
        this.srCommAmt3 = value;
    }

    /**
     * Gets the value of the srCommAmt4 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSRCommAmt4() {
        return srCommAmt4;
    }

    /**
     * Sets the value of the srCommAmt4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSRCommAmt4(BigDecimal value) {
        this.srCommAmt4 = value;
    }

    /**
     * Gets the value of the srCommAmt5 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSRCommAmt5() {
        return srCommAmt5;
    }

    /**
     * Sets the value of the srCommAmt5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSRCommAmt5(BigDecimal value) {
        this.srCommAmt5 = value;
    }

    /**
     * Gets the value of the srCommableAmt1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSRCommableAmt1() {
        return srCommableAmt1;
    }

    /**
     * Sets the value of the srCommableAmt1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSRCommableAmt1(BigDecimal value) {
        this.srCommableAmt1 = value;
    }

    /**
     * Gets the value of the srCommableAmt2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSRCommableAmt2() {
        return srCommableAmt2;
    }

    /**
     * Sets the value of the srCommableAmt2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSRCommableAmt2(BigDecimal value) {
        this.srCommableAmt2 = value;
    }

    /**
     * Gets the value of the srCommableAmt3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSRCommableAmt3() {
        return srCommableAmt3;
    }

    /**
     * Sets the value of the srCommableAmt3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSRCommableAmt3(BigDecimal value) {
        this.srCommableAmt3 = value;
    }

    /**
     * Gets the value of the srCommableAmt4 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSRCommableAmt4() {
        return srCommableAmt4;
    }

    /**
     * Sets the value of the srCommableAmt4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSRCommableAmt4(BigDecimal value) {
        this.srCommableAmt4 = value;
    }

    /**
     * Gets the value of the srCommableAmt5 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSRCommableAmt5() {
        return srCommableAmt5;
    }

    /**
     * Sets the value of the srCommableAmt5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSRCommableAmt5(BigDecimal value) {
        this.srCommableAmt5 = value;
    }

    /**
     * Gets the value of the salesRepCode1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepCode1() {
        return salesRepCode1;
    }

    /**
     * Sets the value of the salesRepCode1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepCode1(JAXBElement<String> value) {
        this.salesRepCode1 = value;
    }

    /**
     * Gets the value of the salesRepCode2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepCode2() {
        return salesRepCode2;
    }

    /**
     * Sets the value of the salesRepCode2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepCode2(JAXBElement<String> value) {
        this.salesRepCode2 = value;
    }

    /**
     * Gets the value of the salesRepCode3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepCode3() {
        return salesRepCode3;
    }

    /**
     * Sets the value of the salesRepCode3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepCode3(JAXBElement<String> value) {
        this.salesRepCode3 = value;
    }

    /**
     * Gets the value of the salesRepCode4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepCode4() {
        return salesRepCode4;
    }

    /**
     * Sets the value of the salesRepCode4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepCode4(JAXBElement<String> value) {
        this.salesRepCode4 = value;
    }

    /**
     * Gets the value of the salesRepCode5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepCode5() {
        return salesRepCode5;
    }

    /**
     * Sets the value of the salesRepCode5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepCode5(JAXBElement<String> value) {
        this.salesRepCode5 = value;
    }

    /**
     * Gets the value of the salesRepList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepList() {
        return salesRepList;
    }

    /**
     * Sets the value of the salesRepList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepList(JAXBElement<String> value) {
        this.salesRepList = value;
    }

    /**
     * Gets the value of the salesRepName1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepName1() {
        return salesRepName1;
    }

    /**
     * Sets the value of the salesRepName1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepName1(JAXBElement<String> value) {
        this.salesRepName1 = value;
    }

    /**
     * Gets the value of the salesRepName2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepName2() {
        return salesRepName2;
    }

    /**
     * Sets the value of the salesRepName2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepName2(JAXBElement<String> value) {
        this.salesRepName2 = value;
    }

    /**
     * Gets the value of the salesRepName3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepName3() {
        return salesRepName3;
    }

    /**
     * Sets the value of the salesRepName3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepName3(JAXBElement<String> value) {
        this.salesRepName3 = value;
    }

    /**
     * Gets the value of the salesRepName4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepName4() {
        return salesRepName4;
    }

    /**
     * Sets the value of the salesRepName4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepName4(JAXBElement<String> value) {
        this.salesRepName4 = value;
    }

    /**
     * Gets the value of the salesRepName5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesRepName5() {
        return salesRepName5;
    }

    /**
     * Sets the value of the salesRepName5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesRepName5(JAXBElement<String> value) {
        this.salesRepName5 = value;
    }

    /**
     * Gets the value of the satDelivery property.
     * This getter has been renamed from isSatDelivery() to getSatDelivery() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSatDelivery() {
        return satDelivery;
    }

    /**
     * Sets the value of the satDelivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSatDelivery(Boolean value) {
        this.satDelivery = value;
    }

    /**
     * Gets the value of the satPickup property.
     * This getter has been renamed from isSatPickup() to getSatPickup() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSatPickup() {
        return satPickup;
    }

    /**
     * Sets the value of the satPickup property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSatPickup(Boolean value) {
        this.satPickup = value;
    }

    /**
     * Gets the value of the servAlert property.
     * This getter has been renamed from isServAlert() to getServAlert() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getServAlert() {
        return servAlert;
    }

    /**
     * Sets the value of the servAlert property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServAlert(Boolean value) {
        this.servAlert = value;
    }

    /**
     * Gets the value of the servAuthNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServAuthNum() {
        return servAuthNum;
    }

    /**
     * Sets the value of the servAuthNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServAuthNum(JAXBElement<String> value) {
        this.servAuthNum = value;
    }

    /**
     * Gets the value of the servDeliveryDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getServDeliveryDate() {
        return servDeliveryDate;
    }

    /**
     * Sets the value of the servDeliveryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setServDeliveryDate(JAXBElement<XMLGregorianCalendar> value) {
        this.servDeliveryDate = value;
    }

    /**
     * Gets the value of the servHomeDel property.
     * This getter has been renamed from isServHomeDel() to getServHomeDel() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getServHomeDel() {
        return servHomeDel;
    }

    /**
     * Sets the value of the servHomeDel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServHomeDel(Boolean value) {
        this.servHomeDel = value;
    }

    /**
     * Gets the value of the servInstruct property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServInstruct() {
        return servInstruct;
    }

    /**
     * Sets the value of the servInstruct property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServInstruct(JAXBElement<String> value) {
        this.servInstruct = value;
    }

    /**
     * Gets the value of the servPhone property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServPhone() {
        return servPhone;
    }

    /**
     * Sets the value of the servPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServPhone(JAXBElement<String> value) {
        this.servPhone = value;
    }

    /**
     * Gets the value of the servRef1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServRef1() {
        return servRef1;
    }

    /**
     * Sets the value of the servRef1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServRef1(JAXBElement<String> value) {
        this.servRef1 = value;
    }

    /**
     * Gets the value of the servRef2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServRef2() {
        return servRef2;
    }

    /**
     * Sets the value of the servRef2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServRef2(JAXBElement<String> value) {
        this.servRef2 = value;
    }

    /**
     * Gets the value of the servRef3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServRef3() {
        return servRef3;
    }

    /**
     * Sets the value of the servRef3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServRef3(JAXBElement<String> value) {
        this.servRef3 = value;
    }

    /**
     * Gets the value of the servRef4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServRef4() {
        return servRef4;
    }

    /**
     * Sets the value of the servRef4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServRef4(JAXBElement<String> value) {
        this.servRef4 = value;
    }

    /**
     * Gets the value of the servRef5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServRef5() {
        return servRef5;
    }

    /**
     * Sets the value of the servRef5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServRef5(JAXBElement<String> value) {
        this.servRef5 = value;
    }

    /**
     * Gets the value of the servRelease property.
     * This getter has been renamed from isServRelease() to getServRelease() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getServRelease() {
        return servRelease;
    }

    /**
     * Sets the value of the servRelease property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServRelease(Boolean value) {
        this.servRelease = value;
    }

    /**
     * Gets the value of the servSignature property.
     * This getter has been renamed from isServSignature() to getServSignature() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getServSignature() {
        return servSignature;
    }

    /**
     * Sets the value of the servSignature property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServSignature(Boolean value) {
        this.servSignature = value;
    }

    /**
     * Gets the value of the shipComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipComment() {
        return shipComment;
    }

    /**
     * Sets the value of the shipComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipComment(JAXBElement<String> value) {
        this.shipComment = value;
    }

    /**
     * Gets the value of the shipExprtDeclartn property.
     * This getter has been renamed from isShipExprtDeclartn() to getShipExprtDeclartn() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getShipExprtDeclartn() {
        return shipExprtDeclartn;
    }

    /**
     * Sets the value of the shipExprtDeclartn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShipExprtDeclartn(Boolean value) {
        this.shipExprtDeclartn = value;
    }

    /**
     * Gets the value of the shipOrderComplete property.
     * This getter has been renamed from isShipOrderComplete() to getShipOrderComplete() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getShipOrderComplete() {
        return shipOrderComplete;
    }

    /**
     * Sets the value of the shipOrderComplete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShipOrderComplete(Boolean value) {
        this.shipOrderComplete = value;
    }

    /**
     * Gets the value of the shipToAddressList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipToAddressList() {
        return shipToAddressList;
    }

    /**
     * Sets the value of the shipToAddressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipToAddressList(JAXBElement<String> value) {
        this.shipToAddressList = value;
    }

    /**
     * Gets the value of the shipToContactEMailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipToContactEMailAddress() {
        return shipToContactEMailAddress;
    }

    /**
     * Sets the value of the shipToContactEMailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipToContactEMailAddress(JAXBElement<String> value) {
        this.shipToContactEMailAddress = value;
    }

    /**
     * Gets the value of the shipToContactFaxNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipToContactFaxNum() {
        return shipToContactFaxNum;
    }

    /**
     * Sets the value of the shipToContactFaxNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipToContactFaxNum(JAXBElement<String> value) {
        this.shipToContactFaxNum = value;
    }

    /**
     * Gets the value of the shipToContactName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipToContactName() {
        return shipToContactName;
    }

    /**
     * Sets the value of the shipToContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipToContactName(JAXBElement<String> value) {
        this.shipToContactName = value;
    }

    /**
     * Gets the value of the shipToContactPhoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipToContactPhoneNum() {
        return shipToContactPhoneNum;
    }

    /**
     * Sets the value of the shipToContactPhoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipToContactPhoneNum(JAXBElement<String> value) {
        this.shipToContactPhoneNum = value;
    }

    /**
     * Gets the value of the shipToCustId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipToCustId() {
        return shipToCustId;
    }

    /**
     * Sets the value of the shipToCustId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipToCustId(JAXBElement<String> value) {
        this.shipToCustId = value;
    }

    /**
     * Gets the value of the shipToCustNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShipToCustNum() {
        return shipToCustNum;
    }

    /**
     * Sets the value of the shipToCustNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShipToCustNum(Integer value) {
        this.shipToCustNum = value;
    }

    /**
     * Gets the value of the shipToNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipToNum() {
        return shipToNum;
    }

    /**
     * Sets the value of the shipToNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipToNum(JAXBElement<String> value) {
        this.shipToNum = value;
    }

    /**
     * Gets the value of the shipViaCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipViaCode() {
        return shipViaCode;
    }

    /**
     * Sets the value of the shipViaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipViaCode(JAXBElement<String> value) {
        this.shipViaCode = value;
    }

    /**
     * Gets the value of the shipViaCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipViaCodeDescription() {
        return shipViaCodeDescription;
    }

    /**
     * Sets the value of the shipViaCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipViaCodeDescription(JAXBElement<String> value) {
        this.shipViaCodeDescription = value;
    }

    /**
     * Gets the value of the shipViaCodeWebDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipViaCodeWebDesc() {
        return shipViaCodeWebDesc;
    }

    /**
     * Sets the value of the shipViaCodeWebDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipViaCodeWebDesc(JAXBElement<String> value) {
        this.shipViaCodeWebDesc = value;
    }

    /**
     * Gets the value of the showApplyOrderDiscountsControl property.
     * This getter has been renamed from isShowApplyOrderDiscountsControl() to getShowApplyOrderDiscountsControl() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getShowApplyOrderDiscountsControl() {
        return showApplyOrderDiscountsControl;
    }

    /**
     * Sets the value of the showApplyOrderDiscountsControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowApplyOrderDiscountsControl(Boolean value) {
        this.showApplyOrderDiscountsControl = value;
    }

    /**
     * Gets the value of the shpConNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShpConNum() {
        return shpConNum;
    }

    /**
     * Sets the value of the shpConNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShpConNum(Integer value) {
        this.shpConNum = value;
    }

    /**
     * Gets the value of the sndAlrtShp property.
     * This getter has been renamed from isSndAlrtShp() to getSndAlrtShp() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSndAlrtShp() {
        return sndAlrtShp;
    }

    /**
     * Sets the value of the sndAlrtShp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSndAlrtShp(Boolean value) {
        this.sndAlrtShp = value;
    }

    /**
     * Gets the value of the soldToAddressList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSoldToAddressList() {
        return soldToAddressList;
    }

    /**
     * Sets the value of the soldToAddressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSoldToAddressList(JAXBElement<String> value) {
        this.soldToAddressList = value;
    }

    /**
     * Gets the value of the soldToContactEMailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSoldToContactEMailAddress() {
        return soldToContactEMailAddress;
    }

    /**
     * Sets the value of the soldToContactEMailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSoldToContactEMailAddress(JAXBElement<String> value) {
        this.soldToContactEMailAddress = value;
    }

    /**
     * Gets the value of the soldToContactFaxNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSoldToContactFaxNum() {
        return soldToContactFaxNum;
    }

    /**
     * Sets the value of the soldToContactFaxNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSoldToContactFaxNum(JAXBElement<String> value) {
        this.soldToContactFaxNum = value;
    }

    /**
     * Gets the value of the soldToContactName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSoldToContactName() {
        return soldToContactName;
    }

    /**
     * Sets the value of the soldToContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSoldToContactName(JAXBElement<String> value) {
        this.soldToContactName = value;
    }

    /**
     * Gets the value of the soldToContactPhoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSoldToContactPhoneNum() {
        return soldToContactPhoneNum;
    }

    /**
     * Sets the value of the soldToContactPhoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSoldToContactPhoneNum(JAXBElement<String> value) {
        this.soldToContactPhoneNum = value;
    }

    /**
     * Gets the value of the sysRevID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSysRevID() {
        return sysRevID;
    }

    /**
     * Sets the value of the sysRevID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSysRevID(Long value) {
        this.sysRevID = value;
    }

    /**
     * Gets the value of the taxPoint property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTaxPoint() {
        return taxPoint;
    }

    /**
     * Sets the value of the taxPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTaxPoint(JAXBElement<XMLGregorianCalendar> value) {
        this.taxPoint = value;
    }

    /**
     * Gets the value of the taxRateDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTaxRateDate() {
        return taxRateDate;
    }

    /**
     * Sets the value of the taxRateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTaxRateDate(JAXBElement<XMLGregorianCalendar> value) {
        this.taxRateDate = value;
    }

    /**
     * Gets the value of the taxRegionCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxRegionCode() {
        return taxRegionCode;
    }

    /**
     * Sets the value of the taxRegionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxRegionCode(JAXBElement<String> value) {
        this.taxRegionCode = value;
    }

    /**
     * Gets the value of the taxRegionCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxRegionCodeDescription() {
        return taxRegionCodeDescription;
    }

    /**
     * Sets the value of the taxRegionCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxRegionCodeDescription(JAXBElement<String> value) {
        this.taxRegionCodeDescription = value;
    }

    /**
     * Gets the value of the termsCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTermsCode() {
        return termsCode;
    }

    /**
     * Sets the value of the termsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTermsCode(JAXBElement<String> value) {
        this.termsCode = value;
    }

    /**
     * Gets the value of the termsCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTermsCodeDescription() {
        return termsCodeDescription;
    }

    /**
     * Sets the value of the termsCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTermsCodeDescription(JAXBElement<String> value) {
        this.termsCodeDescription = value;
    }

    /**
     * Gets the value of the totalAdvBill property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalAdvBill() {
        return totalAdvBill;
    }

    /**
     * Sets the value of the totalAdvBill property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalAdvBill(BigDecimal value) {
        this.totalAdvBill = value;
    }

    /**
     * Gets the value of the totalCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalCharges() {
        return totalCharges;
    }

    /**
     * Sets the value of the totalCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalCharges(BigDecimal value) {
        this.totalCharges = value;
    }

    /**
     * Gets the value of the totalComm property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalComm() {
        return totalComm;
    }

    /**
     * Sets the value of the totalComm property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalComm(BigDecimal value) {
        this.totalComm = value;
    }

    /**
     * Gets the value of the totalCommLines property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalCommLines() {
        return totalCommLines;
    }

    /**
     * Sets the value of the totalCommLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalCommLines(Integer value) {
        this.totalCommLines = value;
    }

    /**
     * Gets the value of the totalDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    /**
     * Sets the value of the totalDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalDiscount(BigDecimal value) {
        this.totalDiscount = value;
    }

    /**
     * Gets the value of the totalInvoiced property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalInvoiced() {
        return totalInvoiced;
    }

    /**
     * Sets the value of the totalInvoiced property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalInvoiced(BigDecimal value) {
        this.totalInvoiced = value;
    }

    /**
     * Gets the value of the totalLines property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalLines() {
        return totalLines;
    }

    /**
     * Sets the value of the totalLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalLines(Integer value) {
        this.totalLines = value;
    }

    /**
     * Gets the value of the totalMisc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalMisc() {
        return totalMisc;
    }

    /**
     * Sets the value of the totalMisc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalMisc(BigDecimal value) {
        this.totalMisc = value;
    }

    /**
     * Gets the value of the totalNet property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalNet() {
        return totalNet;
    }

    /**
     * Sets the value of the totalNet property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalNet(BigDecimal value) {
        this.totalNet = value;
    }

    /**
     * Gets the value of the totalOrder property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalOrder() {
        return totalOrder;
    }

    /**
     * Sets the value of the totalOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalOrder(BigDecimal value) {
        this.totalOrder = value;
    }

    /**
     * Gets the value of the totalRelDates property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalRelDates() {
        return totalRelDates;
    }

    /**
     * Sets the value of the totalRelDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalRelDates(Integer value) {
        this.totalRelDates = value;
    }

    /**
     * Gets the value of the totalReleases property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalReleases() {
        return totalReleases;
    }

    /**
     * Sets the value of the totalReleases property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalReleases(Integer value) {
        this.totalReleases = value;
    }

    /**
     * Gets the value of the totalSATax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalSATax() {
        return totalSATax;
    }

    /**
     * Sets the value of the totalSATax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalSATax(BigDecimal value) {
        this.totalSATax = value;
    }

    /**
     * Gets the value of the totalShipped property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalShipped() {
        return totalShipped;
    }

    /**
     * Sets the value of the totalShipped property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalShipped(BigDecimal value) {
        this.totalShipped = value;
    }

    /**
     * Gets the value of the totalTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalTax() {
        return totalTax;
    }

    /**
     * Sets the value of the totalTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalTax(BigDecimal value) {
        this.totalTax = value;
    }

    /**
     * Gets the value of the totalWHTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalWHTax() {
        return totalWHTax;
    }

    /**
     * Sets the value of the totalWHTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalWHTax(BigDecimal value) {
        this.totalWHTax = value;
    }

    /**
     * Gets the value of the tranDocTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTranDocTypeID() {
        return tranDocTypeID;
    }

    /**
     * Sets the value of the tranDocTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTranDocTypeID(JAXBElement<String> value) {
        this.tranDocTypeID = value;
    }

    /**
     * Gets the value of the upsqvMemo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUPSQVMemo() {
        return upsqvMemo;
    }

    /**
     * Sets the value of the upsqvMemo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUPSQVMemo(JAXBElement<String> value) {
        this.upsqvMemo = value;
    }

    /**
     * Gets the value of the upsqvShipFromName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUPSQVShipFromName() {
        return upsqvShipFromName;
    }

    /**
     * Sets the value of the upsqvShipFromName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUPSQVShipFromName(JAXBElement<String> value) {
        this.upsqvShipFromName = value;
    }

    /**
     * Gets the value of the upsQuantumView property.
     * This getter has been renamed from isUPSQuantumView() to getUPSQuantumView() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getUPSQuantumView() {
        return upsQuantumView;
    }

    /**
     * Sets the value of the upsQuantumView property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUPSQuantumView(Boolean value) {
        this.upsQuantumView = value;
    }

    /**
     * Gets the value of the updateDtlAndRelRecords property.
     * This getter has been renamed from isUpdateDtlAndRelRecords() to getUpdateDtlAndRelRecords() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getUpdateDtlAndRelRecords() {
        return updateDtlAndRelRecords;
    }

    /**
     * Sets the value of the updateDtlAndRelRecords property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUpdateDtlAndRelRecords(Boolean value) {
        this.updateDtlAndRelRecords = value;
    }

    /**
     * Gets the value of the useOTS property.
     * This getter has been renamed from isUseOTS() to getUseOTS() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getUseOTS() {
        return useOTS;
    }

    /**
     * Sets the value of the useOTS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseOTS(Boolean value) {
        this.useOTS = value;
    }

    /**
     * Gets the value of the userChar1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserChar1() {
        return userChar1;
    }

    /**
     * Sets the value of the userChar1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserChar1(JAXBElement<String> value) {
        this.userChar1 = value;
    }

    /**
     * Gets the value of the userChar2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserChar2() {
        return userChar2;
    }

    /**
     * Sets the value of the userChar2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserChar2(JAXBElement<String> value) {
        this.userChar2 = value;
    }

    /**
     * Gets the value of the userChar3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserChar3() {
        return userChar3;
    }

    /**
     * Sets the value of the userChar3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserChar3(JAXBElement<String> value) {
        this.userChar3 = value;
    }

    /**
     * Gets the value of the userChar4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserChar4() {
        return userChar4;
    }

    /**
     * Sets the value of the userChar4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserChar4(JAXBElement<String> value) {
        this.userChar4 = value;
    }

    /**
     * Gets the value of the userDate1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getUserDate1() {
        return userDate1;
    }

    /**
     * Sets the value of the userDate1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setUserDate1(JAXBElement<XMLGregorianCalendar> value) {
        this.userDate1 = value;
    }

    /**
     * Gets the value of the userDate2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getUserDate2() {
        return userDate2;
    }

    /**
     * Sets the value of the userDate2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setUserDate2(JAXBElement<XMLGregorianCalendar> value) {
        this.userDate2 = value;
    }

    /**
     * Gets the value of the userDate3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getUserDate3() {
        return userDate3;
    }

    /**
     * Sets the value of the userDate3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setUserDate3(JAXBElement<XMLGregorianCalendar> value) {
        this.userDate3 = value;
    }

    /**
     * Gets the value of the userDate4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getUserDate4() {
        return userDate4;
    }

    /**
     * Sets the value of the userDate4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setUserDate4(JAXBElement<XMLGregorianCalendar> value) {
        this.userDate4 = value;
    }

    /**
     * Gets the value of the userDecimal1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUserDecimal1() {
        return userDecimal1;
    }

    /**
     * Sets the value of the userDecimal1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUserDecimal1(BigDecimal value) {
        this.userDecimal1 = value;
    }

    /**
     * Gets the value of the userDecimal2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUserDecimal2() {
        return userDecimal2;
    }

    /**
     * Sets the value of the userDecimal2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUserDecimal2(BigDecimal value) {
        this.userDecimal2 = value;
    }

    /**
     * Gets the value of the userInteger1 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUserInteger1() {
        return userInteger1;
    }

    /**
     * Sets the value of the userInteger1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUserInteger1(Integer value) {
        this.userInteger1 = value;
    }

    /**
     * Gets the value of the userInteger2 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUserInteger2() {
        return userInteger2;
    }

    /**
     * Sets the value of the userInteger2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUserInteger2(Integer value) {
        this.userInteger2 = value;
    }

    /**
     * Gets the value of the voidOrder property.
     * This getter has been renamed from isVoidOrder() to getVoidOrder() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getVoidOrder() {
        return voidOrder;
    }

    /**
     * Sets the value of the voidOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVoidOrder(Boolean value) {
        this.voidOrder = value;
    }

    /**
     * Gets the value of the wiApplication property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWIApplication() {
        return wiApplication;
    }

    /**
     * Sets the value of the wiApplication property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWIApplication(JAXBElement<String> value) {
        this.wiApplication = value;
    }

    /**
     * Gets the value of the wiCreditCardorder property.
     * This getter has been renamed from isWICreditCardorder() to getWICreditCardorder() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getWICreditCardorder() {
        return wiCreditCardorder;
    }

    /**
     * Sets the value of the wiCreditCardorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWICreditCardorder(Boolean value) {
        this.wiCreditCardorder = value;
    }

    /**
     * Gets the value of the wiOrder property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWIOrder() {
        return wiOrder;
    }

    /**
     * Sets the value of the wiOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWIOrder(JAXBElement<String> value) {
        this.wiOrder = value;
    }

    /**
     * Gets the value of the wiUserID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWIUserID() {
        return wiUserID;
    }

    /**
     * Sets the value of the wiUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWIUserID(JAXBElement<String> value) {
        this.wiUserID = value;
    }

    /**
     * Gets the value of the wiUsername property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWIUsername() {
        return wiUsername;
    }

    /**
     * Sets the value of the wiUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWIUsername(JAXBElement<String> value) {
        this.wiUsername = value;
    }

    /**
     * Gets the value of the webEntryPerson property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWebEntryPerson() {
        return webEntryPerson;
    }

    /**
     * Sets the value of the webEntryPerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWebEntryPerson(JAXBElement<String> value) {
        this.webEntryPerson = value;
    }

    /**
     * Gets the value of the webOrder property.
     * This getter has been renamed from isWebOrder() to getWebOrder() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getWebOrder() {
        return webOrder;
    }

    /**
     * Sets the value of the webOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWebOrder(Boolean value) {
        this.webOrder = value;
    }

    /**
     * Gets the value of the xRefContractDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getXRefContractDate() {
        return xRefContractDate;
    }

    /**
     * Sets the value of the xRefContractDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setXRefContractDate(JAXBElement<XMLGregorianCalendar> value) {
        this.xRefContractDate = value;
    }

    /**
     * Gets the value of the xRefContractNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getXRefContractNum() {
        return xRefContractNum;
    }

    /**
     * Sets the value of the xRefContractNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setXRefContractNum(JAXBElement<String> value) {
        this.xRefContractNum = value;
    }

    /**
     * Gets the value of the dspBTCustID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDspBTCustID() {
        return dspBTCustID;
    }

    /**
     * Sets the value of the dspBTCustID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDspBTCustID(JAXBElement<String> value) {
        this.dspBTCustID = value;
    }

}
