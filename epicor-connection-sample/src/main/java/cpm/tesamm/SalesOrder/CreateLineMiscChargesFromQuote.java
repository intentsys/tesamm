
package cpm.tesamm.SalesOrder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ipOrderNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ipOrderLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ipQuoteNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ipQuoteLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ipQtyNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ds" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SalesOrderTableset" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ipOrderNum",
    "ipOrderLine",
    "ipQuoteNum",
    "ipQuoteLine",
    "ipQtyNum",
    "ds"
})
@XmlRootElement(name = "CreateLineMiscChargesFromQuote")
public class CreateLineMiscChargesFromQuote {

    protected Integer ipOrderNum;
    protected Integer ipOrderLine;
    protected Integer ipQuoteNum;
    protected Integer ipQuoteLine;
    protected Integer ipQtyNum;
    @XmlElementRef(name = "ds", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<SalesOrderTableset> ds;

    /**
     * Gets the value of the ipOrderNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIpOrderNum() {
        return ipOrderNum;
    }

    /**
     * Sets the value of the ipOrderNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIpOrderNum(Integer value) {
        this.ipOrderNum = value;
    }

    /**
     * Gets the value of the ipOrderLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIpOrderLine() {
        return ipOrderLine;
    }

    /**
     * Sets the value of the ipOrderLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIpOrderLine(Integer value) {
        this.ipOrderLine = value;
    }

    /**
     * Gets the value of the ipQuoteNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIpQuoteNum() {
        return ipQuoteNum;
    }

    /**
     * Sets the value of the ipQuoteNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIpQuoteNum(Integer value) {
        this.ipQuoteNum = value;
    }

    /**
     * Gets the value of the ipQuoteLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIpQuoteLine() {
        return ipQuoteLine;
    }

    /**
     * Sets the value of the ipQuoteLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIpQuoteLine(Integer value) {
        this.ipQuoteLine = value;
    }

    /**
     * Gets the value of the ipQtyNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIpQtyNum() {
        return ipQtyNum;
    }

    /**
     * Sets the value of the ipQtyNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIpQtyNum(Integer value) {
        this.ipQtyNum = value;
    }

    /**
     * Gets the value of the ds property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}
     *     
     */
    public JAXBElement<SalesOrderTableset> getDs() {
        return ds;
    }

    /**
     * Sets the value of the ds property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SalesOrderTableset }{@code >}
     *     
     */
    public void setDs(JAXBElement<SalesOrderTableset> value) {
        this.ds = value;
    }

}
