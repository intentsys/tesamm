
package cpm.tesamm.SalesOrder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetQuoteQtyResult" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrdDtlQuoteQtyTableset" minOccurs="0"/>
 *         &lt;element name="morePages" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getQuoteQtyResult",
    "morePages"
})
@XmlRootElement(name = "GetQuoteQtyResponse")
public class GetQuoteQtyResponse {

    @XmlElementRef(name = "GetQuoteQtyResult", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<OrdDtlQuoteQtyTableset> getQuoteQtyResult;
    protected Boolean morePages;

    /**
     * Gets the value of the getQuoteQtyResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrdDtlQuoteQtyTableset }{@code >}
     *     
     */
    public JAXBElement<OrdDtlQuoteQtyTableset> getGetQuoteQtyResult() {
        return getQuoteQtyResult;
    }

    /**
     * Sets the value of the getQuoteQtyResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrdDtlQuoteQtyTableset }{@code >}
     *     
     */
    public void setGetQuoteQtyResult(JAXBElement<OrdDtlQuoteQtyTableset> value) {
        this.getQuoteQtyResult = value;
    }

    /**
     * Gets the value of the morePages property.
     * This getter has been renamed from isMorePages() to getMorePages() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMorePages() {
        return morePages;
    }

    /**
     * Sets the value of the morePages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMorePages(Boolean value) {
        this.morePages = value;
    }

}
