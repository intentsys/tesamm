
package cpm.tesamm.SalesOrder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuoteQtyTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuoteQtyTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QuoteQtyRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}QuoteQtyRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuoteQtyTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "quoteQtyRow"
})
public class QuoteQtyTable {

    @XmlElement(name = "QuoteQtyRow", nillable = true)
    protected List<QuoteQtyRow> quoteQtyRow;

    /**
     * Gets the value of the quoteQtyRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the quoteQtyRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuoteQtyRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuoteQtyRow }
     * 
     * 
     */
    public List<QuoteQtyRow> getQuoteQtyRow() {
        if (quoteQtyRow == null) {
            quoteQtyRow = new ArrayList<QuoteQtyRow>();
        }
        return this.quoteQtyRow;
    }

    /**
     * Sets the value of the quoteQtyRow property.
     * 
     * @param quoteQtyRow
     *     allowed object is
     *     {@link QuoteQtyRow }
     *     
     */
    public void setQuoteQtyRow(List<QuoteQtyRow> quoteQtyRow) {
        this.quoteQtyRow = quoteQtyRow;
    }

}
