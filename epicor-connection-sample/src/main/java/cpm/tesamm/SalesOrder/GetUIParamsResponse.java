
package cpm.tesamm.SalesOrder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUIParamsResult" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}SOEntryUIParamsTableset" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUIParamsResult"
})
@XmlRootElement(name = "GetUIParamsResponse")
public class GetUIParamsResponse {

    @XmlElementRef(name = "GetUIParamsResult", namespace = "Erp:BO:SalesOrder", type = JAXBElement.class, required = false)
    protected JAXBElement<SOEntryUIParamsTableset> getUIParamsResult;

    /**
     * Gets the value of the getUIParamsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SOEntryUIParamsTableset }{@code >}
     *     
     */
    public JAXBElement<SOEntryUIParamsTableset> getGetUIParamsResult() {
        return getUIParamsResult;
    }

    /**
     * Sets the value of the getUIParamsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SOEntryUIParamsTableset }{@code >}
     *     
     */
    public void setGetUIParamsResult(JAXBElement<SOEntryUIParamsTableset> value) {
        this.getUIParamsResult = value;
    }

}
