
package cpm.tesamm.SalesOrder;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for QuoteQtyRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuoteQtyRow">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceRow">
 *       &lt;sequence>
 *         &lt;element name="BaseCurrSymbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BitFlag" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BurdenMarkUp" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CalcMarkup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CalcMarkupProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CalcProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CalcProfitProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CalcUPCommMarkup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CalcUPCommProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CalcUnitCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CalcUnitPriceMarkup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CalcUnitPriceProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ChangeTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ChangedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CommissionPct" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrSymbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencySwitch" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DisableMtlMarkup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DocInUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocPricePerAdl1000" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocTotalSellPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DocUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="IUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="LaborMarkUp" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MarkUpID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaterialMarkUp" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MaterialMarkupM" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MaterialMarkupP" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MiscChrg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MiscCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MiscCostDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MiscCostMarkUp" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MtlBurMarkUp" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OurQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PercentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PriceBurMarkup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceBurProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceLbrMarkup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceLbrProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceMscChrgMarkup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceMscChrgProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceMtlBurMarkup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceMtlBurProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceMtlMarkup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceMtlProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PricePerAdl1000" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PricePerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PricePerFactor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PriceSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PriceSubMarkup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceSubProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceTotalCommMarkup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceTotalCommProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceTotalMarkup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PriceTotalProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="QtyNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QuoteLine" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QuoteLineLineDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QuoteNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QuoteNumCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QuotedMarkup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="QuotedProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RollUpCostNeeded" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Rpt1InUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt1UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2InUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt2UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3InUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rpt3UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SalesUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SellingFactor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SellingFactorDirection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SellingQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SubcontractMarkUp" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SysRevID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="TotalBurCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalCommProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalCommission" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalLbrCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalMarkup" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalMtlBurCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalMtlCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalProdBurHrs" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalProdLbrHrs" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalProfit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalQuotedPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalSellPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalSetupBurHrs" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalSetupLbrHrs" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalSubCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="UnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="UserChangedUnitPrice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="WQUnitPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuoteQtyRow", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "baseCurrSymbol",
    "bitFlag",
    "burdenMarkUp",
    "calcMarkup",
    "calcMarkupProfit",
    "calcProfit",
    "calcProfitProfit",
    "calcUPCommMarkup",
    "calcUPCommProfit",
    "calcUnitCost",
    "calcUnitPriceMarkup",
    "calcUnitPriceProfit",
    "changeDate",
    "changeTime",
    "changedBy",
    "commissionPct",
    "company",
    "currSymbol",
    "currencyCode",
    "currencySwitch",
    "disableMtlMarkup",
    "docInUnitPrice",
    "docPricePerAdl1000",
    "docTotalSellPrice",
    "docUnitPrice",
    "ium",
    "inUnitPrice",
    "laborMarkUp",
    "markUpID",
    "materialMarkUp",
    "materialMarkupM",
    "materialMarkupP",
    "miscChrg",
    "miscCost",
    "miscCostDesc",
    "miscCostMarkUp",
    "mtlBurMarkUp",
    "ourQuantity",
    "percentType",
    "priceBurMarkup",
    "priceBurProfit",
    "priceLbrMarkup",
    "priceLbrProfit",
    "priceMscChrgMarkup",
    "priceMscChrgProfit",
    "priceMtlBurMarkup",
    "priceMtlBurProfit",
    "priceMtlMarkup",
    "priceMtlProfit",
    "pricePerAdl1000",
    "pricePerCode",
    "pricePerFactor",
    "priceSource",
    "priceSubMarkup",
    "priceSubProfit",
    "priceTotalCommMarkup",
    "priceTotalCommProfit",
    "priceTotalMarkup",
    "priceTotalProfit",
    "qtyNum",
    "quoteLine",
    "quoteLineLineDesc",
    "quoteNum",
    "quoteNumCurrencyCode",
    "quotedMarkup",
    "quotedProfit",
    "rollUpCostNeeded",
    "rpt1InUnitPrice",
    "rpt1UnitPrice",
    "rpt2InUnitPrice",
    "rpt2UnitPrice",
    "rpt3InUnitPrice",
    "rpt3UnitPrice",
    "salesUM",
    "sellingFactor",
    "sellingFactorDirection",
    "sellingQuantity",
    "subcontractMarkUp",
    "sysRevID",
    "totalBurCost",
    "totalCommProfit",
    "totalCommission",
    "totalCost",
    "totalLbrCost",
    "totalMarkup",
    "totalMtlBurCost",
    "totalMtlCost",
    "totalProdBurHrs",
    "totalProdLbrHrs",
    "totalProfit",
    "totalQuotedPrice",
    "totalSellPrice",
    "totalSetupBurHrs",
    "totalSetupLbrHrs",
    "totalSubCost",
    "unitPrice",
    "userChangedUnitPrice",
    "wqUnitPrice"
})
public class QuoteQtyRow
    extends IceRow
{

    @XmlElementRef(name = "BaseCurrSymbol", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> baseCurrSymbol;
    @XmlElement(name = "BitFlag")
    protected Integer bitFlag;
    @XmlElement(name = "BurdenMarkUp")
    protected BigDecimal burdenMarkUp;
    @XmlElement(name = "CalcMarkup")
    protected BigDecimal calcMarkup;
    @XmlElement(name = "CalcMarkupProfit")
    protected BigDecimal calcMarkupProfit;
    @XmlElement(name = "CalcProfit")
    protected BigDecimal calcProfit;
    @XmlElement(name = "CalcProfitProfit")
    protected BigDecimal calcProfitProfit;
    @XmlElement(name = "CalcUPCommMarkup")
    protected BigDecimal calcUPCommMarkup;
    @XmlElement(name = "CalcUPCommProfit")
    protected BigDecimal calcUPCommProfit;
    @XmlElement(name = "CalcUnitCost")
    protected BigDecimal calcUnitCost;
    @XmlElement(name = "CalcUnitPriceMarkup")
    protected BigDecimal calcUnitPriceMarkup;
    @XmlElement(name = "CalcUnitPriceProfit")
    protected BigDecimal calcUnitPriceProfit;
    @XmlElementRef(name = "ChangeDate", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> changeDate;
    @XmlElement(name = "ChangeTime")
    protected Integer changeTime;
    @XmlElementRef(name = "ChangedBy", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changedBy;
    @XmlElement(name = "CommissionPct")
    protected BigDecimal commissionPct;
    @XmlElementRef(name = "Company", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> company;
    @XmlElementRef(name = "CurrSymbol", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currSymbol;
    @XmlElementRef(name = "CurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElement(name = "CurrencySwitch")
    protected Boolean currencySwitch;
    @XmlElement(name = "DisableMtlMarkup")
    protected Boolean disableMtlMarkup;
    @XmlElement(name = "DocInUnitPrice")
    protected BigDecimal docInUnitPrice;
    @XmlElement(name = "DocPricePerAdl1000")
    protected BigDecimal docPricePerAdl1000;
    @XmlElement(name = "DocTotalSellPrice")
    protected BigDecimal docTotalSellPrice;
    @XmlElement(name = "DocUnitPrice")
    protected BigDecimal docUnitPrice;
    @XmlElementRef(name = "IUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ium;
    @XmlElement(name = "InUnitPrice")
    protected BigDecimal inUnitPrice;
    @XmlElement(name = "LaborMarkUp")
    protected BigDecimal laborMarkUp;
    @XmlElementRef(name = "MarkUpID", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> markUpID;
    @XmlElement(name = "MaterialMarkUp")
    protected BigDecimal materialMarkUp;
    @XmlElement(name = "MaterialMarkupM")
    protected BigDecimal materialMarkupM;
    @XmlElement(name = "MaterialMarkupP")
    protected BigDecimal materialMarkupP;
    @XmlElementRef(name = "MiscChrg", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> miscChrg;
    @XmlElement(name = "MiscCost")
    protected BigDecimal miscCost;
    @XmlElementRef(name = "MiscCostDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> miscCostDesc;
    @XmlElement(name = "MiscCostMarkUp")
    protected BigDecimal miscCostMarkUp;
    @XmlElement(name = "MtlBurMarkUp")
    protected BigDecimal mtlBurMarkUp;
    @XmlElement(name = "OurQuantity")
    protected BigDecimal ourQuantity;
    @XmlElementRef(name = "PercentType", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> percentType;
    @XmlElement(name = "PriceBurMarkup")
    protected BigDecimal priceBurMarkup;
    @XmlElement(name = "PriceBurProfit")
    protected BigDecimal priceBurProfit;
    @XmlElement(name = "PriceLbrMarkup")
    protected BigDecimal priceLbrMarkup;
    @XmlElement(name = "PriceLbrProfit")
    protected BigDecimal priceLbrProfit;
    @XmlElement(name = "PriceMscChrgMarkup")
    protected BigDecimal priceMscChrgMarkup;
    @XmlElement(name = "PriceMscChrgProfit")
    protected BigDecimal priceMscChrgProfit;
    @XmlElement(name = "PriceMtlBurMarkup")
    protected BigDecimal priceMtlBurMarkup;
    @XmlElement(name = "PriceMtlBurProfit")
    protected BigDecimal priceMtlBurProfit;
    @XmlElement(name = "PriceMtlMarkup")
    protected BigDecimal priceMtlMarkup;
    @XmlElement(name = "PriceMtlProfit")
    protected BigDecimal priceMtlProfit;
    @XmlElement(name = "PricePerAdl1000")
    protected BigDecimal pricePerAdl1000;
    @XmlElementRef(name = "PricePerCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pricePerCode;
    @XmlElement(name = "PricePerFactor")
    protected Integer pricePerFactor;
    @XmlElementRef(name = "PriceSource", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> priceSource;
    @XmlElement(name = "PriceSubMarkup")
    protected BigDecimal priceSubMarkup;
    @XmlElement(name = "PriceSubProfit")
    protected BigDecimal priceSubProfit;
    @XmlElement(name = "PriceTotalCommMarkup")
    protected BigDecimal priceTotalCommMarkup;
    @XmlElement(name = "PriceTotalCommProfit")
    protected BigDecimal priceTotalCommProfit;
    @XmlElement(name = "PriceTotalMarkup")
    protected BigDecimal priceTotalMarkup;
    @XmlElement(name = "PriceTotalProfit")
    protected BigDecimal priceTotalProfit;
    @XmlElement(name = "QtyNum")
    protected Integer qtyNum;
    @XmlElement(name = "QuoteLine")
    protected Integer quoteLine;
    @XmlElementRef(name = "QuoteLineLineDesc", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> quoteLineLineDesc;
    @XmlElement(name = "QuoteNum")
    protected Integer quoteNum;
    @XmlElementRef(name = "QuoteNumCurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> quoteNumCurrencyCode;
    @XmlElement(name = "QuotedMarkup")
    protected BigDecimal quotedMarkup;
    @XmlElement(name = "QuotedProfit")
    protected BigDecimal quotedProfit;
    @XmlElement(name = "RollUpCostNeeded")
    protected Boolean rollUpCostNeeded;
    @XmlElement(name = "Rpt1InUnitPrice")
    protected BigDecimal rpt1InUnitPrice;
    @XmlElement(name = "Rpt1UnitPrice")
    protected BigDecimal rpt1UnitPrice;
    @XmlElement(name = "Rpt2InUnitPrice")
    protected BigDecimal rpt2InUnitPrice;
    @XmlElement(name = "Rpt2UnitPrice")
    protected BigDecimal rpt2UnitPrice;
    @XmlElement(name = "Rpt3InUnitPrice")
    protected BigDecimal rpt3InUnitPrice;
    @XmlElement(name = "Rpt3UnitPrice")
    protected BigDecimal rpt3UnitPrice;
    @XmlElementRef(name = "SalesUM", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> salesUM;
    @XmlElement(name = "SellingFactor")
    protected BigDecimal sellingFactor;
    @XmlElementRef(name = "SellingFactorDirection", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sellingFactorDirection;
    @XmlElement(name = "SellingQuantity")
    protected BigDecimal sellingQuantity;
    @XmlElement(name = "SubcontractMarkUp")
    protected BigDecimal subcontractMarkUp;
    @XmlElement(name = "SysRevID")
    protected Long sysRevID;
    @XmlElement(name = "TotalBurCost")
    protected BigDecimal totalBurCost;
    @XmlElement(name = "TotalCommProfit")
    protected BigDecimal totalCommProfit;
    @XmlElement(name = "TotalCommission")
    protected BigDecimal totalCommission;
    @XmlElement(name = "TotalCost")
    protected BigDecimal totalCost;
    @XmlElement(name = "TotalLbrCost")
    protected BigDecimal totalLbrCost;
    @XmlElement(name = "TotalMarkup")
    protected BigDecimal totalMarkup;
    @XmlElement(name = "TotalMtlBurCost")
    protected BigDecimal totalMtlBurCost;
    @XmlElement(name = "TotalMtlCost")
    protected BigDecimal totalMtlCost;
    @XmlElement(name = "TotalProdBurHrs")
    protected BigDecimal totalProdBurHrs;
    @XmlElement(name = "TotalProdLbrHrs")
    protected BigDecimal totalProdLbrHrs;
    @XmlElement(name = "TotalProfit")
    protected BigDecimal totalProfit;
    @XmlElement(name = "TotalQuotedPrice")
    protected BigDecimal totalQuotedPrice;
    @XmlElement(name = "TotalSellPrice")
    protected BigDecimal totalSellPrice;
    @XmlElement(name = "TotalSetupBurHrs")
    protected BigDecimal totalSetupBurHrs;
    @XmlElement(name = "TotalSetupLbrHrs")
    protected BigDecimal totalSetupLbrHrs;
    @XmlElement(name = "TotalSubCost")
    protected BigDecimal totalSubCost;
    @XmlElement(name = "UnitPrice")
    protected BigDecimal unitPrice;
    @XmlElement(name = "UserChangedUnitPrice")
    protected Boolean userChangedUnitPrice;
    @XmlElement(name = "WQUnitPrice")
    protected BigDecimal wqUnitPrice;

    /**
     * Gets the value of the baseCurrSymbol property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBaseCurrSymbol() {
        return baseCurrSymbol;
    }

    /**
     * Sets the value of the baseCurrSymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBaseCurrSymbol(JAXBElement<String> value) {
        this.baseCurrSymbol = value;
    }

    /**
     * Gets the value of the bitFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBitFlag() {
        return bitFlag;
    }

    /**
     * Sets the value of the bitFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBitFlag(Integer value) {
        this.bitFlag = value;
    }

    /**
     * Gets the value of the burdenMarkUp property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBurdenMarkUp() {
        return burdenMarkUp;
    }

    /**
     * Sets the value of the burdenMarkUp property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBurdenMarkUp(BigDecimal value) {
        this.burdenMarkUp = value;
    }

    /**
     * Gets the value of the calcMarkup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCalcMarkup() {
        return calcMarkup;
    }

    /**
     * Sets the value of the calcMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCalcMarkup(BigDecimal value) {
        this.calcMarkup = value;
    }

    /**
     * Gets the value of the calcMarkupProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCalcMarkupProfit() {
        return calcMarkupProfit;
    }

    /**
     * Sets the value of the calcMarkupProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCalcMarkupProfit(BigDecimal value) {
        this.calcMarkupProfit = value;
    }

    /**
     * Gets the value of the calcProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCalcProfit() {
        return calcProfit;
    }

    /**
     * Sets the value of the calcProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCalcProfit(BigDecimal value) {
        this.calcProfit = value;
    }

    /**
     * Gets the value of the calcProfitProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCalcProfitProfit() {
        return calcProfitProfit;
    }

    /**
     * Sets the value of the calcProfitProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCalcProfitProfit(BigDecimal value) {
        this.calcProfitProfit = value;
    }

    /**
     * Gets the value of the calcUPCommMarkup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCalcUPCommMarkup() {
        return calcUPCommMarkup;
    }

    /**
     * Sets the value of the calcUPCommMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCalcUPCommMarkup(BigDecimal value) {
        this.calcUPCommMarkup = value;
    }

    /**
     * Gets the value of the calcUPCommProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCalcUPCommProfit() {
        return calcUPCommProfit;
    }

    /**
     * Sets the value of the calcUPCommProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCalcUPCommProfit(BigDecimal value) {
        this.calcUPCommProfit = value;
    }

    /**
     * Gets the value of the calcUnitCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCalcUnitCost() {
        return calcUnitCost;
    }

    /**
     * Sets the value of the calcUnitCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCalcUnitCost(BigDecimal value) {
        this.calcUnitCost = value;
    }

    /**
     * Gets the value of the calcUnitPriceMarkup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCalcUnitPriceMarkup() {
        return calcUnitPriceMarkup;
    }

    /**
     * Sets the value of the calcUnitPriceMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCalcUnitPriceMarkup(BigDecimal value) {
        this.calcUnitPriceMarkup = value;
    }

    /**
     * Gets the value of the calcUnitPriceProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCalcUnitPriceProfit() {
        return calcUnitPriceProfit;
    }

    /**
     * Sets the value of the calcUnitPriceProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCalcUnitPriceProfit(BigDecimal value) {
        this.calcUnitPriceProfit = value;
    }

    /**
     * Gets the value of the changeDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getChangeDate() {
        return changeDate;
    }

    /**
     * Sets the value of the changeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setChangeDate(JAXBElement<XMLGregorianCalendar> value) {
        this.changeDate = value;
    }

    /**
     * Gets the value of the changeTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChangeTime() {
        return changeTime;
    }

    /**
     * Sets the value of the changeTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChangeTime(Integer value) {
        this.changeTime = value;
    }

    /**
     * Gets the value of the changedBy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangedBy() {
        return changedBy;
    }

    /**
     * Sets the value of the changedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangedBy(JAXBElement<String> value) {
        this.changedBy = value;
    }

    /**
     * Gets the value of the commissionPct property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCommissionPct() {
        return commissionPct;
    }

    /**
     * Sets the value of the commissionPct property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCommissionPct(BigDecimal value) {
        this.commissionPct = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompany(JAXBElement<String> value) {
        this.company = value;
    }

    /**
     * Gets the value of the currSymbol property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrSymbol() {
        return currSymbol;
    }

    /**
     * Sets the value of the currSymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrSymbol(JAXBElement<String> value) {
        this.currSymbol = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the currencySwitch property.
     * This getter has been renamed from isCurrencySwitch() to getCurrencySwitch() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCurrencySwitch() {
        return currencySwitch;
    }

    /**
     * Sets the value of the currencySwitch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCurrencySwitch(Boolean value) {
        this.currencySwitch = value;
    }

    /**
     * Gets the value of the disableMtlMarkup property.
     * This getter has been renamed from isDisableMtlMarkup() to getDisableMtlMarkup() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDisableMtlMarkup() {
        return disableMtlMarkup;
    }

    /**
     * Sets the value of the disableMtlMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisableMtlMarkup(Boolean value) {
        this.disableMtlMarkup = value;
    }

    /**
     * Gets the value of the docInUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocInUnitPrice() {
        return docInUnitPrice;
    }

    /**
     * Sets the value of the docInUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocInUnitPrice(BigDecimal value) {
        this.docInUnitPrice = value;
    }

    /**
     * Gets the value of the docPricePerAdl1000 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocPricePerAdl1000() {
        return docPricePerAdl1000;
    }

    /**
     * Sets the value of the docPricePerAdl1000 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocPricePerAdl1000(BigDecimal value) {
        this.docPricePerAdl1000 = value;
    }

    /**
     * Gets the value of the docTotalSellPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocTotalSellPrice() {
        return docTotalSellPrice;
    }

    /**
     * Sets the value of the docTotalSellPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocTotalSellPrice(BigDecimal value) {
        this.docTotalSellPrice = value;
    }

    /**
     * Gets the value of the docUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDocUnitPrice() {
        return docUnitPrice;
    }

    /**
     * Sets the value of the docUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDocUnitPrice(BigDecimal value) {
        this.docUnitPrice = value;
    }

    /**
     * Gets the value of the ium property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIUM() {
        return ium;
    }

    /**
     * Sets the value of the ium property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIUM(JAXBElement<String> value) {
        this.ium = value;
    }

    /**
     * Gets the value of the inUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInUnitPrice() {
        return inUnitPrice;
    }

    /**
     * Sets the value of the inUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInUnitPrice(BigDecimal value) {
        this.inUnitPrice = value;
    }

    /**
     * Gets the value of the laborMarkUp property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLaborMarkUp() {
        return laborMarkUp;
    }

    /**
     * Sets the value of the laborMarkUp property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLaborMarkUp(BigDecimal value) {
        this.laborMarkUp = value;
    }

    /**
     * Gets the value of the markUpID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMarkUpID() {
        return markUpID;
    }

    /**
     * Sets the value of the markUpID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMarkUpID(JAXBElement<String> value) {
        this.markUpID = value;
    }

    /**
     * Gets the value of the materialMarkUp property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaterialMarkUp() {
        return materialMarkUp;
    }

    /**
     * Sets the value of the materialMarkUp property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaterialMarkUp(BigDecimal value) {
        this.materialMarkUp = value;
    }

    /**
     * Gets the value of the materialMarkupM property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaterialMarkupM() {
        return materialMarkupM;
    }

    /**
     * Sets the value of the materialMarkupM property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaterialMarkupM(BigDecimal value) {
        this.materialMarkupM = value;
    }

    /**
     * Gets the value of the materialMarkupP property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaterialMarkupP() {
        return materialMarkupP;
    }

    /**
     * Sets the value of the materialMarkupP property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaterialMarkupP(BigDecimal value) {
        this.materialMarkupP = value;
    }

    /**
     * Gets the value of the miscChrg property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMiscChrg() {
        return miscChrg;
    }

    /**
     * Sets the value of the miscChrg property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMiscChrg(JAXBElement<String> value) {
        this.miscChrg = value;
    }

    /**
     * Gets the value of the miscCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMiscCost() {
        return miscCost;
    }

    /**
     * Sets the value of the miscCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMiscCost(BigDecimal value) {
        this.miscCost = value;
    }

    /**
     * Gets the value of the miscCostDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMiscCostDesc() {
        return miscCostDesc;
    }

    /**
     * Sets the value of the miscCostDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMiscCostDesc(JAXBElement<String> value) {
        this.miscCostDesc = value;
    }

    /**
     * Gets the value of the miscCostMarkUp property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMiscCostMarkUp() {
        return miscCostMarkUp;
    }

    /**
     * Sets the value of the miscCostMarkUp property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMiscCostMarkUp(BigDecimal value) {
        this.miscCostMarkUp = value;
    }

    /**
     * Gets the value of the mtlBurMarkUp property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMtlBurMarkUp() {
        return mtlBurMarkUp;
    }

    /**
     * Sets the value of the mtlBurMarkUp property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMtlBurMarkUp(BigDecimal value) {
        this.mtlBurMarkUp = value;
    }

    /**
     * Gets the value of the ourQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOurQuantity() {
        return ourQuantity;
    }

    /**
     * Sets the value of the ourQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOurQuantity(BigDecimal value) {
        this.ourQuantity = value;
    }

    /**
     * Gets the value of the percentType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPercentType() {
        return percentType;
    }

    /**
     * Sets the value of the percentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPercentType(JAXBElement<String> value) {
        this.percentType = value;
    }

    /**
     * Gets the value of the priceBurMarkup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceBurMarkup() {
        return priceBurMarkup;
    }

    /**
     * Sets the value of the priceBurMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceBurMarkup(BigDecimal value) {
        this.priceBurMarkup = value;
    }

    /**
     * Gets the value of the priceBurProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceBurProfit() {
        return priceBurProfit;
    }

    /**
     * Sets the value of the priceBurProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceBurProfit(BigDecimal value) {
        this.priceBurProfit = value;
    }

    /**
     * Gets the value of the priceLbrMarkup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceLbrMarkup() {
        return priceLbrMarkup;
    }

    /**
     * Sets the value of the priceLbrMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceLbrMarkup(BigDecimal value) {
        this.priceLbrMarkup = value;
    }

    /**
     * Gets the value of the priceLbrProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceLbrProfit() {
        return priceLbrProfit;
    }

    /**
     * Sets the value of the priceLbrProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceLbrProfit(BigDecimal value) {
        this.priceLbrProfit = value;
    }

    /**
     * Gets the value of the priceMscChrgMarkup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceMscChrgMarkup() {
        return priceMscChrgMarkup;
    }

    /**
     * Sets the value of the priceMscChrgMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceMscChrgMarkup(BigDecimal value) {
        this.priceMscChrgMarkup = value;
    }

    /**
     * Gets the value of the priceMscChrgProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceMscChrgProfit() {
        return priceMscChrgProfit;
    }

    /**
     * Sets the value of the priceMscChrgProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceMscChrgProfit(BigDecimal value) {
        this.priceMscChrgProfit = value;
    }

    /**
     * Gets the value of the priceMtlBurMarkup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceMtlBurMarkup() {
        return priceMtlBurMarkup;
    }

    /**
     * Sets the value of the priceMtlBurMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceMtlBurMarkup(BigDecimal value) {
        this.priceMtlBurMarkup = value;
    }

    /**
     * Gets the value of the priceMtlBurProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceMtlBurProfit() {
        return priceMtlBurProfit;
    }

    /**
     * Sets the value of the priceMtlBurProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceMtlBurProfit(BigDecimal value) {
        this.priceMtlBurProfit = value;
    }

    /**
     * Gets the value of the priceMtlMarkup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceMtlMarkup() {
        return priceMtlMarkup;
    }

    /**
     * Sets the value of the priceMtlMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceMtlMarkup(BigDecimal value) {
        this.priceMtlMarkup = value;
    }

    /**
     * Gets the value of the priceMtlProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceMtlProfit() {
        return priceMtlProfit;
    }

    /**
     * Sets the value of the priceMtlProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceMtlProfit(BigDecimal value) {
        this.priceMtlProfit = value;
    }

    /**
     * Gets the value of the pricePerAdl1000 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPricePerAdl1000() {
        return pricePerAdl1000;
    }

    /**
     * Sets the value of the pricePerAdl1000 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPricePerAdl1000(BigDecimal value) {
        this.pricePerAdl1000 = value;
    }

    /**
     * Gets the value of the pricePerCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPricePerCode() {
        return pricePerCode;
    }

    /**
     * Sets the value of the pricePerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPricePerCode(JAXBElement<String> value) {
        this.pricePerCode = value;
    }

    /**
     * Gets the value of the pricePerFactor property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPricePerFactor() {
        return pricePerFactor;
    }

    /**
     * Sets the value of the pricePerFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPricePerFactor(Integer value) {
        this.pricePerFactor = value;
    }

    /**
     * Gets the value of the priceSource property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPriceSource() {
        return priceSource;
    }

    /**
     * Sets the value of the priceSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPriceSource(JAXBElement<String> value) {
        this.priceSource = value;
    }

    /**
     * Gets the value of the priceSubMarkup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceSubMarkup() {
        return priceSubMarkup;
    }

    /**
     * Sets the value of the priceSubMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceSubMarkup(BigDecimal value) {
        this.priceSubMarkup = value;
    }

    /**
     * Gets the value of the priceSubProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceSubProfit() {
        return priceSubProfit;
    }

    /**
     * Sets the value of the priceSubProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceSubProfit(BigDecimal value) {
        this.priceSubProfit = value;
    }

    /**
     * Gets the value of the priceTotalCommMarkup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceTotalCommMarkup() {
        return priceTotalCommMarkup;
    }

    /**
     * Sets the value of the priceTotalCommMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceTotalCommMarkup(BigDecimal value) {
        this.priceTotalCommMarkup = value;
    }

    /**
     * Gets the value of the priceTotalCommProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceTotalCommProfit() {
        return priceTotalCommProfit;
    }

    /**
     * Sets the value of the priceTotalCommProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceTotalCommProfit(BigDecimal value) {
        this.priceTotalCommProfit = value;
    }

    /**
     * Gets the value of the priceTotalMarkup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceTotalMarkup() {
        return priceTotalMarkup;
    }

    /**
     * Sets the value of the priceTotalMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceTotalMarkup(BigDecimal value) {
        this.priceTotalMarkup = value;
    }

    /**
     * Gets the value of the priceTotalProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPriceTotalProfit() {
        return priceTotalProfit;
    }

    /**
     * Sets the value of the priceTotalProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPriceTotalProfit(BigDecimal value) {
        this.priceTotalProfit = value;
    }

    /**
     * Gets the value of the qtyNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQtyNum() {
        return qtyNum;
    }

    /**
     * Sets the value of the qtyNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQtyNum(Integer value) {
        this.qtyNum = value;
    }

    /**
     * Gets the value of the quoteLine property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQuoteLine() {
        return quoteLine;
    }

    /**
     * Sets the value of the quoteLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQuoteLine(Integer value) {
        this.quoteLine = value;
    }

    /**
     * Gets the value of the quoteLineLineDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getQuoteLineLineDesc() {
        return quoteLineLineDesc;
    }

    /**
     * Sets the value of the quoteLineLineDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setQuoteLineLineDesc(JAXBElement<String> value) {
        this.quoteLineLineDesc = value;
    }

    /**
     * Gets the value of the quoteNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQuoteNum() {
        return quoteNum;
    }

    /**
     * Sets the value of the quoteNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQuoteNum(Integer value) {
        this.quoteNum = value;
    }

    /**
     * Gets the value of the quoteNumCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getQuoteNumCurrencyCode() {
        return quoteNumCurrencyCode;
    }

    /**
     * Sets the value of the quoteNumCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setQuoteNumCurrencyCode(JAXBElement<String> value) {
        this.quoteNumCurrencyCode = value;
    }

    /**
     * Gets the value of the quotedMarkup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQuotedMarkup() {
        return quotedMarkup;
    }

    /**
     * Sets the value of the quotedMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQuotedMarkup(BigDecimal value) {
        this.quotedMarkup = value;
    }

    /**
     * Gets the value of the quotedProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQuotedProfit() {
        return quotedProfit;
    }

    /**
     * Sets the value of the quotedProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQuotedProfit(BigDecimal value) {
        this.quotedProfit = value;
    }

    /**
     * Gets the value of the rollUpCostNeeded property.
     * This getter has been renamed from isRollUpCostNeeded() to getRollUpCostNeeded() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRollUpCostNeeded() {
        return rollUpCostNeeded;
    }

    /**
     * Sets the value of the rollUpCostNeeded property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRollUpCostNeeded(Boolean value) {
        this.rollUpCostNeeded = value;
    }

    /**
     * Gets the value of the rpt1InUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1InUnitPrice() {
        return rpt1InUnitPrice;
    }

    /**
     * Sets the value of the rpt1InUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1InUnitPrice(BigDecimal value) {
        this.rpt1InUnitPrice = value;
    }

    /**
     * Gets the value of the rpt1UnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt1UnitPrice() {
        return rpt1UnitPrice;
    }

    /**
     * Sets the value of the rpt1UnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt1UnitPrice(BigDecimal value) {
        this.rpt1UnitPrice = value;
    }

    /**
     * Gets the value of the rpt2InUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2InUnitPrice() {
        return rpt2InUnitPrice;
    }

    /**
     * Sets the value of the rpt2InUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2InUnitPrice(BigDecimal value) {
        this.rpt2InUnitPrice = value;
    }

    /**
     * Gets the value of the rpt2UnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt2UnitPrice() {
        return rpt2UnitPrice;
    }

    /**
     * Sets the value of the rpt2UnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt2UnitPrice(BigDecimal value) {
        this.rpt2UnitPrice = value;
    }

    /**
     * Gets the value of the rpt3InUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3InUnitPrice() {
        return rpt3InUnitPrice;
    }

    /**
     * Sets the value of the rpt3InUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3InUnitPrice(BigDecimal value) {
        this.rpt3InUnitPrice = value;
    }

    /**
     * Gets the value of the rpt3UnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRpt3UnitPrice() {
        return rpt3UnitPrice;
    }

    /**
     * Sets the value of the rpt3UnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRpt3UnitPrice(BigDecimal value) {
        this.rpt3UnitPrice = value;
    }

    /**
     * Gets the value of the salesUM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesUM() {
        return salesUM;
    }

    /**
     * Sets the value of the salesUM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesUM(JAXBElement<String> value) {
        this.salesUM = value;
    }

    /**
     * Gets the value of the sellingFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSellingFactor() {
        return sellingFactor;
    }

    /**
     * Sets the value of the sellingFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSellingFactor(BigDecimal value) {
        this.sellingFactor = value;
    }

    /**
     * Gets the value of the sellingFactorDirection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSellingFactorDirection() {
        return sellingFactorDirection;
    }

    /**
     * Sets the value of the sellingFactorDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSellingFactorDirection(JAXBElement<String> value) {
        this.sellingFactorDirection = value;
    }

    /**
     * Gets the value of the sellingQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSellingQuantity() {
        return sellingQuantity;
    }

    /**
     * Sets the value of the sellingQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSellingQuantity(BigDecimal value) {
        this.sellingQuantity = value;
    }

    /**
     * Gets the value of the subcontractMarkUp property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSubcontractMarkUp() {
        return subcontractMarkUp;
    }

    /**
     * Sets the value of the subcontractMarkUp property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSubcontractMarkUp(BigDecimal value) {
        this.subcontractMarkUp = value;
    }

    /**
     * Gets the value of the sysRevID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSysRevID() {
        return sysRevID;
    }

    /**
     * Sets the value of the sysRevID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSysRevID(Long value) {
        this.sysRevID = value;
    }

    /**
     * Gets the value of the totalBurCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalBurCost() {
        return totalBurCost;
    }

    /**
     * Sets the value of the totalBurCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalBurCost(BigDecimal value) {
        this.totalBurCost = value;
    }

    /**
     * Gets the value of the totalCommProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalCommProfit() {
        return totalCommProfit;
    }

    /**
     * Sets the value of the totalCommProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalCommProfit(BigDecimal value) {
        this.totalCommProfit = value;
    }

    /**
     * Gets the value of the totalCommission property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalCommission() {
        return totalCommission;
    }

    /**
     * Sets the value of the totalCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalCommission(BigDecimal value) {
        this.totalCommission = value;
    }

    /**
     * Gets the value of the totalCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalCost() {
        return totalCost;
    }

    /**
     * Sets the value of the totalCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalCost(BigDecimal value) {
        this.totalCost = value;
    }

    /**
     * Gets the value of the totalLbrCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalLbrCost() {
        return totalLbrCost;
    }

    /**
     * Sets the value of the totalLbrCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalLbrCost(BigDecimal value) {
        this.totalLbrCost = value;
    }

    /**
     * Gets the value of the totalMarkup property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalMarkup() {
        return totalMarkup;
    }

    /**
     * Sets the value of the totalMarkup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalMarkup(BigDecimal value) {
        this.totalMarkup = value;
    }

    /**
     * Gets the value of the totalMtlBurCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalMtlBurCost() {
        return totalMtlBurCost;
    }

    /**
     * Sets the value of the totalMtlBurCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalMtlBurCost(BigDecimal value) {
        this.totalMtlBurCost = value;
    }

    /**
     * Gets the value of the totalMtlCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalMtlCost() {
        return totalMtlCost;
    }

    /**
     * Sets the value of the totalMtlCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalMtlCost(BigDecimal value) {
        this.totalMtlCost = value;
    }

    /**
     * Gets the value of the totalProdBurHrs property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalProdBurHrs() {
        return totalProdBurHrs;
    }

    /**
     * Sets the value of the totalProdBurHrs property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalProdBurHrs(BigDecimal value) {
        this.totalProdBurHrs = value;
    }

    /**
     * Gets the value of the totalProdLbrHrs property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalProdLbrHrs() {
        return totalProdLbrHrs;
    }

    /**
     * Sets the value of the totalProdLbrHrs property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalProdLbrHrs(BigDecimal value) {
        this.totalProdLbrHrs = value;
    }

    /**
     * Gets the value of the totalProfit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalProfit() {
        return totalProfit;
    }

    /**
     * Sets the value of the totalProfit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalProfit(BigDecimal value) {
        this.totalProfit = value;
    }

    /**
     * Gets the value of the totalQuotedPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalQuotedPrice() {
        return totalQuotedPrice;
    }

    /**
     * Sets the value of the totalQuotedPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalQuotedPrice(BigDecimal value) {
        this.totalQuotedPrice = value;
    }

    /**
     * Gets the value of the totalSellPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalSellPrice() {
        return totalSellPrice;
    }

    /**
     * Sets the value of the totalSellPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalSellPrice(BigDecimal value) {
        this.totalSellPrice = value;
    }

    /**
     * Gets the value of the totalSetupBurHrs property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalSetupBurHrs() {
        return totalSetupBurHrs;
    }

    /**
     * Sets the value of the totalSetupBurHrs property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalSetupBurHrs(BigDecimal value) {
        this.totalSetupBurHrs = value;
    }

    /**
     * Gets the value of the totalSetupLbrHrs property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalSetupLbrHrs() {
        return totalSetupLbrHrs;
    }

    /**
     * Sets the value of the totalSetupLbrHrs property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalSetupLbrHrs(BigDecimal value) {
        this.totalSetupLbrHrs = value;
    }

    /**
     * Gets the value of the totalSubCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalSubCost() {
        return totalSubCost;
    }

    /**
     * Sets the value of the totalSubCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalSubCost(BigDecimal value) {
        this.totalSubCost = value;
    }

    /**
     * Gets the value of the unitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * Sets the value of the unitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUnitPrice(BigDecimal value) {
        this.unitPrice = value;
    }

    /**
     * Gets the value of the userChangedUnitPrice property.
     * This getter has been renamed from isUserChangedUnitPrice() to getUserChangedUnitPrice() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getUserChangedUnitPrice() {
        return userChangedUnitPrice;
    }

    /**
     * Sets the value of the userChangedUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUserChangedUnitPrice(Boolean value) {
        this.userChangedUnitPrice = value;
    }

    /**
     * Gets the value of the wqUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWQUnitPrice() {
        return wqUnitPrice;
    }

    /**
     * Sets the value of the wqUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWQUnitPrice(BigDecimal value) {
        this.wqUnitPrice = value;
    }

}
