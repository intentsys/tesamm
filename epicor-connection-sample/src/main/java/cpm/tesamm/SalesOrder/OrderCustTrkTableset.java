
package cpm.tesamm.SalesOrder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderCustTrkTableset complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderCustTrkTableset">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Ice}IceTableset">
 *       &lt;sequence>
 *         &lt;element name="OrderCustTrk" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}OrderCustTrkTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderCustTrkTableset", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "orderCustTrk"
})
public class OrderCustTrkTableset
    extends IceTableset
{

    @XmlElementRef(name = "OrderCustTrk", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", type = JAXBElement.class, required = false)
    protected JAXBElement<OrderCustTrkTable> orderCustTrk;

    /**
     * Gets the value of the orderCustTrk property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OrderCustTrkTable }{@code >}
     *     
     */
    public JAXBElement<OrderCustTrkTable> getOrderCustTrk() {
        return orderCustTrk;
    }

    /**
     * Sets the value of the orderCustTrk property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OrderCustTrkTable }{@code >}
     *     
     */
    public void setOrderCustTrk(JAXBElement<OrderCustTrkTable> value) {
        this.orderCustTrk = value;
    }

}
