
package cpm.tesamm.SalesOrder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GlbSugPOChgTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GlbSugPOChgTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GlbSugPOChgRow" type="{http://schemas.datacontract.org/2004/07/Erp.Tablesets}GlbSugPOChgRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GlbSugPOChgTable", namespace = "http://schemas.datacontract.org/2004/07/Erp.Tablesets", propOrder = {
    "glbSugPOChgRow"
})
public class GlbSugPOChgTable {

    @XmlElement(name = "GlbSugPOChgRow", nillable = true)
    protected List<GlbSugPOChgRow> glbSugPOChgRow;

    /**
     * Gets the value of the glbSugPOChgRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the glbSugPOChgRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGlbSugPOChgRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GlbSugPOChgRow }
     * 
     * 
     */
    public List<GlbSugPOChgRow> getGlbSugPOChgRow() {
        if (glbSugPOChgRow == null) {
            glbSugPOChgRow = new ArrayList<GlbSugPOChgRow>();
        }
        return this.glbSugPOChgRow;
    }

    /**
     * Sets the value of the glbSugPOChgRow property.
     * 
     * @param glbSugPOChgRow
     *     allowed object is
     *     {@link GlbSugPOChgRow }
     *     
     */
    public void setGlbSugPOChgRow(List<GlbSugPOChgRow> glbSugPOChgRow) {
        this.glbSugPOChgRow = glbSugPOChgRow;
    }

}
