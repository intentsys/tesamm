package org.mule.modules.epicor.config;

import java.util.ArrayList;
import java.util.List;

import org.mule.api.annotations.ws.WsdlSecurity;
import org.mule.devkit.api.ws.authentication.WsdlSecurityStrategy;
import org.mule.devkit.api.ws.authentication.WsdlUsernameToken;
import org.mule.module.ws.security.PasswordType;
import org.mule.api.annotations.Configurable;
import org.mule.api.annotations.display.Password;
import org.mule.api.annotations.display.Placement;
import org.mule.api.annotations.components.WsdlProvider;
import org.mule.api.annotations.ws.WsdlServiceEndpoint;
import org.mule.api.annotations.ws.WsdlServiceRetriever;
import org.mule.devkit.api.ws.definition.DefaultServiceDefinition;
import org.mule.devkit.api.ws.definition.ServiceDefinition;
import org.mule.api.annotations.param.Optional;

@WsdlProvider(friendlyName = "Configuration")
public class ConnectorConfig {

    @Configurable
    @Placement(order = 1)
    private String username;

    @Configurable
    @Placement(order = 2)
    @Password
    @Optional
    private String password;

    @WsdlServiceRetriever
    public List<ServiceDefinition> getServiceDefinitions() {

        final List<ServiceDefinition> serviceDefinitions = new ArrayList<ServiceDefinition>();

        serviceDefinitions.add(new DefaultServiceDefinition(
                "POSvcFacade_WSHttpBinding_POSvcContract",
                "PO.svc (WSHttpBinding_POSvcContract)",
                "https://rds-tts.tes-amm.com/ERP100700DEMO-SSL/Erp/BO/PO.svc?wsdl",
                "POSvcFacade",
                "WSHttpBinding_POSvcContract"));

        serviceDefinitions.add(new DefaultServiceDefinition(
                "POSvcFacade_CustomBinding_POSvcContract",
                "PO.svc (CustomBinding_POSvcContract)",
                "https://rds-tts.tes-amm.com/ERP100700DEMO-SSL/Erp/BO/PO.svc?wsdl",
                "POSvcFacade",
                "CustomBinding_POSvcContract"));

        serviceDefinitions.add(new DefaultServiceDefinition(
                "POSvcFacade_BasicHttpBinding_POSvcContract",
                "PO.svc (BasicHttpBinding_POSvcContract)",
                "https://rds-tts.tes-amm.com/ERP100700DEMO-SSL/Erp/BO/PO.svc?wsdl",
                "POSvcFacade",
                "BasicHttpBinding_POSvcContract"));

        serviceDefinitions.add(new DefaultServiceDefinition(
                "SalesOrderSvcFacade_WSHttpBinding_SalesOrderSvcContract",
                "SalesOrder.svc (WSHttpBinding_SalesOrderSvcContract)",
                "https://rds-tts.tes-amm.com/ERP100700DEMO-SSL/Erp/BO/SalesOrder.svc?wsdl",
                "SalesOrderSvcFacade",
                "WSHttpBinding_SalesOrderSvcContract"));

        serviceDefinitions.add(new DefaultServiceDefinition(
                "SalesOrderSvcFacade_CustomBinding_SalesOrderSvcContract",
                "SalesOrder.svc (CustomBinding_SalesOrderSvcContract)",
                "https://rds-tts.tes-amm.com/ERP100700DEMO-SSL/Erp/BO/SalesOrder.svc?wsdl",
                "SalesOrderSvcFacade",
                "CustomBinding_SalesOrderSvcContract"));

        serviceDefinitions.add(new DefaultServiceDefinition(
                "SalesOrderSvcFacade_BasicHttpBinding_SalesOrderSvcContract",
                "SalesOrder.svc (BasicHttpBinding_SalesOrderSvcContract)",
                "https://rds-tts.tes-amm.com/ERP100700DEMO-SSL/Erp/BO/SalesOrder.svc?wsdl",
                "SalesOrderSvcFacade",
                "BasicHttpBinding_SalesOrderSvcContract"));
        return serviceDefinitions;
    }

    @WsdlServiceEndpoint
    public String getServiceEndpoint(ServiceDefinition definition) {
        String result;
        final String id = definition.getId();
        switch(id){
                case "POSvcFacade_WSHttpBinding_POSvcContract": {
                result = "http://tes-amm-epapp0.tes-amm.com/ERP100700DEMO-SSL/Erp/BO/PO.svc";
                break;
            }
                case "POSvcFacade_CustomBinding_POSvcContract": {
                result = "net.tcp://tes-amm-epapp0.tes-amm.com/ERP100700DEMO-SSL/Erp/BO/PO.svc";
                break;
            }
                case "POSvcFacade_BasicHttpBinding_POSvcContract": {
                result = "https://tes-amm-epapp0.tes-amm.com/ERP100700DEMO-SSL/Erp/BO/PO.svc";
                break;
            }
                case "SalesOrderSvcFacade_WSHttpBinding_SalesOrderSvcContract": {
                result = "http://tes-amm-epapp0.tes-amm.com/ERP100700DEMO-SSL/Erp/BO/SalesOrder.svc";
                break;
            }
                case "SalesOrderSvcFacade_CustomBinding_SalesOrderSvcContract": {
                result = "net.tcp://tes-amm-epapp0.tes-amm.com/ERP100700DEMO-SSL/Erp/BO/SalesOrder.svc";
                break;
            }
                case "SalesOrderSvcFacade_BasicHttpBinding_SalesOrderSvcContract": {
                result = "https://tes-amm-epapp0.tes-amm.com/ERP100700DEMO-SSL/Erp/BO/SalesOrder.svc";
                break;
            }
                default: {
                throw new IllegalArgumentException(id + " endpoint could not be resolved.");
            }
        }
        return result;
    }

    @WsdlSecurity
    public List<WsdlSecurityStrategy> getWsdlSecurityResolver(ServiceDefinition definition) {
        List<WsdlSecurityStrategy> result = new ArrayList<WsdlSecurityStrategy>();
        result.add(new WsdlUsernameToken(getUsername(),  getPassword(), PasswordType.TEXT, true, true));
        return result;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}