package org.mule.modules.epicor;

import org.mule.api.annotations.Config;
import org.mule.api.annotations.Connector;
import org.mule.modules.epicor.config.ConnectorConfig;

@Connector(name="epicor", friendlyName="Epicor", minMuleVersion = "3.7")
public class EpicorConnector {

    @Config
    ConnectorConfig config;

    public ConnectorConfig getConfig() {
        return config;
    }

    public void setConfig(ConnectorConfig config) {
        this.config = config;
    }

}