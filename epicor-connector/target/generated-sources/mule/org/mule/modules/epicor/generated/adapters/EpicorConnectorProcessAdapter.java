
package org.mule.modules.epicor.generated.adapters;

import javax.annotation.Generated;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.devkit.ProcessAdapter;
import org.mule.api.devkit.ProcessTemplate;
import org.mule.api.processor.MessageProcessor;
import org.mule.api.routing.filter.Filter;
import org.mule.modules.epicor.EpicorConnector;
import org.mule.security.oauth.callback.ProcessCallback;


/**
 * A <code>EpicorConnectorProcessAdapter</code> is a wrapper around {@link EpicorConnector } that enables custom processing strategies.
 * 
 */
@SuppressWarnings("all")
@Generated(value = "Mule DevKit Version 3.8.0", date = "2016-02-11T11:15:00+08:00", comments = "Build UNNAMED.2762.e3b1307")
public class EpicorConnectorProcessAdapter
    extends EpicorConnectorLifecycleInjectionAdapter
    implements ProcessAdapter<EpicorConnectorCapabilitiesAdapter>
{


    public<P >ProcessTemplate<P, EpicorConnectorCapabilitiesAdapter> getProcessTemplate() {
        final EpicorConnectorCapabilitiesAdapter object = this;
        return new ProcessTemplate<P,EpicorConnectorCapabilitiesAdapter>() {


            @Override
            public P execute(ProcessCallback<P, EpicorConnectorCapabilitiesAdapter> processCallback, MessageProcessor messageProcessor, MuleEvent event)
                throws Exception
            {
                return processCallback.process(object);
            }

            @Override
            public P execute(ProcessCallback<P, EpicorConnectorCapabilitiesAdapter> processCallback, Filter filter, MuleMessage message)
                throws Exception
            {
                return processCallback.process(object);
            }

        }
        ;
    }

}
