
package org.mule.modules.epicor.generated.adapters;

import javax.annotation.Generated;
import org.mule.api.devkit.capability.Capabilities;
import org.mule.api.devkit.capability.ModuleCapability;
import org.mule.modules.epicor.EpicorConnector;


/**
 * A <code>EpicorConnectorCapabilitiesAdapter</code> is a wrapper around {@link EpicorConnector } that implements {@link org.mule.api.Capabilities} interface.
 * 
 */
@SuppressWarnings("all")
@Generated(value = "Mule DevKit Version 3.8.0", date = "2016-02-11T11:15:00+08:00", comments = "Build UNNAMED.2762.e3b1307")
public class EpicorConnectorCapabilitiesAdapter
    extends EpicorConnector
    implements Capabilities
{


    /**
     * Returns true if this module implements such capability
     * 
     */
    public boolean isCapableOf(ModuleCapability capability) {
        if (capability == ModuleCapability.LIFECYCLE_CAPABLE) {
            return true;
        }
        return false;
    }

}
